package com.ofbox.davivienda.h2h.proc;

import java.util.Date;
import java.util.Hashtable;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ofbox.davivienda.h2h.abstraccion.dao.HthBitacoraProcesoDao;
import com.ofbox.davivienda.h2h.abstraccion.dto.HthBitacoraProceso;
import com.ofbox.davivienda.h2h.dependenciasGlobales.H2hResolusor;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.ExcepcionEnDAO;

public class LogHandlerContainer {

	Logger logger = LoggerFactory.getLogger(LogHandlerContainer.class);
	
	private static LogHandlerContainer instance;
	
	private Hashtable<Integer, ProcessLogHandler> handlers;
	
	private LogHandlerContainer(){
		handlers = new Hashtable<Integer, ProcessLogHandler>();
	}
	
	public static LogHandlerContainer getSingleton(){
		if(instance == null){
			instance = new LogHandlerContainer();
		}
		return instance;
	}
	
	
	
	public ProcessLogHandler createLogHandler(Integer idProceso){
		if(handlers.containsKey(idProceso)){
			logger.warn("Ya existe un manejador de bitacora para el proceso {}. Se registrara uno nuevo...", idProceso);
			ProcessLogHandler handler = handlers.get(idProceso);
			handler.log(" - - - [FINALIZADO]  - - - ");
			saveLogHandler(handler);
			handlers.remove(idProceso);
		}
		ProcessLogHandler newHandler = new ProcessLogHandler();
		newHandler.setIdProceso(idProceso);
		newHandler.log(" - - - [INICIADO] - - - ");
		handlers.put(idProceso, newHandler);
		return newHandler;
	}
	
	public ProcessLogHandler getLogHandler(int idProceso){
		if(handlers.containsKey(idProceso)){
			return handlers.get(idProceso);
		}
		return null;
	}
	
	public boolean logHanderExists(Integer idProceso){
		return handlers.containsKey(idProceso);
	}
	
	public void saveLogHandler(ProcessLogHandler handler){
		HthBitacoraProcesoDao dao = (HthBitacoraProcesoDao) H2hResolusor.getSingleton().getInstancia("hthBitacoraProcesoDao");
		HthBitacoraProceso bitacora = new HthBitacoraProceso();
		
		StringBuilder cadena = new StringBuilder();
		Integer cantidadArchivo = handler.getNombreArchivoList() != null ? handler.getNombreArchivoList().size() : 0;
		Integer recorrido = 0;
		if (cantidadArchivo > 0) {
			for (String nombreArchivo : handler.getNombreArchivoList()) {
				recorrido++;
				cadena.append(nombreArchivo);
				if (cantidadArchivo != recorrido) {
					cadena.append(", ");
				}		
			}
		}
					
		bitacora.setArchivo(cadena.toString().length() == 200 ? cadena.toString().substring(0, 195) + "....." : cadena.toString());
		bitacora.setCliente(handler.getCliente());
		bitacora.setConvenio(handler.getConvenio());
		bitacora.setDescripcionResultado(handler.getDescripcionResultado());
		bitacora.setFechaProceso(new Date());
		bitacora.setResultadoProceso(handler.getResultado());
		String lineas = handler.getBuffer().toString();
		if(lineas.length() >= 4000){ 
			lineas = lineas.substring(0,3888) + " [TRIMMED]";
		}
		bitacora.setLineasBitacora(lineas);
		if(handler.getLastException()!=null){
			String strTrace = ExceptionUtils.getStackTrace(handler.getLastException());
			if(strTrace.length() >= 2000){ 
				strTrace = strTrace.substring(0, 1990) + "(...)";
			}
			bitacora.setStackTrace(strTrace);
		}else{
			bitacora.setStackTrace("N/A");	
		}
		
		try {
			dao.crear(bitacora);
			
			removeLogHandler(handler.getIdProceso());
		} catch (ExcepcionEnDAO e) {
			logger.error("Error al guardar registro de bitacora : " + bitacora, e);
		}
	}
	
	public void removeLogHandler(Integer idProceso){
		if(handlers.containsKey(idProceso)){
			logger.info("Removiendo Manejador de Bitacora para el proceso: {}", idProceso);
			handlers.remove(idProceso);
		}else{
			logger.warn("El manejador del proceso {} no se encontraba registrado.");
		}
	}
	
}
