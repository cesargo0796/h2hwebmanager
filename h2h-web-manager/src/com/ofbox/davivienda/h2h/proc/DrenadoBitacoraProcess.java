package com.ofbox.davivienda.h2h.proc;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ibm.icu.util.Calendar;
import com.ofbox.davivienda.h2h.dependenciasGlobales.H2hResolusor;

public class DrenadoBitacoraProcess implements Job{

	Logger logger = LoggerFactory.getLogger(DrenadoBitacoraProcess.class);
	
	public void execute(JobExecutionContext context)
			throws JobExecutionException {
		
		logger.info("Iniciando ejecucion de drenado de bitacora...");
		
		Connection conn = null;
		try{
			conn = H2hResolusor.getSingleton().getLocalizadorDeConexionPrincipal().getConnection();
			QueryRunner runner = new QueryRunner();
			Integer tiempoDrenado = (Integer)runner.query(conn, "select top 1 tiempo_drenado_bitacora from Hth_Parametro_Sistema", new ScalarHandler());
			if(tiempoDrenado == null){
				tiempoDrenado = 2;
				logger.warn("No se ha configurado tiempo de drenado de bitacora.  Se asume valor de {} meses de drenado", tiempoDrenado);
			}
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.MONTH, (-1*tiempoDrenado));
			
			Timestamp delBefore = new Timestamp(cal.getTimeInMillis());
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy HH:mm");
			
			logger.info("Eliminando los registros de bitacora anteriores a la fecha {} ", sdf.format(delBefore));
			
			int results = runner.update(conn, "delete from Hth_Bitacora_Proceso where fecha_proceso <= ? ", delBefore);
			
			logger.info("Se han eliminado {} registros de la bitacora como parte del proceso de Drenado.", results);
			
			logger.info("Finalizando tarea de Drenado. ");
			
		}catch(SQLException e){
			e.printStackTrace();
			logger.error("Error al tratar de eliminar bitacora de procesos: " + e.getMessage(), e);
		}finally{
			DbUtils.closeQuietly(conn);
		}
		
	}

}
