package com.ofbox.davivienda.h2h.proc;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.sql.DataSource;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.MapHandler;
import org.apache.commons.lang.StringUtils;
import org.quartz.CronTrigger;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.SimpleTrigger;
import org.quartz.impl.JobDetailImpl;
import org.quartz.impl.StdSchedulerFactory;
import org.quartz.impl.matchers.GroupMatcher;
import org.quartz.impl.triggers.CronTriggerImpl;
import org.quartz.impl.triggers.SimpleTriggerImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ibm.icu.util.Calendar;
import com.ofbox.davivienda.h2h.proc.file.FileTransferHelper;
import com.ofbox.davivienda.h2h.seguridad.Cifrador;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.util.jdbc.LocalizadorDeConexion;

public class PoolProcessManager {
	
	Logger logger = LoggerFactory.getLogger(PoolProcessManager.class); 
	
	private LocalizadorDeConexion localizadorDeConexion;
	
	/* Calendarizador de quartz */
	private Scheduler scheduler;
	private SchedulerFactory factorysch;
	
	/* Parametros del sistema */
	private Map<String, Object> systemParams;
	
	private static PoolProcessManager instance;
	
	private PoolProcessManager() {} 
	
	public static PoolProcessManager getInstance(){
		if(instance == null){
			instance = new PoolProcessManager();
		}
		return instance;
	}


	public LocalizadorDeConexion getLocalizadorDeConexion() {
		return localizadorDeConexion;
	}

	public void setLocalizadorDeConexion(LocalizadorDeConexion localizadorDeConexion) {
		this.localizadorDeConexion = localizadorDeConexion;
	}

	public Scheduler getScheduler(){
		return scheduler;
	}
	/**
	 * Inicializacion
	 * @throws SchedulerException 
	 */
	public void initialize() throws SchedulerException{
		/*Instanciar el scheduler en la variable scheduler si es null.  Instanciar StdScheduler*/
		if (scheduler == null) {
			Properties props = new Properties();
			props.setProperty("org.quartz.scheduler.skipUpdateCheck","true");
			props.setProperty("org.quartz.jobStore.class", "org.quartz.simpl.RAMJobStore");
			props.setProperty("org.quartz.threadPool.class", "org.quartz.simpl.SimpleThreadPool");
			props.setProperty("org.quartz.threadPool.threadCount", "10");
			
				
			factorysch = new StdSchedulerFactory();
			scheduler = factorysch.getScheduler();
			
		}
		
		try{
			/*Arrancar Scheduler y que este listo para ser ejecutado*/
			scheduler.start();
			 /*Parametros del sistema*/
			loadSystemParams();
			startMonitor();
		}catch(Exception e){
			throw new SchedulerException("Error al cargar los parametros del Sistema: " + e.getMessage(), e);
		}
		
	}
	
	
	/**
	 * Metodo que se encarga de inicializar al proceso de monitoreo
	 * principal.  También tiene la tarea de inicializar el calendarizador
	 * de Quartz y leer de la configuracion de la base de datos
	 * la configuracion para la ejecucion del proceso principal.
	 */
	public void startMonitor(){
		/*Obtener los parametros de ejecucion del sistema (Tabla hthParametroSistema)*/
		
		
		try{
			 
			String horaInicio = (String) systemParams.get("hora_inicio_monitoreo");
			String horaFin    = (String) systemParams.get("hora_fin_monitoreo");
			
			scheduleProcesses(horaInicio, horaFin);
		} catch (ParseException e) {
			logger.error("Error en formato de fechas configuradas en Base de Datos. El formato valido es HH:mm (formato de 24h)." + e.getMessage(), e);
		} catch (SchedulerException e) {
			logger.error("Error en la calendarizacion de los procesos. " + e.getMessage(), e);
		}
	}
	
	
	/**
	 * Metodo que se encarga de detener todos los procesos que se encuentren
	 * dentro del calendarizador de Quartz y que ademas debera detener el
	 * proceso de monitoreo principal 
	 * @throws SchedulerException 
	 */
	public void stopAll() throws SchedulerException{
		 /*Desregistrar todos los jobs que esten en el Scheduler
		 Desregistrar el job del MainMonitor*/
		List<String> jobGroupNames = scheduler.getJobGroupNames(); /*Esto devuelve el nombre de todos los grupos*/
		for(String group : jobGroupNames){
			for(JobKey jobKey : scheduler.getJobKeys(GroupMatcher.jobGroupEquals(group))){
				String jobName = jobKey.getName();
				scheduler.deleteJob(new JobKey(jobName,group));
			}
		}
	}
	
	
	/**
	 * Metodo que apaga el calendarizador

	 */
	public void finalize(){
		try {
			this.stopAll();
			scheduler.shutdown();
		} catch (SchedulerException e) {
			e.printStackTrace();
		}
		
	}
	
	
	/**
	 * Metodo que retorna la fecha en que deberia de iniciarse
	 * el proceso segun la hora de inicio que se recibe como parametro
	 * @param horaInicioMonitoreo en formato HH:mm 
	 * @return
	 * @throws ParseException 
	 * @throws SchedulerException 
	 * 
	 */
	protected void scheduleProcesses(String horaInicioMonitoreo, String horaFinMonitoreo) throws ParseException, SchedulerException{
		logger.debug("Iniciando calendarizacion de procesos.  Hora inicial: {}, Hora final: {}", new Object[]{horaInicioMonitoreo, horaFinMonitoreo});
		
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
		Date horaInicio = sdf.parse(horaInicioMonitoreo);
		Date horaFin = sdf.parse(horaFinMonitoreo);
		
		GregorianCalendar now = new GregorianCalendar();
		
		GregorianCalendar timeCalInicio = new GregorianCalendar();
		timeCalInicio.setTime(horaInicio);
		GregorianCalendar timeCalFin = new GregorianCalendar();
		timeCalFin.setTime(horaFin);
		
		GregorianCalendar calInicio = new GregorianCalendar();
		GregorianCalendar calFin = new GregorianCalendar();
		
		
		calInicio.set(Calendar.HOUR_OF_DAY, timeCalInicio.get(Calendar.HOUR_OF_DAY));
		calInicio.set(Calendar.MINUTE, timeCalInicio.get(Calendar.MINUTE));
		
		calFin.set(Calendar.HOUR_OF_DAY, timeCalFin.get(Calendar.HOUR_OF_DAY));
		calFin.set(Calendar.MINUTE, timeCalFin.get(Calendar.MINUTE));
		
		
		 /*Si la hora de inicio es 'mayor' que la hora final probablemente no se trate de un error si no 
		 que se trata de un intervalo que inicia el dia anterior ( 23:00 - 3:00 am )*/
		if(calInicio.get(Calendar.HOUR_OF_DAY) > calFin.get(Calendar.HOUR_OF_DAY)){
			calFin.add(Calendar.DATE, 1);
		}
		
		Date mainMonitorStartTime = null;
		Date stopMonitorStartTime = calFin.getTime();
		
		/* Ahora estoy SEGURO que debo tener un intervalo con la fecha actual
		 Por lo tanto puedo comparar seguramente con la fecha actual del sistema
		 para obtener la fecha y hora de inicio del proceso*/
		if( now.after(calInicio) && now.before(calFin)){
			 /*Estamos dentro del intervalo y debemos retornar inmediatamente la hora actual*/
			mainMonitorStartTime = now.getTime();
			logger.debug("La ejecucion debera iniciarse en este momento, ya que la hora actual ({}) del sistema "
						+" esta dentro del intervalo de tiempo programado [{} a {}] ", new Object[]{sdf.format(mainMonitorStartTime), horaInicioMonitoreo, horaFinMonitoreo });
		}else{
			/* Estamos FUERA del intervalo asi que la hora inicio
			 sera la hora dada por calInicio si estamos antes del siguiente intervalo o 
			 hasta un dia despues si estamso despues del intervalo*/
			if(now.after(calFin)){
				calInicio.add(Calendar.DATE, 1);
				calFin.add(Calendar.DATE, 1);
			}
			 
			mainMonitorStartTime = calInicio.getTime();
			stopMonitorStartTime = calFin.getTime();
			logger.debug("La hora de inicio sera la hora programada inicial del intervalo: " + sdf.format(mainMonitorStartTime));
		}
		
		SimpleDateFormat format = new SimpleDateFormat("dd/MMM/yyyy HH:mm");
		JobDetailImpl mainJobDetail = new JobDetailImpl();
		mainJobDetail.setName(Vars.MAIN_MONITOR_JOB_NAME);
		mainJobDetail.setJobClass(MainMonitor.class);
		SimpleTriggerImpl mainJobTrigger = new SimpleTriggerImpl();
		mainJobTrigger.setName(Vars.MAIN_MONITOR_TRIGGER_NAME);
		mainJobTrigger.setStartTime(mainMonitorStartTime);
		mainJobTrigger.setEndTime(stopMonitorStartTime);
		mainJobTrigger.setRepeatInterval(5 * 60 * 1000l);/* Intervalo de 24 hrs*/
		mainJobTrigger.setRepeatCount(SimpleTrigger.REPEAT_INDEFINITELY);
		
		logger.debug("Calendarizando proceso principal para ejecucion a iniciarse : {}", format.format(mainMonitorStartTime));
		scheduler.scheduleJob(mainJobDetail, mainJobTrigger);
		
	
		
		JobDetailImpl stopJobDetail = new JobDetailImpl();
		stopJobDetail.setName(Vars.MAIN_STOP_JOB_NAME);
		stopJobDetail.setJobClass(StopMonitor.class);
		CronTriggerImpl cronStopTrigger = new CronTriggerImpl();
		cronStopTrigger.setName(Vars.MAIN_STOP_TRIGGER_NAME);
		cronStopTrigger.setCronExpression("0 15 02 * * ?");
	
		logger.debug("====>> La detencion de los procesos se ejecutara en {} ", format.format(stopMonitorStartTime));
		scheduler.scheduleJob(stopJobDetail, cronStopTrigger);
	
		JobDetailImpl drainJobDetail = new JobDetailImpl();
		drainJobDetail.setName(Vars.DRAIN_JOB_NAME);
		drainJobDetail.setJobClass(DrenadoBitacoraProcess.class);
		CronTriggerImpl cronTrigger = new CronTriggerImpl();
		cronTrigger.setName(Vars.DRAIN_TRIGGER_NAME);
		 /*Todos los dias a las 4:15 A.M.*/
    	cronTrigger.setCronExpression("0 15 04 * * ?");
    	logger.debug("Se agrega job de drenado de bitacora todos los dias a las 4:15 a.m.");
    	scheduler.scheduleJob(drainJobDetail, cronTrigger);
		
	}
	
	public void desactivarProceso(int idProceso) throws SQLException, SchedulerException{
		logger.debug("Iniciando desactivacion del proceso: "  + idProceso);
		Connection conn = null;
		try{
			conn = localizadorDeConexion.getConnection();
			QueryRunner runner = new QueryRunner();
			logger.debug("Buscando los datos correspondientes al proceso : " + idProceso);
			Map<String, Object> map = runner.query(conn, "select * from Hth_Proceso_Monitoreo where id_proceso_monitoreo = ?", new MapHandler(), idProceso);
			if(map!=null && !map.isEmpty()){
				String jobName = Vars.JOB_DETAIL_NAME_PREFIX + map.get(Vars.PM_ID_PROCESO_MONITOREO);
				String groupName = Vars.JOB_DETAIL_GROUP_PREFIX + map.get(Vars.PM_CLIENTE);
				
				
				boolean result = scheduler.deleteJob(new JobKey(jobName, groupName));
				
				
				if(result){
					logger.info("El proceso fue encontrado y eliminado del calendarizador de procesos de Quartz...");
				}else{
					logger.warn("No se encontro el proceso: " + jobName + " en el grupo " + groupName);
				}	
			}
		}finally{
			DbUtils.closeQuietly(conn);
		}
	}
	
	
	public synchronized void loadSystemParams() throws Exception{
		Connection conn = null;
		QueryRunner queryRunner = new QueryRunner();
		try{
			conn = localizadorDeConexion.getConnection();
			if(systemParams!=null){
				systemParams.clear();
			}
		
			
			systemParams = queryRunner.query(conn, "SELECT * FROM HTH_PARAMETRO_SISTEMA", new MapHandler());
			
			Map<String, Object> genParams = queryRunner.query(conn, "SELECT NOMBRE_PARAMETRO, VALOR_PARAMETRO FROM HTH_PARAMETRO_GENERAL", new ResultSetHandler<Map<String, Object>>(){

				public Map<String, Object> handle(ResultSet rs)
						throws SQLException {
					Map<String, Object> map = new HashMap<String, Object>();
					while(rs.next()){
						map.put(rs.getString("NOMBRE_PARAMETRO"), rs.getString("VALOR_PARAMETRO"));
					}
					return map;
				}
			});
			
			systemParams.putAll(genParams);
		}catch(SQLException e){
			logger.error("Error en consulta de parametros de sistema con la base de datos. " + e.getMessage(), e);
		}finally{
			DbUtils.closeQuietly(conn);
		}
	}

	public String getStringParam(String key){
		if(!systemParams.containsKey(key)) return null;
		return (String) systemParams.get(key);
	}
	
	public int getParamAsInt(String key){
		String param = getStringParam(key);
		if(param!=null){
			return Integer.parseInt(param);
		}
		return -1;
	}
	
	public FileTransferHelper createFileTransferHelper(){
		
		FileTransferHelper helper = new FileTransferHelper();
		String host = getStringParam(Vars.SFTP_HOST);
		if(StringUtils.isNotEmpty(host)){
			host = Cifrador.desencriptar(host);
		}
		
		String strPort = Cifrador.desencriptar(getStringParam(Vars.SFTP_PORT));
		int port = 22;
		if(StringUtils.isNotEmpty(strPort)){
			port = Integer.parseInt(strPort);
		}
		
		
		String user = getStringParam(Vars.SFTP_USERNAME);
		if(StringUtils.isNotEmpty(user)){
			user = Cifrador.desencriptar(user);
		}
		
		String pwd = getStringParam(Vars.SFTP_PASSWORD);
		if(StringUtils.isNotEmpty(pwd)){
			pwd = Cifrador.desencriptar(pwd);
		}
		
		String key = getStringParam(Vars.SFTP_KEYFILE);
		if(StringUtils.isNotEmpty(key)){
			key = Cifrador.desencriptar(key);
		}
		
		logger.debug("Creando utileria para la transferencia de archivos ");
		logger.debug("Conectandose a: [{}@{}:{}]", new Object[]{ user, host, port});
		
		helper.setHost(host);
		helper.setPort(port);
		helper.setUser(user);
		helper.setPwd(pwd);
		helper.setSshKey(key);
		
		return helper;
	}
	
	public FileTransferHelper createFileTransferHelper(JobDataMap mergedJobDataMap){
		logger.debug("createFileTransferHelper por proceso id: " + 
		mergedJobDataMap.getInt(Vars.PM_ID_PROCESO_MONITOREO) + " " + 
				mergedJobDataMap.getString(Vars.PM_SFTP_PORT) + " " +
				mergedJobDataMap.getString(Vars.PM_SFTP_USERNAME) + " " + 
				mergedJobDataMap.getString(Vars.PM_SFTP_HOST));

		FileTransferHelper helper = new FileTransferHelper();
		
		String host = getStringParam(Vars.SFTP_HOST);
		if(StringUtils.isNotEmpty(host)){
			host = Cifrador.desencriptar(host);
		}
		
		String strPort = Cifrador.desencriptar(getStringParam(Vars.SFTP_PORT));
		int port = 22;
		if(StringUtils.isNotEmpty(strPort)){
			port = Integer.parseInt(strPort);
		}
		
		
		String user = getStringParam(Vars.SFTP_USERNAME);
		if(StringUtils.isNotEmpty(user)){
			user = Cifrador.desencriptar(user);
		}
		
		String pwd = getStringParam(Vars.SFTP_PASSWORD);
		if(StringUtils.isNotEmpty(pwd)){
			pwd = Cifrador.desencriptar(pwd);
		}
		
		String key = getStringParam(Vars.SFTP_KEYFILE);
		if(StringUtils.isNotEmpty(key)){
			key = Cifrador.desencriptar(key);
		}
		
		/*Datos Sftp del proceso monitor*/
		String pmHost = mergedJobDataMap.getString(Vars.PM_SFTP_HOST);
		/*if(StringUtils.isNotEmpty(host)){
			host = Cifrador.desencriptar(host);
		}*/
		
		String pmStrPort = Cifrador.desencriptar(mergedJobDataMap.getString(Vars.PM_SFTP_PORT));
		int pmPort = 22;
		if(StringUtils.isNotEmpty(pmStrPort)){
			pmPort = Integer.parseInt(pmStrPort);
		}
		
		
		String pmUser = mergedJobDataMap.getString(Vars.PM_SFTP_USERNAME);
		/*if(StringUtils.isNotEmpty(user)){
			user = Cifrador.desencriptar(user);
		}*/
		
		String pmPwd = mergedJobDataMap.getString(Vars.PM_SFTP_PASSWORD);
		/*if(StringUtils.isNotEmpty(pwd)){
			pwd = Cifrador.desencriptar(pwd);
		}*/
		
		String pmKey = mergedJobDataMap.getString(Vars.PM_SFTP_KEYFILE);
		/*if(StringUtils.isNotEmpty(key)){
			key = Cifrador.desencriptar(key);
		}*/
		
		
		String destinationLocal = getStringParam(Vars.DESTINO_LOCAL);
		
		logger.debug("Creando utileria para la transferencia de archivos ");
		logger.debug("Conectandose a: [{}@{}:{}] " + mergedJobDataMap.getInt(Vars.PM_ID_PROCESO_MONITOREO) , new Object[]{ pmUser, pmHost, pmPort});
		
		helper.setHost(host);
		helper.setPort(port);
		helper.setUser(user);
		helper.setPwd(pwd);
		helper.setSshKey(key);
		helper.setPmHost(pmHost);
		helper.setPmPort(pmPort);
		helper.setPmUser(pmUser);
		helper.setPmPwd(pmPwd);
		helper.setPmSshKey(pmKey);
		helper.setDestinationLocal(destinationLocal);
		
		
		return helper;
	}
	
	
}
