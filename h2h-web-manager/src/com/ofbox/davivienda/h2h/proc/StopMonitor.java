package com.ofbox.davivienda.h2h.proc;

import java.util.List;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.impl.matchers.GroupMatcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Job encargado de detener todos los jobs de ejecucion
 * 
 * @author engellt
 *
 */
public class StopMonitor implements Job {

	Logger logger = LoggerFactory.getLogger(StopMonitor.class);

	public void execute(JobExecutionContext context) throws JobExecutionException {
		logger.info("Se inicia el proceso para detener los procesos de monitoreo calendarizados...");
		Scheduler scheduler = context.getScheduler();
		PoolProcessManager pm = PoolProcessManager.getInstance();
		try {
			List<String> jobGroupNames = scheduler.getJobGroupNames();
			for (String group : jobGroupNames) {
				for (JobKey jobKey : scheduler.getJobKeys(GroupMatcher.jobGroupEquals(group))) {
					String jobName = jobKey.getName();

					logger.debug("Deteniendo job [{} - {}]", new Object[] {jobName, group });
					scheduler.deleteJob(new JobKey(jobName, group));
				}
			}
			pm.initialize();

			logger.info("Se han detenido todos los procesos calendarizados...");
		} catch (Exception e) {
			logger.error("Error al detener los procesos calendarizados. "+ e.getMessage(), e);
		}
	}

}
