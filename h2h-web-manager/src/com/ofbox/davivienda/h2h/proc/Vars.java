package com.ofbox.davivienda.h2h.proc;

/**
 * 
 */
public class Vars {

	public static final String DEFAULT_DATE_FORMAT = "yyyyMMdd";
	public static final String DEFAULT_USER = "Davivienda";

	/* Variables de los jobs en ejecucion */
	
	public static final String MAIN_MONITOR_JOB_NAME = "MAIN_MONITOR";
	public static final String MAIN_MONITOR_TRIGGER_NAME = "MAIN_TRIGGER";
	public static final String MAIN_STOP_JOB_NAME = "STOP_MONITOR";
	public static final String MAIN_STOP_TRIGGER_NAME = "STOP_TRIGGER";
	
	public static final String DRAIN_JOB_NAME = "DRAIN_JOB";
	public static final String DRAIN_TRIGGER_NAME = "DRAIN_TRIGGER";
	
	/* Variables de parametros */
	public static final String SFTP_HOST = 		"host_sftp";
	public static final String SFTP_PORT = 		"puerto_sftp";
	public static final String SFTP_USERNAME = 	"usuario_sftp";
	public static final String SFTP_PASSWORD = 	"password_sftp";
	public static final String SFTP_KEYFILE  = 	"llave_sftp";
	
	/* Variables que se obtienen del query  */
	public static final String PM_ID_PROCESO_MONITOREO = "id_proceso_monitoreo";
	public static final String PM_CLIENTE = "Cliente";
	public static final String PM_CONVENIO = "Convenio";
	public static final String PM_EMPRESAODEPTO = "EmpresaODepto";
	public static final String PM_TIPOCONVENIO = "TipoConvenio";
	public static final String PM_NOMBRE_ARCHIVO = "nombre_archivo";
	public static final String PM_RUTA_SFTP = "ruta_sftp";
	public static final String PM_RUTA_ARCHIVOS_IN = "ruta_archivos_in";
	public static final String PM_RUTA_ARCHIVOS_OUT = "ruta_archivos_out";
	public static final String PM_RUTA_PROCESADOS = "ruta_procesados";
	public static final String PM_TIEMPO_MONITOREO = "tiempo_monitoreo";
	public static final String PM_LINEA_ENCABEZADO = "linea_encabezado";
	public static final String PM_ID_FORMATO_ENCABEZADO = "id_formato_encabezado";
	public static final String PM_PROCESAMIENTO_SUPERUSUARIO = "procesamiento_superusuario";
	public static final String PM_ID_FORMATO_DETALLE = "id_formato_detalle";
	public static final String PM_ID_INSTALACION = "id_instalacion";
	public static final String PM_MONTO_MAXIMO_PROCESAR = "monto_maximo_procesar";
	public static final String PM_NOMBRE_CLIENTE = "Nombre_Cliente";
	public static final String PM_CONVENIO_PEACH = "convenio_PE_ACH";
	public static final String PM_CONVENIO_NOMBRE = "convenio_nombre";
	public static final String PM_EMAIL_CLIENTE = "email_cliente";
	public static final String PM_SFTP_HOST = 		"pm_host_sftp";
	public static final String PM_SFTP_PORT = 		"pm_puerto_sftp";
	public static final String PM_SFTP_USERNAME = 	"pm_usuario_sftp";
	public static final String PM_SFTP_PASSWORD = 	"pm_password_sftp";
	public static final String PM_SFTP_KEYFILE  = 	"pm_llave_sftp";
	
	/* Variables parametros sistema */
	public static final String PARAMETRO_SISTEMA_TIEMPO_MONITOREO_DEFAULT = "tiempo_monitoreo_default";
	public static final String PARAMETRO_SISTEMA_HORA_INICIO_MONITOREO = "hora_inicio_monitoreo";
	public static final String PARAMETRO_SISTEMA_HORA_FIN_MONITOREO = "hora_fin_monitoreo";
	public static final String PARAMETRO_SISTEMA_CARPETA_LECTURA_DEFAULT = "carpeta_lectura_default";
	public static final String PARAMETRO_SISTEMA_CARPETA_ESCRITURA_DEFAULT = "carpeta_escritura_default";
	public static final String PARAMETRO_SISTEMA_RUTA_RAIZ = "ruta_raiz";
	public static final String PARAMETRO_SISTEMA_CAR_SEPARADOR_DEFAULT = "car_separador_default";
	public static final String PARAMETRO_SISTEMA_HABILITAR_ENVIO_CORREO = "habilitar_envio_correo";
	public static final String PARAMETRO_SISTEMA_REMITENTE_ENVIO_CORREO = "remitente_envio_correo";
	public static final String PARAMETRO_SISTEMA_ASUNTO_ENVIO_CORREO = "asunto_envio_correo";
	public static final String PARAMETRO_SISTEMA_JAVAMAIL_JNDI = "javamail_jndi"; 
	public static final String PARAMETRO_SISTEMA_LISTA_ENVIO_CORREO = "lista_envio_correo";
	public static final String PARAMETRO_SISTEMA_DIRECTORIO_LOCAL_DESTINO = "directorio_local_destino";
	
	/* Variables de ejecucion del proceso*/
	public static final String JOB_EXEC_FILE_SIZE = "FILE_SIZE";
	public static final String JOB_EXEC_STATE = 	"JOB_STATE";
	public static final String JOB_EXEC_LOCAL_FILE = "LOCAL_FILE";
	
	
	
	/* Variables utilizadas con Quartz */
	public static final String JOB_DETAIL_NAME_PREFIX = 	"JOB-";
	public static final String JOB_DETAIL_GROUP_PREFIX=		"C-";
	public static final String TRIGGER_NAME_PREFIX = 		"TRIGGER-";
	public static final String TRIGGER_GROUP_PREFIX = 		"TG-";
	
	
	/* Variables parametros generales */
	public static final String DESTINO_LOCAL = 	"destino_local";
	
	
	
	
	
}
