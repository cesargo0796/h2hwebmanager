package com.ofbox.davivienda.h2h.proc.file.format;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ofbox.davivienda.h2h.proc.LogHandlerContainer;
import com.ofbox.davivienda.h2h.proc.ProcessLogHandler;
import com.ofbox.davivienda.h2h.proc.Vars;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.util.jdbc.LocalizadorDeConexion;

public class BusinessData {

	Logger logger = LoggerFactory.getLogger(BusinessData.class);
	
	private LocalizadorDeConexion localizadorDeConexion;
	
	private Map<String, Object> parameterMap;

	private EncabezadoData encabezadoData;
	
	private String referenceInfo;
	
	private String resultadoMensaje;
	
	private int lineaEnArchivo = 0;
	
	private ProcessLogHandler handler;
	
	private Boolean archivoConEncabezado = Boolean.FALSE;
	
	public static final String SQL_SELECT_FORMATO = "select f.nombre_formato, f.tipo_formato, f.tipo_separador, f.caracter_separador " + 
			" from HTH_Formato f " + 
			" where id_formato = ?" ;
	
	public static final String SQL_SELECT_ENCABEZADO = "select af.posicion, af.posicion_ini, af.posicion_fin, af.tipo_dato_atributo, " + 
			"       af.formato_atributo, af.opcional, e.nombre_atributo, e.descripcion, e.columna, e.tabla" + 
			" from   HTH_Atributo_Formato af, Hth_Atributo_Encabezado e" + 
			" where af.id_atributo_encabezado = e.id_atributo_encabezado " + 
			" and af.id_formato = ?";
	
	public static final String SQL_SELECT_DETALLE = "select af.posicion, af.posicion_ini, af.posicion_fin, af.tipo_dato_atributo, " + 
			"       af.formato_atributo, af.opcional, d.nombre_atributo, d.descripcion, d.columna, d.tabla" + 
			" from   HTH_Atributo_Formato af, HTH_Atributo_Detalle d" + 
			" where af.id_atributo_detalle = d.id_atributo_detalle " + 
			" and af.id_formato = ?";
	
	public BusinessData(LocalizadorDeConexion localizadorDeConexion, Map<String, Object> parameterMap) {
		super();
		this.localizadorDeConexion = localizadorDeConexion;
		this.parameterMap = parameterMap;
		this.encabezadoData = new EncabezadoData();
		
		referenceInfo = " Cliente [" + getInt(Vars.PM_CLIENTE) + "] " + getString(Vars.PM_NOMBRE_CLIENTE) +
					    ", Convenio [" + getInt(Vars.PM_CONVENIO) + "] " + getString(Vars.PM_CONVENIO_NOMBRE) +
					    ", Proceso [" + getInt(Vars.PM_ID_PROCESO_MONITOREO) + "] ";
		encabezadoData.setMontoTotal(new BigDecimal(0));
		encabezadoData.setCantidadOperaciones(new Integer(0));
	}

	public ProcessLogHandler getHandler() {
		if(handler == null){
			Integer idProceso = getInt(Vars.PM_ID_PROCESO_MONITOREO);
			if(LogHandlerContainer.getSingleton().logHanderExists(idProceso)){
				handler = LogHandlerContainer.getSingleton().getLogHandler(idProceso);
			}else{
				handler = LogHandlerContainer.getSingleton().createLogHandler(idProceso);
			}
		}
		return handler;
	}
	public void setHandler(ProcessLogHandler handler) {
		this.handler = handler;
	}

	public int getLineaEnArchivo() {
		return lineaEnArchivo;
	}



	public void setLineaEnArchivo(int lineaEnArchivo) {
		this.lineaEnArchivo = lineaEnArchivo;
	}



	public String getResultadoMensaje() {
		return resultadoMensaje;
	}



	public void setResultadoMensaje(String resultadoMensaje) {
		this.resultadoMensaje = resultadoMensaje;
	}



	public EncabezadoData getEncabezadoData(){
		return encabezadoData;
	}

	public LocalizadorDeConexion getLocalizadorDeConexion() {
		return localizadorDeConexion;
	}

	public Map<String, Object> getParameterMap() {
		return parameterMap;
	}

	public int getInt(String key){
		if(parameterMapAndKeyExists(key)){
			Number num = (Number) parameterMap.get(key);
			if(num == null){ 
				return 0;
			}
			return num.intValue();
		}
		return 0;
	}
	
	public String getString(String key){
		if(parameterMapAndKeyExists(key)){
			String retVal = (String) parameterMap.get(key);
			return retVal;
		}
		return null;
	}

	public BigDecimal getAsBigDecimal(String key){
		if(parameterMapAndKeyExists(key)){
			Object obj = (Number)parameterMap.get(key);
			if(obj instanceof BigDecimal){
				return (BigDecimal)obj;
			}else if(obj instanceof String){
				return new BigDecimal((String)obj);
			}else if(obj instanceof Number){
				return new BigDecimal(((Number)obj).doubleValue());
			}
		}
		return null;
	}

	
	private boolean parameterMapAndKeyExists(String key){
		return parameterMap!=null && !parameterMap.isEmpty() && parameterMap.containsKey(key);
	}
	
	public boolean isPoseeEncabezado(){
		boolean poseeEncabezado = false;
		String encabezado = getString(Vars.PM_LINEA_ENCABEZADO);
		poseeEncabezado = "S".equalsIgnoreCase(encabezado);
		return poseeEncabezado;
	}
	
	public String getReferenceInfo(){
		return referenceInfo;
	}

	public FormatoLinea cargarFormatoEncabezado(){
		try{
	    	int idFormatoEncabezado = getInt(Vars.PM_ID_FORMATO_ENCABEZADO);
			if(idFormatoEncabezado > 0){
				return buscarFormato(idFormatoEncabezado);
			}
		}catch(SQLException e){
			e.printStackTrace();
			int idProceso = getInt(Vars.PM_ID_PROCESO_MONITOREO);
			logger.error("Error al cargar formato de Encabezado para el Proceso con ID: " + idProceso, e);
		}
		return null;
	}
	
	public FormatoLinea cargarFormatoDetalle(){
		int idProceso = getInt(Vars.PM_ID_PROCESO_MONITOREO);
		try{
			int idFormatoDetalle = getInt(Vars.PM_ID_FORMATO_DETALLE);
			if(idFormatoDetalle > 0){
				return buscarFormato(idFormatoDetalle);
			}
			logger.warn("El proceso [{}] no tiene un formato de Detalle definido!.  Esto impide que se pueda procesar el archivo correctamente!!", idProceso);
		}catch(SQLException e){
			e.printStackTrace();
			logger.error("Error al cargar formato de Detalle para el Proceso con ID: " + idProceso, e);
		}
		return null;
	}
	
	private FormatoLinea buscarFormato(int idFormato) throws SQLException{
		Connection conn = null;
		try{
			conn = localizadorDeConexion.getConnection();
			QueryRunner runner = new QueryRunner();
			
			FormatoLinea formato =
					runner.query(conn, SQL_SELECT_FORMATO, new ResultSetHandler<FormatoLinea>(){
	
						public FormatoLinea handle(ResultSet rs)
								throws SQLException {
							if(rs.next()){
								FormatoLinea formato = new FormatoLinea();
								formato.setNombreFormato(rs.getString("nombre_formato"));
								formato.setTipoFormato(rs.getString("tipo_formato"));
								formato.setTipoSeparador(rs.getString("tipo_separador"));
								formato.setCaracterSeparador(rs.getString("caracter_separador"));
								return formato;
							}
							return null;
						}
					}, idFormato);
			
			if(formato!=null){
				logger.debug("Se encontro el formato : [{}] ", formato.getNombreFormato());
				String sql = formato.isFormatoEncabezado()?SQL_SELECT_ENCABEZADO:SQL_SELECT_DETALLE;
				
				logger.debug("Buscando los valores de los campos definidos.  Ejecutando [[{}]]", sql);
				List<CampoLinea> campos = runner.query(conn, sql, new ResultSetHandler<List<CampoLinea>>(){
	
					public List<CampoLinea> handle(ResultSet rs) throws SQLException {
						List<CampoLinea> campos = new LinkedList<CampoLinea>();
						while(rs.next()){
							CampoLinea c = new CampoLinea();
							c.setColumnaDestino(rs.getString("columna"));
							c.setFormatoAtributo(rs.getString("formato_atributo"));
							c.setNombre(rs.getString("nombre_atributo"));
							c.setPosicionCampo(rs.getInt("posicion"));
							c.setPosicionFin(rs.getInt("posicion_fin"));
							c.setPosicionInicio(rs.getInt("posicion_ini"));
							c.setTipoDatoAtributo(rs.getString("tipo_dato_atributo"));
							c.setTablaDestino(rs.getString("tabla"));
							c.setOpcional(rs.getString("opcional"));
							campos.add(c);
						}
						
						return campos;
					}
					
				}, idFormato);
				
				logger.debug("Se encontraron [{}] registros de campos para el formato [{}]", campos.size(), formato.getNombreFormato());
				
				formato.setCampos(campos);
				 /*Si traemos al menos un registro*/
				if(!campos.isEmpty()){
					 /*Se supone que para un unico formato deberá de ser una sola tabla de destino*/
					formato.setTablaDestino(campos.get(0).getTablaDestino());
				}
	
			}else{
				logger.warn("No se encontro el formato definido con el ID [{}] en la tabla HTH_Formato", idFormato);
			}		
			return formato;
		}finally{
			DbUtils.closeQuietly(conn);
		}
	}

	public Boolean getArchivoConEncabezado() {
		return archivoConEncabezado;
	}

	public void setArchivoConEncabezado(Boolean archivoConEncabezado) {
		this.archivoConEncabezado = archivoConEncabezado;
	}

	
}
