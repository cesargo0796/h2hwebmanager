package com.ofbox.davivienda.h2h.proc.file;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import com.ofbox.davivienda.h2h.abstraccion.dto.HthBitacoraProceso;
import com.ofbox.davivienda.h2h.proc.ProcessLogHandler;
import com.ofbox.davivienda.h2h.proc.Vars;
import com.ofbox.davivienda.h2h.proc.file.format.BusinessData;
import com.ofbox.davivienda.h2h.proc.file.format.DetalleACHLineProcessor;
import com.ofbox.davivienda.h2h.proc.file.format.DetallePagoElectronicoLineProcessor;
import com.ofbox.davivienda.h2h.proc.file.format.FormatoLinea;
import com.ofbox.davivienda.h2h.proc.file.format.LineProcessException;
import com.ofbox.davivienda.h2h.proc.file.format.LoteACHLineProcessor;
import com.ofbox.davivienda.h2h.proc.file.format.LoteACHPostProcessor;
import com.ofbox.davivienda.h2h.proc.file.format.LoteACHPreProcessor;
import com.ofbox.davivienda.h2h.proc.file.format.LotePagoElectronicoLineProcessor;
import com.ofbox.davivienda.h2h.proc.file.format.LotePagoElectronicoPostProcessor;
import com.ofbox.davivienda.h2h.proc.file.format.LotePagoElectronicoPreProcessor;
import com.ofbox.davivienda.h2h.proc.file.format.ProcessException;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.util.jdbc.LocalizadorDeConexion;

public class FileProcessor {

	Logger logger = LoggerFactory.getLogger(FileProcessor.class);
	
	Map<String, Object> parameterMap;
	
	ProcessLogHandler handler = null;
	
	private LocalizadorDeConexion localizadorDeConexion;
	private static final String LINE_BREAK = "\r\n";
	private static final String LINE_SEPARATOR = ",";
	StringBuilder resultadoPE = new StringBuilder();
	StringBuilder resultadoACH = new StringBuilder();
	
	private String resultMessage;
	
	public FileProcessor(Map<String, Object> parameterMap, LocalizadorDeConexion localizadorDeConexion) {
		super();
		this.parameterMap = parameterMap;
		this.localizadorDeConexion = localizadorDeConexion;
	}

	public String getResultMessage() {
		return resultMessage;
	}

	public void setResultMessage(String resultMessage) {
		this.resultMessage = resultMessage;
	}

	public void process(File file) {
		String filePath = file.getAbsolutePath();
		String filename = file.getName();
	
		MDC.put("ff", filename);
		logger.info("Iniciando procesamiento de archivo : " + filePath);
		
		BusinessData businessData = new BusinessData(localizadorDeConexion, parameterMap);
		businessData.getEncabezadoData().setFileName(filename);
		handler = businessData.getHandler();
		handler.log("Iniciando el procesamiento del archivo en ruta: " + filePath);
		try{
			String convenioPEACH = businessData.getString(Vars.PM_CONVENIO_PEACH);
			if("PE".equals(convenioPEACH)){
				logger.warn("Iniciando procesamiento de archivo segun convenio de Pago Electronico.  Si el archivo {} NO es de Convenio PE no se procesara correctamente.", filename);
				handler.log("La configuracion del proceso indica archivo de Convenio de Pago Electronio. ");
				procesarArchivoPE(file, businessData);
			}else if("ACH".equals(convenioPEACH)){
				logger.warn("Iniciando procesamiento de archivo segun convenio ACH. Si el archivo {} NO es de Convenio ACH no se procesara correctamente. ", filename);
				handler.log("La configuracion del proceso indica archivo de Convenio ACH. ");
				procesarArchivoACH(file, businessData);
			}else if("PS".equals(convenioPEACH)){
				logger.warn("Iniciando procesamiento de archivo segun convenio de Pago de servicio. Si el archivo {} NO es de Convenio PS no se procesara correctamente. ", filename);
				handler.log("La configuracion del proceso indica archivo de Convenio PS. ");
				procesarArchivoPs(file, businessData);
			}else if("TEXT".equals(convenioPEACH)){
				logger.warn("Iniciando procesamiento de archivo segun convenio de Transferencias al Exterior. Si el archivo {} NO es de Convenio TEXT no se procesara correctamente. ", filename);
				handler.log("La configuracion del proceso indica archivo de Convenio TEXT. ");
				procesarArchivoText(file, businessData);
			}else{
				logger.error("ERROR !!! Formato de Convenio PE - ACH no esta definido correctamente. {}. _Referencia: {}. Archivo: {}", new Object[]{convenioPEACH, businessData.getReferenceInfo(), filename});
				handler.log("ERROR: Formato de convenio PE o ACH no esta definido correctamente. ");
				throw new ProcessException("Fallo en configuracion de Proceso.  El registro no define configuracion de Convenio PE o ACH.  Referencia: " + businessData.getReferenceInfo());
			}
			
			/*kpalacios 13/10/2016
			Construyendo respuesta para documento de salida. */
			StringBuilder resultado = new StringBuilder();
			String resultadoProceso = handler.getResultado().trim().equals(HthBitacoraProceso.resultado_proceso_ADVERTENCIA)?"ADVERTENCIA":
									  handler.getResultado().trim().equals(HthBitacoraProceso.resultado_proceso_ERROR)?"ERROR":
									  handler.getResultado().trim().equals(HthBitacoraProceso.resultado_proceso_OK)?"OK":"";
			resultado.append(resultadoProceso).append(LINE_SEPARATOR).append(handler.getDescripcionResultado()).append(LINE_BREAK);
			resultado.append(resultadoACH);
			resultado.append(resultadoPE);
			
			setResultMessage(resultado.toString());
			
		}catch(Exception e){
			e.printStackTrace();
			logger.error("Error en procesamiento del archivo. " + e.getMessage(), e);
			handler.log("ERROR - " + e.getMessage());
			handler.setLastException(e);
			handler.setResultado("E");
			handler.setDescripcionResultado("El archivo no pudo ser procesado total o parcialmente");
			setResultMessage("ERROR, El archivo no pudo ser procesado total o parcialmente. " + e.getMessage());
		}finally{
			MDC.remove("ff");
		}
		
	}


	private void procesarArchivoACH(File file, BusinessData businessData) throws IOException, ProcessException{
		logger.info("Iniciando procesamiento de archivo ACH");
		
		LoteACHPreProcessor preProcessor = new LoteACHPreProcessor(businessData, null);
		preProcessor.process(null);
		
		logger.debug("Validaciones iniciales de archivo para convenio ACH finalizadas.  Iniciando con lectura de los datos del archivo.");
		BufferedReader reader = new BufferedReader(new FileReader(file));
		String linea = null;
		boolean primeraLinea = true;
		
		FormatoLinea formatoEncabezado = businessData.cargarFormatoEncabezado();
		FormatoLinea formatoDetalle = businessData.cargarFormatoDetalle();
		
		LoteACHLineProcessor lote = new LoteACHLineProcessor(businessData, formatoEncabezado);
		DetalleACHLineProcessor detalle = new DetalleACHLineProcessor(businessData, formatoDetalle);
		LoteACHPostProcessor postProcessor = new LoteACHPostProcessor(businessData, null);
		
		handler.log("Iniciando procesamiento de las lineas del archivo");
		int cantLineas=0;
		int cantProcesadas=0;
		int cantOperaciones=0;
		businessData.setArchivoConEncabezado(Boolean.FALSE);
		String lineaEncabezado = businessData.getString(Vars.PM_LINEA_ENCABEZADO);
		 /*Esto determinara si el formato define primera linea del archivo como encabezado*/
		primeraLinea = "S".equals(lineaEncabezado);
		 /*El procesador de encabezado recibe null para que tome valores por defecto*/
		if(!primeraLinea){
			handler.log("La configuracion del proceso indica que el archivo no posee linea de encabezado.");
			lote.process(null);
			handler.log("Se han registrado los valores por defecto en el registro de encabezado del Lote");
		}
		
		while( (linea = reader.readLine()) != null ){
			
			businessData.setLineaEnArchivo(++cantLineas);
			/*Despues procesar el archivo segun formato definido*/
			if(primeraLinea){
				handler.log("Procesando la primera linea del archivo");
				try{
					lote.process(linea);
					businessData.setArchivoConEncabezado(Boolean.TRUE);
					handler.log("Procesada la primera linea del archivo.");
				}catch(LineProcessException e){
					e.printStackTrace();
					logger.error("Error en ingreso de encabezado de lote. " + e.getMessage(), e);
					
					handler.log("Ha ocurrido un error procesando el encabezado del lote. " + e.getMessage());
					handler.setLastException(e);
					throw new ProcessException("Error en ingreso de encabezado de Lote. ", e);
				}
				primeraLinea = false;
			}else{
				logger.debug("Procesando linea {} de detalle del archivo", cantLineas);
				
				try {
					cantOperaciones++;
					System.out.println("linea linea linea  "+linea);
					detalle.process(linea);
					cantProcesadas++;
					/*kpalacios | 13/10/2016 | Asignando valores para enviarlo al documento de salida*/
					resultadoACH.append(linea).append(",Linea procesada").append(",0").append(LINE_BREAK);					
				} catch (LineProcessException e) {
					e.printStackTrace();
					logger.error("Error en procesamiento de la linea: [{}]-[{}]", cantLineas, linea);
					handler.log("ADVERTENCIA ha ocurrido un error en el procesamiento de la linea " + cantLineas + " -> [" + linea + "]");
					handler.setLastException(e);
					/*kpalacios | 13/10/2016 | Asignando valores para enviarlo al documento de salida*/
					resultadoACH.append(linea).append(",Linea NO procesada").append(",9999").append(LINE_BREAK);
				}
			}
		}
		
		handler.log("Finalizado de procesar " + cantLineas + " del archivo.");
		
		handler.log("Detalle de Operaciones: " + cantOperaciones + " operaciones en el archivo. ");
		handler.log("Detalle de Operaciones: " + cantProcesadas +  " lineas se procesaron correctamente. ");
		
		handler.setLineasArchivo(cantLineas);
		handler.setLineasProcesadas(cantProcesadas);
		
		try{
			reader.close();
		}catch(IOException ioe){
			logger.warn("Ocurrio un error de I/O al tratar de cerrar el archivo. " + ioe.getMessage(), ioe);
		}

		logger.debug("Finalizado el procesamiento de las lineas.  Se procede a actualizar el lote con los montos totales procesados y la cantidad de operaciones procesadas");
		handler.log("Finalizado de procesar " + cantLineas + " del archivo.  Se procesaron correctamente " + cantProcesadas + " lineas");
		
		businessData.getEncabezadoData().setCantidadOperaciones(cantOperaciones);
		try{
			handler.log("Procediendo a actualizar montos totales y cantidad de operaciones en registro de encabezado");
					
			if(cantOperaciones != cantProcesadas){
				handler.setResultado("A");
				handler.setDescripcionResultado("Archivo finalizo su proceso, sin embargo no todas las lineas se procesaron correctamente.");
				if (cantProcesadas == 0) {
					handler.setDescripcionResultado("Archivo NO finalizo su proceso, Formato invalido de archivo.");
				}
			}else{
				handler.setResultado("O");
				handler.setDescripcionResultado("Archivo procesado");
			}
			
			postProcessor.process(null);
			
		}catch(LineProcessException e){
			e.printStackTrace();
			logger.error("Error al actualizar valores de Monto Total y Cantidad de Operaciones asi como Estatus de Lote de Pagos ACH", e);
			handler.log("Error inesperado al actualizar cantidad de operaciones y monto total.  Lote ACH. -> " + e.getMessage());
			handler.setLastException(e);
			
			handler.setResultado("E");
			handler.setDescripcionResultado("Archivo procesado.  Ocurrieron errores en actualizacion del encabezado. ");
		}
		
		logger.info("Finalizado procesamiento de Archivo de Convenio para ACH");
	}
	
	private void procesarArchivoPE(File file, BusinessData businessData) throws IOException, ProcessException{
		logger.info("Iniciando procesamiento de archivo de Pago Electronico...");
		
		
		logger.debug("Iniciando validaciones iniciales de Pago Electronico...");
		LotePagoElectronicoPreProcessor preProcessor = new LotePagoElectronicoPreProcessor(businessData, null);
		preProcessor.process(null);
		
		
		BufferedReader reader = new BufferedReader(new FileReader(file));
		BufferedReader readerLineNumber = new BufferedReader(new FileReader(file));
		String linea = null;
		boolean primeraLinea = true;
		
		FormatoLinea formatoEncabezado = businessData.cargarFormatoEncabezado();
		FormatoLinea formatoDetalle = businessData.cargarFormatoDetalle();
		
		
		
		LotePagoElectronicoLineProcessor lote = new LotePagoElectronicoLineProcessor(businessData, formatoEncabezado);
		DetallePagoElectronicoLineProcessor detalle = new DetallePagoElectronicoLineProcessor(businessData, formatoDetalle);
		LotePagoElectronicoPostProcessor lotePostProcessor = new LotePagoElectronicoPostProcessor(businessData, null);
		
		int cantLineas=0;
		int cantProcesadas=0;
		int cantOperaciones=0;
		
		businessData.setArchivoConEncabezado(Boolean.FALSE);
		String lineaEncabezado = businessData.getString(Vars.PM_LINEA_ENCABEZADO);
		primeraLinea = "S".equals(lineaEncabezado);
		if(!primeraLinea){
			handler.log("El archivo no posee linea de encabezado segun configuracion. ");
			lote.process(null);
			handler.log("Encabezado de lote registrado segun parametros por defecto.");
		}
		
		
		while ((linea = readerLineNumber.readLine()) != null) {
			businessData.setLineaEnArchivo(++cantLineas);
		}
		
		while( (linea = reader.readLine()) != null ){
			System.out.println("primeraLinea..... "+primeraLinea);
			
			if(primeraLinea){
				handler.log("El archivo posee linea de encabezado segun configuracion.  Procesando primera linea del archivo.");
				try{
					lote.process(linea);
					businessData.setArchivoConEncabezado(Boolean.TRUE);
					handler.log("Encabezado procesado.");
				}catch(LineProcessException e){
					e.printStackTrace();
					logger.error("Error en ingreso de encabezado de lote. " + e.getMessage(), e);
					handler.log("ERROR - " + e.getMessage());
					throw new ProcessException("Error en ingreso de encabezado de Lote. ", e);
				}
				primeraLinea = false;
			}else{
				logger.debug("Procesando linea {} de detalle del archivo", cantOperaciones);
				
				try {
					cantOperaciones++;
					System.out.println("cantOperacionescantOperaciones1 "+cantOperaciones);
					detalle.process(linea);
					cantProcesadas++;
					System.out.println("cantProcesadascantProcesadas1 "+cantProcesadas);
					/*kpalacios | 13/10/2016 | Asignando valores para enviarlo al documento de salida*/
					resultadoPE.append(linea).append(",Linea procesada").append(",0").append(LINE_BREAK);
				} catch (LineProcessException e) {
					e.printStackTrace();
					logger.error("Error en procesamiento de la linea: [{}]-[{}]", cantOperaciones, linea);
					handler.log("ADVERTENCIA ha ocurrido un error en el procesamiento de la linea " + cantOperaciones + " -> [" + linea + "]");
					handler.setLastException(e);
					/*kpalacios | 13/10/2016 | Asignando valores para enviarlo al documento de salida*/
					resultadoPE.append(linea).append(",Linea NO procesada").append(",9999").append(LINE_BREAK);
				}
			}
		}
		handler.log("Finalizado de procesar " + cantLineas + " del archivo.");
		
		handler.log("Detalle de Operaciones: " + cantOperaciones + " operaciones en el archivo. ");
		handler.log("Detalle de Operaciones: " + cantProcesadas +  " lineas se procesaron correctamente. ");
		
		handler.setLineasArchivo(cantLineas);
		handler.setLineasProcesadas(cantProcesadas);
		
		try{
			reader.close();
		}catch(IOException ioe){
			logger.warn("Ocurrio un error de I/O al tratar de cerrar el archivo. " + ioe.getMessage(), ioe);
		}
		

		logger.debug("Finalizado el procesamiento de las lineas.  Se procede a actualizar el lote con los montos totales procesados y la cantidad de operaciones procesadas");
		handler.log("Finalizado el procesamiento de las lineas.  Actualizando montos totales y cantidad de operaciones");
		businessData.getEncabezadoData().setCantidadOperaciones(cantOperaciones);
		
		try{
			
			if(cantOperaciones != cantProcesadas){
				handler.setResultado("A");
				handler.setDescripcionResultado("Archivo finalizo su proceso, sin embargo no todas las lineas se procesaron correctamente.");
				if (cantProcesadas == 0) {
					handler.setDescripcionResultado("Archivo NO finalizo su proceso, Formato invalido de archivo.");
				}
			}else{
				handler.setResultado("O");
				handler.setDescripcionResultado("Archivo procesado");
			}
			
			lotePostProcessor.process(null);			
		}catch(LineProcessException e){
			e.printStackTrace();
			logger.error("Error al actualizar valores de Monto Total y Cantidad de Operaciones asi como Estatus de Lote. ", e);
			handler.log("Error al actualizar Monto total y Cantidad de operaciones del Lote. " + e.getMessage());
			handler.setLastException(e);
			
			handler.setResultado("E");
			handler.setDescripcionResultado("Archivo procesado.  Ocurrieron errores en actualizacion del encabezado. ");
		}
		
		
		logger.info("Finalizado procesamiento de Archivo de Convenio para Pago Electronico");
	}
	
	private void procesarArchivoPs(File file, BusinessData businessData) throws IOException, ProcessException{
		logger.info("Iniciando procesamiento de archivo de Pago Electronico...");
		
		
		logger.debug("Iniciando validaciones iniciales de Pago Electronico...");
		LotePagoElectronicoPreProcessor preProcessor = new LotePagoElectronicoPreProcessor(businessData, null);
		preProcessor.process(null);
		
		
		BufferedReader reader = new BufferedReader(new FileReader(file));
		BufferedReader readerLineNumber = new BufferedReader(new FileReader(file));
		String linea = null;
		boolean primeraLinea = true;
		
		FormatoLinea formatoEncabezado = businessData.cargarFormatoEncabezado();
		FormatoLinea formatoDetalle = businessData.cargarFormatoDetalle();
		
		
		
		LotePagoElectronicoLineProcessor lote = new LotePagoElectronicoLineProcessor(businessData, formatoEncabezado);
		DetallePagoElectronicoLineProcessor detalle = new DetallePagoElectronicoLineProcessor(businessData, formatoDetalle);
		LotePagoElectronicoPostProcessor lotePostProcessor = new LotePagoElectronicoPostProcessor(businessData, null);
		
		int cantLineas=0;
		int cantProcesadas=0;
		int cantOperaciones=0;
		
		businessData.setArchivoConEncabezado(Boolean.FALSE);
		String lineaEncabezado = businessData.getString(Vars.PM_LINEA_ENCABEZADO);
		primeraLinea = "S".equals(lineaEncabezado);
		if(!primeraLinea){
			handler.log("El archivo no posee linea de encabezado segun configuracion. ");
			lote.process(null);
			handler.log("Encabezado de lote registrado segun parametros por defecto.");
		}
		
		
		while ((linea = readerLineNumber.readLine()) != null) {
			businessData.setLineaEnArchivo(++cantLineas);
		}
		
		while( (linea = reader.readLine()) != null ){
			System.out.println("primeraLinea..... "+primeraLinea);
			
			if(primeraLinea){
				handler.log("El archivo posee linea de encabezado segun configuracion.  Procesando primera linea del archivo.");
				try{
					lote.process(linea);
					businessData.setArchivoConEncabezado(Boolean.TRUE);
					handler.log("Encabezado procesado.");
				}catch(LineProcessException e){
					e.printStackTrace();
					logger.error("Error en ingreso de encabezado de lote. " + e.getMessage(), e);
					handler.log("ERROR - " + e.getMessage());
					throw new ProcessException("Error en ingreso de encabezado de Lote. ", e);
				}
				primeraLinea = false;
			}else{
				logger.debug("Procesando linea {} de detalle del archivo", cantOperaciones);
				
				try {
					cantOperaciones++;
					System.out.println("cantOperacionescantOperaciones1 "+cantOperaciones);
					detalle.process(linea);
					cantProcesadas++;
					System.out.println("cantProcesadascantProcesadas1 "+cantProcesadas);
					/*kpalacios | 13/10/2016 | Asignando valores para enviarlo al documento de salida*/
					resultadoPE.append(linea).append(",Linea procesada").append(",0").append(LINE_BREAK);
				} catch (LineProcessException e) {
					e.printStackTrace();
					logger.error("Error en procesamiento de la linea: [{}]-[{}]", cantOperaciones, linea);
					handler.log("ADVERTENCIA ha ocurrido un error en el procesamiento de la linea " + cantOperaciones + " -> [" + linea + "]");
					handler.setLastException(e);
					/*kpalacios | 13/10/2016 | Asignando valores para enviarlo al documento de salida*/
					resultadoPE.append(linea).append(",Linea NO procesada").append(",9999").append(LINE_BREAK);
				}
			}
		}
		handler.log("Finalizado de procesar " + cantLineas + " del archivo.");
		
		handler.log("Detalle de Operaciones: " + cantOperaciones + " operaciones en el archivo. ");
		handler.log("Detalle de Operaciones: " + cantProcesadas +  " lineas se procesaron correctamente. ");
		
		handler.setLineasArchivo(cantLineas);
		handler.setLineasProcesadas(cantProcesadas);
		
		try{
			reader.close();
		}catch(IOException ioe){
			logger.warn("Ocurrio un error de I/O al tratar de cerrar el archivo. " + ioe.getMessage(), ioe);
		}
		

		logger.debug("Finalizado el procesamiento de las lineas.  Se procede a actualizar el lote con los montos totales procesados y la cantidad de operaciones procesadas");
		handler.log("Finalizado el procesamiento de las lineas.  Actualizando montos totales y cantidad de operaciones");
		businessData.getEncabezadoData().setCantidadOperaciones(cantOperaciones);
		
		try{
			
			if(cantOperaciones != cantProcesadas){
				handler.setResultado("A");
				handler.setDescripcionResultado("Archivo finalizo su proceso, sin embargo no todas las lineas se procesaron correctamente.");
				if (cantProcesadas == 0) {
					handler.setDescripcionResultado("Archivo NO finalizo su proceso, Formato invalido de archivo.");
				}
			}else{
				handler.setResultado("O");
				handler.setDescripcionResultado("Archivo procesado");
			}
			
			lotePostProcessor.process(null);			
		}catch(LineProcessException e){
			e.printStackTrace();
			logger.error("Error al actualizar valores de Monto Total y Cantidad de Operaciones asi como Estatus de Lote. ", e);
			handler.log("Error al actualizar Monto total y Cantidad de operaciones del Lote. " + e.getMessage());
			handler.setLastException(e);
			
			handler.setResultado("E");
			handler.setDescripcionResultado("Archivo procesado.  Ocurrieron errores en actualizacion del encabezado. ");
		}
		
		
		logger.info("Finalizado procesamiento de Archivo de Convenio para Pago Electronico");
	}
	
	private void procesarArchivoText(File file, BusinessData businessData) throws IOException, ProcessException{
		logger.info("Iniciando procesamiento de archivo de Pago Electronico...");
		
		
		logger.debug("Iniciando validaciones iniciales de Pago Electronico...");
		LotePagoElectronicoPreProcessor preProcessor = new LotePagoElectronicoPreProcessor(businessData, null);
		preProcessor.process(null);
		
		
		BufferedReader reader = new BufferedReader(new FileReader(file));
		BufferedReader readerLineNumber = new BufferedReader(new FileReader(file));
		String linea = null;
		boolean primeraLinea = true;
		
		FormatoLinea formatoEncabezado = businessData.cargarFormatoEncabezado();
		FormatoLinea formatoDetalle = businessData.cargarFormatoDetalle();
		
		
		
		LotePagoElectronicoLineProcessor lote = new LotePagoElectronicoLineProcessor(businessData, formatoEncabezado);
		DetallePagoElectronicoLineProcessor detalle = new DetallePagoElectronicoLineProcessor(businessData, formatoDetalle);
		LotePagoElectronicoPostProcessor lotePostProcessor = new LotePagoElectronicoPostProcessor(businessData, null);
		
		int cantLineas=0;
		int cantProcesadas=0;
		int cantOperaciones=0;
		
		businessData.setArchivoConEncabezado(Boolean.FALSE);
		String lineaEncabezado = businessData.getString(Vars.PM_LINEA_ENCABEZADO);
		primeraLinea = "S".equals(lineaEncabezado);
		if(!primeraLinea){
			handler.log("El archivo no posee linea de encabezado segun configuracion. ");
			lote.process(null);
			handler.log("Encabezado de lote registrado segun parametros por defecto.");
		}
		
		
		while ((linea = readerLineNumber.readLine()) != null) {
			businessData.setLineaEnArchivo(++cantLineas);
		}
		
		while( (linea = reader.readLine()) != null ){
			System.out.println("primeraLinea..... "+primeraLinea);
			
			if(primeraLinea){
				handler.log("El archivo posee linea de encabezado segun configuracion.  Procesando primera linea del archivo.");
				try{
					lote.process(linea);
					businessData.setArchivoConEncabezado(Boolean.TRUE);
					handler.log("Encabezado procesado.");
				}catch(LineProcessException e){
					e.printStackTrace();
					logger.error("Error en ingreso de encabezado de lote. " + e.getMessage(), e);
					handler.log("ERROR - " + e.getMessage());
					throw new ProcessException("Error en ingreso de encabezado de Lote. ", e);
				}
				primeraLinea = false;
			}else{
				logger.debug("Procesando linea {} de detalle del archivo", cantOperaciones);
				
				try {
					cantOperaciones++;
					System.out.println("cantOperacionescantOperaciones1 "+cantOperaciones);
					detalle.process(linea);
					cantProcesadas++;
					System.out.println("cantProcesadascantProcesadas1 "+cantProcesadas);
					/*kpalacios | 13/10/2016 | Asignando valores para enviarlo al documento de salida*/
					resultadoPE.append(linea).append(",Linea procesada").append(",0").append(LINE_BREAK);
				} catch (LineProcessException e) {
					e.printStackTrace();
					logger.error("Error en procesamiento de la linea: [{}]-[{}]", cantOperaciones, linea);
					handler.log("ADVERTENCIA ha ocurrido un error en el procesamiento de la linea " + cantOperaciones + " -> [" + linea + "]");
					handler.setLastException(e);
					/*kpalacios | 13/10/2016 | Asignando valores para enviarlo al documento de salida*/
					resultadoPE.append(linea).append(",Linea NO procesada").append(",9999").append(LINE_BREAK);
				}
			}
		}
		handler.log("Finalizado de procesar " + cantLineas + " del archivo.");
		
		handler.log("Detalle de Operaciones: " + cantOperaciones + " operaciones en el archivo. ");
		handler.log("Detalle de Operaciones: " + cantProcesadas +  " lineas se procesaron correctamente. ");
		
		handler.setLineasArchivo(cantLineas);
		handler.setLineasProcesadas(cantProcesadas);
		
		try{
			reader.close();
		}catch(IOException ioe){
			logger.warn("Ocurrio un error de I/O al tratar de cerrar el archivo. " + ioe.getMessage(), ioe);
		}
		

		logger.debug("Finalizado el procesamiento de las lineas.  Se procede a actualizar el lote con los montos totales procesados y la cantidad de operaciones procesadas");
		handler.log("Finalizado el procesamiento de las lineas.  Actualizando montos totales y cantidad de operaciones");
		businessData.getEncabezadoData().setCantidadOperaciones(cantOperaciones);
		
		try{
			
			if(cantOperaciones != cantProcesadas){
				handler.setResultado("A");
				handler.setDescripcionResultado("Archivo finalizo su proceso, sin embargo no todas las lineas se procesaron correctamente.");
				if (cantProcesadas == 0) {
					handler.setDescripcionResultado("Archivo NO finalizo su proceso, Formato invalido de archivo.");
				}
			}else{
				handler.setResultado("O");
				handler.setDescripcionResultado("Archivo procesado");
			}
			
			lotePostProcessor.process(null);			
		}catch(LineProcessException e){
			e.printStackTrace();
			logger.error("Error al actualizar valores de Monto Total y Cantidad de Operaciones asi como Estatus de Lote. ", e);
			handler.log("Error al actualizar Monto total y Cantidad de operaciones del Lote. " + e.getMessage());
			handler.setLastException(e);
			
			handler.setResultado("E");
			handler.setDescripcionResultado("Archivo procesado.  Ocurrieron errores en actualizacion del encabezado. ");
		}
		
		
		logger.info("Finalizado procesamiento de Archivo de Convenio para Pago Electronico");
	}

	
}
