package com.ofbox.davivienda.h2h.proc.file.format;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.MapHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hsbc.sv.desarrollo.contenedores.Respuesta;
import com.ofbox.davivienda.h2h.proc.Vars;

public class DetalleACHLineProcessor extends Processor{

	Logger logger = LoggerFactory.getLogger(DetalleACHLineProcessor.class);
	
	QueryRunner queryRunner = null;
	InvocadorJ2Entorno invocadorEntorno;
	
	public static final String INSERT_DETALLE_ACH = "insert into bs_detalleach (" + 
			"	Instalacion, Lote, Operacion, Cuenta, TipoOperacion, " + 
			"	Monto, Adenda, FechaEstatus, Estatus, IDPago, " + 
			"	AutorizacionHost, Email, ComisionAplicada, NombreCuenta, Autorizador, " + 
			"	EstatusPayBank, FechaHoraHOST, CuentaDebito, NombreDeCuenta, CodBanco, " + 
			"	TipoCuenta, DescEstatusPayBank, EnviarAHost, MontoImpuesto, propiedad, referenciainterna" + 
			") " + 
			"values (" + 
			"	?, ?, ?, ?, ?, " + 
			"	?, ?, ?, ?, ?," + 
			"	null, ?, 0.0000, ?, null, " + 
			"	null, null, ?, ?, ?, " + 
			"	?, null, null, ?, ?, ?" + 
			")";
	
	private int instalacion;
	private int lote;
	private Long codLote;
	private int operacion;
	private String cuenta;
	private int tipoOperacion;
	
	private BigDecimal monto;
	private String adenda;
	private Timestamp fechaEstatus;
	private int estatus;
	private String idPago;
	
	private String email;
	private String nombreCuenta;
	private String referencia;
	
	private String cuentaDebito;
	private String nombreDeCuenta;
	private String codPayBank;
	private Integer codBanco;
	
	private int tipoCuenta;
	private String propiedad; 			/*Por default tomara valor '0' segun definicion de estructura*/
	
	private String nombreBeneficiario; 	/*No esta definido en el detalle pero si esta definido en documento "DR H2H con BE+.doc"*/
	private String codigo;				
	private String transaccionExenta;	
	
	/*kpalacios | 17/08/2016 - variable utilizada para el calculo de monto de impuesto*/
	private BigDecimal montoImpuesto;
	private int aplicacionDebitoHost;
	
	public DetalleACHLineProcessor(BusinessData businessData,
			FormatoLinea formatoLinea) {
		super(businessData, formatoLinea);
		queryRunner = new QueryRunner();
		invocadorEntorno = new InvocadorJ2Entorno();
	}

	@Override
	public void process(String line) throws ProcessException {
		logger.info("Procesando linea de archivo : [{}]", line);
		clearValues();
		parseLine(line);
		
		logger.debug("Iniciando lectura de campos de la linea segun definicion del formato de detalle en la configuracion del proceso. [{}]", businessData.getReferenceInfo());
		setupValues();
		System.out.println("Antes de entrar a ReadValues");
		readValues();
		System.out.println("Despues de entrar a ReadValues codPayBank  "+codPayBank);
		

		logger.debug("Ejecutando validaciones correspondientes a transaccion ACH...");
		if(	validarCuentaDebitoDefinida()   &&
			validarCuentaMultiplesCuentas() && 
			validarCuentasPolParticipante() &&
			validarEntidadBancaria()        &&
			validarCuentaServicioEntorno() 	&&
			validarPropiedadCuenta()        &&
			validarTipoCuenta()             &&
			validarLongitadCuenta()
			
		  ){
			logger.debug("Se han ejecutado las validaciones correctamente. Se procede a ingresar registro ");
			estatus = 1;
			
			logger.debug("Se aumenta el monto total del lote ya que el estado de las validaciones es = 1");
			 /*Aumentar el monto total segun lo que se va procesando*/
			businessData.getEncabezadoData().addMontoTotal(monto);
		}
		
		try {
			guardarDetalle();
		} catch (SQLException e) {
			e.printStackTrace();
			logger.error("Error al guardar registro de convenio en Bs_DetallePE.  " + businessData.getReferenceInfo() + " -> " + e.getMessage(), e);
			String message = "Error al guardar registro en Detalle ACH.  Linea " +
					         businessData.getLineaEnArchivo() + " del archivo " + businessData.getEncabezadoData().getFileName();
			throw new LineProcessException(message, e);
		}
		
	}

	
	public void guardarDetalle() throws SQLException, ProcessException {
		operacion++;
		logger.debug("Guardando registro de detalle ACH correspondiente a la linea [{}] del archivo. ",	businessData.getLineaEnArchivo());
		Connection conn = null;
		try {

			conn = businessData.getLocalizadorDeConexion().getConnection();

			/******************** kpalacios - 17/08/2016 *********************** 
			 * Desencadena validaci�n de LIOF para el monto de la operaci�n, 
			 * esto para cuando sea convenio pago Detallado y cuenta de terceros. Tambien actualiza en Bs_LoteACH el MontoImpuesto.
			 */
			System.out.println("aplicacionDebitoHost | propiedad --> "	+ aplicacionDebitoHost + " | " + propiedad);
			if (aplicacionDebitoHost == 1 && propiedad.trim().equals("0")) {
				if (validarCuentaDebitoDefinida()) {					
					
					if (montoImpuesto != null && montoImpuesto.intValue() > 0) {
						BigDecimal montoImpuestoLoteACH = (BigDecimal) queryRunner.query(conn, "select MontoImpuesto from BS_LoteACH where CodLote = ? and Instalacion = ? and Lote = ? ", 
								new ScalarHandler("MontoImpuesto"), new Object[]{codLote, instalacion, lote});
				
						System.out.println("montoImpuestoLoteACH.. "+montoImpuestoLoteACH);
						BigDecimal sumaMonto = montoImpuestoLoteACH != null && montoImpuestoLoteACH.intValue() > 0 ? montoImpuestoLoteACH.add(montoImpuesto) : montoImpuestoLoteACH.ZERO.add(montoImpuesto);
						
						actualizarMontoImpLote(sumaMonto);
					}
				}
			} 
			String codBancoS = "";
			System.out.println("codBanco codBanco codBanco  "+codBanco);


			System.out.println("idPago idPago idPago  "+idPago);
			if(codBanco == 25) {
				codBancoS = "00000025";
			}else if(codBanco == 37) {
				codBancoS = "00000037";
			}else if(codBanco == 30) {
				codBancoS = "00000030";
			}else if(codBanco == 18) {
				codBancoS = "00000018";
			}else {
				codBancoS = String.valueOf(codBanco);
			}
			if(email != null && !email.equals("") && !email.equals("null")) {
				logger.debug("Entro en el == email email  "+email);
			}else if (email.equals("null")){
				logger.debug("Entro en el email email  "+email);
				email = null;
				logger.debug("Entro en el else if email");
			}else if (email == null){
				logger.debug("Entro en el else email  "+email);
				email = null;			
			}
			System.out.println("codBancoS codBancoS codBancoS  "+codBancoS);
			logger.debug("nombreCuentas :::  "+nombreCuenta);
			logger.info("nombreCuentas  "+nombreCuenta);
			Object[] params = new Object[] { instalacion, lote, operacion,
					cuenta != null ? cuenta.trim() : null, tipoOperacion, monto, adenda, fechaEstatus,
					estatus, idPago, email, nombreCuenta, cuentaDebito != null ? cuentaDebito.trim() : null,
					nombreDeCuenta, codBancoS, tipoCuenta, montoImpuesto,
					propiedad, referencia};
			for (int i = 0; i < params.length; i++) {
				System.out.println("paramsparams  "+params[i]);
			}
			logger.debug("Procediendo a guardar registro en Bs_DetalleACH con estatus = [{}]",estatus);	
			System.out.println("INSERT_DETALLE_ACHINSERT_DETALLE_ACH:"+INSERT_DETALLE_ACH);
			queryRunner.update(conn, INSERT_DETALLE_ACH, params);
			System.out.println("queryRunnerErroroupdate"+queryRunner.toString());
			logger.info("Se ha ingresado el registro correctamente ");		
			
		} finally {
			DbUtils.closeQuietly(conn);
		}
	}
	
	public void actualizarMontoImpLote(BigDecimal sumaMonto) throws ProcessException {
		logger.debug("actualizando Monto Impuesto: [{}]. ",	montoImpuesto);
		Connection conn = null;
		boolean autocommit = true;
		try {
			conn = businessData.getLocalizadorDeConexion().getConnection();
			autocommit = conn.getAutoCommit();
			conn.setAutoCommit(false);		
			
			System.out.println("Ingresando a realizar update Bs_LoteACH");
			queryRunner.update(conn, "update Bs_LoteACH set MontoImpuesto = ? where CodLote = ? and Instalacion = ? and Lote = ? ",
						new Object[] { sumaMonto, codLote, instalacion, lote});
			conn.commit();
			logger.info("Actualizados los valores montoImpuesto del registro de Lote del archivo ");
			
		} catch (SQLException e) {
			if(conn!=null){
				try{
					conn.rollback();
				}catch(Exception ignored){}
			}
			e.printStackTrace();
			logger.error("Error al realizar actualiacion de Bs_LoteACH. " + businessData.getReferenceInfo() + " -> " + e.getMessage(),e);
			throw new LineProcessException("Error al actualizar los valores del Lote despues de procesar monto impuesto. " + e.getMessage(), e);
		}finally{
			if(conn!=null){
				try{ conn.setAutoCommit(autocommit); }catch(SQLException e){ }
			}
			DbUtils.closeQuietly(conn);
		}
	}

	
	public boolean validarCuentaDebitoDefinida(){
		System.out.println("validarCuentaDebitoDefinida codPayBank  "+codPayBank);
		logger.debug("Validando la cuenta de Debito provista en el archivo (si existe) o la cuenta definida segun convenio" );
		String cuentaSegunConvenio = businessData.getEncabezadoData().getCuentaDebitoCredito();
		if(isEmptyString(cuentaDebito)){
			logger.debug("No se ha definido la cuenta de Debito/Credito en la linea.  Se toma la cuenta definida segun convenio: [{}]", cuentaSegunConvenio);
			cuentaDebito = cuentaSegunConvenio;
		}
		 /*Puede parecer doble validacion, pero segun la logica definida 
		 en realidad estoy validando si la cuenta segun convenio esta definida*/
		if(isEmptyString(cuentaDebito)){
			logger.debug("Cuenta segun convenio NO esta definida.  Por lo tanto quedara con estatus = 97 (Cuenta de debito sin permiso)");
			estatus = 7;
			return false;
		}
		
		return true;
	}
	
	public boolean validarCuentaMultiplesCuentas(){
		System.out.println("validarCuentaMultiplesCuentas codPayBank  "+codPayBank);
		logger.debug("Validando si el convenio es de multiples cuentas");
		boolean esMultiplesCuentas = businessData.getEncabezadoData().getMultiplesCuentas();
		if(!esMultiplesCuentas){
			logger.debug("Convenio de Multiples cuentas es false.");
			 /*No es convenio multiples cuentas, ya tuvo que haber sido validado
			 por el metodo validarCuentaDebitoDefinida()*/
			return true;
		}
		logger.debug("Convenio Multiples Cuentas es verdadero.  Por lo tanto se validara si la cuenta se encuentra definida en 'BS_ConvenioACH' primero y si no, se busca en el listado de 'Bs_ConvenioCuentaACH'");
		
		Integer convenio = businessData.getInt(Vars.PM_CONVENIO);
		Integer cliente = businessData.getInt(Vars.PM_CLIENTE);
		Connection conn = null;
		try{
			conn = businessData.getLocalizadorDeConexion().getConnection();
			 /*AGREGADO 31-MARZO-2106
			 Busqueda de la cuentaDebito en BS_ConvenioACH previo a buscar en BS_ConvenioCuentaACH
			 La Cuenta segun Bs_ConvenioACH ya la tengo en la variable 'cuentaDebitoCredito' de 'EncabezadoData'*/
			logger.debug("Validando la cuenta {} se encuentre definida en tabla 'Bs_ConvenioACH' para Convenio: {}, Cliente: {} ", new Object[]{cuentaDebito, convenio, cliente});
			String ctaConvenio = (String) queryRunner.query(conn, "select Cuenta from Bs_ConvenioACH where Convenio = ? and Cliente = ?", 
					new ScalarHandler("Cuenta"), new Object[]{convenio, cliente});
			logger.debug("Cuenta default definida en Bs_ConvenioACH para Convenio: [{}], Cliente: [{}] se encontro Cuenta: [{}]", new Object[]{convenio, cliente, ctaConvenio});		
			if(ctaConvenio.equals(cuentaDebito)){
				logger.debug("La cuenta {} se encuentra definida en 'BS_ConvenioACH' para el convenio {} ", cuentaDebito, convenio);
				return true;
			}
			
			logger.debug("La cuenta {} no se encuentra en 'BS_ConvenioACH', por lo que se buscara en 'BS_ConvenioCuentaACH' ", cuentaDebito);
			
			Integer cant = (Integer)queryRunner.query(conn, "select count(Cuenta) from BS_ConvenioCuentaACH where Convenio = ? and Cuenta = ?", new ScalarHandler(),  convenio, cuentaDebito);
			if(cant <= 0){
				logger.warn("NO se 97 encontro la cuenta definida en 'BS_ConvenioCuentaACH' para convenio de multiples cuentas. [{}]", businessData.getReferenceInfo());
				estatus = 7;
				return false;
			}
			logger.debug("La cuenta [{}] se encuentra definida en el listado de Bs_ConvenioCuentaACH", cuentaDebito);
			
			return true;
		}catch(SQLException e){
			e.printStackTrace();
			logger.error("Ocurrio un error al hacer la consulta de la cuenta " + cuentaDebito + " en el registro 'Bs_ConvenioCuentaACH'. " + e.getMessage(), e);
			estatus = 7;
		}finally{
			DbUtils.closeQuietly(conn);
		}
		return false;
	}
	
	public boolean validarCuentasPolParticipante(){
		System.out.println("validarCuentasPolParticipante codPayBank  "+codPayBank);
		logger.debug("Iniciando validacion de las cuentas especificadas en el archivo segun convenio abierto/cerrado");
		int polParticipante = businessData.getEncabezadoData().getPolParticipante();
		Integer convenio = businessData.getInt(Vars.PM_CONVENIO);

		if(polParticipante == 1){
			Connection conn = null;
			try{
				conn = businessData.getLocalizadorDeConexion().getConnection();
				logger.debug("El convenio es cerrado por lo tanto se validara la cuenta [{}] que existe en 'BS_ParticipanteACH' ", cuenta);
				
				Map<String, Object> map = queryRunner.query(conn, "select MontoAutorizado, Estatus from Bs_ParticipanteACH where Convenio = ? and CAST(Cuenta AS NUMERIC) = ?", new MapHandler(), convenio, cuenta);
				 /*Si el resultado es 'null' quiere decir que la cuenta no estaba registrada en la tabla*/
				if(map == null){
					logger.warn("La cuenta [{}] no se encuentra registrada en la tabla Bs_ParticipanteACH y se trata de un convenio cerrado. [{}]", cuenta, businessData.getReferenceInfo());
					logger.debug("Se registra el estatus = 7 (Empleado o Proveedor no registrado)");
					// se modifico a estatus 7 antes era 4
					estatus = 7;
					return false;
				}
				
				logger.debug("Se encontraron resultados de Bs_ParticipanteACH.  Procediendo a validar monto autorizado y estatus");
				Integer polParticipanteEstatus = (Integer) map.get("Estatus");
				Number polParticipanteMontoAutorizado = (Number) map.get("MontoAutorizado");
				
				
				if(polParticipanteEstatus!=1){
					logger.warn("La cuenta [{}] participante no se encuentra habilitada. Se coloca el registro con estatus = 7 (Participante deshabilitado)", cuenta);
					//se mofico a estatus 7 antes era estatus 2
					estatus = 7;
					return false;
				}
				
				BigDecimal montoAutorizado = new BigDecimal(polParticipanteMontoAutorizado.doubleValue());
				if(montoAutorizado.compareTo(monto)<0){
					logger.warn("La cuenta [{}] participante no cuenta con un monto autorizado ({}) superior al monto indicado en el archivo: ({}). Se coloca estatus = 7 (Monto no Autorizado)", new Object[]{cuenta, montoAutorizado, monto});
					// se modifico a estatus 7 antes era 3
					estatus = 7;
					return false;
				}
				
				logger.debug("Monto y Estatus autorizados.  La cuenta puede continuar con el proceso de validaciones");
				return true;
				
			}catch(SQLException e){
				e.printStackTrace();
				logger.error("Error al validar cuenta [{}] en Bs_ParticipanteACH", cuenta, e);
				estatus = 7;
				return false;
			}finally{
				DbUtils.closeQuietly(conn);
			}
		}
		
		logger.debug("El campo polParticipante dio resultado distinto del valor 1, por lo tanto se trata de convenio abierto y se deja pasar.  Valor: [{}]", polParticipante);
		 /*Si es convenio abierto, se deja pasar*/ 
		return true;
	}
	
	public boolean validarCuentaServicioEntorno(){
		System.out.println("validarCuentaServicioEntorno codPayBank  "+codPayBank);
		 /*Si codBanco = 5 entonces se debe validar la cuenta*/
		if(codBanco != 5){
			logger.warn("La entidad bancaria con el codigo {} no corresponde al valor '5':Banco Davivienda, por lo tanto la cuenta no es validada contra servicios de Entorno...", codBanco);
			nombreCuenta = "cuenta no validada";
			return true;
		}
		
//		logger.debug("Validando cuenta [{}] contra servicio de Entorno ...", cuenta);
//		Respuesta resp = invocadorEntorno.invocarInformacionCuentaDeposito(cuenta);
//		
//		if(resp == null || resp.getCodigo() != 0){
//			logger.warn("Se encontro un error en la validacion de la cuenta contra el servicio fabricaCoreBanca/srvInformacionCuentaDeposito. ");
//			estatus = 7;
//			return false;
//		}
//		
//		resp.primerContenedor();
//		if(!resp.siguiente()){
//			logger.warn("No se encontraron resultados para la consulta de la cuenta [{}] contra servicio fabricaCoreBanca/srvInformacionCuentaDeposito", cuenta);
//			estatus = 7;
//			return false;
//		}
//		
//		String estado = resp.obtenerString("estado");
//		if(!"A".equalsIgnoreCase(estado)){
//			logger.warn("El estado de la cuenta [{}] segun el servicio fabricaCoreBanca/srvInformacionCuentaDeposito es [{}]. Cuenta no se encuentra Activa estado = 7", cuenta, estado);
//			/************** kpalacios - 07/10/2016 ***************
//			Se modific� el estado a 65 (C_DETEST_CUENTA_INACTIVA)*/
//			estatus = 7;
//			return false;
//		}
//		
//		 /*Si se llega hasta aca es necesario invocar el siguiente servicio 
//		 que obtiene el nombre para la cuenta*/
//		String cliente = resp.obtenerString("cliente");
//		if(StringUtils.isEmpty(cliente)){
//			logger.warn("El servicio de informacion de cuenta no retorno el valor para el campo 'cliente'.  Por lo tanto no es posible obtener informacion del nombre de cuenta");
//			estatus = 7;
//			return false;
//		}
//		
//		Respuesta respInfoCliente = invocadorEntorno.invocarInformacionCliente(cliente);
//		
//		if(respInfoCliente==null || respInfoCliente.getCodigo()!=0){
//			logger.warn("Se encontro un error en la validacion de la cuenta contra el servicio fabricaCoreBanca/srvInfoCliente. ");
//			estatus = 7;
//			return false;
//		}
//		
//		respInfoCliente.primerContenedor(); 
//		if(!respInfoCliente.siguiente()){
//			logger.warn("No se encontraron resultados para la consulta de cliente [{}] contra servicio fabricaCoreBanca/srvInfoCliente", cliente);
//			estatus = 7;
//			return false;
//		}
//		
//		nombreDeCuenta = respInfoCliente.obtenerString("nombreCompleto");
//		if(isEmptyString(nombreDeCuenta)){
//			logger.warn("No se ha definido el nombre completo para la cuenta [{}] del cliente [{}], segun el servicio fabricaCoreBancoa/srvInfoCliente. ", cuenta, cliente);
//			estatus = 7;
//			return false;
//		}
		
		if(codBanco == 5) {
			
			Respuesta respNombreCuenta = invocadorEntorno.invocarInformacionNombreCuenta(cuenta, tipoCuenta);
			
			if(respNombreCuenta == null || respNombreCuenta.getCodigo() != 0){
				logger.warn("Se encontro un error en la validacion de la cuenta contra el servicio fabricaCoreBanca/srvInfoCuenta. ");
				estatus = 7;
				return false;
			}
			respNombreCuenta.primerContenedor();
			if(!respNombreCuenta.siguiente()){
				logger.warn("No se encontraron resultados para la consulta de la cuenta [{}] contra servicio fabricaCoreBanca/srvInfoCuenta/TDC/Prestamo", cuenta);
				estatus = 7;
				return false;
			}
			nombreCuenta = respNombreCuenta.obtenerString("nombreCuenta");
			logger.info("Nombre de cuenta"+nombreCuenta); 
			if(isEmptyString(nombreCuenta)){
				logger.warn("No se ha definido el nombre completo para la cuenta [{}] , segun el servicio fabricaCoreBanco/srvInfo/Cuenta/TDC/Prestamo ", cuenta);
				estatus = 7;
				return false;
			}
			logger.debug("La cuenta [{}] ha pasado las validaciones del servicio fabricaCoreBanca/srvInformacionCuentaDeposito ", cuenta);
			logger.debug("Se ha recuperado el valor de nombre completo [{}] del servicio fabricaCoreBanca/srvInfoCliente", nombreDeCuenta);
			logger.debug("Se ha recuperado el valor de nombre de cuenta [{}] del servicio fabricaCoreBanca/srvInfo/Prestamo/TDC/Cuenta", nombreCuenta);
		}
		return true;
	}
	
	public boolean validarEntidadBancaria(){
		Connection conn = null;
		try {
			conn = businessData.getLocalizadorDeConexion().getConnection();
			int lineaArchivo = businessData.getLineaEnArchivo();
			System.out.println("validarCuentasPolParticipante codPayBank  "+codPayBank);
	
			if(codPayBank.length() >2) {
				codPayBank = validarCodigoSwift(codPayBank, conn);				
			}
			String codBancoS = "";
			if(Integer.parseInt(codPayBank) == 25) {
				codBancoS = "00000025";
			}else if(Integer.parseInt(codPayBank) == 37) {
				codBancoS = "00000037";
			}else if(Integer.parseInt(codPayBank) == 30) {
				codBancoS = "00000030";
			}else if(Integer.parseInt(codPayBank) == 18) {
				codBancoS = "00000018";
			}else {
				codBancoS = String.valueOf(codPayBank);
			}
			logger.debug("Validando el valor de la entidad bancaria: [{}], que se ingresa en la linea [{}] del archivo. ", codBanco, lineaArchivo);
			
			
			Map<String, Object> map = queryRunner.query(conn, "select CodBanco, Nombre, Moneda, EsActivo from Bs_BancoACH where CodPayBank = ?", new MapHandler(), codBancoS);
			if(map==null || map.isEmpty()){
				logger.warn("No se encontro definido el Codigo de Banco [{}] en el catalogo 'BS_BancoACH' en la linea del archivo [{}]", codBanco, lineaArchivo);
				estatus = 7;
				return false;
			}
			
			String strNombreBanco = (String) map.get("Nombre");
			Boolean esActivo = (Boolean) map.get("EsActivo");
			System.out.println("Integer codBanco codBanco  "+(Integer) map.get("CodBanco"));
			codBanco = (Integer) map.get("CodBanco");
			
			System.out.println("IntegerInteger codBanco codBancocodBanco  "+codBanco);
			logger.debug("Se encontro el registro de [{}] con estado esActivo = [{}], CodBanco = [{}] para el CodPayBank: [{}] en el registro de Bs_BancoACH", new Object[]{strNombreBanco, esActivo, codBanco, codPayBank});
			
			return true;
			
		} catch (SQLException e) {
			e.printStackTrace();
			logger.error("Error al realizar consulta SQL hacia la entidad Bs_BancoACH. " + e.getMessage(), e);
			estatus = 7;  /*A falta de otro codigo queda con estatus = 7*/
			return false;
		}finally{
			DbUtils.closeQuietly(conn);
		}
		
	}
	
	
	/**
	 * @author kpalacios - 11/10/2016
	 * @return
	 */
	public boolean validarPropiedadCuenta(){
		System.out.println("validarPropiedadCuenta codPayBank  "+codPayBank);
		int lineaArchivo = businessData.getLineaEnArchivo();
		logger.debug("Validando el valor de la entidad bancaria: [{}], que se ingresa en la linea [{}] del archivo. ", codBanco, lineaArchivo);
		try {
		
			if(!isEmptyString(propiedad) && propiedad.equals("-1")){
				logger.warn("El valor especificado para determinar la propiedad de la cuenta no es v�lido estado = 67");
				/************** kpalacios - 11/10/2016 ***************
				Se modific� el estado a 67 (C_DETEST_PROPIEDAD_CUENTA_INVALIDO)*/
				//Se modific� el estado a 7 (C_DETEST_PROPIEDAD_CUENTA_INVALIDO)*/
				estatus = 7;
				return false;
			}
			
			return true;
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error al validar la propiedad de la cuenta" + e.getMessage(), e);
			estatus = 7; /*A falta de otro codigo queda con estatus = 7*/
			return false;
		}			
	}
	
	/**
	 * @author kpalacios - 11/10/2016
	 * @return
	 */
	public boolean validarTipoCuenta(){
		int lineaArchivo = businessData.getLineaEnArchivo();
		logger.debug("Validando el valor de tipo cuenta: [{}], que se ingresa en la linea [{}] del archivo. ", tipoCuenta, lineaArchivo);
		Connection conn = null;
		try {
			conn = businessData.getLocalizadorDeConexion().getConnection();
			Map<String, Object> map = queryRunner.query(conn, "select NombreCorto from BS_TipoCuenta where idAS400 = ?", new MapHandler(), tipoCuenta);
			if(map==null || map.isEmpty()){
				logger.warn("El tipo de cuenta especificado no es V�lido");
				/************** kpalacios - 11/10/2016 ***************
				Se modific� el estado a 68 (C_DETEST_TIPO_CUENTA_INVALIDO)*/
				//Se modific� el estado a 7 (C_DETEST_TIPO_CUENTA_INVALIDO)*/
				estatus = 7;
				return false;
			}
			
			return true;
			
		} catch (SQLException e) {
			e.printStackTrace();
			logger.error("Error al realizar consulta SQL hacia la entidad BS_TipoCuenta. " + e.getMessage(), e);
			estatus = 7; /*A falta de otro codigo queda con estatus = 7*/
			return false;
		}finally{
			DbUtils.closeQuietly(conn);
		}
			
	}
	
	/**
	 * @author kpalacios - 11/10/2016
	 * @return
	 */
	public boolean validarLongitadCuenta(){
		int lineaArchivo = businessData.getLineaEnArchivo();
		logger.debug("Validando la longitud de la cuenta: [{}], que se ingresa en la linea [{}] del archivo. ", cuenta, lineaArchivo);
		Connection conn = null;
		try {
			conn = businessData.getLocalizadorDeConexion().getConnection();
			StringBuilder sql = new StringBuilder();
			
			sql.append("select LongitudCtaCorriente, LongitudCtaAhorro, LongitudCtaPrestamo, LongitudTjCredito ");
			sql.append("from BS_BancoACH where CodBanco = ? ");
			
			if (tipoCuenta == 1) {
				sql.append("and LongitudCtaCorriente = ? ");
			} else if (tipoCuenta == 2) {
				sql.append("and LongitudCtaAhorro = ? ");
			} else if (tipoCuenta == 3) {
				sql.append("and LongitudCtaPrestamo = ? ");
			} else if (tipoCuenta == 4) {
				sql.append("and LongitudTjCredito = ? ");
			}			
			if(codBanco == 5 && tipoCuenta == 2 && cuenta.length()>=8){
				
			} else {
				Map<String, Object> map = queryRunner.query(conn, sql.toString(), new MapHandler(), new Object[]{codBanco, cuenta.trim().length()});
				if(map==null || map.isEmpty()){
					logger.warn("La longitud de la cuenta: [{}] no es la requerida para el tipo cuenta (idAS400): [{}] y codBanco: [{}] Especificado", new Object[]{cuenta, tipoCuenta, codBanco});
					estatus = 7;
					return false;
				}
			}
						
			return true;			
		} catch (SQLException e) {
			e.printStackTrace();
			logger.error("Error al realizar consulta SQL hacia la entidad Bs_BancoACH. " + e.getMessage(), e);
			estatus = 7;
			return false;
		}finally{
			DbUtils.closeQuietly(conn);
		}
		
	}
		
	public void readValues() throws LineProcessException {
		List<CampoLinea> campos = formatoLinea.getCampos();
		logger.debug("Iniciando proceso de readValues");
		System.out.println("CodPayBank campos campos  "+campos);
		codPayBank = buscarValorColumna("CodPayBank", campos).toString();
		System.out.println("CodPayBank CodPayBank CodPayBank  "+codPayBank);
		String strTipoCuenta = String.valueOf(buscarValorColumna("TipoCuenta", campos));
		System.out.println("CodPayBank strTipoCuenta strTipoCuenta  "+strTipoCuenta);
		System.out.println("CodPayBank strTipoCuenta.trim() strTipoCuenta.trim()  "+strTipoCuenta.trim().substring(0,1));
		logger.debug("Iniciando proceso de strTipoCuenta   "+strTipoCuenta.trim().substring(0,1));
		 /*Podra venir uno de los siguientes valores: 1 = C  Corriente, 2 = A  Ahorros, 3 = P  Prestamos, 4 = T  Tarjeta de Credito"
*/		tipoCuenta = "C".equalsIgnoreCase(strTipoCuenta.trim().substring(0,1))?1:
					 "A".equalsIgnoreCase(strTipoCuenta.trim().substring(0,1))?2:
				     "P".equalsIgnoreCase(strTipoCuenta.trim().substring(0,1))?3:
				     "T".equalsIgnoreCase(strTipoCuenta.trim().substring(0,1))?4:-1;
		cuenta = String.valueOf(buscarValorColumna("Cuenta", campos));
		monto = (BigDecimal) buscarValorColumna("Monto", campos);
		nombreBeneficiario = String.valueOf(buscarValorColumna("NombreBenef", campos));
		adenda = String.valueOf(buscarValorColumna("Adenda", campos));
		email = String.valueOf(buscarValorColumna("Email", campos));
		idPago = String.valueOf(buscarValorColumna("IDPago", campos));
		montoImpuesto = (BigDecimal) buscarValorColumna("MontoImpuesto", campos);
		referencia = String.valueOf(buscarValorColumna("CodigoORefInt", campos));
		
		String strPropiedad = String.valueOf(buscarValorColumna("Propiedad", campos));
		if(!isEmptyString(strPropiedad)){
			propiedad = "P".equalsIgnoreCase(strPropiedad.trim().substring(0,1))?"1":"0";
		}
		
		String ctaDebitoLinea = String.valueOf(buscarValorColumna("CuentaDebito", campos));
		if(!isEmptyString(ctaDebitoLinea)){
			logger.debug("Se ha definido la Cuenta Debito para la operacion en la linea: [{}]", businessData.getLineaEnArchivo());
			cuentaDebito = ctaDebitoLinea;
		}
		 
		 /*codigo = no esta definido en tabla de Detalle
		 transaccion exenta = no esta definido en tabla de detalle*/
	}

	public void setupValues() {
		EncabezadoData data = businessData.getEncabezadoData();
		instalacion = data.getInstalacion();
		lote = data.getLote();
		codLote = data.getCodLote();
		fechaEstatus = new Timestamp(System.currentTimeMillis());
		cuentaDebito = businessData.getEncabezadoData().getCuentaDebitoCredito();
		propiedad = "0"; 
		tipoOperacion = 2; 		
		/*kpalacios | 17/08/2016*/
		aplicacionDebitoHost = data.getAplicacionDebitoHost();
	}

	public void clearValues() {
		instalacion = 0;
		lote = 0;
		codLote = null;
		 /*El campo operacion no se debe resetear  */
		cuenta = null;
		tipoOperacion= 0; 
		
		monto = null; 
		adenda = null; 
		fechaEstatus = null; 
		estatus = 0; 
		idPago = null; 
		
		email = null; 
		nombreCuenta = null; 
		referencia = null;
		
		cuentaDebito = null; 
		nombreDeCuenta = null; 
		codBanco = 0;
		codPayBank = null;
		
		tipoCuenta = 0; 
		propiedad = null; 
		montoImpuesto = BigDecimal.ZERO;
	}
	
	private String validarCodigoSwift(String codBanco, Connection conn) {
		String codigoUni = "0";
		try {
			codigoUni = (String) queryRunner.query(conn, "select CodigoUni from BS_CodigosBanco where CodigoSwift = ?", 
					new ScalarHandler("CodigoSwift"), new Object[]{codBanco});
		} catch (SQLException e) {
			logger.error("Error al realizar consulta SQL hacia la entidad BS_CodigosBanco. " + e.getMessage(), e);
			return "0";
		}
		return codigoUni==null || "".equals(codigoUni)?codBanco:codigoUni;
	}

}
