package com.ofbox.davivienda.h2h.proc.file.format;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ibm.icu.util.Calendar;
import com.ofbox.davivienda.h2h.proc.ProcessLogHandler;
import com.ofbox.davivienda.h2h.proc.Vars;

public class LoteACHLineProcessor extends Processor{

	Logger logger = LoggerFactory.getLogger(LoteACHLineProcessor.class);
	
	public static final String SELECT_MAX_LOTE = "SELECT MAX(lote) + 1 FROM BS_LoteACH WHERE Instalacion = ? ";
	
	public static final String INSERT_LOTE_ACH = "insert into bs_loteach ("
			+ " 	Instalacion, Lote, Convenio, EmpresaODepto, TipoLote, "
			+ " 	MontoTotal, NumOperaciones, NombreLote, FechaEnviado, FechaRecibido, FechaAplicacion, "
			+ " 	FechaEstatus, Estatus, UsuarioIngreso, Autorizaciones, AplicacionDebitoHost, Sys_MarcaTiempo, "
			+ " 	Sys_ProcessId, Sys_LowDateTime, Sys_HighDateTime, Ponderacion, Firmas, FechaAplicacionHost, "
			+ " 	ListoParaHost, FechaHoraProcesado, Token, ComentarioRechazo, Descripcion, EstatusPayBank, "
			+ " 	ArchivoGenerado, ComisionCobrada, CuentaDebito, AutorizacionHost, DescEstatus, idTransaccion, "
			+ " 	intActualizacion, MontoImpuesto"
			+ ") values ("
			+ "		?, ?, ?, ?, ?,"
			+ " 	?, ?, ?, ?, ?, ?,"
			+ " 	?, ?, ?, ?, ?, null,"
			+ "	 	null, null, null, 0, 0, null, "
			+ "		null, null, null, null, null, null, "
			+ "		null, null, ?, null, '', null, "
			+ "		0, 0.0000"
			+ ")";
	
	private int instalacion;
	private int lote;
	private int convenio;
	private int empresaODepto;
	private int tipoLote;
	
	private BigDecimal montoTotal;
	private Long numOperaciones;
	private String nombreLote;
	private Timestamp fechaEnviado;
	private Timestamp fechaRecibido;
	private Timestamp fechaAplicacion;
	
	private Timestamp fechaEstatus;
	private int estatus;
	private String usuarioIngreso = "H2H";
	private int autorizaciones;
	private int aplicacionDebitoHost;
	
	private String cuentaDebito;
	
	public LoteACHLineProcessor(BusinessData businessData, FormatoLinea formatoLinea) {
		super(businessData, formatoLinea);
	}

	@Override
	public void process(String line) throws ProcessException {
		ProcessLogHandler handler = businessData.getHandler();
		logger.debug("Se inicia procesamiento de encabezado de Lote ACH");
		
		valuesFromBusinessData();
		
		if(line == null){
			logger.info("El archivo {} no posee linea de encabezado definido.  Se toman los valores por defecto");
			handler.log("El archivo no posee linea de encabezado definido.  Se toman los valores por defecto ");
			defaultValues();
		}else{
			logger.debug("Se toman los valores de la linea: {}", line);
			if(formatoLinea==null){
				logger.error("Error, no existe el formato de la linea de encabezado del archivo definido!. Se asumen valores por defecto.");
				handler.log("ADVERTENCIA: No se ha definido formato de encabezado para el archivo a pesar que se ha definido que si posee encabezado");
				defaultValues();
			}else{
				parseLine(line);
			}
		}
		
		logger.debug("Recuperando valores de la linea para proceder a insercion");
		if(line!=null){
			readValuesFromLine();
		}
		
		logger.debug("Ingresando valores en registro Bs_LoteACH");
		handler.log("Ingresando valores hacia registro en tabla Bs_LoteACH");
		insertValues();
		
		logger.debug("Finalizado de ingresar registro en Bs_LoteACH");
		handler.log("Valores ingresados");
	}
	
	
	public void insertValues() throws LineProcessException{
		
		QueryRunner runner = new QueryRunner();
		Connection conn = null;
		boolean autocommit = true;
		try{
			conn = businessData.getLocalizadorDeConexion().getConnection();
			autocommit = conn.getAutoCommit();
			conn.setAutoCommit(false);
			
			logger.debug("Obteniendo valor de siguiente secuencia de lote ...");
			
			Number num = (Number) runner.query(conn, SELECT_MAX_LOTE, new ScalarHandler(), instalacion);
			lote = num!=null?num.intValue():1; 
			logger.debug("Siguiente valor para num de Lote en tabla Bs_LoteACH: {}", lote);
			
			businessData.getEncabezadoData().setLote(lote);
			businessData.getEncabezadoData().setAplicacionDebitoHost(aplicacionDebitoHost);
			
			 /*Preparo los parametros
			kpalacios | 24/08/2016 se restringe a 30 caracteres el nombreLote ya que es la longitud m�xima q permite la BD*/
			Object[] parameters = {
				instalacion, lote, convenio, empresaODepto, tipoLote,montoTotal, numOperaciones, 
				nombreLote.trim().length() < 30 ? nombreLote.trim() : nombreLote.trim().substring(0, 30), fechaEnviado, fechaRecibido, 
				fechaAplicacion,fechaEstatus, estatus, usuarioIngreso, autorizaciones, aplicacionDebitoHost,cuentaDebito
			};
			
			logger.debug("Realizando insert en tabla BS_LotePE ...");
			System.out.println("INSERT_LOTE_ACHINSERT_LOTE_ACH:"+INSERT_LOTE_ACH);
			for (int i = 0; i < parameters.length; i++) {
				System.out.println("parametersparameters  "+parameters[i]);
			}
			runner.update(conn, INSERT_LOTE_ACH, parameters);
			System.out.println("runnerrunnerrunner"+runner.toString());
			logger.debug("Insert realizado.  Obteniendo valor de CodLote generado de la insercion...");
			
			Number ident = (Number) runner.query(conn, "SELECT IDENT_CURRENT('Bs_LoteACH')" , new ScalarHandler());
			logger.debug("Se ha generado el valor de {} para CodLote ", ident);
			businessData.getEncabezadoData().setCodLote(ident.longValue());
			
			logger.debug("Realizando commit de la transaccion ...");
			conn.commit();
			logger.debug("Transaccion realizada correctamente...");
			conn.setAutoCommit(autocommit);
			
		}catch(SQLException e){
			if(conn!=null) try{ conn.rollback(); }catch(Exception ignored) {} 
			logger.error("Error al intentar realizar la insercion de registro en BS_LoteACH. " + e.getMessage(), e);
			throw new LineProcessException(e);
		}finally{
			DbUtils.closeQuietly(conn);
		}
	}
	
	public void readValuesFromLine() throws LineProcessException{
		List<CampoLinea> campos = formatoLinea.getCampos();
		logger.debug("Iniciando busqueda de campos correspondientes al encabezado de un Lote ACH");
		montoTotal = (BigDecimal) buscarValorColumna("MontoTotal", campos);
		numOperaciones = (Long) buscarValorColumna("NumOperaciones", campos);
		String strAplicacionDebitoHost = String.valueOf(buscarValorColumna("AplicacionDebitoHost", campos));
		aplicacionDebitoHost = "D".equals(strAplicacionDebitoHost)?1:2;
		
		java.util.Date udFechaAplicacion = (java.util.Date) buscarValorColumna("FechaAplicacion", campos);
		if(udFechaAplicacion!=null){
			Long horaAplicacion =  (Long) buscarValorColumna("HoraAplicacion", campos);
			String fecha = "";
			Calendar cal = Calendar.getInstance();
			if(horaAplicacion != null && !horaAplicacion.equals("")) {
				fecha = horaAplicacion.toString();
			}else {
				fecha = "";
			}
			
			if(fecha.length() == 4) {
				String hora = fecha.substring(0, 2);
				String minutos = fecha.substring(2, 4);
				System.out.println("hora:   "+hora);
				System.out.println("minutos:   "+minutos);
				if(horaAplicacion != null){
					cal.setTime(udFechaAplicacion);
					if(horaAplicacion > 0){
						cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(hora));
						cal.set(Calendar.MINUTE, Integer.parseInt(minutos));
					}
				}
			}else if(fecha.length() == 3) {
				String hora = fecha.substring(0, 1);
				String minutos = fecha.substring(1, 3);
				System.out.println("hora:   "+hora);
				System.out.println("minutos:   "+minutos);
				if(horaAplicacion != null){
					cal.setTime(udFechaAplicacion);
					if(horaAplicacion > 0){
						cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(hora));
						cal.set(Calendar.MINUTE, Integer.parseInt(minutos));
					}
				}
			}
			
			fechaAplicacion = new Timestamp(cal.getTimeInMillis());
		}else{
			logger.warn("La fecha de Aplicacion no ha sido definida en el encabezado.  Se toma la fecha de sistema por default.");
			fechaAplicacion = new Timestamp(System.currentTimeMillis());
		}
		
		
		nombreLote = String.valueOf(buscarValorColumna("NombreLote", campos));
		if(isEmptyString(nombreLote)){
			nombreLote = businessData.getEncabezadoData().getFileName();
		}
		
		logger.debug("Campos recuperados correctamente...");
	}
	
	private void defaultValues(){
		montoTotal = new BigDecimal(0);		/* Se actualizara despues de leer las lineas*/
		numOperaciones = new Long(0); 		/* Se actualizara despues de leer las lineas*/
		aplicacionDebitoHost = 2;			/*Valor por defecto 1: Detallado, 2:Consolidado */
		nombreLote = businessData.getEncabezadoData().getFileName();
		/* 
		 * MODIFICACION A PETICION DE DAVIVIENDA -> 14-MAR-2016
		 * 1. El campo FechaAplicacion de la tabla BS_LotePE, Cuando no sea un lote programado el campo debe de quedar con el valor de NULL
		 * Segun definicion de documento, la unica manera de saber si un lote es programado o no 
		 * es si posee encabezado, ya que la fecha se encuentra indicada en el cabezado y segun la documentacion
		 * (DR H2H con BE+.doc) 
		 * */
		fechaAplicacion = null;
	}
	
	private void valuesFromBusinessData(){
		EncabezadoData data = businessData.getEncabezadoData();
		instalacion = data.getInstalacion();
		lote = data.getLote();
		cuentaDebito = data.getCuentaDebitoCredito();
		
		convenio = businessData.getInt(Vars.PM_CONVENIO);
		empresaODepto = businessData.getInt(Vars.PM_EMPRESAODEPTO);
		tipoLote = data.getTipoLote();
				
		fechaEnviado = new Timestamp(System.currentTimeMillis());
		fechaRecibido = new Timestamp(System.currentTimeMillis());
		fechaEstatus = new Timestamp(System.currentTimeMillis());
		
		
		estatus = 70;
		usuarioIngreso = "H2H";
		autorizaciones = 0;

		
	}

}
