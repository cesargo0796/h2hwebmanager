package com.ofbox.davivienda.h2h.proc.file.format;

public class CampoLinea {

	private String nombre;
	
	private String tablaDestino;
	
	private String columnaDestino;
	
	
	private String tipoDatoAtributo;
	
	private int posicionCampo;
	
	private int posicionInicio;
	
	private int posicionFin;
	
	private String formatoAtributo;

	private String valor;
	
	private String opcional;
	
	public String getTablaDestino() {
		return tablaDestino;
	}

	public void setTablaDestino(String tablaDestino) {
		this.tablaDestino = tablaDestino;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getColumnaDestino() {
		return columnaDestino;
	}

	public void setColumnaDestino(String columnaDestino) {
		this.columnaDestino = columnaDestino;
	}

	public String getTipoDatoAtributo() {
		return tipoDatoAtributo;
	}

	public void setTipoDatoAtributo(String tipoDatoAtributo) {
		this.tipoDatoAtributo = tipoDatoAtributo;
	}

	public int getPosicionCampo() {
		return posicionCampo;
	}

	public void setPosicionCampo(int posicionCampo) {
		this.posicionCampo = posicionCampo;
	}

	public int getPosicionInicio() {
		return posicionInicio;
	}

	public void setPosicionInicio(int posicionInicio) {
		this.posicionInicio = posicionInicio;
	}

	public int getPosicionFin() {
		return posicionFin;
	}

	public void setPosicionFin(int posicionFin) {
		this.posicionFin = posicionFin;
	}

	public String getFormatoAtributo() {
		return formatoAtributo;
	}

	public void setFormatoAtributo(String formatoAtributo) {
		this.formatoAtributo = formatoAtributo;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public String getOpcional() {
		return opcional;
	}

	public void setOpcional(String opcional) {
		this.opcional = opcional;
	}
	
	public boolean isCampoOpcional(){
		return "S".equals(this.opcional);
	}

	@Override
	public String toString() {
		return "CampoLinea [nombre=" + nombre + ", tablaDestino=" + tablaDestino + ", columnaDestino=" + columnaDestino
				+ ", tipoDatoAtributo=" + tipoDatoAtributo + ", posicionCampo=" + posicionCampo + ", posicionInicio="
				+ posicionInicio + ", posicionFin=" + posicionFin + ", formatoAtributo=" + formatoAtributo + ", valor="
				+ valor + ", opcional=" + opcional + "]";
	}
	
	
	
}
