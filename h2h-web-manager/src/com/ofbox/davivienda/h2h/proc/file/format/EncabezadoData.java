package com.ofbox.davivienda.h2h.proc.file.format;

import java.math.BigDecimal;

public class EncabezadoData {
	
	private String nombreCuenta;
	/**
	 * Instalacion a utilizar
	 */
	private Integer instalacion;
	
	/**
	 * Tipo de lote en las tablas de encabezado
	 * Indica el tipo de lote:
	 * <ul> 
	 * <li>1=Convenio de cobro (creditos a las cuentas destino)</li> 
	 * <li>2=Convenio de pago </li>
	 * </ul> 
	 */
	private Integer tipoLote;
	
	/**
	 * Indica si el convenio es de multiples cuentas origen
	 * <ul>
	 * 	<li> 0 = No permite multiples cuentas origen, por lo tanto la cuenta origen es la cuenta de la tabla 'Bs_Convenio' </li>
	 *  <li> 1 = Multiples cuentas origen por lo tanto hay validar la cuenta origen del archivo en la tabla Bs_ConvenioCuenta </li>
	 * </ul>
	 */
	private Boolean multiplesCuentas;
	
	/**
	 * Cuenta configurada en la tabla Bs_Convenio si multiplesCuentas = 0
	 * Cuenta configurada en la tabla Bs_ConvenioCuenta si multiplesCuentas = 1
	 */
	private String cuentaDebitoCredito;
	
	/**
	 * Estatus que debera colocarse en el encabezado del lote
	 */
	private Integer estatus;
	
	/**
	 * Flag que indica si son multiples participantes del convenio. 
	 * <ul>
	 * 	<li> 1 = Convenio Cerrado : Indica que se deben de validar las cuentas de detalle en la tabla "BS_Participante" </li>
	 *  <li> 2 = Convenio Abierto : Se dejan pasar las cuentas </li>
	 * </ul>
	 */
	private Integer polParticipante;
	
	/**
	 * Nombre del archivo del cual se tomara el nombre del lote en caso
	 * no se haya definido
	 */
	private String fileName;
	
	/**
	 * Identificador del lote
	 */
	private int lote;
	
	/**
	 * Monto total
	 */
	private BigDecimal montoTotal;
	
	/**
	 * Cantidad de operaciones
	 */
	private Integer cantidadOperaciones;
	
	/**
	 * CodLote para los registros de Bs_LoteACH
	 */
	private Long codLote;
	
	/**
	 * aplicacionDebitoHost, Valor por defecto 1: Detallado, 2:Consolidado.
	 * 
	 * kpalacios | 17/08/2016
	 */
	private int aplicacionDebitoHost;
	
	public EncabezadoData() {
		montoTotal = new BigDecimal(0);
	}

	public Integer getInstalacion() {
		return instalacion;
	}

	public void setInstalacion(Integer instalacion) {
		this.instalacion = instalacion;
	}

	

	public Boolean getMultiplesCuentas() {
		return multiplesCuentas;
	}

	public void setMultiplesCuentas(Boolean multiplesCuentas) {
		this.multiplesCuentas = multiplesCuentas;
	}

	public String getCuentaDebitoCredito() {
		return cuentaDebitoCredito;
	}

	public void setCuentaDebitoCredito(String cuentaDebitoCredito) {
		this.cuentaDebitoCredito = cuentaDebitoCredito;
	}

	public Integer getEstatus() {
		return estatus;
	}

	public void setEstatus(Integer estatus) {
		this.estatus = estatus;
	}

	public Integer getPolParticipante() {
		return polParticipante;
	}

	public void setPolParticipante(Integer polParticipante) {
		this.polParticipante = polParticipante;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public int getLote() {
		return lote;
	}

	public void setLote(int lote) {
		this.lote = lote;
	}

	public void addMontoTotal(BigDecimal monto){
		montoTotal = montoTotal.add(monto);
	}
	
	public BigDecimal getMontoTotal() {
		return montoTotal;
	}

	public void setMontoTotal(BigDecimal montoTotal) {
		this.montoTotal = montoTotal;
	}

	public Integer getCantidadOperaciones() {
		return cantidadOperaciones;
	}

	public void setCantidadOperaciones(Integer cantidadOperaciones) {
		this.cantidadOperaciones = cantidadOperaciones;
	}

	public Integer getTipoLote() {
		return tipoLote;
	}

	public void setTipoLote(Integer tipoLote) {
		this.tipoLote = tipoLote;
	}

	public Long getCodLote() {
		return codLote;
	}

	public void setCodLote(Long codLote) {
		this.codLote = codLote;
	}

	public int getAplicacionDebitoHost() {
		return aplicacionDebitoHost;
	}

	public void setAplicacionDebitoHost(int aplicacionDebitoHost) {
		this.aplicacionDebitoHost = aplicacionDebitoHost;
	}

	public String getNombreCuenta() {
		return nombreCuenta;
	}

	public void setNombreCuenta(String nombreCuenta) {
		this.nombreCuenta = nombreCuenta;
	}

	
	
	
}
