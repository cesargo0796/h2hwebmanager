package com.ofbox.davivienda.h2h.proc.file;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Expression {

	/* El logger es de FileNameMatcher ya que esta clase se utiliza unicamente ahi*/
	Logger logger = LoggerFactory.getLogger(FileNameMatcher.class);
	
	private Map<String, Object> parameterMap;
	
	private String expression;

	public Expression(String expression,Map<String, Object> parameterMap) {
		super();
		this.parameterMap = parameterMap;
		this.expression = expression;
	}

	public String getExpression() {
		return this.expression;
	}

	public String evaluate() {
		logger.debug("Evaluando formato expression: " + expression.toString());
		logger.debug("Evaluando formato parameterMap: " + parameterMap.toString());
		logger.debug("Evaluando formato parameterMap.size(): " + parameterMap.size());
		if (expression == null || expression.length() == 0)
			return "";
		if (expression.startsWith("$P")) {
			String param = expression.substring(expression.indexOf("('") + 2, expression.lastIndexOf("')"));
			logger.debug("Evaluando parametro: " + param);
			Object result = parameterMap.get(param);
			logger.debug("Evaluando parametro result: " + result);
			if (result != null)
				return result.toString();
		} else if (expression.startsWith("$D")) {
			String format = expression.substring(expression.indexOf("('") + 2,
					expression.lastIndexOf("')"));
			logger.debug("Evaluando formato: " + format);		
			SimpleDateFormat sdf = new SimpleDateFormat(format);
			logger.debug("Evaluando formato sdf.toString(): " + sdf.toString());
			logger.debug("Evaluando formato System.currentTimeMillis(): " + System.currentTimeMillis());
			return sdf.format(new Date(System.currentTimeMillis()));
		}
		return "";
	}

	public String toString() {
		return this.expression;
	}
}