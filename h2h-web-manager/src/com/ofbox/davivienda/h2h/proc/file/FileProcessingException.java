package com.ofbox.davivienda.h2h.proc.file;

public class FileProcessingException extends Exception {

	public FileProcessingException() {
		super();
		
	}



	public FileProcessingException(String message, Throwable cause) {
		super(message, cause);
		
	}

	public FileProcessingException(String message) {
		super(message);
		
	}

	public FileProcessingException(Throwable cause) {
		super(cause);
		
	}

	
	
}
