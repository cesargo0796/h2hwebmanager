package com.ofbox.davivienda.h2h.proc.file.format;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.MapHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hsbc.sv.desarrollo.contenedores.Respuesta;
import com.ofbox.davivienda.h2h.proc.Vars;

public class DetallePagoElectronicoLineProcessor extends Processor{

	Logger logger = LoggerFactory.getLogger(DetallePagoElectronicoLineProcessor.class);
	
	private int instalacion;
	private int lote;
	private int sublote;
	private int operacion = 0;
	private String cuenta;

	private BigDecimal monto;
	
	private String adenda;
	private Timestamp fechaEstatus;
	private int estatus;
	private String idPago;
	private String nombreBenef;
	private String direccionBenef;
	private String email;
	private String cuentaDebito;
	private String nombreDeCuenta; 
	private String codigoORefInt;
	private String nombreCuenta;
	private BigDecimal montoImpuesto;
	private int aplicacionDebitoHost;
	
	
	private static final String INSERT_BS_DETALLE_PE = "insert into bs_detallepe ("
			+ "Instalacion, Lote, Operacion, Cuenta, TipoOperacion, Monto, "
			+ "MontoAplicado, Adenda, FechaEstatus, Estatus, IDPago, AutorizacionHost, "
			+ "EnviarAHost, Contrasena, NombreBenef, DireccionBenef, Email, ComisionAplicada, "
			+ "IVAAplicado, NombreCuenta, NIU, Autorizador, Sublote, CuentaDebito, "
			+ "NombreDeCuenta, MontoImpuesto, CodigoORefInt) "
			+ "values ( ?, ?, ?, ?, ?, ?, "
			+ "			NULL, ?, ?, ?, ?, NULL, "
			+ "			1, NULL, ?, ?, ?, 0.0000, "
			+ "			0.0000, ?, NULL, NULL, ?, ?, "
			+ "			?, ?, ? )";
	
	private static final String INSERT_BS_DETALLE_PE_CHE = "insert into bs_detallepe ("
			+ "Instalacion, Lote, Operacion, Cuenta, TipoOperacion, Monto, "
			+ "MontoAplicado, Adenda, FechaEstatus, Estatus, IDPago, AutorizacionHost, "
			+ "EnviarAHost, Contrasena, NombreBenef, DireccionBenef, Email, ComisionAplicada, "
			+ "IVAAplicado, NombreCuenta, NIU, Autorizador, Sublote, CuentaDebito, "
			+ "NombreDeCuenta, MontoImpuesto, CodigoORefInt) "
			+ "values ( ?, ?, ?, ?, ?, ?, "
			+ "			NULL, ?, ?, ?, ?, NULL, "
			+ "			1, ?, ?, ?, ?, 0.0000, "
			+ "			0.0000, ?, NULL, NULL, ?, ?, "
			+ "			?, ?, ? )";
	
	QueryRunner queryRunner = null;
	InvocadorJ2Entorno invocadorEntorno;
	
	public DetallePagoElectronicoLineProcessor(BusinessData businessData,
			FormatoLinea formatoLinea) {
		
		super(businessData, formatoLinea);
		queryRunner = new QueryRunner();
		invocadorEntorno = new InvocadorJ2Entorno();
	}

	@Override
	public void process(String line) throws LineProcessException {
		logger.info("Procesando linea de archivo : [{}]", line);
		clearValues();
		parseLine(line);
		logger.debug("validarCuentaDebitoDefinida cuentaDebito1.2.2: "+cuentaDebito);
		logger.debug("Iniciando lectura de campos de la linea segun definicion del formato de detalle en la configuracion del proceso. [{}]", businessData.getReferenceInfo());
		setupValues();
		logger.debug("validarCuentaDebitoDefinida cuentaDebito1.2.3: "+cuentaDebito);
		readValues();
		
		logger.debug("validarCuentaDebitoDefinida cuentaDebito1.2: "+cuentaDebito);
		if(cuenta != null) {						
		}else {
			cuenta = "000000000000";
		}	
		boolean convenioChequeA = 4 == businessData.getEncabezadoData().getTipoLote();
		logger.debug("convenioChequeAconvenioChequeA   "+convenioChequeA); 
		logger.debug("Ejecutando validaciones de la cuenta ...");
		logger.debug("linelinelinelinelineline  "+line); 
		logger.debug("validarCuentaDebitoDefinida()   "+validarCuentaDebitoDefinida() ); 
		logger.debug("validarCuentaMultiplesCuentas()  "+validarCuentaMultiplesCuentas()); 
		logger.debug("validarCuentasPolParticipante()  "+validarCuentasPolParticipante()); 
		logger.debug("validarCuentaServicioEntorno()  "+validarCuentaServicioEntorno()); 

		if( convenioChequeA && validarCuentaDebitoDefinida()   &&		/*Se valida que la cuenta este en la linea o bien, que se utilice la cuenta definida en el convenio */
				validarCuentaMultiplesCuentas() &&						/*Se valida el flag convenios multiples cuentas*/
				validarCuentasPolParticipante()  						/*Se valida la cuenta que viene en el archivo		*/			
			  ){
			logger.debug("Se cumplió la validación: convenioChequeA, validarCuentaDebitoDefinida(), validarCuentaMultiplesCuentas(), validarCuentasPolParticipante()");
			estatus = 1;			
			logger.debug("Se aumenta el monto total del lote ya que el estado de las validaciones es = 1");
			businessData.getEncabezadoData().addMontoTotal(monto);
			
		}else if( validarCuentaDebitoDefinida()   &&	/*Se valida que la cuenta este en la linea o bien, que se utilice la cuenta definida en el convenio */
			validarCuentaMultiplesCuentas() &&			/*Se valida el flag convenios multiples cuentas*/
			validarCuentasPolParticipante() && 			/*Se valida la cuenta que viene en el archivo*/
			validarCuentaServicioEntorno()				/*Validar contra servicio Entorno*/
		  ){
			logger.debug("Se cumplió las validaciones: validarCuentaDebitoDefinida(), validarCuentaMultiplesCuentas(), validarCuentasPolParticipante(), validarCuentaServicioEntorno()");
			estatus = 1;			
			logger.debug("Se aumenta el monto total del lote ya que el estado de las validaciones es = 1");
			businessData.getEncabezadoData().addMontoTotal(monto);
			
		}else{
			logger.debug("El registro no ha superado las validaciones de la cuenta. ");
		}

		
		try {
			guardarDetalle();
		} catch (SQLException e) {
			e.printStackTrace();
			logger.error("Error al guardar registro de convenio en Bs_DetallePE.  " + businessData.getReferenceInfo() + " -> " + e.getMessage(), e);
			throw new LineProcessException("Error al guardar registro de Detalle del Lote. ", e);
		}
		
	}

	
	
	public void guardarDetalle() throws SQLException{
		Connection conn = null;
		try{
			conn = businessData.getLocalizadorDeConexion().getConnection();
			 /*Primero determinar si se trata de convenio de pago o de cobro para saber como se 
			 Guardaran los registros, siempre y cuando el estatus = 1*/
			logger.debug("businessData.getEncabezadoData().getTipoLote()   "+businessData.getEncabezadoData().getTipoLote());
			logger.debug("estatus estatus estatus   "+estatus);
			/*Se determina que es un convenio de pago si tipoLote = 2*/
			boolean convenioPago = 2 == businessData.getEncabezadoData().getTipoLote();
			boolean convenioCheque = 4 == businessData.getEncabezadoData().getTipoLote();
			/*Se determina que es un convenio de cobro si tipoLote = 1*/
			boolean convenioCobro = 1 == businessData.getEncabezadoData().getTipoLote();
			
			int lineNumber = businessData.getArchivoConEncabezado().equals(Boolean.TRUE) ? businessData.getLineaEnArchivo() - 1 : businessData.getLineaEnArchivo();
			logger.debug("businessData.getArchivoConEncabezado(): "+businessData.getArchivoConEncabezado());
			logger.debug("lineNumber Inicial: "+lineNumber);
			
			int tipoOperacion = 0;
			int tipoOperacionCuentaOrigen = 0;
			logger.debug("convenioPago convenioPago   "+convenioPago);
			logger.debug("convenioCobro convenioCobro   "+convenioCobro);
			logger.debug("convenioCheque convenioCheque   "+convenioCheque);
			boolean flagA = 4 == businessData.getEncabezadoData().getTipoLote();
			if(convenioPago || convenioCheque){
				tipoOperacion = 2;
				tipoOperacionCuentaOrigen = 1;
			}
			
			if(convenioCobro){
				tipoOperacion = 1;
				tipoOperacionCuentaOrigen = 2;
			}
			
			if(estatus!=1){
				operacion++;
				
				logger.debug("estatus != 1, por lo tanto se guardara un solo registro en BS_DetallePE");
				logger.debug("operacion   "+operacion);
				logger.debug("guardarDetalle emailemail   "+email);
				logger.debug("guardarDetalle adendaadenda   "+adenda);
				logger.debug("guardarDetalle idPagoidPago   "+idPago);
				logger.debug("parametros insert cuentaDebito1.1: "+cuentaDebito);
				logger.debug("parametros insert nombreDeCuenta1.1.1: "+nombreDeCuenta);
				logger.debug("parametros insert cuenta1.1.1: "+cuenta);
				logger.debug("convenioCheque convenioCheque11   "+convenioCheque);
				logger.debug("parametros insert cuenta1.1.2: "+cuenta);
				
				if(nombreDeCuenta != null && !nombreDeCuenta.equals("") && !nombreDeCuenta.equals("null")) {					
					logger.debug("Entro en el nombreDeCuenta nombreDeCuenta nombreDeCuenta  "+nombreDeCuenta);
				}else if (nombreDeCuenta == null){
					logger.debug("Entro en el else nombreDeCuenta  "+nombreDeCuenta);
					nombreDeCuenta = "";			
				}
				logger.debug("parametros insert tipoOperacion1: "+tipoOperacion);
				sublote = lote+1;
				montoImpuesto = BigDecimal.ZERO;
				Object[] params = new Object[]{
					instalacion, lote, operacion, cuenta != null ? cuenta.trim() : null, tipoOperacion, monto,
					adenda, fechaEstatus, estatus, idPago, nombreBenef,
					direccionBenef, email, null, sublote, cuentaDebito != null ? cuentaDebito.trim() : null, nombreDeCuenta, montoImpuesto, codigoORefInt 
				};	
				logger.debug("parametros params.length1: "+params.length);
				logger.debug("********************************************");
				for (int i = 0; i < params.length; i++) {
					System.out.println("parametros params[i]:  "+params[i]);
				}
				logger.debug("parametros insert INSERT_BS_DETALLE_PE: "+INSERT_BS_DETALLE_PE);
				queryRunner.update(conn, INSERT_BS_DETALLE_PE, params);
				logger.debug("Registro agregado en Bs_DetallePE");
				
				
				/******************* kpalacios - 19/08/2020 ***********************
				 Validación para insertar solamente 1 vez CuentaDebito, donde el valor monto es el monto total
				 de las operaciones antes procesadas. */
				if (lineNumber == operacion ) {
					guardarDetalleEncabezado(tipoOperacionCuentaOrigen, conn);
				}
				
				return;
			}
			
			
			logger.debug("convenioCheque convenioCheque22   "+convenioCheque);
			if(convenioPago || convenioCheque){
				
				String codigoRef = "";
				
				/******************* kpalacios - 17/08/2016 ***********************
				 Desencadena validación de LIOF para el monto de la operación, esto cuando sea convenio pago Detallado. */
				System.out.println("aplicacionDebitoHost --> "+aplicacionDebitoHost);
				if (aplicacionDebitoHost == 1) {
					codigoRef = "000000000000";
					logger.debug("parametros insert cuentaDebito2: "+cuentaDebito);
					if (validarCuentaDebitoDefinida()) {
					}				
				} else {
					if (lineNumber == 1) {
						codigoRef = codigoORefInt;
						
					}
				}
				
				operacion++;
				logger.debug("Realizando insercion de operacion de Credito para un convenio de PAGO");
				logger.debug("operacion   "+operacion);
				logger.debug("guardarDetalle emailemail2   "+email);
				logger.debug("guardarDetalle adendaadenda2   "+adenda);
				logger.debug("guardarDetalle idPagoidPago2   "+idPago);
				logger.debug("parametros insert cuentaDebito2: "+cuentaDebito);
				logger.debug("parametros insert nombreCuenta2: "+nombreCuenta);
				logger.debug("parametros insert nombreDeCuenta2: "+nombreDeCuenta);
				logger.debug("parametros insert tipoOperacion2: "+tipoOperacion);
				if(direccionBenef != null && !direccionBenef.equals("") && !direccionBenef.equals("null")) {
					logger.debug("Entro en el == direccionBenef direccionBenef  "+direccionBenef);
				}else if (direccionBenef.equals("null")){
					logger.debug("Entro en el direccionBenef direccionBenef  "+direccionBenef);
					direccionBenef = null;
					logger.debug("Entro en el else if direccionBenef");
				}else if (direccionBenef == null){
					logger.debug("Entro en el else direccionBenef  "+direccionBenef);
					direccionBenef = null;			
				}
				if(flagA) {
					nombreBenef = idPago;
					idPago = "";
					if(tipoOperacion == 1) {
						codigoORefInt = "000000000000";
					}
				}
				boolean flagB = 4 == businessData.getEncabezadoData().getTipoLote();
				Object[] params = new Object[]{
						instalacion, lote, operacion, cuenta != null ? cuenta.trim() : null, tipoOperacion, monto,
						adenda, fechaEstatus, estatus, idPago, nombreBenef,
						direccionBenef, email, nombreCuenta, sublote, cuentaDebito != null ? cuentaDebito.trim() : null, nombreDeCuenta, montoImpuesto, codigoORefInt
					};
				if(flagB) {
					montoImpuesto = BigDecimal.ZERO;
					sublote = lote+1;	
					String contrasenaBD = (String) queryRunner.query(conn, "select top 1 Contrasena from BS_GeneradorContrasenaPE where Estatus = 0 ", new ScalarHandler("Contrasena"));
					logger.debug("contrasenaBD contrasenaBD contrasenaBD: "+contrasenaBD);
					params = new Object[]{
							instalacion, lote, operacion, cuenta != null ? cuenta.trim() : null, tipoOperacion, monto,
							adenda, fechaEstatus, estatus, idPago, contrasenaBD, nombreBenef,
							direccionBenef, email, nombreCuenta, sublote, cuentaDebito != null ? cuentaDebito.trim() : null, nombreDeCuenta, montoImpuesto, codigoORefInt
						};
					logger.debug("parametros insert detlote2: "+codigoORefInt);
					logger.debug("parametros params.length2: "+params.length);
					logger.debug("********************************************");
					for (int i = 0; i < params.length; i++) {
						System.out.println("parametros params[i]:  "+params[i]);
						logger.debug("parametros params[i]:  "+params[i]);
					}
					queryRunner.update(conn, INSERT_BS_DETALLE_PE_CHE, params);
					logger.debug("Registro agregado en Bs_DetallePE_CHE para la operacion de Credito");
					Object[] paramsG = new Object[]{instalacion, lote, contrasenaBD};
					queryRunner.update(conn, "update BS_GeneradorContrasenaPE SET  Estatus = 2, Instalacion = ?,Lote = ? where contrasena = ?", paramsG);
					
				}else {
					montoImpuesto = BigDecimal.ZERO;
					sublote = lote+1;				
					params = new Object[]{
							instalacion, lote, operacion, cuenta != null ? cuenta.trim() : null, tipoOperacion, monto,
							adenda, fechaEstatus, estatus, idPago, nombreBenef,
							direccionBenef, email, nombreCuenta, sublote, cuentaDebito != null ? cuentaDebito.trim() : null, nombreDeCuenta , montoImpuesto, codigoORefInt
						};
					logger.debug("parametros insert detlote2: "+codigoORefInt);
					logger.debug("parametros params.length2: "+params.length);
					logger.debug("********************************************");
					for (int i = 0; i < params.length; i++) {
						System.out.println("parametros params[i]:  "+params[i]);
						logger.debug("parametros params[i]:  "+params[i]);
					}
					queryRunner.update(conn, INSERT_BS_DETALLE_PE, params);
					logger.debug("Registro agregado en Bs_DetallePE para la operacion de Credito");
				}
				
				
				
				/******************* kpalacios - 19/08/2016 ***********************
				 Validación para insertar solamente 1 vez CuentaDebito, donde el valor monto es el monto total
				 de las operaciones antes procesadas. */
				if (lineNumber == operacion ) {
					guardarDetalleEncabezado(tipoOperacionCuentaOrigen, conn);
				}
				
				
				logger.debug("Finalizado el proceso de insercion");
				return;
			}
			
			
			if(convenioCobro){
				
				/******************* kpalacios - 17/08/2016 ***********************
				 Desencadena validación de LIOF para el monto de la operación, 
				 esto cuando sea convenio cobro sin importar si es Detallado o Consolidado. */
				if (validarCuentaDebitoDefinida()) {
				}
				
				operacion++;
				logger.debug("Realizando insercion de operacion de Debito para un convenio de COBRO");
				logger.debug("operacion   "+operacion);
				logger.debug("parametros insert cuentaDebito5: "+cuentaDebito);
				logger.debug("parametros insert nombreDeCuenta5: "+nombreDeCuenta);
				if(nombreDeCuenta != null && !nombreDeCuenta.equals("") && !nombreDeCuenta.equals("null")) {					
					logger.debug("Entro en el nombreDeCuenta nombreDeCuenta nombreDeCuenta  "+nombreDeCuenta);
				}else if (nombreDeCuenta == null){
					logger.debug("Entro en el else nombreDeCuenta  "+nombreDeCuenta);
					nombreDeCuenta = "";			
				}
				sublote = lote+1;
				montoImpuesto = BigDecimal.ZERO;
				logger.debug("parametros insert tipoOperacion5: "+tipoOperacion);
				Object[]  params = new Object[]{
						instalacion, lote, operacion, cuenta != null ? cuenta.trim() : null, tipoOperacion, monto,
						adenda, fechaEstatus, estatus, idPago, nombreBenef,
						direccionBenef, email, nombreCuenta, sublote, "000000000000", nombreDeCuenta, montoImpuesto, codigoORefInt
					};
				
				
				logger.debug("parametros params.length4: "+params.length);
				logger.debug("********************************************");
				for (int i = 0; i < params.length; i++) {
					System.out.println("parametros params["+i+"]:  "+params[i]);
				}
				logger.debug("parametros insert detlote4: "+codigoORefInt);
				queryRunner.update(conn, INSERT_BS_DETALLE_PE, params);
				logger.debug("Registro agregado en Bs_DetallePE para la operacion de Debito");
				
				
				/******************* kpalacios - y19/08/2016 ***********************
				 Validación para insertar solamente 1 vez CuentaDebito, donde el valor monto es el monto total
				 de las operaciones antes procesadas. */
				if (lineNumber == operacion ) {
					guardarDetalleEncabezado(tipoOperacionCuentaOrigen, conn);
				}
				
				logger.debug("Finalizado el proceso de insercion");
				return;
			}
		}finally{
			DbUtils.closeQuietly(conn);
		}
	}
	
	/******************* kpalacios - 19/08/2020 ***********************
	 Validación para insertar solamente 1 vez CuentaDebito, donde el valor monto es el monto total
	 de las operaciones antes procesadas. */
	public void guardarDetalleEncabezado(int tipoOperacionCuentaOrigen, Connection conn) throws SQLException {
		logger.debug("Realizando insercion de operacion de monto total");					
		
		BigDecimal montoTotal = businessData.getEncabezadoData().getMontoTotal();
		logger.debug("MontoTotal [{}] "+montoTotal);
		nombreCuenta = businessData.getEncabezadoData().getNombreCuenta();
		estatus = businessData.getEncabezadoData().getEstatus();
		if(tipoOperacionCuentaOrigen == 1) {
			idPago = null;
			nombreDeCuenta = "";
		}
		Object[] params = new Object[]{
				instalacion, lote, operacion + 1, cuentaDebito != null ? cuentaDebito.trim() : null, tipoOperacionCuentaOrigen, montoTotal,
				null, fechaEstatus, estatus, idPago, null,
				null, null,null,sublote, null, nombreCuenta, BigDecimal.ZERO, "000000000000"
			};
		logger.debug("parametros params: "+params.length);
		for (int i = 0; i < params.length; i++) {
			System.out.println("parametros params[i]:  "+params[i]);
		}
		queryRunner.update(conn, INSERT_BS_DETALLE_PE, params);
		logger.debug("Registro agregado en Bs_DetallePE para la operacion de monto total");
	}
	
	public boolean validarCuentaDebitoDefinida(){
		logger.debug("Validando la cuenta de Debito provista en el archivo (si existe) o la cuenta definida segun convenio" );
		String cuentaSegunConvenio = businessData.getEncabezadoData().getCuentaDebitoCredito();
		logger.debug("parametros insert cuentaDebito7: "+cuentaDebito);	
		if(isEmptyString(cuentaDebito)){
			logger.debug("No se ha definido la cuenta de Debito/Credito en la linea.  Se toma la cuenta definida segun convenio: [{}]", cuentaSegunConvenio);
			cuentaDebito = cuentaSegunConvenio;
		}
		/* Puede parecer doble validacion, pero segun la logica definida 
		 en realidad estoy validando si la cuenta segun convenio esta definida.*/
		logger.debug("parametros insert cuentaDebito8: "+cuentaDebito);	
		if(isEmptyString(cuentaDebito)){
			logger.debug("Cuenta segun convenio NO esta definida.  Por lo tanto quedara con estatus = 97 (Cuenta de debito sin permiso)");
			estatus = 97;
			return false;
		}
		
		return true;
	}
	
	public boolean validarCuentaMultiplesCuentas(){
		logger.debug("Validando si el convenio es de multiples cuentas");
		boolean esMultiplesCuentas = businessData.getEncabezadoData().getMultiplesCuentas();
		if(!esMultiplesCuentas){
			logger.debug("Convenio de Multiples cuentas es false.");
			/* No es convenio multiples cuentas, ya tuvo que haber sido validado
			 por el metodo validarCuentaDebitoDefinida()*/
			return true;
		}
		logger.debug("Convenio Multiples Cuentas es verdadero.  Por lo tanto se validara si la cuenta se encuentra definida en 'BS_Convenio' primero, y si no en el listado de 'Bs_ConvenioCuenta'");
		
		Integer convenio = businessData.getInt(Vars.PM_CONVENIO);
		Integer tipoconvenio = businessData.getInt(Vars.PM_TIPOCONVENIO);
		Integer cliente = businessData.getInt(Vars.PM_CLIENTE);
		Connection conn = null;
		try{
			conn = businessData.getLocalizadorDeConexion().getConnection();
			/* AGREGADO 31-MARZO-2106
			 Busqueda de la cuentaDebito en BS_Convenio previo a buscar en BS_ConvenioCuenta
			 La Cuenta segun Bs_Convenio ya la tengo en la variable 'cuentaDebitoCredito' de 'EncabezadoData'*/
			logger.debug("parametros insert cuentaDebito9: "+cuentaDebito);	
			logger.debug("Validando la cuenta {} se encuentre definida en tabla 'Bs_Convenio' para Convenio: {}, Cliente: {} ", new Object[]{cuentaDebito, convenio, cliente});
			String ctaConvenio = (String) queryRunner.query(conn, "select Cuenta from BS_Convenio where Convenio = ? and Cliente = ? ", 
					new ScalarHandler("Cuenta"), new Object[]{convenio, cliente});
			logger.debug("Cuenta default definida en Bs_Convenio para Convenio: [{}], Cliente: [{}] se encontro Cuenta: [{}]", new Object[]{convenio, cliente, ctaConvenio});
			logger.debug("parametros insert cuentaDebito10: "+cuentaDebito);	
			if(ctaConvenio.trim().equals(cuentaDebito.trim())){
				logger.debug("La cuenta {} se encuentra definida en 'BS_Convenio' para el convenio {} ", cuentaDebito, convenio);
				return true;
			}
			logger.debug("parametros insert cuentaDebito11: "+cuentaDebito);	
			logger.debug("La cuenta {} no se encuentra en 'BS_Convenio', por lo que se buscara en 'BS_ConvenioCuenta' ", cuentaDebito);
			logger.debug("parametros insert cuentaDebito12: "+cuentaDebito);	
			Integer cant = (Integer)queryRunner.query(conn,"select count(Cuenta) from BS_ConvenioCuenta where TipoConvenio = ? and Convenio = ? and Cuenta = ?", new ScalarHandler(), tipoconvenio, convenio, cuentaDebito);
			if(cant <= 0){
				logger.debug("NO se encontro la cuenta definida en 'BS_ConvenioCuenta' para convenio de multiples cuentas. [{}]", businessData.getReferenceInfo());
				estatus = 97;
				return false;
			}
			logger.debug("La cuenta [{}] se encuentra definida en el listado de Bs_ConvenioCuenta", cuentaDebito);
			return true;
		}catch(SQLException e){
			e.printStackTrace();
			logger.error("Ocurrio un error al hacer la consulta de la cuenta [{}] en el registro 'Bs_ConvenioCuenta'. " + e.getMessage(), e);
			estatus = 7;
		}finally{
			DbUtils.closeQuietly(conn);
		}
		return false;
	}
	
	public boolean validarCuentasPolParticipante(){
		logger.debug("Iniciando validacion de las cuentas especificadas en el archivo segun convenio abierto/cerrado");
		int polParticipante = businessData.getEncabezadoData().getPolParticipante();
		Integer convenio = businessData.getInt(Vars.PM_CONVENIO);
		Integer tipoconvenio = businessData.getInt(Vars.PM_TIPOCONVENIO);
		Connection conn = null;
		logger.debug("polParticipante: [{}]", polParticipante);
		if(polParticipante == 1){
			try{
				conn = businessData.getLocalizadorDeConexion().getConnection();
				logger.debug("El convenio es cerrado por lo tanto se validara la cuenta [{}] que existe en 'BS_Participante' ", cuenta);
				/*kpalacios | 22/08/2016, Se aplico CAST al campo cuenta, ya que el valor de la variable que se el esta enviando no contiene los ceros a la izquierda.*/ 
				Map<String, Object> map = queryRunner.query(conn, "select MontoAutorizado, Estatus from Bs_Participante where TipoConvenio = ? and Convenio = ? and CAST(Cuenta AS NUMERIC) = ?", new MapHandler(), tipoconvenio, convenio, cuenta);
				if(map == null){
					logger.warn("La cuenta [{}] no se encuentra registrada en la tabla Bs_Participante. [{}]", cuenta, businessData.getReferenceInfo());
					logger.debug("Se registra el estatus = 4 (Empleado o Proveedor no registrado)");
					estatus = 4;
					return false;
				}
				
				logger.debug("Se encontraron resultados de Bs_Participante.  Procediendo a validar monto autorizado y estatus");
				Integer polParticipanteEstatus = (Integer) map.get("Estatus");
				Number polParticipanteMontoAutorizado = (Number) map.get("MontoAutorizado");
				
				if(polParticipanteEstatus!=1){
					logger.warn("La cuenta [{}] participante no se encuentra habilitada. Se coloca el registro con estatus = 2 (Participante deshabilitado)", cuenta);
					estatus = 2;
					return false;
				}
				
				BigDecimal montoAutorizado = new BigDecimal(polParticipanteMontoAutorizado.doubleValue());
				if(montoAutorizado.compareTo(monto)<0){
					logger.warn("La cuenta [{}] participante no cuenta con un monto autorizado ({}) superior al monto indicado en el archivo: ({}). Se coloca estatus = 3 (Monto no Autorizado)", new Object[]{cuenta, montoAutorizado, monto});
					estatus = 3;
					return false;
				}
				
				logger.debug("Monto y Estatus autorizados.  La cuenta puede continuar con el proceso de validaciones");
				return true;
				
			}catch(SQLException e){
				e.printStackTrace();
				logger.error("Error al validar cuenta [{}] en Bs_Participante", cuenta, e);
				estatus = 7;
				return false;
			}finally{
				DbUtils.closeQuietly(conn);
			}
		}
		return true;
	}
	
	public boolean validarCuentaServicioEntorno(){
		logger.debug("Validando cuenta [{}] contra servicio de Entorno ...", cuenta);
		Respuesta resp = null;
		resp = new Respuesta();
			Respuesta respNombreCuenta = invocadorEntorno.invocarInformacionNombreCuenta(cuenta);
			
			if(respNombreCuenta == null || respNombreCuenta.getCodigo() != 0){
				logger.warn("Se encontro un error en la validacion de la cuenta desde PagoElectronico contra el servicio fabricaCoreBanca/srvInfoNombreCuenta. ");
				estatus = 7;
				return false;
			}
			respNombreCuenta.primerContenedor();
			if(!respNombreCuenta.siguiente()){
				logger.warn("No se encontraron resultados para la consulta de la cuenta desde PagoElectronico [{}] contra servicio fabricaCoreBanca/srvInfoNombreCuenta", cuenta);
				estatus = 7;
				return false;
			}
			
			nombreDeCuenta = respNombreCuenta.obtenerString("nombreCuenta");
			logger.info("nombreDeCuenta "+nombreDeCuenta);
			if(isEmptyString(nombreDeCuenta)){
				logger.warn("No se ha definido el nombre completo para la cuenta [{}] , desde PagoELectronico segun el servicio fabricaCoreBanca/srvInfoNombreCuenta ", cuenta);
				estatus = 7;
				return false;
			}
			logger.debug("Se ha recuperado el valor de nombre completo desde PagoElectronico [{}] del servicio fabricaCoreBanca/srvInfoNombreCuenta", nombreDeCuenta);
	
		return true;
	}
	
	public void readValues()
			throws LineProcessException {		
		List<CampoLinea> campos = formatoLinea.getCampos();
				
		cuenta = String.valueOf(buscarValorColumna("Cuenta", campos));
		monto = (BigDecimal) buscarValorColumna("Monto", campos);
		adenda = String.valueOf(buscarValorColumna("Adenda", campos));
		idPago = String.valueOf(buscarValorColumna("IDPago", campos));
		nombreBenef = String.valueOf(buscarValorColumna("NombreBenef", campos));
		direccionBenef = String.valueOf(buscarValorColumna("DireccionBenef", campos));
		email = String.valueOf(buscarValorColumna("Email", campos));
		cuentaDebito = String.valueOf(buscarValorColumna("CuentaDebito", campos));
		codigoORefInt = String.valueOf(buscarValorColumna("CodigoORefInt", campos));
		
		logger.debug("Entro en el email " + email + " adenda " + adenda + " idPago " + idPago);
		logger.debug("Entro en el nombreBenef "+nombreBenef + " cuenta  "+cuenta);
		if(cuenta != null && !cuenta.equals("") && !cuenta.equals("null")) {
			logger.debug("Entro en el == cuenta cuenta  "+cuenta);
		}else if (cuenta.equals("null")){
			logger.debug("Entro en el cuenta cuenta  "+cuenta);
			cuenta = null;			
		}else if (cuenta == null){
			logger.debug("Entro en el else cuenta  "+cuenta);
			cuenta = null;			
		}
		if(email != null && !email.equals("") && !email.equals("null")) {
			logger.debug("Entro en el == email email  "+email);
		}else if (email.equals("null")){
			logger.debug("Entro en el email email  "+email);
			email = null;
		}else if (email == null){
			logger.debug("Entro en el else email  "+email);
			email = null;			
		}
		
		if(adenda != null && !adenda.equals("") && !adenda.equals("null")) {
			logger.debug("Entro en el == adenda adenda  "+adenda);
		}else if (adenda.equals("null")){
			logger.debug("Entro en el adenda adenda  "+adenda);
			adenda = null;
		}else if (adenda == null){
			logger.debug("Entro en el else adenda  "+adenda);
		}
		
		if(idPago != null && !idPago.equals("") && !idPago.equals("null")) {
			logger.debug("Entro en el == idPago idPago  "+idPago);
		}else if (idPago.equals("null")){
			logger.debug("Entro en el idPago idPago  "+idPago);
			idPago = null;
		}else if (idPago == null){
			logger.debug("Entro en el else idPago  "+idPago);
		}
		
		if(nombreBenef != null && !nombreBenef.equals("") && !nombreBenef.equals("null")) {
			logger.debug("Entro en el == nombreBenef nombreBenef  "+nombreBenef);
		}else if (nombreBenef.equals("null")){
			logger.debug("Entro en el nombreBenef nombreBenef  "+nombreBenef);
			nombreBenef = null;
		}else if (nombreBenef == null){
			logger.debug("Entro en el else nombreBenef  "+nombreBenef);
		}
		
		
		logger.debug("codigoORefInt["+codigoORefInt+"]");
		
		if(codigoORefInt==null){
			codigoORefInt="000000000000";
		}else if(codigoORefInt!=null){
			if("".equals(codigoORefInt.trim()) || "null".equals(codigoORefInt.trim())){
				codigoORefInt="000000000000";
			}
		}
		montoImpuesto = (BigDecimal) buscarValorColumna("MontoImpuesto", campos);
		
	}
	
	public void setupValues(){
		EncabezadoData data = businessData.getEncabezadoData();
		instalacion = data.getInstalacion();
		lote = data.getLote();
		aplicacionDebitoHost = data.getAplicacionDebitoHost();
		fechaEstatus = new Timestamp(System.currentTimeMillis());
	}
	
	public void clearValues(){
		instalacion = 0;
		lote = 0;
		cuenta = null;
		monto = null;
		adenda = null;
		fechaEstatus = null;
		estatus = 0;
		idPago = null;
		nombreBenef = null;
		direccionBenef = null;
		email = null;
		cuentaDebito = null;
		nombreDeCuenta = null; 
		codigoORefInt = null;
		montoImpuesto = null;
		sublote = 0;
	}

}