package com.ofbox.davivienda.h2h.proc.file.format;

public class LineProcessException extends ProcessException {

	private static final long serialVersionUID = 1L;

	public LineProcessException() {
		super();
		
	}


	public LineProcessException(String message, Throwable cause) {
		super(message, cause);
		
	}

	public LineProcessException(String message) {
		super(message);
		
	}

	public LineProcessException(Throwable cause) {
		super(cause);
		
	}

	
	
}
