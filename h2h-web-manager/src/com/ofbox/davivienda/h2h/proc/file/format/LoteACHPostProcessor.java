package com.ofbox.davivienda.h2h.proc.file.format;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;

import com.hsbc.sv.desarrollo.contenedores.Respuesta;
import com.ofbox.davivienda.h2h.proc.Vars;

public class LoteACHPostProcessor extends Processor{
	
	public static final String UPDATE_LOTE_ACH_LIOF = "update Bs_LoteACH set MontoTotal = ?, NumOperaciones = ?, Estatus = ?, FechaEstatus = ?, MontoImpuesto = ? where CodLote = ? and Instalacion = ? and Lote = ? ";
	public static final String UPDATE_LOTE_ACH = "update Bs_LoteACH set MontoTotal = ?, NumOperaciones = ?, Estatus = ?, FechaEstatus = ? where CodLote = ? and Instalacion = ? and Lote = ? ";

	public LoteACHPostProcessor(BusinessData businessData,
			FormatoLinea formatoLinea) {
		super(businessData, formatoLinea);
	}

	@Override
	public void process(String line) throws ProcessException {
		logger.debug("Iniciando post procesamiento del lote. Se actualizaran los valores del registro de BS_LoteACH");
		
		int estatusLote = 2;
		BigDecimal montoMaximo = businessData.getAsBigDecimal(Vars.PM_MONTO_MAXIMO_PROCESAR);
		Integer instalacion = businessData.getEncabezadoData().getInstalacion();
		Integer lote = businessData.getEncabezadoData().getLote(); 
		Integer numOperaciones = businessData.getEncabezadoData().getCantidadOperaciones();
		Timestamp fechaEstatus = new Timestamp(System.currentTimeMillis());
		BigDecimal montoTotalLote = businessData.getEncabezadoData().getMontoTotal();
		Long codLote = businessData.getEncabezadoData().getCodLote();
		Integer cliente = businessData.getInt(Vars.PM_CLIENTE);
				
		String cuentaDebito = null;
		BigDecimal montoImpuesto = null;
		
		/******************* kpalacios - 12/08/2016 ***********************
		Se agreg� el siguiente if y se coloc� el estado 28 (C_LOTEEST_FORMATO_INVALIDO_ARCHIVO, constante de BE), 
		esto para cuando archivo finalizo su proceso, sin embargo no todas las l�neas se procesaron correctamente. */		
		if(businessData.getHandler().getLineasProcesadas() == 0){
			logger.debug("Archivo NO finalizo su proceso, Formato invalido de archivo.");
			estatusLote = 28;
		}else{
			
			 /*Si posee encabezado, entonces hay una fecha y hora programados*/
			if(businessData.isPoseeEncabezado()){
				logger.debug("El lote posee encabezado. Por lo tanto se asume que hay una fecha y hora programadas.  El estado del lote quedara con valor '4' (En Espera)");
				estatusLote = 4;
			}
			
			if("N".equalsIgnoreCase(businessData.getString(Vars.PM_PROCESAMIENTO_SUPERUSUARIO))){
				logger.debug("La configuracion indica procesamiento de super usuario con 'No'.  El estado de lote quedara en valor 51 (Pendiente de autorizacion)");
				estatusLote = 51; 
			}		
			
			/******************* kpalacios - 23/08/2016 ***********************
			 Desencadena validaci�n de LIOF para el monto total del lote, esto cuando sea de tipo Consolidado. */
			System.out.println("businessData.getEncabezadoData().getAplicacionDebitoHost()...... "+businessData.getEncabezadoData().getAplicacionDebitoHost());
			if (businessData.getEncabezadoData().getAplicacionDebitoHost() == 2) {
				if (validarCuentaDebitoDefinida(cuentaDebito, estatusLote)) {
					montoImpuesto = invocarLIOF(cuentaDebito, montoTotalLote); 
				}
			}
			
			if(montoMaximo!=null && montoMaximo.compareTo(BigDecimal.ZERO) > 0){
				if(montoTotalLote.compareTo(montoMaximo) > 0){
					/******************* kpalacios - 07/10/2016 ***********************
					Se modific� el estado a 27 (C_LOTEEST_SUPERA_LIMITE) ya que el valor 70 no existe en las constantes de BE */
					estatusLote = 27;  
					businessData.setResultadoMensaje("ERROR,El monto total del lote indicado en el archivo supera el monto total configurado para el convenio");
				}
			}
		}		
		
		Connection conn = null;
		QueryRunner runner = new QueryRunner();
		boolean autocommit = true;
		try {
			conn = businessData.getLocalizadorDeConexion().getConnection();
			autocommit = conn.getAutoCommit();
			conn.setAutoCommit(false);
			
			if (montoImpuesto == null) {
				Object[] parameters = {	montoTotalLote, numOperaciones, estatusLote, fechaEstatus, codLote, instalacion, lote };
				runner.update(conn, UPDATE_LOTE_ACH, parameters);
			} else {
				Object[] parameters_liof = { montoTotalLote, numOperaciones, estatusLote, fechaEstatus, montoImpuesto , codLote, instalacion, lote };
				runner.update(conn, UPDATE_LOTE_ACH_LIOF, parameters_liof);
			}			
			logger.info("Actualizados los valores del registro de Lote del archivo ");
			
			logger.debug("Actualizando valor de siguiente instalacion de Bs_Instalacion");
			Integer siguienteLote = lote + 1;
			runner.update(conn, "update Bs_Instalacion set SiguienteLoteACH = ? where Instalacion = ? and Cliente = ?",
					new Object[] { siguienteLote, instalacion, cliente } );
			conn.commit();
			logger.debug("Se han actualizado el valor de la siguiente instalacion hacia el valor: {}", siguienteLote);
			
			
		} catch (SQLException e) {
			if(conn!=null){
				try{
					conn.rollback();
				}catch(Exception ignored){}
			}
			e.printStackTrace();
			logger.error("Error al realizar actualizacion del Lote. " + businessData.getReferenceInfo() + " -> " + e.getMessage(),e);
			throw new LineProcessException("Error al actualizar los valores totales del Lote despues de procesar las lineas. " + e.getMessage(), e);
		}finally{
			if(conn!=null){
				try{ conn.setAutoCommit(autocommit); }catch(SQLException e){ }
			}
			DbUtils.closeQuietly(conn);
		}
		
	}
	
	/**
	 * Valida cuenta debito a utilizar para el c�lculo del LIOF 
	 * 
	 * @author kpalacios | 23/08/2016
	 * @return
	 */
	public boolean validarCuentaDebitoDefinida(String cuentaDebito, int estatusLote){
		logger.debug("Validando la cuenta de Debito provista en el archivo (si existe) o la cuenta definida segun convenio" );
		String cuentaSegunConvenio = businessData.getEncabezadoData().getCuentaDebitoCredito();
		if(isEmptyString(cuentaDebito)){
			logger.debug("No se ha definido la cuenta de Debito/Credito en la linea.  Se toma la cuenta definida segun convenio: [{}]", cuentaSegunConvenio);
			cuentaDebito = cuentaSegunConvenio;
		}
		 /*Puede parecer doble validacion, pero seg�n la logica definida 
		 en realidad estoy validando si la cuenta segun convenio esta definida*/
		if(isEmptyString(cuentaDebito)){
			logger.debug("Cuenta seg�n convenio NO esta definida.  Por lo tanto quedara con estatus = 106 (Error validando cuentas)");
			estatusLote = 106;
			return false;
		}
		
		return true;
	}

	/**
	 * Invoca el srv de entorno TRANSACCION_IMPUESTO_OPERACIONES_FINANCIERAS 
	 * para realizar el calculo del LIOF.
	 * 
	 * @author kpalacios | 23/08/2016
	 */
	public BigDecimal invocarLIOF(String cuentaDebito, BigDecimal montoTotal) {
		InvocadorJ2Entorno invocadorEntorno = new InvocadorJ2Entorno();
		BigDecimal montoImpuesto = null;
		try {
			Respuesta respImpuesto = invocadorEntorno.invocarImpuestosOpeFinancieras(cuentaDebito, "", montoTotal);

			String datosXML = respImpuesto.getDatosXML();
			Document docResp = DocumentHelper.parseText(datosXML);
			Integer codigo = Integer.valueOf(docResp.selectSingleNode("/respuestaEntorno/header/codigo").getText());

			if (codigo == 0) {
				logger.info("Servicio fabricaESB / TRANSACCION_IMPUESTO_OPERACIONES_FINANCIERAS respondio correctamente. ");

				String impuesto = docResp.selectSingleNode("/respuestaEntorno/body/contenedor/contenedor/montoImpuesto").getText();
				montoImpuesto = BigDecimal.valueOf(Double.valueOf((String) ("".equals(impuesto) ? "0.00" : impuesto)));
				
				System.out.println("LoteACHPostProcessor - montoImpuesto.... "+montoImpuesto);
			} else {
				logger.error("El servicio fabricaESB / TRANSACCION_IMPUESTO_OPERACIONES_FINANCIERAS respondio codigo distintos a cero ");
			}
		} catch (Exception e) {
			logger.error("Error tratando de recorrer XML del servicio fabricaESB / TRANSACCION_IMPUESTO_OPERACIONES_FINANCIERAS. ");
			e.printStackTrace();
		}
		return montoImpuesto;
	}

}
