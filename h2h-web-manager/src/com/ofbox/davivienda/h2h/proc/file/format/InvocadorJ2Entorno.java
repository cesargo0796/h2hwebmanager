package com.ofbox.davivienda.h2h.proc.file.format;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hsbc.sv.desarrollo.J2Entorno;
import com.hsbc.sv.desarrollo.contenedores.Peticion;
import com.hsbc.sv.desarrollo.contenedores.Respuesta;
import com.hsbc.sv.desarrollo.interconexion.FabricaServicios;
import com.hsbc.sv.j2entorno.core.FabricaEntornoCore;

/**
 * Clase auxiliar que sirve para encapsular la invocacion 
 * hacia el servicio de Entorno que permite realizar
 * la validacion del estatus de la cuenta contra la plataforma
 * de IBS
 *
 */
public class InvocadorJ2Entorno {

	private static final String NOMBRE_FABRICA = "fabricaCoreBanca";
	private static final String NOMBRE_SERVICIO_INFO_CUENTA = "srvInformacionCuentaDeposito";
	private static final String NOMBRE_SERVICIO_INFO_CLIENTE = "srvInfoCliente";
	private static final String NOMBRE_CLIENTE_ENTORNO = "OperadorH2H";
	private static final String NOMBRE_SERVICIO_NOMBRE_CUENTA = "srvInfoNombreCuenta";
	private static final String NOMBRE_SERVICIO_NOMBRE_TARJETA = "srvInfoNombreTDC";
	private static final String NOMBRE_SERVICIO_NOMBRE_PRESTAMO = "srvInfoNombrePrestamos";

	/*kpalacios | 17/08/2016 fabrica y srv a utilizar para la evaluaci�n del monto de impuestos*/
	private static final String NOMBRE_FABRICAESB = "fabricaESB";
	private static final String TRANSACCION_IMPUESTO_OPERACIONES_FINANCIERAS = "TRANSACCION_IMPUESTO_OPERACIONES_FINANCIERAS";
	private static final String TRANSACCION_CONS_H2H = "TRANSACCION_CONS_H2H";
	private static final String TRANSACCION_DETA_H2H = "TRANSACCION_DETA_H2H";
	
	Logger logger = LoggerFactory.getLogger(InvocadorJ2Entorno.class);
	
	public InvocadorJ2Entorno(){
		try {
			if (J2Entorno.e() == null || J2Entorno.e().fabricas() == null || J2Entorno.e().fabricas().size() <= 1) {
				arrancarEntorno();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
	private void arrancarEntorno(){
		logger.info("Creando fabricaEntornoCore");

		FabricaEntornoCore f = new FabricaEntornoCore();
		J2Entorno.e().setIniciado(false);
		
		logger.info("Arrancando cliente de entorno...");
		f.arrancarJ2EntornoCliente(NOMBRE_CLIENTE_ENTORNO);
		System.out.println("************************");
		System.out.println("DATOS FABRICA CARGADA: ");
		System.out.println("Aplicaci�n Cliente: "+f.getAplicacionCliente());
		System.out.println("Clase Implementaci�n: "+f.getClaseImplementacion());
		System.out.println("Descripci�n: "+f.getDescripcion());
		System.out.println("Identificador: "+f.getIdentificador());
		System.out.println("Ubicaci�n Jar: " + f.getUbicacionJar());
		System.out.println("Identificador entorno: " + f.identificadorEntorno());
		System.out.println("************************");
		logger.info("Cliente de entorno inicializado");
	}
	
	/**
	 * Valida que la cuenta tenga un estatus valido segun servicios del core bancario
	 * @param cuentaValidar
	 * @return
	 */
	public Respuesta invocarInformacionCuentaDeposito(String cuentaValidar){

		if(J2Entorno.e() == null || J2Entorno.e().fabricas() == null || J2Entorno.e().fabricas().size() <= 1) {
			logger.info("Cliente de entorno no inicializado... ");
			arrancarEntorno();
		}
		logger.info("Obteniendo fabrica {}", NOMBRE_FABRICA);
		FabricaServicios fabrica = J2Entorno.e().obtenerFabrica(NOMBRE_FABRICA);
		if(fabrica == null){
			logger.error("ERROR !!! no es posible obtener fabrica '{}' de cliente de entorno.  Validar inicializacion del cliente en archivos de log. ", NOMBRE_FABRICA);
			return null;
		}
		
		Peticion p = new Peticion();
		Respuesta resp = null;
		p.agregarParametro("cuenta", cuentaValidar);
		logger.info("Invocando servicio '{}' de fabrica '{}'", NOMBRE_SERVICIO_INFO_CUENTA, NOMBRE_FABRICA);
		
		resp = fabrica.obtenerDatos(p, null, NOMBRE_SERVICIO_INFO_CUENTA, null);

		return resp;
	}
	
	public Respuesta invocarInformacionCliente(String cliente){
		
		if(J2Entorno.e() == null || J2Entorno.e().fabricas() == null || J2Entorno.e().fabricas().size() <= 1) {
			logger.info("Cliente de entorno no inicializado... ");
			arrancarEntorno();
		}
		logger.info("Obteniendo fabrica {} ", NOMBRE_FABRICA);
		FabricaServicios fabrica = J2Entorno.e().obtenerFabrica(NOMBRE_FABRICA);
		if(fabrica == null){
			logger.error("ERROR !!! no es posible obtener fabrica '{}' de cliente de entorno.  Validar inicializacion del cliente en archivos de log. ", NOMBRE_FABRICA);
			return null;
		}
		
		Peticion p = new Peticion();
		p.agregarParametro("niu", cliente);
		logger.info("Invocando servicio '{}' de fabrica '{}'", NOMBRE_SERVICIO_INFO_CLIENTE, NOMBRE_FABRICA);
		Respuesta resp = null;
		resp = fabrica.obtenerDatos(p, null, NOMBRE_SERVICIO_INFO_CLIENTE, null);
		
		return resp;
	}
	
	public Respuesta invocarInformacionNombreCuenta(String cuenta){
		logger.info("Invocando Servicios de informacion de cuenta ... ");
		if(J2Entorno.e() == null || J2Entorno.e().fabricas() == null || J2Entorno.e().fabricas().size() <= 1) {
			logger.info("Cliente de entorno no inicializado... ");
			arrancarEntorno();
		}
		logger.info("Obteniendo fabrica {} ", NOMBRE_FABRICA);
		FabricaServicios fabrica = J2Entorno.e().obtenerFabrica(NOMBRE_FABRICA);
		if(fabrica == null){
			logger.error("ERROR !!! no es posible obtener fabrica '{}' de cliente de entorno.  Validar inicializacion del cliente en archivos de log. ", NOMBRE_FABRICA);
			return null;
		}
		int cuentaLength = cuenta.length();
		Peticion p = new Peticion();
		p.agregarParametro("cuenta", cuenta);
		p.agregarParametro("longCuenta", cuentaLength);
		Respuesta resp = null;
				logger.info("Invocando servicio '{}' de fabrica '{}'", NOMBRE_SERVICIO_NOMBRE_CUENTA, NOMBRE_FABRICA);
				resp = fabrica.obtenerDatos(p, null, NOMBRE_SERVICIO_NOMBRE_CUENTA, null);

		return resp;
	}
	
	
	public Respuesta invocarInformacionNombreCuenta(String cuenta, int tipoCuenta){
		logger.info("Invocando Servicios de informacion de cuenta ... ");
		if(J2Entorno.e() == null || J2Entorno.e().fabricas() == null || J2Entorno.e().fabricas().size() <= 1) {
			logger.info("Cliente de entorno no inicializado... ");
			arrancarEntorno();
		}
		logger.info("Obteniendo fabrica {} ", NOMBRE_FABRICA);
		FabricaServicios fabrica = J2Entorno.e().obtenerFabrica(NOMBRE_FABRICA);
		if(fabrica == null){
			logger.error("ERROR !!! no es posible obtener fabrica '{}' de cliente de entorno.  Validar inicializacion del cliente en archivos de log. ", NOMBRE_FABRICA);
			return null;
		}
		int cuentaLength = cuenta.length();
		logger.info("TIPO DE CUENTA :: "+tipoCuenta);
		Peticion p = new Peticion();
		p.agregarParametro("cuenta", cuenta);
		p.agregarParametro("longCuenta", cuentaLength);
		Respuesta resp = null;
		switch(tipoCuenta) 
		{
			case 1:
				logger.info("Invocando servicio '{}' de fabrica '{}'", NOMBRE_SERVICIO_NOMBRE_CUENTA, NOMBRE_FABRICA);
				resp = fabrica.obtenerDatos(p, null, NOMBRE_SERVICIO_NOMBRE_CUENTA, null);
				break;
			case 2:
				logger.info("Invocando servicio '{}' de fabrica '{}'", NOMBRE_SERVICIO_NOMBRE_CUENTA, NOMBRE_FABRICA);
				resp = fabrica.obtenerDatos(p, null, NOMBRE_SERVICIO_NOMBRE_CUENTA, null);
				break;
			case 3:
				logger.info("Invocando servicio '{}' de fabrica '{}'", NOMBRE_SERVICIO_NOMBRE_PRESTAMO, NOMBRE_FABRICA);
				resp = fabrica.obtenerDatos(p, null, NOMBRE_SERVICIO_NOMBRE_PRESTAMO, null);
				break;
			case 4:
				logger.info("Invocando servicio '{}' de fabrica '{}'", NOMBRE_SERVICIO_NOMBRE_TARJETA, NOMBRE_FABRICA);
				resp = fabrica.obtenerDatos(p, null, NOMBRE_SERVICIO_NOMBRE_TARJETA, null);
				break;
			default:
				break;
		}
		return resp;
	}
	
	
	/**
	 * Invoca el srv(TRANSACCION_IMPUESTO_OPERACIONES_FINANCIERAS) de J2Entorno que realiza el c�lculo para el LIOF
	 * 
	 * @author kpalacios | 17/08/2016
	 * 
	 * @param ctaOrigen
	 * @param ctaDestino
	 * @param monto
	 * @return
	 */
	public Respuesta invocarImpuestosOpeFinancieras(String ctaOrigen, String ctaDestino, BigDecimal monto){
		if(J2Entorno.e() == null || J2Entorno.e().fabricas() == null || J2Entorno.e().fabricas().size() <= 1) {
			logger.info("Cliente de entorno no inicializado... ");
			arrancarEntorno();
		}
		logger.info("Obteniendo fabrica {} ", NOMBRE_FABRICAESB);
		FabricaServicios fabrica = J2Entorno.e().obtenerFabrica(NOMBRE_FABRICAESB);
		if(fabrica == null){
			logger.error("ERROR !!! no es posible obtener fabrica '{}' de cliente de entorno.  Validar inicializacion del cliente en archivos de log. ", NOMBRE_FABRICAESB);
			return null;
		}
		
		System.out.println("XML Petici�n... "+xmlImpuestoOpeFinancieras(ctaOrigen, ctaDestino, monto));
		Peticion p = new Peticion();
		p.setDatosXML(xmlImpuestoOpeFinancieras(ctaOrigen, ctaDestino, monto));
		logger.info("Invocando servicio '{}' de fabrica '{}'", TRANSACCION_IMPUESTO_OPERACIONES_FINANCIERAS, NOMBRE_FABRICAESB);
		
		Respuesta resp = fabrica.obtenerDatos(p, null, TRANSACCION_IMPUESTO_OPERACIONES_FINANCIERAS, null);
		
		return resp;
	}
	
	/**
	 * Petici�n XML para el srv TRANSACCION_IMPUESTO_OPERACIONES_FINANCIERAS
	 * 
	 * @author kpalacios | 17/08/2016
	 * 
	 * @param ctaOrigen
	 * @param ctaDestino
	 * @param monto
	 * @return
	 */
	public String xmlImpuestoOpeFinancieras(String ctaOrigen, String ctaDestino, BigDecimal monto){
		String tipoTransac = "".equals(ctaDestino) ? TRANSACCION_CONS_H2H : TRANSACCION_DETA_H2H;
		return "<transaccion>"
				+ "    <tipoTransaccion>"+ tipoTransac +"</tipoTransaccion>"
				+ "    <ctaOrigen>"+ctaOrigen+"</ctaOrigen>"
				+ "    <ctaDestino>"+ctaDestino+"</ctaDestino>"
				+ "    <monto>"+monto+"</monto>"
				+ "    <tipoProceso>I</tipoProceso>"
				+ "    <flgReversa/>"
				+ "    <canal>BEP</canal>"
				+ "</transaccion>";
	}
	
}
