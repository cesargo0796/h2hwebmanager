package com.ofbox.davivienda.h2h.proc.file.format;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.MapHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import com.ofbox.davivienda.h2h.proc.ProcessLogHandler;
import com.ofbox.davivienda.h2h.proc.Vars;

public class LoteACHPreProcessor extends Processor{

	public LoteACHPreProcessor(BusinessData businessData,
			FormatoLinea formatoLinea) {
		super(businessData, formatoLinea);
	}

	@Override
	public void process(String line) throws ProcessException {
		ProcessLogHandler handler = businessData.getHandler();
		int convenio = businessData.getInt(Vars.PM_CONVENIO);
		int tipoconvenio = businessData.getInt(Vars.PM_TIPOCONVENIO);
		int cliente = businessData.getInt(Vars.PM_CLIENTE);
		Object[] params = new Object[]{cliente, convenio, tipoconvenio};
		logger.debug("Iniciando validacion de estado de Cliente : {}, Convenio : {}, TipoConvenio : {} ", params);
		handler.log("Iniciando validaciones de Instalacion, Convenio y estatus de Convenio");
		Connection conn = null;
		try{
			conn = businessData.getLocalizadorDeConexion().getConnection();
			EncabezadoData data = businessData.getEncabezadoData();
			QueryRunner queryRunner = new QueryRunner();
			
			Number instalacion = (Number)queryRunner.query(conn, "select Instalacion from Bs_Instalacion where Cliente = ?", new ScalarHandler(), cliente);
			logger.debug("Instalacion encontrada: {}", instalacion);
			handler.log("Se ha encontrado la instalacion " + instalacion + " para el Cliente " + cliente);
			data.setInstalacion(instalacion.intValue());
			
			Map<String, Object> mapResult = queryRunner.query(conn, "select * from BS_ConvenioACH where Cliente = ? and Convenio = ? and TipoConvenio = ?", new MapHandler(), params);
			Integer estatus = (Integer) mapResult.get("Estatus");
			if(estatus != 1){
				String nombreCliente = businessData.getString(Vars.PM_NOMBRE_CLIENTE);
				String message = "El estado del Convenio " + convenio + " del Cliente " + nombreCliente + " no se encuentra habilitado";
				logger.warn(message);
				handler.log(message);
				throw new ProcessException(message);
				
			}
			logger.debug("La validacion del estado del Convenio {} ha dado como resultado el estado {}.  El convenio se encuentra Habilitado.", convenio, estatus);
			handler.log("El convenio se encuentra habilitado");
			
			data.setTipoLote(2); 
			data.setMultiplesCuentas(((Boolean)mapResult.get("MultiplesCuentas")).booleanValue());
			String cuenta = (String)mapResult.get("Cuenta");
			data.setCuentaDebitoCredito(cuenta);
			
			
			data.setPolParticipante(((Number)mapResult.get("PolParticipante")).intValue());
			
			logger.debug("Finaliza la obtencion/validacion de reglas de negocio de la tabla Bs_ConvenioACH para Cliente : {}, Convenio : {}, TipoConvenio : {}", params);
			
		}catch(SQLException e){
			logger.error("Error de SQL al validar estado de convenio en tabla BS_Convenio: " + e.getMessage(), e);
			handler.log("ERROR en consulta de estado de convenio");
			handler.log(e.getMessage());
			handler.setLastException(e);
			throw new ProcessException("Error en validacion de estado de Convenio. ", e);
		}finally{
			DbUtils.closeQuietly(conn);
		}
	}

}
