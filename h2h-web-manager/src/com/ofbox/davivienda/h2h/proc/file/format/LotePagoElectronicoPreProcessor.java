package com.ofbox.davivienda.h2h.proc.file.format;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.MapHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ofbox.davivienda.h2h.proc.ProcessLogHandler;
import com.ofbox.davivienda.h2h.proc.Vars;

public class LotePagoElectronicoPreProcessor extends Processor{

	Logger logger = LoggerFactory.getLogger(LotePagoElectronicoPreProcessor.class);
	
	public LotePagoElectronicoPreProcessor(BusinessData businessData,
			FormatoLinea formatoLinea) {
		super(businessData, formatoLinea);
	}

	@Override
	public void process(String line) throws ProcessException {
		int convenio = businessData.getInt(Vars.PM_CONVENIO);
		int tipoconvenio = businessData.getInt(Vars.PM_TIPOCONVENIO);
		int cliente = businessData.getInt(Vars.PM_CLIENTE);
		Object[] params = new Object[]{cliente, convenio, tipoconvenio};
		logger.debug("Iniciando validacion de estado de Cliente : {}, Convenio : {}, TipoConvenio : {} ", params);
		ProcessLogHandler handler = businessData.getHandler();
		handler.log("Iniciando validaciones de Instalacion y estatus de convenio ");
		Connection conn = null;
		try{
			conn = businessData.getLocalizadorDeConexion().getConnection();
			QueryRunner queryRunner = new QueryRunner();
			
			Number instalacion = (Number)queryRunner.query(conn,"select Instalacion from Bs_Instalacion where Cliente = ?", new ScalarHandler(), cliente);
			logger.debug("Instalacion encontrada: {}", instalacion);
			handler.log("Se ha encontrado la Instalacion " + instalacion + " para el cliente " + cliente);
			businessData.getEncabezadoData().setInstalacion(instalacion.intValue());
			
			Map<String, Object> mapResult = queryRunner.query(conn,"select * from BS_Convenio where Cliente = ? and Convenio = ? and TipoConvenio = ?", new MapHandler(), params);
			if(mapResult == null){
				String nombreCliente = businessData.getString(Vars.PM_NOMBRE_CLIENTE);
				handler.log("No fue posible encontrar el Convenio asociado correctamente al cliente " + nombreCliente + ".  Convenio: " + convenio);
				throw new ProcessException("No se encontro el Convenio asociado correctamente al cliente: " + nombreCliente + ".  Convenio: " + convenio);
			}
			
			Integer estatus = (Integer) mapResult.get("Estatus");
			if(estatus != 1){
				String nombreCliente = businessData.getString(Vars.PM_NOMBRE_CLIENTE);
				handler.log("ADVERTENCIA: El estado del Convenio " + convenio + " no se encuentra habilitado.");
				throw new ProcessException("El estado del Convenio " + convenio + " del Cliente " + nombreCliente + " no se encuentra habilitado");
			}
			logger.debug("La validacion del estado del Convenio {} ha dado como resultado el estado {}.  El convenio se encuentra Habilitado.", convenio, estatus);
			handler.log("Validacion de convenio indica que el estatus del convenio se encuentra Habilitado");
			EncabezadoData data = businessData.getEncabezadoData();
			data.setTipoLote(((Number)mapResult.get("Categoria")).intValue());
			data.setMultiplesCuentas(((Boolean)mapResult.get("MultiplesCuentas")).booleanValue());
			String cuenta = (String)mapResult.get("Cuenta");
			data.setCuentaDebitoCredito(cuenta);
			String nombreCuenta = (String)mapResult.get("NombreCta");
			logger.debug("LotePagoElectronicoPreProcessor nombreCuenta nombreCuenta nombreCuenta  "+nombreCuenta);
			data.setNombreCuenta(nombreCuenta);
			data.setEstatus(estatus);
			
			
			data.setPolParticipante(((Number)mapResult.get("PolParticipante")).intValue());
			
			logger.debug("Finaliza la obtencion/validacion de reglas de negocio de la tabla Bs_Convenio para Cliente : {}, Convenio : {}, TipoConvenio : {}", params);
			
		}catch(SQLException e){
			logger.error("Error de SQL al validar estado de convenio en tabla BS_Convenio: " + e.getMessage(), e);
			throw new ProcessException("Error en validacion de estado de Convenio. ", e);
		}finally{
			DbUtils.closeQuietly(conn);
		}
	}

}
