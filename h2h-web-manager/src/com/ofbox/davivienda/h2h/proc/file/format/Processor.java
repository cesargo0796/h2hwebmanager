package com.ofbox.davivienda.h2h.proc.file.format;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ofbox.davivienda.h2h.proc.Vars;

public abstract class Processor {

	Logger logger = LoggerFactory.getLogger(Processor.class);
	protected BusinessData businessData;
	protected FormatoLinea formatoLinea;

	public Processor(BusinessData businessData, FormatoLinea formatoLinea) {
		super();
		this.businessData = businessData;
		this.formatoLinea = formatoLinea;
	}
	
	public abstract void process(String line) throws ProcessException;

	
	protected void parseLine(String line){
		String[] values = null;
		List<CampoLinea> campos = formatoLinea.getCampos();
		if(!formatoLinea.isCamposLongitudesFijas()){
			logger.debug("Procesando linea en formato de campos separados por caracter...");
			values = line.split(formatoLinea.getCaracterSeparador());
			for(int i=0; i<values.length; i++){
				logger.debug("Buscando el primer campo de la definicion del formato para el valor {} ", values[i]);
				for(CampoLinea c : campos){
					if(c.getPosicionCampo()==i+1){
						String valor = values[i].trim();
						logger.debug("Se coloca el valor [{}] (trimmed) al campo [{}] de la posicion <{}> del formato [{}] ", new Object[]{valor, c.getNombre(), c.getPosicionCampo(), formatoLinea.getNombreFormato()});
						c.setValor(valor);
						break;
					}
				}
			}
			
		}else{
			logger.debug("Procesando linea en formato de campos por posiciones fijas");
			for(CampoLinea c : campos){
				String valor = line.substring(c.getPosicionInicio(), c.getPosicionFin()).trim();
				logger.debug("Se coloca el valor [{}] (trimmed) al campo [{}] por longitudes fijas de [{}] a [{}]", 
						new Object[]{valor, c.getNombre(), c.getPosicionInicio(), c.getPosicionFin()});
				c.setValor(valor);
			}
		}
	}
	
	public Object buscarValorColumna(String nombreColumnaDestino, List<CampoLinea> campos) throws LineProcessException{
		logger.debug("Columna nombreColumnaDestino: {} ", nombreColumnaDestino);
		for(CampoLinea c : campos){
			if(nombreColumnaDestino.equals(c.getColumnaDestino())){
				if(isEmptyString(c.getValor())){
					if(c.isCampoOpcional()){
						return null;
					}else{
						throw new LineProcessException("El campo " + c.getNombre() + " no se encontro dentro de los valores de la linea. " );
					}
				}				
				String tipoDato = c.getTipoDatoAtributo();
				System.out.println("buscarValorColumna tipoDato  "+tipoDato);
				if("T".equals(tipoDato)){
					System.out.println("c.getValor() 1  "+c.getValor());
					return c.getValor();
				}else if("D".equals(tipoDato)){
					if(!isEmptyString(c.getFormatoAtributo())){
						DecimalFormat df = new DecimalFormat(c.getFormatoAtributo());
						
						try{
							double d = df.parse(c.getValor()).doubleValue();
							/*kpalacios | 22/08/2016, retornará solamente 2 decimales a partir del punto.*/ 
							System.out.println("c.getValor() 2  "+new BigDecimal(d).setScale(2, RoundingMode.HALF_UP));
							return new BigDecimal(d).setScale(2, RoundingMode.HALF_UP);
						}catch(ParseException e){
							throw new LineProcessException("El formato [[" + c.getFormatoAtributo() + "]] no es valido para el atributo [[" + 
										c.getNombre() + "]] de tipo Decimal", e);
						}
						
					}else{
						try{
							System.out.println("c.getValor() 3  "+new BigDecimal(c.getValor()));
							return new BigDecimal(c.getValor());
							
						}catch(NumberFormatException e){
							throw new LineProcessException("El valor [[" + c.getValor() + "]] para el atributo [[" + 
									c.getNombre() + "]] no puede ser convertido a un tipo de dato Decimal", e);
						}
						
					}
				}else if("E".equals(tipoDato)){
					try{
						System.out.println("c.getValor() 4  "+new Long(c.getValor()));
						return new Long(c.getValor());
					}catch(NumberFormatException e){
						throw new LineProcessException("El valor [[" + c.getValor() + "]] para el atributo [[" + 
								c.getNombre() + "]] no puede ser convertido a un tipo de dato Entero", e);
					}
				}else if("F".equals(tipoDato)){
					SimpleDateFormat sdf = isEmptyString(c.getFormatoAtributo()) 
												? new SimpleDateFormat(Vars.DEFAULT_DATE_FORMAT)
												: new SimpleDateFormat(c.getFormatoAtributo());
					try{
						String mapeo = isEmptyString(c.getFormatoAtributo()) ? Vars.DEFAULT_DATE_FORMAT: c.getFormatoAtributo();
						System.out.println("mapeomapeo 5  "+mapeo);
						System.out.println("c.getValor() 5  "+new Long(c.getValor()));
						System.out.println("c.getFormatoAtributo() 5  "+c.getFormatoAtributo());
						System.out.println("sdf.parse(c.getValor()) 5  "+sdf.parse(c.getValor()));
						return sdf.parse(c.getValor());
					}catch(ParseException e){
						throw new LineProcessException("El valor [[" + c.getValor() + "]] para el atributo [[" + 
								c.getNombre() + "]] no puede ser convertido a un tipo de dato Fecha con el formato " + sdf.toPattern(), e);
					}
				}else{
					logger.warn("Tipo de dato no valido para el campo : {} ", c.getNombre());
					throw new LineProcessException("El campo " + c.getNombre() + " no cuenta con un valor valido dentro de los tipos de datos soportados. "); 
				}
			}
		}
		
		logger.warn("No se encontro definicion en el formato para la columna: {} -> {}", nombreColumnaDestino, businessData.getReferenceInfo());
		return null;
	}
	
	protected boolean isEmptyString(String str){
		return str==null || "".equals(str.trim()) || str.equals("null");
	}
	
}
