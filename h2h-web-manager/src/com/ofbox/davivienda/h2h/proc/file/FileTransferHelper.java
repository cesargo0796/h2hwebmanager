package com.ofbox.davivienda.h2h.proc.file;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sshtools.j2ssh.SftpClient;
import com.sshtools.j2ssh.SshClient;
import com.sshtools.j2ssh.authentication.AuthenticationProtocolState;
import com.sshtools.j2ssh.authentication.PasswordAuthenticationClient;
import com.sshtools.j2ssh.sftp.FileAttributes;
import com.sshtools.j2ssh.sftp.SftpFile;
import com.sshtools.j2ssh.transport.IgnoreHostKeyVerification;

public class FileTransferHelper {

	/**
	 * https://tools.ietf.org/html/draft-ietf-secsh-filexfer-02#section-6.2
	 * <strong>6.2 File Names</strong>
	 * This protocol represents file names as strings.  File names are
   	 * assumed to use the slash ('/') character as a directory separator.
	 */
	public static final String DIRECTORY_SEPARATOR = "/";
	
	public static final String ENCRYPT_HINT = "_H2H_BEP_";
	
	private AntPathMatcher matcher = new AntPathMatcher();
	
	Logger logger = LoggerFactory.getLogger(FileTransferHelper.class);
	
	private String user;
	private String pwd;
	private int port;
	private String host;
	private String sshKey;
	private String origin;
	private String sftpDestination;
	private String filterBy;
	private String uploadFileName;
	private String downloadFileName;
	private String processedDir;
	private String pmUser;
	private String pmPwd;
	private String pmHost;
	private int pmPort;
	private String pmSshKey;
	private String destinationLocal;


	
	public long checkFileExists() throws Exception {
		SshClient ssh = new SshClient();
		logger.debug("Iniciando conexion hacia servidor publico SFTP {}@{}", pmUser, pmHost);
		try {
			if (pmPwd.length() > 0)
				ssh.connect(pmHost, pmPort, new IgnoreHostKeyVerification());
			else
				ssh.connect(pmHost, pmPort);

			PasswordAuthenticationClient pwdClient = new PasswordAuthenticationClient();
			pwdClient.setUsername(pmUser);
			
			logger.debug("pmPwd {}", pmPwd);
			if (pmPwd.length() > 0)
				pwdClient.setPassword(pmPwd);

			int result = ssh.authenticate(pwdClient);

			if (result != AuthenticationProtocolState.COMPLETE) {
				logger.debug("Error en la autenticaci�n con el servidor SFTP");
			}

			SftpClient client = ssh.openSftpClient();

			logger.debug("Verificando contenido en la ruta [{}]", origin);
			client.cd(origin);
			List<SftpFile> fileContained = client.ls();


			for (SftpFile sftpFile : fileContained) {
				String fileName = sftpFile.getFilename();
				logger.debug("Longitud del fileName  [{}]", fileName.length());
				if (fileName.length() > 3) {
					String originFile = origin + DIRECTORY_SEPARATOR + fileName;
					logger.debug("Nombre del fileName  [{}]", fileName);
					logger.debug("Nombre del filterBy  [{}]", filterBy);
					System.out.println("Nombre del fileName.toUpperCase()   " + fileName.toUpperCase());
					System.out.println("Nombre del filterBy.toUpperCase()   " + filterBy.toUpperCase());
					if (matcher.match(filterBy.toUpperCase(), fileName.toUpperCase())) {
						FileAttributes attrs = sftpFile.getAttributes();
						long fileSize = attrs.getSize().longValue();
						logger.debug("Archivo encontrado: {}, tamaño de archivo: {}",
								new Object[] { fileName, fileSize });
						return fileSize;
					} else {
						logger.info("El archivo [{}] no es seleccionable por el filtro [{}]", originFile, filterBy);
					}

				}
			}

			return -1l;
		} catch (Exception exception) {
			logger.error(
					"SFTP CONEXION ERROR - Se ha encontrado un error al conectarse con el servidor SFTP, cuyo resultado es el siguiente: "
							+ exception.getMessage());
			throw new Exception(exception.getMessage());
		} finally {
			if (ssh != null && ssh.isConnected())
				ssh.disconnect();
		}
	}
	
	@SuppressWarnings("rawtypes")
	public boolean sftpDownload(String destLocal) throws IOException {
		SshClient ssh = new SshClient();
				
		try {
			logger.debug("Iniciando conexion hacia servidor publico SFTP {}@{}", pmUser, pmHost);
			
			if (pmPwd.length() > 0)
				ssh.connect(pmHost, pmPort, new IgnoreHostKeyVerification());
			else
				ssh.connect(pmHost, pmPort);

			PasswordAuthenticationClient pwdClient = new PasswordAuthenticationClient();
			pwdClient.setUsername(pmUser);

			if (pmPwd.length() > 0)
				pwdClient.setPassword(pmPwd);

			int result = ssh.authenticate(pwdClient);

			if (result != AuthenticationProtocolState.COMPLETE) {
				logger.debug("Error en la autenticaci�n con el servidor SFTP");
			}
			
			logger.debug("Verificando contenido en la ruta {}", origin);
			
			SftpClient sftpClient = ssh.openSftpClient();
			sftpClient.cd(origin);
			List<SftpFile> fileContained = sftpClient.ls();
			boolean moveFile = processedDir!=null && !"".equals(processedDir);
			boolean useMatch = filterBy!=null && !"".equals(filterBy);
			
			for (SftpFile sftpFile : fileContained) {
				String fileName = sftpFile.getFilename();
				
				if(fileName.length() > 3){     
					String originFile = origin + DIRECTORY_SEPARATOR + fileName;
					String destLocalFile = destLocal + DIRECTORY_SEPARATOR + fileName;
					
					boolean fileCopied = false;
					System.out.print("Nombre del filterBy While  "+filterBy);
					if(useMatch && matcher.match(filterBy.toUpperCase(), fileName.toUpperCase())){
						logger.debug("Intentando copiar el archivo [{}] hacia [{}] filtrado por {}", new Object[]{originFile, destLocalFile, filterBy});
									
						File fileDestLocal = new File(destLocal);
						if(!fileDestLocal.exists()){
							fileDestLocal.mkdir();
							
							if (!fileDestLocal.exists()) {
								logger.error("No es posible crear la ruta destino local para la transferencia de archivo : " + fileName + ", -> ");
							} 
						}
						
						if (fileDestLocal.exists()) {
							sftpClient.cd(origin);
							sftpClient.lcd(destLocal);
							sftpClient.get(fileName);
							
							File file = new File(destLocalFile);
							
							if (!file.exists()) {
								logger.debug("The file {} was not tranferred to {} ", fileName, destLocal);
							} else {
								fileCopied = true;
								logger.debug("The file {} was tranferred successfully to {} ", fileName, destLocal);
							}
						} else {
							logger.debug("No es posible copiar el archivo {} al destino local {} ",  fileName, destLocal);
						}
						
						
					}else if(!useMatch){
						logger.debug("Intentando copiar el archivo [{}] hacia [{}]", originFile, destLocalFile);
						System.out.println("Trying to Copy [" + originFile + "] to [" + destLocalFile + "]");
						File fileDestLocal = new File(destLocal);
						if(!fileDestLocal.exists()){
							fileDestLocal.mkdir();
							
							if (!fileDestLocal.exists()) {
								logger.error("No es posible crear la ruta destino local para la transferencia de archivo : " + fileName + ", -> ");
							} 
						}
						
						if (fileDestLocal.exists()) {
							sftpClient.cd(origin);
							sftpClient.lcd(destLocal);
							sftpClient.get(fileName);
							
							File file = new File(destLocalFile);
							
							if (!file.exists()) {
								logger.debug("The file {} was not tranferred to {} ", fileName, destLocal);
							} else {
								fileCopied = true;
								logger.debug("The file {} was tranferred successfully to {} ", fileName, destLocal);
							}
						} else {
							logger.debug("No es posible copiar el archivo {} al destino local {} ",  fileName, destLocal);
						}
						
					}else{
						logger.info("El archivo [{}] no es seleccionable por el filtro [{}]", originFile, filterBy);
					}
					
					logger.debug("isFileCopied {}, isMoveFile {}", fileCopied, moveFile);
					if(fileCopied && moveFile){
						
						logger.debug("Cambiando archivo origen hacia carpeta {}", processedDir);
						String directory = originFile.substring(0, originFile.lastIndexOf(DIRECTORY_SEPARATOR));
						String processedDir = directory + DIRECTORY_SEPARATOR + getProcessedDir();
						boolean processedDirExists = false;
						try{
							sftpClient.cd(processedDir);
							processedDirExists = true;
						}catch(IOException ex){
							logger.warn("El directorio [{}] no existe. Sera creado en este momento...", processedDir);
							try{
								sftpClient.mkdir(processedDir);
								processedDirExists = true;
								logger.debug("Directorio fue creado exitosamente");
							}catch(IOException mkEx){
								logger.warn("El directorio [{}] no fue creado.  No es posible mover el archivo hacia directorio de archivos procesados. {}", processedDir, mkEx.getMessage());
							}							
						}
						
						if(processedDirExists){
							try{
								logger.debug("Traladando el archivo {} hacia directorio {}", fileName, processedDir);
								sftpClient.rename(originFile, processedDir + DIRECTORY_SEPARATOR + fileName);
								logger.debug("Archivo fue trasladado correctamente");
							}catch(IOException exc){
								logger.warn("No es posible traladar hacia directorio {}, Mensaje: {}", processedDir, exc.getMessage());
							}
						}else{
							logger.warn("El directorio [{}] no existe y/o no pudo ser creado. El archivo [{}] no se movera.", processedDir, originFile);
						}
						
					}
				}	
				
			}	
			
			return true;
					

		}  finally {
			if (ssh != null && ssh.isConnected())
				ssh.disconnect();
		}
		
	}
	
	
//	public boolean mkdirSftpRemoto(String dest) throws JSchException, FileNotFoundException, SftpException {
//		boolean dirExists = false;
//		SshClient ssh = new SshClient();
//
//		try {
//			try {
//				logger.debug("Iniciando conexion hacia servidor privado SFTP {}@{}", user, host);
//				logger.debug("Password?: {} ", pwd);
//				if (pwd.length() > 0)
//					ssh.connect(host, port, new IgnoreHostKeyVerification());
//				else
//					ssh.connect(host, port);
//	
//				PasswordAuthenticationClient pwdClient = new PasswordAuthenticationClient();
//				pwdClient.setUsername(user);
//	
//				if (pwd.length() > 0)
//					pwdClient.setPassword(pwd);
//	
//				int result = ssh.authenticate(pwdClient);
//	
//				if (result != AuthenticationProtocolState.COMPLETE) {
//					logger.debug("Error en la autenticaci�n con el servidor SFTP");
//					return dirExists;
//				}
//	
//				SftpClient client = ssh.openSftpClient();
//				client.mkdir(dest);
//
//				dirExists = true;
//				logger.debug("Directorio fue creado exitosamente");
//			} catch (Exception ex) {
//				logger.warn("El directorio [{}] no fue creado.  No es posible mover el archivo a la carpeta. {}", dest, ex.getMessage());
//			} 
//			return dirExists;
//
//		} finally {
//			if (ssh != null && ssh.isConnected())
//				ssh.disconnect();
//		}
//	}
	
	
//	public boolean sftpUpload(String localFileName, String sftpDestination) throws JSchException, FileNotFoundException, SftpException {
//		
//		JSch sftp = new JSch();
//		Session session = null;
//		ChannelSftp channelSftp = null;
//		
//		boolean usesKey = sshKey!=null && sshKey.trim().length()>0;
//		try {
//			if(usesKey){
//				sftp.addIdentity(sshKey);
//			}
//			
//			session = sftp.getSession(user, host, port);
//			if(!usesKey){
//				session.setPassword(pwd);
//			}
//			 /*El SFTP requiere un intercambio de claves
//			 con esta propiedad le decimos que acepte la clave
//			 sin pedir confirmación de aceptar RSA fingerprint*/
//			Properties prop = new Properties();
//			prop.put("StrictHostKeyChecking", "no");
//			session.setConfig(prop);
// 
//			session.connect();
// 
//			channelSftp = (ChannelSftp) session.openChannel("sftp");
//			channelSftp.connect();
//			
//			
//			InputStream in = new BufferedInputStream(new FileInputStream(localFileName));
//			
//			
//			logger.debug("Transfiriendo desde {} hacia {}", localFileName, sftpDestination);
//			channelSftp.put(in, sftpDestination);
//			
//			return true;
//			
//		} finally {
//			if(channelSftp!=null){
//				if (channelSftp.isConnected())
//					channelSftp.disconnect();
//			}
//			if(session!=null){
//				if (session.isConnected())
//					session.disconnect();
//			}
//		}
//	}
	
	public boolean sftpUploadRemote(String destRemoto, String destLocal, String fileName) throws Exception {
		SshClient ssh = new SshClient();

		try {
			logger.debug("Iniciando conexion hacia servidor privado SFTP {}@{}", user, host);
			if (pwd.length() > 0)
				ssh.connect(host, port, new IgnoreHostKeyVerification());
			else
				ssh.connect(host, port);

			PasswordAuthenticationClient pwdClient = new PasswordAuthenticationClient();
			pwdClient.setUsername(user);

			if (pwd.length() > 0)
				pwdClient.setPassword(pwd);

			int result = ssh.authenticate(pwdClient);

			if (result != AuthenticationProtocolState.COMPLETE) {
				logger.debug("Error en la autenticaci�n con el servidor SFTP");
			}

			String origenLocalFile = destLocal + DIRECTORY_SEPARATOR + fileName;
			SftpClient client = ssh.openSftpClient();
			client.mkdir(destRemoto);
			client.cd(destRemoto);
			client.put(new FileInputStream(origenLocalFile), fileName);
			logger.debug("Transfiriendo desde {} hacia {}", origenLocalFile, sftpDestination);

			return true;

		} catch (Exception ex) {
			logger.warn("No es posible mover el archivo a la carpeta sftp remoto. {}", ex.getMessage());
			throw new Exception(ex.getMessage());
		}

		finally {
			if (ssh != null && ssh.isConnected())
				ssh.disconnect();
		}
	}
	
	

	@Override
	public String toString() {
		return "FileTransferHelper [matcher=" + matcher + ", logger=" + logger + ", user=" + user + ", pwd=" + pwd
				+ ", port=" + port + ", host=" + host + ", sshKey=" + sshKey + ", origin=" + origin + ", sftpDestination="
				+ sftpDestination + ", filterBy=" + filterBy + ", uploadFileName=" + uploadFileName + ", downloadFileName="
				+ downloadFileName + ", processedDir=" + processedDir + ", pmUser=" + pmUser + ", pmPwd=" + pmPwd
				+ ", pmHost=" + pmHost + ", pmPort=" + pmPort + ", pmSshKey=" + pmSshKey + "]";
	}

	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}

	public String getPwd() {
		return pwd;
	}
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public int getPort() {
		return port;
	}
	public void setPort(int port) {
		this.port = port;
	}

	public String getHost() {
		return host;
	}
	public void setHost(String host) {
		this.host = host;
	}

	public String getSshKey() {
		return sshKey;
	}
	public void setSshKey(String sshKey) {
		this.sshKey = sshKey;
	}

	public String getOrigin() {
		return origin;
	}
	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getSftpDestination() {
		return sftpDestination;
	}
	public void setSftpDestination(String sftpDestination) {
		this.sftpDestination = sftpDestination;
	}

	public String getFilterBy() {
		return filterBy.toUpperCase();
	}
	public void setFilterBy(String filterBy) {
		this.filterBy = filterBy;
	}
	
	public String getUploadFileName() {
		return uploadFileName;
	}
	public void setUploadFileName(String uploadFileName) {
		this.uploadFileName = uploadFileName;
	}
	
	public String getProcessedDir() {
		return processedDir;
	}
	public void setProcessedDir(String processedDir) {
		this.processedDir = processedDir;
	}
	
	public String getPmUser() {
		return pmUser;
	}
	public void setPmUser(String pmUser) {
		this.pmUser = pmUser;
	}

	public String getPmPwd() {
		return pmPwd;
	}
	public void setPmPwd(String pmPwd) {
		this.pmPwd = pmPwd;
	}

	public int getPmPort() {
		return port;
	}
	public void setPmPort(int pmPort) {
		this.pmPort = pmPort;
	}

	public String getPmHost() {
		return pmHost;
	}
	public void setPmHost(String pmHost) {
		this.pmHost = pmHost;
	}

	public String getPmSshKey() {
		return pmSshKey;
	}
	public void setPmSshKey(String pmSshKey) {
		this.pmSshKey = pmSshKey;
	}
	
	public String getDestinationLocal() {
		return destinationLocal;
	}
	public void setDestinationLocal(String destinationLocal) {
		this.destinationLocal = destinationLocal;
	}
	
	
}
