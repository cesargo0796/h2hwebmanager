package com.ofbox.davivienda.h2h.proc;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.dbutils.BasicRowProcessor;
import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.MapHandler;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleTrigger;
import org.quartz.impl.JobDetailImpl;
import org.quartz.impl.triggers.SimpleTriggerImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ofbox.davivienda.h2h.dependenciasGlobales.H2hResolusor;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.util.jdbc.LocalizadorDeConexion;

/**
 * Clase principal que estará programada en el calendarizador
 * de Quartz.  Esta clase se encargara de levantar los demas
 * procesos que hayan sido configurados para revisar los archivos
 * dentro de las carpetas configuradas
 * @author engellt
 *
 */
public class MainMonitor implements Job{
	
	
	
	public static final String SQL_PROCESS_QUERY = "select  p.*, " + 
								" c.Nombre as \"Nombre_Cliente\", " + 
								" vc.Nombre as \"Nombre_Convenio\"" + 
								" from HTH_Proceso_Monitoreo p, BS_Cliente c, HTH_Vista_Convenio vc" + 
								" where p.Cliente = c.Cliente" + 
								" and   p.Id_Vista_Convenio = vc.Id_Vista_Convenio " + 
								" and   p.estado = 'A'";
	
	private static final String SQL_SYSTEM_PARAMS = "select * from HTH_Parametro_Sistema";
	

	Logger logger = LoggerFactory.getLogger(MainMonitor.class); 
	
	public String getJobName(){
		return Vars.MAIN_MONITOR_JOB_NAME;
	}
	
	public void execute(JobExecutionContext context)
			throws JobExecutionException {
		try{
			logger.info("Iniciando ejecucion de proceso principal... ");
			List<ProcessInformation> procs = loadProcessInfo();
			Map<String, Object> sysParams = loadSystemParamsMap();
			logger.debug("Iniciando configuracion de procesos en calendarizador...");
			Scheduler scheduler = PoolProcessManager.getInstance().getScheduler();
			registerJobs(procs, sysParams, scheduler);
		
		}catch(SQLException e){
			logger.error("Error al tratar de consultar informacion de procesos en base de datos. " + e.getMessage(), e);
		}catch(SchedulerException e){
			logger.error("Error en calendarizacion de los procesos: " + e.getMessage(), e);
		}catch(Exception e){
			logger.error("Error de ejecucion: " + e.getMessage(), e);
		}
		
	}

	
	private void registerJobs(List<ProcessInformation> procs, Map<String, Object> sysParams, Scheduler scheduler) throws SchedulerException{
		logger.debug("Iniciando registro de procesos en calendarizador...");
		
		for(ProcessInformation p : procs){
			JobDetailImpl jobDetail = new JobDetailImpl();
			jobDetail.setDescription("PROCESO " + p.getNombreCliente() + " - " + p.getNombreConvenio());
			jobDetail.setName(Vars.JOB_DETAIL_NAME_PREFIX + p.getIdProcesoMonitoreo());
			jobDetail.setGroup(Vars.JOB_DETAIL_NAME_PREFIX + p.getCliente());
			jobDetail.setJobClass(MonitorProcess.class);
			JobDataMap dataMap = new JobDataMap();
			
			dataMap.putAll(p.getDataMap());
			dataMap.putAll(sysParams);
			
			jobDetail.setJobDataMap(dataMap);
			
			SimpleTriggerImpl trigger = new SimpleTriggerImpl();
			trigger.setDescription("TRIGGER DE EJECUCION PARA CONVENIO " + p.getNombreConvenio() + " del Cliente " + p.getNombreCliente());
			trigger.setName(Vars.TRIGGER_NAME_PREFIX + p.getIdProcesoMonitoreo());
			trigger.setGroup(Vars.TRIGGER_GROUP_PREFIX + p.getCliente());
			trigger.setStartTime(new Date(System.currentTimeMillis() + 500));
			
			 /*El intervalo de repeticion es el tiempo configurado en segundos x 1000 
			 ya que este se configura en milisegundos dentro de quartz*/
			long repeatInterval = p.getTiempoMonitoreo() * 1000l;
			trigger.setRepeatInterval(repeatInterval);
			trigger.setRepeatCount(10);
					
			scheduler.scheduleJob(jobDetail, trigger);
		}
		
		logger.debug("Finalizada la calendarizacion de los procesos ...");
	}
	
	
	private Map<String, Object> loadSystemParamsMap() throws SQLException{
		logger.debug("Cargando parametros de tabla de parametros de sistema");
		LocalizadorDeConexion localizadorDeConexion = H2hResolusor.getSingleton().getLocalizadorDeConexionPrincipal();
		Connection conn = null;
		try{
			conn = localizadorDeConexion.getConnection();
			QueryRunner runner = new QueryRunner();
			Map<String, Object> map = runner.query(conn, SQL_SYSTEM_PARAMS, new MapHandler());
			return map;
		}finally{
			DbUtils.closeQuietly(conn);
		}
	}
	
	private List<ProcessInformation> loadProcessInfo() throws SQLException{
		logger.debug("Iniciando recoleccion de procesos de monitoreo configurados en base de datos...");
		LocalizadorDeConexion localizadorDeConexion = H2hResolusor.getSingleton().getLocalizadorDeConexionPrincipal();
		Connection conn = null;
		try{
			conn = localizadorDeConexion.getConnection();
			QueryRunner runner = new QueryRunner();
			List<ProcessInformation> procs = runner.query(conn, SQL_PROCESS_QUERY, new ResultSetHandler<List<ProcessInformation>>(){
	
				public List<ProcessInformation> handle(ResultSet rs)
						throws SQLException {
					List<ProcessInformation> procs = new ArrayList<ProcessInformation>();
					BasicRowProcessor rowProcessor = new BasicRowProcessor();
					while(rs.next()){
						ProcessInformation p = new ProcessInformation();
						p.setCliente(rs.getInt(Vars.PM_CLIENTE));
						p.setConvenio(rs.getInt(Vars.PM_CONVENIO));
						p.setIdProcesoMonitoreo(rs.getInt(Vars.PM_ID_PROCESO_MONITOREO));
						p.setNombreCliente(rs.getString(Vars.PM_NOMBRE_CLIENTE));
						p.setNombreConvenio(rs.getString(Vars.PM_CONVENIO_NOMBRE));
						p.setTiempoMonitoreo(rs.getInt(Vars.PM_TIEMPO_MONITOREO));
						p.setTipoConvenio(rs.getInt(Vars.PM_TIPOCONVENIO));
						p.setDataMap(rowProcessor.toMap(rs)); 
						procs.add(p);			
					}
					return procs;
				}
			});
		
			logger.debug("Se encontraron {} procesos configurados en estado 'Activo'", procs.size());
			return procs;
		}finally{
			DbUtils.closeQuietly(conn);
		}
		
	}
	
}
