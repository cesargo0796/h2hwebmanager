package com.ofbox.davivienda.h2h.proc;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class ProcessLogHandler {

	private Integer idProceso;
	
	private StringBuilder buffer;
	
	private Throwable lastException;
	
	private String resultado;
	
	private List<String> nombreArchivoList;
	
	private String cliente;
	
	private String convenio;
	
	private String descripcionResultado;
	
	private int lineasArchivo;
	
	private int lineasProcesadas;
	
	private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS '- '");
	
	public ProcessLogHandler(){
		buffer = new StringBuilder();
	}
	
	public void log(String logmessage){
		buffer.append(sdf.format(new Date()));
		buffer.append(logmessage);
		buffer.append("\n");
	}

	public Integer getIdProceso() {
		return idProceso;
	}

	public void setIdProceso(Integer idProceso) {
		this.idProceso = idProceso;
	}

	public StringBuilder getBuffer() {
		return buffer;
	}

	public void setBuffer(StringBuilder buffer) {
		this.buffer = buffer;
	}

	public Throwable getLastException() {
		return lastException;
	}

	public void setLastException(Throwable lastException) {
		this.lastException = lastException;
	}

	public String getResultado() {
		return resultado;
	}

	public void setResultado(String resultado) {
		this.resultado = resultado;
	}
	
	public List<String> getNombreArchivoList() {
		return nombreArchivoList;
	}

	public void setNombreArchivoList(List<String> nombreArchivoList) {
		this.nombreArchivoList = nombreArchivoList;
	}

	public String getCliente() {
		return cliente;
	}

	public void setCliente(String cliente) {
		this.cliente = cliente;
	}

	public String getConvenio() {
		return convenio;
	}

	public void setConvenio(String convenio) {
		this.convenio = convenio;
	}

	public String getDescripcionResultado() {
		return descripcionResultado;
	}

	public void setDescripcionResultado(String descripcionResultado) {
		this.descripcionResultado = descripcionResultado;
	}

	public int getLineasArchivo() {
		return lineasArchivo;
	}

	public void setLineasArchivo(int lineasArchivo) {
		this.lineasArchivo = lineasArchivo;
	}

	public int getLineasProcesadas() {
		return lineasProcesadas;
	}

	public void setLineasProcesadas(int lineasProcesadas) {
		this.lineasProcesadas = lineasProcesadas;
	}

	@Override
	public String toString() {
		return "ProcessLogHandler [idProceso=" + idProceso + ", buffer=" + buffer + ", lastException=" + lastException
				+ ", resultado=" + resultado + ", nombreArchivoList=" + nombreArchivoList + ", cliente=" + cliente
				+ ", convenio=" + convenio + ", descripcionResultado=" + descripcionResultado + ", lineasArchivo="
				+ lineasArchivo + ", lineasProcesadas=" + lineasProcesadas + "]";
	}
	
	
	
}
