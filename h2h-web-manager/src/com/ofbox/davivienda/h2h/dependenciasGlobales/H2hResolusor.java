package com.ofbox.davivienda.h2h.dependenciasGlobales;
          
import java.sql.Connection;
import java.sql.SQLException;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import com.ofbox.davivienda.h2h.abstraccion.negocio.BsBancoAchNegocio;
import com.ofbox.davivienda.h2h.abstraccion.negocio.BsClienteNegocio;
import com.ofbox.davivienda.h2h.abstraccion.negocio.BsCodigosBancoNegocio;
import com.ofbox.davivienda.h2h.abstraccion.negocio.BsConvenioNegocio;
import com.ofbox.davivienda.h2h.abstraccion.negocio.BsConvenioempresaNegocio;
import com.ofbox.davivienda.h2h.abstraccion.negocio.BsConvenioempresaachNegocio;
import com.ofbox.davivienda.h2h.abstraccion.negocio.BsDetalleachNegocio;
import com.ofbox.davivienda.h2h.abstraccion.negocio.BsDetallepeNegocio;
import com.ofbox.davivienda.h2h.abstraccion.negocio.BsEmpresaodeptoNegocio;
import com.ofbox.davivienda.h2h.abstraccion.negocio.BsInstalacionNegocio;
import com.ofbox.davivienda.h2h.abstraccion.negocio.BsLoteachNegocio;
import com.ofbox.davivienda.h2h.abstraccion.negocio.BsLotepeNegocio;
import com.ofbox.davivienda.h2h.abstraccion.negocio.BsRedOperacionBancoNegocio;
import com.ofbox.davivienda.h2h.abstraccion.negocio.HthAtributoDetalleNegocio;
import com.ofbox.davivienda.h2h.abstraccion.negocio.HthAtributoEncabezadoNegocio;
import com.ofbox.davivienda.h2h.abstraccion.negocio.HthAtributoFormatoNegocio;
import com.ofbox.davivienda.h2h.abstraccion.negocio.HthBitacoraProcesoNegocio;
import com.ofbox.davivienda.h2h.abstraccion.negocio.HthFormatoEncabezadoNegocio;
import com.ofbox.davivienda.h2h.abstraccion.negocio.HthFormatoNegocio;
import com.ofbox.davivienda.h2h.abstraccion.negocio.HthParametroGeneralNegocio;
import com.ofbox.davivienda.h2h.abstraccion.negocio.HthParametroSistemaNegocio;
import com.ofbox.davivienda.h2h.abstraccion.negocio.HthProcesoMonitoreoNegocio;
import com.ofbox.davivienda.h2h.abstraccion.negocio.HthVistaConvenioNegocio;
import com.ofbox.davivienda.h2h.mail.ManejadorDeCorreos;
import com.ofbox.davivienda.h2h.orm.jdbc.sql.impl.BsBancoAchDaoJdbcSql;
import com.ofbox.davivienda.h2h.orm.jdbc.sql.impl.BsClienteDaoJdbcSql;
import com.ofbox.davivienda.h2h.orm.jdbc.sql.impl.BsCodigosBancoDaoJdbcSql;
import com.ofbox.davivienda.h2h.orm.jdbc.sql.impl.BsConvenioDaoJdbcSql;
import com.ofbox.davivienda.h2h.orm.jdbc.sql.impl.BsConvenioachDaoJdbcSql;
import com.ofbox.davivienda.h2h.orm.jdbc.sql.impl.BsConvenioempresaDaoJdbcSql;
import com.ofbox.davivienda.h2h.orm.jdbc.sql.impl.BsConvenioempresaachDaoJdbcSql;
import com.ofbox.davivienda.h2h.orm.jdbc.sql.impl.BsDetalleachDaoJdbcSql;
import com.ofbox.davivienda.h2h.orm.jdbc.sql.impl.BsDetallepeDaoJdbcSql;
import com.ofbox.davivienda.h2h.orm.jdbc.sql.impl.BsEmpresaodeptoDaoJdbcSql;
import com.ofbox.davivienda.h2h.orm.jdbc.sql.impl.BsInstalacionDaoJdbcSql;
import com.ofbox.davivienda.h2h.orm.jdbc.sql.impl.BsLoteachDaoJdbcSql;
import com.ofbox.davivienda.h2h.orm.jdbc.sql.impl.BsLotepeDaoJdbcSql;
import com.ofbox.davivienda.h2h.orm.jdbc.sql.impl.BsRedOperacionBancoDaoJdbcSql;
import com.ofbox.davivienda.h2h.orm.jdbc.sql.impl.HthAtributoDetalleDaoJdbcSql;
import com.ofbox.davivienda.h2h.orm.jdbc.sql.impl.HthAtributoEncabezadoDaoJdbcSql;
import com.ofbox.davivienda.h2h.orm.jdbc.sql.impl.HthAtributoFormatoDaoJdbcSql;
import com.ofbox.davivienda.h2h.orm.jdbc.sql.impl.HthBitacoraProcesoDaoJdbcSql;
import com.ofbox.davivienda.h2h.orm.jdbc.sql.impl.HthFormatoDaoJdbcSql;
import com.ofbox.davivienda.h2h.orm.jdbc.sql.impl.HthFormatoEncabezadoDaoJdbcSql;
import com.ofbox.davivienda.h2h.orm.jdbc.sql.impl.HthParametroGeneralDaoJdbcSql;
import com.ofbox.davivienda.h2h.orm.jdbc.sql.impl.HthParametroSistemaDaoJdbcSql;
import com.ofbox.davivienda.h2h.orm.jdbc.sql.impl.HthProcesoMonitoreoDaoJdbcSql;
import com.ofbox.davivienda.h2h.orm.jdbc.sql.impl.HthVistaConvenioDaoJdbcSql;
import com.ofbox.davivienda.h2h.proc.PoolProcessManager;
import com.ofbox.davivienda.h2h.resourceBundle.MessagesResources_es_SV;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Dao;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dependencias.ExcepcionEnLocalizacionDeServicio;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dependencias.LocalizadorDeServicio;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dependencias.LocalizadorDeServiciosMultiples;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dependencias.impl.LocalizadorDeSerivicioBase;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.dependencias.ConstructorPrincipal;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.dependencias.Recursos;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.impl.SQLServerLenguaje;

/**
 * Clase Resolusor, utiliza el patr&oacute;n de dise&ntilde;o IoC o tambi&eacute;n conocido como inyecci&oacute;n
 * de  dependencias, brinda la capacidad de proveer de los objetos de Negocio y de la capa DAO cuando es necesario.
 *
 * @author Systems Out of the Box
 * @version f3.1
 */
public class H2hResolusor extends ConstructorPrincipal{

	/**
	 * Atributo que almacena el objeto Resolusor
	 */
    private static H2hResolusor h2hResolusor;
	private Recursos recursos = new MessagesResources_es_SV();
    /**
     * Devuelve una instancia del Resolusor utilizando el patr&oacute;n Singleton.
     * @return <code>Resolusor</code>
     */
    public static H2hResolusor getSingleton(){
         if(h2hResolusor == null){
               h2hResolusor =  new H2hResolusor(); 
         }
         return h2hResolusor;
    }
    
	/**
     * M&eacute;todo encargado de preparar las instancias de cada objeto de Negocio, las coloca en una colecci&oacute;n
     * de objetos de Negocio para que el localizador correspondiente pueda inyectar las dependencias cuando sea necesario.<br/>
     * 
     * Tambi&eacute;n realiza el trabajo de obtener la implementaci&oacute;n del Servicio de Acesso a datos, para este caso
     * el acceso a la capa DAO, para cada objeto de Negocio espec&iacute;fico.
     */
    public void prepararInstanciasNegocio(){    
        LocalizadorDeServiciosMultiples locMultiDao = getLocMultiDao();
        
        HthParametroGeneralNegocio hthParametroGeneralNegocio = new HthParametroGeneralNegocio();
        hthParametroGeneralNegocio.setDao((Dao)locMultiDao.getServicio("hthParametroGeneralDao"));
        BsClienteNegocio bsClienteNegocio = new BsClienteNegocio();
        bsClienteNegocio.setDao((Dao)locMultiDao.getServicio("bsClienteDao"));
        HthFormatoNegocio hthFormatoNegocio = new HthFormatoNegocio();
        hthFormatoNegocio.setDao((Dao)locMultiDao.getServicio("hthFormatoDao"));
        BsConvenioempresaNegocio bsConvenioempresaNegocio = new BsConvenioempresaNegocio();
        bsConvenioempresaNegocio.setDao((Dao)locMultiDao.getServicio("bsConvenioempresaDao"));
        BsDetallepeNegocio bsDetallepeNegocio = new BsDetallepeNegocio();
        bsDetallepeNegocio.setDao((Dao)locMultiDao.getServicio("bsDetallepeDao"));
        BsConvenioempresaachNegocio bsConvenioempresaachNegocio = new BsConvenioempresaachNegocio();
        bsConvenioempresaachNegocio.setDao((Dao)locMultiDao.getServicio("bsConvenioempresaachDao"));
        HthAtributoDetalleNegocio hthAtributoDetalleNegocio = new HthAtributoDetalleNegocio();
        hthAtributoDetalleNegocio.setDao((Dao)locMultiDao.getServicio("hthAtributoDetalleDao"));
        HthFormatoEncabezadoNegocio hthFormatoEncabezadoNegocio = new HthFormatoEncabezadoNegocio();
        hthFormatoEncabezadoNegocio.setDao((Dao)locMultiDao.getServicio("hthFormatoEncabezadoDao"));
        HthAtributoEncabezadoNegocio hthAtributoEncabezadoNegocio = new HthAtributoEncabezadoNegocio();
        hthAtributoEncabezadoNegocio.setDao((Dao)locMultiDao.getServicio("hthAtributoEncabezadoDao"));
        BsInstalacionNegocio bsInstalacionNegocio = new BsInstalacionNegocio();
        bsInstalacionNegocio.setDao((Dao)locMultiDao.getServicio("bsInstalacionDao"));
        HthVistaConvenioNegocio hthVistaConvenioNegocio = new HthVistaConvenioNegocio();
        hthVistaConvenioNegocio.setDao((Dao)locMultiDao.getServicio("hthVistaConvenioDao"));
        HthBitacoraProcesoNegocio hthBitacoraProcesoNegocio = new HthBitacoraProcesoNegocio();
        hthBitacoraProcesoNegocio.setDao((Dao)locMultiDao.getServicio("hthBitacoraProcesoDao"));
        BsLoteachNegocio bsLoteachNegocio = new BsLoteachNegocio();
        bsLoteachNegocio.setDao((Dao)locMultiDao.getServicio("bsLoteachDao"));
        BsDetalleachNegocio bsDetalleachNegocio = new BsDetalleachNegocio();
        bsDetalleachNegocio.setDao((Dao)locMultiDao.getServicio("bsDetalleachDao"));
        BsConvenioNegocio bsConvenioNegocio = new BsConvenioNegocio();
        bsConvenioNegocio.setDao((Dao)locMultiDao.getServicio("bsConvenioDao"));
        HthParametroSistemaNegocio hthParametroSistemaNegocio = new HthParametroSistemaNegocio();
        hthParametroSistemaNegocio.setDao((Dao)locMultiDao.getServicio("hthParametroSistemaDao"));
        BsEmpresaodeptoNegocio bsEmpresaodeptoNegocio = new BsEmpresaodeptoNegocio();
        bsEmpresaodeptoNegocio.setDao((Dao)locMultiDao.getServicio("bsEmpresaodeptoDao"));
        BsLotepeNegocio bsLotepeNegocio = new BsLotepeNegocio();
        bsLotepeNegocio.setDao((Dao)locMultiDao.getServicio("bsLotepeDao"));
        HthProcesoMonitoreoNegocio hthProcesoMonitoreoNegocio = new HthProcesoMonitoreoNegocio();
        hthProcesoMonitoreoNegocio.setDao((Dao)locMultiDao.getServicio("hthProcesoMonitoreoDao"));
        BsBancoAchNegocio bsConvenioachNegocio = new BsBancoAchNegocio();
        bsConvenioachNegocio.setDao((Dao)locMultiDao.getServicio("bsConvenioachDao"));
        HthAtributoFormatoNegocio hthAtributoFormatoNegocio = new HthAtributoFormatoNegocio();
        hthAtributoFormatoNegocio.setDao((Dao)locMultiDao.getServicio("hthAtributoFormatoDao"));
        BsCodigosBancoNegocio bsCodigosBanco = new BsCodigosBancoNegocio();
        bsCodigosBanco.setDao((Dao)locMultiDao.getServicio("bsCodigosBancoDao"));
        BsRedOperacionBancoNegocio bsRedOperacionBancoNegocio = new BsRedOperacionBancoNegocio();
        bsRedOperacionBancoNegocio.setDao((Dao)locMultiDao.getServicio("bsRedOperacionBancoDao"));
        BsBancoAchNegocio bsBancoAchNegocio = new BsBancoAchNegocio();
        bsBancoAchNegocio.setDao((Dao)locMultiDao.getServicio("bsBancoAchDao"));
  
        agregarInstanciaNegocio("hthParametroGeneralNegocio",hthParametroGeneralNegocio);
        agregarInstanciaNegocio("bsClienteNegocio",bsClienteNegocio);
        agregarInstanciaNegocio("hthFormatoNegocio",hthFormatoNegocio);
        agregarInstanciaNegocio("bsConvenioempresaNegocio",bsConvenioempresaNegocio);
        agregarInstanciaNegocio("bsDetallepeNegocio",bsDetallepeNegocio);
        agregarInstanciaNegocio("bsConvenioempresaachNegocio",bsConvenioempresaachNegocio);
        agregarInstanciaNegocio("hthAtributoDetalleNegocio",hthAtributoDetalleNegocio);
        agregarInstanciaNegocio("hthFormatoEncabezadoNegocio",hthFormatoEncabezadoNegocio);
        agregarInstanciaNegocio("hthAtributoEncabezadoNegocio",hthAtributoEncabezadoNegocio);
        agregarInstanciaNegocio("bsInstalacionNegocio",bsInstalacionNegocio);
        agregarInstanciaNegocio("hthVistaConvenioNegocio",hthVistaConvenioNegocio);
        agregarInstanciaNegocio("hthBitacoraProcesoNegocio",hthBitacoraProcesoNegocio);
        agregarInstanciaNegocio("bsLoteachNegocio",bsLoteachNegocio);
        agregarInstanciaNegocio("bsDetalleachNegocio",bsDetalleachNegocio);
        agregarInstanciaNegocio("bsConvenioNegocio",bsConvenioNegocio);
        agregarInstanciaNegocio("hthParametroSistemaNegocio",hthParametroSistemaNegocio);
        agregarInstanciaNegocio("bsEmpresaodeptoNegocio",bsEmpresaodeptoNegocio);
        agregarInstanciaNegocio("bsLotepeNegocio",bsLotepeNegocio);
        agregarInstanciaNegocio("hthProcesoMonitoreoNegocio",hthProcesoMonitoreoNegocio);
        agregarInstanciaNegocio("bsConvenioachNegocio",bsConvenioachNegocio);
        agregarInstanciaNegocio("hthAtributoFormatoNegocio",hthAtributoFormatoNegocio);
        agregarInstanciaNegocio("bsCodigosBancoNegocio",bsCodigosBanco);
        agregarInstanciaNegocio("bsRedOperacionBancoNegocio",bsRedOperacionBancoNegocio);
        agregarInstanciaNegocio("bsBancoAchNegocio",bsBancoAchNegocio);
        
        
        prepararUtileriasProcesos();
        prepararUtileriaCorreo();
        prepararAyudaSistema();
    }
	/**
     * Constructor de la clase, con acceso limitado.
     */
    private H2hResolusor(){
    
    }
	
	/**
	 * M&eacute;todo encargado de preparar las instancias de la capa de acceso a datos, para poder inyectar las dependencias
	 * a trav&eacute;s del Localizador correspondiente.<br/>
	 * 
	 * <ul>
	 * 	<li>Inicializa la configuraci&oacute;n del objeto encargado del Lenguaje de la base de datos a utilizar.</li>
	 * 	<li>Crea las instancias correspondientes para acceder a la capa de negocio de cada objeto de Negocio.</li>
	 * </ul>
	 * 
	 */
	public void prepararInstanciasDao(){
		final java.util.Properties params = new java.util.Properties();
				
		LocalizadorDeServicio localizadorLenguaje = new LocalizadorDeSerivicioBase() {

			public Object getServicio() throws ExcepcionEnLocalizacionDeServicio {
				return new SQLServerLenguaje(params);
			}
		};			 
	
		HthParametroGeneralDaoJdbcSql.setLocalizadorLenguaje(localizadorLenguaje);
        agregarInstanciaDao("hthParametroGeneralDao",new HthParametroGeneralDaoJdbcSql());
		BsClienteDaoJdbcSql.setLocalizadorLenguaje(localizadorLenguaje);
        agregarInstanciaDao("bsClienteDao",new BsClienteDaoJdbcSql());
		HthFormatoDaoJdbcSql.setLocalizadorLenguaje(localizadorLenguaje);
        agregarInstanciaDao("hthFormatoDao",new HthFormatoDaoJdbcSql());
		BsConvenioempresaDaoJdbcSql.setLocalizadorLenguaje(localizadorLenguaje);
        agregarInstanciaDao("bsConvenioempresaDao",new BsConvenioempresaDaoJdbcSql());
		BsDetallepeDaoJdbcSql.setLocalizadorLenguaje(localizadorLenguaje);
        agregarInstanciaDao("bsDetallepeDao",new BsDetallepeDaoJdbcSql());
		BsConvenioempresaachDaoJdbcSql.setLocalizadorLenguaje(localizadorLenguaje);
        agregarInstanciaDao("bsConvenioempresaachDao",new BsConvenioempresaachDaoJdbcSql());
		HthAtributoDetalleDaoJdbcSql.setLocalizadorLenguaje(localizadorLenguaje);
        agregarInstanciaDao("hthAtributoDetalleDao",new HthAtributoDetalleDaoJdbcSql());
		HthFormatoEncabezadoDaoJdbcSql.setLocalizadorLenguaje(localizadorLenguaje);
        agregarInstanciaDao("hthFormatoEncabezadoDao",new HthFormatoEncabezadoDaoJdbcSql());
		HthAtributoEncabezadoDaoJdbcSql.setLocalizadorLenguaje(localizadorLenguaje);
        agregarInstanciaDao("hthAtributoEncabezadoDao",new HthAtributoEncabezadoDaoJdbcSql());
		BsInstalacionDaoJdbcSql.setLocalizadorLenguaje(localizadorLenguaje);



        agregarInstanciaDao("bsInstalacionDao",new BsInstalacionDaoJdbcSql());
		HthVistaConvenioDaoJdbcSql.setLocalizadorLenguaje(localizadorLenguaje);
        agregarInstanciaDao("hthVistaConvenioDao",new HthVistaConvenioDaoJdbcSql());
		HthBitacoraProcesoDaoJdbcSql.setLocalizadorLenguaje(localizadorLenguaje);
        agregarInstanciaDao("hthBitacoraProcesoDao",new HthBitacoraProcesoDaoJdbcSql());
		BsLoteachDaoJdbcSql.setLocalizadorLenguaje(localizadorLenguaje);
        agregarInstanciaDao("bsLoteachDao",new BsLoteachDaoJdbcSql());
		BsDetalleachDaoJdbcSql.setLocalizadorLenguaje(localizadorLenguaje);
        agregarInstanciaDao("bsDetalleachDao",new BsDetalleachDaoJdbcSql());
		BsConvenioDaoJdbcSql.setLocalizadorLenguaje(localizadorLenguaje);
        agregarInstanciaDao("bsConvenioDao",new BsConvenioDaoJdbcSql());
		HthParametroSistemaDaoJdbcSql.setLocalizadorLenguaje(localizadorLenguaje);
        agregarInstanciaDao("hthParametroSistemaDao",new HthParametroSistemaDaoJdbcSql());
		BsEmpresaodeptoDaoJdbcSql.setLocalizadorLenguaje(localizadorLenguaje);
        agregarInstanciaDao("bsEmpresaodeptoDao",new BsEmpresaodeptoDaoJdbcSql());
		BsLotepeDaoJdbcSql.setLocalizadorLenguaje(localizadorLenguaje);
        agregarInstanciaDao("bsLotepeDao",new BsLotepeDaoJdbcSql());
		HthProcesoMonitoreoDaoJdbcSql.setLocalizadorLenguaje(localizadorLenguaje);
        agregarInstanciaDao("hthProcesoMonitoreoDao",new HthProcesoMonitoreoDaoJdbcSql());
		BsConvenioachDaoJdbcSql.setLocalizadorLenguaje(localizadorLenguaje);
        agregarInstanciaDao("bsConvenioachDao",new BsConvenioachDaoJdbcSql());
		HthAtributoFormatoDaoJdbcSql.setLocalizadorLenguaje(localizadorLenguaje);
        agregarInstanciaDao("hthAtributoFormatoDao",new HthAtributoFormatoDaoJdbcSql());
        agregarInstanciaDao("bsCodigosBancoDao",new BsCodigosBancoDaoJdbcSql());
        BsCodigosBancoDaoJdbcSql.setLocalizadorLenguaje(localizadorLenguaje);
        agregarInstanciaDao("bsRedOperacionBancoDao",new BsRedOperacionBancoDaoJdbcSql());
        BsRedOperacionBancoDaoJdbcSql.setLocalizadorLenguaje(localizadorLenguaje);
        agregarInstanciaDao("bsBancoAchDao",new BsBancoAchDaoJdbcSql());
        BsBancoAchDaoJdbcSql.setLocalizadorLenguaje(localizadorLenguaje);
	}
	
	public void setRecursos(Recursos recursos){
		this.recursos = recursos;
	}
	
	public Recursos getRecursos(){
		return this.recursos;
	}
	
	
	private void prepararUtileriasProcesos(){
		try{
			PoolProcessManager poolManager = PoolProcessManager.getInstance();
			poolManager.setLocalizadorDeConexion(getLocalizadorDeConexionPrincipal());
			poolManager.initialize();
			poolManager.startMonitor();
			
			getLocMulti().agregarInstancia("poolProcessManager", poolManager);
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void prepararUtileriaCorreo(){
		
		QueryRunner runner = new QueryRunner();
		Connection conn = null; 
		String javaMailJndi = null;
		try{
			conn = getLocalizadorDeConexionPrincipal().getConnection();
			javaMailJndi = (String) runner.query(conn, "select top 1 javamail_jndi from Hth_Parametro_Sistema", new ScalarHandler());
			if(javaMailJndi == null){
				javaMailJndi = "javamail/MailJ2Entorno";
			}
		}catch(SQLException e){
			e.printStackTrace();
			javaMailJndi = "javamail/MailJ2Entorno";
		}finally{
			DbUtils.closeQuietly(conn);
		}
		
		ManejadorDeCorreos mailSender = new ManejadorDeCorreos(javaMailJndi);
		getLocMulti().agregarInstancia("mailSender", mailSender);
	}
	
	
	public void prepararAyudaSistema(){
		AyudaSistema ayuda = new AyudaSistema();
       getLocMulti().agregarInstancia("ayuda", ayuda);
	}
    
}


