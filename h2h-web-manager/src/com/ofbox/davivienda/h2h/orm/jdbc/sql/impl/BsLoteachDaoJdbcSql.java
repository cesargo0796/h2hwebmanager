package com.ofbox.davivienda.h2h.orm.jdbc.sql.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.sql.Connection;
import java.util.Collection;

import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Dao;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.ExcepcionEnDAO;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.ExcepcionPorDatoNoEncontrado;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.PeticionDeDatos;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Transaccion;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.foraneos.DataConValoresForaneos;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLColumnaMeta;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLEntidadMeta;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLImplementacionBasica;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLImplementacionOperacionesComunes;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLLenguaje;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLOperaciones;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLUtils;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.TransaccionJdbc;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dependencias.LocalizadorDeServicio;


import com.ofbox.davivienda.h2h.abstraccion.dto.BsLoteach;
import com.ofbox.davivienda.h2h.abstraccion.dto.BsLoteachID;
import com.ofbox.davivienda.h2h.abstraccion.dao.BsLoteachDao;

import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.impl.DataConValoresForaneosImpl;   
import com.ofbox.davivienda.h2h.abstraccion.dto.BsInstalacion;
import com.ofbox.davivienda.h2h.abstraccion.dto.BsConvenioempresaach;


import com.ofbox.davivienda.h2h.orm.jdbc.sql.impl.BsInstalacionDaoJdbcSql;
import com.ofbox.davivienda.h2h.abstraccion.dao.BsInstalacionDao;
import com.ofbox.davivienda.h2h.orm.jdbc.sql.impl.BsConvenioempresaachDaoJdbcSql;
import com.ofbox.davivienda.h2h.abstraccion.dao.BsConvenioempresaachDao;

/**
 * Clase que hereda comportamientos de {@link SQLImplementacionBasica} e implementa el comportamiento definido en la capa DAO.<br/>
 * Esta clase es una implementaci&oacute;n intermedia para el DAO de <b>BS Lote ACH</b>
 * asociado a la tabla <b>BS_LoteACH</b>, en esta clase se construyen todos los sql para las operaciones de listado,
 * creaci&oacute;n, consulta, actualizaci&oacute;n y elminaci&oacute;n.<br/>
 * @author Systems Out of the Box
 * @version f3.1
 *
 */
public class BsLoteachDaoJdbcSql extends SQLImplementacionBasica implements BsLoteachDao{

	/**
	 * Atributo del tipo {@link SQLEntidadMeta}, que almacena la descripci&oacute;n del negocio como una entidad.<br/>
	 * Entre sus caracter&iacute;sticas tiene el nombre de la tabla, nombre de la secuencia, posici&oacute;n de las columnas,
	 * nombre de las columnas y tipos de datos para la columna.<br/>
	 * El arreglo que recibe son objetos de tipo {@link SQLColumnaMeta}, y definen informaci&oacute;n para obtener las columnas.
	 */
	public static final SQLEntidadMeta SQL_ENTIDAD_META = new SQLEntidadMeta("BS_LoteACH", new SQLColumnaMeta[]{
        new SQLColumnaMeta(9, "BS_LoteACH", "E9", 9001, "CodLote", "C9001", Types.BIGINT ,true)
     ,  new SQLColumnaMeta(9, "BS_LoteACH", "E9", 9002, "Instalacion", "C9002", Types.BIGINT ,true)
     ,  new SQLColumnaMeta(9, "BS_LoteACH", "E9", 9003, "Lote", "C9003", Types.BIGINT ,true)
     ,  new SQLColumnaMeta(9, "BS_LoteACH", "E9", 9004, "Convenio", "C9004", Types.BIGINT )
     ,  new SQLColumnaMeta(9, "BS_LoteACH", "E9", 9038, "EmpresaODepto", "C9038", Types.BIGINT )
     ,  new SQLColumnaMeta(9, "BS_LoteACH", "E9", 9005, "TipoLote", "C9005", Types.BIGINT )
     ,  new SQLColumnaMeta(9, "BS_LoteACH", "E9", 9006, "MontoTotal", "C9006", Types.NUMERIC )
     ,  new SQLColumnaMeta(9, "BS_LoteACH", "E9", 9007, "NumOperaciones", "C9007", Types.BIGINT )
     ,  new SQLColumnaMeta(9, "BS_LoteACH", "E9", 9008, "NombreLote", "C9008", Types.VARCHAR )
     ,  new SQLColumnaMeta(9, "BS_LoteACH", "E9", 9009, "FechaEnviado", "C9009", Types.TIMESTAMP )
     ,  new SQLColumnaMeta(9, "BS_LoteACH", "E9", 9010, "FechaRecibido", "C9010", Types.TIMESTAMP )
     ,  new SQLColumnaMeta(9, "BS_LoteACH", "E9", 9011, "FechaAplicacion", "C9011", Types.TIMESTAMP )
     ,  new SQLColumnaMeta(9, "BS_LoteACH", "E9", 9012, "FechaEstatus", "C9012", Types.TIMESTAMP )
     ,  new SQLColumnaMeta(9, "BS_LoteACH", "E9", 9013, "Estatus", "C9013", Types.BIGINT )
     ,  new SQLColumnaMeta(9, "BS_LoteACH", "E9", 9014, "UsuarioIngreso", "C9014", Types.VARCHAR )
     ,  new SQLColumnaMeta(9, "BS_LoteACH", "E9", 9015, "Autorizaciones", "C9015", Types.BIGINT )
     ,  new SQLColumnaMeta(9, "BS_LoteACH", "E9", 9016, "AplicacionDebitoHost", "C9016", Types.BIGINT )
     ,  new SQLColumnaMeta(9, "BS_LoteACH", "E9", 9017, "Sys_MarcaTiempo", "C9017", Types.TIMESTAMP )
     ,  new SQLColumnaMeta(9, "BS_LoteACH", "E9", 9018, "Sys_ProcessId", "C9018", Types.BIGINT )
     ,  new SQLColumnaMeta(9, "BS_LoteACH", "E9", 9019, "Sys_LowDateTime", "C9019", Types.BIGINT )
     ,  new SQLColumnaMeta(9, "BS_LoteACH", "E9", 9020, "Sys_HighDateTime", "C9020", Types.BIGINT )
     ,  new SQLColumnaMeta(9, "BS_LoteACH", "E9", 9021, "Ponderacion", "C9021", Types.BIGINT )
     ,  new SQLColumnaMeta(9, "BS_LoteACH", "E9", 9022, "Firmas", "C9022", Types.BIGINT )
     ,  new SQLColumnaMeta(9, "BS_LoteACH", "E9", 9023, "FechaAplicacionHost", "C9023", Types.TIMESTAMP )
     ,  new SQLColumnaMeta(9, "BS_LoteACH", "E9", 9024, "ListoParaHost", "C9024", Types.TIMESTAMP )
     ,  new SQLColumnaMeta(9, "BS_LoteACH", "E9", 9025, "FechaHoraProcesado", "C9025", Types.TIMESTAMP )
     ,  new SQLColumnaMeta(9, "BS_LoteACH", "E9", 9026, "Token", "C9026", Types.BIGINT )
     ,  new SQLColumnaMeta(9, "BS_LoteACH", "E9", 9027, "ComentarioRechazo", "C9027", Types.VARCHAR )
     ,  new SQLColumnaMeta(9, "BS_LoteACH", "E9", 9028, "Descripcion", "C9028", Types.VARCHAR )
     ,  new SQLColumnaMeta(9, "BS_LoteACH", "E9", 9029, "EstatusPayBank", "C9029", Types.VARCHAR )
     ,  new SQLColumnaMeta(9, "BS_LoteACH", "E9", 9030, "ArchivoGenerado", "C9030", Types.VARCHAR )
     ,  new SQLColumnaMeta(9, "BS_LoteACH", "E9", 9031, "ComisionCobrada", "C9031", Types.NUMERIC )
     ,  new SQLColumnaMeta(9, "BS_LoteACH", "E9", 9032, "CuentaDebito", "C9032", Types.VARCHAR )
     ,  new SQLColumnaMeta(9, "BS_LoteACH", "E9", 9033, "AutorizacionHost", "C9033", Types.BIGINT )
     ,  new SQLColumnaMeta(9, "BS_LoteACH", "E9", 9034, "DescEstatus", "C9034", Types.VARCHAR )
     ,  new SQLColumnaMeta(9, "BS_LoteACH", "E9", 9035, "idTransaccion", "C9035", Types.BIGINT )
     ,  new SQLColumnaMeta(9, "BS_LoteACH", "E9", 9036, "intActualizacion", "C9036", Types.BIGINT )
     ,  new SQLColumnaMeta(9, "BS_LoteACH", "E9", 9037, "MontoImpuesto", "C9037", Types.NUMERIC )
     ,  new SQLColumnaMeta(8, "BS_Instalacion", "E8", 8001, "Instalacion", "C8001", Types.BIGINT)
     ,  new SQLColumnaMeta(8, "BS_Instalacion", "E8", 8002, "Cliente", "C8002", Types.BIGINT)
     ,  new SQLColumnaMeta(8, "BS_Instalacion", "E8", 8003, "Modulos", "C8003", Types.BIGINT)
     ,  new SQLColumnaMeta(8, "BS_Instalacion", "E8", 8004, "Nombre", "C8004", Types.VARCHAR)
     ,  new SQLColumnaMeta(8, "BS_Instalacion", "E8", 8005, "ID", "C8005", Types.VARCHAR)
     ,  new SQLColumnaMeta(8, "BS_Instalacion", "E8", 8006, "Password", "C8006", Types.VARCHAR)
     ,  new SQLColumnaMeta(8, "BS_Instalacion", "E8", 8007, "PesoA", "C8007", Types.BIGINT)
     ,  new SQLColumnaMeta(8, "BS_Instalacion", "E8", 8008, "PesoB", "C8008", Types.BIGINT)
     ,  new SQLColumnaMeta(8, "BS_Instalacion", "E8", 8009, "PesoC", "C8009", Types.BIGINT)
     ,  new SQLColumnaMeta(8, "BS_Instalacion", "E8", 8010, "PesoD", "C8010", Types.BIGINT)
     ,  new SQLColumnaMeta(8, "BS_Instalacion", "E8", 8011, "PesoE", "C8011", Types.BIGINT)
     ,  new SQLColumnaMeta(8, "BS_Instalacion", "E8", 8012, "EstatusCom", "C8012", Types.BIGINT)
     ,  new SQLColumnaMeta(8, "BS_Instalacion", "E8", 8013, "FechaEstatus", "C8013", Types.TIMESTAMP)
     ,  new SQLColumnaMeta(8, "BS_Instalacion", "E8", 8014, "Estatus", "C8014", Types.BIGINT)
     ,  new SQLColumnaMeta(8, "BS_Instalacion", "E8", 8015, "Envio_Cliente", "C8015", Types.BIGINT)
     ,  new SQLColumnaMeta(8, "BS_Instalacion", "E8", 8016, "version", "C8016", Types.VARCHAR)
     ,  new SQLColumnaMeta(8, "BS_Instalacion", "E8", 8017, "versionPE", "C8017", Types.VARCHAR)
     ,  new SQLColumnaMeta(8, "BS_Instalacion", "E8", 8018, "OrigenLotes", "C8018", Types.BIGINT)
     ,  new SQLColumnaMeta(8, "BS_Instalacion", "E8", 8019, "SiguienteLoteCC", "C8019", Types.BIGINT)
     ,  new SQLColumnaMeta(8, "BS_Instalacion", "E8", 8020, "SiguienteLotePE", "C8020", Types.BIGINT)
     ,  new SQLColumnaMeta(8, "BS_Instalacion", "E8", 8021, "SiguienteLotePP", "C8021", Types.BIGINT)
     ,  new SQLColumnaMeta(8, "BS_Instalacion", "E8", 8022, "SiguienteLotePS", "C8022", Types.BIGINT)
     ,  new SQLColumnaMeta(8, "BS_Instalacion", "E8", 8023, "SiguienteLoteACH", "C8023", Types.BIGINT)
     ,  new SQLColumnaMeta(4, "BS_ConvenioEmpresaACH", "E4", 4001, "EmpresaODepto", "C4001", Types.BIGINT)
     ,  new SQLColumnaMeta(4, "BS_ConvenioEmpresaACH", "E4", 4002, "Convenio", "C4002", Types.BIGINT)
	});
	
    
	public BsInstalacion getBsInstalacion(BsLoteach bsLoteach)throws ExcepcionEnDAO{
		return getBsInstalacion(bsLoteach, null);
	}

    public BsInstalacion getBsInstalacion(BsLoteach bsLoteach,Transaccion tx)throws ExcepcionEnDAO{
         	     
	     BsInstalacion bsInstalacionRel = bsLoteach.getBsInstalacion();
	     if(bsInstalacionRel==null){
                PeticionDeDatos peticionDeDatosParaBsInstalacion = SQLImplementacionBasica.crearPeticionDeDatosST();
                peticionDeDatosParaBsInstalacion.agregarRestriccion(PeticionDeDatos.IGUAL,
	     	                              BsInstalacionDao.INSTALACION, 
	     	                              bsLoteach.getInstalacion()
	     	                              );
	            TransaccionJdbc tjdbc = (TransaccionJdbc)tx;
	     		bsInstalacionRel = (BsInstalacion)SQLImplementacionOperacionesComunes.resultadoUnicoST(
							            peticionDeDatosParaBsInstalacion, 
										tjdbc!=null? tjdbc.getConn(): getConnection(), 
										BsInstalacionDaoJdbcSql.SQL_OPERACIONES, 
										tjdbc==null, 
										getLog());
				bsLoteach.setBsInstalacion(bsInstalacionRel);										
		}
										
		return 	bsInstalacionRel;									
    }
    
    
	public BsConvenioempresaach getBsConvenioempresaach(BsLoteach bsLoteach)throws ExcepcionEnDAO{
		return getBsConvenioempresaach(bsLoteach, null);
	}

    public BsConvenioempresaach getBsConvenioempresaach(BsLoteach bsLoteach,Transaccion tx)throws ExcepcionEnDAO{
         	     
	     BsConvenioempresaach bsConvenioempresaachRel = bsLoteach.getBsConvenioempresaach();
	     if(bsConvenioempresaachRel==null){
                PeticionDeDatos peticionDeDatosParaBsConvenioempresaach = SQLImplementacionBasica.crearPeticionDeDatosST();
                peticionDeDatosParaBsConvenioempresaach.agregarRestriccion(PeticionDeDatos.IGUAL,
	     	                              BsConvenioempresaachDao.EMPRESAODEPTO, 
	     	                              bsLoteach.getEmpresaodepto()
	     	                              );
	            TransaccionJdbc tjdbc = (TransaccionJdbc)tx;
	     		bsConvenioempresaachRel = (BsConvenioempresaach)SQLImplementacionOperacionesComunes.resultadoUnicoST(
							            peticionDeDatosParaBsConvenioempresaach, 
										tjdbc!=null? tjdbc.getConn(): getConnection(), 
										BsConvenioempresaachDaoJdbcSql.SQL_OPERACIONES, 
										tjdbc==null, 
										getLog());
				bsLoteach.setBsConvenioempresaach(bsConvenioempresaachRel);										
		}
										
		return 	bsConvenioempresaachRel;									
    }
    

	

	/**
	 * Atributo que almacena el <code>String</code> para obtener el listado de registros de la tabla principal
	 * junto con otras tablas foraneas, realiza un JOIN.
	 */
	public static final String SELECT_HEADER_SQL_F =  
		"SELECT E9.CodLote AS C9001 , E9.Instalacion AS C9002 , E9.Lote AS C9003 , E9.Convenio AS C9004 , " +
		"       E9.EmpresaODepto AS C9038 , E9.TipoLote AS C9005 , E9.MontoTotal AS C9006 , E9.NumOperaciones AS C9007 , " +
		"       E9.NombreLote AS C9008 , E9.FechaEnviado AS C9009 , E9.FechaRecibido AS C9010 , E9.FechaAplicacion AS C9011 , " +
		"       E9.FechaEstatus AS C9012 , E9.Estatus AS C9013 , E9.UsuarioIngreso AS C9014 , E9.Autorizaciones AS C9015 , " +
		"       E9.AplicacionDebitoHost AS C9016 , E9.Sys_MarcaTiempo AS C9017 , E9.Sys_ProcessId AS C9018 , E9.Sys_LowDateTime AS C9019 , " +
		"       E9.Sys_HighDateTime AS C9020 , E9.Ponderacion AS C9021 , E9.Firmas AS C9022 , E9.FechaAplicacionHost AS C9023 , " +
		"       E9.ListoParaHost AS C9024 , E9.FechaHoraProcesado AS C9025 , E9.Token AS C9026 , E9.ComentarioRechazo AS C9027 , " +
		"       E9.Descripcion AS C9028 , E9.EstatusPayBank AS C9029 , E9.ArchivoGenerado AS C9030 , E9.ComisionCobrada AS C9031 , " +
		"       E9.CuentaDebito AS C9032 , E9.AutorizacionHost AS C9033 , E9.DescEstatus AS C9034 , E9.idTransaccion AS C9035 , " +
		"       E9.intActualizacion AS C9036 , E9.MontoImpuesto AS C9037 , " +
		"       E8.Instalacion AS C8001 , E8.Cliente AS C8002 , E8.Modulos AS C8003 , E8.Nombre AS C8004 , " +
		"       E8.ID AS C8005 , E8.Password AS C8006 , E8.PesoA AS C8007 , E8.PesoB AS C8008 , " +
		"       E8.PesoC AS C8009 , E8.PesoD AS C8010 , E8.PesoE AS C8011 , E8.EstatusCom AS C8012 , " +
		"       E8.FechaEstatus AS C8013 , E8.Estatus AS C8014 , E8.Envio_Cliente AS C8015 , E8.version AS C8016 , " +
		"       E8.versionPE AS C8017 , E8.OrigenLotes AS C8018 , E8.SiguienteLoteCC AS C8019 , E8.SiguienteLotePE AS C8020 , " +
		"       E8.SiguienteLotePP AS C8021 , E8.SiguienteLotePS AS C8022 , E8.SiguienteLoteACH AS C8023 , " +
		"       E4.EmpresaODepto AS C4001 , E4.Convenio AS C4002" +
		"  FROM BS_LoteACH E9 , BS_Instalacion E8 , BS_ConvenioEmpresaACH E4" +
		" WHERE E8.Instalacion = E9.Instalacion" +
		"   AND E4.EmpresaODepto = E9.EmpresaODepto";
	/**
	 * Atributo que almacena el <code>String</code> para obtener el listado de registros de la tabla principal
     * junto con otras tablas foraneas, realiza un JOIN utilizando la <code>primary key</code> de la tabla principal.
	 */
	public static final String FIND_BY_ID_SQL_F = 
		"SELECT E9.CodLote AS C9001 , E9.Instalacion AS C9002 , E9.Lote AS C9003 , E9.Convenio AS C9004 , " +
		"       E9.EmpresaODepto AS C9038 , E9.TipoLote AS C9005 , E9.MontoTotal AS C9006 , E9.NumOperaciones AS C9007 , " +
		"       E9.NombreLote AS C9008 , E9.FechaEnviado AS C9009 , E9.FechaRecibido AS C9010 , E9.FechaAplicacion AS C9011 , " +
		"       E9.FechaEstatus AS C9012 , E9.Estatus AS C9013 , E9.UsuarioIngreso AS C9014 , E9.Autorizaciones AS C9015 , " +
		"       E9.AplicacionDebitoHost AS C9016 , E9.Sys_MarcaTiempo AS C9017 , E9.Sys_ProcessId AS C9018 , E9.Sys_LowDateTime AS C9019 , " +
		"       E9.Sys_HighDateTime AS C9020 , E9.Ponderacion AS C9021 , E9.Firmas AS C9022 , E9.FechaAplicacionHost AS C9023 , " +
		"       E9.ListoParaHost AS C9024 , E9.FechaHoraProcesado AS C9025 , E9.Token AS C9026 , E9.ComentarioRechazo AS C9027 , " +
		"       E9.Descripcion AS C9028 , E9.EstatusPayBank AS C9029 , E9.ArchivoGenerado AS C9030 , E9.ComisionCobrada AS C9031 , " +
		"       E9.CuentaDebito AS C9032 , E9.AutorizacionHost AS C9033 , E9.DescEstatus AS C9034 , E9.idTransaccion AS C9035 , " +
		"       E9.intActualizacion AS C9036 , E9.MontoImpuesto AS C9037 , " +
		"       E8.Instalacion AS C8001 , E8.Cliente AS C8002 , E8.Modulos AS C8003 , E8.Nombre AS C8004 , " +
		"       E8.ID AS C8005 , E8.Password AS C8006 , E8.PesoA AS C8007 , E8.PesoB AS C8008 , " +
		"       E8.PesoC AS C8009 , E8.PesoD AS C8010 , E8.PesoE AS C8011 , E8.EstatusCom AS C8012 , " +
		"       E8.FechaEstatus AS C8013 , E8.Estatus AS C8014 , E8.Envio_Cliente AS C8015 , E8.version AS C8016 , " +
		"       E8.versionPE AS C8017 , E8.OrigenLotes AS C8018 , E8.SiguienteLoteCC AS C8019 , E8.SiguienteLotePE AS C8020 , " +
		"       E8.SiguienteLotePP AS C8021 , E8.SiguienteLotePS AS C8022 , E8.SiguienteLoteACH AS C8023 , " +
		"       E4.EmpresaODepto AS C4001 , E4.Convenio AS C4002" +
		"  FROM BS_LoteACH E9 , BS_Instalacion E8 , BS_ConvenioEmpresaACH E4" +
		" WHERE E8.Instalacion = E9.Instalacion" +
		"   AND E4.EmpresaODepto = E9.EmpresaODepto" +
		"   AND E9.CodLote= ? " +
		"   AND E9.Instalacion= ? " +
		"   AND E9.Lote= ? ";
	

	/**
	 * Atributo que almacena el <code>String</code> para obtener el listado de registros del negocio.
	 */
	public static final String SELECT_HEADER_SQL =  
		"SELECT E9.CodLote AS C9001 , E9.Instalacion AS C9002 , E9.Lote AS C9003 , E9.Convenio AS C9004 , " +
		"       E9.EmpresaODepto AS C9038 , E9.TipoLote AS C9005 , E9.MontoTotal AS C9006 , E9.NumOperaciones AS C9007 , " +
		"       E9.NombreLote AS C9008 , E9.FechaEnviado AS C9009 , E9.FechaRecibido AS C9010 , E9.FechaAplicacion AS C9011 , " +
		"       E9.FechaEstatus AS C9012 , E9.Estatus AS C9013 , E9.UsuarioIngreso AS C9014 , E9.Autorizaciones AS C9015 , " +
		"       E9.AplicacionDebitoHost AS C9016 , E9.Sys_MarcaTiempo AS C9017 , E9.Sys_ProcessId AS C9018 , E9.Sys_LowDateTime AS C9019 , " +
		"       E9.Sys_HighDateTime AS C9020 , E9.Ponderacion AS C9021 , E9.Firmas AS C9022 , E9.FechaAplicacionHost AS C9023 , " +
		"       E9.ListoParaHost AS C9024 , E9.FechaHoraProcesado AS C9025 , E9.Token AS C9026 , E9.ComentarioRechazo AS C9027 , " +
		"       E9.Descripcion AS C9028 , E9.EstatusPayBank AS C9029 , E9.ArchivoGenerado AS C9030 , E9.ComisionCobrada AS C9031 , " +
		"       E9.CuentaDebito AS C9032 , E9.AutorizacionHost AS C9033 , E9.DescEstatus AS C9034 , E9.idTransaccion AS C9035 , " +
		"       E9.intActualizacion AS C9036 , E9.MontoImpuesto AS C9037" +
		"  FROM BS_LoteACH E9";
	/**
	 * Atributo que almacena el <code>String</code> para obtener el n&uacute;mero de registros presentes en el negocio.
	 */
	public static final String COUNT_HEADER_SQL =   "SELECT COUNT(*) FROM BS_LoteACH";    												
	/**
	 * Atributo que almacena el <code>String</code> para la inserci&oacute;n, sin id, de un nuevo registro en la tabla.
	 */
	public static final String CREATE_SQL = 
		"INSERT INTO BS_LoteACH (" +
		"Convenio,EmpresaODepto,TipoLote,MontoTotal,NumOperaciones" +
		",NombreLote,FechaEnviado,FechaRecibido,FechaAplicacion" +
		",FechaEstatus,Estatus,UsuarioIngreso,Autorizaciones" +
		",AplicacionDebitoHost,Sys_MarcaTiempo,Sys_ProcessId,Sys_LowDateTime" +
		",Sys_HighDateTime,Ponderacion,Firmas,FechaAplicacionHost" +
		",ListoParaHost,FechaHoraProcesado,Token,ComentarioRechazo" +
		",Descripcion,EstatusPayBank,ArchivoGenerado,ComisionCobrada" +
		",CuentaDebito,AutorizacionHost,DescEstatus,idTransaccion" +
		",intActualizacion,MontoImpuesto" +
		") VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	/**
	 * Atributo que almacena el <code>String</code> para insertar, con id, un registro en la table.
	 */
	public static final String CREATE_SQL_CON_ID = 
		"INSERT INTO BS_LoteACH (" +
		"CodLote,Instalacion,Lote,Convenio,EmpresaODepto" +
		",TipoLote,MontoTotal,NumOperaciones,NombreLote" +
		",FechaEnviado,FechaRecibido,FechaAplicacion,FechaEstatus" +
		",Estatus,UsuarioIngreso,Autorizaciones,AplicacionDebitoHost" +
		",Sys_MarcaTiempo,Sys_ProcessId,Sys_LowDateTime,Sys_HighDateTime" +
		",Ponderacion,Firmas,FechaAplicacionHost,ListoParaHost" +
		",FechaHoraProcesado,Token,ComentarioRechazo,Descripcion" +
		",EstatusPayBank,ArchivoGenerado,ComisionCobrada,CuentaDebito" +
		",AutorizacionHost,DescEstatus,idTransaccion,intActualizacion" +
		",MontoImpuesto" +
		") VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	/**
	 * Atributo que almacena el <code>String</code> para la atualizaci&oacute; de un registro en la tabla.
	 */
	public static final String UPDATE_SQL = 
		"UPDATE BS_LoteACH" +
		" SET Convenio= ?  , EmpresaODepto= ?  , TipoLote= ?  , MontoTotal= ?  , " +
		"NumOperaciones= ?  , NombreLote= ?  , FechaEnviado= ?  , FechaRecibido= ?  , " +
		"FechaAplicacion= ?  , FechaEstatus= ?  , Estatus= ?  , UsuarioIngreso= ?  , " +
		"Autorizaciones= ?  , AplicacionDebitoHost= ?  , Sys_MarcaTiempo= ?  , Sys_ProcessId= ?  , " +
		"Sys_LowDateTime= ?  , Sys_HighDateTime= ?  , Ponderacion= ?  , Firmas= ?  , " +
		"FechaAplicacionHost= ?  , ListoParaHost= ?  , FechaHoraProcesado= ?  , Token= ?  , " +
		"ComentarioRechazo= ?  , Descripcion= ?  , EstatusPayBank= ?  , ArchivoGenerado= ?  , " +
		"ComisionCobrada= ?  , CuentaDebito= ?  , AutorizacionHost= ?  , DescEstatus= ?  , " +
		"idTransaccion= ?  , intActualizacion= ?  , MontoImpuesto= ? " +
		" WHERE CodLote= ?    AND Instalacion= ?    AND Lote= ? ";
	/**
	 * Atributo que almacena el <code>String</code> para la eliminaci&oacute; de un registro en la tabla.
	 */
	public static final String DELETE_SQL =  
		"DELETE FROM BS_LoteACH" +
		" WHERE CodLote= ?    AND Instalacion= ?    AND Lote= ? ";
	/**
	 * Atributo que almacena el <code>String</code> para la b&uacute;squeda por id de un registro de la tabla.
	 */
	public static final String FIND_BY_ID_SQL = 
		"SELECT E9.CodLote AS C9001 , E9.Instalacion AS C9002 , E9.Lote AS C9003 , E9.Convenio AS C9004 , " +
		"       E9.EmpresaODepto AS C9038 , E9.TipoLote AS C9005 , E9.MontoTotal AS C9006 , E9.NumOperaciones AS C9007 , " +
		"       E9.NombreLote AS C9008 , E9.FechaEnviado AS C9009 , E9.FechaRecibido AS C9010 , E9.FechaAplicacion AS C9011 , " +
		"       E9.FechaEstatus AS C9012 , E9.Estatus AS C9013 , E9.UsuarioIngreso AS C9014 , E9.Autorizaciones AS C9015 , " +
		"       E9.AplicacionDebitoHost AS C9016 , E9.Sys_MarcaTiempo AS C9017 , E9.Sys_ProcessId AS C9018 , E9.Sys_LowDateTime AS C9019 , " +
		"       E9.Sys_HighDateTime AS C9020 , E9.Ponderacion AS C9021 , E9.Firmas AS C9022 , E9.FechaAplicacionHost AS C9023 , " +
		"       E9.ListoParaHost AS C9024 , E9.FechaHoraProcesado AS C9025 , E9.Token AS C9026 , E9.ComentarioRechazo AS C9027 , " +
		"       E9.Descripcion AS C9028 , E9.EstatusPayBank AS C9029 , E9.ArchivoGenerado AS C9030 , E9.ComisionCobrada AS C9031 , " +
		"       E9.CuentaDebito AS C9032 , E9.AutorizacionHost AS C9033 , E9.DescEstatus AS C9034 , E9.idTransaccion AS C9035 , " +
		"       E9.intActualizacion AS C9036 , E9.MontoImpuesto AS C9037" +
		"  FROM BS_LoteACH E9" +
		" WHERE E9.CodLote= ? " +
		"   AND E9.Instalacion= ? " +
		"   AND E9.Lote= ? ";
	/**
	 * Atributo que almacena el <code>String</code> para la creacion de la tabla en la base de datos.
	 */
	public static final String CREATE_TABLE_DDL = 
		"CREATE TABLE BS_LoteACH (" +
		"    CodLote LONG NOT NULL," +
		"    Instalacion LONG NOT NULL," +
		"    Lote LONG NOT NULL," +
		"    Convenio LONG NOT NULL," +
		"    EmpresaODepto LONG NOT NULL," +
		"    TipoLote LONG," +
		"    MontoTotal NUMERIC(19, 0)  NOT NULL," +
		"    NumOperaciones LONG NOT NULL," +
		"    NombreLote VARCHAR(30) ," +
		"    FechaEnviado TIMESTAMP NOT NULL," +
		"    FechaRecibido TIMESTAMP," +
		"    FechaAplicacion TIMESTAMP," +
		"    FechaEstatus TIMESTAMP," +
		"    Estatus LONG NOT NULL," +
		"    UsuarioIngreso VARCHAR(10) ," +
		"    Autorizaciones LONG," +
		"    AplicacionDebitoHost LONG," +
		"    Sys_MarcaTiempo TIMESTAMP," +
		"    Sys_ProcessId LONG," +
		"    Sys_LowDateTime LONG," +
		"    Sys_HighDateTime LONG," +
		"    Ponderacion LONG NOT NULL," +
		"    Firmas LONG NOT NULL," +
		"    FechaAplicacionHost TIMESTAMP," +
		"    ListoParaHost TIMESTAMP," +
		"    FechaHoraProcesado TIMESTAMP," +
		"    Token LONG," +
		"    ComentarioRechazo VARCHAR(100) ," +
		"    Descripcion VARCHAR(50) ," +
		"    EstatusPayBank VARCHAR(20) ," +
		"    ArchivoGenerado VARCHAR(100) ," +
		"    ComisionCobrada NUMERIC(19, 0) ," +
		"    CuentaDebito VARCHAR(16) ," +
		"    AutorizacionHost LONG," +
		"    DescEstatus VARCHAR(500) ," +
		"    idTransaccion LONG," +
		"    intActualizacion LONG NOT NULL," +
		"    MontoImpuesto NUMERIC(19, 0) " +
		")";
	/**
	 * Atributo que almacena un objeto del tipo {@link SQLLenguaje}.
	 */
	private static SQLLenguaje lenguaje;
	
	/**
	 * M&eacute;todo que realiza la creaci&oacute;n de un DTO, del negocio, a partir del resultado de alguna consulta.
	 * @param <code>rst</code> objeto de tipo <code>Resultset</code>, que contiene los resultados de la consulta.
	 * @return <code>Object</code> - representa al DTO creado.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static Object crearDtoST(ResultSet rst)
			throws SQLException, ExcepcionEnDAO {
 	    BsLoteach theDto = new BsLoteach();
     	theDto.setCodlote(SQLUtils.getLongFromResultSet(rst, "C9001"));
     	theDto.setInstalacion(SQLUtils.getLongFromResultSet(rst, "C9002"));
     	theDto.setLote(SQLUtils.getLongFromResultSet(rst, "C9003"));
     	theDto.setConvenio(SQLUtils.getLongFromResultSet(rst, "C9004"));
     	theDto.setEmpresaodepto(SQLUtils.getLongFromResultSet(rst, "C9038"));
     	theDto.setTipolote(SQLUtils.getLongFromResultSet(rst, "C9005"));
		theDto.setMontototal(rst.getBigDecimal("C9006"));
     	theDto.setNumoperaciones(SQLUtils.getLongFromResultSet(rst, "C9007"));
		theDto.setNombrelote(rst.getString("C9008"));
		theDto.setFechaenviado(rst.getTimestamp("C9009"));
		theDto.setFecharecibido(rst.getTimestamp("C9010"));
		theDto.setFechaaplicacion(rst.getTimestamp("C9011"));
		theDto.setFechaestatus(rst.getTimestamp("C9012"));
     	theDto.setEstatus(SQLUtils.getLongFromResultSet(rst, "C9013"));
		theDto.setUsuarioingreso(rst.getString("C9014"));
     	theDto.setAutorizaciones(SQLUtils.getLongFromResultSet(rst, "C9015"));
     	theDto.setAplicaciondebitohost(SQLUtils.getLongFromResultSet(rst, "C9016"));
		theDto.setSysMarcatiempo(rst.getTimestamp("C9017"));
     	theDto.setSysProcessid(SQLUtils.getLongFromResultSet(rst, "C9018"));
     	theDto.setSysLowdatetime(SQLUtils.getLongFromResultSet(rst, "C9019"));
     	theDto.setSysHighdatetime(SQLUtils.getLongFromResultSet(rst, "C9020"));
     	theDto.setPonderacion(SQLUtils.getLongFromResultSet(rst, "C9021"));
     	theDto.setFirmas(SQLUtils.getLongFromResultSet(rst, "C9022"));
		theDto.setFechaaplicacionhost(rst.getTimestamp("C9023"));
		theDto.setListoparahost(rst.getTimestamp("C9024"));
		theDto.setFechahoraprocesado(rst.getTimestamp("C9025"));
     	theDto.setToken(SQLUtils.getLongFromResultSet(rst, "C9026"));
		theDto.setComentariorechazo(rst.getString("C9027"));
		theDto.setDescripcion(rst.getString("C9028"));
		theDto.setEstatuspaybank(rst.getString("C9029"));
		theDto.setArchivogenerado(rst.getString("C9030"));
		theDto.setComisioncobrada(rst.getBigDecimal("C9031"));
		theDto.setCuentadebito(rst.getString("C9032"));
     	theDto.setAutorizacionhost(SQLUtils.getLongFromResultSet(rst, "C9033"));
		theDto.setDescestatus(rst.getString("C9034"));
     	theDto.setIdtransaccion(SQLUtils.getLongFromResultSet(rst, "C9035"));
     	theDto.setIntactualizacion(SQLUtils.getLongFromResultSet(rst, "C9036"));
		theDto.setMontoimpuesto(rst.getBigDecimal("C9037"));
		return theDto;
	}
	
	


	/**
	 * M&eacute;todo que realiza la creaci&oacute;n de un DTO foraneo, del negocio, a partir del resultado de alguna consulta.
	 * @param <code>rst</code> objeto de tipo <code>Resultset</code>, que contiene los resultados de la consulta.
	 * @return <code>BsLoteach</code> - representa al DTO del negocio con el nuevo DTO foraneo asociado.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static BsLoteach crearDataConValoresForaneosST(ResultSet rst)throws SQLException, ExcepcionEnDAO{
		BsLoteach bsLoteach = (BsLoteach) crearDtoST(rst);
        BsInstalacion bsInstalacionRel  = (BsInstalacion) BsInstalacionDaoJdbcSql.crearDtoST(rst);
        bsLoteach.setBsInstalacion(bsInstalacionRel);
        BsConvenioempresaach bsConvenioempresaachRel  = (BsConvenioempresaach) BsConvenioempresaachDaoJdbcSql.crearDtoST(rst);
        bsLoteach.setBsConvenioempresaach(bsConvenioempresaachRel);
		return bsLoteach;
	}




	/**
	 * M&eacute;todo que realiza la creaci&oacute;n de un DTO foraneo, del negocio, a partir de la b&uacute;squeda por <code>id</code>.
	 * @param <code>rst</code> objeto de tipo <code>Resultset</code>, que contiene los resultados de la consulta.
	 * @return <code>BsLoteach</code> - representa al DTO del negocio con el nuevo DTO foraneo asociado.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static BsLoteach crearDataConValoresForaneosNoRelST(ResultSet rst, Connection conn)throws SQLException, ExcepcionEnDAO{	
		BsLoteach bsLoteach = (BsLoteach) crearDtoST(rst);
        BsInstalacion instalacionRelbsInstalacion  = (BsInstalacion) SQLImplementacionOperacionesComunes.buscarPorIDST(
                bsLoteach.getInstalacion(), conn, 
				BsInstalacionDaoJdbcSql.SQL_OPERACIONES,
	       		false, 
	       		null);
        bsLoteach.setBsInstalacion(instalacionRelbsInstalacion);
        BsConvenioempresaach empresaodeptoRelbsConvenioempresaach  = (BsConvenioempresaach) SQLImplementacionOperacionesComunes.buscarPorIDST(
                bsLoteach.getEmpresaodepto(), conn, 
				BsConvenioempresaachDaoJdbcSql.SQL_OPERACIONES,
	       		false, 
	       		null);
        bsLoteach.setBsConvenioempresaach(empresaodeptoRelbsConvenioempresaach);
		return bsLoteach;
	}

	/**
	 * M&eacute;todo que coloca los valores provenientes del objeto Dto en el objeto PreparedStatement,
	 * el cual proviene de la llamada a la implementacion de {@link SQLOperaciones}, este m&eacute;todo
	 * no toma en cuenta el valor del <code>id</code>, puesto que se asume que es un valor secuencial
	 * autonumerico o serial propio de la base de datos.
	 * 
	 * @param <code>theDtoUntyped</code> objeto sin tipo que almacena al DTO del negocio.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaCrearST(Object theDtoUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {			
	        BsLoteach theDto = (BsLoteach)theDtoUntyped;
				Long convenio = theDto.getConvenio();     
		Long empresaodepto = theDto.getEmpresaodepto();     
		Long tipolote = theDto.getTipolote();     
		java.math.BigDecimal montototal = theDto.getMontototal();     
		Long numoperaciones = theDto.getNumoperaciones();     
		String nombrelote = theDto.getNombrelote();     
		java.util.Date fechaenviado = theDto.getFechaenviado();     
		java.util.Date fecharecibido = theDto.getFecharecibido();     
		java.util.Date fechaaplicacion = theDto.getFechaaplicacion();     
		java.util.Date fechaestatus = theDto.getFechaestatus();     
		Long estatus = theDto.getEstatus();     
		String usuarioingreso = theDto.getUsuarioingreso();     
		Long autorizaciones = theDto.getAutorizaciones();     
		Long aplicaciondebitohost = theDto.getAplicaciondebitohost();     
		java.util.Date sysMarcatiempo = theDto.getSysMarcatiempo();     
		Long sysProcessid = theDto.getSysProcessid();     
		Long sysLowdatetime = theDto.getSysLowdatetime();     
		Long sysHighdatetime = theDto.getSysHighdatetime();     
		Long ponderacion = theDto.getPonderacion();     
		Long firmas = theDto.getFirmas();     
		java.util.Date fechaaplicacionhost = theDto.getFechaaplicacionhost();     
		java.util.Date listoparahost = theDto.getListoparahost();     
		java.util.Date fechahoraprocesado = theDto.getFechahoraprocesado();     
		Long token = theDto.getToken();     
		String comentariorechazo = theDto.getComentariorechazo();     
		String descripcion = theDto.getDescripcion();     
		String estatuspaybank = theDto.getEstatuspaybank();     
		String archivogenerado = theDto.getArchivogenerado();     
		java.math.BigDecimal comisioncobrada = theDto.getComisioncobrada();     
		String cuentadebito = theDto.getCuentadebito();     
		Long autorizacionhost = theDto.getAutorizacionhost();     
		String descestatus = theDto.getDescestatus();     
		Long idtransaccion = theDto.getIdtransaccion();     
		Long intactualizacion = theDto.getIntactualizacion();     
		java.math.BigDecimal montoimpuesto = theDto.getMontoimpuesto();     
		     
		SQLUtils.setIntoStatement( 1, convenio, smt);  
    		     
		SQLUtils.setIntoStatement( 2, empresaodepto, smt);  
    		     
		SQLUtils.setIntoStatement( 3, tipolote, smt);  
    		     
		SQLUtils.setIntoStatement( 4, montototal, smt);  
    		     
		SQLUtils.setIntoStatement( 5, numoperaciones, smt);  
    		     
		SQLUtils.setIntoStatement( 6, nombrelote, smt);  
    		     
		SQLUtils.setIntoStatement( 7, fechaenviado, smt);  
    		     
		SQLUtils.setIntoStatement( 8, fecharecibido, smt);  
    		     
		SQLUtils.setIntoStatement( 9, fechaaplicacion, smt);  
    		     
		SQLUtils.setIntoStatement( 10, fechaestatus, smt);  
    		     
		SQLUtils.setIntoStatement( 11, estatus, smt);  
    		     
		SQLUtils.setIntoStatement( 12, usuarioingreso, smt);  
    		     
		SQLUtils.setIntoStatement( 13, autorizaciones, smt);  
    		     
		SQLUtils.setIntoStatement( 14, aplicaciondebitohost, smt);  
    		     
		SQLUtils.setIntoStatement( 15, sysMarcatiempo, smt);  
    		     
		SQLUtils.setIntoStatement( 16, sysProcessid, smt);  
    		     
		SQLUtils.setIntoStatement( 17, sysLowdatetime, smt);  
    		     
		SQLUtils.setIntoStatement( 18, sysHighdatetime, smt);  
    		     
		SQLUtils.setIntoStatement( 19, ponderacion, smt);  
    		     
		SQLUtils.setIntoStatement( 20, firmas, smt);  
    		     
		SQLUtils.setIntoStatement( 21, fechaaplicacionhost, smt);  
    		     
		SQLUtils.setIntoStatement( 22, listoparahost, smt);  
    		     
		SQLUtils.setIntoStatement( 23, fechahoraprocesado, smt);  
    		     
		SQLUtils.setIntoStatement( 24, token, smt);  
    		     
		SQLUtils.setIntoStatement( 25, comentariorechazo, smt);  
    		     
		SQLUtils.setIntoStatement( 26, descripcion, smt);  
    		     
		SQLUtils.setIntoStatement( 27, estatuspaybank, smt);  
    		     
		SQLUtils.setIntoStatement( 28, archivogenerado, smt);  
    		     
		SQLUtils.setIntoStatement( 29, comisioncobrada, smt);  
    		     
		SQLUtils.setIntoStatement( 30, cuentadebito, smt);  
    		     
		SQLUtils.setIntoStatement( 31, autorizacionhost, smt);  
    		     
		SQLUtils.setIntoStatement( 32, descestatus, smt);  
    		     
		SQLUtils.setIntoStatement( 33, idtransaccion, smt);  
    		     
		SQLUtils.setIntoStatement( 34, intactualizacion, smt);  
    		     
		SQLUtils.setIntoStatement( 35, montoimpuesto, smt);  
    		
	}

	/**
	 * M&eacute;todo que coloca los valores provenientes del objeto Dto en el objeto PreparedStatement,
	 * el cual proviene de la llamada a la implementacion de {@link SQLOperaciones}, este m&eacute;todo
	 * toma en cuenta el <code>id</code>, lo que quiere decir que se usa al momento de  crear registros
	 * en donde el identificador no es un valor secuencial autonumerico o serial propio de la base de datos
	 *
	 * @param <code>theDtoUntyped</code> objeto sin tipo que almacena al DTO del negocio.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaCrearConIdST(Object theDtoUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {			
	        BsLoteach theDto = (BsLoteach)theDtoUntyped;
				Long codlote = theDto.getCodlote();     
		Long instalacion = theDto.getInstalacion();     
		Long lote = theDto.getLote();     
		Long convenio = theDto.getConvenio();     
		Long empresaodepto = theDto.getEmpresaodepto();     
		Long tipolote = theDto.getTipolote();     
		java.math.BigDecimal montototal = theDto.getMontototal();     
		Long numoperaciones = theDto.getNumoperaciones();     
		String nombrelote = theDto.getNombrelote();     
		java.util.Date fechaenviado = theDto.getFechaenviado();     
		java.util.Date fecharecibido = theDto.getFecharecibido();     
		java.util.Date fechaaplicacion = theDto.getFechaaplicacion();     
		java.util.Date fechaestatus = theDto.getFechaestatus();     
		Long estatus = theDto.getEstatus();     
		String usuarioingreso = theDto.getUsuarioingreso();     
		Long autorizaciones = theDto.getAutorizaciones();     
		Long aplicaciondebitohost = theDto.getAplicaciondebitohost();     
		java.util.Date sysMarcatiempo = theDto.getSysMarcatiempo();     
		Long sysProcessid = theDto.getSysProcessid();     
		Long sysLowdatetime = theDto.getSysLowdatetime();     
		Long sysHighdatetime = theDto.getSysHighdatetime();     
		Long ponderacion = theDto.getPonderacion();     
		Long firmas = theDto.getFirmas();     
		java.util.Date fechaaplicacionhost = theDto.getFechaaplicacionhost();     
		java.util.Date listoparahost = theDto.getListoparahost();     
		java.util.Date fechahoraprocesado = theDto.getFechahoraprocesado();     
		Long token = theDto.getToken();     
		String comentariorechazo = theDto.getComentariorechazo();     
		String descripcion = theDto.getDescripcion();     
		String estatuspaybank = theDto.getEstatuspaybank();     
		String archivogenerado = theDto.getArchivogenerado();     
		java.math.BigDecimal comisioncobrada = theDto.getComisioncobrada();     
		String cuentadebito = theDto.getCuentadebito();     
		Long autorizacionhost = theDto.getAutorizacionhost();     
		String descestatus = theDto.getDescestatus();     
		Long idtransaccion = theDto.getIdtransaccion();     
		Long intactualizacion = theDto.getIntactualizacion();     
		java.math.BigDecimal montoimpuesto = theDto.getMontoimpuesto();     
	
		SQLUtils.setIntoStatement( 1, codlote, smt);
    	
		SQLUtils.setIntoStatement( 2, instalacion, smt);
    	
		SQLUtils.setIntoStatement( 3, lote, smt);
    	
		SQLUtils.setIntoStatement( 4, convenio, smt);
    	
		SQLUtils.setIntoStatement( 5, empresaodepto, smt);
    	
		SQLUtils.setIntoStatement( 6, tipolote, smt);
    	
		SQLUtils.setIntoStatement( 7, montototal, smt);
    	
		SQLUtils.setIntoStatement( 8, numoperaciones, smt);
    	
		SQLUtils.setIntoStatement( 9, nombrelote, smt);
    	
		SQLUtils.setIntoStatement( 10, fechaenviado, smt);
    	
		SQLUtils.setIntoStatement( 11, fecharecibido, smt);
    	
		SQLUtils.setIntoStatement( 12, fechaaplicacion, smt);
    	
		SQLUtils.setIntoStatement( 13, fechaestatus, smt);
    	
		SQLUtils.setIntoStatement( 14, estatus, smt);
    	
		SQLUtils.setIntoStatement( 15, usuarioingreso, smt);
    	
		SQLUtils.setIntoStatement( 16, autorizaciones, smt);
    	
		SQLUtils.setIntoStatement( 17, aplicaciondebitohost, smt);
    	
		SQLUtils.setIntoStatement( 18, sysMarcatiempo, smt);
    	
		SQLUtils.setIntoStatement( 19, sysProcessid, smt);
    	
		SQLUtils.setIntoStatement( 20, sysLowdatetime, smt);
    	
		SQLUtils.setIntoStatement( 21, sysHighdatetime, smt);
    	
		SQLUtils.setIntoStatement( 22, ponderacion, smt);
    	
		SQLUtils.setIntoStatement( 23, firmas, smt);
    	
		SQLUtils.setIntoStatement( 24, fechaaplicacionhost, smt);
    	
		SQLUtils.setIntoStatement( 25, listoparahost, smt);
    	
		SQLUtils.setIntoStatement( 26, fechahoraprocesado, smt);
    	
		SQLUtils.setIntoStatement( 27, token, smt);
    	
		SQLUtils.setIntoStatement( 28, comentariorechazo, smt);
    	
		SQLUtils.setIntoStatement( 29, descripcion, smt);
    	
		SQLUtils.setIntoStatement( 30, estatuspaybank, smt);
    	
		SQLUtils.setIntoStatement( 31, archivogenerado, smt);
    	
		SQLUtils.setIntoStatement( 32, comisioncobrada, smt);
    	
		SQLUtils.setIntoStatement( 33, cuentadebito, smt);
    	
		SQLUtils.setIntoStatement( 34, autorizacionhost, smt);
    	
		SQLUtils.setIntoStatement( 35, descestatus, smt);
    	
		SQLUtils.setIntoStatement( 36, idtransaccion, smt);
    	
		SQLUtils.setIntoStatement( 37, intactualizacion, smt);
    	
		SQLUtils.setIntoStatement( 38, montoimpuesto, smt);
    		
	}

	/**
	 * M&eacute;todo que coloca el identificador recibido en el objeto PreparedStatement, para poder ser eliminado.
	 * @param <code>theIdUntyped</code> objeto sin tipo que identifica al <code>id</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaEliminarST(Object theIdUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {
			BsLoteachID theId = (BsLoteachID)theIdUntyped;
			Long codlote = theId.getCodlote();     
		Long instalacion = theId.getInstalacion();     
		Long lote = theId.getLote();     
		     
		SQLUtils.setIntoStatement( 1, codlote, smt);  
    		     
		SQLUtils.setIntoStatement( 2, instalacion, smt);  
    		     
		SQLUtils.setIntoStatement( 3, lote, smt);  
    		}

	/**
	 * M&eacute;todo que coloca el identificador recibido, en el objeto PreparedStatement para poder hacer la consulta de busqueda.
	 * @param <code>theIdUntyped</code> objeto sin tipo que identifica al <code>id</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaBuscarPorIdST(Object theIdUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {
			BsLoteachID theId = (BsLoteachID)theIdUntyped;
				Long codlote = theId.getCodlote();     
		Long instalacion = theId.getInstalacion();     
		Long lote = theId.getLote();     
		     
		SQLUtils.setIntoStatement( 1, codlote, smt);  
    		     
		SQLUtils.setIntoStatement( 2, instalacion, smt);  
    		     
		SQLUtils.setIntoStatement( 3, lote, smt);  
    			}

	/**
	 * M&eacute;todo que coloca los valores provenientes del objeto Dto en el objeto PreparedStatement,
	 * el cual proviene de la llamada a la implementacion de {@link SQLOperaciones} para poder actualizar
     * un registro en espec&iacute;fico.
	 * 
	 * @param <code>theDtoUntyped</code> objeto sin tipo que almacena al DTO del negocio.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaActualizarST(Object theDtoUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {
			BsLoteach theDto = (BsLoteach)theDtoUntyped;
				Long codlote = theDto.getCodlote();     
		Long instalacion = theDto.getInstalacion();     
		Long lote = theDto.getLote();     
		Long convenio = theDto.getConvenio();     
		Long empresaodepto = theDto.getEmpresaodepto();     
		Long tipolote = theDto.getTipolote();     
		java.math.BigDecimal montototal = theDto.getMontototal();     
		Long numoperaciones = theDto.getNumoperaciones();     
		String nombrelote = theDto.getNombrelote();     
		java.util.Date fechaenviado = theDto.getFechaenviado();     
		java.util.Date fecharecibido = theDto.getFecharecibido();     
		java.util.Date fechaaplicacion = theDto.getFechaaplicacion();     
		java.util.Date fechaestatus = theDto.getFechaestatus();     
		Long estatus = theDto.getEstatus();     
		String usuarioingreso = theDto.getUsuarioingreso();     
		Long autorizaciones = theDto.getAutorizaciones();     
		Long aplicaciondebitohost = theDto.getAplicaciondebitohost();     
		java.util.Date sysMarcatiempo = theDto.getSysMarcatiempo();     
		Long sysProcessid = theDto.getSysProcessid();     
		Long sysLowdatetime = theDto.getSysLowdatetime();     
		Long sysHighdatetime = theDto.getSysHighdatetime();     
		Long ponderacion = theDto.getPonderacion();     
		Long firmas = theDto.getFirmas();     
		java.util.Date fechaaplicacionhost = theDto.getFechaaplicacionhost();     
		java.util.Date listoparahost = theDto.getListoparahost();     
		java.util.Date fechahoraprocesado = theDto.getFechahoraprocesado();     
		Long token = theDto.getToken();     
		String comentariorechazo = theDto.getComentariorechazo();     
		String descripcion = theDto.getDescripcion();     
		String estatuspaybank = theDto.getEstatuspaybank();     
		String archivogenerado = theDto.getArchivogenerado();     
		java.math.BigDecimal comisioncobrada = theDto.getComisioncobrada();     
		String cuentadebito = theDto.getCuentadebito();     
		Long autorizacionhost = theDto.getAutorizacionhost();     
		String descestatus = theDto.getDescestatus();     
		Long idtransaccion = theDto.getIdtransaccion();     
		Long intactualizacion = theDto.getIntactualizacion();     
		java.math.BigDecimal montoimpuesto = theDto.getMontoimpuesto();     
		     
		SQLUtils.setIntoStatement( 1, convenio, smt);  
        	     
		SQLUtils.setIntoStatement( 2, empresaodepto, smt);  
        	     
		SQLUtils.setIntoStatement( 3, tipolote, smt);  
        	     
		SQLUtils.setIntoStatement( 4, montototal, smt);  
        	     
		SQLUtils.setIntoStatement( 5, numoperaciones, smt);  
        	     
		SQLUtils.setIntoStatement( 6, nombrelote, smt);  
        	     
		SQLUtils.setIntoStatement( 7, fechaenviado, smt);  
        	     
		SQLUtils.setIntoStatement( 8, fecharecibido, smt);  
        	     
		SQLUtils.setIntoStatement( 9, fechaaplicacion, smt);  
        	     
		SQLUtils.setIntoStatement( 10, fechaestatus, smt);  
        	     
		SQLUtils.setIntoStatement( 11, estatus, smt);  
        	     
		SQLUtils.setIntoStatement( 12, usuarioingreso, smt);  
        	     
		SQLUtils.setIntoStatement( 13, autorizaciones, smt);  
        	     
		SQLUtils.setIntoStatement( 14, aplicaciondebitohost, smt);  
        	     
		SQLUtils.setIntoStatement( 15, sysMarcatiempo, smt);  
        	     
		SQLUtils.setIntoStatement( 16, sysProcessid, smt);  
        	     
		SQLUtils.setIntoStatement( 17, sysLowdatetime, smt);  
        	     
		SQLUtils.setIntoStatement( 18, sysHighdatetime, smt);  
        	     
		SQLUtils.setIntoStatement( 19, ponderacion, smt);  
        	     
		SQLUtils.setIntoStatement( 20, firmas, smt);  
        	     
		SQLUtils.setIntoStatement( 21, fechaaplicacionhost, smt);  
        	     
		SQLUtils.setIntoStatement( 22, listoparahost, smt);  
        	     
		SQLUtils.setIntoStatement( 23, fechahoraprocesado, smt);  
        	     
		SQLUtils.setIntoStatement( 24, token, smt);  
        	     
		SQLUtils.setIntoStatement( 25, comentariorechazo, smt);  
        	     
		SQLUtils.setIntoStatement( 26, descripcion, smt);  
        	     
		SQLUtils.setIntoStatement( 27, estatuspaybank, smt);  
        	     
		SQLUtils.setIntoStatement( 28, archivogenerado, smt);  
        	     
		SQLUtils.setIntoStatement( 29, comisioncobrada, smt);  
        	     
		SQLUtils.setIntoStatement( 30, cuentadebito, smt);  
        	     
		SQLUtils.setIntoStatement( 31, autorizacionhost, smt);  
        	     
		SQLUtils.setIntoStatement( 32, descestatus, smt);  
        	     
		SQLUtils.setIntoStatement( 33, idtransaccion, smt);  
        	     
		SQLUtils.setIntoStatement( 34, intactualizacion, smt);  
        	     
		SQLUtils.setIntoStatement( 35, montoimpuesto, smt);  
        		     
		SQLUtils.setIntoStatement( 36, codlote, smt);  
    		     
		SQLUtils.setIntoStatement( 37, instalacion, smt);  
    		     
		SQLUtils.setIntoStatement( 38, lote, smt);  
    			
	}


	/**
	 * M&eacute;todo que ejecuta una petici&oacute;n de datos a trav&eacute;s de un objeto {@link PreparedStatement},
	 * y un objeto del tipo {@link PeticionDeDatos}, el cual en su implementaci&oacute;n define una serie de restricciones
	 * usadas para hacer la petici&oacute;n.
	 *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaPeticionDeDatosST(PeticionDeDatos peticion,
			PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
		SQLImplementacionOperacionesComunes.operacionesParaPeticionDeDatosST( peticion, smt, SQL_OPERACIONES, null);	
	}	
	
	/**
	 * M&eacute;todo que ejecuta una petici&oacute;n de datos a trav&eacute;s de un objeto {@link PreparedStatement},
     * y un objeto del tipo {@link PeticionDeDatos}, el cual en su implementaci&oacute;n define una serie de restricciones
     * usadas para hacer la petici&oacute;n.
	 *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaPeticionDeDatosFST(PeticionDeDatos peticion,
			PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
		SQLImplementacionOperacionesComunes.operacionesParaPeticionDeDatosST( peticion, smt, SQL_OPERACIONES, null);	
	}	

	/**
	 * M&eacute;todo que ejecuta una petici&oacute;n de conteo a trav&eacute;s de un objeto {@link PreparedStatement},
     * y un objeto del tipo {@link PeticionDeDatos}, el cual en su implementaci&oacute;n define una serie de restricciones
     * usadas para hacer la petici&oacute;n.
	 *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaConteoST(PeticionDeDatos peticion,
			PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
		SQLImplementacionOperacionesComunes.operacionesParaPeticionDeDatosST( peticion, smt, SQL_OPERACIONES, null);
	}

	/**
	 * M&eacute;todo que agrega al atributo {@link #SELECT_HEADER_SQL} las condiciones de b&uacute;squeda
     * que vienen en el objeto del tipo {@link PeticionDeDatos}
     *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>, que contiene las condiciones de la consulta.
	 * @return <code>String</code> - la consulta condicionada como cadena de caracteres.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static String getSqlParaPeticionDeDatosST(PeticionDeDatos peticion)
			throws SQLException, ExcepcionEnDAO {
		return SELECT_HEADER_SQL + SQLImplementacionOperacionesComunes.crearCondicionesSQLST( peticion, SQL_OPERACIONES, SQLImplementacionOperacionesComunes.COND_ALIAS_ORDEN_WHERE);
	}
	
	
	
	/**
	 * M&eacute;todo que agrega al atributo {@link #SELECT_HEADER_SQL_F} las condiciones de b&uacute;squeda
     * que vienen en el objeto del tipo {@link PeticionDeDatos}
     *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>, que contiene las condiciones de la consulta.
	 * @return <code>String</code> - la consulta condicionada como cadena de caracteres.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static String getSqlParaPeticionDeDatosFST(PeticionDeDatos peticion)
			throws SQLException, ExcepcionEnDAO {
		return SELECT_HEADER_SQL_F + SQLImplementacionOperacionesComunes.crearCondicionesSQLST( peticion, SQL_OPERACIONES, SQLImplementacionOperacionesComunes.COND_ALIAS_ORDEN_AND);
	}
	

	

	/**
	 * M&eacute;todo que agrega al atributo {@link #COUNT_HEADER_SQL} las condiciones de conteo
	 * que vienen en el objeto de tipo {@link PeticionDeDatos}.
	 *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>, que contiene las condiciones de la consulta.
	 * @return <code>String</code> - la consulta condicionada como cadena de caracteres.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static String getSqlDeConteoST(PeticionDeDatos peticion)
			throws SQLException, ExcepcionEnDAO {
		return COUNT_HEADER_SQL + SQLImplementacionOperacionesComunes.crearCondicionesSQLST( peticion, SQL_OPERACIONES, SQLImplementacionOperacionesComunes.COND_SQL_WHERE);
	}
	
	/**
	 * M&eacute;todo que se utiliza para actualizar un registro a nivel de tabla, recibe como par&aacute;metro un DTO del negocio
	 * que contiene la informaci&oacute;n a actualizar, e invoca la funci&oacute;n <code>actualizar</code> de la clase de la que hereda.
	 *
	 * @param <code>bsLoteach</code> objeto del tipo <code>BsLoteach</code>.
	 * @throws ExcepcionEnDAO
	 * @see {@link SQLImplementacionBasica}
	 */
	public void actualizar(BsLoteach bsLoteach) throws ExcepcionEnDAO {
	    actualizar(bsLoteach, AUTO_COMMIT);
	
	}
	
	/**
	 * M&eacute;todo utilizado para realizar una b&uacute;squeda de un registro espec&iacute;fico, utilizando un identificador.
	 *
	 * @param <code>bsLoteachID</code> objeto del tipo <code>BsLoteachID</code>.
	 * @return <code>BsLoteach</code> - un objeto DTO.
	 * @throws ExcepcionEnDAO
	 * @see {@link SQLImplementacionBasica}
	 */
	public BsLoteach buscarPorID(BsLoteachID bsLoteachID)
			throws ExcepcionEnDAO {
		return (BsLoteach)buscarPorID(bsLoteachID, AUTO_COMMIT);				 
	}

	/**
	 * M&eacute;todo utilizado para la creaci&oacute;n de un registro, a nivel de tabla, a partir de la informaci&oacute;n
	 * recibida en un DTO del objeto de Negocio, e invoca la funci&oacute;n <code>crear</code> de la clase de la que hereda.
	 * 
	 * @param <code>bsLoteach</code> objeto del tipo <code>BsLoteach</code>.
	 * @throws ExcepcionEnDAO
	 * @see {@link SQLImplementacionBasica}
	 */
	public void crear(BsLoteach bsLoteach) throws ExcepcionEnDAO {
	    crear(bsLoteach, AUTO_COMMIT);		
	}

	/**
	 * M&eacute;todo utilizado para la eliminaci&oacute;n de un registro, a nivel de tabla,
	 * que se busca a trav&eacute;s del identificador recibido. Se invoca la funci&oacute;n <code>eliminar</code> de la clase de la que hereda.
	 *
	 * @param <code>bsLoteachID</code> objeto del tipo <code>BsLoteachID</code>.
	 * @throws ExcepcionEnDAO
	 * @see {@link SQLImplementacionBasica}
	 */
	public void eliminar(BsLoteachID bsLoteachID) throws ExcepcionEnDAO {
	    eliminar(bsLoteachID, AUTO_COMMIT);		
	}

	/**
	 * M&eacute;todo utilizado para devolver un &uacute;nico resultado de la petici&oacute;n recibida,
	 * la cual se realiza a trav&eacuate;s del objeto de tipo {@link PeticionDeDatos}.
	 * 
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>, que contiene las condiciones de la consulta.
	 * @return <code>BsLoteach</code> - un objeto DTO.
	 * @throws ExcepcionEnDAO
	 * @throws ExcepcionPorDatoNoEncontrado
	 * @see {@link SQLImplementacionBasica}
	 */
	public BsLoteach resultadoUnico(PeticionDeDatos peticion)
			throws ExcepcionEnDAO, ExcepcionPorDatoNoEncontrado {
		return (BsLoteach)resultadoUnico(peticion, AUTO_COMMIT);
	}
	
	/**
	 * Atributo del tipo {@link SQLOperaciones}, almacena la operaciones disponibles para esta implementaci&oacute;n.<br/>
	 * Algunas de las llamadas a los m&eacute;todos se realiza a trav&eacute;s de la invocaci&oacute;n de estas implementaciones.
	 */
    public static final SQLOperaciones	SQL_OPERACIONES =  new SQLOperaciones() {
			
			public int getManejoForaneosPorDefecto() {
				return Dao.FORANEOS_JOIN;
			}
			
			
			public void operacionesParaPeticionDeDatosF(PeticionDeDatos request,
					PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
				BsLoteachDaoJdbcSql.operacionesParaPeticionDeDatosFST(request, smt);
			}
			
			public void operacionesParaPeticionDeDatos(PeticionDeDatos request,
					PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
				BsLoteachDaoJdbcSql.operacionesParaPeticionDeDatosST(request, smt);
			}
			
			public void operacionesParaEliminar(Object theId, PreparedStatement smt)
					throws SQLException, ExcepcionEnDAO {
				BsLoteachDaoJdbcSql.operacionesParaEliminarST(theId, smt);
			}
			
			public void operacionesParaCrear(Object theDto, PreparedStatement smt)
					throws SQLException, ExcepcionEnDAO {
				BsLoteachDaoJdbcSql.operacionesParaCrearST(theDto, smt);
			}
			
			public void operacionesParaCrearConId(Object theDto, PreparedStatement smt) 
			        throws SQLException, ExcepcionEnDAO {
			    BsLoteachDaoJdbcSql.operacionesParaCrearConIdST(theDto, smt);
            }
			
			public void operacionesParaConteo(PeticionDeDatos request,
					PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
				BsLoteachDaoJdbcSql.operacionesParaConteoST(request, smt);				
			}
			
			public void operacionesParaBuscarPorId(Object theId, PreparedStatement smt)
					throws SQLException, ExcepcionEnDAO {
				BsLoteachDaoJdbcSql.operacionesParaBuscarPorIdST(theId, smt);
			}
			
			public void operacionesParaActualizar(Object theDto, PreparedStatement smt)
					throws SQLException, ExcepcionEnDAO {
				BsLoteachDaoJdbcSql.operacionesParaActualizarST(theDto, smt);
				
			}
			
			public String getSqlParaPeticionDeDatosF(PeticionDeDatos request)
					throws SQLException, ExcepcionEnDAO {	
				return BsLoteachDaoJdbcSql.getSqlParaPeticionDeDatosFST(request);
			}
			
			public String getSqlParaPeticionDeDatos(PeticionDeDatos request)
					throws SQLException, ExcepcionEnDAO {
				return BsLoteachDaoJdbcSql.getSqlParaPeticionDeDatosST(request);
			}
			
			public String getSqlDeConteo(PeticionDeDatos request) throws SQLException,
					ExcepcionEnDAO {
				return BsLoteachDaoJdbcSql.getSqlDeConteoST(request);
			}
			
			public SQLEntidadMeta getSQLEntidadMeta() throws ExcepcionEnDAO {
				return BsLoteachDaoJdbcSql.SQL_ENTIDAD_META;
			}
			
			public String getEliminarSql() {
				return  BsLoteachDaoJdbcSql.DELETE_SQL;
			}
			
			public String getCrearSql() {
				return  BsLoteachDaoJdbcSql.CREATE_SQL;
			}
			
			public String getCrearSqlConId(){
			    return  BsLoteachDaoJdbcSql.CREATE_SQL_CON_ID;
			}			
			
			public String getBuscarPorIdSqlF() {
				return BsLoteachDaoJdbcSql.FIND_BY_ID_SQL_F;
			}
			
			public String getBuscarPorIdSql() {				
				return BsLoteachDaoJdbcSql.FIND_BY_ID_SQL;
			}
			
			public String getActualizarSql() {				
				return BsLoteachDaoJdbcSql.UPDATE_SQL;
			}
			
			public Object crearDto(ResultSet rst) throws SQLException, ExcepcionEnDAO {
				return BsLoteachDaoJdbcSql.crearDtoST(rst);
			}
	        
	        public Object crearDataConValoresForaneos(ResultSet rst)
					throws SQLException, ExcepcionEnDAO {
				return BsLoteachDaoJdbcSql.crearDataConValoresForaneosST(rst);
			}
			
	        public Object crearDataConValoresForaneosNoRel(ResultSet rst, Connection conn)
					throws SQLException, ExcepcionEnDAO {
				return BsLoteachDaoJdbcSql.crearDataConValoresForaneosNoRelST(rst, conn);
			}
			
			
            public boolean isForaneosDisponible(){
				return true;
            
            }
            
            public SQLLenguaje getSQLLenguaje() {
                return BsLoteachDaoJdbcSql.getLenguaje();
            }
            
            public void setIdEnDto(Object theDto, Object dtoId) {
                setIdEnDtoST(theDto, dtoId);
            }
	};
	
	
	
	
	
	/**
	 * Devuelve el atributo {@link #SQL_OPERACIONES}.
	 * @return <code>SQLOperaciones</code> - objeto del tipo {@link SQLOperaciones}
	 */
    public SQLOperaciones getSqlOperaciones() {
       return SQL_OPERACIONES;
	}
	
	/**
	 * Devuelve el atributo {@link #lenguaje}
	 * @return <code>SQLLenguaje</code> - objeto del tipo {@link SQLLenguaje}
	 */	
	public static SQLLenguaje getLenguaje() {
		return lenguaje;
	}
	
	/**
	 * M&eacute;todo que permite obtener a trav&eacute;s de un Servicio la implementaci&oacute;n del objeto de Lenguaje,
	 * y coloca las reglas de negocio almacenadas en el atributo {@link #SQL_ENTIDAD_META}.
	 * @param <code>localizadorLenguaje</code> objeto del tipo {@link LocalizadorDeServicio} que posee informaci&oacute;n para trabajar con la base de datos.
	 */
	public static void setLocalizadorLenguaje(LocalizadorDeServicio localizadorLenguaje) {
		lenguaje = (SQLLenguaje) localizadorLenguaje.getServicio();
		lenguaje.setSqlEntidadMeta(SQL_ENTIDAD_META);
	}

	/**
	 * M&eacute;todo que agrega un identificador a un DTO del tipo <code>BsLoteach</code>.
	 * @param <code>theDtoUntyped</code> objeto gen&eacute;rico, que representa a un <code>DTO</code>.
	 * @param <code>dtoIdUntyped</code> objeto gen&eacute;rico, que representa al identificador.
	 */
	public static void setIdEnDtoST(Object theDtoUntyped, Object dtoIdUntyped){
		BsLoteach dto = (BsLoteach)theDtoUntyped;
		BsLoteachID theId = (BsLoteachID)dtoIdUntyped;
		Long codlote = theId.getCodlote();     
		Long instalacion = theId.getInstalacion();     
		Long lote = theId.getLote();     
		dto.setCodlote(codlote);
		dto.setInstalacion(instalacion);
		dto.setLote(lote);

	}	
	
	
}
