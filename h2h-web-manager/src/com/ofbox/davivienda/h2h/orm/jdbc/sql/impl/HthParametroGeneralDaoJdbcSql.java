package com.ofbox.davivienda.h2h.orm.jdbc.sql.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.sql.Connection;
import java.util.Collection;

import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Dao;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.ExcepcionEnDAO;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.ExcepcionPorDatoNoEncontrado;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.PeticionDeDatos;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Transaccion;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.foraneos.DataConValoresForaneos;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLColumnaMeta;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLEntidadMeta;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLImplementacionBasica;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLImplementacionOperacionesComunes;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLLenguaje;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLOperaciones;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLUtils;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.TransaccionJdbc;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dependencias.LocalizadorDeServicio;


import com.ofbox.davivienda.h2h.abstraccion.dto.HthParametroGeneral;
import java.lang.Long;
import com.ofbox.davivienda.h2h.abstraccion.dao.HthParametroGeneralDao;




/**
 * Clase que hereda comportamientos de {@link SQLImplementacionBasica} e implementa el comportamiento definido en la capa DAO.<br/>
 * Esta clase es una implementaci&oacute;n intermedia para el DAO de <b>Par&aacutemetro General</b>
 * asociado a la tabla <b>HTH_Parametro_General</b>, en esta clase se construyen todos los sql para las operaciones de listado,
 * creaci&oacute;n, consulta, actualizaci&oacute;n y elminaci&oacute;n.<br/>
 * @author Systems Out of the Box
 * @version f3.1
 *
 */
public class HthParametroGeneralDaoJdbcSql extends SQLImplementacionBasica implements HthParametroGeneralDao{

	/**
	 * Atributo del tipo {@link SQLEntidadMeta}, que almacena la descripci&oacute;n del negocio como una entidad.<br/>
	 * Entre sus caracter&iacute;sticas tiene el nombre de la tabla, nombre de la secuencia, posici&oacute;n de las columnas,
	 * nombre de las columnas y tipos de datos para la columna.<br/>
	 * El arreglo que recibe son objetos de tipo {@link SQLColumnaMeta}, y definen informaci&oacute;n para obtener las columnas.
	 */
	public static final SQLEntidadMeta SQL_ENTIDAD_META = new SQLEntidadMeta("HTH_Parametro_General", new SQLColumnaMeta[]{
        new SQLColumnaMeta(17, "HTH_Parametro_General", "E17", 17001, "id_parametro_general", "C17001", Types.BIGINT ,true)
     ,  new SQLColumnaMeta(17, "HTH_Parametro_General", "E17", 17002, "nombre_parametro", "C17002", Types.VARCHAR )
     ,  new SQLColumnaMeta(17, "HTH_Parametro_General", "E17", 17003, "valor_parametro", "C17003", Types.VARCHAR )
     ,  new SQLColumnaMeta(17, "HTH_Parametro_General", "E17", 17004, "usuario_creacion", "C17004", Types.VARCHAR )
     ,  new SQLColumnaMeta(17, "HTH_Parametro_General", "E17", 17005, "fecha_creacion", "C17005", Types.TIMESTAMP )
     ,  new SQLColumnaMeta(17, "HTH_Parametro_General", "E17", 17006, "usuario_ult_act", "C17006", Types.VARCHAR )
     ,  new SQLColumnaMeta(17, "HTH_Parametro_General", "E17", 17007, "fecha_ult_act", "C17007", Types.TIMESTAMP )
	});
	
	


	/**
	 * Atributo que almacena el <code>String</code> para obtener el listado de registros del negocio.
	 */
	public static final String SELECT_HEADER_SQL =  
		"SELECT E17.id_parametro_general AS C17001 , E17.nombre_parametro AS C17002 , E17.valor_parametro AS C17003 , E17.usuario_creacion AS C17004 , " +
		"       E17.fecha_creacion AS C17005 , E17.usuario_ult_act AS C17006 , E17.fecha_ult_act AS C17007" +
		"  FROM HTH_Parametro_General E17";
	/**
	 * Atributo que almacena el <code>String</code> para obtener el n&uacute;mero de registros presentes en el negocio.
	 */
	public static final String COUNT_HEADER_SQL =   "SELECT COUNT(*) FROM HTH_Parametro_General";    												
	/**
	 * Atributo que almacena el <code>String</code> para la inserci&oacute;n, sin id, de un nuevo registro en la tabla.
	 */
	public static final String CREATE_SQL = 
		"INSERT INTO HTH_Parametro_General (" +
		"nombre_parametro,valor_parametro,usuario_creacion,fecha_creacion,usuario_ult_act" +
		",fecha_ult_act" +
		") VALUES (?,?,?,?,?,?)";
	/**
	 * Atributo que almacena el <code>String</code> para insertar, con id, un registro en la table.
	 */
	public static final String CREATE_SQL_CON_ID = 
		"INSERT INTO HTH_Parametro_General (" +
		"id_parametro_general,nombre_parametro,valor_parametro,usuario_creacion,fecha_creacion" +
		",usuario_ult_act,fecha_ult_act" +
		") VALUES (?,?,?,?,?,?,?)";
	/**
	 * Atributo que almacena el <code>String</code> para la atualizaci&oacute; de un registro en la tabla.
	 */
	public static final String UPDATE_SQL = 
		"UPDATE HTH_Parametro_General" +
		" SET nombre_parametro= ?  , valor_parametro= ?  , usuario_creacion= ?  , fecha_creacion= ?  , " +
		"usuario_ult_act= ?  , fecha_ult_act= ? " +
		" WHERE id_parametro_general= ? ";
	/**
	 * Atributo que almacena el <code>String</code> para la eliminaci&oacute; de un registro en la tabla.
	 */
	public static final String DELETE_SQL =  
		"DELETE FROM HTH_Parametro_General" +
		" WHERE id_parametro_general= ? ";
	/**
	 * Atributo que almacena el <code>String</code> para la b&uacute;squeda por id de un registro de la tabla.
	 */
	public static final String FIND_BY_ID_SQL = 
		"SELECT E17.id_parametro_general AS C17001 , E17.nombre_parametro AS C17002 , E17.valor_parametro AS C17003 , E17.usuario_creacion AS C17004 , " +
		"       E17.fecha_creacion AS C17005 , E17.usuario_ult_act AS C17006 , E17.fecha_ult_act AS C17007" +
		"  FROM HTH_Parametro_General E17" +
		" WHERE E17.id_parametro_general= ? ";
	/**
	 * Atributo que almacena el <code>String</code> para la creacion de la tabla en la base de datos.
	 */
	public static final String CREATE_TABLE_DDL = 
		"CREATE TABLE HTH_Parametro_General (" +
		"    id_parametro_general LONG NOT NULL," +
		"    nombre_parametro VARCHAR(100)  NOT NULL," +
		"    valor_parametro VARCHAR(200)  NOT NULL," +
		"    usuario_creacion VARCHAR(100)  NOT NULL," +
		"    fecha_creacion TIMESTAMP NOT NULL," +
		"    usuario_ult_act VARCHAR(100)  NOT NULL," +
		"    fecha_ult_act TIMESTAMP NOT NULL" +
		")";
	/**
	 * Atributo que almacena un objeto del tipo {@link SQLLenguaje}.
	 */
	private static SQLLenguaje lenguaje;
	
	/**
	 * M&eacute;todo que realiza la creaci&oacute;n de un DTO, del negocio, a partir del resultado de alguna consulta.
	 * @param <code>rst</code> objeto de tipo <code>Resultset</code>, que contiene los resultados de la consulta.
	 * @return <code>Object</code> - representa al DTO creado.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static Object crearDtoST(ResultSet rst)
			throws SQLException, ExcepcionEnDAO {
 	    HthParametroGeneral theDto = new HthParametroGeneral();
     	theDto.setIdParametroGeneral(SQLUtils.getLongFromResultSet(rst, "C17001"));
		theDto.setNombreParametro(rst.getString("C17002"));
		theDto.setValorParametro(rst.getString("C17003"));
		theDto.setUsuarioCreacion(rst.getString("C17004"));
		theDto.setFechaCreacion(rst.getTimestamp("C17005"));
		theDto.setUsuarioUltAct(rst.getString("C17006"));
		theDto.setFechaUltAct(rst.getTimestamp("C17007"));
		return theDto;
	}
	
	







	/**
	 * M&eacute;todo que coloca los valores provenientes del objeto Dto en el objeto PreparedStatement,
	 * el cual proviene de la llamada a la implementacion de {@link SQLOperaciones}, este m&eacute;todo
	 * no toma en cuenta el valor del <code>id</code>, puesto que se asume que es un valor secuencial
	 * autonumerico o serial propio de la base de datos.
	 * 
	 * @param <code>theDtoUntyped</code> objeto sin tipo que almacena al DTO del negocio.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaCrearST(Object theDtoUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {			
	        HthParametroGeneral theDto = (HthParametroGeneral)theDtoUntyped;
				String nombreParametro = theDto.getNombreParametro();     
		String valorParametro = theDto.getValorParametro();     
		String usuarioCreacion = theDto.getUsuarioCreacion();     
		java.util.Date fechaCreacion = theDto.getFechaCreacion();     
		String usuarioUltAct = theDto.getUsuarioUltAct();     
		java.util.Date fechaUltAct = theDto.getFechaUltAct();     
		     
		SQLUtils.setIntoStatement( 1, nombreParametro, smt);  
    		     
		SQLUtils.setIntoStatement( 2, valorParametro, smt);  
    		     
		SQLUtils.setIntoStatement( 3, usuarioCreacion, smt);  
    		     
		SQLUtils.setIntoStatement( 4, fechaCreacion, smt);  
    		     
		SQLUtils.setIntoStatement( 5, usuarioUltAct, smt);  
    		     
		SQLUtils.setIntoStatement( 6, fechaUltAct, smt);  
    		
	}

	/**
	 * M&eacute;todo que coloca los valores provenientes del objeto Dto en el objeto PreparedStatement,
	 * el cual proviene de la llamada a la implementacion de {@link SQLOperaciones}, este m&eacute;todo
	 * toma en cuenta el <code>id</code>, lo que quiere decir que se usa al momento de  crear registros
	 * en donde el identificador no es un valor secuencial autonumerico o serial propio de la base de datos
	 *
	 * @param <code>theDtoUntyped</code> objeto sin tipo que almacena al DTO del negocio.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaCrearConIdST(Object theDtoUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {			
	        HthParametroGeneral theDto = (HthParametroGeneral)theDtoUntyped;
				Long idParametroGeneral = theDto.getIdParametroGeneral();     
		String nombreParametro = theDto.getNombreParametro();     
		String valorParametro = theDto.getValorParametro();     
		String usuarioCreacion = theDto.getUsuarioCreacion();     
		java.util.Date fechaCreacion = theDto.getFechaCreacion();     
		String usuarioUltAct = theDto.getUsuarioUltAct();     
		java.util.Date fechaUltAct = theDto.getFechaUltAct();     
	
		SQLUtils.setIntoStatement( 1, idParametroGeneral, smt);
    	
		SQLUtils.setIntoStatement( 2, nombreParametro, smt);
    	
		SQLUtils.setIntoStatement( 3, valorParametro, smt);
    	
		SQLUtils.setIntoStatement( 4, usuarioCreacion, smt);
    	
		SQLUtils.setIntoStatement( 5, fechaCreacion, smt);
    	
		SQLUtils.setIntoStatement( 6, usuarioUltAct, smt);
    	
		SQLUtils.setIntoStatement( 7, fechaUltAct, smt);
    		
	}

	/**
	 * M&eacute;todo que coloca el identificador recibido en el objeto PreparedStatement, para poder ser eliminado.
	 * @param <code>theIdUntyped</code> objeto sin tipo que identifica al <code>id</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaEliminarST(Object theIdUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {
			Long theId = (Long)theIdUntyped;
		SQLUtils.setIntoStatement( 1, theId, smt);
	}

	/**
	 * M&eacute;todo que coloca el identificador recibido, en el objeto PreparedStatement para poder hacer la consulta de busqueda.
	 * @param <code>theIdUntyped</code> objeto sin tipo que identifica al <code>id</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaBuscarPorIdST(Object theIdUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {
			Long theId = (Long)theIdUntyped;
			SQLUtils.setIntoStatement( 1, theId, smt);
		}

	/**
	 * M&eacute;todo que coloca los valores provenientes del objeto Dto en el objeto PreparedStatement,
	 * el cual proviene de la llamada a la implementacion de {@link SQLOperaciones} para poder actualizar
     * un registro en espec&iacute;fico.
	 * 
	 * @param <code>theDtoUntyped</code> objeto sin tipo que almacena al DTO del negocio.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaActualizarST(Object theDtoUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {
			HthParametroGeneral theDto = (HthParametroGeneral)theDtoUntyped;
				Long idParametroGeneral = theDto.getIdParametroGeneral();     
		String nombreParametro = theDto.getNombreParametro();     
		String valorParametro = theDto.getValorParametro();     
		String usuarioCreacion = theDto.getUsuarioCreacion();     
		java.util.Date fechaCreacion = theDto.getFechaCreacion();     
		String usuarioUltAct = theDto.getUsuarioUltAct();     
		java.util.Date fechaUltAct = theDto.getFechaUltAct();     
		     
		SQLUtils.setIntoStatement( 1, nombreParametro, smt);  
        	     
		SQLUtils.setIntoStatement( 2, valorParametro, smt);  
        	     
		SQLUtils.setIntoStatement( 3, usuarioCreacion, smt);  
        	     
		SQLUtils.setIntoStatement( 4, fechaCreacion, smt);  
        	     
		SQLUtils.setIntoStatement( 5, usuarioUltAct, smt);  
        	     
		SQLUtils.setIntoStatement( 6, fechaUltAct, smt);  
        		     
		SQLUtils.setIntoStatement( 7, idParametroGeneral, smt);  
    			
	}


	/**
	 * M&eacute;todo que ejecuta una petici&oacute;n de datos a trav&eacute;s de un objeto {@link PreparedStatement},
	 * y un objeto del tipo {@link PeticionDeDatos}, el cual en su implementaci&oacute;n define una serie de restricciones
	 * usadas para hacer la petici&oacute;n.
	 *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaPeticionDeDatosST(PeticionDeDatos peticion,
			PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
		SQLImplementacionOperacionesComunes.operacionesParaPeticionDeDatosST( peticion, smt, SQL_OPERACIONES, null);	
	}	
	
	/**
	 * M&eacute;todo que ejecuta una petici&oacute;n de datos a trav&eacute;s de un objeto {@link PreparedStatement},
     * y un objeto del tipo {@link PeticionDeDatos}, el cual en su implementaci&oacute;n define una serie de restricciones
     * usadas para hacer la petici&oacute;n.
	 *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaPeticionDeDatosFST(PeticionDeDatos peticion,
			PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
		SQLImplementacionOperacionesComunes.operacionesParaPeticionDeDatosST( peticion, smt, SQL_OPERACIONES, null);	
	}	

	/**
	 * M&eacute;todo que ejecuta una petici&oacute;n de conteo a trav&eacute;s de un objeto {@link PreparedStatement},
     * y un objeto del tipo {@link PeticionDeDatos}, el cual en su implementaci&oacute;n define una serie de restricciones
     * usadas para hacer la petici&oacute;n.
	 *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaConteoST(PeticionDeDatos peticion,
			PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
		SQLImplementacionOperacionesComunes.operacionesParaPeticionDeDatosST( peticion, smt, SQL_OPERACIONES, null);
	}

	/**
	 * M&eacute;todo que agrega al atributo {@link #SELECT_HEADER_SQL} las condiciones de b&uacute;squeda
     * que vienen en el objeto del tipo {@link PeticionDeDatos}
     *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>, que contiene las condiciones de la consulta.
	 * @return <code>String</code> - la consulta condicionada como cadena de caracteres.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static String getSqlParaPeticionDeDatosST(PeticionDeDatos peticion)
			throws SQLException, ExcepcionEnDAO {
		return SELECT_HEADER_SQL + SQLImplementacionOperacionesComunes.crearCondicionesSQLST( peticion, SQL_OPERACIONES, SQLImplementacionOperacionesComunes.COND_ALIAS_ORDEN_WHERE);
	}
	
	
	
	

	/**
	 * M&eacute;todo que agrega al atributo {@link #COUNT_HEADER_SQL} las condiciones de conteo
	 * que vienen en el objeto de tipo {@link PeticionDeDatos}.
	 *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>, que contiene las condiciones de la consulta.
	 * @return <code>String</code> - la consulta condicionada como cadena de caracteres.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static String getSqlDeConteoST(PeticionDeDatos peticion)
			throws SQLException, ExcepcionEnDAO {
		return COUNT_HEADER_SQL + SQLImplementacionOperacionesComunes.crearCondicionesSQLST( peticion, SQL_OPERACIONES, SQLImplementacionOperacionesComunes.COND_SQL_WHERE);
	}
	
	/**
	 * M&eacute;todo que se utiliza para actualizar un registro a nivel de tabla, recibe como par&aacute;metro un DTO del negocio
	 * que contiene la informaci&oacute;n a actualizar, e invoca la funci&oacute;n <code>actualizar</code> de la clase de la que hereda.
	 *
	 * @param <code>hthParametroGeneral</code> objeto del tipo <code>HthParametroGeneral</code>.
	 * @throws ExcepcionEnDAO
	 * @see {@link SQLImplementacionBasica}
	 */
	public void actualizar(HthParametroGeneral hthParametroGeneral) throws ExcepcionEnDAO {
	    actualizar(hthParametroGeneral, AUTO_COMMIT);
	
	}
	
	/**
	 * M&eacute;todo utilizado para realizar una b&uacute;squeda de un registro espec&iacute;fico, utilizando un identificador.
	 *
	 * @param <code>idParametroGeneralId</code> objeto del tipo <code>Long</code>.
	 * @return <code>HthParametroGeneral</code> - un objeto DTO.
	 * @throws ExcepcionEnDAO
	 * @see {@link SQLImplementacionBasica}
	 */
	public HthParametroGeneral buscarPorID(Long idParametroGeneralId)
			throws ExcepcionEnDAO {
		return (HthParametroGeneral)buscarPorID(idParametroGeneralId, AUTO_COMMIT);				 
	}

	/**
	 * M&eacute;todo utilizado para la creaci&oacute;n de un registro, a nivel de tabla, a partir de la informaci&oacute;n
	 * recibida en un DTO del objeto de Negocio, e invoca la funci&oacute;n <code>crear</code> de la clase de la que hereda.
	 * 
	 * @param <code>hthParametroGeneral</code> objeto del tipo <code>HthParametroGeneral</code>.
	 * @throws ExcepcionEnDAO
	 * @see {@link SQLImplementacionBasica}
	 */
	public void crear(HthParametroGeneral hthParametroGeneral) throws ExcepcionEnDAO {
	    crear(hthParametroGeneral, AUTO_COMMIT);		
	}

	/**
	 * M&eacute;todo utilizado para la eliminaci&oacute;n de un registro, a nivel de tabla,
	 * que se busca a trav&eacute;s del identificador recibido. Se invoca la funci&oacute;n <code>eliminar</code> de la clase de la que hereda.
	 *
	 * @param <code>idParametroGeneralId</code> objeto del tipo <code>Long</code>.
	 * @throws ExcepcionEnDAO
	 * @see {@link SQLImplementacionBasica}
	 */
	public void eliminar(Long idParametroGeneralId) throws ExcepcionEnDAO {
	    eliminar(idParametroGeneralId, AUTO_COMMIT);		
	}

	/**
	 * M&eacute;todo utilizado para devolver un &uacute;nico resultado de la petici&oacute;n recibida,
	 * la cual se realiza a trav&eacuate;s del objeto de tipo {@link PeticionDeDatos}.
	 * 
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>, que contiene las condiciones de la consulta.
	 * @return <code>HthParametroGeneral</code> - un objeto DTO.
	 * @throws ExcepcionEnDAO
	 * @throws ExcepcionPorDatoNoEncontrado
	 * @see {@link SQLImplementacionBasica}
	 */
	public HthParametroGeneral resultadoUnico(PeticionDeDatos peticion)
			throws ExcepcionEnDAO, ExcepcionPorDatoNoEncontrado {
		return (HthParametroGeneral)resultadoUnico(peticion, AUTO_COMMIT);
	}
	
	/**
	 * Atributo del tipo {@link SQLOperaciones}, almacena la operaciones disponibles para esta implementaci&oacute;n.<br/>
	 * Algunas de las llamadas a los m&eacute;todos se realiza a trav&eacute;s de la invocaci&oacute;n de estas implementaciones.
	 */
    public static final SQLOperaciones	SQL_OPERACIONES =  new SQLOperaciones() {
			
			public int getManejoForaneosPorDefecto() {
				return Dao.SIN_FORANEOS;
			}
			
			
			public void operacionesParaPeticionDeDatosF(PeticionDeDatos request,
					PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
				HthParametroGeneralDaoJdbcSql.operacionesParaPeticionDeDatosFST(request, smt);
			}
			
			public void operacionesParaPeticionDeDatos(PeticionDeDatos request,
					PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
				HthParametroGeneralDaoJdbcSql.operacionesParaPeticionDeDatosST(request, smt);
			}
			
			public void operacionesParaEliminar(Object theId, PreparedStatement smt)
					throws SQLException, ExcepcionEnDAO {
				HthParametroGeneralDaoJdbcSql.operacionesParaEliminarST(theId, smt);
			}
			
			public void operacionesParaCrear(Object theDto, PreparedStatement smt)
					throws SQLException, ExcepcionEnDAO {
				HthParametroGeneralDaoJdbcSql.operacionesParaCrearST(theDto, smt);
			}
			
			public void operacionesParaCrearConId(Object theDto, PreparedStatement smt) 
			        throws SQLException, ExcepcionEnDAO {
			    HthParametroGeneralDaoJdbcSql.operacionesParaCrearConIdST(theDto, smt);
            }
			
			public void operacionesParaConteo(PeticionDeDatos request,
					PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
				HthParametroGeneralDaoJdbcSql.operacionesParaConteoST(request, smt);				
			}
			
			public void operacionesParaBuscarPorId(Object theId, PreparedStatement smt)
					throws SQLException, ExcepcionEnDAO {
				HthParametroGeneralDaoJdbcSql.operacionesParaBuscarPorIdST(theId, smt);
			}
			
			public void operacionesParaActualizar(Object theDto, PreparedStatement smt)
					throws SQLException, ExcepcionEnDAO {
				HthParametroGeneralDaoJdbcSql.operacionesParaActualizarST(theDto, smt);
				
			}
			
			public String getSqlParaPeticionDeDatosF(PeticionDeDatos request)
					throws SQLException, ExcepcionEnDAO {	
				return null;
			}
			
			public String getSqlParaPeticionDeDatos(PeticionDeDatos request)
					throws SQLException, ExcepcionEnDAO {
				return HthParametroGeneralDaoJdbcSql.getSqlParaPeticionDeDatosST(request);
			}
			
			public String getSqlDeConteo(PeticionDeDatos request) throws SQLException,
					ExcepcionEnDAO {
				return HthParametroGeneralDaoJdbcSql.getSqlDeConteoST(request);
			}
			
			public SQLEntidadMeta getSQLEntidadMeta() throws ExcepcionEnDAO {
				return HthParametroGeneralDaoJdbcSql.SQL_ENTIDAD_META;
			}
			
			public String getEliminarSql() {
				return  HthParametroGeneralDaoJdbcSql.DELETE_SQL;
			}
			
			public String getCrearSql() {
				return  HthParametroGeneralDaoJdbcSql.CREATE_SQL;
			}
			
			public String getCrearSqlConId(){
			    return  HthParametroGeneralDaoJdbcSql.CREATE_SQL_CON_ID;
			}			
			
			public String getBuscarPorIdSqlF() {
				return null;
			}
			
			public String getBuscarPorIdSql() {				
				return HthParametroGeneralDaoJdbcSql.FIND_BY_ID_SQL;
			}
			
			public String getActualizarSql() {				
				return HthParametroGeneralDaoJdbcSql.UPDATE_SQL;
			}
			
			public Object crearDto(ResultSet rst) throws SQLException, ExcepcionEnDAO {
				return HthParametroGeneralDaoJdbcSql.crearDtoST(rst);
			}
	        
	        public Object crearDataConValoresForaneos(ResultSet rst)
					throws SQLException, ExcepcionEnDAO {
				return null;
			}
			
	        public Object crearDataConValoresForaneosNoRel(ResultSet rst, Connection conn)
					throws SQLException, ExcepcionEnDAO {
				return null;
			}
			
			
            public boolean isForaneosDisponible(){
				return false;
            
            }
            
            public SQLLenguaje getSQLLenguaje() {
                return HthParametroGeneralDaoJdbcSql.getLenguaje();
            }
            
            public void setIdEnDto(Object theDto, Object dtoId) {
                setIdEnDtoST(theDto, dtoId);
            }
	};
	
	
	
	
	
	/**
	 * Devuelve el atributo {@link #SQL_OPERACIONES}.
	 * @return <code>SQLOperaciones</code> - objeto del tipo {@link SQLOperaciones}
	 */
    public SQLOperaciones getSqlOperaciones() {
       return SQL_OPERACIONES;
	}
	
	/**
	 * Devuelve el atributo {@link #lenguaje}
	 * @return <code>SQLLenguaje</code> - objeto del tipo {@link SQLLenguaje}
	 */	
	public static SQLLenguaje getLenguaje() {
		return lenguaje;
	}
	
	/**
	 * M&eacute;todo que permite obtener a trav&eacute;s de un Servicio la implementaci&oacute;n del objeto de Lenguaje,
	 * y coloca las reglas de negocio almacenadas en el atributo {@link #SQL_ENTIDAD_META}.
	 * @param <code>localizadorLenguaje</code> objeto del tipo {@link LocalizadorDeServicio} que posee informaci&oacute;n para trabajar con la base de datos.
	 */
	public static void setLocalizadorLenguaje(LocalizadorDeServicio localizadorLenguaje) {
		lenguaje = (SQLLenguaje) localizadorLenguaje.getServicio();
		lenguaje.setSqlEntidadMeta(SQL_ENTIDAD_META);
	}

	/**
	 * M&eacute;todo que agrega un identificador a un DTO del tipo <code>HthParametroGeneral</code>.
	 * @param <code>theDtoUntyped</code> objeto gen&eacute;rico, que representa a un <code>DTO</code>.
	 * @param <code>dtoIdUntyped</code> objeto gen&eacute;rico, que representa al identificador.
	 */
	public static void setIdEnDtoST(Object theDtoUntyped, Object dtoIdUntyped){
	    HthParametroGeneral dto = (HthParametroGeneral)theDtoUntyped;
        Long dtoId = (Long) dtoIdUntyped;;
        dto.setIdParametroGeneral(dtoId);

	}	
	
	
}
