package com.ofbox.davivienda.h2h.orm.jdbc.sql.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.sql.Connection;
import java.util.Collection;

import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Dao;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.ExcepcionEnDAO;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.ExcepcionPorDatoNoEncontrado;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.PeticionDeDatos;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Transaccion;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.foraneos.DataConValoresForaneos;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLColumnaMeta;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLEntidadMeta;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLImplementacionBasica;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLImplementacionOperacionesComunes;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLLenguaje;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLOperaciones;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLUtils;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.TransaccionJdbc;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dependencias.LocalizadorDeServicio;
import com.ofbox.davivienda.h2h.abstraccion.dto.HthParametroSistema;

import java.lang.Long;

import org.apache.commons.lang.StringUtils;

import com.ofbox.davivienda.h2h.abstraccion.dao.HthParametroSistemaDao;
import com.ofbox.davivienda.h2h.seguridad.Cifrador;




/**
 * Clase que hereda comportamientos de {@link SQLImplementacionBasica} e implementa el comportamiento definido en la capa DAO.<br/>
 * Esta clase es una implementaci&oacute;n intermedia para el DAO de <b>Par&aacutemetro Sistema</b>
 * asociado a la tabla <b>HTH_Parametro_Sistema</b>, en esta clase se construyen todos los sql para las operaciones de listado,
 * creaci&oacute;n, consulta, actualizaci&oacute;n y elminaci&oacute;n.<br/>
 * @author Systems Out of the Box
 * @version f3.1
 *
 */
public class HthParametroSistemaDaoJdbcSql extends SQLImplementacionBasica implements HthParametroSistemaDao{

	/**
	 * Atributo del tipo {@link SQLEntidadMeta}, que almacena la descripci&oacute;n del negocio como una entidad.<br/>
	 * Entre sus caracter&iacute;sticas tiene el nombre de la tabla, nombre de la secuencia, posici&oacute;n de las columnas,
	 * nombre de las columnas y tipos de datos para la columna.<br/>
	 * El arreglo que recibe son objetos de tipo {@link SQLColumnaMeta}, y definen informaci&oacute;n para obtener las columnas.
	 */
	public static final SQLEntidadMeta SQL_ENTIDAD_META = new SQLEntidadMeta("HTH_Parametro_Sistema", new SQLColumnaMeta[]{
        new SQLColumnaMeta(18, "HTH_Parametro_Sistema", "E18", 18001, "id_parametro_sistema", "C18001", Types.BIGINT ,true)
     ,  new SQLColumnaMeta(18, "HTH_Parametro_Sistema", "E18", 18002, "tiempo_monitoreo_default", "C18002", Types.BIGINT )
     ,  new SQLColumnaMeta(18, "HTH_Parametro_Sistema", "E18", 18003, "hora_inicio_monitoreo", "C18003", Types.VARCHAR )
     ,  new SQLColumnaMeta(18, "HTH_Parametro_Sistema", "E18", 18004, "hora_fin_monitoreo", "C18004", Types.VARCHAR )
     ,  new SQLColumnaMeta(18, "HTH_Parametro_Sistema", "E18", 18005, "carpeta_lectura_default", "C18005", Types.VARCHAR )
     ,  new SQLColumnaMeta(18, "HTH_Parametro_Sistema", "E18", 18006, "carpeta_escritura_default", "C18006", Types.VARCHAR )
     ,  new SQLColumnaMeta(18, "HTH_Parametro_Sistema", "E18", 18007, "ruta_raiz", "C18007", Types.VARCHAR )
     ,  new SQLColumnaMeta(18, "HTH_Parametro_Sistema", "E18", 18008, "car_separador_default", "C18008", Types.VARCHAR )
     ,  new SQLColumnaMeta(18, "HTH_Parametro_Sistema", "E18", 18009, "habilitar_envio_correo", "C18009", Types.VARCHAR )
     ,  new SQLColumnaMeta(18, "HTH_Parametro_Sistema", "E18", 18010, "remitente_envio_correo", "C18010", Types.VARCHAR )
     ,  new SQLColumnaMeta(18, "HTH_Parametro_Sistema", "E18", 18011, "asunto_envio_correo", "C18011", Types.VARCHAR )
     ,  new SQLColumnaMeta(18, "HTH_Parametro_Sistema", "E18", 18012, "javamail_jndi", "C18012", Types.VARCHAR )
     ,  new SQLColumnaMeta(18, "HTH_Parametro_Sistema", "E18", 18013, "lista_envio_correo", "C18013", Types.VARCHAR )
     ,  new SQLColumnaMeta(18, "HTH_Parametro_Sistema", "E18", 18014, "servidor_envio_correo", "C18014", Types.VARCHAR )
     ,  new SQLColumnaMeta(18, "HTH_Parametro_Sistema", "E18", 18015, "usuario_envio_correo", "C18015", Types.VARCHAR )
     ,  new SQLColumnaMeta(18, "HTH_Parametro_Sistema", "E18", 18016, "password_envio_correo", "C18016", Types.VARCHAR )
     ,  new SQLColumnaMeta(18, "HTH_Parametro_Sistema", "E18", 18017, "puerto_envio_correo", "C18017", Types.BIGINT )
     ,  new SQLColumnaMeta(18, "HTH_Parametro_Sistema", "E18", 18018, "directorio_local_destino", "C18018", Types.VARCHAR )
     ,  new SQLColumnaMeta(18, "HTH_Parametro_Sistema", "E18", 18019, "host_sftp", "C18019", Types.VARCHAR )
     ,  new SQLColumnaMeta(18, "HTH_Parametro_Sistema", "E18", 18020, "puerto_sftp", "C18020", Types.VARCHAR )
     ,  new SQLColumnaMeta(18, "HTH_Parametro_Sistema", "E18", 18021, "usuario_sftp", "C18021", Types.VARCHAR )
     ,  new SQLColumnaMeta(18, "HTH_Parametro_Sistema", "E18", 18022, "password_sftp", "C18022", Types.VARCHAR )
     ,  new SQLColumnaMeta(18, "HTH_Parametro_Sistema", "E18", 18023, "llave_sftp", "C18023", Types.VARCHAR )
     ,  new SQLColumnaMeta(18, "HTH_Parametro_Sistema", "E18", 18024, "tiempo_drenado_bitacora", "C18024", Types.BIGINT )
	});
	
	


	/**
	 * Atributo que almacena el <code>String</code> para obtener el listado de registros del negocio.
	 */
	public static final String SELECT_HEADER_SQL =  
		"SELECT E18.id_parametro_sistema AS C18001 , E18.tiempo_monitoreo_default AS C18002 , E18.hora_inicio_monitoreo AS C18003 , E18.hora_fin_monitoreo AS C18004 , " +
		"       E18.carpeta_lectura_default AS C18005 , E18.carpeta_escritura_default AS C18006 , E18.ruta_raiz AS C18007 , E18.car_separador_default AS C18008 , " +
		"       E18.habilitar_envio_correo AS C18009 , E18.remitente_envio_correo AS C18010 , E18.asunto_envio_correo AS C18011 , E18.javamail_jndi AS C18012 , " +
		"       E18.lista_envio_correo AS C18013 , E18.servidor_envio_correo AS C18014 , E18.usuario_envio_correo AS C18015 , E18.password_envio_correo AS C18016 , " +
		"       E18.puerto_envio_correo AS C18017 , E18.directorio_local_destino AS C18018 , E18.host_sftp AS C18019 , E18.puerto_sftp AS C18020 , " +
		"       E18.usuario_sftp AS C18021 , E18.password_sftp AS C18022 , E18.llave_sftp AS C18023 , E18.tiempo_drenado_bitacora AS C18024" +
		"  FROM HTH_Parametro_Sistema E18";
	/**
	 * Atributo que almacena el <code>String</code> para obtener el n&uacute;mero de registros presentes en el negocio.
	 */
	public static final String COUNT_HEADER_SQL =   "SELECT COUNT(*) FROM HTH_Parametro_Sistema";    												
	/**
	 * Atributo que almacena el <code>String</code> para la inserci&oacute;n, sin id, de un nuevo registro en la tabla.
	 */
	public static final String CREATE_SQL = 
		"INSERT INTO HTH_Parametro_Sistema (" +
		"tiempo_monitoreo_default,hora_inicio_monitoreo,hora_fin_monitoreo,carpeta_lectura_default,carpeta_escritura_default" +
		",ruta_raiz,car_separador_default,habilitar_envio_correo,remitente_envio_correo" +
		",asunto_envio_correo,javamail_jndi,lista_envio_correo,servidor_envio_correo" +
		",usuario_envio_correo,password_envio_correo,puerto_envio_correo,directorio_local_destino" +
		",host_sftp,puerto_sftp,usuario_sftp,password_sftp" +
		",llave_sftp,tiempo_drenado_bitacora" +
		") VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	/**
	 * Atributo que almacena el <code>String</code> para insertar, con id, un registro en la table.
	 */
	public static final String CREATE_SQL_CON_ID = 
		"INSERT INTO HTH_Parametro_Sistema (" +
		"id_parametro_sistema,tiempo_monitoreo_default,hora_inicio_monitoreo,hora_fin_monitoreo,carpeta_lectura_default" +
		",carpeta_escritura_default,ruta_raiz,car_separador_default,habilitar_envio_correo" +
		",remitente_envio_correo,asunto_envio_correo,javamail_jndi,lista_envio_correo" +
		",servidor_envio_correo,usuario_envio_correo,password_envio_correo,puerto_envio_correo" +
		",directorio_local_destino,host_sftp,puerto_sftp,usuario_sftp" +
		",password_sftp,llave_sftp,tiempo_drenado_bitacora" +
		") VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	/**
	 * Atributo que almacena el <code>String</code> para la atualizaci&oacute; de un registro en la tabla.
	 */
	public static final String UPDATE_SQL = 
		"UPDATE HTH_Parametro_Sistema" +
		" SET tiempo_monitoreo_default= ?  , hora_inicio_monitoreo= ?  , hora_fin_monitoreo= ?  , carpeta_lectura_default= ?  , " +
		"carpeta_escritura_default= ?  , ruta_raiz= ?  , car_separador_default= ?  , habilitar_envio_correo= ?  , " +
		"remitente_envio_correo= ?  , asunto_envio_correo= ?  , javamail_jndi= ?  , lista_envio_correo= ?  , " +
		"servidor_envio_correo= ?  , usuario_envio_correo= ?  , password_envio_correo= ?  , puerto_envio_correo= ?  , " +
		"directorio_local_destino= ?  , host_sftp= ?  , puerto_sftp= ?  , usuario_sftp= ?  , " +
		"password_sftp= ?  , llave_sftp= ?  , tiempo_drenado_bitacora= ? " +
		" WHERE id_parametro_sistema= ? ";
	/**
	 * Atributo que almacena el <code>String</code> para la eliminaci&oacute; de un registro en la tabla.
	 */
	public static final String DELETE_SQL =  
		"DELETE FROM HTH_Parametro_Sistema" +
		" WHERE id_parametro_sistema= ? ";
	/**
	 * Atributo que almacena el <code>String</code> para la b&uacute;squeda por id de un registro de la tabla.
	 */
	public static final String FIND_BY_ID_SQL = 
		"SELECT E18.id_parametro_sistema AS C18001 , E18.tiempo_monitoreo_default AS C18002 , E18.hora_inicio_monitoreo AS C18003 , E18.hora_fin_monitoreo AS C18004 , " +
		"       E18.carpeta_lectura_default AS C18005 , E18.carpeta_escritura_default AS C18006 , E18.ruta_raiz AS C18007 , E18.car_separador_default AS C18008 , " +
		"       E18.habilitar_envio_correo AS C18009 , E18.remitente_envio_correo AS C18010 , E18.asunto_envio_correo AS C18011 , E18.javamail_jndi AS C18012 , " +
		"       E18.lista_envio_correo AS C18013 , E18.servidor_envio_correo AS C18014 , E18.usuario_envio_correo AS C18015 , E18.password_envio_correo AS C18016 , " +
		"       E18.puerto_envio_correo AS C18017 , E18.directorio_local_destino AS C18018 , E18.host_sftp AS C18019 , E18.puerto_sftp AS C18020 , " +
		"       E18.usuario_sftp AS C18021 , E18.password_sftp AS C18022 , E18.llave_sftp AS C18023 , E18.tiempo_drenado_bitacora AS C18024" +
		"  FROM HTH_Parametro_Sistema E18" +
		" WHERE E18.id_parametro_sistema= ? ";
	/**
	 * Atributo que almacena el <code>String</code> para la creacion de la tabla en la base de datos.
	 */
	public static final String CREATE_TABLE_DDL = 
		"CREATE TABLE HTH_Parametro_Sistema (" +
		"    id_parametro_sistema LONG NOT NULL," +
		"    tiempo_monitoreo_default LONG NOT NULL," +
		"    hora_inicio_monitoreo VARCHAR(12)  NOT NULL," +
		"    hora_fin_monitoreo VARCHAR(12)  NOT NULL," +
		"    carpeta_lectura_default VARCHAR(100)  NOT NULL," +
		"    carpeta_escritura_default VARCHAR(100)  NOT NULL," +
		"    ruta_raiz VARCHAR(150)  NOT NULL," +
		"    car_separador_default VARCHAR(10)  NOT NULL," +
		"    habilitar_envio_correo VARCHAR(1)  NOT NULL," +
		"    remitente_envio_correo VARCHAR(100)  NOT NULL," +
		"    asunto_envio_correo VARCHAR(200)  NOT NULL," +
		"    javamail_jndi VARCHAR(100)  NOT NULL," +
		"    lista_envio_correo VARCHAR(500)  NOT NULL," +
		"    servidor_envio_correo VARCHAR(100)  NOT NULL," +
		"    usuario_envio_correo VARCHAR(100)  NOT NULL," +
		"    password_envio_correo VARCHAR(250)  NOT NULL," +
		"    puerto_envio_correo LONG NOT NULL," +
		"    directorio_local_destino VARCHAR(100) ," +
		"    host_sftp VARCHAR(100) ," +
		"    puerto_sftp VARCHAR(100) ," +
		"    usuario_sftp VARCHAR(100) ," +
		"    password_sftp VARCHAR(100) ," +
		"    llave_sftp VARCHAR(200) ," +
		"    tiempo_drenado_bitacora LONG NOT NULL" +
		")";
	/**
	 * Atributo que almacena un objeto del tipo {@link SQLLenguaje}.
	 */
	private static SQLLenguaje lenguaje;
	
	/**
	 * M&eacute;todo que realiza la creaci&oacute;n de un DTO, del negocio, a partir del resultado de alguna consulta.
	 * @param <code>rst</code> objeto de tipo <code>Resultset</code>, que contiene los resultados de la consulta.
	 * @return <code>Object</code> - representa al DTO creado.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static Object crearDtoST(ResultSet rst)
			throws SQLException, ExcepcionEnDAO {
 	    HthParametroSistema theDto = new HthParametroSistema();
     	theDto.setIdParametroSistema(SQLUtils.getLongFromResultSet(rst, "C18001"));
     	theDto.setTiempoMonitoreoDefault(SQLUtils.getLongFromResultSet(rst, "C18002"));
		theDto.setHoraInicioMonitoreo(rst.getString("C18003"));
		theDto.setHoraFinMonitoreo(rst.getString("C18004"));
		theDto.setCarpetaLecturaDefault(rst.getString("C18005"));
		theDto.setCarpetaEscrituraDefault(rst.getString("C18006"));
		theDto.setRutaRaiz(rst.getString("C18007"));
		theDto.setCarSeparadorDefault(rst.getString("C18008"));
		theDto.setHabilitarEnvioCorreo(rst.getString("C18009"));
		theDto.setRemitenteEnvioCorreo(rst.getString("C18010"));
		theDto.setAsuntoEnvioCorreo(rst.getString("C18011"));
		theDto.setJavamailJndi(rst.getString("C18012"));
		theDto.setListaEnvioCorreo(rst.getString("C18013"));
		theDto.setServidorEnvioCorreo(rst.getString("C18014"));
		theDto.setUsuarioEnvioCorreo(rst.getString("C18015"));
		theDto.setPasswordEnvioCorreo(rst.getString("C18016"));
     	theDto.setPuertoEnvioCorreo(SQLUtils.getLongFromResultSet(rst, "C18017"));
		theDto.setDirectorioLocalDestino(rst.getString("C18018"));
		 /*Valores encriptados*/
		String encHostSftp = rst.getString("C18019");
		if(StringUtils.isNotEmpty(encHostSftp)){
			String hostSftp = encHostSftp.length()>0?Cifrador.desencriptar(encHostSftp):"";
			theDto.setHostSftp(hostSftp);
		}
		
		String encPuertoSftp = rst.getString("C18020");
		if(StringUtils.isNotEmpty(encPuertoSftp)){
			String puertoSftp =  Cifrador.desencriptar(encPuertoSftp);
			theDto.setPuertoSftp(puertoSftp);
		}
		
		String encUsuarioSftp = rst.getString("C18021");
		if(StringUtils.isNotEmpty(encUsuarioSftp)){
			String usuarioSftp = Cifrador.desencriptar(encUsuarioSftp);
			theDto.setUsuarioSftp(usuarioSftp);
		}
		
		String encPasswordSftp = rst.getString("C18022");
		if(StringUtils.isNotEmpty(encPasswordSftp)){
			String passwordSftp = Cifrador.desencriptar(encPasswordSftp);
			theDto.setPasswordSftp(passwordSftp);
		}
		
		String encLlaveSftp = rst.getString("C18023");
		if(StringUtils.isNotEmpty(encLlaveSftp)){
			String llaveSftp = Cifrador.desencriptar(encLlaveSftp);
			theDto.setLlaveSftp(llaveSftp);
		}
		
     	theDto.setTiempoDrenadoBitacora(SQLUtils.getLongFromResultSet(rst, "C18024"));
		return theDto;
	}
	
	







	/**
	 * M&eacute;todo que coloca los valores provenientes del objeto Dto en el objeto PreparedStatement,
	 * el cual proviene de la llamada a la implementacion de {@link SQLOperaciones}, este m&eacute;todo
	 * no toma en cuenta el valor del <code>id</code>, puesto que se asume que es un valor secuencial
	 * autonumerico o serial propio de la base de datos.
	 * 
	 * @param <code>theDtoUntyped</code> objeto sin tipo que almacena al DTO del negocio.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaCrearST(Object theDtoUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {			
	        HthParametroSistema theDto = (HthParametroSistema)theDtoUntyped;
				Long tiempoMonitoreoDefault = theDto.getTiempoMonitoreoDefault();     
		String horaInicioMonitoreo = theDto.getHoraInicioMonitoreo();     
		String horaFinMonitoreo = theDto.getHoraFinMonitoreo();     
		String carpetaLecturaDefault = theDto.getCarpetaLecturaDefault();     
		String carpetaEscrituraDefault = theDto.getCarpetaEscrituraDefault();     
		String rutaRaiz = theDto.getRutaRaiz();     
		String carSeparadorDefault = theDto.getCarSeparadorDefault();     
		String habilitarEnvioCorreo = theDto.getHabilitarEnvioCorreo();     
		String remitenteEnvioCorreo = theDto.getRemitenteEnvioCorreo();     
		String asuntoEnvioCorreo = theDto.getAsuntoEnvioCorreo();     
		String javamailJndi = theDto.getJavamailJndi();     
		String listaEnvioCorreo = theDto.getListaEnvioCorreo();     
		String servidorEnvioCorreo = theDto.getServidorEnvioCorreo();     
		String usuarioEnvioCorreo = theDto.getUsuarioEnvioCorreo();     
		String passwordEnvioCorreo = theDto.getPasswordEnvioCorreo();     
		Long puertoEnvioCorreo = theDto.getPuertoEnvioCorreo();     
		String directorioLocalDestino = theDto.getDirectorioLocalDestino();
		
		
		String hostSftp = theDto.getHostSftp();
		if(StringUtils.isNotEmpty(hostSftp)){
			hostSftp = Cifrador.encriptar(hostSftp);
		}
		
		
		String puertoSftp = theDto.getPuertoSftp();
		if(StringUtils.isNotEmpty(puertoSftp)){
			puertoSftp = Cifrador.encriptar(puertoSftp);
		}
		
		String usuarioSftp = theDto.getUsuarioSftp();
		if(StringUtils.isNotEmpty(usuarioSftp)){
			usuarioSftp = Cifrador.encriptar(usuarioSftp);
		}
		
		String passwordSftp = theDto.getPasswordSftp();
		if(StringUtils.isNotEmpty(passwordSftp)){
			passwordSftp = Cifrador.encriptar(passwordSftp);
		}
		
		String llaveSftp = theDto.getLlaveSftp();
		if(StringUtils.isNotEmpty(llaveSftp)){
			llaveSftp = Cifrador.encriptar(llaveSftp);
		}
		Long tiempoDrenadoBitacora = theDto.getTiempoDrenadoBitacora();     
		     
		SQLUtils.setIntoStatement( 1, tiempoMonitoreoDefault, smt);  
    		     
		SQLUtils.setIntoStatement( 2, horaInicioMonitoreo, smt);  
    		     
		SQLUtils.setIntoStatement( 3, horaFinMonitoreo, smt);  
    		     
		SQLUtils.setIntoStatement( 4, carpetaLecturaDefault, smt);  
    		     
		SQLUtils.setIntoStatement( 5, carpetaEscrituraDefault, smt);  
    		     
		SQLUtils.setIntoStatement( 6, rutaRaiz, smt);  
    		     
		SQLUtils.setIntoStatement( 7, carSeparadorDefault, smt);  
    		     
		SQLUtils.setIntoStatement( 8, habilitarEnvioCorreo, smt);  
    		     
		SQLUtils.setIntoStatement( 9, remitenteEnvioCorreo, smt);  
    		     
		SQLUtils.setIntoStatement( 10, asuntoEnvioCorreo, smt);  
    		     
		SQLUtils.setIntoStatement( 11, javamailJndi, smt);  
    		     
		SQLUtils.setIntoStatement( 12, listaEnvioCorreo, smt);  
    		     
		SQLUtils.setIntoStatement( 13, servidorEnvioCorreo, smt);  
    		     
		SQLUtils.setIntoStatement( 14, usuarioEnvioCorreo, smt);  
    		     
		SQLUtils.setIntoStatement( 15, passwordEnvioCorreo, smt);  
    		     
		SQLUtils.setIntoStatement( 16, puertoEnvioCorreo, smt);  
    		     
		SQLUtils.setIntoStatement( 17, directorioLocalDestino, smt);  
    		     
		SQLUtils.setIntoStatement( 18, hostSftp, smt);  
    		     
		SQLUtils.setIntoStatement( 19, puertoSftp, smt);  
    		     
		SQLUtils.setIntoStatement( 20, usuarioSftp, smt);  
    		     
		SQLUtils.setIntoStatement( 21, passwordSftp, smt);  
    		     
		SQLUtils.setIntoStatement( 22, llaveSftp, smt);  
    		     
		SQLUtils.setIntoStatement( 23, tiempoDrenadoBitacora, smt);  
    		
	}

	/**
	 * M&eacute;todo que coloca los valores provenientes del objeto Dto en el objeto PreparedStatement,
	 * el cual proviene de la llamada a la implementacion de {@link SQLOperaciones}, este m&eacute;todo
	 * toma en cuenta el <code>id</code>, lo que quiere decir que se usa al momento de  crear registros
	 * en donde el identificador no es un valor secuencial autonumerico o serial propio de la base de datos
	 *
	 * @param <code>theDtoUntyped</code> objeto sin tipo que almacena al DTO del negocio.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaCrearConIdST(Object theDtoUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {			
	        HthParametroSistema theDto = (HthParametroSistema)theDtoUntyped;
				Long idParametroSistema = theDto.getIdParametroSistema();     
		Long tiempoMonitoreoDefault = theDto.getTiempoMonitoreoDefault();     
		String horaInicioMonitoreo = theDto.getHoraInicioMonitoreo();     
		String horaFinMonitoreo = theDto.getHoraFinMonitoreo();     
		String carpetaLecturaDefault = theDto.getCarpetaLecturaDefault();     
		String carpetaEscrituraDefault = theDto.getCarpetaEscrituraDefault();     
		String rutaRaiz = theDto.getRutaRaiz();     
		String carSeparadorDefault = theDto.getCarSeparadorDefault();     
		String habilitarEnvioCorreo = theDto.getHabilitarEnvioCorreo();     
		String remitenteEnvioCorreo = theDto.getRemitenteEnvioCorreo();     
		String asuntoEnvioCorreo = theDto.getAsuntoEnvioCorreo();     
		String javamailJndi = theDto.getJavamailJndi();     
		String listaEnvioCorreo = theDto.getListaEnvioCorreo();     
		String servidorEnvioCorreo = theDto.getServidorEnvioCorreo();     
		String usuarioEnvioCorreo = theDto.getUsuarioEnvioCorreo();     
		String passwordEnvioCorreo = theDto.getPasswordEnvioCorreo();     
		Long puertoEnvioCorreo = theDto.getPuertoEnvioCorreo();     
		String directorioLocalDestino = theDto.getDirectorioLocalDestino();    
		
		String hostSftp = theDto.getHostSftp();
		if(StringUtils.isNotEmpty(hostSftp)){
			hostSftp = Cifrador.encriptar(hostSftp);
		}
		
		
		String puertoSftp = theDto.getPuertoSftp();
		if(StringUtils.isNotEmpty(puertoSftp)){
			puertoSftp = Cifrador.encriptar(puertoSftp);
		}
		
		String usuarioSftp = theDto.getUsuarioSftp();
		if(StringUtils.isNotEmpty(usuarioSftp)){
			usuarioSftp = Cifrador.encriptar(usuarioSftp);
		}
		
		String passwordSftp = theDto.getPasswordSftp();
		if(StringUtils.isNotEmpty(passwordSftp)){
			passwordSftp = Cifrador.encriptar(passwordSftp);
		}
		
		String llaveSftp = theDto.getLlaveSftp();
		if(StringUtils.isNotEmpty(llaveSftp)){
			llaveSftp = Cifrador.encriptar(llaveSftp);
		}  
		Long tiempoDrenadoBitacora = theDto.getTiempoDrenadoBitacora();     
	
		SQLUtils.setIntoStatement( 1, idParametroSistema, smt);
    	
		SQLUtils.setIntoStatement( 2, tiempoMonitoreoDefault, smt);
    	
		SQLUtils.setIntoStatement( 3, horaInicioMonitoreo, smt);
    	
		SQLUtils.setIntoStatement( 4, horaFinMonitoreo, smt);
    	
		SQLUtils.setIntoStatement( 5, carpetaLecturaDefault, smt);
    	
		SQLUtils.setIntoStatement( 6, carpetaEscrituraDefault, smt);
    	
		SQLUtils.setIntoStatement( 7, rutaRaiz, smt);
    	
		SQLUtils.setIntoStatement( 8, carSeparadorDefault, smt);
    	
		SQLUtils.setIntoStatement( 9, habilitarEnvioCorreo, smt);
    	
		SQLUtils.setIntoStatement( 10, remitenteEnvioCorreo, smt);
    	
		SQLUtils.setIntoStatement( 11, asuntoEnvioCorreo, smt);
    	
		SQLUtils.setIntoStatement( 12, javamailJndi, smt);
    	
		SQLUtils.setIntoStatement( 13, listaEnvioCorreo, smt);
    	
		SQLUtils.setIntoStatement( 14, servidorEnvioCorreo, smt);
    	
		SQLUtils.setIntoStatement( 15, usuarioEnvioCorreo, smt);
    	
		SQLUtils.setIntoStatement( 16, passwordEnvioCorreo, smt);
    	
		SQLUtils.setIntoStatement( 17, puertoEnvioCorreo, smt);
    	
		SQLUtils.setIntoStatement( 18, directorioLocalDestino, smt);
    	
		SQLUtils.setIntoStatement( 19, hostSftp, smt);
    	
		SQLUtils.setIntoStatement( 20, puertoSftp, smt);
    	
		SQLUtils.setIntoStatement( 21, usuarioSftp, smt);
    	
		SQLUtils.setIntoStatement( 22, passwordSftp, smt);
    	
		SQLUtils.setIntoStatement( 23, llaveSftp, smt);
    	
		SQLUtils.setIntoStatement( 24, tiempoDrenadoBitacora, smt);
    		
	}

	/**
	 * M&eacute;todo que coloca el identificador recibido en el objeto PreparedStatement, para poder ser eliminado.
	 * @param <code>theIdUntyped</code> objeto sin tipo que identifica al <code>id</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaEliminarST(Object theIdUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {
			Long theId = (Long)theIdUntyped;
		SQLUtils.setIntoStatement( 1, theId, smt);
	}

	/**
	 * M&eacute;todo que coloca el identificador recibido, en el objeto PreparedStatement para poder hacer la consulta de busqueda.
	 * @param <code>theIdUntyped</code> objeto sin tipo que identifica al <code>id</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaBuscarPorIdST(Object theIdUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {
			Long theId = (Long)theIdUntyped;
			SQLUtils.setIntoStatement( 1, theId, smt);
		}

	/**
	 * M&eacute;todo que coloca los valores provenientes del objeto Dto en el objeto PreparedStatement,
	 * el cual proviene de la llamada a la implementacion de {@link SQLOperaciones} para poder actualizar
     * un registro en espec&iacute;fico.
	 * 
	 * @param <code>theDtoUntyped</code> objeto sin tipo que almacena al DTO del negocio.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaActualizarST(Object theDtoUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {
			HthParametroSistema theDto = (HthParametroSistema)theDtoUntyped;
				Long idParametroSistema = theDto.getIdParametroSistema();     
		Long tiempoMonitoreoDefault = theDto.getTiempoMonitoreoDefault();     
		String horaInicioMonitoreo = theDto.getHoraInicioMonitoreo();     
		String horaFinMonitoreo = theDto.getHoraFinMonitoreo();     
		String carpetaLecturaDefault = theDto.getCarpetaLecturaDefault();     
		String carpetaEscrituraDefault = theDto.getCarpetaEscrituraDefault();     
		String rutaRaiz = theDto.getRutaRaiz();     
		String carSeparadorDefault = theDto.getCarSeparadorDefault();     
		String habilitarEnvioCorreo = theDto.getHabilitarEnvioCorreo();     
		String remitenteEnvioCorreo = theDto.getRemitenteEnvioCorreo();     
		String asuntoEnvioCorreo = theDto.getAsuntoEnvioCorreo();     
		String javamailJndi = theDto.getJavamailJndi();     
		String listaEnvioCorreo = theDto.getListaEnvioCorreo();     
		String servidorEnvioCorreo = theDto.getServidorEnvioCorreo();     
		String usuarioEnvioCorreo = theDto.getUsuarioEnvioCorreo();     
		String passwordEnvioCorreo = theDto.getPasswordEnvioCorreo();     
		Long puertoEnvioCorreo = theDto.getPuertoEnvioCorreo();     
		String directorioLocalDestino = theDto.getDirectorioLocalDestino();     
		
		String hostSftp = theDto.getHostSftp();
		if(StringUtils.isNotEmpty(hostSftp)){
			hostSftp = Cifrador.encriptar(hostSftp);
		}
		
		
		String puertoSftp = theDto.getPuertoSftp();
		if(StringUtils.isNotEmpty(puertoSftp)){
			puertoSftp = Cifrador.encriptar(puertoSftp);
		}
		
		String usuarioSftp = theDto.getUsuarioSftp();
		if(StringUtils.isNotEmpty(usuarioSftp)){
			usuarioSftp = Cifrador.encriptar(usuarioSftp);
		}
		
		String passwordSftp = theDto.getPasswordSftp();
		if(StringUtils.isNotEmpty(passwordSftp)){
			passwordSftp = Cifrador.encriptar(passwordSftp);
		}
		
		String llaveSftp = theDto.getLlaveSftp();
		if(StringUtils.isNotEmpty(llaveSftp)){
			llaveSftp = Cifrador.encriptar(llaveSftp);
		}
		Long tiempoDrenadoBitacora = theDto.getTiempoDrenadoBitacora();     
		     
		SQLUtils.setIntoStatement( 1, tiempoMonitoreoDefault, smt);  
        	     
		SQLUtils.setIntoStatement( 2, horaInicioMonitoreo, smt);  
        	     
		SQLUtils.setIntoStatement( 3, horaFinMonitoreo, smt);  
        	     
		SQLUtils.setIntoStatement( 4, carpetaLecturaDefault, smt);  
        	     
		SQLUtils.setIntoStatement( 5, carpetaEscrituraDefault, smt);  
        	     
		SQLUtils.setIntoStatement( 6, rutaRaiz, smt);  
        	     
		SQLUtils.setIntoStatement( 7, carSeparadorDefault, smt);  
        	     
		SQLUtils.setIntoStatement( 8, habilitarEnvioCorreo, smt);  
        	     
		SQLUtils.setIntoStatement( 9, remitenteEnvioCorreo, smt);  
        	     
		SQLUtils.setIntoStatement( 10, asuntoEnvioCorreo, smt);  
        	     
		SQLUtils.setIntoStatement( 11, javamailJndi, smt);  
        	     
		SQLUtils.setIntoStatement( 12, listaEnvioCorreo, smt);  
        	     
		SQLUtils.setIntoStatement( 13, servidorEnvioCorreo, smt);  
        	     
		SQLUtils.setIntoStatement( 14, usuarioEnvioCorreo, smt);  
        	     
		SQLUtils.setIntoStatement( 15, passwordEnvioCorreo, smt);  
        	     
		SQLUtils.setIntoStatement( 16, puertoEnvioCorreo, smt);  
        	     
		SQLUtils.setIntoStatement( 17, directorioLocalDestino, smt);  
        	     
		SQLUtils.setIntoStatement( 18, hostSftp, smt);  
        	     
		SQLUtils.setIntoStatement( 19, puertoSftp, smt);  
        	     
		SQLUtils.setIntoStatement( 20, usuarioSftp, smt);  
        	     
		SQLUtils.setIntoStatement( 21, passwordSftp, smt);  
        	     
		SQLUtils.setIntoStatement( 22, llaveSftp, smt);  
        	     
		SQLUtils.setIntoStatement( 23, tiempoDrenadoBitacora, smt);  
        		     
		SQLUtils.setIntoStatement( 24, idParametroSistema, smt);  
    			
	}


	/**
	 * M&eacute;todo que ejecuta una petici&oacute;n de datos a trav&eacute;s de un objeto {@link PreparedStatement},
	 * y un objeto del tipo {@link PeticionDeDatos}, el cual en su implementaci&oacute;n define una serie de restricciones
	 * usadas para hacer la petici&oacute;n.
	 *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaPeticionDeDatosST(PeticionDeDatos peticion,
			PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
		SQLImplementacionOperacionesComunes.operacionesParaPeticionDeDatosST( peticion, smt, SQL_OPERACIONES, null);	
	}	
	
	/**
	 * M&eacute;todo que ejecuta una petici&oacute;n de datos a trav&eacute;s de un objeto {@link PreparedStatement},
     * y un objeto del tipo {@link PeticionDeDatos}, el cual en su implementaci&oacute;n define una serie de restricciones
     * usadas para hacer la petici&oacute;n.
	 *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaPeticionDeDatosFST(PeticionDeDatos peticion,
			PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
		SQLImplementacionOperacionesComunes.operacionesParaPeticionDeDatosST( peticion, smt, SQL_OPERACIONES, null);	
	}	

	/**
	 * M&eacute;todo que ejecuta una petici&oacute;n de conteo a trav&eacute;s de un objeto {@link PreparedStatement},
     * y un objeto del tipo {@link PeticionDeDatos}, el cual en su implementaci&oacute;n define una serie de restricciones
     * usadas para hacer la petici&oacute;n.
	 *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaConteoST(PeticionDeDatos peticion,
			PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
		SQLImplementacionOperacionesComunes.operacionesParaPeticionDeDatosST( peticion, smt, SQL_OPERACIONES, null);
	}

	/**
	 * M&eacute;todo que agrega al atributo {@link #SELECT_HEADER_SQL} las condiciones de b&uacute;squeda
     * que vienen en el objeto del tipo {@link PeticionDeDatos}
     *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>, que contiene las condiciones de la consulta.
	 * @return <code>String</code> - la consulta condicionada como cadena de caracteres.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static String getSqlParaPeticionDeDatosST(PeticionDeDatos peticion)
			throws SQLException, ExcepcionEnDAO {
		return SELECT_HEADER_SQL + SQLImplementacionOperacionesComunes.crearCondicionesSQLST( peticion, SQL_OPERACIONES, SQLImplementacionOperacionesComunes.COND_ALIAS_ORDEN_WHERE);
	}
	
	
	
	

	/**
	 * M&eacute;todo que agrega al atributo {@link #COUNT_HEADER_SQL} las condiciones de conteo
	 * que vienen en el objeto de tipo {@link PeticionDeDatos}.
	 *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>, que contiene las condiciones de la consulta.
	 * @return <code>String</code> - la consulta condicionada como cadena de caracteres.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static String getSqlDeConteoST(PeticionDeDatos peticion)
			throws SQLException, ExcepcionEnDAO {
		return COUNT_HEADER_SQL + SQLImplementacionOperacionesComunes.crearCondicionesSQLST( peticion, SQL_OPERACIONES, SQLImplementacionOperacionesComunes.COND_SQL_WHERE);
	}
	
	/**
	 * M&eacute;todo que se utiliza para actualizar un registro a nivel de tabla, recibe como par&aacute;metro un DTO del negocio
	 * que contiene la informaci&oacute;n a actualizar, e invoca la funci&oacute;n <code>actualizar</code> de la clase de la que hereda.
	 *
	 * @param <code>hthParametroSistema</code> objeto del tipo <code>HthParametroSistema</code>.
	 * @throws ExcepcionEnDAO
	 * @see {@link SQLImplementacionBasica}
	 */
	public void actualizar(HthParametroSistema hthParametroSistema) throws ExcepcionEnDAO {
	    actualizar(hthParametroSistema, AUTO_COMMIT);
	
	}
	
	/**
	 * M&eacute;todo utilizado para realizar una b&uacute;squeda de un registro espec&iacute;fico, utilizando un identificador.
	 *
	 * @param <code>idParametroSistemaId</code> objeto del tipo <code>Long</code>.
	 * @return <code>HthParametroSistema</code> - un objeto DTO.
	 * @throws ExcepcionEnDAO
	 * @see {@link SQLImplementacionBasica}
	 */
	public HthParametroSistema buscarPorID(Long idParametroSistemaId)
			throws ExcepcionEnDAO {
		return (HthParametroSistema)buscarPorID(idParametroSistemaId, AUTO_COMMIT);				 
	}

	/**
	 * M&eacute;todo utilizado para la creaci&oacute;n de un registro, a nivel de tabla, a partir de la informaci&oacute;n
	 * recibida en un DTO del objeto de Negocio, e invoca la funci&oacute;n <code>crear</code> de la clase de la que hereda.
	 * 
	 * @param <code>hthParametroSistema</code> objeto del tipo <code>HthParametroSistema</code>.
	 * @throws ExcepcionEnDAO
	 * @see {@link SQLImplementacionBasica}
	 */
	public void crear(HthParametroSistema hthParametroSistema) throws ExcepcionEnDAO {
	    crear(hthParametroSistema, AUTO_COMMIT);		
	}

	/**
	 * M&eacute;todo utilizado para la eliminaci&oacute;n de un registro, a nivel de tabla,
	 * que se busca a trav&eacute;s del identificador recibido. Se invoca la funci&oacute;n <code>eliminar</code> de la clase de la que hereda.
	 *
	 * @param <code>idParametroSistemaId</code> objeto del tipo <code>Long</code>.
	 * @throws ExcepcionEnDAO
	 * @see {@link SQLImplementacionBasica}
	 */
	public void eliminar(Long idParametroSistemaId) throws ExcepcionEnDAO {
	    eliminar(idParametroSistemaId, AUTO_COMMIT);		
	}

	/**
	 * M&eacute;todo utilizado para devolver un &uacute;nico resultado de la petici&oacute;n recibida,
	 * la cual se realiza a trav&eacuate;s del objeto de tipo {@link PeticionDeDatos}.
	 * 
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>, que contiene las condiciones de la consulta.
	 * @return <code>HthParametroSistema</code> - un objeto DTO.
	 * @throws ExcepcionEnDAO
	 * @throws ExcepcionPorDatoNoEncontrado
	 * @see {@link SQLImplementacionBasica}
	 */
	public HthParametroSistema resultadoUnico(PeticionDeDatos peticion)
			throws ExcepcionEnDAO, ExcepcionPorDatoNoEncontrado {
		return (HthParametroSistema)resultadoUnico(peticion, AUTO_COMMIT);
	}
	
	/**
	 * Atributo del tipo {@link SQLOperaciones}, almacena la operaciones disponibles para esta implementaci&oacute;n.<br/>
	 * Algunas de las llamadas a los m&eacute;todos se realiza a trav&eacute;s de la invocaci&oacute;n de estas implementaciones.
	 */
    public static final SQLOperaciones	SQL_OPERACIONES =  new SQLOperaciones() {
			
			public int getManejoForaneosPorDefecto() {
				return Dao.SIN_FORANEOS;
			}
			
			
			public void operacionesParaPeticionDeDatosF(PeticionDeDatos request,
					PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
				HthParametroSistemaDaoJdbcSql.operacionesParaPeticionDeDatosFST(request, smt);
			}
			
			public void operacionesParaPeticionDeDatos(PeticionDeDatos request,
					PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
				HthParametroSistemaDaoJdbcSql.operacionesParaPeticionDeDatosST(request, smt);
			}
			
			public void operacionesParaEliminar(Object theId, PreparedStatement smt)
					throws SQLException, ExcepcionEnDAO {
				HthParametroSistemaDaoJdbcSql.operacionesParaEliminarST(theId, smt);
			}
			
			public void operacionesParaCrear(Object theDto, PreparedStatement smt)
					throws SQLException, ExcepcionEnDAO {
				HthParametroSistemaDaoJdbcSql.operacionesParaCrearST(theDto, smt);
			}
			
			public void operacionesParaCrearConId(Object theDto, PreparedStatement smt) 
			        throws SQLException, ExcepcionEnDAO {
			    HthParametroSistemaDaoJdbcSql.operacionesParaCrearConIdST(theDto, smt);
            }
			
			public void operacionesParaConteo(PeticionDeDatos request,
					PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
				HthParametroSistemaDaoJdbcSql.operacionesParaConteoST(request, smt);				
			}
			
			public void operacionesParaBuscarPorId(Object theId, PreparedStatement smt)
					throws SQLException, ExcepcionEnDAO {
				HthParametroSistemaDaoJdbcSql.operacionesParaBuscarPorIdST(theId, smt);
			}
			
			public void operacionesParaActualizar(Object theDto, PreparedStatement smt)
					throws SQLException, ExcepcionEnDAO {
				HthParametroSistemaDaoJdbcSql.operacionesParaActualizarST(theDto, smt);
				
			}
			
			public String getSqlParaPeticionDeDatosF(PeticionDeDatos request)
					throws SQLException, ExcepcionEnDAO {	
				return null;
			}
			
			public String getSqlParaPeticionDeDatos(PeticionDeDatos request)
					throws SQLException, ExcepcionEnDAO {
				return HthParametroSistemaDaoJdbcSql.getSqlParaPeticionDeDatosST(request);
			}
			
			public String getSqlDeConteo(PeticionDeDatos request) throws SQLException,
					ExcepcionEnDAO {
				return HthParametroSistemaDaoJdbcSql.getSqlDeConteoST(request);
			}
			
			public SQLEntidadMeta getSQLEntidadMeta() throws ExcepcionEnDAO {
				return HthParametroSistemaDaoJdbcSql.SQL_ENTIDAD_META;
			}
			
			public String getEliminarSql() {
				return  HthParametroSistemaDaoJdbcSql.DELETE_SQL;
			}
			
			public String getCrearSql() {
				return  HthParametroSistemaDaoJdbcSql.CREATE_SQL;
			}
			
			public String getCrearSqlConId(){
			    return  HthParametroSistemaDaoJdbcSql.CREATE_SQL_CON_ID;
			}			
			
			public String getBuscarPorIdSqlF() {
				return null;
			}
			
			public String getBuscarPorIdSql() {				
				return HthParametroSistemaDaoJdbcSql.FIND_BY_ID_SQL;
			}
			
			public String getActualizarSql() {				
				return HthParametroSistemaDaoJdbcSql.UPDATE_SQL;
			}
			
			public Object crearDto(ResultSet rst) throws SQLException, ExcepcionEnDAO {
				return HthParametroSistemaDaoJdbcSql.crearDtoST(rst);
			}
	        
	        public Object crearDataConValoresForaneos(ResultSet rst)
					throws SQLException, ExcepcionEnDAO {
				return null;
			}
			
	        public Object crearDataConValoresForaneosNoRel(ResultSet rst, Connection conn)
					throws SQLException, ExcepcionEnDAO {
				return null;
			}
			
			
            public boolean isForaneosDisponible(){
				return false;
            
            }
            
            public SQLLenguaje getSQLLenguaje() {
                return HthParametroSistemaDaoJdbcSql.getLenguaje();
            }
            
            public void setIdEnDto(Object theDto, Object dtoId) {
                setIdEnDtoST(theDto, dtoId);
            }
	};
	
	
	
	
	
	/**
	 * Devuelve el atributo {@link #SQL_OPERACIONES}.
	 * @return <code>SQLOperaciones</code> - objeto del tipo {@link SQLOperaciones}
	 */
    public SQLOperaciones getSqlOperaciones() {
       return SQL_OPERACIONES;
	}
	
	/**
	 * Devuelve el atributo {@link #lenguaje}
	 * @return <code>SQLLenguaje</code> - objeto del tipo {@link SQLLenguaje}
	 */	
	public static SQLLenguaje getLenguaje() {
		return lenguaje;
	}
	
	/**
	 * M&eacute;todo que permite obtener a trav&eacute;s de un Servicio la implementaci&oacute;n del objeto de Lenguaje,
	 * y coloca las reglas de negocio almacenadas en el atributo {@link #SQL_ENTIDAD_META}.
	 * @param <code>localizadorLenguaje</code> objeto del tipo {@link LocalizadorDeServicio} que posee informaci&oacute;n para trabajar con la base de datos.
	 */
	public static void setLocalizadorLenguaje(LocalizadorDeServicio localizadorLenguaje) {
		lenguaje = (SQLLenguaje) localizadorLenguaje.getServicio();
		lenguaje.setSqlEntidadMeta(SQL_ENTIDAD_META);
	}

	/**
	 * M&eacute;todo que agrega un identificador a un DTO del tipo <code>HthParametroSistema</code>.
	 * @param <code>theDtoUntyped</code> objeto gen&eacute;rico, que representa a un <code>DTO</code>.
	 * @param <code>dtoIdUntyped</code> objeto gen&eacute;rico, que representa al identificador.
	 */
	public static void setIdEnDtoST(Object theDtoUntyped, Object dtoIdUntyped){
	    HthParametroSistema dto = (HthParametroSistema)theDtoUntyped;
        Long dtoId = (Long) dtoIdUntyped;;
        dto.setIdParametroSistema(dtoId);

	}

	public HthParametroSistema buscarParametrosSistema() throws ExcepcionEnDAO,
			ExcepcionPorDatoNoEncontrado {
		PeticionDeDatos peticion = crearPeticionDeDatos();
		Collection<HthParametroSistema> todos = ejecutarPeticionDeDatos(peticion).cargarTodosLosDatosEnMemoria();
		/* Debe existir uno al menos*/
		if(todos.isEmpty()){
			return null;
		}
		HthParametroSistema param = todos.iterator().next();
		return param;
	}	
	
	
}
