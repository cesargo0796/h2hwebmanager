package com.ofbox.davivienda.h2h.orm.jdbc.sql.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.sql.Connection;
import java.util.Collection;

import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Dao;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.ExcepcionEnDAO;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.ExcepcionPorDatoNoEncontrado;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.PeticionDeDatos;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Transaccion;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.foraneos.DataConValoresForaneos;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLColumnaMeta;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLEntidadMeta;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLImplementacionBasica;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLImplementacionOperacionesComunes;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLLenguaje;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLOperaciones;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLUtils;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.TransaccionJdbc;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dependencias.LocalizadorDeServicio;


import com.ofbox.davivienda.h2h.abstraccion.dto.BsInstalacion;
import java.lang.Long;
import com.ofbox.davivienda.h2h.abstraccion.dao.BsInstalacionDao;




/**
 * Clase que hereda comportamientos de {@link SQLImplementacionBasica} e implementa el comportamiento definido en la capa DAO.<br/>
 * Esta clase es una implementaci&oacute;n intermedia para el DAO de <b>BS Instalaci&oacute;n</b>
 * asociado a la tabla <b>BS_Instalacion</b>, en esta clase se construyen todos los sql para las operaciones de listado,
 * creaci&oacute;n, consulta, actualizaci&oacute;n y elminaci&oacute;n.<br/>
 * @author Systems Out of the Box
 * @version f3.1
 *
 */
public class BsInstalacionDaoJdbcSql extends SQLImplementacionBasica implements BsInstalacionDao{

	/**
	 * Atributo del tipo {@link SQLEntidadMeta}, que almacena la descripci&oacute;n del negocio como una entidad.<br/>
	 * Entre sus caracter&iacute;sticas tiene el nombre de la tabla, nombre de la secuencia, posici&oacute;n de las columnas,
	 * nombre de las columnas y tipos de datos para la columna.<br/>
	 * El arreglo que recibe son objetos de tipo {@link SQLColumnaMeta}, y definen informaci&oacute;n para obtener las columnas.
	 */
	public static final SQLEntidadMeta SQL_ENTIDAD_META = new SQLEntidadMeta("BS_Instalacion", new SQLColumnaMeta[]{
        new SQLColumnaMeta(8, "BS_Instalacion", "E8", 8001, "Instalacion", "C8001", Types.BIGINT ,true)
     ,  new SQLColumnaMeta(8, "BS_Instalacion", "E8", 8002, "Cliente", "C8002", Types.BIGINT )
     ,  new SQLColumnaMeta(8, "BS_Instalacion", "E8", 8003, "Modulos", "C8003", Types.BIGINT )
     ,  new SQLColumnaMeta(8, "BS_Instalacion", "E8", 8004, "Nombre", "C8004", Types.VARCHAR )
     ,  new SQLColumnaMeta(8, "BS_Instalacion", "E8", 8005, "ID", "C8005", Types.VARCHAR )
     ,  new SQLColumnaMeta(8, "BS_Instalacion", "E8", 8006, "Password", "C8006", Types.VARCHAR )
     ,  new SQLColumnaMeta(8, "BS_Instalacion", "E8", 8007, "PesoA", "C8007", Types.BIGINT )
     ,  new SQLColumnaMeta(8, "BS_Instalacion", "E8", 8008, "PesoB", "C8008", Types.BIGINT )
     ,  new SQLColumnaMeta(8, "BS_Instalacion", "E8", 8009, "PesoC", "C8009", Types.BIGINT )
     ,  new SQLColumnaMeta(8, "BS_Instalacion", "E8", 8010, "PesoD", "C8010", Types.BIGINT )
     ,  new SQLColumnaMeta(8, "BS_Instalacion", "E8", 8011, "PesoE", "C8011", Types.BIGINT )
     ,  new SQLColumnaMeta(8, "BS_Instalacion", "E8", 8012, "EstatusCom", "C8012", Types.BIGINT )
     ,  new SQLColumnaMeta(8, "BS_Instalacion", "E8", 8013, "FechaEstatus", "C8013", Types.TIMESTAMP )
     ,  new SQLColumnaMeta(8, "BS_Instalacion", "E8", 8014, "Estatus", "C8014", Types.BIGINT )
     ,  new SQLColumnaMeta(8, "BS_Instalacion", "E8", 8015, "Envio_Cliente", "C8015", Types.BIGINT )
     ,  new SQLColumnaMeta(8, "BS_Instalacion", "E8", 8016, "version", "C8016", Types.VARCHAR )
     ,  new SQLColumnaMeta(8, "BS_Instalacion", "E8", 8017, "versionPE", "C8017", Types.VARCHAR )
     ,  new SQLColumnaMeta(8, "BS_Instalacion", "E8", 8018, "OrigenLotes", "C8018", Types.BIGINT )
     ,  new SQLColumnaMeta(8, "BS_Instalacion", "E8", 8019, "SiguienteLoteCC", "C8019", Types.BIGINT )
     ,  new SQLColumnaMeta(8, "BS_Instalacion", "E8", 8020, "SiguienteLotePE", "C8020", Types.BIGINT )
     ,  new SQLColumnaMeta(8, "BS_Instalacion", "E8", 8021, "SiguienteLotePP", "C8021", Types.BIGINT )
     ,  new SQLColumnaMeta(8, "BS_Instalacion", "E8", 8022, "SiguienteLotePS", "C8022", Types.BIGINT )
     ,  new SQLColumnaMeta(8, "BS_Instalacion", "E8", 8023, "SiguienteLoteACH", "C8023", Types.BIGINT )
	});
	
	


	/**
	 * Atributo que almacena el <code>String</code> para obtener el listado de registros del negocio.
	 */
	public static final String SELECT_HEADER_SQL =  
		"SELECT E8.Instalacion AS C8001 , E8.Cliente AS C8002 , E8.Modulos AS C8003 , E8.Nombre AS C8004 , " +
		"       E8.ID AS C8005 , E8.Password AS C8006 , E8.PesoA AS C8007 , E8.PesoB AS C8008 , " +
		"       E8.PesoC AS C8009 , E8.PesoD AS C8010 , E8.PesoE AS C8011 , E8.EstatusCom AS C8012 , " +
		"       E8.FechaEstatus AS C8013 , E8.Estatus AS C8014 , E8.Envio_Cliente AS C8015 , E8.version AS C8016 , " +
		"       E8.versionPE AS C8017 , E8.OrigenLotes AS C8018 , E8.SiguienteLoteCC AS C8019 , E8.SiguienteLotePE AS C8020 , " +
		"       E8.SiguienteLotePP AS C8021 , E8.SiguienteLotePS AS C8022 , E8.SiguienteLoteACH AS C8023" +
		"  FROM BS_Instalacion E8";
	/**
	 * Atributo que almacena el <code>String</code> para obtener el n&uacute;mero de registros presentes en el negocio.
	 */
	public static final String COUNT_HEADER_SQL =   "SELECT COUNT(*) FROM BS_Instalacion";    												
	/**
	 * Atributo que almacena el <code>String</code> para la inserci&oacute;n, sin id, de un nuevo registro en la tabla.
	 */
	public static final String CREATE_SQL = 
		"INSERT INTO BS_Instalacion (" +
		"Cliente,Modulos,Nombre,ID,Password" +
		",PesoA,PesoB,PesoC,PesoD" +
		",PesoE,EstatusCom,FechaEstatus,Estatus" +
		",Envio_Cliente,version,versionPE,OrigenLotes" +
		",SiguienteLoteCC,SiguienteLotePE,SiguienteLotePP,SiguienteLotePS" +
		",SiguienteLoteACH" +
		") VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	/**
	 * Atributo que almacena el <code>String</code> para insertar, con id, un registro en la table.
	 */
	public static final String CREATE_SQL_CON_ID = 
		"INSERT INTO BS_Instalacion (" +
		"Instalacion,Cliente,Modulos,Nombre,ID" +
		",Password,PesoA,PesoB,PesoC" +
		",PesoD,PesoE,EstatusCom,FechaEstatus" +
		",Estatus,Envio_Cliente,version,versionPE" +
		",OrigenLotes,SiguienteLoteCC,SiguienteLotePE,SiguienteLotePP" +
		",SiguienteLotePS,SiguienteLoteACH" +
		") VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	/**
	 * Atributo que almacena el <code>String</code> para la atualizaci&oacute; de un registro en la tabla.
	 */
	public static final String UPDATE_SQL = 
		"UPDATE BS_Instalacion" +
		" SET Cliente= ?  , Modulos= ?  , Nombre= ?  , ID= ?  , " +
		"Password= ?  , PesoA= ?  , PesoB= ?  , PesoC= ?  , " +
		"PesoD= ?  , PesoE= ?  , EstatusCom= ?  , FechaEstatus= ?  , " +
		"Estatus= ?  , Envio_Cliente= ?  , version= ?  , versionPE= ?  , " +
		"OrigenLotes= ?  , SiguienteLoteCC= ?  , SiguienteLotePE= ?  , SiguienteLotePP= ?  , " +
		"SiguienteLotePS= ?  , SiguienteLoteACH= ? " +
		" WHERE Instalacion= ? ";
	/**
	 * Atributo que almacena el <code>String</code> para la eliminaci&oacute; de un registro en la tabla.
	 */
	public static final String DELETE_SQL =  
		"DELETE FROM BS_Instalacion" +
		" WHERE Instalacion= ? ";
	/**
	 * Atributo que almacena el <code>String</code> para la b&uacute;squeda por id de un registro de la tabla.
	 */
	public static final String FIND_BY_ID_SQL = 
		"SELECT E8.Instalacion AS C8001 , E8.Cliente AS C8002 , E8.Modulos AS C8003 , E8.Nombre AS C8004 , " +
		"       E8.ID AS C8005 , E8.Password AS C8006 , E8.PesoA AS C8007 , E8.PesoB AS C8008 , " +
		"       E8.PesoC AS C8009 , E8.PesoD AS C8010 , E8.PesoE AS C8011 , E8.EstatusCom AS C8012 , " +
		"       E8.FechaEstatus AS C8013 , E8.Estatus AS C8014 , E8.Envio_Cliente AS C8015 , E8.version AS C8016 , " +
		"       E8.versionPE AS C8017 , E8.OrigenLotes AS C8018 , E8.SiguienteLoteCC AS C8019 , E8.SiguienteLotePE AS C8020 , " +
		"       E8.SiguienteLotePP AS C8021 , E8.SiguienteLotePS AS C8022 , E8.SiguienteLoteACH AS C8023" +
		"  FROM BS_Instalacion E8" +
		" WHERE E8.Instalacion= ? ";
	/**
	 * Atributo que almacena el <code>String</code> para la creacion de la tabla en la base de datos.
	 */
	public static final String CREATE_TABLE_DDL = 
		"CREATE TABLE BS_Instalacion (" +
		"    Instalacion LONG NOT NULL," +
		"    Cliente LONG NOT NULL," +
		"    Modulos LONG NOT NULL," +
		"    Nombre VARCHAR(50)  NOT NULL," +
		"    ID VARCHAR(1)  NOT NULL," +
		"    Password VARCHAR(10)  NOT NULL," +
		"    PesoA LONG NOT NULL," +
		"    PesoB LONG NOT NULL," +
		"    PesoC LONG NOT NULL," +
		"    PesoD LONG NOT NULL," +
		"    PesoE LONG NOT NULL," +
		"    EstatusCom LONG," +
		"    FechaEstatus TIMESTAMP NOT NULL," +
		"    Estatus LONG NOT NULL," +
		"    Envio_Cliente LONG," +
		"    version VARCHAR(11) ," +
		"    versionPE VARCHAR(11) ," +
		"    OrigenLotes LONG NOT NULL," +
		"    SiguienteLoteCC LONG," +
		"    SiguienteLotePE LONG," +
		"    SiguienteLotePP LONG," +
		"    SiguienteLotePS LONG," +
		"    SiguienteLoteACH LONG" +
		")";
	/**
	 * Atributo que almacena un objeto del tipo {@link SQLLenguaje}.
	 */
	private static SQLLenguaje lenguaje;
	
	/**
	 * M&eacute;todo que realiza la creaci&oacute;n de un DTO, del negocio, a partir del resultado de alguna consulta.
	 * @param <code>rst</code> objeto de tipo <code>Resultset</code>, que contiene los resultados de la consulta.
	 * @return <code>Object</code> - representa al DTO creado.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static Object crearDtoST(ResultSet rst)
			throws SQLException, ExcepcionEnDAO {
 	    BsInstalacion theDto = new BsInstalacion();
     	theDto.setInstalacion(SQLUtils.getLongFromResultSet(rst, "C8001"));
     	theDto.setCliente(SQLUtils.getLongFromResultSet(rst, "C8002"));
     	theDto.setModulos(SQLUtils.getLongFromResultSet(rst, "C8003"));
		theDto.setNombre(rst.getString("C8004"));
		theDto.setId(rst.getString("C8005"));
		theDto.setPassword(rst.getString("C8006"));
     	theDto.setPesoa(SQLUtils.getLongFromResultSet(rst, "C8007"));
     	theDto.setPesob(SQLUtils.getLongFromResultSet(rst, "C8008"));
     	theDto.setPesoc(SQLUtils.getLongFromResultSet(rst, "C8009"));
     	theDto.setPesod(SQLUtils.getLongFromResultSet(rst, "C8010"));
     	theDto.setPesoe(SQLUtils.getLongFromResultSet(rst, "C8011"));
     	theDto.setEstatuscom(SQLUtils.getLongFromResultSet(rst, "C8012"));
		theDto.setFechaestatus(rst.getTimestamp("C8013"));
     	theDto.setEstatus(SQLUtils.getLongFromResultSet(rst, "C8014"));
     	theDto.setEnvioCliente(SQLUtils.getLongFromResultSet(rst, "C8015"));
		theDto.setVersion(rst.getString("C8016"));
		theDto.setVersionpe(rst.getString("C8017"));
     	theDto.setOrigenlotes(SQLUtils.getLongFromResultSet(rst, "C8018"));
     	theDto.setSiguientelotecc(SQLUtils.getLongFromResultSet(rst, "C8019"));
     	theDto.setSiguientelotepe(SQLUtils.getLongFromResultSet(rst, "C8020"));
     	theDto.setSiguientelotepp(SQLUtils.getLongFromResultSet(rst, "C8021"));
     	theDto.setSiguienteloteps(SQLUtils.getLongFromResultSet(rst, "C8022"));
     	theDto.setSiguienteloteach(SQLUtils.getLongFromResultSet(rst, "C8023"));
		return theDto;
	}
	
	







	/**
	 * M&eacute;todo que coloca los valores provenientes del objeto Dto en el objeto PreparedStatement,
	 * el cual proviene de la llamada a la implementacion de {@link SQLOperaciones}, este m&eacute;todo
	 * no toma en cuenta el valor del <code>id</code>, puesto que se asume que es un valor secuencial
	 * autonumerico o serial propio de la base de datos.
	 * 
	 * @param <code>theDtoUntyped</code> objeto sin tipo que almacena al DTO del negocio.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaCrearST(Object theDtoUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {			
	        BsInstalacion theDto = (BsInstalacion)theDtoUntyped;
				Long cliente = theDto.getCliente();     
		Long modulos = theDto.getModulos();     
		String nombre = theDto.getNombre();     
		String id = theDto.getId();     
		String password = theDto.getPassword();     
		Long pesoa = theDto.getPesoa();     
		Long pesob = theDto.getPesob();     
		Long pesoc = theDto.getPesoc();     
		Long pesod = theDto.getPesod();     
		Long pesoe = theDto.getPesoe();     
		Long estatuscom = theDto.getEstatuscom();     
		java.util.Date fechaestatus = theDto.getFechaestatus();     
		Long estatus = theDto.getEstatus();     
		Long envioCliente = theDto.getEnvioCliente();     
		String version = theDto.getVersion();     
		String versionpe = theDto.getVersionpe();     
		Long origenlotes = theDto.getOrigenlotes();     
		Long siguientelotecc = theDto.getSiguientelotecc();     
		Long siguientelotepe = theDto.getSiguientelotepe();     
		Long siguientelotepp = theDto.getSiguientelotepp();     
		Long siguienteloteps = theDto.getSiguienteloteps();     
		Long siguienteloteach = theDto.getSiguienteloteach();     
		     
		SQLUtils.setIntoStatement( 1, cliente, smt);  
    		     
		SQLUtils.setIntoStatement( 2, modulos, smt);  
    		     
		SQLUtils.setIntoStatement( 3, nombre, smt);  
    		     
		SQLUtils.setIntoStatement( 4, id, smt);  
    		     
		SQLUtils.setIntoStatement( 5, password, smt);  
    		     
		SQLUtils.setIntoStatement( 6, pesoa, smt);  
    		     
		SQLUtils.setIntoStatement( 7, pesob, smt);  
    		     
		SQLUtils.setIntoStatement( 8, pesoc, smt);  
    		     
		SQLUtils.setIntoStatement( 9, pesod, smt);  
    		     
		SQLUtils.setIntoStatement( 10, pesoe, smt);  
    		     
		SQLUtils.setIntoStatement( 11, estatuscom, smt);  
    		     
		SQLUtils.setIntoStatement( 12, fechaestatus, smt);  
    		     
		SQLUtils.setIntoStatement( 13, estatus, smt);  
    		     
		SQLUtils.setIntoStatement( 14, envioCliente, smt);  
    		     
		SQLUtils.setIntoStatement( 15, version, smt);  
    		     
		SQLUtils.setIntoStatement( 16, versionpe, smt);  
    		     
		SQLUtils.setIntoStatement( 17, origenlotes, smt);  
    		     
		SQLUtils.setIntoStatement( 18, siguientelotecc, smt);  
    		     
		SQLUtils.setIntoStatement( 19, siguientelotepe, smt);  
    		     
		SQLUtils.setIntoStatement( 20, siguientelotepp, smt);  
    		     
		SQLUtils.setIntoStatement( 21, siguienteloteps, smt);  
    		     
		SQLUtils.setIntoStatement( 22, siguienteloteach, smt);  
    		
	}

	/**
	 * M&eacute;todo que coloca los valores provenientes del objeto Dto en el objeto PreparedStatement,
	 * el cual proviene de la llamada a la implementacion de {@link SQLOperaciones}, este m&eacute;todo
	 * toma en cuenta el <code>id</code>, lo que quiere decir que se usa al momento de  crear registros
	 * en donde el identificador no es un valor secuencial autonumerico o serial propio de la base de datos
	 *
	 * @param <code>theDtoUntyped</code> objeto sin tipo que almacena al DTO del negocio.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaCrearConIdST(Object theDtoUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {			
	        BsInstalacion theDto = (BsInstalacion)theDtoUntyped;
				Long instalacion = theDto.getInstalacion();     
		Long cliente = theDto.getCliente();     
		Long modulos = theDto.getModulos();     
		String nombre = theDto.getNombre();     
		String id = theDto.getId();     
		String password = theDto.getPassword();     
		Long pesoa = theDto.getPesoa();     
		Long pesob = theDto.getPesob();     
		Long pesoc = theDto.getPesoc();     
		Long pesod = theDto.getPesod();     
		Long pesoe = theDto.getPesoe();     
		Long estatuscom = theDto.getEstatuscom();     
		java.util.Date fechaestatus = theDto.getFechaestatus();     
		Long estatus = theDto.getEstatus();     
		Long envioCliente = theDto.getEnvioCliente();     
		String version = theDto.getVersion();     
		String versionpe = theDto.getVersionpe();     
		Long origenlotes = theDto.getOrigenlotes();     
		Long siguientelotecc = theDto.getSiguientelotecc();     
		Long siguientelotepe = theDto.getSiguientelotepe();     
		Long siguientelotepp = theDto.getSiguientelotepp();     
		Long siguienteloteps = theDto.getSiguienteloteps();     
		Long siguienteloteach = theDto.getSiguienteloteach();     
	
		SQLUtils.setIntoStatement( 1, instalacion, smt);
    	
		SQLUtils.setIntoStatement( 2, cliente, smt);
    	
		SQLUtils.setIntoStatement( 3, modulos, smt);
    	
		SQLUtils.setIntoStatement( 4, nombre, smt);
    	
		SQLUtils.setIntoStatement( 5, id, smt);
    	
		SQLUtils.setIntoStatement( 6, password, smt);
    	
		SQLUtils.setIntoStatement( 7, pesoa, smt);
    	
		SQLUtils.setIntoStatement( 8, pesob, smt);
    	
		SQLUtils.setIntoStatement( 9, pesoc, smt);
    	
		SQLUtils.setIntoStatement( 10, pesod, smt);
    	
		SQLUtils.setIntoStatement( 11, pesoe, smt);
    	
		SQLUtils.setIntoStatement( 12, estatuscom, smt);
    	
		SQLUtils.setIntoStatement( 13, fechaestatus, smt);
    	
		SQLUtils.setIntoStatement( 14, estatus, smt);
    	
		SQLUtils.setIntoStatement( 15, envioCliente, smt);
    	
		SQLUtils.setIntoStatement( 16, version, smt);
    	
		SQLUtils.setIntoStatement( 17, versionpe, smt);
    	
		SQLUtils.setIntoStatement( 18, origenlotes, smt);
    	
		SQLUtils.setIntoStatement( 19, siguientelotecc, smt);
    	
		SQLUtils.setIntoStatement( 20, siguientelotepe, smt);
    	
		SQLUtils.setIntoStatement( 21, siguientelotepp, smt);
    	
		SQLUtils.setIntoStatement( 22, siguienteloteps, smt);
    	
		SQLUtils.setIntoStatement( 23, siguienteloteach, smt);
    		
	}

	/**
	 * M&eacute;todo que coloca el identificador recibido en el objeto PreparedStatement, para poder ser eliminado.
	 * @param <code>theIdUntyped</code> objeto sin tipo que identifica al <code>id</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaEliminarST(Object theIdUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {
			Long theId = (Long)theIdUntyped;
		SQLUtils.setIntoStatement( 1, theId, smt);
	}

	/**
	 * M&eacute;todo que coloca el identificador recibido, en el objeto PreparedStatement para poder hacer la consulta de busqueda.
	 * @param <code>theIdUntyped</code> objeto sin tipo que identifica al <code>id</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaBuscarPorIdST(Object theIdUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {
			Long theId = (Long)theIdUntyped;
			SQLUtils.setIntoStatement( 1, theId, smt);
		}

	/**
	 * M&eacute;todo que coloca los valores provenientes del objeto Dto en el objeto PreparedStatement,
	 * el cual proviene de la llamada a la implementacion de {@link SQLOperaciones} para poder actualizar
     * un registro en espec&iacute;fico.
	 * 
	 * @param <code>theDtoUntyped</code> objeto sin tipo que almacena al DTO del negocio.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaActualizarST(Object theDtoUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {
			BsInstalacion theDto = (BsInstalacion)theDtoUntyped;
				Long instalacion = theDto.getInstalacion();     
		Long cliente = theDto.getCliente();     
		Long modulos = theDto.getModulos();     
		String nombre = theDto.getNombre();     
		String id = theDto.getId();     
		String password = theDto.getPassword();     
		Long pesoa = theDto.getPesoa();     
		Long pesob = theDto.getPesob();     
		Long pesoc = theDto.getPesoc();     
		Long pesod = theDto.getPesod();     
		Long pesoe = theDto.getPesoe();     
		Long estatuscom = theDto.getEstatuscom();     
		java.util.Date fechaestatus = theDto.getFechaestatus();     
		Long estatus = theDto.getEstatus();     
		Long envioCliente = theDto.getEnvioCliente();     
		String version = theDto.getVersion();     
		String versionpe = theDto.getVersionpe();     
		Long origenlotes = theDto.getOrigenlotes();     
		Long siguientelotecc = theDto.getSiguientelotecc();     
		Long siguientelotepe = theDto.getSiguientelotepe();     
		Long siguientelotepp = theDto.getSiguientelotepp();     
		Long siguienteloteps = theDto.getSiguienteloteps();     
		Long siguienteloteach = theDto.getSiguienteloteach();     
		     
		SQLUtils.setIntoStatement( 1, cliente, smt);  
        	     
		SQLUtils.setIntoStatement( 2, modulos, smt);  
        	     
		SQLUtils.setIntoStatement( 3, nombre, smt);  
        	     
		SQLUtils.setIntoStatement( 4, id, smt);  
        	     
		SQLUtils.setIntoStatement( 5, password, smt);  
        	     
		SQLUtils.setIntoStatement( 6, pesoa, smt);  
        	     
		SQLUtils.setIntoStatement( 7, pesob, smt);  
        	     
		SQLUtils.setIntoStatement( 8, pesoc, smt);  
        	     
		SQLUtils.setIntoStatement( 9, pesod, smt);  
        	     
		SQLUtils.setIntoStatement( 10, pesoe, smt);  
        	     
		SQLUtils.setIntoStatement( 11, estatuscom, smt);  
        	     
		SQLUtils.setIntoStatement( 12, fechaestatus, smt);  
        	     
		SQLUtils.setIntoStatement( 13, estatus, smt);  
        	     
		SQLUtils.setIntoStatement( 14, envioCliente, smt);  
        	     
		SQLUtils.setIntoStatement( 15, version, smt);  
        	     
		SQLUtils.setIntoStatement( 16, versionpe, smt);  
        	     
		SQLUtils.setIntoStatement( 17, origenlotes, smt);  
        	     
		SQLUtils.setIntoStatement( 18, siguientelotecc, smt);  
        	     
		SQLUtils.setIntoStatement( 19, siguientelotepe, smt);  
        	     
		SQLUtils.setIntoStatement( 20, siguientelotepp, smt);  
        	     
		SQLUtils.setIntoStatement( 21, siguienteloteps, smt);  
        	     
		SQLUtils.setIntoStatement( 22, siguienteloteach, smt);  
        		     
		SQLUtils.setIntoStatement( 23, instalacion, smt);  
    			
	}


	/**
	 * M&eacute;todo que ejecuta una petici&oacute;n de datos a trav&eacute;s de un objeto {@link PreparedStatement},
	 * y un objeto del tipo {@link PeticionDeDatos}, el cual en su implementaci&oacute;n define una serie de restricciones
	 * usadas para hacer la petici&oacute;n.
	 *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaPeticionDeDatosST(PeticionDeDatos peticion,
			PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
		SQLImplementacionOperacionesComunes.operacionesParaPeticionDeDatosST( peticion, smt, SQL_OPERACIONES, null);	
	}	
	
	/**
	 * M&eacute;todo que ejecuta una petici&oacute;n de datos a trav&eacute;s de un objeto {@link PreparedStatement},
     * y un objeto del tipo {@link PeticionDeDatos}, el cual en su implementaci&oacute;n define una serie de restricciones
     * usadas para hacer la petici&oacute;n.
	 *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaPeticionDeDatosFST(PeticionDeDatos peticion,
			PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
		SQLImplementacionOperacionesComunes.operacionesParaPeticionDeDatosST( peticion, smt, SQL_OPERACIONES, null);	
	}	

	/**
	 * M&eacute;todo que ejecuta una petici&oacute;n de conteo a trav&eacute;s de un objeto {@link PreparedStatement},
     * y un objeto del tipo {@link PeticionDeDatos}, el cual en su implementaci&oacute;n define una serie de restricciones
     * usadas para hacer la petici&oacute;n.
	 *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaConteoST(PeticionDeDatos peticion,
			PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
		SQLImplementacionOperacionesComunes.operacionesParaPeticionDeDatosST( peticion, smt, SQL_OPERACIONES, null);
	}

	/**
	 * M&eacute;todo que agrega al atributo {@link #SELECT_HEADER_SQL} las condiciones de b&uacute;squeda
     * que vienen en el objeto del tipo {@link PeticionDeDatos}
     *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>, que contiene las condiciones de la consulta.
	 * @return <code>String</code> - la consulta condicionada como cadena de caracteres.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static String getSqlParaPeticionDeDatosST(PeticionDeDatos peticion)
			throws SQLException, ExcepcionEnDAO {
		return SELECT_HEADER_SQL + SQLImplementacionOperacionesComunes.crearCondicionesSQLST( peticion, SQL_OPERACIONES, SQLImplementacionOperacionesComunes.COND_ALIAS_ORDEN_WHERE);
	}
	
	
	
	

	/**
	 * M&eacute;todo que agrega al atributo {@link #COUNT_HEADER_SQL} las condiciones de conteo
	 * que vienen en el objeto de tipo {@link PeticionDeDatos}.
	 *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>, que contiene las condiciones de la consulta.
	 * @return <code>String</code> - la consulta condicionada como cadena de caracteres.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static String getSqlDeConteoST(PeticionDeDatos peticion)
			throws SQLException, ExcepcionEnDAO {
		return COUNT_HEADER_SQL + SQLImplementacionOperacionesComunes.crearCondicionesSQLST( peticion, SQL_OPERACIONES, SQLImplementacionOperacionesComunes.COND_SQL_WHERE);
	}
	
	/**
	 * M&eacute;todo que se utiliza para actualizar un registro a nivel de tabla, recibe como par&aacute;metro un DTO del negocio
	 * que contiene la informaci&oacute;n a actualizar, e invoca la funci&oacute;n <code>actualizar</code> de la clase de la que hereda.
	 *
	 * @param <code>bsInstalacion</code> objeto del tipo <code>BsInstalacion</code>.
	 * @throws ExcepcionEnDAO
	 * @see {@link SQLImplementacionBasica}
	 */
	public void actualizar(BsInstalacion bsInstalacion) throws ExcepcionEnDAO {
	    actualizar(bsInstalacion, AUTO_COMMIT);
	
	}
	
	/**
	 * M&eacute;todo utilizado para realizar una b&uacute;squeda de un registro espec&iacute;fico, utilizando un identificador.
	 *
	 * @param <code>instalacionId</code> objeto del tipo <code>Long</code>.
	 * @return <code>BsInstalacion</code> - un objeto DTO.
	 * @throws ExcepcionEnDAO
	 * @see {@link SQLImplementacionBasica}
	 */
	public BsInstalacion buscarPorID(Long instalacionId)
			throws ExcepcionEnDAO {
		return (BsInstalacion)buscarPorID(instalacionId, AUTO_COMMIT);				 
	}

	/**
	 * M&eacute;todo utilizado para la creaci&oacute;n de un registro, a nivel de tabla, a partir de la informaci&oacute;n
	 * recibida en un DTO del objeto de Negocio, e invoca la funci&oacute;n <code>crear</code> de la clase de la que hereda.
	 * 
	 * @param <code>bsInstalacion</code> objeto del tipo <code>BsInstalacion</code>.
	 * @throws ExcepcionEnDAO
	 * @see {@link SQLImplementacionBasica}
	 */
	public void crear(BsInstalacion bsInstalacion) throws ExcepcionEnDAO {
	    crear(bsInstalacion, AUTO_COMMIT);		
	}

	/**
	 * M&eacute;todo utilizado para la eliminaci&oacute;n de un registro, a nivel de tabla,
	 * que se busca a trav&eacute;s del identificador recibido. Se invoca la funci&oacute;n <code>eliminar</code> de la clase de la que hereda.
	 *
	 * @param <code>instalacionId</code> objeto del tipo <code>Long</code>.
	 * @throws ExcepcionEnDAO
	 * @see {@link SQLImplementacionBasica}
	 */
	public void eliminar(Long instalacionId) throws ExcepcionEnDAO {
	    eliminar(instalacionId, AUTO_COMMIT);		
	}

	/**
	 * M&eacute;todo utilizado para devolver un &uacute;nico resultado de la petici&oacute;n recibida,
	 * la cual se realiza a trav&eacuate;s del objeto de tipo {@link PeticionDeDatos}.
	 * 
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>, que contiene las condiciones de la consulta.
	 * @return <code>BsInstalacion</code> - un objeto DTO.
	 * @throws ExcepcionEnDAO
	 * @throws ExcepcionPorDatoNoEncontrado
	 * @see {@link SQLImplementacionBasica}
	 */
	public BsInstalacion resultadoUnico(PeticionDeDatos peticion)
			throws ExcepcionEnDAO, ExcepcionPorDatoNoEncontrado {
		return (BsInstalacion)resultadoUnico(peticion, AUTO_COMMIT);
	}
	
	/**
	 * Atributo del tipo {@link SQLOperaciones}, almacena la operaciones disponibles para esta implementaci&oacute;n.<br/>
	 * Algunas de las llamadas a los m&eacute;todos se realiza a trav&eacute;s de la invocaci&oacute;n de estas implementaciones.
	 */
    public static final SQLOperaciones	SQL_OPERACIONES =  new SQLOperaciones() {
			
			public int getManejoForaneosPorDefecto() {
				return Dao.SIN_FORANEOS;
			}
			
			
			public void operacionesParaPeticionDeDatosF(PeticionDeDatos request,
					PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
				BsInstalacionDaoJdbcSql.operacionesParaPeticionDeDatosFST(request, smt);
			}
			
			public void operacionesParaPeticionDeDatos(PeticionDeDatos request,
					PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
				BsInstalacionDaoJdbcSql.operacionesParaPeticionDeDatosST(request, smt);
			}
			
			public void operacionesParaEliminar(Object theId, PreparedStatement smt)
					throws SQLException, ExcepcionEnDAO {
				BsInstalacionDaoJdbcSql.operacionesParaEliminarST(theId, smt);
			}
			
			public void operacionesParaCrear(Object theDto, PreparedStatement smt)
					throws SQLException, ExcepcionEnDAO {
				BsInstalacionDaoJdbcSql.operacionesParaCrearST(theDto, smt);
			}
			
			public void operacionesParaCrearConId(Object theDto, PreparedStatement smt) 
			        throws SQLException, ExcepcionEnDAO {
			    BsInstalacionDaoJdbcSql.operacionesParaCrearConIdST(theDto, smt);
            }
			
			public void operacionesParaConteo(PeticionDeDatos request,
					PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
				BsInstalacionDaoJdbcSql.operacionesParaConteoST(request, smt);				
			}
			
			public void operacionesParaBuscarPorId(Object theId, PreparedStatement smt)
					throws SQLException, ExcepcionEnDAO {
				BsInstalacionDaoJdbcSql.operacionesParaBuscarPorIdST(theId, smt);
			}
			
			public void operacionesParaActualizar(Object theDto, PreparedStatement smt)
					throws SQLException, ExcepcionEnDAO {
				BsInstalacionDaoJdbcSql.operacionesParaActualizarST(theDto, smt);
				
			}
			
			public String getSqlParaPeticionDeDatosF(PeticionDeDatos request)
					throws SQLException, ExcepcionEnDAO {	
				return null;
			}
			
			public String getSqlParaPeticionDeDatos(PeticionDeDatos request)
					throws SQLException, ExcepcionEnDAO {
				return BsInstalacionDaoJdbcSql.getSqlParaPeticionDeDatosST(request);
			}
			
			public String getSqlDeConteo(PeticionDeDatos request) throws SQLException,
					ExcepcionEnDAO {
				return BsInstalacionDaoJdbcSql.getSqlDeConteoST(request);
			}
			
			public SQLEntidadMeta getSQLEntidadMeta() throws ExcepcionEnDAO {
				return BsInstalacionDaoJdbcSql.SQL_ENTIDAD_META;
			}
			
			public String getEliminarSql() {
				return  BsInstalacionDaoJdbcSql.DELETE_SQL;
			}
			
			public String getCrearSql() {
				return  BsInstalacionDaoJdbcSql.CREATE_SQL;
			}
			
			public String getCrearSqlConId(){
			    return  BsInstalacionDaoJdbcSql.CREATE_SQL_CON_ID;
			}			
			
			public String getBuscarPorIdSqlF() {
				return null;
			}
			
			public String getBuscarPorIdSql() {				
				return BsInstalacionDaoJdbcSql.FIND_BY_ID_SQL;
			}
			
			public String getActualizarSql() {				
				return BsInstalacionDaoJdbcSql.UPDATE_SQL;
			}
			
			public Object crearDto(ResultSet rst) throws SQLException, ExcepcionEnDAO {
				return BsInstalacionDaoJdbcSql.crearDtoST(rst);
			}
	        
	        public Object crearDataConValoresForaneos(ResultSet rst)
					throws SQLException, ExcepcionEnDAO {
				return null;
			}
			
	        public Object crearDataConValoresForaneosNoRel(ResultSet rst, Connection conn)
					throws SQLException, ExcepcionEnDAO {
				return null;
			}
			
			
            public boolean isForaneosDisponible(){
				return false;
            
            }
            
            public SQLLenguaje getSQLLenguaje() {
                return BsInstalacionDaoJdbcSql.getLenguaje();
            }
            
            public void setIdEnDto(Object theDto, Object dtoId) {
                setIdEnDtoST(theDto, dtoId);
            }
	};
	
	
	
	
	
	/**
	 * Devuelve el atributo {@link #SQL_OPERACIONES}.
	 * @return <code>SQLOperaciones</code> - objeto del tipo {@link SQLOperaciones}
	 */
    public SQLOperaciones getSqlOperaciones() {
       return SQL_OPERACIONES;
	}
	
	/**
	 * Devuelve el atributo {@link #lenguaje}
	 * @return <code>SQLLenguaje</code> - objeto del tipo {@link SQLLenguaje}
	 */	
	public static SQLLenguaje getLenguaje() {
		return lenguaje;
	}
	
	/**
	 * M&eacute;todo que permite obtener a trav&eacute;s de un Servicio la implementaci&oacute;n del objeto de Lenguaje,
	 * y coloca las reglas de negocio almacenadas en el atributo {@link #SQL_ENTIDAD_META}.
	 * @param <code>localizadorLenguaje</code> objeto del tipo {@link LocalizadorDeServicio} que posee informaci&oacute;n para trabajar con la base de datos.
	 */
	public static void setLocalizadorLenguaje(LocalizadorDeServicio localizadorLenguaje) {
		lenguaje = (SQLLenguaje) localizadorLenguaje.getServicio();
		lenguaje.setSqlEntidadMeta(SQL_ENTIDAD_META);
	}

	/**
	 * M&eacute;todo que agrega un identificador a un DTO del tipo <code>BsInstalacion</code>.
	 * @param <code>theDtoUntyped</code> objeto gen&eacute;rico, que representa a un <code>DTO</code>.
	 * @param <code>dtoIdUntyped</code> objeto gen&eacute;rico, que representa al identificador.
	 */
	public static void setIdEnDtoST(Object theDtoUntyped, Object dtoIdUntyped){
	    BsInstalacion dto = (BsInstalacion)theDtoUntyped;
        Long dtoId = (Long) dtoIdUntyped;;
        dto.setInstalacion(dtoId);

	}	
	
	
}
