package com.ofbox.davivienda.h2h.orm.jdbc.sql.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.sql.Connection;
import java.util.Collection;

import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Dao;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.ExcepcionEnDAO;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.ExcepcionPorDatoNoEncontrado;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.PeticionDeDatos;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Transaccion;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.foraneos.DataConValoresForaneos;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLColumnaMeta;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLEntidadMeta;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLImplementacionBasica;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLImplementacionOperacionesComunes;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLLenguaje;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLOperaciones;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLUtils;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.TransaccionJdbc;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dependencias.LocalizadorDeServicio;


import com.ofbox.davivienda.h2h.abstraccion.dto.HthBitacoraProceso;
import java.lang.Long;
import com.ofbox.davivienda.h2h.abstraccion.dao.HthBitacoraProcesoDao;




/**
 * Clase que hereda comportamientos de {@link SQLImplementacionBasica} e implementa el comportamiento definido en la capa DAO.<br/>
 * Esta clase es una implementaci&oacute;n intermedia para el DAO de <b>Bit&aacute;cora Proceso</b>
 * asociado a la tabla <b>HTH_Bitacora_Proceso</b>, en esta clase se construyen todos los sql para las operaciones de listado,
 * creaci&oacute;n, consulta, actualizaci&oacute;n y elminaci&oacute;n.<br/>
 * @author Systems Out of the Box
 * @version f3.1
 *
 */
public class HthBitacoraProcesoDaoJdbcSql extends SQLImplementacionBasica implements HthBitacoraProcesoDao{

	/**
	 * Atributo del tipo {@link SQLEntidadMeta}, que almacena la descripci&oacute;n del negocio como una entidad.<br/>
	 * Entre sus caracter&iacute;sticas tiene el nombre de la tabla, nombre de la secuencia, posici&oacute;n de las columnas,
	 * nombre de las columnas y tipos de datos para la columna.<br/>
	 * El arreglo que recibe son objetos de tipo {@link SQLColumnaMeta}, y definen informaci&oacute;n para obtener las columnas.
	 */
	public static final SQLEntidadMeta SQL_ENTIDAD_META = new SQLEntidadMeta("HTH_Bitacora_Proceso", new SQLColumnaMeta[]{
        new SQLColumnaMeta(14, "HTH_Bitacora_Proceso", "E14", 14001, "id_bitacora_proceso", "C14001", Types.BIGINT ,true)
     ,  new SQLColumnaMeta(14, "HTH_Bitacora_Proceso", "E14", 14002, "fecha_proceso", "C14002", Types.TIMESTAMP )
     ,  new SQLColumnaMeta(14, "HTH_Bitacora_Proceso", "E14", 14003, "resultado_proceso", "C14003", Types.VARCHAR )
     ,  new SQLColumnaMeta(14, "HTH_Bitacora_Proceso", "E14", 14004, "descripcion_resultado", "C14004", Types.VARCHAR )
     ,  new SQLColumnaMeta(14, "HTH_Bitacora_Proceso", "E14", 14005, "stack_trace", "C14005", Types.VARCHAR )
     ,  new SQLColumnaMeta(14, "HTH_Bitacora_Proceso", "E14", 14006, "cliente", "C14006", Types.VARCHAR )
     ,  new SQLColumnaMeta(14, "HTH_Bitacora_Proceso", "E14", 14007, "convenio", "C14007", Types.VARCHAR )
     ,  new SQLColumnaMeta(14, "HTH_Bitacora_Proceso", "E14", 14008, "archivo", "C14008", Types.VARCHAR )
     ,  new SQLColumnaMeta(14, "HTH_Bitacora_Proceso", "E14", 14009, "lineas_bitacora", "C14009", Types.VARCHAR )
	});
	
	


	/**
	 * Atributo que almacena el <code>String</code> para obtener el listado de registros del negocio.
	 */
	public static final String SELECT_HEADER_SQL =  
		"SELECT E14.id_bitacora_proceso AS C14001 , E14.fecha_proceso AS C14002 , E14.resultado_proceso AS C14003 , E14.descripcion_resultado AS C14004 , " +
		"       E14.stack_trace AS C14005 , E14.cliente AS C14006 , E14.convenio AS C14007 , E14.archivo AS C14008 , " +
		"       E14.lineas_bitacora AS C14009" +
		"  FROM HTH_Bitacora_Proceso E14";
	/**
	 * Atributo que almacena el <code>String</code> para obtener el n&uacute;mero de registros presentes en el negocio.
	 */
	public static final String COUNT_HEADER_SQL =   "SELECT COUNT(*) FROM HTH_Bitacora_Proceso";    												
	/**
	 * Atributo que almacena el <code>String</code> para la inserci&oacute;n, sin id, de un nuevo registro en la tabla.
	 */
	public static final String CREATE_SQL = 
		"INSERT INTO HTH_Bitacora_Proceso (" +
		"fecha_proceso,resultado_proceso,descripcion_resultado,stack_trace,cliente" +
		",convenio,archivo,lineas_bitacora" +
		") VALUES (?,?,?,?,?,?,?,?)";
	/**
	 * Atributo que almacena el <code>String</code> para insertar, con id, un registro en la table.
	 */
	public static final String CREATE_SQL_CON_ID = 
		"INSERT INTO HTH_Bitacora_Proceso (" +
		"id_bitacora_proceso,fecha_proceso,resultado_proceso,descripcion_resultado,stack_trace" +
		",cliente,convenio,archivo,lineas_bitacora" +
		"" +
		") VALUES (?,?,?,?,?,?,?,?,?)";
	/**
	 * Atributo que almacena el <code>String</code> para la atualizaci&oacute; de un registro en la tabla.
	 */
	public static final String UPDATE_SQL = 
		"UPDATE HTH_Bitacora_Proceso" +
		" SET fecha_proceso= ?  , resultado_proceso= ?  , descripcion_resultado= ?  , stack_trace= ?  , " +
		"cliente= ?  , convenio= ?  , archivo= ?  , lineas_bitacora= ? " +
		" WHERE id_bitacora_proceso= ? ";
	/**
	 * Atributo que almacena el <code>String</code> para la eliminaci&oacute; de un registro en la tabla.
	 */
	public static final String DELETE_SQL =  
		"DELETE FROM HTH_Bitacora_Proceso" +
		" WHERE id_bitacora_proceso= ? ";
	/**
	 * Atributo que almacena el <code>String</code> para la b&uacute;squeda por id de un registro de la tabla.
	 */
	public static final String FIND_BY_ID_SQL = 
		"SELECT E14.id_bitacora_proceso AS C14001 , E14.fecha_proceso AS C14002 , E14.resultado_proceso AS C14003 , E14.descripcion_resultado AS C14004 , " +
		"       E14.stack_trace AS C14005 , E14.cliente AS C14006 , E14.convenio AS C14007 , E14.archivo AS C14008 , " +
		"       E14.lineas_bitacora AS C14009" +
		"  FROM HTH_Bitacora_Proceso E14" +
		" WHERE E14.id_bitacora_proceso= ? ";
	/**
	 * Atributo que almacena el <code>String</code> para la creacion de la tabla en la base de datos.
	 */
	public static final String CREATE_TABLE_DDL = 
		"CREATE TABLE HTH_Bitacora_Proceso (" +
		"    id_bitacora_proceso LONG NOT NULL," +
		"    fecha_proceso TIMESTAMP NOT NULL," +
		"    resultado_proceso VARCHAR(1)  NOT NULL," +
		"    descripcion_resultado VARCHAR(200)  NOT NULL," +
		"    stack_trace VARCHAR(2000) ," +
		"    cliente VARCHAR(100) ," +
		"    convenio VARCHAR(100) ," +
		"    archivo VARCHAR(200) ," +
		"    lineas_bitacora VARCHAR(4000) " +
		")";
	/**
	 * Atributo que almacena un objeto del tipo {@link SQLLenguaje}.
	 */
	private static SQLLenguaje lenguaje;
	
	/**
	 * M&eacute;todo que realiza la creaci&oacute;n de un DTO, del negocio, a partir del resultado de alguna consulta.
	 * @param <code>rst</code> objeto de tipo <code>Resultset</code>, que contiene los resultados de la consulta.
	 * @return <code>Object</code> - representa al DTO creado.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static Object crearDtoST(ResultSet rst)
			throws SQLException, ExcepcionEnDAO {
 	    HthBitacoraProceso theDto = new HthBitacoraProceso();
     	theDto.setIdBitacoraProceso(SQLUtils.getLongFromResultSet(rst, "C14001"));
		theDto.setFechaProceso(rst.getTimestamp("C14002"));
		theDto.setResultadoProceso(rst.getString("C14003"));
		theDto.setDescripcionResultado(rst.getString("C14004"));
		theDto.setStackTrace(rst.getString("C14005"));
		theDto.setCliente(rst.getString("C14006"));
		theDto.setConvenio(rst.getString("C14007"));
		theDto.setArchivo(rst.getString("C14008"));
		theDto.setLineasBitacora(rst.getString("C14009"));
		return theDto;
	}
	
	







	/**
	 * M&eacute;todo que coloca los valores provenientes del objeto Dto en el objeto PreparedStatement,
	 * el cual proviene de la llamada a la implementacion de {@link SQLOperaciones}, este m&eacute;todo
	 * no toma en cuenta el valor del <code>id</code>, puesto que se asume que es un valor secuencial
	 * autonumerico o serial propio de la base de datos.
	 * 
	 * @param <code>theDtoUntyped</code> objeto sin tipo que almacena al DTO del negocio.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaCrearST(Object theDtoUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {			
	        HthBitacoraProceso theDto = (HthBitacoraProceso)theDtoUntyped;
				java.util.Date fechaProceso = theDto.getFechaProceso();     
		String resultadoProceso = theDto.getResultadoProceso();     
		String descripcionResultado = theDto.getDescripcionResultado();     
		String stackTrace = theDto.getStackTrace();     
		String cliente = theDto.getCliente();     
		String convenio = theDto.getConvenio();     
		String archivo = theDto.getArchivo();     
		String lineasBitacora = theDto.getLineasBitacora();     
		     
		SQLUtils.setIntoStatement( 1, fechaProceso, smt);  
    		     
		SQLUtils.setIntoStatement( 2, resultadoProceso, smt);  
    		     
		SQLUtils.setIntoStatement( 3, descripcionResultado, smt);  
    		     
		SQLUtils.setIntoStatement( 4, stackTrace, smt);  
    		     
		SQLUtils.setIntoStatement( 5, cliente, smt);  
    		     
		SQLUtils.setIntoStatement( 6, convenio, smt);  
    		     
		SQLUtils.setIntoStatement( 7, archivo, smt);  
    		     
		SQLUtils.setIntoStatement( 8, lineasBitacora, smt);  
    		
	}

	/**
	 * M&eacute;todo que coloca los valores provenientes del objeto Dto en el objeto PreparedStatement,
	 * el cual proviene de la llamada a la implementacion de {@link SQLOperaciones}, este m&eacute;todo
	 * toma en cuenta el <code>id</code>, lo que quiere decir que se usa al momento de  crear registros
	 * en donde el identificador no es un valor secuencial autonumerico o serial propio de la base de datos
	 *
	 * @param <code>theDtoUntyped</code> objeto sin tipo que almacena al DTO del negocio.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaCrearConIdST(Object theDtoUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {			
	        HthBitacoraProceso theDto = (HthBitacoraProceso)theDtoUntyped;
				Long idBitacoraProceso = theDto.getIdBitacoraProceso();     
		java.util.Date fechaProceso = theDto.getFechaProceso();     
		String resultadoProceso = theDto.getResultadoProceso();     
		String descripcionResultado = theDto.getDescripcionResultado();     
		String stackTrace = theDto.getStackTrace();     
		String cliente = theDto.getCliente();     
		String convenio = theDto.getConvenio();     
		String archivo = theDto.getArchivo();     
		String lineasBitacora = theDto.getLineasBitacora();     
	
		SQLUtils.setIntoStatement( 1, idBitacoraProceso, smt);
    	
		SQLUtils.setIntoStatement( 2, fechaProceso, smt);
    	
		SQLUtils.setIntoStatement( 3, resultadoProceso, smt);
    	
		SQLUtils.setIntoStatement( 4, descripcionResultado, smt);
    	
		SQLUtils.setIntoStatement( 5, stackTrace, smt);
    	
		SQLUtils.setIntoStatement( 6, cliente, smt);
    	
		SQLUtils.setIntoStatement( 7, convenio, smt);
    	
		SQLUtils.setIntoStatement( 8, archivo, smt);
    	
		SQLUtils.setIntoStatement( 9, lineasBitacora, smt);
    		
	}

	/**
	 * M&eacute;todo que coloca el identificador recibido en el objeto PreparedStatement, para poder ser eliminado.
	 * @param <code>theIdUntyped</code> objeto sin tipo que identifica al <code>id</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaEliminarST(Object theIdUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {
			Long theId = (Long)theIdUntyped;
		SQLUtils.setIntoStatement( 1, theId, smt);
	}

	/**
	 * M&eacute;todo que coloca el identificador recibido, en el objeto PreparedStatement para poder hacer la consulta de busqueda.
	 * @param <code>theIdUntyped</code> objeto sin tipo que identifica al <code>id</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaBuscarPorIdST(Object theIdUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {
			Long theId = (Long)theIdUntyped;
			SQLUtils.setIntoStatement( 1, theId, smt);
		}

	/**
	 * M&eacute;todo que coloca los valores provenientes del objeto Dto en el objeto PreparedStatement,
	 * el cual proviene de la llamada a la implementacion de {@link SQLOperaciones} para poder actualizar
     * un registro en espec&iacute;fico.
	 * 
	 * @param <code>theDtoUntyped</code> objeto sin tipo que almacena al DTO del negocio.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaActualizarST(Object theDtoUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {
			HthBitacoraProceso theDto = (HthBitacoraProceso)theDtoUntyped;
				Long idBitacoraProceso = theDto.getIdBitacoraProceso();     
		java.util.Date fechaProceso = theDto.getFechaProceso();     
		String resultadoProceso = theDto.getResultadoProceso();     
		String descripcionResultado = theDto.getDescripcionResultado();     
		String stackTrace = theDto.getStackTrace();     
		String cliente = theDto.getCliente();     
		String convenio = theDto.getConvenio();     
		String archivo = theDto.getArchivo();     
		String lineasBitacora = theDto.getLineasBitacora();     
		     
		SQLUtils.setIntoStatement( 1, fechaProceso, smt);  
        	     
		SQLUtils.setIntoStatement( 2, resultadoProceso, smt);  
        	     
		SQLUtils.setIntoStatement( 3, descripcionResultado, smt);  
        	     
		SQLUtils.setIntoStatement( 4, stackTrace, smt);  
        	     
		SQLUtils.setIntoStatement( 5, cliente, smt);  
        	     
		SQLUtils.setIntoStatement( 6, convenio, smt);  
        	     
		SQLUtils.setIntoStatement( 7, archivo, smt);  
        	     
		SQLUtils.setIntoStatement( 8, lineasBitacora, smt);  
        		     
		SQLUtils.setIntoStatement( 9, idBitacoraProceso, smt);  
    			
	}


	/**
	 * M&eacute;todo que ejecuta una petici&oacute;n de datos a trav&eacute;s de un objeto {@link PreparedStatement},
	 * y un objeto del tipo {@link PeticionDeDatos}, el cual en su implementaci&oacute;n define una serie de restricciones
	 * usadas para hacer la petici&oacute;n.
	 *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaPeticionDeDatosST(PeticionDeDatos peticion,
			PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
		SQLImplementacionOperacionesComunes.operacionesParaPeticionDeDatosST( peticion, smt, SQL_OPERACIONES, null);	
	}	
	
	/**
	 * M&eacute;todo que ejecuta una petici&oacute;n de datos a trav&eacute;s de un objeto {@link PreparedStatement},
     * y un objeto del tipo {@link PeticionDeDatos}, el cual en su implementaci&oacute;n define una serie de restricciones
     * usadas para hacer la petici&oacute;n.
	 *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaPeticionDeDatosFST(PeticionDeDatos peticion,
			PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
		SQLImplementacionOperacionesComunes.operacionesParaPeticionDeDatosST( peticion, smt, SQL_OPERACIONES, null);	
	}	

	/**
	 * M&eacute;todo que ejecuta una petici&oacute;n de conteo a trav&eacute;s de un objeto {@link PreparedStatement},
     * y un objeto del tipo {@link PeticionDeDatos}, el cual en su implementaci&oacute;n define una serie de restricciones
     * usadas para hacer la petici&oacute;n.
	 *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaConteoST(PeticionDeDatos peticion,
			PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
		SQLImplementacionOperacionesComunes.operacionesParaPeticionDeDatosST( peticion, smt, SQL_OPERACIONES, null);
	}

	/**
	 * M&eacute;todo que agrega al atributo {@link #SELECT_HEADER_SQL} las condiciones de b&uacute;squeda
     * que vienen en el objeto del tipo {@link PeticionDeDatos}
     *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>, que contiene las condiciones de la consulta.
	 * @return <code>String</code> - la consulta condicionada como cadena de caracteres.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static String getSqlParaPeticionDeDatosST(PeticionDeDatos peticion)
			throws SQLException, ExcepcionEnDAO {
		return SELECT_HEADER_SQL + SQLImplementacionOperacionesComunes.crearCondicionesSQLST( peticion, SQL_OPERACIONES, SQLImplementacionOperacionesComunes.COND_ALIAS_ORDEN_WHERE);
	}
	
	
	
	

	/**
	 * M&eacute;todo que agrega al atributo {@link #COUNT_HEADER_SQL} las condiciones de conteo
	 * que vienen en el objeto de tipo {@link PeticionDeDatos}.
	 *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>, que contiene las condiciones de la consulta.
	 * @return <code>String</code> - la consulta condicionada como cadena de caracteres.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static String getSqlDeConteoST(PeticionDeDatos peticion)
			throws SQLException, ExcepcionEnDAO {
		return COUNT_HEADER_SQL + SQLImplementacionOperacionesComunes.crearCondicionesSQLST( peticion, SQL_OPERACIONES, SQLImplementacionOperacionesComunes.COND_SQL_WHERE);
	}
	
	/**
	 * M&eacute;todo que se utiliza para actualizar un registro a nivel de tabla, recibe como par&aacute;metro un DTO del negocio
	 * que contiene la informaci&oacute;n a actualizar, e invoca la funci&oacute;n <code>actualizar</code> de la clase de la que hereda.
	 *
	 * @param <code>hthBitacoraProceso</code> objeto del tipo <code>HthBitacoraProceso</code>.
	 * @throws ExcepcionEnDAO
	 * @see {@link SQLImplementacionBasica}
	 */
	public void actualizar(HthBitacoraProceso hthBitacoraProceso) throws ExcepcionEnDAO {
	    actualizar(hthBitacoraProceso, AUTO_COMMIT);
	
	}
	
	/**
	 * M&eacute;todo utilizado para realizar una b&uacute;squeda de un registro espec&iacute;fico, utilizando un identificador.
	 *
	 * @param <code>idBitacoraProcesoId</code> objeto del tipo <code>Long</code>.
	 * @return <code>HthBitacoraProceso</code> - un objeto DTO.
	 * @throws ExcepcionEnDAO
	 * @see {@link SQLImplementacionBasica}
	 */
	public HthBitacoraProceso buscarPorID(Long idBitacoraProcesoId)
			throws ExcepcionEnDAO {
		return (HthBitacoraProceso)buscarPorID(idBitacoraProcesoId, AUTO_COMMIT);				 
	}

	/**
	 * M&eacute;todo utilizado para la creaci&oacute;n de un registro, a nivel de tabla, a partir de la informaci&oacute;n
	 * recibida en un DTO del objeto de Negocio, e invoca la funci&oacute;n <code>crear</code> de la clase de la que hereda.
	 * 
	 * @param <code>hthBitacoraProceso</code> objeto del tipo <code>HthBitacoraProceso</code>.
	 * @throws ExcepcionEnDAO
	 * @see {@link SQLImplementacionBasica}
	 */
	public void crear(HthBitacoraProceso hthBitacoraProceso) throws ExcepcionEnDAO {
	    crear(hthBitacoraProceso, AUTO_COMMIT);		
	}

	/**
	 * M&eacute;todo utilizado para la eliminaci&oacute;n de un registro, a nivel de tabla,
	 * que se busca a trav&eacute;s del identificador recibido. Se invoca la funci&oacute;n <code>eliminar</code> de la clase de la que hereda.
	 *
	 * @param <code>idBitacoraProcesoId</code> objeto del tipo <code>Long</code>.
	 * @throws ExcepcionEnDAO
	 * @see {@link SQLImplementacionBasica}
	 */
	public void eliminar(Long idBitacoraProcesoId) throws ExcepcionEnDAO {
	    eliminar(idBitacoraProcesoId, AUTO_COMMIT);		
	}

	/**
	 * M&eacute;todo utilizado para devolver un &uacute;nico resultado de la petici&oacute;n recibida,
	 * la cual se realiza a trav&eacuate;s del objeto de tipo {@link PeticionDeDatos}.
	 * 
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>, que contiene las condiciones de la consulta.
	 * @return <code>HthBitacoraProceso</code> - un objeto DTO.
	 * @throws ExcepcionEnDAO
	 * @throws ExcepcionPorDatoNoEncontrado
	 * @see {@link SQLImplementacionBasica}
	 */
	public HthBitacoraProceso resultadoUnico(PeticionDeDatos peticion)
			throws ExcepcionEnDAO, ExcepcionPorDatoNoEncontrado {
		return (HthBitacoraProceso)resultadoUnico(peticion, AUTO_COMMIT);
	}
	
	/**
	 * Atributo del tipo {@link SQLOperaciones}, almacena la operaciones disponibles para esta implementaci&oacute;n.<br/>
	 * Algunas de las llamadas a los m&eacute;todos se realiza a trav&eacute;s de la invocaci&oacute;n de estas implementaciones.
	 */
    public static final SQLOperaciones	SQL_OPERACIONES =  new SQLOperaciones() {
			
			public int getManejoForaneosPorDefecto() {
				return Dao.SIN_FORANEOS;
			}
			
			
			public void operacionesParaPeticionDeDatosF(PeticionDeDatos request,
					PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
				HthBitacoraProcesoDaoJdbcSql.operacionesParaPeticionDeDatosFST(request, smt);
			}
			
			public void operacionesParaPeticionDeDatos(PeticionDeDatos request,
					PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
				HthBitacoraProcesoDaoJdbcSql.operacionesParaPeticionDeDatosST(request, smt);
			}
			
			public void operacionesParaEliminar(Object theId, PreparedStatement smt)
					throws SQLException, ExcepcionEnDAO {
				HthBitacoraProcesoDaoJdbcSql.operacionesParaEliminarST(theId, smt);
			}
			
			public void operacionesParaCrear(Object theDto, PreparedStatement smt)
					throws SQLException, ExcepcionEnDAO {
				HthBitacoraProcesoDaoJdbcSql.operacionesParaCrearST(theDto, smt);
			}
			
			public void operacionesParaCrearConId(Object theDto, PreparedStatement smt) 
			        throws SQLException, ExcepcionEnDAO {
			    HthBitacoraProcesoDaoJdbcSql.operacionesParaCrearConIdST(theDto, smt);
            }
			
			public void operacionesParaConteo(PeticionDeDatos request,
					PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
				HthBitacoraProcesoDaoJdbcSql.operacionesParaConteoST(request, smt);				
			}
			
			public void operacionesParaBuscarPorId(Object theId, PreparedStatement smt)
					throws SQLException, ExcepcionEnDAO {
				HthBitacoraProcesoDaoJdbcSql.operacionesParaBuscarPorIdST(theId, smt);
			}
			
			public void operacionesParaActualizar(Object theDto, PreparedStatement smt)
					throws SQLException, ExcepcionEnDAO {
				HthBitacoraProcesoDaoJdbcSql.operacionesParaActualizarST(theDto, smt);
				
			}
			
			public String getSqlParaPeticionDeDatosF(PeticionDeDatos request)
					throws SQLException, ExcepcionEnDAO {	
				return null;
			}
			
			public String getSqlParaPeticionDeDatos(PeticionDeDatos request)
					throws SQLException, ExcepcionEnDAO {
				return HthBitacoraProcesoDaoJdbcSql.getSqlParaPeticionDeDatosST(request);
			}
			
			public String getSqlDeConteo(PeticionDeDatos request) throws SQLException,
					ExcepcionEnDAO {
				return HthBitacoraProcesoDaoJdbcSql.getSqlDeConteoST(request);
			}
			
			public SQLEntidadMeta getSQLEntidadMeta() throws ExcepcionEnDAO {
				return HthBitacoraProcesoDaoJdbcSql.SQL_ENTIDAD_META;
			}
			
			public String getEliminarSql() {
				return  HthBitacoraProcesoDaoJdbcSql.DELETE_SQL;
			}
			
			public String getCrearSql() {
				return  HthBitacoraProcesoDaoJdbcSql.CREATE_SQL;
			}
			
			public String getCrearSqlConId(){
			    return  HthBitacoraProcesoDaoJdbcSql.CREATE_SQL_CON_ID;
			}			
			
			public String getBuscarPorIdSqlF() {
				return null;
			}
			
			public String getBuscarPorIdSql() {				
				return HthBitacoraProcesoDaoJdbcSql.FIND_BY_ID_SQL;
			}
			
			public String getActualizarSql() {				
				return HthBitacoraProcesoDaoJdbcSql.UPDATE_SQL;
			}
			
			public Object crearDto(ResultSet rst) throws SQLException, ExcepcionEnDAO {
				return HthBitacoraProcesoDaoJdbcSql.crearDtoST(rst);
			}
	        
	        public Object crearDataConValoresForaneos(ResultSet rst)
					throws SQLException, ExcepcionEnDAO {
				return null;
			}
			
	        public Object crearDataConValoresForaneosNoRel(ResultSet rst, Connection conn)
					throws SQLException, ExcepcionEnDAO {
				return null;
			}
			
			
            public boolean isForaneosDisponible(){
				return false;
            
            }
            
            public SQLLenguaje getSQLLenguaje() {
                return HthBitacoraProcesoDaoJdbcSql.getLenguaje();
            }
            
            public void setIdEnDto(Object theDto, Object dtoId) {
                setIdEnDtoST(theDto, dtoId);
            }
	};
	
	
	
	
	
	/**
	 * Devuelve el atributo {@link #SQL_OPERACIONES}.
	 * @return <code>SQLOperaciones</code> - objeto del tipo {@link SQLOperaciones}
	 */
    public SQLOperaciones getSqlOperaciones() {
       return SQL_OPERACIONES;
	}
	
	/**
	 * Devuelve el atributo {@link #lenguaje}
	 * @return <code>SQLLenguaje</code> - objeto del tipo {@link SQLLenguaje}
	 */	
	public static SQLLenguaje getLenguaje() {
		return lenguaje;
	}
	
	/**
	 * M&eacute;todo que permite obtener a trav&eacute;s de un Servicio la implementaci&oacute;n del objeto de Lenguaje,
	 * y coloca las reglas de negocio almacenadas en el atributo {@link #SQL_ENTIDAD_META}.
	 * @param <code>localizadorLenguaje</code> objeto del tipo {@link LocalizadorDeServicio} que posee informaci&oacute;n para trabajar con la base de datos.
	 */
	public static void setLocalizadorLenguaje(LocalizadorDeServicio localizadorLenguaje) {
		lenguaje = (SQLLenguaje) localizadorLenguaje.getServicio();
		lenguaje.setSqlEntidadMeta(SQL_ENTIDAD_META);
	}

	/**
	 * M&eacute;todo que agrega un identificador a un DTO del tipo <code>HthBitacoraProceso</code>.
	 * @param <code>theDtoUntyped</code> objeto gen&eacute;rico, que representa a un <code>DTO</code>.
	 * @param <code>dtoIdUntyped</code> objeto gen&eacute;rico, que representa al identificador.
	 */
	public static void setIdEnDtoST(Object theDtoUntyped, Object dtoIdUntyped){
	    HthBitacoraProceso dto = (HthBitacoraProceso)theDtoUntyped;
        Long dtoId = (Long) dtoIdUntyped;;
        dto.setIdBitacoraProceso(dtoId);

	}	
	
	
}
