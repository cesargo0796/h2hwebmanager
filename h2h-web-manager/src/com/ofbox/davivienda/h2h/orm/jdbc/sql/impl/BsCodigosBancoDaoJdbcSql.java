package com.ofbox.davivienda.h2h.orm.jdbc.sql.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import org.json.JSONObject;

import com.ofbox.davivienda.h2h.abstraccion.dao.BsBancoAchDao;
import com.ofbox.davivienda.h2h.abstraccion.dao.BsCodigosBancoDao;
import com.ofbox.davivienda.h2h.abstraccion.dao.BsRedOperacionBancoDao;
import com.ofbox.davivienda.h2h.abstraccion.dto.BsBancoAch;
import com.ofbox.davivienda.h2h.abstraccion.dto.BsCodigosBanco;
import com.ofbox.davivienda.h2h.abstraccion.dto.BsRedOperacionBanco;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Dao;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.ExcepcionEnDAO;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.ExcepcionPorDatoNoEncontrado;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.PeticionDeDatos;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Transaccion;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dependencias.LocalizadorDeServicio;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLColumnaMeta;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLEntidadMeta;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLImplementacionBasica;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLImplementacionOperacionesComunes;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLLenguaje;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLOperaciones;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLUtils;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.TransaccionJdbc;




/**
 * Clase que hereda comportamientos de {@link SQLImplementacionBasica} e implementa el comportamiento definido en la capa DAO.<br/>
 * Esta clase es una implementaci&oacute;n intermedia para el DAO de <b>BS Detalle ACH</b>
 * asociado a la tabla <b>BS_BancoACH</b>, en esta clase se construyen todos los sql para las operaciones de listado,
 * creaci&oacute;n, consulta, actualizaci&oacute;n y elminaci&oacute;n.<br/>
 * @author Systems Out of the Box
 * @version f3.1
 *
 */
public class BsCodigosBancoDaoJdbcSql extends SQLImplementacionBasica implements BsCodigosBancoDao{

	/**
	 * Atributo del tipo {@link SQLEntidadMeta}, que almacena la descripci&oacute;n del negocio como una entidad.<br/>
	 * Entre sus caracter&iacute;sticas tiene el nombre de la tabla, nombre de la secuencia, posici&oacute;n de las columnas,
	 * nombre de las columnas y tipos de datos para la columna.<br/>
	 * El arreglo que recibe son objetos de tipo {@link SQLColumnaMeta}, y definen informaci&oacute;n para obtener las columnas.
	 */
	public static final SQLEntidadMeta SQL_ENTIDAD_META = new SQLEntidadMeta("BS_CodigosBanco", new SQLColumnaMeta[]{
        new SQLColumnaMeta(5, "BS_CodigosBanco", "E5", 5001, "IdCodigoBanco", "C5001", Types.BIGINT ,true)
     ,  new SQLColumnaMeta(5, "BS_CodigosBanco", "E5", 5002, "CodigoSwift", "C5002", Types.VARCHAR )
     ,  new SQLColumnaMeta(5, "BS_CodigosBanco", "E5", 5003, "CodigoUni", "C5003", Types.VARCHAR )
     ,  new SQLColumnaMeta(5, "BS_CodigosBanco", "E5", 5004, "CodigoTransfer", "C5004", Types.BIGINT )
     ,  new SQLColumnaMeta(5, "BS_CodigosBanco", "E5", 5005, "IdRedOperacion", "C5005", Types.BIGINT )
     ,  new SQLColumnaMeta(5, "BS_CodigosBanco", "E5", 5006, "Status", "C5006", Types.VARCHAR )
     ,  new SQLColumnaMeta(5, "BS_CodigosBanco", "E5", 5007, "CodiBanco", "C5007", Types.BIGINT )
     
	  ,  new SQLColumnaMeta(6, "BS_BancoACH", "E6", 6001, "codBanco", "C6001", Types.BIGINT ,true)    
	  ,  new SQLColumnaMeta(6, "BS_BancoACH", "E6", 6002, "codPayBank", "C6002", Types.VARCHAR )   
	  ,  new SQLColumnaMeta(6, "BS_BancoACH", "E6", 6003, "digitoChequeo", "C6003", Types.SMALLINT )   
	  ,  new SQLColumnaMeta(6, "BS_BancoACH", "E6", 6004, "enlinea", "C6004", Types.BIGINT )   
	  ,  new SQLColumnaMeta(6, "BS_BancoACH", "E6", 6005, "esActivo", "C6005", Types.BOOLEAN )   
	  ,  new SQLColumnaMeta(6, "BS_BancoACH", "E6", 6006, "longitudCtaAhorro", "C6006", Types.SMALLINT )   
	  ,  new SQLColumnaMeta(6, "BS_BancoACH", "E6", 6007, "longitudCtaCorriente", "C6007", Types.SMALLINT )   
	  ,  new SQLColumnaMeta(6, "BS_BancoACH", "E6", 6008, "longitudCtaPrestamo", "C6008", Types.SMALLINT )   
	  ,  new SQLColumnaMeta(6, "BS_BancoACH", "E6", 6009, "longitudTjCredito", "C6009", Types.SMALLINT )   
	  ,  new SQLColumnaMeta(6, "BS_BancoACH", "E6", 6010, "idAs400", "C6010", Types.BIGINT )   
	  ,  new SQLColumnaMeta(6, "BS_BancoACH", "E6", 6011, "moneda", "C6011", Types.BIGINT )   
	  ,  new SQLColumnaMeta(6, "BS_BancoACH", "E6", 6012, "nombre", "C6012", Types.VARCHAR )
     
     ,  new SQLColumnaMeta(7, "BS_RedOperacionBanco", "E7", 7001, "IdRedOperacion", "C7001", Types.BIGINT)
     ,  new SQLColumnaMeta(7, "BS_RedOperacionBanco", "E7", 7002, "Interpretacion", "C7002", Types.VARCHAR)
	});
	

	public BsBancoAch getBsBancoAch(BsCodigosBanco bsCodigosBanco) throws ExcepcionEnDAO {
		return getBsBancoAch(bsCodigosBanco, null);
	}

	public BsBancoAch getBsBancoAch(BsCodigosBanco bsCodigosBanco, Transaccion tx) throws ExcepcionEnDAO {	     
		BsBancoAch bsBancoAchRel = bsCodigosBanco.getBsBancoAch();
		if(bsBancoAchRel==null){
		       PeticionDeDatos peticionDeDatosParaBsCliente = SQLImplementacionBasica.crearPeticionDeDatosST();
		       peticionDeDatosParaBsCliente.agregarRestriccion(PeticionDeDatos.IGUAL,
		    		   					BsBancoAchDao.CODBANCO, 
		       							bsCodigosBanco.getIdRedOperacion()
			                              );
		       TransaccionJdbc tjdbc = (TransaccionJdbc)tx;
		       bsBancoAchRel = (BsBancoAch)SQLImplementacionOperacionesComunes.resultadoUnicoST(
							            peticionDeDatosParaBsCliente, 
										tjdbc!=null? tjdbc.getConn(): getConnection(), 
										BsClienteDaoJdbcSql.SQL_OPERACIONES, 
										tjdbc==null, 
										getLog());
		       bsCodigosBanco.setBsBancoAch(bsBancoAchRel);										
		}										
		return 	bsBancoAchRel;	
	}	
	
	public BsRedOperacionBanco getBsRedOperacionBanco(BsCodigosBanco bsCodigosBanco)throws ExcepcionEnDAO{
		return getBsRedOperacionBanco(bsCodigosBanco, null);
	}

    public BsRedOperacionBanco getBsRedOperacionBanco(BsCodigosBanco bsCodigosBanco,Transaccion tx)throws ExcepcionEnDAO{
         	     
    	BsRedOperacionBanco bsRedOperacionBancoRel = bsCodigosBanco.getBsRedOperacionBanco();
	     if(bsRedOperacionBancoRel==null){
                PeticionDeDatos peticionDeDatosParaBsCliente = SQLImplementacionBasica.crearPeticionDeDatosST();
                peticionDeDatosParaBsCliente.agregarRestriccion(PeticionDeDatos.IGUAL,
                							BsRedOperacionBancoDao.IDREDOPERACION, 
                							bsCodigosBanco.getIdRedOperacion()
	     	                              );
	            TransaccionJdbc tjdbc = (TransaccionJdbc)tx;
	            bsRedOperacionBancoRel = (BsRedOperacionBanco)SQLImplementacionOperacionesComunes.resultadoUnicoST(
							            peticionDeDatosParaBsCliente, 
										tjdbc!=null? tjdbc.getConn(): getConnection(), 
										BsClienteDaoJdbcSql.SQL_OPERACIONES, 
										tjdbc==null, 
										getLog());
	            bsCodigosBanco.setBsRedOperacionBanco(bsRedOperacionBancoRel);										
		}										
		return 	bsRedOperacionBancoRel;									
    }
    
	/**
	 * Atributo que almacena el <code>String</code> para obtener el listado de registros de la tabla principal
	 * junto con otras tablas foraneas, realiza un JOIN.
	 */
	public static final String SELECT_HEADER_SQL_F =  
			"SELECT E5.IdCodigoBanco as C5001, E5.CodigoSwift as C5002, E5.CodigoUni as C5003, " +
					"E5.CodigoTransfer as C5004, E5.IdRedOperacion as C5005, E5.Status as C5006, E5.CodiBanco as C5007, "+
					"E6.codBanco as C6001, E6.codPayBank as C6002, E6.digitoChequeo as C6003, E6.enlinea as C6004, "+
					"E6.esActivo as C6005, E6.longitudCtaAhorro as C6006, E6.longitudCtaCorriente as C6007, E6.longitudCtaPrestamo as C6008, "+
					"E6.longitudTjCredito as C6009, E6.idAs400 as C6010, E6.moneda as C6011, E6.nombre as C6012, "+
					"E7.IdRedOperacion as C7001, E7.Interpretacion as C7002 "+
					"FROM BS_CodigosBanco E5 "+
					"INNER JOIN BS_BancoACH E6 on E6.CodBanco = E5.CodiBanco "+
					"INNER JOIN BS_RedOperacionBanco E7 on E7.IdRedOperacion = E5.IdRedOperacion";
	/**
	 * Atributo que almacena el <code>String</code> para obtener el listado de registros de la tabla principal
     * junto con otras tablas foraneas, realiza un JOIN utilizando la <code>primary key</code> de la tabla principal.
	 */
	public static final String FIND_BY_ID_SQL_F =  
			"SELECT E5.IdCodigoBanco as C5001, E5.CodigoSwift as C5002, E5.CodigoUni as C5003, " +
					"E5.CodigoTransfer as C5004, E5.IdRedOperacion as C5005, E5.Status as C5006, E5.CodiBanco as C5007, " + 
					"E6.codBanco as C6001, E6.codPayBank as C6002, E6.digitoChequeo as C6003, E6.enlinea as C6004, "+
					"E6.esActivo as C6005, E6.longitudCtaAhorro as C6006, E6.longitudCtaCorriente as C6007, E6.longitudCtaPrestamo as C6008, "+
					"E6.longitudTjCredito as C6009, E6.idAs400 as C6010, E6.moneda as C6011, E6.nombre as C6012, "+
					"E7.IdRedOperacion as C7001, E7.Interpretacion as C7002 "+
					"FROM BS_CodigosBanco E5 "+
					"INNER JOIN BS_BancoACH E6 on E6.CodBanco = E5.CodiBanco "+
					"INNER JOIN BS_RedOperacionBanco E7 on E7.IdRedOperacion = E5.IdRedOperacion "+		
					"WHERE E5.IdCodigoBanco= ?";

	/**
	 * Atributo que almacena el <code>String</code> para obtener el listado de registros del negocio.
	 */
	public static final String SELECT_HEADER_SQL =  
		"SELECT E5.IdCodigoBanco as C5001, E5.CodigoSwift as C5002, E5.CodigoUni as C5003, " +
		"E5.CodigoTransfer as C5004, E5.IdRedOperacion as C5005, E5.Status as C5006, E5.CodiBanco as C5007, "+
		"E6.codBanco as C6001, E6.codPayBank as C6002, E6.digitoChequeo as C6003, E6.enlinea as C6004, "+
		"E6.esActivo as C6005, E6.longitudCtaAhorro as C6006, E6.longitudCtaCorriente as C6007, E6.longitudCtaPrestamo as C6008, "+
		"E6.longitudTjCredito as C6009, E6.idAs400 as C6010, E6.moneda as C6011, E6.nombre as C6012, "+
		"E7.IdRedOperacion as C7001, E7.Interpretacion as C7002 "+
		"FROM BS_CodigosBanco E5 "+
		"INNER JOIN BS_BancoACH E6 on E6.CodBanco = E5.CodiBanco "+
		"INNER JOIN BS_RedOperacionBanco E7 on E7.IdRedOperacion = E5.IdRedOperacion";
	/**
	 * Atributo que almacena el <code>String</code> para obtener el n&uacute;mero de c&oacute;digos de banco.
	 */
	public static final String COUNT_HEADER_SQL =   "SELECT COUNT(*) FROM BS_CodigosBanco";    												
	/**
	 * Atributo que almacena el <code>String</code> para la inserci&oacute;n, sin id, de un nuevo registro en la tabla.
	 */
	public static final String CREATE_SQL = 
		"INSERT INTO BS_CodigosBanco " +
		"(CodiBanco,CodigoSwift, CodigoUni, CodigoTransfer, IdRedOperacion, Status) "+
		"VALUES (?,?,?,?,?,?)";
	/**
	 * Atributo que almacena el <code>String</code> para insertar, con id, un registro en la table.
	 */
	public static final String CREATE_SQL_CON_ID = 
		"INSERT INTO dbo.BS_CodigosBanco " +
		"(CodiBanco,IdCodigoBanco, CodigoSwift, CodigoUni, CodigoTransfer, IdRedOperacion, Status) "+
		"VALUES (?,?,?,?,?,?,?)";
	/**
	 * Atributo que almacena el <code>String</code> para la atualizaci&oacute; de un registro en la tabla.
	 */
	public static final String UPDATE_SQL = 
		"UPDATE BS_CodigosBanco SET CodiBanco=?, CodigoSwift=?, CodigoUni=?, CodigoTransfer=?, IdRedOperacion=?, Status=? "
		+ "WHERE IdCodigoBanco=?";
	/**
	 * Atributo que almacena el <code>String</code> para la eliminaci&oacute; de un registro en la tabla.
	 */
	public static final String DELETE_SQL = 
		"UPDATE BS_CodigosBanco SET Status= ? "+
		"WHERE IdCodigoBanco=?";
	/**
	 * Atributo que almacena el <code>String</code> para la b&uacute;squeda por id de un registro de la tabla.
	 */
	public static final String FIND_BY_ID_SQL = 
		"SELECT E5.IdCodigoBanco as C5001, E5.CodigoSwift as C5002, E5.CodigoUni as C5003, " +
		"E5.CodigoTransfer as C5004, E5.IdRedOperacion as C5005, E5.Status as C5006, E5.CodiBanco as C5007, " + 
		"E6.codBanco as C6001, E6.codPayBank as C6002, E6.digitoChequeo as C6003, E6.enlinea as C6004, "+
		"E6.esActivo as C6005, E6.longitudCtaAhorro as C6006, E6.longitudCtaCorriente as C6007, E6.longitudCtaPrestamo as C6008, "+
		"E6.longitudTjCredito as C6009, E6.idAs400 as C6010, E6.moneda as C6011, E6.nombre as C6012, "+
		"E7.IdRedOperacion as C7001, E7.Interpretacion as C7002 "+
		"FROM BS_CodigosBanco E5 "+
		"INNER JOIN BS_BancoACH E6 on E6.CodBanco = E5.CodiBanco "+
		"INNER JOIN BS_RedOperacionBanco E7 on E7.IdRedOperacion = E5.IdRedOperacion "+		
		"WHERE E5.IdCodigoBanco= ?";
	/**
	 * Atributo que almacena el <code>String</code> para la creacion de la tabla en la base de datos.
	 */
	public static final String CREATE_TABLE_DDL = 
		"CREATE " + 
		"    TABLE BancaEmpresaPlus.dbo.BS_CodigosBanco " + 
		"    (" + 
		"        IdCodigoBanco INT NOT NULL IDENTITY," + 
		"        CodiBanco INT NOT NULL," + 
		"        CodigoSwift VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL," + 
		"        CodigoUNI VARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL," + 
		"        CodigoTransfer INT NOT NULL," + 
		"        IdRedOperacion INT NOT NULL," + 
		"        Status VARCHAR(10) NOT NULL DEFAULT 'ACTIVO'," + 
		"        CONSTRAINT PK_BS_Codig_F93D241E05BA6B6A PRIMARY KEY (IdCodigoBanco)," + 
		"        CONSTRAINT FK_BS_CodigoIdRed_398D8EEE FOREIGN KEY (IdRedOperacion) REFERENCES BS_RedOperacionBanco (IdRedOperacion)," + 
		"        CONSTRAINT FK_BS_CodigoBanco FOREIGN KEY (CodiBanco) REFERENCES BS_BancoACH (CodBanco)," + 
		"        CONSTRAINT IX_CodigoBanco UNIQUE (CodigoSwift)" + 
		"    )";
	/**
	 * Atributo que almacena un objeto del tipo {@link SQLLenguaje}.
	 */
	private static SQLLenguaje lenguaje;
	
	/**
	 * M&eacute;todo que realiza la creaci&oacute;n de un DTO, del negocio, a partir del resultado de alguna consulta.
	 * @param <code>rst</code> objeto de tipo <code>Resultset</code>, que contiene los resultados de la consulta.
	 * @return <code>Object</code> - representa al DTO creado.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static Object crearDtoST(ResultSet rst)
			throws SQLException, ExcepcionEnDAO {
		BsCodigosBanco bsCodigosBancoDto = new BsCodigosBanco();	
		bsCodigosBancoDto.setIdCodigoBanco(SQLUtils.getLongFromResultSet(rst, "C5001"));
		bsCodigosBancoDto.setCodigoSwift(rst.getString("C5002"));
		bsCodigosBancoDto.setCodigoUni(rst.getString("C5003"));
		bsCodigosBancoDto.setCodigoTransfer(SQLUtils.getLongFromResultSet(rst, "C5004"));
		bsCodigosBancoDto.setIdRedOperacion(SQLUtils.getLongFromResultSet(rst, "C5005"));
		bsCodigosBancoDto.setStatus(rst.getString("C5006"));
		bsCodigosBancoDto.setCodiBanco(SQLUtils.getLongFromResultSet(rst, "C5007"));
		return bsCodigosBancoDto;
	}
	

	/**
	 * M&eacute;todo que realiza la creaci&oacute;n de un DTO foraneo, del negocio, a partir del resultado de alguna consulta.
	 * @param <code>rst</code> objeto de tipo <code>Resultset</code>, que contiene los resultados de la consulta.
	 * @return <code>HthProcesoMonitoreo</code> - representa al DTO del negocio con el nuevo DTO foraneo asociado.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static BsCodigosBanco crearDataConValoresForaneosST(ResultSet rst)throws SQLException, ExcepcionEnDAO{
		BsCodigosBanco bsCodigosBancoDto = (BsCodigosBanco) crearDtoST(rst);
        BsRedOperacionBanco bsRedOperacionBancoRel  = (BsRedOperacionBanco) BsRedOperacionBancoDaoJdbcSql.crearDtoST(rst);
        bsCodigosBancoDto.setBsRedOperacionBanco(bsRedOperacionBancoRel);
        
        BsBancoAch bsBancoAchRel  = (BsBancoAch) BsBancoAchDaoJdbcSql.crearDtoST(rst);
        bsCodigosBancoDto.setBsBancoAch(bsBancoAchRel);
		return bsCodigosBancoDto;
	}
	
	/**
	 * M&eacute;todo que realiza la creaci&oacute;n de un DTO foraneo, del negocio, a partir de la b&uacute;squeda por <code>id</code>.
	 * @param <code>rst</code> objeto de tipo <code>Resultset</code>, que contiene los resultados de la consulta.
	 * @return <code>HthProcesoMonitoreo</code> - representa al DTO del negocio con el nuevo DTO foraneo asociado.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static BsCodigosBanco crearDataConValoresForaneosNoRelST(ResultSet rst, Connection conn)throws SQLException, ExcepcionEnDAO{	
		BsCodigosBanco bsCodigosBanco = (BsCodigosBanco) crearDtoST(rst);
		BsRedOperacionBanco bsRedOperacionBancoRellbsRed  = (BsRedOperacionBanco) SQLImplementacionOperacionesComunes.buscarPorIDST(
				bsCodigosBanco.getIdRedOperacion(), conn, 
				BsRedOperacionBancoDaoJdbcSql.SQL_OPERACIONES,
	       		false, 
	       		null);
        bsCodigosBanco.setBsRedOperacionBanco(bsRedOperacionBancoRellbsRed);

        BsBancoAch bsBancoAchRellbsRed  = (BsBancoAch) SQLImplementacionOperacionesComunes.buscarPorIDST(
				bsCodigosBanco.getCodiBanco(), conn, 
				BsBancoAchDaoJdbcSql.SQL_OPERACIONES,
	       		false, 
	       		null);
        bsCodigosBanco.setBsBancoAch(bsBancoAchRellbsRed);
		return bsCodigosBanco;
	}


	/**
	 * M&eacute;todo que coloca los valores provenientes del objeto Dto en el objeto PreparedStatement,
	 * el cual proviene de la llamada a la implementacion de {@link SQLOperaciones}, este m&eacute;todo
	 * no toma en cuenta el valor del <code>id</code>, puesto que se asume que es un valor secuencial
	 * autonumerico o serial propio de la base de datos.
	 * 
	 * @param <code>theDtoUntyped</code> objeto sin tipo que almacena al DTO del negocio.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaCrearST(Object theDtoUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {	
		BsCodigosBanco bsCodigosBancoDto = (BsCodigosBanco)theDtoUntyped;	
 	    Long codigoBanco=bsCodigosBancoDto.getCodiBanco();
 	    String codigoSwift=bsCodigosBancoDto.getCodigoSwift();
 	    String codigoUni=bsCodigosBancoDto.getBsBancoAch().getCodPayBank();
 	    Long codigoTransfer=bsCodigosBancoDto.getCodigoTransfer();
 	    Long idRedOperacion=bsCodigosBancoDto.getIdRedOperacion();
 	    String estado=bsCodigosBancoDto.getStatus();
		     
 	    SQLUtils.setIntoStatement( 1, codigoBanco, smt); 
 	    SQLUtils.setIntoStatement( 2, codigoSwift, smt);         		     
		SQLUtils.setIntoStatement( 3, codigoUni, smt);      		     
		SQLUtils.setIntoStatement( 4, codigoTransfer, smt);      		     
		SQLUtils.setIntoStatement( 5, idRedOperacion, smt);   		     
		SQLUtils.setIntoStatement( 6, estado, smt);       
    		
	}

	/**
	 * M&eacute;todo que coloca los valores provenientes del objeto Dto en el objeto PreparedStatement,
	 * el cual proviene de la llamada a la implementacion de {@link SQLOperaciones}, este m&eacute;todo
	 * toma en cuenta el <code>id</code>, lo que quiere decir que se usa al momento de  crear registros
	 * en donde el identificador no es un valor secuencial autonumerico o serial propio de la base de datos
	 *
	 * @param <code>theDtoUntyped</code> objeto sin tipo que almacena al DTO del negocio.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaCrearConIdST(Object theDtoUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {
		BsCodigosBanco bsCodigosBancoDto = (BsCodigosBanco)theDtoUntyped;
 	    Long codBanco=bsCodigosBancoDto.getIdCodigoBanco();
 	    Long codigoBanco=bsCodigosBancoDto.getCodiBanco();
 	    String codigoSwift=bsCodigosBancoDto.getCodigoSwift();
 	    String codigoUni=bsCodigosBancoDto.getBsBancoAch().getCodPayBank();
 	    Long codigoTransfer=bsCodigosBancoDto.getCodigoTransfer();
 	    Long idRedOperacion=bsCodigosBancoDto.getIdRedOperacion();
 	    String estado=bsCodigosBancoDto.getStatus();

 	    SQLUtils.setIntoStatement( 1, codigoBanco, smt); 
 	    SQLUtils.setIntoStatement( 2, codBanco, smt); 
 	    SQLUtils.setIntoStatement( 3, codigoSwift, smt);         		     
		SQLUtils.setIntoStatement( 4, codigoUni, smt);      		     
		SQLUtils.setIntoStatement( 5, codigoTransfer, smt);      		     
		SQLUtils.setIntoStatement( 6, idRedOperacion, smt);  		     
		SQLUtils.setIntoStatement( 7, estado, smt);    
	}

	/**
	 * M&eacute;todo que coloca el identificador recibido en el objeto PreparedStatement, para poder ser eliminado.
	 * @param <code>theIdUntyped</code> objeto sin tipo que identifica al <code>id</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaEliminarST(Object theIdUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {
 	    Long idCodBanco=(Long)theIdUntyped;
 	    String estado= "INACTIVO";   
 	    
		SQLUtils.setIntoStatement( 1, estado, smt);     		     
		SQLUtils.setIntoStatement( 2, idCodBanco, smt);   		 		 
		
	}

	/**
	 * M&eacute;todo que coloca el identificador recibido, en el objeto PreparedStatement para poder hacer la consulta de busqueda.
	 * @param <code>theIdUntyped</code> objeto sin tipo que identifica al <code>id</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaBuscarPorIdST(Object theIdUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {
 	    Long idCodBanco=(Long)theIdUntyped;
	     
 	    SQLUtils.setIntoStatement( 1, idCodBanco, smt); 
	}

	/**
	 * M&eacute;todo que coloca los valores provenientes del objeto Dto en el objeto PreparedStatement,
	 * el cual proviene de la llamada a la implementacion de {@link SQLOperaciones} para poder actualizar
     * un registro en espec&iacute;fico.
	 * 
	 * @param <code>theDtoUntyped</code> objeto sin tipo que almacena al DTO del negocio.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaActualizarST(Object theDtoUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {
		BsCodigosBanco bsCodigosBancoDto = (BsCodigosBanco)theDtoUntyped;	
 	    Long idCodBanco=bsCodigosBancoDto.getIdCodigoBanco();
 	    Long codigoBanco=bsCodigosBancoDto.getCodiBanco();
 	    String codigoSwift=bsCodigosBancoDto.getCodigoSwift();
 	    String codigoUni=bsCodigosBancoDto.getCodigoUni();
 	    Long codigoTransfer=bsCodigosBancoDto.getCodigoTransfer();
 	    Long idRedOperacion=bsCodigosBancoDto.getIdRedOperacion();
 	    String estado=bsCodigosBancoDto.getStatus();
 	    SQLUtils.setIntoStatement( 1, codigoBanco, smt);   
 	    SQLUtils.setIntoStatement( 2, codigoSwift, smt);         		     
		SQLUtils.setIntoStatement( 3, codigoUni, smt);      		     
		SQLUtils.setIntoStatement( 4, codigoTransfer, smt);      		     
		SQLUtils.setIntoStatement( 5, idRedOperacion, smt);     		     
		SQLUtils.setIntoStatement( 6, estado, smt);     		     
		SQLUtils.setIntoStatement( 7, idCodBanco, smt);         			
	}


	/**
	 * M&eacute;todo que ejecuta una petici&oacute;n de datos a trav&eacute;s de un objeto {@link PreparedStatement},
	 * y un objeto del tipo {@link PeticionDeDatos}, el cual en su implementaci&oacute;n define una serie de restricciones
	 * usadas para hacer la petici&oacute;n.
	 *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaPeticionDeDatosST(PeticionDeDatos peticion,
			PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
		SQLImplementacionOperacionesComunes.operacionesParaPeticionDeDatosST( peticion, smt, SQL_OPERACIONES, null);	
	}	
	
	/**
	 * M&eacute;todo que ejecuta una petici&oacute;n de datos a trav&eacute;s de un objeto {@link PreparedStatement},
     * y un objeto del tipo {@link PeticionDeDatos}, el cual en su implementaci&oacute;n define una serie de restricciones
     * usadas para hacer la petici&oacute;n.
	 *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaPeticionDeDatosFST(PeticionDeDatos peticion,
			PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
		SQLImplementacionOperacionesComunes.operacionesParaPeticionDeDatosST( peticion, smt, SQL_OPERACIONES, null);	
	}	

	/**
	 * M&eacute;todo que ejecuta una petici&oacute;n de conteo a trav&eacute;s de un objeto {@link PreparedStatement},
     * y un objeto del tipo {@link PeticionDeDatos}, el cual en su implementaci&oacute;n define una serie de restricciones
     * usadas para hacer la petici&oacute;n.
	 *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaConteoST(PeticionDeDatos peticion,
			PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
		SQLImplementacionOperacionesComunes.operacionesParaPeticionDeDatosST( peticion, smt, SQL_OPERACIONES, null);
	}	

	/**
	 * M&eacute;todo que agrega al atributo {@link #SELECT_HEADER_SQL} las condiciones de b&uacute;squeda
     * que vienen en el objeto del tipo {@link PeticionDeDatos}
     *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>, que contiene las condiciones de la consulta.
	 * @return <code>String</code> - la consulta condicionada como cadena de caracteres.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static String getSqlParaPeticionDeDatosST(PeticionDeDatos peticion)
			throws SQLException, ExcepcionEnDAO {
		return SELECT_HEADER_SQL + SQLImplementacionOperacionesComunes.crearCondicionesSQLST( peticion, SQL_OPERACIONES, SQLImplementacionOperacionesComunes.COND_ALIAS_ORDEN_WHERE);
	}
	
	
	/**
	 * M&eacute;todo que agrega al atributo {@link #SELECT_HEADER_SQL_F} las condiciones de b&uacute;squeda
     * que vienen en el objeto del tipo {@link PeticionDeDatos}
     *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>, que contiene las condiciones de la consulta.
	 * @return <code>String</code> - la consulta condicionada como cadena de caracteres.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static String getSqlParaPeticionDeDatosFST(PeticionDeDatos peticion)
			throws SQLException, ExcepcionEnDAO {
		return SELECT_HEADER_SQL_F + SQLImplementacionOperacionesComunes.crearCondicionesSQLST( peticion, SQL_OPERACIONES, SQLImplementacionOperacionesComunes.COND_ALIAS_ORDEN_AND);
	}	
	
	

	/**
	 * M&eacute;todo que agrega al atributo {@link #COUNT_HEADER_SQL} las condiciones de conteo
	 * que vienen en el objeto de tipo {@link PeticionDeDatos}.
	 *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>, que contiene las condiciones de la consulta.
	 * @return <code>String</code> - la consulta condicionada como cadena de caracteres.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static String getSqlDeConteoST(PeticionDeDatos peticion)
			throws SQLException, ExcepcionEnDAO {
		return COUNT_HEADER_SQL + SQLImplementacionOperacionesComunes.crearCondicionesSQLST( peticion, SQL_OPERACIONES, SQLImplementacionOperacionesComunes.COND_SQL_WHERE);
	}
	
	/**
	 * M&eacute;todo que se utiliza para actualizar un registro a nivel de tabla, recibe como par&aacute;metro un DTO del negocio
	 * que contiene la informaci&oacute;n a actualizar, e invoca la funci&oacute;n <code>actualizar</code> de la clase de la que hereda.
	 *
	 * @param <code>BsBancoAch</code> objeto del tipo <code>BsBancoAch</code>.
	 * @throws ExcepcionEnDAO
	 * @see {@link SQLImplementacionBasica}
	 */
	public void actualizar(BsCodigosBanco bsCodigosBanco) throws ExcepcionEnDAO {
	    actualizar(bsCodigosBanco, AUTO_COMMIT);
	
	}
	
	/**
	 * M&eacute;todo utilizado para realizar una b&uacute;squeda de un registro espec&iacute;fico, utilizando un identificador.
	 *
	 * @param <code>BsBancoAchID</code> objeto del tipo <code>BsBancoAchID</code>.
	 * @return <code>BsBancoAch</code> - un objeto DTO.
	 * @throws ExcepcionEnDAO
	 * @see {@link SQLImplementacionBasica}
	 */
	public BsCodigosBanco buscarPorID(BsCodigosBanco bsCodigosBanco)
			throws ExcepcionEnDAO {
		return (BsCodigosBanco)buscarPorID(bsCodigosBanco, AUTO_COMMIT);				 
	}

	/**
	 * M&eacute;todo utilizado para la creaci&oacute;n de un registro, a nivel de tabla, a partir de la informaci&oacute;n
	 * recibida en un DTO del objeto de Negocio, e invoca la funci&oacute;n <code>crear</code> de la clase de la que hereda.
	 * 
	 * @param <code>BsBancoAch</code> objeto del tipo <code>BsBancoAch</code>.
	 * @throws ExcepcionEnDAO
	 * @see {@link SQLImplementacionBasica}
	 */
	public void crear(BsCodigosBanco bsCodigosBanco) throws ExcepcionEnDAO {
	    crear(bsCodigosBanco, AUTO_COMMIT);		
	}

	/**
	 * M&eacute;todo utilizado para la eliminaci&oacute;n de un registro, a nivel de tabla,
	 * que se busca a trav&eacute;s del identificador recibido. Se invoca la funci&oacute;n <code>eliminar</code> de la clase de la que hereda.
	 *
	 * @param <code>BsBancoAchID</code> objeto del tipo <code>BsBancoAchID</code>.
	 * @throws ExcepcionEnDAO
	 * @see {@link SQLImplementacionBasica}
	 */
	public void eliminar(BsCodigosBanco bsCodigosBanco) throws ExcepcionEnDAO {
	    eliminar(bsCodigosBanco, AUTO_COMMIT);		
	}

	/**
	 * M&eacute;todo utilizado para devolver un &uacute;nico resultado de la petici&oacute;n recibida,
	 * la cual se realiza a trav&eacuate;s del objeto de tipo {@link PeticionDeDatos}.
	 * 
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>, que contiene las condiciones de la consulta.
	 * @return <code>BsBancoAch</code> - un objeto DTO.
	 * @throws ExcepcionEnDAO
	 * @throws ExcepcionPorDatoNoEncontrado
	 * @see {@link SQLImplementacionBasica}
	 */
	public BsCodigosBanco resultadoUnico(PeticionDeDatos peticion)
			throws ExcepcionEnDAO, ExcepcionPorDatoNoEncontrado {
		return (BsCodigosBanco)resultadoUnico(peticion, AUTO_COMMIT);
	}
	
	/**
	 * Atributo del tipo {@link SQLOperaciones}, almacena la operaciones disponibles para esta implementaci&oacute;n.<br/>
	 * Algunas de las llamadas a los m&eacute;todos se realiza a trav&eacute;s de la invocaci&oacute;n de estas implementaciones.
	 */
    public static final SQLOperaciones	SQL_OPERACIONES =  new SQLOperaciones() {
		
		public int getManejoForaneosPorDefecto() {
			return Dao.FORANEOS_JOIN;
		}
			
			
		public void operacionesParaPeticionDeDatosF(PeticionDeDatos request,
				PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
			BsCodigosBancoDaoJdbcSql.operacionesParaPeticionDeDatosFST(request, smt);
		}
		
		public void operacionesParaPeticionDeDatos(PeticionDeDatos request,
				PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
			BsCodigosBancoDaoJdbcSql.operacionesParaPeticionDeDatosST(request, smt);
		}
		
		public void operacionesParaEliminar(Object theId, PreparedStatement smt)
				throws SQLException, ExcepcionEnDAO {
			BsCodigosBancoDaoJdbcSql.operacionesParaEliminarST(theId, smt);
		}
		
		public void operacionesParaCrear(Object theDto, PreparedStatement smt)
				throws SQLException, ExcepcionEnDAO {
			BsCodigosBancoDaoJdbcSql.operacionesParaCrearST(theDto, smt);
		}
		
		public void operacionesParaCrearConId(Object theDto, PreparedStatement smt) 
		        throws SQLException, ExcepcionEnDAO {
		    BsCodigosBancoDaoJdbcSql.operacionesParaCrearConIdST(theDto, smt);
        }
		
		public void operacionesParaConteo(PeticionDeDatos request,
				PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
			BsCodigosBancoDaoJdbcSql.operacionesParaConteoST(request, smt);				
		}
		
		public void operacionesParaBuscarPorId(Object theId, PreparedStatement smt)
				throws SQLException, ExcepcionEnDAO {
			BsCodigosBancoDaoJdbcSql.operacionesParaBuscarPorIdST(theId, smt);
		}
		
		public void operacionesParaActualizar(Object theDto, PreparedStatement smt)
				throws SQLException, ExcepcionEnDAO {
			BsCodigosBancoDaoJdbcSql.operacionesParaActualizarST(theDto, smt);			
		}
		
		public String getSqlParaPeticionDeDatosF(PeticionDeDatos request)
				throws SQLException, ExcepcionEnDAO {
			return BsCodigosBancoDaoJdbcSql.getSqlParaPeticionDeDatosFST(request);	
		}
		
		public String getSqlParaPeticionDeDatos(PeticionDeDatos request)
				throws SQLException, ExcepcionEnDAO {
			return BsCodigosBancoDaoJdbcSql.getSqlParaPeticionDeDatosST(request);
		}
		
		public String getSqlDeConteo(PeticionDeDatos request) throws SQLException,
				ExcepcionEnDAO {
			return BsCodigosBancoDaoJdbcSql.getSqlDeConteoST(request);
		}
		
		public SQLEntidadMeta getSQLEntidadMeta() throws ExcepcionEnDAO {
			return BsCodigosBancoDaoJdbcSql.SQL_ENTIDAD_META;
		}
		
		public String getEliminarSql() {
			return  BsCodigosBancoDaoJdbcSql.DELETE_SQL;
		}
		
		public String getCrearSql() {
			return  BsCodigosBancoDaoJdbcSql.CREATE_SQL;
		}
		
		public String getCrearSqlConId(){
		    return  BsCodigosBancoDaoJdbcSql.CREATE_SQL_CON_ID;
		}			
		
		public String getBuscarPorIdSqlF() {
			return BsCodigosBancoDaoJdbcSql.FIND_BY_ID_SQL_F;
		}
		
		public String getBuscarPorIdSql() {				
			return BsCodigosBancoDaoJdbcSql.FIND_BY_ID_SQL;
		}
		
		public String getActualizarSql() {				
			return BsCodigosBancoDaoJdbcSql.UPDATE_SQL;
		}
		
		public Object crearDto(ResultSet rst) throws SQLException, ExcepcionEnDAO {
			return BsCodigosBancoDaoJdbcSql.crearDtoST(rst);
		}
        
        public Object crearDataConValoresForaneos(ResultSet rst)
				throws SQLException, ExcepcionEnDAO {
			return BsCodigosBancoDaoJdbcSql.crearDataConValoresForaneosST(rst);
		}
		
        public Object crearDataConValoresForaneosNoRel(ResultSet rst, Connection conn)
				throws SQLException, ExcepcionEnDAO {
			return BsCodigosBancoDaoJdbcSql.crearDataConValoresForaneosNoRelST(rst, conn);
		}
		
		
        public boolean isForaneosDisponible(){
			return true;
        
        }
        
        public SQLLenguaje getSQLLenguaje() {
            return BsCodigosBancoDaoJdbcSql.getLenguaje();
        }
        
        public void setIdEnDto(Object theDto, Object dtoId) {
            setIdEnDtoST(theDto, dtoId);
        }
	};
	
	
	
	
	
	/**
	 * Devuelve el atributo {@link #SQL_OPERACIONES}.
	 * @return <code>SQLOperaciones</code> - objeto del tipo {@link SQLOperaciones}
	 */
    public SQLOperaciones getSqlOperaciones() {
       return SQL_OPERACIONES;
	}
	
	/**
	 * Devuelve el atributo {@link #lenguaje}
	 * @return <code>SQLLenguaje</code> - objeto del tipo {@link SQLLenguaje}
	 */	
	public static SQLLenguaje getLenguaje() {
		return lenguaje;
	}
	
	/**
	 * M&eacute;todo que permite obtener a trav&eacute;s de un Servicio la implementaci&oacute;n del objeto de Lenguaje,
	 * y coloca las reglas de negocio almacenadas en el atributo {@link #SQL_ENTIDAD_META}.
	 * @param <code>localizadorLenguaje</code> objeto del tipo {@link LocalizadorDeServicio} que posee informaci&oacute;n para trabajar con la base de datos.
	 */
	public static void setLocalizadorLenguaje(LocalizadorDeServicio localizadorLenguaje) {
		lenguaje = (SQLLenguaje) localizadorLenguaje.getServicio();
		lenguaje.setSqlEntidadMeta(SQL_ENTIDAD_META);
	}

	/**
	 * M&eacute;todo que agrega un identificador a un DTO del tipo <code>BsCodigosBanco</code>.
	 * @param <code>theDtoUntyped</code> objeto gen&eacute;rico, que representa a un <code>DTO</code>.
	 * @param <code>dtoIdUntyped</code> objeto gen&eacute;rico, que representa al identificador.
	 */
	public static void setIdEnDtoST(Object theDtoUntyped, Object dtoIdUntyped){
		BsCodigosBanco dto = (BsCodigosBanco)theDtoUntyped;
        Long dtoId = (Long) dtoIdUntyped;;
        dto.setIdCodigoBanco(dtoId);	
	}
}
