package com.ofbox.davivienda.h2h.orm.jdbc.sql.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.sql.Connection;
import java.util.Collection;

import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Dao;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.ExcepcionEnDAO;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.ExcepcionPorDatoNoEncontrado;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.PeticionDeDatos;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Transaccion;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.foraneos.DataConValoresForaneos;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLColumnaMeta;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLEntidadMeta;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLImplementacionBasica;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLImplementacionOperacionesComunes;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLLenguaje;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLOperaciones;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLUtils;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.TransaccionJdbc;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dependencias.LocalizadorDeServicio;


import com.ofbox.davivienda.h2h.abstraccion.dto.BsDetallepe;
import com.ofbox.davivienda.h2h.abstraccion.dto.BsDetallepeID;
import com.ofbox.davivienda.h2h.abstraccion.dao.BsDetallepeDao;

import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.impl.DataConValoresForaneosImpl;   
import com.ofbox.davivienda.h2h.abstraccion.dto.BsLotepe;


import com.ofbox.davivienda.h2h.orm.jdbc.sql.impl.BsLotepeDaoJdbcSql;
import com.ofbox.davivienda.h2h.abstraccion.dao.BsLotepeDao;

/**
 * Clase que hereda comportamientos de {@link SQLImplementacionBasica} e implementa el comportamiento definido en la capa DAO.<br/>
 * Esta clase es una implementaci&oacute;n intermedia para el DAO de <b>BS Detalle PE</b>
 * asociado a la tabla <b>BS_DetallePE</b>, en esta clase se construyen todos los sql para las operaciones de listado,
 * creaci&oacute;n, consulta, actualizaci&oacute;n y elminaci&oacute;n.<br/>
 * @author Systems Out of the Box
 * @version f3.1
 *
 */
public class BsDetallepeDaoJdbcSql extends SQLImplementacionBasica implements BsDetallepeDao{

	/**
	 * Atributo del tipo {@link SQLEntidadMeta}, que almacena la descripci&oacute;n del negocio como una entidad.<br/>
	 * Entre sus caracter&iacute;sticas tiene el nombre de la tabla, nombre de la secuencia, posici&oacute;n de las columnas,
	 * nombre de las columnas y tipos de datos para la columna.<br/>
	 * El arreglo que recibe son objetos de tipo {@link SQLColumnaMeta}, y definen informaci&oacute;n para obtener las columnas.
	 */
	public static final SQLEntidadMeta SQL_ENTIDAD_META = new SQLEntidadMeta("BS_DetallePE", new SQLColumnaMeta[]{
        new SQLColumnaMeta(6, "BS_DetallePE", "E6", 6001, "Instalacion", "C6001", Types.BIGINT ,true)
     ,  new SQLColumnaMeta(6, "BS_DetallePE", "E6", 6002, "Lote", "C6002", Types.BIGINT ,true)
     ,  new SQLColumnaMeta(6, "BS_DetallePE", "E6", 6003, "Operacion", "C6003", Types.BIGINT ,true)
     ,  new SQLColumnaMeta(6, "BS_DetallePE", "E6", 6004, "Cuenta", "C6004", Types.VARCHAR )
     ,  new SQLColumnaMeta(6, "BS_DetallePE", "E6", 6005, "TipoOperacion", "C6005", Types.BIGINT )
     ,  new SQLColumnaMeta(6, "BS_DetallePE", "E6", 6006, "Monto", "C6006", Types.NUMERIC )
     ,  new SQLColumnaMeta(6, "BS_DetallePE", "E6", 6007, "MontoAplicado", "C6007", Types.NUMERIC )
     ,  new SQLColumnaMeta(6, "BS_DetallePE", "E6", 6008, "Adenda", "C6008", Types.VARCHAR )
     ,  new SQLColumnaMeta(6, "BS_DetallePE", "E6", 6009, "FechaEstatus", "C6009", Types.TIMESTAMP )
     ,  new SQLColumnaMeta(6, "BS_DetallePE", "E6", 6010, "Estatus", "C6010", Types.BIGINT )
     ,  new SQLColumnaMeta(6, "BS_DetallePE", "E6", 6011, "IDPago", "C6011", Types.VARCHAR )
     ,  new SQLColumnaMeta(6, "BS_DetallePE", "E6", 6012, "AutorizacionHost", "C6012", Types.BIGINT )
     ,  new SQLColumnaMeta(6, "BS_DetallePE", "E6", 6013, "EnviarAHost", "C6013", Types.BIGINT )
     ,  new SQLColumnaMeta(6, "BS_DetallePE", "E6", 6014, "Contrasena", "C6014", Types.VARCHAR )
     ,  new SQLColumnaMeta(6, "BS_DetallePE", "E6", 6015, "NombreBenef", "C6015", Types.VARCHAR )
     ,  new SQLColumnaMeta(6, "BS_DetallePE", "E6", 6016, "DireccionBenef", "C6016", Types.VARCHAR )
     ,  new SQLColumnaMeta(6, "BS_DetallePE", "E6", 6017, "Email", "C6017", Types.VARCHAR )
     ,  new SQLColumnaMeta(6, "BS_DetallePE", "E6", 6018, "ComisionAplicada", "C6018", Types.NUMERIC )
     ,  new SQLColumnaMeta(6, "BS_DetallePE", "E6", 6019, "IVAAplicado", "C6019", Types.NUMERIC )
     ,  new SQLColumnaMeta(6, "BS_DetallePE", "E6", 6020, "NombreCuenta", "C6020", Types.VARCHAR )
     ,  new SQLColumnaMeta(6, "BS_DetallePE", "E6", 6021, "NIU", "C6021", Types.BIGINT )
     ,  new SQLColumnaMeta(6, "BS_DetallePE", "E6", 6022, "Autorizador", "C6022", Types.VARCHAR )
     ,  new SQLColumnaMeta(6, "BS_DetallePE", "E6", 6023, "Sublote", "C6023", Types.BIGINT )
     ,  new SQLColumnaMeta(6, "BS_DetallePE", "E6", 6024, "CuentaDebito", "C6024", Types.VARCHAR )
     ,  new SQLColumnaMeta(6, "BS_DetallePE", "E6", 6025, "NombreDeCuenta", "C6025", Types.VARCHAR )
     ,  new SQLColumnaMeta(6, "BS_DetallePE", "E6", 6026, "MontoImpuesto", "C6026", Types.NUMERIC )
     ,  new SQLColumnaMeta(6, "BS_DetallePE", "E6", 6027, "CodigoORefInt", "C6027", Types.VARCHAR )
     ,  new SQLColumnaMeta(10, "BS_LotePE", "E10", 10001, "Instalacion", "C10001", Types.BIGINT)
     ,  new SQLColumnaMeta(10, "BS_LotePE", "E10", 10002, "Lote", "C10002", Types.BIGINT)
     ,  new SQLColumnaMeta(10, "BS_LotePE", "E10", 10030, "TipoConvenio", "C10030", Types.BIGINT)
     ,  new SQLColumnaMeta(10, "BS_LotePE", "E10", 10003, "Convenio", "C10003", Types.BIGINT)
     ,  new SQLColumnaMeta(10, "BS_LotePE", "E10", 10004, "EmpresaODepto", "C10004", Types.BIGINT)
     ,  new SQLColumnaMeta(10, "BS_LotePE", "E10", 10005, "TipoLote", "C10005", Types.BIGINT)
     ,  new SQLColumnaMeta(10, "BS_LotePE", "E10", 10006, "MontoTotal", "C10006", Types.NUMERIC)
     ,  new SQLColumnaMeta(10, "BS_LotePE", "E10", 10007, "NumOperaciones", "C10007", Types.BIGINT)
     ,  new SQLColumnaMeta(10, "BS_LotePE", "E10", 10008, "NombreLote", "C10008", Types.VARCHAR)
     ,  new SQLColumnaMeta(10, "BS_LotePE", "E10", 10009, "FechaEnviado", "C10009", Types.TIMESTAMP)
     ,  new SQLColumnaMeta(10, "BS_LotePE", "E10", 10010, "FechaRecibido", "C10010", Types.TIMESTAMP)
     ,  new SQLColumnaMeta(10, "BS_LotePE", "E10", 10011, "FechaAplicacion", "C10011", Types.TIMESTAMP)
     ,  new SQLColumnaMeta(10, "BS_LotePE", "E10", 10012, "FechaEstatus", "C10012", Types.TIMESTAMP)
     ,  new SQLColumnaMeta(10, "BS_LotePE", "E10", 10013, "Estatus", "C10013", Types.BIGINT)
     ,  new SQLColumnaMeta(10, "BS_LotePE", "E10", 10014, "NumeroReintento", "C10014", Types.BIGINT)
     ,  new SQLColumnaMeta(10, "BS_LotePE", "E10", 10015, "UsuarioIngreso", "C10015", Types.VARCHAR)
     ,  new SQLColumnaMeta(10, "BS_LotePE", "E10", 10016, "Autorizaciones", "C10016", Types.BIGINT)
     ,  new SQLColumnaMeta(10, "BS_LotePE", "E10", 10017, "AplicacionDebitoHost", "C10017", Types.BIGINT)
     ,  new SQLColumnaMeta(10, "BS_LotePE", "E10", 10018, "Sys_MarcaTiempo", "C10018", Types.TIMESTAMP)
     ,  new SQLColumnaMeta(10, "BS_LotePE", "E10", 10019, "Sys_ProcessId", "C10019", Types.BIGINT)
     ,  new SQLColumnaMeta(10, "BS_LotePE", "E10", 10020, "Sys_LowDateTime", "C10020", Types.BIGINT)
     ,  new SQLColumnaMeta(10, "BS_LotePE", "E10", 10021, "Sys_HighDateTime", "C10021", Types.BIGINT)
     ,  new SQLColumnaMeta(10, "BS_LotePE", "E10", 10022, "Ponderacion", "C10022", Types.BIGINT)
     ,  new SQLColumnaMeta(10, "BS_LotePE", "E10", 10023, "Firmas", "C10023", Types.BIGINT)
     ,  new SQLColumnaMeta(10, "BS_LotePE", "E10", 10024, "FechaEnvioHost", "C10024", Types.TIMESTAMP)
     ,  new SQLColumnaMeta(10, "BS_LotePE", "E10", 10025, "ListoParaHost", "C10025", Types.TIMESTAMP)
     ,  new SQLColumnaMeta(10, "BS_LotePE", "E10", 10026, "FechaHoraProcesado", "C10026", Types.TIMESTAMP)
     ,  new SQLColumnaMeta(10, "BS_LotePE", "E10", 10027, "Token", "C10027", Types.BIGINT)
     ,  new SQLColumnaMeta(10, "BS_LotePE", "E10", 10028, "ComentarioRechazo", "C10028", Types.VARCHAR)
     ,  new SQLColumnaMeta(10, "BS_LotePE", "E10", 10029, "MontoImpuesto", "C10029", Types.NUMERIC)
	});
	
    
	public BsLotepe getBsLotepe(BsDetallepe bsDetallepe)throws ExcepcionEnDAO{
		return getBsLotepe(bsDetallepe, null);
	}

    public BsLotepe getBsLotepe(BsDetallepe bsDetallepe,Transaccion tx)throws ExcepcionEnDAO{
         	     
	     BsLotepe bsLotepeRel = bsDetallepe.getBsLotepe();
	     if(bsLotepeRel==null){
                PeticionDeDatos peticionDeDatosParaBsLotepe = SQLImplementacionBasica.crearPeticionDeDatosST();
                peticionDeDatosParaBsLotepe.agregarRestriccion(PeticionDeDatos.IGUAL,
	     	                              BsLotepeDao.INSTALACION, 
	     	                              bsDetallepe.getInstalacion()
	     	                              );
	            TransaccionJdbc tjdbc = (TransaccionJdbc)tx;
	     		bsLotepeRel = (BsLotepe)SQLImplementacionOperacionesComunes.resultadoUnicoST(
							            peticionDeDatosParaBsLotepe, 
										tjdbc!=null? tjdbc.getConn(): getConnection(), 
										BsLotepeDaoJdbcSql.SQL_OPERACIONES, 
										tjdbc==null, 
										getLog());
				bsDetallepe.setBsLotepe(bsLotepeRel);										
		}
										
		return 	bsLotepeRel;									
    }
    

	

	/**
	 * Atributo que almacena el <code>String</code> para obtener el listado de registros de la tabla principal
	 * junto con otras tablas foraneas, realiza un JOIN.
	 */
	public static final String SELECT_HEADER_SQL_F =  
		"SELECT E6.Instalacion AS C6001 , E6.Lote AS C6002 , E6.Operacion AS C6003 , E6.Cuenta AS C6004 , " +
		"       E6.TipoOperacion AS C6005 , E6.Monto AS C6006 , E6.MontoAplicado AS C6007 , E6.Adenda AS C6008 , " +
		"       E6.FechaEstatus AS C6009 , E6.Estatus AS C6010 , E6.IDPago AS C6011 , E6.AutorizacionHost AS C6012 , " +
		"       E6.EnviarAHost AS C6013 , E6.Contrasena AS C6014 , E6.NombreBenef AS C6015 , E6.DireccionBenef AS C6016 , " +
		"       E6.Email AS C6017 , E6.ComisionAplicada AS C6018 , E6.IVAAplicado AS C6019 , E6.NombreCuenta AS C6020 , " +
		"       E6.NIU AS C6021 , E6.Autorizador AS C6022 , E6.Sublote AS C6023 , E6.CuentaDebito AS C6024 , " +
		"       E6.NombreDeCuenta AS C6025 , E6.MontoImpuesto AS C6026 , E6.CodigoORefInt AS C6027 , " +
		"       E10.Instalacion AS C10001 , E10.Lote AS C10002 , E10.TipoConvenio AS C10030 , E10.Convenio AS C10003 , " +
		"       E10.EmpresaODepto AS C10004 , E10.TipoLote AS C10005 , E10.MontoTotal AS C10006 , E10.NumOperaciones AS C10007 , " +
		"       E10.NombreLote AS C10008 , E10.FechaEnviado AS C10009 , E10.FechaRecibido AS C10010 , E10.FechaAplicacion AS C10011 , " +
		"       E10.FechaEstatus AS C10012 , E10.Estatus AS C10013 , E10.NumeroReintento AS C10014 , E10.UsuarioIngreso AS C10015 , " +
		"       E10.Autorizaciones AS C10016 , E10.AplicacionDebitoHost AS C10017 , E10.Sys_MarcaTiempo AS C10018 , E10.Sys_ProcessId AS C10019 , " +
		"       E10.Sys_LowDateTime AS C10020 , E10.Sys_HighDateTime AS C10021 , E10.Ponderacion AS C10022 , E10.Firmas AS C10023 , " +
		"       E10.FechaEnvioHost AS C10024 , E10.ListoParaHost AS C10025 , E10.FechaHoraProcesado AS C10026 , E10.Token AS C10027 , " +
		"       E10.ComentarioRechazo AS C10028 , E10.MontoImpuesto AS C10029" +
		"  FROM BS_DetallePE E6 , BS_LotePE E10" +
		" WHERE E10.Instalacion = E6.Instalacion";
	/**
	 * Atributo que almacena el <code>String</code> para obtener el listado de registros de la tabla principal
     * junto con otras tablas foraneas, realiza un JOIN utilizando la <code>primary key</code> de la tabla principal.
	 */
	public static final String FIND_BY_ID_SQL_F = 
		"SELECT E6.Instalacion AS C6001 , E6.Lote AS C6002 , E6.Operacion AS C6003 , E6.Cuenta AS C6004 , " +
		"       E6.TipoOperacion AS C6005 , E6.Monto AS C6006 , E6.MontoAplicado AS C6007 , E6.Adenda AS C6008 , " +
		"       E6.FechaEstatus AS C6009 , E6.Estatus AS C6010 , E6.IDPago AS C6011 , E6.AutorizacionHost AS C6012 , " +
		"       E6.EnviarAHost AS C6013 , E6.Contrasena AS C6014 , E6.NombreBenef AS C6015 , E6.DireccionBenef AS C6016 , " +
		"       E6.Email AS C6017 , E6.ComisionAplicada AS C6018 , E6.IVAAplicado AS C6019 , E6.NombreCuenta AS C6020 , " +
		"       E6.NIU AS C6021 , E6.Autorizador AS C6022 , E6.Sublote AS C6023 , E6.CuentaDebito AS C6024 , " +
		"       E6.NombreDeCuenta AS C6025 , E6.MontoImpuesto AS C6026 , E6.CodigoORefInt AS C6027 , " +
		"       E10.Instalacion AS C10001 , E10.Lote AS C10002 , E10.TipoConvenio AS C10030 , E10.Convenio AS C10003 , " +
		"       E10.EmpresaODepto AS C10004 , E10.TipoLote AS C10005 , E10.MontoTotal AS C10006 , E10.NumOperaciones AS C10007 , " +
		"       E10.NombreLote AS C10008 , E10.FechaEnviado AS C10009 , E10.FechaRecibido AS C10010 , E10.FechaAplicacion AS C10011 , " +
		"       E10.FechaEstatus AS C10012 , E10.Estatus AS C10013 , E10.NumeroReintento AS C10014 , E10.UsuarioIngreso AS C10015 , " +
		"       E10.Autorizaciones AS C10016 , E10.AplicacionDebitoHost AS C10017 , E10.Sys_MarcaTiempo AS C10018 , E10.Sys_ProcessId AS C10019 , " +
		"       E10.Sys_LowDateTime AS C10020 , E10.Sys_HighDateTime AS C10021 , E10.Ponderacion AS C10022 , E10.Firmas AS C10023 , " +
		"       E10.FechaEnvioHost AS C10024 , E10.ListoParaHost AS C10025 , E10.FechaHoraProcesado AS C10026 , E10.Token AS C10027 , " +
		"       E10.ComentarioRechazo AS C10028 , E10.MontoImpuesto AS C10029" +
		"  FROM BS_DetallePE E6 , BS_LotePE E10" +
		" WHERE E10.Instalacion = E6.Instalacion" +
		"   AND E6.Instalacion= ? " +
		"   AND E6.Lote= ? " +
		"   AND E6.Operacion= ? ";
	

	/**
	 * Atributo que almacena el <code>String</code> para obtener el listado de registros del negocio.
	 */
	public static final String SELECT_HEADER_SQL =  
		"SELECT E6.Instalacion AS C6001 , E6.Lote AS C6002 , E6.Operacion AS C6003 , E6.Cuenta AS C6004 , " +
		"       E6.TipoOperacion AS C6005 , E6.Monto AS C6006 , E6.MontoAplicado AS C6007 , E6.Adenda AS C6008 , " +
		"       E6.FechaEstatus AS C6009 , E6.Estatus AS C6010 , E6.IDPago AS C6011 , E6.AutorizacionHost AS C6012 , " +
		"       E6.EnviarAHost AS C6013 , E6.Contrasena AS C6014 , E6.NombreBenef AS C6015 , E6.DireccionBenef AS C6016 , " +
		"       E6.Email AS C6017 , E6.ComisionAplicada AS C6018 , E6.IVAAplicado AS C6019 , E6.NombreCuenta AS C6020 , " +
		"       E6.NIU AS C6021 , E6.Autorizador AS C6022 , E6.Sublote AS C6023 , E6.CuentaDebito AS C6024 , " +
		"       E6.NombreDeCuenta AS C6025 , E6.MontoImpuesto AS C6026 , E6.CodigoORefInt AS C6027" +
		"  FROM BS_DetallePE E6";
	/**
	 * Atributo que almacena el <code>String</code> para obtener el n&uacute;mero de registros presentes en el negocio.
	 */
	public static final String COUNT_HEADER_SQL =   "SELECT COUNT(*) FROM BS_DetallePE";    												
	/**
	 * Atributo que almacena el <code>String</code> para la inserci&oacute;n, sin id, de un nuevo registro en la tabla.
	 */
	public static final String CREATE_SQL = 
		"INSERT INTO BS_DetallePE (" +
		"Cuenta,TipoOperacion,Monto,MontoAplicado,Adenda" +
		",FechaEstatus,Estatus,IDPago,AutorizacionHost" +
		",EnviarAHost,Contrasena,NombreBenef,DireccionBenef" +
		",Email,ComisionAplicada,IVAAplicado,NombreCuenta" +
		",NIU,Autorizador,Sublote,CuentaDebito" +
		",NombreDeCuenta,MontoImpuesto,CodigoORefInt" +
		") VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	/**
	 * Atributo que almacena el <code>String</code> para insertar, con id, un registro en la table.
	 */
	public static final String CREATE_SQL_CON_ID = 
		"INSERT INTO BS_DetallePE (" +
		"Instalacion,Lote,Operacion,Cuenta,TipoOperacion" +
		",Monto,MontoAplicado,Adenda,FechaEstatus" +
		",Estatus,IDPago,AutorizacionHost,EnviarAHost" +
		",Contrasena,NombreBenef,DireccionBenef,Email" +
		",ComisionAplicada,IVAAplicado,NombreCuenta,NIU" +
		",Autorizador,Sublote,CuentaDebito,NombreDeCuenta" +
		",MontoImpuesto,CodigoORefInt" +
		") VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	/**
	 * Atributo que almacena el <code>String</code> para la atualizaci&oacute; de un registro en la tabla.
	 */
	public static final String UPDATE_SQL = 
		"UPDATE BS_DetallePE" +
		" SET Cuenta= ?  , TipoOperacion= ?  , Monto= ?  , MontoAplicado= ?  , " +
		"Adenda= ?  , FechaEstatus= ?  , Estatus= ?  , IDPago= ?  , " +
		"AutorizacionHost= ?  , EnviarAHost= ?  , Contrasena= ?  , NombreBenef= ?  , " +
		"DireccionBenef= ?  , Email= ?  , ComisionAplicada= ?  , IVAAplicado= ?  , " +
		"NombreCuenta= ?  , NIU= ?  , Autorizador= ?  , Sublote= ?  , " +
		"CuentaDebito= ?  , NombreDeCuenta= ?  , MontoImpuesto= ?  , CodigoORefInt= ? " +
		" WHERE Instalacion= ?    AND Lote= ?    AND Operacion= ? ";
	/**
	 * Atributo que almacena el <code>String</code> para la eliminaci&oacute; de un registro en la tabla.
	 */
	public static final String DELETE_SQL =  
		"DELETE FROM BS_DetallePE" +
		" WHERE Instalacion= ?    AND Lote= ?    AND Operacion= ? ";
	/**
	 * Atributo que almacena el <code>String</code> para la b&uacute;squeda por id de un registro de la tabla.
	 */
	public static final String FIND_BY_ID_SQL = 
		"SELECT E6.Instalacion AS C6001 , E6.Lote AS C6002 , E6.Operacion AS C6003 , E6.Cuenta AS C6004 , " +
		"       E6.TipoOperacion AS C6005 , E6.Monto AS C6006 , E6.MontoAplicado AS C6007 , E6.Adenda AS C6008 , " +
		"       E6.FechaEstatus AS C6009 , E6.Estatus AS C6010 , E6.IDPago AS C6011 , E6.AutorizacionHost AS C6012 , " +
		"       E6.EnviarAHost AS C6013 , E6.Contrasena AS C6014 , E6.NombreBenef AS C6015 , E6.DireccionBenef AS C6016 , " +
		"       E6.Email AS C6017 , E6.ComisionAplicada AS C6018 , E6.IVAAplicado AS C6019 , E6.NombreCuenta AS C6020 , " +
		"       E6.NIU AS C6021 , E6.Autorizador AS C6022 , E6.Sublote AS C6023 , E6.CuentaDebito AS C6024 , " +
		"       E6.NombreDeCuenta AS C6025 , E6.MontoImpuesto AS C6026 , E6.CodigoORefInt AS C6027" +
		"  FROM BS_DetallePE E6" +
		" WHERE E6.Instalacion= ? " +
		"   AND E6.Lote= ? " +
		"   AND E6.Operacion= ? ";
	/**
	 * Atributo que almacena el <code>String</code> para la creacion de la tabla en la base de datos.
	 */
	public static final String CREATE_TABLE_DDL = 
		"CREATE TABLE BS_DetallePE (" +
		"    Instalacion LONG NOT NULL," +
		"    Lote LONG NOT NULL," +
		"    Operacion LONG NOT NULL," +
		"    Cuenta VARCHAR(12) ," +
		"    TipoOperacion LONG," +
		"    Monto NUMERIC(19, 0)  NOT NULL," +
		"    MontoAplicado NUMERIC(19, 0) ," +
		"    Adenda VARCHAR(500) ," +
		"    FechaEstatus TIMESTAMP NOT NULL," +
		"    Estatus LONG NOT NULL," +
		"    IDPago VARCHAR(50) ," +
		"    AutorizacionHost LONG," +
		"    EnviarAHost LONG," +
		"    Contrasena VARCHAR(50) ," +
		"    NombreBenef VARCHAR(60) ," +
		"    DireccionBenef VARCHAR(50) ," +
		"    Email VARCHAR(75) ," +
		"    ComisionAplicada NUMERIC(19, 0)  NOT NULL," +
		"    IVAAplicado NUMERIC(19, 0)  NOT NULL," +
		"    NombreCuenta VARCHAR(50) ," +
		"    NIU LONG," +
		"    Autorizador VARCHAR(10) ," +
		"    Sublote LONG NOT NULL," +
		"    CuentaDebito VARCHAR(16) ," +
		"    NombreDeCuenta VARCHAR(50) ," +
		"    MontoImpuesto NUMERIC(19, 0) ," +
		"    CodigoORefInt VARCHAR(60) " +
		")";
	/**
	 * Atributo que almacena un objeto del tipo {@link SQLLenguaje}.
	 */
	private static SQLLenguaje lenguaje;
	
	/**
	 * M&eacute;todo que realiza la creaci&oacute;n de un DTO, del negocio, a partir del resultado de alguna consulta.
	 * @param <code>rst</code> objeto de tipo <code>Resultset</code>, que contiene los resultados de la consulta.
	 * @return <code>Object</code> - representa al DTO creado.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static Object crearDtoST(ResultSet rst)
			throws SQLException, ExcepcionEnDAO {
 	    BsDetallepe theDto = new BsDetallepe();
     	theDto.setInstalacion(SQLUtils.getLongFromResultSet(rst, "C6001"));
     	theDto.setLote(SQLUtils.getLongFromResultSet(rst, "C6002"));
     	theDto.setOperacion(SQLUtils.getLongFromResultSet(rst, "C6003"));
		theDto.setCuenta(rst.getString("C6004"));
     	theDto.setTipooperacion(SQLUtils.getLongFromResultSet(rst, "C6005"));
		theDto.setMonto(rst.getBigDecimal("C6006"));
		theDto.setMontoaplicado(rst.getBigDecimal("C6007"));
		theDto.setAdenda(rst.getString("C6008"));
		theDto.setFechaestatus(rst.getTimestamp("C6009"));
     	theDto.setEstatus(SQLUtils.getLongFromResultSet(rst, "C6010"));
		theDto.setIdpago(rst.getString("C6011"));
     	theDto.setAutorizacionhost(SQLUtils.getLongFromResultSet(rst, "C6012"));
     	theDto.setEnviarahost(SQLUtils.getLongFromResultSet(rst, "C6013"));
		theDto.setContrasena(rst.getString("C6014"));
		theDto.setNombrebenef(rst.getString("C6015"));
		theDto.setDireccionbenef(rst.getString("C6016"));
		theDto.setEmail(rst.getString("C6017"));
		theDto.setComisionaplicada(rst.getBigDecimal("C6018"));
		theDto.setIvaaplicado(rst.getBigDecimal("C6019"));
		theDto.setNombrecuenta(rst.getString("C6020"));
     	theDto.setNiu(SQLUtils.getLongFromResultSet(rst, "C6021"));
		theDto.setAutorizador(rst.getString("C6022"));
     	theDto.setSublote(SQLUtils.getLongFromResultSet(rst, "C6023"));
		theDto.setCuentadebito(rst.getString("C6024"));
		theDto.setNombredecuenta(rst.getString("C6025"));
		theDto.setMontoimpuesto(rst.getBigDecimal("C6026"));
		theDto.setCodigoorefint(rst.getString("C6027"));
		return theDto;
	}
	
	


	/**
	 * M&eacute;todo que realiza la creaci&oacute;n de un DTO foraneo, del negocio, a partir del resultado de alguna consulta.
	 * @param <code>rst</code> objeto de tipo <code>Resultset</code>, que contiene los resultados de la consulta.
	 * @return <code>BsDetallepe</code> - representa al DTO del negocio con el nuevo DTO foraneo asociado.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static BsDetallepe crearDataConValoresForaneosST(ResultSet rst)throws SQLException, ExcepcionEnDAO{
		BsDetallepe bsDetallepe = (BsDetallepe) crearDtoST(rst);
        BsLotepe bsLotepeRel  = (BsLotepe) BsLotepeDaoJdbcSql.crearDtoST(rst);
        bsDetallepe.setBsLotepe(bsLotepeRel);
		return bsDetallepe;
	}




	/**
	 * M&eacute;todo que realiza la creaci&oacute;n de un DTO foraneo, del negocio, a partir de la b&uacute;squeda por <code>id</code>.
	 * @param <code>rst</code> objeto de tipo <code>Resultset</code>, que contiene los resultados de la consulta.
	 * @return <code>BsDetallepe</code> - representa al DTO del negocio con el nuevo DTO foraneo asociado.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static BsDetallepe crearDataConValoresForaneosNoRelST(ResultSet rst, Connection conn)throws SQLException, ExcepcionEnDAO{	
		BsDetallepe bsDetallepe = (BsDetallepe) crearDtoST(rst);
        BsLotepe instalacionRelbsLotepe  = (BsLotepe) SQLImplementacionOperacionesComunes.buscarPorIDST(
                bsDetallepe.getInstalacion(), conn, 
				BsLotepeDaoJdbcSql.SQL_OPERACIONES,
	       		false, 
	       		null);
        bsDetallepe.setBsLotepe(instalacionRelbsLotepe);
		return bsDetallepe;
	}

	/**
	 * M&eacute;todo que coloca los valores provenientes del objeto Dto en el objeto PreparedStatement,
	 * el cual proviene de la llamada a la implementacion de {@link SQLOperaciones}, este m&eacute;todo
	 * no toma en cuenta el valor del <code>id</code>, puesto que se asume que es un valor secuencial
	 * autonumerico o serial propio de la base de datos.
	 * 
	 * @param <code>theDtoUntyped</code> objeto sin tipo que almacena al DTO del negocio.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaCrearST(Object theDtoUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {			
	        BsDetallepe theDto = (BsDetallepe)theDtoUntyped;
				String cuenta = theDto.getCuenta();     
		Long tipooperacion = theDto.getTipooperacion();     
		java.math.BigDecimal monto = theDto.getMonto();     
		java.math.BigDecimal montoaplicado = theDto.getMontoaplicado();     
		String adenda = theDto.getAdenda();     
		java.util.Date fechaestatus = theDto.getFechaestatus();     
		Long estatus = theDto.getEstatus();     
		String idpago = theDto.getIdpago();     
		Long autorizacionhost = theDto.getAutorizacionhost();     
		Long enviarahost = theDto.getEnviarahost();     
		String contrasena = theDto.getContrasena();     
		String nombrebenef = theDto.getNombrebenef();     
		String direccionbenef = theDto.getDireccionbenef();     
		String email = theDto.getEmail();     
		java.math.BigDecimal comisionaplicada = theDto.getComisionaplicada();     
		java.math.BigDecimal ivaaplicado = theDto.getIvaaplicado();     
		String nombrecuenta = theDto.getNombrecuenta();     
		Long niu = theDto.getNiu();     
		String autorizador = theDto.getAutorizador();     
		Long sublote = theDto.getSublote();     
		String cuentadebito = theDto.getCuentadebito();     
		String nombredecuenta = theDto.getNombredecuenta();     
		java.math.BigDecimal montoimpuesto = theDto.getMontoimpuesto();     
		String codigoorefint = theDto.getCodigoorefint();     
		     
		SQLUtils.setIntoStatement( 1, cuenta, smt);  
    		     
		SQLUtils.setIntoStatement( 2, tipooperacion, smt);  
    		     
		SQLUtils.setIntoStatement( 3, monto, smt);  
    		     
		SQLUtils.setIntoStatement( 4, montoaplicado, smt);  
    		     
		SQLUtils.setIntoStatement( 5, adenda, smt);  
    		     
		SQLUtils.setIntoStatement( 6, fechaestatus, smt);  
    		     
		SQLUtils.setIntoStatement( 7, estatus, smt);  
    		     
		SQLUtils.setIntoStatement( 8, idpago, smt);  
    		     
		SQLUtils.setIntoStatement( 9, autorizacionhost, smt);  
    		     
		SQLUtils.setIntoStatement( 10, enviarahost, smt);  
    		     
		SQLUtils.setIntoStatement( 11, contrasena, smt);  
    		     
		SQLUtils.setIntoStatement( 12, nombrebenef, smt);  
    		     
		SQLUtils.setIntoStatement( 13, direccionbenef, smt);  
    		     
		SQLUtils.setIntoStatement( 14, email, smt);  
    		     
		SQLUtils.setIntoStatement( 15, comisionaplicada, smt);  
    		     
		SQLUtils.setIntoStatement( 16, ivaaplicado, smt);  
    		     
		SQLUtils.setIntoStatement( 17, nombrecuenta, smt);  
    		     
		SQLUtils.setIntoStatement( 18, niu, smt);  
    		     
		SQLUtils.setIntoStatement( 19, autorizador, smt);  
    		     
		SQLUtils.setIntoStatement( 20, sublote, smt);  
    		     
		SQLUtils.setIntoStatement( 21, cuentadebito, smt);  
    		     
		SQLUtils.setIntoStatement( 22, nombredecuenta, smt);  
    		     
		SQLUtils.setIntoStatement( 23, montoimpuesto, smt);  
    		     
		SQLUtils.setIntoStatement( 24, codigoorefint, smt);  
    		
	}

	/**
	 * M&eacute;todo que coloca los valores provenientes del objeto Dto en el objeto PreparedStatement,
	 * el cual proviene de la llamada a la implementacion de {@link SQLOperaciones}, este m&eacute;todo
	 * toma en cuenta el <code>id</code>, lo que quiere decir que se usa al momento de  crear registros
	 * en donde el identificador no es un valor secuencial autonumerico o serial propio de la base de datos
	 *
	 * @param <code>theDtoUntyped</code> objeto sin tipo que almacena al DTO del negocio.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaCrearConIdST(Object theDtoUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {			
	        BsDetallepe theDto = (BsDetallepe)theDtoUntyped;
				Long instalacion = theDto.getInstalacion();     
		Long lote = theDto.getLote();     
		Long operacion = theDto.getOperacion();     
		String cuenta = theDto.getCuenta();     
		Long tipooperacion = theDto.getTipooperacion();     
		java.math.BigDecimal monto = theDto.getMonto();     
		java.math.BigDecimal montoaplicado = theDto.getMontoaplicado();     
		String adenda = theDto.getAdenda();     
		java.util.Date fechaestatus = theDto.getFechaestatus();     
		Long estatus = theDto.getEstatus();     
		String idpago = theDto.getIdpago();     
		Long autorizacionhost = theDto.getAutorizacionhost();     
		Long enviarahost = theDto.getEnviarahost();     
		String contrasena = theDto.getContrasena();     
		String nombrebenef = theDto.getNombrebenef();     
		String direccionbenef = theDto.getDireccionbenef();     
		String email = theDto.getEmail();     
		java.math.BigDecimal comisionaplicada = theDto.getComisionaplicada();     
		java.math.BigDecimal ivaaplicado = theDto.getIvaaplicado();     
		String nombrecuenta = theDto.getNombrecuenta();     
		Long niu = theDto.getNiu();     
		String autorizador = theDto.getAutorizador();     
		Long sublote = theDto.getSublote();     
		String cuentadebito = theDto.getCuentadebito();     
		String nombredecuenta = theDto.getNombredecuenta();     
		java.math.BigDecimal montoimpuesto = theDto.getMontoimpuesto();     
		String codigoorefint = theDto.getCodigoorefint();     
	
		SQLUtils.setIntoStatement( 1, instalacion, smt);
    	
		SQLUtils.setIntoStatement( 2, lote, smt);
    	
		SQLUtils.setIntoStatement( 3, operacion, smt);
    	
		SQLUtils.setIntoStatement( 4, cuenta, smt);
    	
		SQLUtils.setIntoStatement( 5, tipooperacion, smt);
    	
		SQLUtils.setIntoStatement( 6, monto, smt);
    	
		SQLUtils.setIntoStatement( 7, montoaplicado, smt);
    	
		SQLUtils.setIntoStatement( 8, adenda, smt);
    	
		SQLUtils.setIntoStatement( 9, fechaestatus, smt);
    	
		SQLUtils.setIntoStatement( 10, estatus, smt);
    	
		SQLUtils.setIntoStatement( 11, idpago, smt);
    	
		SQLUtils.setIntoStatement( 12, autorizacionhost, smt);
    	
		SQLUtils.setIntoStatement( 13, enviarahost, smt);
    	
		SQLUtils.setIntoStatement( 14, contrasena, smt);
    	
		SQLUtils.setIntoStatement( 15, nombrebenef, smt);
    	
		SQLUtils.setIntoStatement( 16, direccionbenef, smt);
    	
		SQLUtils.setIntoStatement( 17, email, smt);
    	
		SQLUtils.setIntoStatement( 18, comisionaplicada, smt);
    	
		SQLUtils.setIntoStatement( 19, ivaaplicado, smt);
    	
		SQLUtils.setIntoStatement( 20, nombrecuenta, smt);
    	
		SQLUtils.setIntoStatement( 21, niu, smt);
    	
		SQLUtils.setIntoStatement( 22, autorizador, smt);
    	
		SQLUtils.setIntoStatement( 23, sublote, smt);
    	
		SQLUtils.setIntoStatement( 24, cuentadebito, smt);
    	
		SQLUtils.setIntoStatement( 25, nombredecuenta, smt);
    	
		SQLUtils.setIntoStatement( 26, montoimpuesto, smt);
    	
		SQLUtils.setIntoStatement( 27, codigoorefint, smt);
    		
	}

	/**
	 * M&eacute;todo que coloca el identificador recibido en el objeto PreparedStatement, para poder ser eliminado.
	 * @param <code>theIdUntyped</code> objeto sin tipo que identifica al <code>id</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaEliminarST(Object theIdUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {
			BsDetallepeID theId = (BsDetallepeID)theIdUntyped;
			Long instalacion = theId.getInstalacion();     
		Long lote = theId.getLote();     
		Long operacion = theId.getOperacion();     
		     
		SQLUtils.setIntoStatement( 1, instalacion, smt);  
    		     
		SQLUtils.setIntoStatement( 2, lote, smt);  
    		     
		SQLUtils.setIntoStatement( 3, operacion, smt);  
    		}

	/**
	 * M&eacute;todo que coloca el identificador recibido, en el objeto PreparedStatement para poder hacer la consulta de busqueda.
	 * @param <code>theIdUntyped</code> objeto sin tipo que identifica al <code>id</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaBuscarPorIdST(Object theIdUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {
			BsDetallepeID theId = (BsDetallepeID)theIdUntyped;
				Long instalacion = theId.getInstalacion();     
		Long lote = theId.getLote();     
		Long operacion = theId.getOperacion();     
		     
		SQLUtils.setIntoStatement( 1, instalacion, smt);  
    		     
		SQLUtils.setIntoStatement( 2, lote, smt);  
    		     
		SQLUtils.setIntoStatement( 3, operacion, smt);  
    			}

	/**
	 * M&eacute;todo que coloca los valores provenientes del objeto Dto en el objeto PreparedStatement,
	 * el cual proviene de la llamada a la implementacion de {@link SQLOperaciones} para poder actualizar
     * un registro en espec&iacute;fico.
	 * 
	 * @param <code>theDtoUntyped</code> objeto sin tipo que almacena al DTO del negocio.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaActualizarST(Object theDtoUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {
			BsDetallepe theDto = (BsDetallepe)theDtoUntyped;
				Long instalacion = theDto.getInstalacion();     
		Long lote = theDto.getLote();     
		Long operacion = theDto.getOperacion();     
		String cuenta = theDto.getCuenta();     
		Long tipooperacion = theDto.getTipooperacion();     
		java.math.BigDecimal monto = theDto.getMonto();     
		java.math.BigDecimal montoaplicado = theDto.getMontoaplicado();     
		String adenda = theDto.getAdenda();     
		java.util.Date fechaestatus = theDto.getFechaestatus();     
		Long estatus = theDto.getEstatus();     
		String idpago = theDto.getIdpago();     
		Long autorizacionhost = theDto.getAutorizacionhost();     
		Long enviarahost = theDto.getEnviarahost();     
		String contrasena = theDto.getContrasena();     
		String nombrebenef = theDto.getNombrebenef();     
		String direccionbenef = theDto.getDireccionbenef();     
		String email = theDto.getEmail();     
		java.math.BigDecimal comisionaplicada = theDto.getComisionaplicada();     
		java.math.BigDecimal ivaaplicado = theDto.getIvaaplicado();     
		String nombrecuenta = theDto.getNombrecuenta();     
		Long niu = theDto.getNiu();     
		String autorizador = theDto.getAutorizador();     
		Long sublote = theDto.getSublote();     
		String cuentadebito = theDto.getCuentadebito();     
		String nombredecuenta = theDto.getNombredecuenta();     
		java.math.BigDecimal montoimpuesto = theDto.getMontoimpuesto();     
		String codigoorefint = theDto.getCodigoorefint();     
		     
		SQLUtils.setIntoStatement( 1, cuenta, smt);  
        	     
		SQLUtils.setIntoStatement( 2, tipooperacion, smt);  
        	     
		SQLUtils.setIntoStatement( 3, monto, smt);  
        	     
		SQLUtils.setIntoStatement( 4, montoaplicado, smt);  
        	     
		SQLUtils.setIntoStatement( 5, adenda, smt);  
        	     
		SQLUtils.setIntoStatement( 6, fechaestatus, smt);  
        	     
		SQLUtils.setIntoStatement( 7, estatus, smt);  
        	     
		SQLUtils.setIntoStatement( 8, idpago, smt);  
        	     
		SQLUtils.setIntoStatement( 9, autorizacionhost, smt);  
        	     
		SQLUtils.setIntoStatement( 10, enviarahost, smt);  
        	     
		SQLUtils.setIntoStatement( 11, contrasena, smt);  
        	     
		SQLUtils.setIntoStatement( 12, nombrebenef, smt);  
        	     
		SQLUtils.setIntoStatement( 13, direccionbenef, smt);  
        	     
		SQLUtils.setIntoStatement( 14, email, smt);  
        	     
		SQLUtils.setIntoStatement( 15, comisionaplicada, smt);  
        	     
		SQLUtils.setIntoStatement( 16, ivaaplicado, smt);  
        	     
		SQLUtils.setIntoStatement( 17, nombrecuenta, smt);  
        	     
		SQLUtils.setIntoStatement( 18, niu, smt);  
        	     
		SQLUtils.setIntoStatement( 19, autorizador, smt);  
        	     
		SQLUtils.setIntoStatement( 20, sublote, smt);  
        	     
		SQLUtils.setIntoStatement( 21, cuentadebito, smt);  
        	     
		SQLUtils.setIntoStatement( 22, nombredecuenta, smt);  
        	     
		SQLUtils.setIntoStatement( 23, montoimpuesto, smt);  
        	     
		SQLUtils.setIntoStatement( 24, codigoorefint, smt);  
        		     
		SQLUtils.setIntoStatement( 25, instalacion, smt);  
    		     
		SQLUtils.setIntoStatement( 26, lote, smt);  
    		     
		SQLUtils.setIntoStatement( 27, operacion, smt);  
    			
	}


	/**
	 * M&eacute;todo que ejecuta una petici&oacute;n de datos a trav&eacute;s de un objeto {@link PreparedStatement},
	 * y un objeto del tipo {@link PeticionDeDatos}, el cual en su implementaci&oacute;n define una serie de restricciones
	 * usadas para hacer la petici&oacute;n.
	 *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaPeticionDeDatosST(PeticionDeDatos peticion,
			PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
		SQLImplementacionOperacionesComunes.operacionesParaPeticionDeDatosST( peticion, smt, SQL_OPERACIONES, null);	
	}	
	
	/**
	 * M&eacute;todo que ejecuta una petici&oacute;n de datos a trav&eacute;s de un objeto {@link PreparedStatement},
     * y un objeto del tipo {@link PeticionDeDatos}, el cual en su implementaci&oacute;n define una serie de restricciones
     * usadas para hacer la petici&oacute;n.
	 *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaPeticionDeDatosFST(PeticionDeDatos peticion,
			PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
		SQLImplementacionOperacionesComunes.operacionesParaPeticionDeDatosST( peticion, smt, SQL_OPERACIONES, null);	
	}	

	/**
	 * M&eacute;todo que ejecuta una petici&oacute;n de conteo a trav&eacute;s de un objeto {@link PreparedStatement},
     * y un objeto del tipo {@link PeticionDeDatos}, el cual en su implementaci&oacute;n define una serie de restricciones
     * usadas para hacer la petici&oacute;n.
	 *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaConteoST(PeticionDeDatos peticion,
			PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
		SQLImplementacionOperacionesComunes.operacionesParaPeticionDeDatosST( peticion, smt, SQL_OPERACIONES, null);
	}

	/**
	 * M&eacute;todo que agrega al atributo {@link #SELECT_HEADER_SQL} las condiciones de b&uacute;squeda
     * que vienen en el objeto del tipo {@link PeticionDeDatos}
     *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>, que contiene las condiciones de la consulta.
	 * @return <code>String</code> - la consulta condicionada como cadena de caracteres.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static String getSqlParaPeticionDeDatosST(PeticionDeDatos peticion)
			throws SQLException, ExcepcionEnDAO {
		return SELECT_HEADER_SQL + SQLImplementacionOperacionesComunes.crearCondicionesSQLST( peticion, SQL_OPERACIONES, SQLImplementacionOperacionesComunes.COND_ALIAS_ORDEN_WHERE);
	}
	
	
	
	/**
	 * M&eacute;todo que agrega al atributo {@link #SELECT_HEADER_SQL_F} las condiciones de b&uacute;squeda
     * que vienen en el objeto del tipo {@link PeticionDeDatos}
     *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>, que contiene las condiciones de la consulta.
	 * @return <code>String</code> - la consulta condicionada como cadena de caracteres.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static String getSqlParaPeticionDeDatosFST(PeticionDeDatos peticion)
			throws SQLException, ExcepcionEnDAO {
		return SELECT_HEADER_SQL_F + SQLImplementacionOperacionesComunes.crearCondicionesSQLST( peticion, SQL_OPERACIONES, SQLImplementacionOperacionesComunes.COND_ALIAS_ORDEN_AND);
	}
	

	

	/**
	 * M&eacute;todo que agrega al atributo {@link #COUNT_HEADER_SQL} las condiciones de conteo
	 * que vienen en el objeto de tipo {@link PeticionDeDatos}.
	 *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>, que contiene las condiciones de la consulta.
	 * @return <code>String</code> - la consulta condicionada como cadena de caracteres.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static String getSqlDeConteoST(PeticionDeDatos peticion)
			throws SQLException, ExcepcionEnDAO {
		return COUNT_HEADER_SQL + SQLImplementacionOperacionesComunes.crearCondicionesSQLST( peticion, SQL_OPERACIONES, SQLImplementacionOperacionesComunes.COND_SQL_WHERE);
	}
	
	/**
	 * M&eacute;todo que se utiliza para actualizar un registro a nivel de tabla, recibe como par&aacute;metro un DTO del negocio
	 * que contiene la informaci&oacute;n a actualizar, e invoca la funci&oacute;n <code>actualizar</code> de la clase de la que hereda.
	 *
	 * @param <code>bsDetallepe</code> objeto del tipo <code>BsDetallepe</code>.
	 * @throws ExcepcionEnDAO
	 * @see {@link SQLImplementacionBasica}
	 */
	public void actualizar(BsDetallepe bsDetallepe) throws ExcepcionEnDAO {
	    actualizar(bsDetallepe, AUTO_COMMIT);
	
	}
	
	/**
	 * M&eacute;todo utilizado para realizar una b&uacute;squeda de un registro espec&iacute;fico, utilizando un identificador.
	 *
	 * @param <code>bsDetallepeID</code> objeto del tipo <code>BsDetallepeID</code>.
	 * @return <code>BsDetallepe</code> - un objeto DTO.
	 * @throws ExcepcionEnDAO
	 * @see {@link SQLImplementacionBasica}
	 */
	public BsDetallepe buscarPorID(BsDetallepeID bsDetallepeID)
			throws ExcepcionEnDAO {
		return (BsDetallepe)buscarPorID(bsDetallepeID, AUTO_COMMIT);				 
	}

	/**
	 * M&eacute;todo utilizado para la creaci&oacute;n de un registro, a nivel de tabla, a partir de la informaci&oacute;n
	 * recibida en un DTO del objeto de Negocio, e invoca la funci&oacute;n <code>crear</code> de la clase de la que hereda.
	 * 
	 * @param <code>bsDetallepe</code> objeto del tipo <code>BsDetallepe</code>.
	 * @throws ExcepcionEnDAO
	 * @see {@link SQLImplementacionBasica}
	 */
	public void crear(BsDetallepe bsDetallepe) throws ExcepcionEnDAO {
	    crear(bsDetallepe, AUTO_COMMIT);		
	}

	/**
	 * M&eacute;todo utilizado para la eliminaci&oacute;n de un registro, a nivel de tabla,
	 * que se busca a trav&eacute;s del identificador recibido. Se invoca la funci&oacute;n <code>eliminar</code> de la clase de la que hereda.
	 *
	 * @param <code>bsDetallepeID</code> objeto del tipo <code>BsDetallepeID</code>.
	 * @throws ExcepcionEnDAO
	 * @see {@link SQLImplementacionBasica}
	 */
	public void eliminar(BsDetallepeID bsDetallepeID) throws ExcepcionEnDAO {
	    eliminar(bsDetallepeID, AUTO_COMMIT);		
	}

	/**
	 * M&eacute;todo utilizado para devolver un &uacute;nico resultado de la petici&oacute;n recibida,
	 * la cual se realiza a trav&eacuate;s del objeto de tipo {@link PeticionDeDatos}.
	 * 
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>, que contiene las condiciones de la consulta.
	 * @return <code>BsDetallepe</code> - un objeto DTO.
	 * @throws ExcepcionEnDAO
	 * @throws ExcepcionPorDatoNoEncontrado
	 * @see {@link SQLImplementacionBasica}
	 */
	public BsDetallepe resultadoUnico(PeticionDeDatos peticion)
			throws ExcepcionEnDAO, ExcepcionPorDatoNoEncontrado {
		return (BsDetallepe)resultadoUnico(peticion, AUTO_COMMIT);
	}
	
	/**
	 * Atributo del tipo {@link SQLOperaciones}, almacena la operaciones disponibles para esta implementaci&oacute;n.<br/>
	 * Algunas de las llamadas a los m&eacute;todos se realiza a trav&eacute;s de la invocaci&oacute;n de estas implementaciones.
	 */
    public static final SQLOperaciones	SQL_OPERACIONES =  new SQLOperaciones() {
			
			public int getManejoForaneosPorDefecto() {
				return Dao.FORANEOS_JOIN;
			}
			
			
			public void operacionesParaPeticionDeDatosF(PeticionDeDatos request,
					PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
				BsDetallepeDaoJdbcSql.operacionesParaPeticionDeDatosFST(request, smt);
			}
			
			public void operacionesParaPeticionDeDatos(PeticionDeDatos request,
					PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
				BsDetallepeDaoJdbcSql.operacionesParaPeticionDeDatosST(request, smt);
			}
			
			public void operacionesParaEliminar(Object theId, PreparedStatement smt)
					throws SQLException, ExcepcionEnDAO {
				BsDetallepeDaoJdbcSql.operacionesParaEliminarST(theId, smt);
			}
			
			public void operacionesParaCrear(Object theDto, PreparedStatement smt)
					throws SQLException, ExcepcionEnDAO {
				BsDetallepeDaoJdbcSql.operacionesParaCrearST(theDto, smt);
			}
			
			public void operacionesParaCrearConId(Object theDto, PreparedStatement smt) 
			        throws SQLException, ExcepcionEnDAO {
			    BsDetallepeDaoJdbcSql.operacionesParaCrearConIdST(theDto, smt);
            }
			
			public void operacionesParaConteo(PeticionDeDatos request,
					PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
				BsDetallepeDaoJdbcSql.operacionesParaConteoST(request, smt);				
			}
			
			public void operacionesParaBuscarPorId(Object theId, PreparedStatement smt)
					throws SQLException, ExcepcionEnDAO {
				BsDetallepeDaoJdbcSql.operacionesParaBuscarPorIdST(theId, smt);
			}
			
			public void operacionesParaActualizar(Object theDto, PreparedStatement smt)
					throws SQLException, ExcepcionEnDAO {
				BsDetallepeDaoJdbcSql.operacionesParaActualizarST(theDto, smt);
				
			}
			
			public String getSqlParaPeticionDeDatosF(PeticionDeDatos request)
					throws SQLException, ExcepcionEnDAO {	
				return BsDetallepeDaoJdbcSql.getSqlParaPeticionDeDatosFST(request);
			}
			
			public String getSqlParaPeticionDeDatos(PeticionDeDatos request)
					throws SQLException, ExcepcionEnDAO {
				return BsDetallepeDaoJdbcSql.getSqlParaPeticionDeDatosST(request);
			}
			
			public String getSqlDeConteo(PeticionDeDatos request) throws SQLException,
					ExcepcionEnDAO {
				return BsDetallepeDaoJdbcSql.getSqlDeConteoST(request);
			}
			
			public SQLEntidadMeta getSQLEntidadMeta() throws ExcepcionEnDAO {
				return BsDetallepeDaoJdbcSql.SQL_ENTIDAD_META;
			}
			
			public String getEliminarSql() {
				return  BsDetallepeDaoJdbcSql.DELETE_SQL;
			}
			
			public String getCrearSql() {
				return  BsDetallepeDaoJdbcSql.CREATE_SQL;
			}
			
			public String getCrearSqlConId(){
			    return  BsDetallepeDaoJdbcSql.CREATE_SQL_CON_ID;
			}			
			
			public String getBuscarPorIdSqlF() {
				return BsDetallepeDaoJdbcSql.FIND_BY_ID_SQL_F;
			}
			
			public String getBuscarPorIdSql() {				
				return BsDetallepeDaoJdbcSql.FIND_BY_ID_SQL;
			}
			
			public String getActualizarSql() {				
				return BsDetallepeDaoJdbcSql.UPDATE_SQL;
			}
			
			public Object crearDto(ResultSet rst) throws SQLException, ExcepcionEnDAO {
				return BsDetallepeDaoJdbcSql.crearDtoST(rst);
			}
	        
	        public Object crearDataConValoresForaneos(ResultSet rst)
					throws SQLException, ExcepcionEnDAO {
				return BsDetallepeDaoJdbcSql.crearDataConValoresForaneosST(rst);
			}
			
	        public Object crearDataConValoresForaneosNoRel(ResultSet rst, Connection conn)
					throws SQLException, ExcepcionEnDAO {
				return BsDetallepeDaoJdbcSql.crearDataConValoresForaneosNoRelST(rst, conn);
			}
			
			
            public boolean isForaneosDisponible(){
				return true;
            
            }
            
            public SQLLenguaje getSQLLenguaje() {
                return BsDetallepeDaoJdbcSql.getLenguaje();
            }
            
            public void setIdEnDto(Object theDto, Object dtoId) {
                setIdEnDtoST(theDto, dtoId);
            }
	};
	
	
	
	
	
	/**
	 * Devuelve el atributo {@link #SQL_OPERACIONES}.
	 * @return <code>SQLOperaciones</code> - objeto del tipo {@link SQLOperaciones}
	 */
    public SQLOperaciones getSqlOperaciones() {
       return SQL_OPERACIONES;
	}
	
	/**
	 * Devuelve el atributo {@link #lenguaje}
	 * @return <code>SQLLenguaje</code> - objeto del tipo {@link SQLLenguaje}
	 */	
	public static SQLLenguaje getLenguaje() {
		return lenguaje;
	}
	
	/**
	 * M&eacute;todo que permite obtener a trav&eacute;s de un Servicio la implementaci&oacute;n del objeto de Lenguaje,
	 * y coloca las reglas de negocio almacenadas en el atributo {@link #SQL_ENTIDAD_META}.
	 * @param <code>localizadorLenguaje</code> objeto del tipo {@link LocalizadorDeServicio} que posee informaci&oacute;n para trabajar con la base de datos.
	 */
	public static void setLocalizadorLenguaje(LocalizadorDeServicio localizadorLenguaje) {
		lenguaje = (SQLLenguaje) localizadorLenguaje.getServicio();
		lenguaje.setSqlEntidadMeta(SQL_ENTIDAD_META);
	}

	/**
	 * M&eacute;todo que agrega un identificador a un DTO del tipo <code>BsDetallepe</code>.
	 * @param <code>theDtoUntyped</code> objeto gen&eacute;rico, que representa a un <code>DTO</code>.
	 * @param <code>dtoIdUntyped</code> objeto gen&eacute;rico, que representa al identificador.
	 */
	public static void setIdEnDtoST(Object theDtoUntyped, Object dtoIdUntyped){
		BsDetallepe dto = (BsDetallepe)theDtoUntyped;
		BsDetallepeID theId = (BsDetallepeID)dtoIdUntyped;
		Long instalacion = theId.getInstalacion();     
		Long lote = theId.getLote();     
		Long operacion = theId.getOperacion();     
		dto.setInstalacion(instalacion);
		dto.setLote(lote);
		dto.setOperacion(operacion);

	}	
	
	
}
