package com.ofbox.davivienda.h2h.orm.jdbc.sql.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.sql.Connection;
import java.util.Collection;

import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Dao;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.ExcepcionEnDAO;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.ExcepcionPorDatoNoEncontrado;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.PeticionDeDatos;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Transaccion;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.foraneos.DataConValoresForaneos;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLColumnaMeta;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLEntidadMeta;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLImplementacionBasica;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLImplementacionOperacionesComunes;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLLenguaje;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLOperaciones;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLUtils;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.TransaccionJdbc;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dependencias.LocalizadorDeServicio;


import com.ofbox.davivienda.h2h.abstraccion.dto.BsCliente;
import java.lang.Long;
import com.ofbox.davivienda.h2h.abstraccion.dao.BsClienteDao;




/**
 * Clase que hereda comportamientos de {@link SQLImplementacionBasica} e implementa el comportamiento definido en la capa DAO.<br/>
 * Esta clase es una implementaci&oacute;n intermedia para el DAO de <b>Cliente</b>
 * asociado a la tabla <b>BS_Cliente</b>, en esta clase se construyen todos los sql para las operaciones de listado,
 * creaci&oacute;n, consulta, actualizaci&oacute;n y elminaci&oacute;n.<br/>
 * @author Systems Out of the Box
 * @version f3.1
 *
 */
public class BsClienteDaoJdbcSql extends SQLImplementacionBasica implements BsClienteDao{

	/**
	 * Atributo del tipo {@link SQLEntidadMeta}, que almacena la descripci&oacute;n del negocio como una entidad.<br/>
	 * Entre sus caracter&iacute;sticas tiene el nombre de la tabla, nombre de la secuencia, posici&oacute;n de las columnas,
	 * nombre de las columnas y tipos de datos para la columna.<br/>
	 * El arreglo que recibe son objetos de tipo {@link SQLColumnaMeta}, y definen informaci&oacute;n para obtener las columnas.
	 */
	public static final SQLEntidadMeta SQL_ENTIDAD_META = new SQLEntidadMeta("BS_Cliente", new SQLColumnaMeta[]{
        new SQLColumnaMeta(0, "BS_Cliente", "E0", 1, "Cliente", "C1", Types.BIGINT ,true)
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 2, "Nombre", "C2", Types.VARCHAR )
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 3, "Modulos", "C3", Types.BIGINT )
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 4, "Direccion", "C4", Types.VARCHAR )
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 5, "Telefono", "C5", Types.VARCHAR )
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 6, "Fax", "C6", Types.VARCHAR )
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 7, "Email", "C7", Types.VARCHAR )
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 8, "NombreContacto", "C8", Types.VARCHAR )
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 9, "ApellidoContacto", "C9", Types.VARCHAR )
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 10, "NumInstalacion", "C10", Types.BIGINT )
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 11, "FechaEstatus", "C11", Types.TIMESTAMP )
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 12, "Estatus", "C12", Types.BIGINT )
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 13, "FechaCreacion", "C13", Types.TIMESTAMP )
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 14, "SolicitudPendiente", "C14", Types.BIGINT )
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 15, "NivelSeguridadTEx", "C15", Types.BIGINT )
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 16, "NivelSeguridadPImp", "C16", Types.BIGINT )
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 17, "nombreRepresentante", "C17", Types.VARCHAR )
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 18, "apellidoRepresentante", "C18", Types.VARCHAR )
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 19, "proveedorInternet", "C19", Types.VARCHAR )
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 20, "Banca", "C20", Types.BIGINT )
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 21, "PoliticaCobroTEx", "C21", Types.BIGINT )
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 22, "ComisionTransExt", "C22", Types.NUMERIC )
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 23, "CodigoTablaTransExt", "C23", Types.BIGINT )
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 24, "NivelSeguridadISSS", "C24", Types.BIGINT )
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 25, "NivelSeguridadAFP", "C25", Types.BIGINT )
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 26, "NivelSeguridadCCre", "C26", Types.BIGINT )
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 27, "NivelSeguridadPPag", "C27", Types.BIGINT )
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 28, "CargaPersonalizable", "C28", Types.VARCHAR )
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 29, "TipoDoctoRepresentante", "C29", Types.BIGINT )
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 30, "DoctoRepresentante", "C30", Types.VARCHAR )
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 31, "Ciudad", "C31", Types.VARCHAR )
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 32, "Cargo", "C32", Types.VARCHAR )
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 33, "LimiteXArchivo", "C33", Types.NUMERIC )
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 34, "LimiteXLote", "C34", Types.NUMERIC )
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 35, "LimiteXTransaccion", "C35", Types.NUMERIC )
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 36, "PermiteDebitos", "C36", Types.VARCHAR )
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 37, "LimiteCreditos", "C37", Types.NUMERIC )
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 38, "LimiteDebitos", "C38", Types.NUMERIC )
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 39, "ComisionTrnACH", "C39", Types.NUMERIC )
	});
	
	


	/**
	 * Atributo que almacena el <code>String</code> para obtener el listado de registros del negocio.
	 */
	public static final String SELECT_HEADER_SQL =  
		"SELECT E0.Cliente AS C1 , E0.Nombre AS C2 , E0.Modulos AS C3 , E0.Direccion AS C4 , " +
		"       E0.Telefono AS C5 , E0.Fax AS C6 , E0.Email AS C7 , E0.NombreContacto AS C8 , " +
		"       E0.ApellidoContacto AS C9 , E0.NumInstalacion AS C10 , E0.FechaEstatus AS C11 , E0.Estatus AS C12 , " +
		"       E0.FechaCreacion AS C13 , E0.SolicitudPendiente AS C14 , E0.NivelSeguridadTEx AS C15 , E0.NivelSeguridadPImp AS C16 , " +
		"       E0.nombreRepresentante AS C17 , E0.apellidoRepresentante AS C18 , E0.proveedorInternet AS C19 , E0.Banca AS C20 , " +
		"       E0.PoliticaCobroTEx AS C21 , E0.ComisionTransExt AS C22 , E0.CodigoTablaTransExt AS C23 , E0.NivelSeguridadISSS AS C24 , " +
		"       E0.NivelSeguridadAFP AS C25 , E0.NivelSeguridadCCre AS C26 , E0.NivelSeguridadPPag AS C27 , E0.CargaPersonalizable AS C28 , " +
		"       E0.TipoDoctoRepresentante AS C29 , E0.DoctoRepresentante AS C30 , E0.Ciudad AS C31 , E0.Cargo AS C32 , " +
		"       E0.LimiteXArchivo AS C33 , E0.LimiteXLote AS C34 , E0.LimiteXTransaccion AS C35 , E0.PermiteDebitos AS C36 , " +
		"       E0.LimiteCreditos AS C37 , E0.LimiteDebitos AS C38 , E0.ComisionTrnACH AS C39" +
		"  FROM BS_Cliente E0";
	/**
	 * Atributo que almacena el <code>String</code> para obtener el n&uacute;mero de registros presentes en el negocio.
	 */
	public static final String COUNT_HEADER_SQL =   "SELECT COUNT(*) FROM BS_Cliente";    												
	/**
	 * Atributo que almacena el <code>String</code> para la inserci&oacute;n, sin id, de un nuevo registro en la tabla.
	 */
	public static final String CREATE_SQL = 
		"INSERT INTO BS_Cliente (" +
		"Nombre,Modulos,Direccion,Telefono,Fax" +
		",Email,NombreContacto,ApellidoContacto,NumInstalacion" +
		",FechaEstatus,Estatus,FechaCreacion,SolicitudPendiente" +
		",NivelSeguridadTEx,NivelSeguridadPImp,nombreRepresentante,apellidoRepresentante" +
		",proveedorInternet,Banca,PoliticaCobroTEx,ComisionTransExt" +
		",CodigoTablaTransExt,NivelSeguridadISSS,NivelSeguridadAFP,NivelSeguridadCCre" +
		",NivelSeguridadPPag,CargaPersonalizable,TipoDoctoRepresentante,DoctoRepresentante" +
		",Ciudad,Cargo,LimiteXArchivo,LimiteXLote" +
		",LimiteXTransaccion,PermiteDebitos,LimiteCreditos,LimiteDebitos" +
		",ComisionTrnACH" +
		") VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	/**
	 * Atributo que almacena el <code>String</code> para insertar, con id, un registro en la table.
	 */
	public static final String CREATE_SQL_CON_ID = 
		"INSERT INTO BS_Cliente (" +
		"Cliente,Nombre,Modulos,Direccion,Telefono" +
		",Fax,Email,NombreContacto,ApellidoContacto" +
		",NumInstalacion,FechaEstatus,Estatus,FechaCreacion" +
		",SolicitudPendiente,NivelSeguridadTEx,NivelSeguridadPImp,nombreRepresentante" +
		",apellidoRepresentante,proveedorInternet,Banca,PoliticaCobroTEx" +
		",ComisionTransExt,CodigoTablaTransExt,NivelSeguridadISSS,NivelSeguridadAFP" +
		",NivelSeguridadCCre,NivelSeguridadPPag,CargaPersonalizable,TipoDoctoRepresentante" +
		",DoctoRepresentante,Ciudad,Cargo,LimiteXArchivo" +
		",LimiteXLote,LimiteXTransaccion,PermiteDebitos,LimiteCreditos" +
		",LimiteDebitos,ComisionTrnACH" +
		") VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	/**
	 * Atributo que almacena el <code>String</code> para la atualizaci&oacute; de un registro en la tabla.
	 */
	public static final String UPDATE_SQL = 
		"UPDATE BS_Cliente" +
		" SET Nombre= ?  , Modulos= ?  , Direccion= ?  , Telefono= ?  , " +
		"Fax= ?  , Email= ?  , NombreContacto= ?  , ApellidoContacto= ?  , " +
		"NumInstalacion= ?  , FechaEstatus= ?  , Estatus= ?  , FechaCreacion= ?  , " +
		"SolicitudPendiente= ?  , NivelSeguridadTEx= ?  , NivelSeguridadPImp= ?  , nombreRepresentante= ?  , " +
		"apellidoRepresentante= ?  , proveedorInternet= ?  , Banca= ?  , PoliticaCobroTEx= ?  , " +
		"ComisionTransExt= ?  , CodigoTablaTransExt= ?  , NivelSeguridadISSS= ?  , NivelSeguridadAFP= ?  , " +
		"NivelSeguridadCCre= ?  , NivelSeguridadPPag= ?  , CargaPersonalizable= ?  , TipoDoctoRepresentante= ?  , " +
		"DoctoRepresentante= ?  , Ciudad= ?  , Cargo= ?  , LimiteXArchivo= ?  , " +
		"LimiteXLote= ?  , LimiteXTransaccion= ?  , PermiteDebitos= ?  , LimiteCreditos= ?  , " +
		"LimiteDebitos= ?  , ComisionTrnACH= ? " +
		" WHERE Cliente= ? ";
	/**
	 * Atributo que almacena el <code>String</code> para la eliminaci&oacute; de un registro en la tabla.
	 */
	public static final String DELETE_SQL =  
		"DELETE FROM BS_Cliente" +
		" WHERE Cliente= ? ";
	/**
	 * Atributo que almacena el <code>String</code> para la b&uacute;squeda por id de un registro de la tabla.
	 */
	public static final String FIND_BY_ID_SQL = 
		"SELECT E0.Cliente AS C1 , E0.Nombre AS C2 , E0.Modulos AS C3 , E0.Direccion AS C4 , " +
		"       E0.Telefono AS C5 , E0.Fax AS C6 , E0.Email AS C7 , E0.NombreContacto AS C8 , " +
		"       E0.ApellidoContacto AS C9 , E0.NumInstalacion AS C10 , E0.FechaEstatus AS C11 , E0.Estatus AS C12 , " +
		"       E0.FechaCreacion AS C13 , E0.SolicitudPendiente AS C14 , E0.NivelSeguridadTEx AS C15 , E0.NivelSeguridadPImp AS C16 , " +
		"       E0.nombreRepresentante AS C17 , E0.apellidoRepresentante AS C18 , E0.proveedorInternet AS C19 , E0.Banca AS C20 , " +
		"       E0.PoliticaCobroTEx AS C21 , E0.ComisionTransExt AS C22 , E0.CodigoTablaTransExt AS C23 , E0.NivelSeguridadISSS AS C24 , " +
		"       E0.NivelSeguridadAFP AS C25 , E0.NivelSeguridadCCre AS C26 , E0.NivelSeguridadPPag AS C27 , E0.CargaPersonalizable AS C28 , " +
		"       E0.TipoDoctoRepresentante AS C29 , E0.DoctoRepresentante AS C30 , E0.Ciudad AS C31 , E0.Cargo AS C32 , " +
		"       E0.LimiteXArchivo AS C33 , E0.LimiteXLote AS C34 , E0.LimiteXTransaccion AS C35 , E0.PermiteDebitos AS C36 , " +
		"       E0.LimiteCreditos AS C37 , E0.LimiteDebitos AS C38 , E0.ComisionTrnACH AS C39" +
		"  FROM BS_Cliente E0" +
		" WHERE E0.Cliente= ? ";
	/**
	 * Atributo que almacena el <code>String</code> para la creacion de la tabla en la base de datos.
	 */
	public static final String CREATE_TABLE_DDL = 
		"CREATE TABLE BS_Cliente (" +
		"    Cliente LONG NOT NULL," +
		"    Nombre VARCHAR(50)  NOT NULL," +
		"    Modulos LONG NOT NULL," +
		"    Direccion VARCHAR(50) ," +
		"    Telefono VARCHAR(40) ," +
		"    Fax VARCHAR(30) ," +
		"    Email VARCHAR(45) ," +
		"    NombreContacto VARCHAR(30) ," +
		"    ApellidoContacto VARCHAR(30) ," +
		"    NumInstalacion LONG NOT NULL," +
		"    FechaEstatus TIMESTAMP NOT NULL," +
		"    Estatus LONG NOT NULL," +
		"    FechaCreacion TIMESTAMP NOT NULL," +
		"    SolicitudPendiente LONG," +
		"    NivelSeguridadTEx LONG NOT NULL," +
		"    NivelSeguridadPImp LONG," +
		"    nombreRepresentante VARCHAR(30) ," +
		"    apellidoRepresentante VARCHAR(30) ," +
		"    proveedorInternet VARCHAR(30) ," +
		"    Banca LONG NOT NULL," +
		"    PoliticaCobroTEx LONG NOT NULL," +
		"    ComisionTransExt NUMERIC(19, 0) ," +
		"    CodigoTablaTransExt LONG," +
		"    NivelSeguridadISSS LONG," +
		"    NivelSeguridadAFP LONG," +
		"    NivelSeguridadCCre LONG," +
		"    NivelSeguridadPPag LONG," +
		"    CargaPersonalizable VARCHAR(1) ," +
		"    TipoDoctoRepresentante LONG," +
		"    DoctoRepresentante VARCHAR(35) ," +
		"    Ciudad VARCHAR(50)  NOT NULL," +
		"    Cargo VARCHAR(100)  NOT NULL," +
		"    LimiteXArchivo NUMERIC(19, 0)  NOT NULL," +
		"    LimiteXLote NUMERIC(19, 0)  NOT NULL," +
		"    LimiteXTransaccion NUMERIC(19, 0)  NOT NULL," +
		"    PermiteDebitos VARCHAR(1)  NOT NULL," +
		"    LimiteCreditos NUMERIC(19, 0)  NOT NULL," +
		"    LimiteDebitos NUMERIC(19, 0)  NOT NULL," +
		"    ComisionTrnACH NUMERIC(19, 0)  NOT NULL" +
		")";
	/**
	 * Atributo que almacena un objeto del tipo {@link SQLLenguaje}.
	 */
	private static SQLLenguaje lenguaje;
	
	/**
	 * M&eacute;todo que realiza la creaci&oacute;n de un DTO, del negocio, a partir del resultado de alguna consulta.
	 * @param <code>rst</code> objeto de tipo <code>Resultset</code>, que contiene los resultados de la consulta.
	 * @return <code>Object</code> - representa al DTO creado.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static Object crearDtoST(ResultSet rst)
			throws SQLException, ExcepcionEnDAO {
 	    BsCliente theDto = new BsCliente();
     	theDto.setCliente(SQLUtils.getLongFromResultSet(rst, "C1"));
		theDto.setNombre(rst.getString("C2"));
     	theDto.setModulos(SQLUtils.getLongFromResultSet(rst, "C3"));
		theDto.setDireccion(rst.getString("C4"));
		theDto.setTelefono(rst.getString("C5"));
		theDto.setFax(rst.getString("C6"));
		theDto.setEmail(rst.getString("C7"));
		theDto.setNombrecontacto(rst.getString("C8"));
		theDto.setApellidocontacto(rst.getString("C9"));
     	theDto.setNuminstalacion(SQLUtils.getLongFromResultSet(rst, "C10"));
		theDto.setFechaestatus(rst.getTimestamp("C11"));
     	theDto.setEstatus(SQLUtils.getLongFromResultSet(rst, "C12"));
		theDto.setFechacreacion(rst.getTimestamp("C13"));
     	theDto.setSolicitudpendiente(SQLUtils.getLongFromResultSet(rst, "C14"));
     	theDto.setNivelseguridadtex(SQLUtils.getLongFromResultSet(rst, "C15"));
     	theDto.setNivelseguridadPIMP(SQLUtils.getLongFromResultSet(rst, "C16"));
		theDto.setNombrerepresentante(rst.getString("C17"));
		theDto.setApellidorepresentante(rst.getString("C18"));
		theDto.setProveedorinternet(rst.getString("C19"));
     	theDto.setBanca(SQLUtils.getLongFromResultSet(rst, "C20"));
     	theDto.setPoliticacobrotex(SQLUtils.getLongFromResultSet(rst, "C21"));
		theDto.setComisiontransext(rst.getBigDecimal("C22"));
     	theDto.setCodigotablatransext(SQLUtils.getLongFromResultSet(rst, "C23"));
     	theDto.setNivelseguridadisss(SQLUtils.getLongFromResultSet(rst, "C24"));
     	theDto.setNivelseguridadafp(SQLUtils.getLongFromResultSet(rst, "C25"));
     	theDto.setNivelseguridadccre(SQLUtils.getLongFromResultSet(rst, "C26"));
     	theDto.setNivelseguridadppag(SQLUtils.getLongFromResultSet(rst, "C27"));
		theDto.setCargapersonalizable(rst.getString("C28"));
     	theDto.setTipodoctorepresentante(SQLUtils.getLongFromResultSet(rst, "C29"));
		theDto.setDoctorepresentante(rst.getString("C30"));
		theDto.setCiudad(rst.getString("C31"));
		theDto.setCargo(rst.getString("C32"));
		theDto.setLimitexarchivo(rst.getBigDecimal("C33"));
		theDto.setLimitexlote(rst.getBigDecimal("C34"));
		theDto.setLimitextransaccion(rst.getBigDecimal("C35"));
		theDto.setPermitedebitos(rst.getString("C36"));
		theDto.setLimitecreditos(rst.getBigDecimal("C37"));
		theDto.setLimitedebitos(rst.getBigDecimal("C38"));
		theDto.setComisiontrnach(rst.getBigDecimal("C39"));
		return theDto;
	}
	
	







	/**
	 * M&eacute;todo que coloca los valores provenientes del objeto Dto en el objeto PreparedStatement,
	 * el cual proviene de la llamada a la implementacion de {@link SQLOperaciones}, este m&eacute;todo
	 * no toma en cuenta el valor del <code>id</code>, puesto que se asume que es un valor secuencial
	 * autonumerico o serial propio de la base de datos.
	 * 
	 * @param <code>theDtoUntyped</code> objeto sin tipo que almacena al DTO del negocio.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaCrearST(Object theDtoUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {			
	        BsCliente theDto = (BsCliente)theDtoUntyped;
				String nombre = theDto.getNombre();     
		Long modulos = theDto.getModulos();     
		String direccion = theDto.getDireccion();     
		String telefono = theDto.getTelefono();     
		String fax = theDto.getFax();     
		String email = theDto.getEmail();     
		String nombrecontacto = theDto.getNombrecontacto();     
		String apellidocontacto = theDto.getApellidocontacto();     
		Long numinstalacion = theDto.getNuminstalacion();     
		java.util.Date fechaestatus = theDto.getFechaestatus();     
		Long estatus = theDto.getEstatus();     
		java.util.Date fechacreacion = theDto.getFechacreacion();     
		Long solicitudpendiente = theDto.getSolicitudpendiente();     
		Long nivelseguridadtex = theDto.getNivelseguridadtex();     
		Long nivelseguridadPIMP = theDto.getNivelseguridadPIMP();     
		String nombrerepresentante = theDto.getNombrerepresentante();     
		String apellidorepresentante = theDto.getApellidorepresentante();     
		String proveedorinternet = theDto.getProveedorinternet();     
		Long banca = theDto.getBanca();     
		Long politicacobrotex = theDto.getPoliticacobrotex();     
		java.math.BigDecimal comisiontransext = theDto.getComisiontransext();     
		Long codigotablatransext = theDto.getCodigotablatransext();     
		Long nivelseguridadisss = theDto.getNivelseguridadisss();     
		Long nivelseguridadafp = theDto.getNivelseguridadafp();     
		Long nivelseguridadccre = theDto.getNivelseguridadccre();     
		Long nivelseguridadppag = theDto.getNivelseguridadppag();     
		String cargapersonalizable = theDto.getCargapersonalizable();     
		Long tipodoctorepresentante = theDto.getTipodoctorepresentante();     
		String doctorepresentante = theDto.getDoctorepresentante();     
		String ciudad = theDto.getCiudad();     
		String cargo = theDto.getCargo();     
		java.math.BigDecimal limitexarchivo = theDto.getLimitexarchivo();     
		java.math.BigDecimal limitexlote = theDto.getLimitexlote();     
		java.math.BigDecimal limitextransaccion = theDto.getLimitextransaccion();     
		String permitedebitos = theDto.getPermitedebitos();     
		java.math.BigDecimal limitecreditos = theDto.getLimitecreditos();     
		java.math.BigDecimal limitedebitos = theDto.getLimitedebitos();     
		java.math.BigDecimal comisiontrnach = theDto.getComisiontrnach();     
		     
		SQLUtils.setIntoStatement( 1, nombre, smt);  
    		     
		SQLUtils.setIntoStatement( 2, modulos, smt);  
    		     
		SQLUtils.setIntoStatement( 3, direccion, smt);  
    		     
		SQLUtils.setIntoStatement( 4, telefono, smt);  
    		     
		SQLUtils.setIntoStatement( 5, fax, smt);  
    		     
		SQLUtils.setIntoStatement( 6, email, smt);  
    		     
		SQLUtils.setIntoStatement( 7, nombrecontacto, smt);  
    		     
		SQLUtils.setIntoStatement( 8, apellidocontacto, smt);  
    		     
		SQLUtils.setIntoStatement( 9, numinstalacion, smt);  
    		     
		SQLUtils.setIntoStatement( 10, fechaestatus, smt);  
    		     
		SQLUtils.setIntoStatement( 11, estatus, smt);  
    		     
		SQLUtils.setIntoStatement( 12, fechacreacion, smt);  
    		     
		SQLUtils.setIntoStatement( 13, solicitudpendiente, smt);  
    		     
		SQLUtils.setIntoStatement( 14, nivelseguridadtex, smt);  
    		     
		SQLUtils.setIntoStatement( 15, nivelseguridadPIMP, smt);  
    		     
		SQLUtils.setIntoStatement( 16, nombrerepresentante, smt);  
    		     
		SQLUtils.setIntoStatement( 17, apellidorepresentante, smt);  
    		     
		SQLUtils.setIntoStatement( 18, proveedorinternet, smt);  
    		     
		SQLUtils.setIntoStatement( 19, banca, smt);  
    		     
		SQLUtils.setIntoStatement( 20, politicacobrotex, smt);  
    		     
		SQLUtils.setIntoStatement( 21, comisiontransext, smt);  
    		     
		SQLUtils.setIntoStatement( 22, codigotablatransext, smt);  
    		     
		SQLUtils.setIntoStatement( 23, nivelseguridadisss, smt);  
    		     
		SQLUtils.setIntoStatement( 24, nivelseguridadafp, smt);  
    		     
		SQLUtils.setIntoStatement( 25, nivelseguridadccre, smt);  
    		     
		SQLUtils.setIntoStatement( 26, nivelseguridadppag, smt);  
    		     
		SQLUtils.setIntoStatement( 27, cargapersonalizable, smt);  
    		     
		SQLUtils.setIntoStatement( 28, tipodoctorepresentante, smt);  
    		     
		SQLUtils.setIntoStatement( 29, doctorepresentante, smt);  
    		     
		SQLUtils.setIntoStatement( 30, ciudad, smt);  
    		     
		SQLUtils.setIntoStatement( 31, cargo, smt);  
    		     
		SQLUtils.setIntoStatement( 32, limitexarchivo, smt);  
    		     
		SQLUtils.setIntoStatement( 33, limitexlote, smt);  
    		     
		SQLUtils.setIntoStatement( 34, limitextransaccion, smt);  
    		     
		SQLUtils.setIntoStatement( 35, permitedebitos, smt);  
    		     
		SQLUtils.setIntoStatement( 36, limitecreditos, smt);  
    		     
		SQLUtils.setIntoStatement( 37, limitedebitos, smt);  
    		     
		SQLUtils.setIntoStatement( 38, comisiontrnach, smt);  
    		
	}

	/**
	 * M&eacute;todo que coloca los valores provenientes del objeto Dto en el objeto PreparedStatement,
	 * el cual proviene de la llamada a la implementacion de {@link SQLOperaciones}, este m&eacute;todo
	 * toma en cuenta el <code>id</code>, lo que quiere decir que se usa al momento de  crear registros
	 * en donde el identificador no es un valor secuencial autonumerico o serial propio de la base de datos
	 *
	 * @param <code>theDtoUntyped</code> objeto sin tipo que almacena al DTO del negocio.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaCrearConIdST(Object theDtoUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {			
	        BsCliente theDto = (BsCliente)theDtoUntyped;
				Long cliente = theDto.getCliente();     
		String nombre = theDto.getNombre();     
		Long modulos = theDto.getModulos();     
		String direccion = theDto.getDireccion();     
		String telefono = theDto.getTelefono();     
		String fax = theDto.getFax();     
		String email = theDto.getEmail();     
		String nombrecontacto = theDto.getNombrecontacto();     
		String apellidocontacto = theDto.getApellidocontacto();     
		Long numinstalacion = theDto.getNuminstalacion();     
		java.util.Date fechaestatus = theDto.getFechaestatus();     
		Long estatus = theDto.getEstatus();     
		java.util.Date fechacreacion = theDto.getFechacreacion();     
		Long solicitudpendiente = theDto.getSolicitudpendiente();     
		Long nivelseguridadtex = theDto.getNivelseguridadtex();     
		Long nivelseguridadPIMP = theDto.getNivelseguridadPIMP();     
		String nombrerepresentante = theDto.getNombrerepresentante();     
		String apellidorepresentante = theDto.getApellidorepresentante();     
		String proveedorinternet = theDto.getProveedorinternet();     
		Long banca = theDto.getBanca();     
		Long politicacobrotex = theDto.getPoliticacobrotex();     
		java.math.BigDecimal comisiontransext = theDto.getComisiontransext();     
		Long codigotablatransext = theDto.getCodigotablatransext();     
		Long nivelseguridadisss = theDto.getNivelseguridadisss();     
		Long nivelseguridadafp = theDto.getNivelseguridadafp();     
		Long nivelseguridadccre = theDto.getNivelseguridadccre();     
		Long nivelseguridadppag = theDto.getNivelseguridadppag();     
		String cargapersonalizable = theDto.getCargapersonalizable();     
		Long tipodoctorepresentante = theDto.getTipodoctorepresentante();     
		String doctorepresentante = theDto.getDoctorepresentante();     
		String ciudad = theDto.getCiudad();     
		String cargo = theDto.getCargo();     
		java.math.BigDecimal limitexarchivo = theDto.getLimitexarchivo();     
		java.math.BigDecimal limitexlote = theDto.getLimitexlote();     
		java.math.BigDecimal limitextransaccion = theDto.getLimitextransaccion();     
		String permitedebitos = theDto.getPermitedebitos();     
		java.math.BigDecimal limitecreditos = theDto.getLimitecreditos();     
		java.math.BigDecimal limitedebitos = theDto.getLimitedebitos();     
		java.math.BigDecimal comisiontrnach = theDto.getComisiontrnach();     
	
		SQLUtils.setIntoStatement( 1, cliente, smt);
    	
		SQLUtils.setIntoStatement( 2, nombre, smt);
    	
		SQLUtils.setIntoStatement( 3, modulos, smt);
    	
		SQLUtils.setIntoStatement( 4, direccion, smt);
    	
		SQLUtils.setIntoStatement( 5, telefono, smt);
    	
		SQLUtils.setIntoStatement( 6, fax, smt);
    	
		SQLUtils.setIntoStatement( 7, email, smt);
    	
		SQLUtils.setIntoStatement( 8, nombrecontacto, smt);
    	
		SQLUtils.setIntoStatement( 9, apellidocontacto, smt);
    	
		SQLUtils.setIntoStatement( 10, numinstalacion, smt);
    	
		SQLUtils.setIntoStatement( 11, fechaestatus, smt);
    	
		SQLUtils.setIntoStatement( 12, estatus, smt);
    	
		SQLUtils.setIntoStatement( 13, fechacreacion, smt);
    	
		SQLUtils.setIntoStatement( 14, solicitudpendiente, smt);
    	
		SQLUtils.setIntoStatement( 15, nivelseguridadtex, smt);
    	
		SQLUtils.setIntoStatement( 16, nivelseguridadPIMP, smt);
    	
		SQLUtils.setIntoStatement( 17, nombrerepresentante, smt);
    	
		SQLUtils.setIntoStatement( 18, apellidorepresentante, smt);
    	
		SQLUtils.setIntoStatement( 19, proveedorinternet, smt);
    	
		SQLUtils.setIntoStatement( 20, banca, smt);
    	
		SQLUtils.setIntoStatement( 21, politicacobrotex, smt);
    	
		SQLUtils.setIntoStatement( 22, comisiontransext, smt);
    	
		SQLUtils.setIntoStatement( 23, codigotablatransext, smt);
    	
		SQLUtils.setIntoStatement( 24, nivelseguridadisss, smt);
    	
		SQLUtils.setIntoStatement( 25, nivelseguridadafp, smt);
    	
		SQLUtils.setIntoStatement( 26, nivelseguridadccre, smt);
    	
		SQLUtils.setIntoStatement( 27, nivelseguridadppag, smt);
    	
		SQLUtils.setIntoStatement( 28, cargapersonalizable, smt);
    	
		SQLUtils.setIntoStatement( 29, tipodoctorepresentante, smt);
    	
		SQLUtils.setIntoStatement( 30, doctorepresentante, smt);
    	
		SQLUtils.setIntoStatement( 31, ciudad, smt);
    	
		SQLUtils.setIntoStatement( 32, cargo, smt);
    	
		SQLUtils.setIntoStatement( 33, limitexarchivo, smt);
    	
		SQLUtils.setIntoStatement( 34, limitexlote, smt);
    	
		SQLUtils.setIntoStatement( 35, limitextransaccion, smt);
    	
		SQLUtils.setIntoStatement( 36, permitedebitos, smt);
    	
		SQLUtils.setIntoStatement( 37, limitecreditos, smt);
    	
		SQLUtils.setIntoStatement( 38, limitedebitos, smt);
    	
		SQLUtils.setIntoStatement( 39, comisiontrnach, smt);
    		
	}

	/**
	 * M&eacute;todo que coloca el identificador recibido en el objeto PreparedStatement, para poder ser eliminado.
	 * @param <code>theIdUntyped</code> objeto sin tipo que identifica al <code>id</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaEliminarST(Object theIdUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {
			Long theId = (Long)theIdUntyped;
		SQLUtils.setIntoStatement( 1, theId, smt);
	}

	/**
	 * M&eacute;todo que coloca el identificador recibido, en el objeto PreparedStatement para poder hacer la consulta de busqueda.
	 * @param <code>theIdUntyped</code> objeto sin tipo que identifica al <code>id</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaBuscarPorIdST(Object theIdUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {
			Long theId = (Long)theIdUntyped;
			SQLUtils.setIntoStatement( 1, theId, smt);
		}

	/**
	 * M&eacute;todo que coloca los valores provenientes del objeto Dto en el objeto PreparedStatement,
	 * el cual proviene de la llamada a la implementacion de {@link SQLOperaciones} para poder actualizar
     * un registro en espec&iacute;fico.
	 * 
	 * @param <code>theDtoUntyped</code> objeto sin tipo que almacena al DTO del negocio.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaActualizarST(Object theDtoUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {
			BsCliente theDto = (BsCliente)theDtoUntyped;
				Long cliente = theDto.getCliente();     
		String nombre = theDto.getNombre();     
		Long modulos = theDto.getModulos();     
		String direccion = theDto.getDireccion();     
		String telefono = theDto.getTelefono();     
		String fax = theDto.getFax();     
		String email = theDto.getEmail();     
		String nombrecontacto = theDto.getNombrecontacto();     
		String apellidocontacto = theDto.getApellidocontacto();     
		Long numinstalacion = theDto.getNuminstalacion();     
		java.util.Date fechaestatus = theDto.getFechaestatus();     
		Long estatus = theDto.getEstatus();     
		java.util.Date fechacreacion = theDto.getFechacreacion();     
		Long solicitudpendiente = theDto.getSolicitudpendiente();     
		Long nivelseguridadtex = theDto.getNivelseguridadtex();     
		Long nivelseguridadPIMP = theDto.getNivelseguridadPIMP();     
		String nombrerepresentante = theDto.getNombrerepresentante();     
		String apellidorepresentante = theDto.getApellidorepresentante();     
		String proveedorinternet = theDto.getProveedorinternet();     
		Long banca = theDto.getBanca();     
		Long politicacobrotex = theDto.getPoliticacobrotex();     
		java.math.BigDecimal comisiontransext = theDto.getComisiontransext();     
		Long codigotablatransext = theDto.getCodigotablatransext();     
		Long nivelseguridadisss = theDto.getNivelseguridadisss();     
		Long nivelseguridadafp = theDto.getNivelseguridadafp();     
		Long nivelseguridadccre = theDto.getNivelseguridadccre();     
		Long nivelseguridadppag = theDto.getNivelseguridadppag();     
		String cargapersonalizable = theDto.getCargapersonalizable();     
		Long tipodoctorepresentante = theDto.getTipodoctorepresentante();     
		String doctorepresentante = theDto.getDoctorepresentante();     
		String ciudad = theDto.getCiudad();     
		String cargo = theDto.getCargo();     
		java.math.BigDecimal limitexarchivo = theDto.getLimitexarchivo();     
		java.math.BigDecimal limitexlote = theDto.getLimitexlote();     
		java.math.BigDecimal limitextransaccion = theDto.getLimitextransaccion();     
		String permitedebitos = theDto.getPermitedebitos();     
		java.math.BigDecimal limitecreditos = theDto.getLimitecreditos();     
		java.math.BigDecimal limitedebitos = theDto.getLimitedebitos();     
		java.math.BigDecimal comisiontrnach = theDto.getComisiontrnach();     
		     
		SQLUtils.setIntoStatement( 1, nombre, smt);  
        	     
		SQLUtils.setIntoStatement( 2, modulos, smt);  
        	     
		SQLUtils.setIntoStatement( 3, direccion, smt);  
        	     
		SQLUtils.setIntoStatement( 4, telefono, smt);  
        	     
		SQLUtils.setIntoStatement( 5, fax, smt);  
        	     
		SQLUtils.setIntoStatement( 6, email, smt);  
        	     
		SQLUtils.setIntoStatement( 7, nombrecontacto, smt);  
        	     
		SQLUtils.setIntoStatement( 8, apellidocontacto, smt);  
        	     
		SQLUtils.setIntoStatement( 9, numinstalacion, smt);  
        	     
		SQLUtils.setIntoStatement( 10, fechaestatus, smt);  
        	     
		SQLUtils.setIntoStatement( 11, estatus, smt);  
        	     
		SQLUtils.setIntoStatement( 12, fechacreacion, smt);  
        	     
		SQLUtils.setIntoStatement( 13, solicitudpendiente, smt);  
        	     
		SQLUtils.setIntoStatement( 14, nivelseguridadtex, smt);  
        	     
		SQLUtils.setIntoStatement( 15, nivelseguridadPIMP, smt);  
        	     
		SQLUtils.setIntoStatement( 16, nombrerepresentante, smt);  
        	     
		SQLUtils.setIntoStatement( 17, apellidorepresentante, smt);  
        	     
		SQLUtils.setIntoStatement( 18, proveedorinternet, smt);  
        	     
		SQLUtils.setIntoStatement( 19, banca, smt);  
        	     
		SQLUtils.setIntoStatement( 20, politicacobrotex, smt);  
        	     
		SQLUtils.setIntoStatement( 21, comisiontransext, smt);  
        	     
		SQLUtils.setIntoStatement( 22, codigotablatransext, smt);  
        	     
		SQLUtils.setIntoStatement( 23, nivelseguridadisss, smt);  
        	     
		SQLUtils.setIntoStatement( 24, nivelseguridadafp, smt);  
        	     
		SQLUtils.setIntoStatement( 25, nivelseguridadccre, smt);  
        	     
		SQLUtils.setIntoStatement( 26, nivelseguridadppag, smt);  
        	     
		SQLUtils.setIntoStatement( 27, cargapersonalizable, smt);  
        	     
		SQLUtils.setIntoStatement( 28, tipodoctorepresentante, smt);  
        	     
		SQLUtils.setIntoStatement( 29, doctorepresentante, smt);  
        	     
		SQLUtils.setIntoStatement( 30, ciudad, smt);  
        	     
		SQLUtils.setIntoStatement( 31, cargo, smt);  
        	     
		SQLUtils.setIntoStatement( 32, limitexarchivo, smt);  
        	     
		SQLUtils.setIntoStatement( 33, limitexlote, smt);  
        	     
		SQLUtils.setIntoStatement( 34, limitextransaccion, smt);  
        	     
		SQLUtils.setIntoStatement( 35, permitedebitos, smt);  
        	     
		SQLUtils.setIntoStatement( 36, limitecreditos, smt);  
        	     
		SQLUtils.setIntoStatement( 37, limitedebitos, smt);  
        	     
		SQLUtils.setIntoStatement( 38, comisiontrnach, smt);  
        		     
		SQLUtils.setIntoStatement( 39, cliente, smt);  
    			
	}


	/**
	 * M&eacute;todo que ejecuta una petici&oacute;n de datos a trav&eacute;s de un objeto {@link PreparedStatement},
	 * y un objeto del tipo {@link PeticionDeDatos}, el cual en su implementaci&oacute;n define una serie de restricciones
	 * usadas para hacer la petici&oacute;n.
	 *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaPeticionDeDatosST(PeticionDeDatos peticion,
			PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
		SQLImplementacionOperacionesComunes.operacionesParaPeticionDeDatosST( peticion, smt, SQL_OPERACIONES, null);	
	}	
	
	/**
	 * M&eacute;todo que ejecuta una petici&oacute;n de datos a trav&eacute;s de un objeto {@link PreparedStatement},
     * y un objeto del tipo {@link PeticionDeDatos}, el cual en su implementaci&oacute;n define una serie de restricciones
     * usadas para hacer la petici&oacute;n.
	 *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaPeticionDeDatosFST(PeticionDeDatos peticion,
			PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
		SQLImplementacionOperacionesComunes.operacionesParaPeticionDeDatosST( peticion, smt, SQL_OPERACIONES, null);	
	}	

	/**
	 * M&eacute;todo que ejecuta una petici&oacute;n de conteo a trav&eacute;s de un objeto {@link PreparedStatement},
     * y un objeto del tipo {@link PeticionDeDatos}, el cual en su implementaci&oacute;n define una serie de restricciones
     * usadas para hacer la petici&oacute;n.
	 *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaConteoST(PeticionDeDatos peticion,
			PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
		SQLImplementacionOperacionesComunes.operacionesParaPeticionDeDatosST( peticion, smt, SQL_OPERACIONES, null);
	}

	/**
	 * M&eacute;todo que agrega al atributo {@link #SELECT_HEADER_SQL} las condiciones de b&uacute;squeda
     * que vienen en el objeto del tipo {@link PeticionDeDatos}
     *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>, que contiene las condiciones de la consulta.
	 * @return <code>String</code> - la consulta condicionada como cadena de caracteres.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static String getSqlParaPeticionDeDatosST(PeticionDeDatos peticion)
			throws SQLException, ExcepcionEnDAO {
		return SELECT_HEADER_SQL + SQLImplementacionOperacionesComunes.crearCondicionesSQLST( peticion, SQL_OPERACIONES, SQLImplementacionOperacionesComunes.COND_ALIAS_ORDEN_WHERE);
	}
	
	
	
	

	/**
	 * M&eacute;todo que agrega al atributo {@link #COUNT_HEADER_SQL} las condiciones de conteo
	 * que vienen en el objeto de tipo {@link PeticionDeDatos}.
	 *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>, que contiene las condiciones de la consulta.
	 * @return <code>String</code> - la consulta condicionada como cadena de caracteres.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static String getSqlDeConteoST(PeticionDeDatos peticion)
			throws SQLException, ExcepcionEnDAO {
		return COUNT_HEADER_SQL + SQLImplementacionOperacionesComunes.crearCondicionesSQLST( peticion, SQL_OPERACIONES, SQLImplementacionOperacionesComunes.COND_SQL_WHERE);
	}
	
	/**
	 * M&eacute;todo que se utiliza para actualizar un registro a nivel de tabla, recibe como par&aacute;metro un DTO del negocio
	 * que contiene la informaci&oacute;n a actualizar, e invoca la funci&oacute;n <code>actualizar</code> de la clase de la que hereda.
	 *
	 * @param <code>bsCliente</code> objeto del tipo <code>BsCliente</code>.
	 * @throws ExcepcionEnDAO
	 * @see {@link SQLImplementacionBasica}
	 */
	public void actualizar(BsCliente bsCliente) throws ExcepcionEnDAO {
	    actualizar(bsCliente, AUTO_COMMIT);
	
	}
	
	/**
	 * M&eacute;todo utilizado para realizar una b&uacute;squeda de un registro espec&iacute;fico, utilizando un identificador.
	 *
	 * @param <code>clienteId</code> objeto del tipo <code>Long</code>.
	 * @return <code>BsCliente</code> - un objeto DTO.
	 * @throws ExcepcionEnDAO
	 * @see {@link SQLImplementacionBasica}
	 */
	public BsCliente buscarPorID(Long clienteId)
			throws ExcepcionEnDAO {
		return (BsCliente)buscarPorID(clienteId, AUTO_COMMIT);				 
	}

	/**
	 * M&eacute;todo utilizado para la creaci&oacute;n de un registro, a nivel de tabla, a partir de la informaci&oacute;n
	 * recibida en un DTO del objeto de Negocio, e invoca la funci&oacute;n <code>crear</code> de la clase de la que hereda.
	 * 
	 * @param <code>bsCliente</code> objeto del tipo <code>BsCliente</code>.
	 * @throws ExcepcionEnDAO
	 * @see {@link SQLImplementacionBasica}
	 */
	public void crear(BsCliente bsCliente) throws ExcepcionEnDAO {
	    crear(bsCliente, AUTO_COMMIT);		
	}

	/**
	 * M&eacute;todo utilizado para la eliminaci&oacute;n de un registro, a nivel de tabla,
	 * que se busca a trav&eacute;s del identificador recibido. Se invoca la funci&oacute;n <code>eliminar</code> de la clase de la que hereda.
	 *
	 * @param <code>clienteId</code> objeto del tipo <code>Long</code>.
	 * @throws ExcepcionEnDAO
	 * @see {@link SQLImplementacionBasica}
	 */
	public void eliminar(Long clienteId) throws ExcepcionEnDAO {
	    eliminar(clienteId, AUTO_COMMIT);		
	}

	/**
	 * M&eacute;todo utilizado para devolver un &uacute;nico resultado de la petici&oacute;n recibida,
	 * la cual se realiza a trav&eacuate;s del objeto de tipo {@link PeticionDeDatos}.
	 * 
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>, que contiene las condiciones de la consulta.
	 * @return <code>BsCliente</code> - un objeto DTO.
	 * @throws ExcepcionEnDAO
	 * @throws ExcepcionPorDatoNoEncontrado
	 * @see {@link SQLImplementacionBasica}
	 */
	public BsCliente resultadoUnico(PeticionDeDatos peticion)
			throws ExcepcionEnDAO, ExcepcionPorDatoNoEncontrado {
		return (BsCliente)resultadoUnico(peticion, AUTO_COMMIT);
	}
	
	/**
	 * Atributo del tipo {@link SQLOperaciones}, almacena la operaciones disponibles para esta implementaci&oacute;n.<br/>
	 * Algunas de las llamadas a los m&eacute;todos se realiza a trav&eacute;s de la invocaci&oacute;n de estas implementaciones.
	 */
    public static final SQLOperaciones	SQL_OPERACIONES =  new SQLOperaciones() {
			
			public int getManejoForaneosPorDefecto() {
				return Dao.SIN_FORANEOS;
			}
			
			
			public void operacionesParaPeticionDeDatosF(PeticionDeDatos request,
					PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
				BsClienteDaoJdbcSql.operacionesParaPeticionDeDatosFST(request, smt);
			}
			
			public void operacionesParaPeticionDeDatos(PeticionDeDatos request,
					PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
				BsClienteDaoJdbcSql.operacionesParaPeticionDeDatosST(request, smt);
			}
			
			public void operacionesParaEliminar(Object theId, PreparedStatement smt)
					throws SQLException, ExcepcionEnDAO {
				BsClienteDaoJdbcSql.operacionesParaEliminarST(theId, smt);
			}
			
			public void operacionesParaCrear(Object theDto, PreparedStatement smt)
					throws SQLException, ExcepcionEnDAO {
				BsClienteDaoJdbcSql.operacionesParaCrearST(theDto, smt);
			}
			
			public void operacionesParaCrearConId(Object theDto, PreparedStatement smt) 
			        throws SQLException, ExcepcionEnDAO {
			    BsClienteDaoJdbcSql.operacionesParaCrearConIdST(theDto, smt);
            }
			
			public void operacionesParaConteo(PeticionDeDatos request,
					PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
				BsClienteDaoJdbcSql.operacionesParaConteoST(request, smt);				
			}
			
			public void operacionesParaBuscarPorId(Object theId, PreparedStatement smt)
					throws SQLException, ExcepcionEnDAO {
				BsClienteDaoJdbcSql.operacionesParaBuscarPorIdST(theId, smt);
			}
			
			public void operacionesParaActualizar(Object theDto, PreparedStatement smt)
					throws SQLException, ExcepcionEnDAO {
				BsClienteDaoJdbcSql.operacionesParaActualizarST(theDto, smt);
				
			}
			
			public String getSqlParaPeticionDeDatosF(PeticionDeDatos request)
					throws SQLException, ExcepcionEnDAO {	
				return null;
			}
			
			public String getSqlParaPeticionDeDatos(PeticionDeDatos request)
					throws SQLException, ExcepcionEnDAO {
				return BsClienteDaoJdbcSql.getSqlParaPeticionDeDatosST(request);
			}
			
			public String getSqlDeConteo(PeticionDeDatos request) throws SQLException,
					ExcepcionEnDAO {
				return BsClienteDaoJdbcSql.getSqlDeConteoST(request);
			}
			
			public SQLEntidadMeta getSQLEntidadMeta() throws ExcepcionEnDAO {
				return BsClienteDaoJdbcSql.SQL_ENTIDAD_META;
			}
			
			public String getEliminarSql() {
				return  BsClienteDaoJdbcSql.DELETE_SQL;
			}
			
			public String getCrearSql() {
				return  BsClienteDaoJdbcSql.CREATE_SQL;
			}
			
			public String getCrearSqlConId(){
			    return  BsClienteDaoJdbcSql.CREATE_SQL_CON_ID;
			}			
			
			public String getBuscarPorIdSqlF() {
				return null;
			}
			
			public String getBuscarPorIdSql() {				
				return BsClienteDaoJdbcSql.FIND_BY_ID_SQL;
			}
			
			public String getActualizarSql() {				
				return BsClienteDaoJdbcSql.UPDATE_SQL;
			}
			
			public Object crearDto(ResultSet rst) throws SQLException, ExcepcionEnDAO {
				return BsClienteDaoJdbcSql.crearDtoST(rst);
			}
	        
	        public Object crearDataConValoresForaneos(ResultSet rst)
					throws SQLException, ExcepcionEnDAO {
				return null;
			}
			
	        public Object crearDataConValoresForaneosNoRel(ResultSet rst, Connection conn)
					throws SQLException, ExcepcionEnDAO {
				return null;
			}
			
			
            public boolean isForaneosDisponible(){
				return false;
            
            }
            
            public SQLLenguaje getSQLLenguaje() {
                return BsClienteDaoJdbcSql.getLenguaje();
            }
            
            public void setIdEnDto(Object theDto, Object dtoId) {
                setIdEnDtoST(theDto, dtoId);
            }
	};
	
	
	
	
	
	/**
	 * Devuelve el atributo {@link #SQL_OPERACIONES}.
	 * @return <code>SQLOperaciones</code> - objeto del tipo {@link SQLOperaciones}
	 */
    public SQLOperaciones getSqlOperaciones() {
       return SQL_OPERACIONES;
	}
	
	/**
	 * Devuelve el atributo {@link #lenguaje}
	 * @return <code>SQLLenguaje</code> - objeto del tipo {@link SQLLenguaje}
	 */	
	public static SQLLenguaje getLenguaje() {
		return lenguaje;
	}
	
	/**
	 * M&eacute;todo que permite obtener a trav&eacute;s de un Servicio la implementaci&oacute;n del objeto de Lenguaje,
	 * y coloca las reglas de negocio almacenadas en el atributo {@link #SQL_ENTIDAD_META}.
	 * @param <code>localizadorLenguaje</code> objeto del tipo {@link LocalizadorDeServicio} que posee informaci&oacute;n para trabajar con la base de datos.
	 */
	public static void setLocalizadorLenguaje(LocalizadorDeServicio localizadorLenguaje) {
		lenguaje = (SQLLenguaje) localizadorLenguaje.getServicio();
		lenguaje.setSqlEntidadMeta(SQL_ENTIDAD_META);
	}

	/**
	 * M&eacute;todo que agrega un identificador a un DTO del tipo <code>BsCliente</code>.
	 * @param <code>theDtoUntyped</code> objeto gen&eacute;rico, que representa a un <code>DTO</code>.
	 * @param <code>dtoIdUntyped</code> objeto gen&eacute;rico, que representa al identificador.
	 */
	public static void setIdEnDtoST(Object theDtoUntyped, Object dtoIdUntyped){
	    BsCliente dto = (BsCliente)theDtoUntyped;
        Long dtoId = (Long) dtoIdUntyped;;
        dto.setCliente(dtoId);

	}	
	
	
}
