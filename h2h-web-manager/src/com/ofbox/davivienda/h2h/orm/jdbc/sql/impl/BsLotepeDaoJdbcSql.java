package com.ofbox.davivienda.h2h.orm.jdbc.sql.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.sql.Connection;
import java.util.Collection;

import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Dao;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.ExcepcionEnDAO;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.ExcepcionPorDatoNoEncontrado;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.PeticionDeDatos;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Transaccion;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.foraneos.DataConValoresForaneos;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLColumnaMeta;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLEntidadMeta;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLImplementacionBasica;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLImplementacionOperacionesComunes;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLLenguaje;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLOperaciones;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLUtils;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.TransaccionJdbc;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dependencias.LocalizadorDeServicio;


import com.ofbox.davivienda.h2h.abstraccion.dto.BsLotepe;
import com.ofbox.davivienda.h2h.abstraccion.dto.BsLotepeID;
import com.ofbox.davivienda.h2h.abstraccion.dao.BsLotepeDao;

import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.impl.DataConValoresForaneosImpl;   
import com.ofbox.davivienda.h2h.abstraccion.dto.BsConvenio;


import com.ofbox.davivienda.h2h.orm.jdbc.sql.impl.BsConvenioDaoJdbcSql;
import com.ofbox.davivienda.h2h.abstraccion.dao.BsConvenioDao;

/**
 * Clase que hereda comportamientos de {@link SQLImplementacionBasica} e implementa el comportamiento definido en la capa DAO.<br/>
 * Esta clase es una implementaci&oacute;n intermedia para el DAO de <b>BS Lotepe</b>
 * asociado a la tabla <b>BS_LotePE</b>, en esta clase se construyen todos los sql para las operaciones de listado,
 * creaci&oacute;n, consulta, actualizaci&oacute;n y elminaci&oacute;n.<br/>
 * @author Systems Out of the Box
 * @version f3.1
 *
 */
public class BsLotepeDaoJdbcSql extends SQLImplementacionBasica implements BsLotepeDao{

	/**
	 * Atributo del tipo {@link SQLEntidadMeta}, que almacena la descripci&oacute;n del negocio como una entidad.<br/>
	 * Entre sus caracter&iacute;sticas tiene el nombre de la tabla, nombre de la secuencia, posici&oacute;n de las columnas,
	 * nombre de las columnas y tipos de datos para la columna.<br/>
	 * El arreglo que recibe son objetos de tipo {@link SQLColumnaMeta}, y definen informaci&oacute;n para obtener las columnas.
	 */
	public static final SQLEntidadMeta SQL_ENTIDAD_META = new SQLEntidadMeta("BS_LotePE", new SQLColumnaMeta[]{
        new SQLColumnaMeta(10, "BS_LotePE", "E10", 10001, "Instalacion", "C10001", Types.BIGINT ,true)
     ,  new SQLColumnaMeta(10, "BS_LotePE", "E10", 10002, "Lote", "C10002", Types.BIGINT ,true)
     ,  new SQLColumnaMeta(10, "BS_LotePE", "E10", 10030, "TipoConvenio", "C10030", Types.BIGINT )
     ,  new SQLColumnaMeta(10, "BS_LotePE", "E10", 10003, "Convenio", "C10003", Types.BIGINT )
     ,  new SQLColumnaMeta(10, "BS_LotePE", "E10", 10004, "EmpresaODepto", "C10004", Types.BIGINT )
     ,  new SQLColumnaMeta(10, "BS_LotePE", "E10", 10005, "TipoLote", "C10005", Types.BIGINT )
     ,  new SQLColumnaMeta(10, "BS_LotePE", "E10", 10006, "MontoTotal", "C10006", Types.NUMERIC )
     ,  new SQLColumnaMeta(10, "BS_LotePE", "E10", 10007, "NumOperaciones", "C10007", Types.BIGINT )
     ,  new SQLColumnaMeta(10, "BS_LotePE", "E10", 10008, "NombreLote", "C10008", Types.VARCHAR )
     ,  new SQLColumnaMeta(10, "BS_LotePE", "E10", 10009, "FechaEnviado", "C10009", Types.TIMESTAMP )
     ,  new SQLColumnaMeta(10, "BS_LotePE", "E10", 10010, "FechaRecibido", "C10010", Types.TIMESTAMP )
     ,  new SQLColumnaMeta(10, "BS_LotePE", "E10", 10011, "FechaAplicacion", "C10011", Types.TIMESTAMP )
     ,  new SQLColumnaMeta(10, "BS_LotePE", "E10", 10012, "FechaEstatus", "C10012", Types.TIMESTAMP )
     ,  new SQLColumnaMeta(10, "BS_LotePE", "E10", 10013, "Estatus", "C10013", Types.BIGINT )
     ,  new SQLColumnaMeta(10, "BS_LotePE", "E10", 10014, "NumeroReintento", "C10014", Types.BIGINT )
     ,  new SQLColumnaMeta(10, "BS_LotePE", "E10", 10015, "UsuarioIngreso", "C10015", Types.VARCHAR )
     ,  new SQLColumnaMeta(10, "BS_LotePE", "E10", 10016, "Autorizaciones", "C10016", Types.BIGINT )
     ,  new SQLColumnaMeta(10, "BS_LotePE", "E10", 10017, "AplicacionDebitoHost", "C10017", Types.BIGINT )
     ,  new SQLColumnaMeta(10, "BS_LotePE", "E10", 10018, "Sys_MarcaTiempo", "C10018", Types.TIMESTAMP )
     ,  new SQLColumnaMeta(10, "BS_LotePE", "E10", 10019, "Sys_ProcessId", "C10019", Types.BIGINT )
     ,  new SQLColumnaMeta(10, "BS_LotePE", "E10", 10020, "Sys_LowDateTime", "C10020", Types.BIGINT )
     ,  new SQLColumnaMeta(10, "BS_LotePE", "E10", 10021, "Sys_HighDateTime", "C10021", Types.BIGINT )
     ,  new SQLColumnaMeta(10, "BS_LotePE", "E10", 10022, "Ponderacion", "C10022", Types.BIGINT )
     ,  new SQLColumnaMeta(10, "BS_LotePE", "E10", 10023, "Firmas", "C10023", Types.BIGINT )
     ,  new SQLColumnaMeta(10, "BS_LotePE", "E10", 10024, "FechaEnvioHost", "C10024", Types.TIMESTAMP )
     ,  new SQLColumnaMeta(10, "BS_LotePE", "E10", 10025, "ListoParaHost", "C10025", Types.TIMESTAMP )
     ,  new SQLColumnaMeta(10, "BS_LotePE", "E10", 10026, "FechaHoraProcesado", "C10026", Types.TIMESTAMP )
     ,  new SQLColumnaMeta(10, "BS_LotePE", "E10", 10027, "Token", "C10027", Types.BIGINT )
     ,  new SQLColumnaMeta(10, "BS_LotePE", "E10", 10028, "ComentarioRechazo", "C10028", Types.VARCHAR )
     ,  new SQLColumnaMeta(10, "BS_LotePE", "E10", 10029, "MontoImpuesto", "C10029", Types.NUMERIC )
     ,  new SQLColumnaMeta(1, "BS_Convenio", "E1", 1001, "TipoConvenio", "C1001", Types.BIGINT)
     ,  new SQLColumnaMeta(1, "BS_Convenio", "E1", 1002, "Convenio", "C1002", Types.BIGINT)
     ,  new SQLColumnaMeta(1, "BS_Convenio", "E1", 1003, "Cliente", "C1003", Types.BIGINT)
     ,  new SQLColumnaMeta(1, "BS_Convenio", "E1", 1004, "Nombre", "C1004", Types.VARCHAR)
     ,  new SQLColumnaMeta(1, "BS_Convenio", "E1", 1005, "Cuenta", "C1005", Types.VARCHAR)
     ,  new SQLColumnaMeta(1, "BS_Convenio", "E1", 1006, "NombreCta", "C1006", Types.VARCHAR)
     ,  new SQLColumnaMeta(1, "BS_Convenio", "E1", 1007, "TipoMoneda", "C1007", Types.BIGINT)
     ,  new SQLColumnaMeta(1, "BS_Convenio", "E1", 1008, "Categoria", "C1008", Types.BIGINT)
     ,  new SQLColumnaMeta(1, "BS_Convenio", "E1", 1009, "Descripcion", "C1009", Types.VARCHAR)
     ,  new SQLColumnaMeta(1, "BS_Convenio", "E1", 1010, "DiaAplicacion", "C1010", Types.BIGINT)
     ,  new SQLColumnaMeta(1, "BS_Convenio", "E1", 1011, "NumReintentos", "C1011", Types.BIGINT)
     ,  new SQLColumnaMeta(1, "BS_Convenio", "E1", 1012, "PolParticipante", "C1012", Types.BIGINT)
     ,  new SQLColumnaMeta(1, "BS_Convenio", "E1", 1013, "PolDiaNoExistente", "C1013", Types.BIGINT)
     ,  new SQLColumnaMeta(1, "BS_Convenio", "E1", 1014, "Estatus", "C1014", Types.BIGINT)
     ,  new SQLColumnaMeta(1, "BS_Convenio", "E1", 1015, "FechaEstatus", "C1015", Types.TIMESTAMP)
     ,  new SQLColumnaMeta(1, "BS_Convenio", "E1", 1016, "IntervaloReintento", "C1016", Types.BIGINT)
     ,  new SQLColumnaMeta(1, "BS_Convenio", "E1", 1017, "PolMontoFijo", "C1017", Types.BIGINT)
     ,  new SQLColumnaMeta(1, "BS_Convenio", "E1", 1018, "MontoFijo", "C1018", Types.NUMERIC)
     ,  new SQLColumnaMeta(1, "BS_Convenio", "E1", 1019, "IntervaloAplica", "C1019", Types.BIGINT)
     ,  new SQLColumnaMeta(1, "BS_Convenio", "E1", 1020, "CreditosParciales", "C1020", Types.BIGINT)
     ,  new SQLColumnaMeta(1, "BS_Convenio", "E1", 1021, "AutorizacionWeb", "C1021", Types.BIGINT)
     ,  new SQLColumnaMeta(1, "BS_Convenio", "E1", 1022, "Clasificacion", "C1022", Types.BIGINT)
     ,  new SQLColumnaMeta(1, "BS_Convenio", "E1", 1023, "ComisionXOperacion", "C1023", Types.NUMERIC)
     ,  new SQLColumnaMeta(1, "BS_Convenio", "E1", 1024, "ComisionXOperacionPP", "C1024", Types.NUMERIC)
     ,  new SQLColumnaMeta(1, "BS_Convenio", "E1", 1025, "MultiplesCuentas", "C1025", Types.VARCHAR)
	});
	
    
	public BsConvenio getBsConvenio(BsLotepe bsLotepe)throws ExcepcionEnDAO{
		return getBsConvenio(bsLotepe, null);
	}

    public BsConvenio getBsConvenio(BsLotepe bsLotepe,Transaccion tx)throws ExcepcionEnDAO{
         	     
	     BsConvenio bsConvenioRel = bsLotepe.getBsConvenio();
	     if(bsConvenioRel==null){
                PeticionDeDatos peticionDeDatosParaBsConvenio = SQLImplementacionBasica.crearPeticionDeDatosST();
                peticionDeDatosParaBsConvenio.agregarRestriccion(PeticionDeDatos.IGUAL,
	     	                              BsConvenioDao.TIPOCONVENIO, 
	     	                              bsLotepe.getTipoconvenio()
	     	                              );
	            TransaccionJdbc tjdbc = (TransaccionJdbc)tx;
	     		bsConvenioRel = (BsConvenio)SQLImplementacionOperacionesComunes.resultadoUnicoST(
							            peticionDeDatosParaBsConvenio, 
										tjdbc!=null? tjdbc.getConn(): getConnection(), 
										BsConvenioDaoJdbcSql.SQL_OPERACIONES, 
										tjdbc==null, 
										getLog());
				bsLotepe.setBsConvenio(bsConvenioRel);										
		}
										
		return 	bsConvenioRel;									
    }
    

	

	/**
	 * Atributo que almacena el <code>String</code> para obtener el listado de registros de la tabla principal
	 * junto con otras tablas foraneas, realiza un JOIN.
	 */
	public static final String SELECT_HEADER_SQL_F =  
		"SELECT E10.Instalacion AS C10001 , E10.Lote AS C10002 , E10.TipoConvenio AS C10030 , E10.Convenio AS C10003 , " +
		"       E10.EmpresaODepto AS C10004 , E10.TipoLote AS C10005 , E10.MontoTotal AS C10006 , E10.NumOperaciones AS C10007 , " +
		"       E10.NombreLote AS C10008 , E10.FechaEnviado AS C10009 , E10.FechaRecibido AS C10010 , E10.FechaAplicacion AS C10011 , " +
		"       E10.FechaEstatus AS C10012 , E10.Estatus AS C10013 , E10.NumeroReintento AS C10014 , E10.UsuarioIngreso AS C10015 , " +
		"       E10.Autorizaciones AS C10016 , E10.AplicacionDebitoHost AS C10017 , E10.Sys_MarcaTiempo AS C10018 , E10.Sys_ProcessId AS C10019 , " +
		"       E10.Sys_LowDateTime AS C10020 , E10.Sys_HighDateTime AS C10021 , E10.Ponderacion AS C10022 , E10.Firmas AS C10023 , " +
		"       E10.FechaEnvioHost AS C10024 , E10.ListoParaHost AS C10025 , E10.FechaHoraProcesado AS C10026 , E10.Token AS C10027 , " +
		"       E10.ComentarioRechazo AS C10028 , E10.MontoImpuesto AS C10029 , " +
		"       E1.TipoConvenio AS C1001 , E1.Convenio AS C1002 , E1.Cliente AS C1003 , E1.Nombre AS C1004 , " +
		"       E1.Cuenta AS C1005 , E1.NombreCta AS C1006 , E1.TipoMoneda AS C1007 , E1.Categoria AS C1008 , " +
		"       E1.Descripcion AS C1009 , E1.DiaAplicacion AS C1010 , E1.NumReintentos AS C1011 , E1.PolParticipante AS C1012 , " +
		"       E1.PolDiaNoExistente AS C1013 , E1.Estatus AS C1014 , E1.FechaEstatus AS C1015 , E1.IntervaloReintento AS C1016 , " +
		"       E1.PolMontoFijo AS C1017 , E1.MontoFijo AS C1018 , E1.IntervaloAplica AS C1019 , E1.CreditosParciales AS C1020 , " +
		"       E1.AutorizacionWeb AS C1021 , E1.Clasificacion AS C1022 , E1.ComisionXOperacion AS C1023 , E1.ComisionXOperacionPP AS C1024 , " +
		"       E1.MultiplesCuentas AS C1025" +
		"  FROM BS_LotePE E10 , BS_Convenio E1" +
		" WHERE E1.TipoConvenio = E10.TipoConvenio";
	/**
	 * Atributo que almacena el <code>String</code> para obtener el listado de registros de la tabla principal
     * junto con otras tablas foraneas, realiza un JOIN utilizando la <code>primary key</code> de la tabla principal.
	 */
	public static final String FIND_BY_ID_SQL_F = 
		"SELECT E10.Instalacion AS C10001 , E10.Lote AS C10002 , E10.TipoConvenio AS C10030 , E10.Convenio AS C10003 , " +
		"       E10.EmpresaODepto AS C10004 , E10.TipoLote AS C10005 , E10.MontoTotal AS C10006 , E10.NumOperaciones AS C10007 , " +
		"       E10.NombreLote AS C10008 , E10.FechaEnviado AS C10009 , E10.FechaRecibido AS C10010 , E10.FechaAplicacion AS C10011 , " +
		"       E10.FechaEstatus AS C10012 , E10.Estatus AS C10013 , E10.NumeroReintento AS C10014 , E10.UsuarioIngreso AS C10015 , " +
		"       E10.Autorizaciones AS C10016 , E10.AplicacionDebitoHost AS C10017 , E10.Sys_MarcaTiempo AS C10018 , E10.Sys_ProcessId AS C10019 , " +
		"       E10.Sys_LowDateTime AS C10020 , E10.Sys_HighDateTime AS C10021 , E10.Ponderacion AS C10022 , E10.Firmas AS C10023 , " +
		"       E10.FechaEnvioHost AS C10024 , E10.ListoParaHost AS C10025 , E10.FechaHoraProcesado AS C10026 , E10.Token AS C10027 , " +
		"       E10.ComentarioRechazo AS C10028 , E10.MontoImpuesto AS C10029 , " +
		"       E1.TipoConvenio AS C1001 , E1.Convenio AS C1002 , E1.Cliente AS C1003 , E1.Nombre AS C1004 , " +
		"       E1.Cuenta AS C1005 , E1.NombreCta AS C1006 , E1.TipoMoneda AS C1007 , E1.Categoria AS C1008 , " +
		"       E1.Descripcion AS C1009 , E1.DiaAplicacion AS C1010 , E1.NumReintentos AS C1011 , E1.PolParticipante AS C1012 , " +
		"       E1.PolDiaNoExistente AS C1013 , E1.Estatus AS C1014 , E1.FechaEstatus AS C1015 , E1.IntervaloReintento AS C1016 , " +
		"       E1.PolMontoFijo AS C1017 , E1.MontoFijo AS C1018 , E1.IntervaloAplica AS C1019 , E1.CreditosParciales AS C1020 , " +
		"       E1.AutorizacionWeb AS C1021 , E1.Clasificacion AS C1022 , E1.ComisionXOperacion AS C1023 , E1.ComisionXOperacionPP AS C1024 , " +
		"       E1.MultiplesCuentas AS C1025" +
		"  FROM BS_LotePE E10 , BS_Convenio E1" +
		" WHERE E1.TipoConvenio = E10.TipoConvenio" +
		"   AND E10.Instalacion= ? " +
		"   AND E10.Lote= ? ";
	

	/**
	 * Atributo que almacena el <code>String</code> para obtener el listado de registros del negocio.
	 */
	public static final String SELECT_HEADER_SQL =  
		"SELECT E10.Instalacion AS C10001 , E10.Lote AS C10002 , E10.TipoConvenio AS C10030 , E10.Convenio AS C10003 , " +
		"       E10.EmpresaODepto AS C10004 , E10.TipoLote AS C10005 , E10.MontoTotal AS C10006 , E10.NumOperaciones AS C10007 , " +
		"       E10.NombreLote AS C10008 , E10.FechaEnviado AS C10009 , E10.FechaRecibido AS C10010 , E10.FechaAplicacion AS C10011 , " +
		"       E10.FechaEstatus AS C10012 , E10.Estatus AS C10013 , E10.NumeroReintento AS C10014 , E10.UsuarioIngreso AS C10015 , " +
		"       E10.Autorizaciones AS C10016 , E10.AplicacionDebitoHost AS C10017 , E10.Sys_MarcaTiempo AS C10018 , E10.Sys_ProcessId AS C10019 , " +
		"       E10.Sys_LowDateTime AS C10020 , E10.Sys_HighDateTime AS C10021 , E10.Ponderacion AS C10022 , E10.Firmas AS C10023 , " +
		"       E10.FechaEnvioHost AS C10024 , E10.ListoParaHost AS C10025 , E10.FechaHoraProcesado AS C10026 , E10.Token AS C10027 , " +
		"       E10.ComentarioRechazo AS C10028 , E10.MontoImpuesto AS C10029" +
		"  FROM BS_LotePE E10";
	/**
	 * Atributo que almacena el <code>String</code> para obtener el n&uacute;mero de registros presentes en el negocio.
	 */
	public static final String COUNT_HEADER_SQL =   "SELECT COUNT(*) FROM BS_LotePE";    												
	/**
	 * Atributo que almacena el <code>String</code> para la inserci&oacute;n, sin id, de un nuevo registro en la tabla.
	 */
	public static final String CREATE_SQL = 
		"INSERT INTO BS_LotePE (" +
		"TipoConvenio,Convenio,EmpresaODepto,TipoLote,MontoTotal" +
		",NumOperaciones,NombreLote,FechaEnviado,FechaRecibido" +
		",FechaAplicacion,FechaEstatus,Estatus,NumeroReintento" +
		",UsuarioIngreso,Autorizaciones,AplicacionDebitoHost,Sys_MarcaTiempo" +
		",Sys_ProcessId,Sys_LowDateTime,Sys_HighDateTime,Ponderacion" +
		",Firmas,FechaEnvioHost,ListoParaHost,FechaHoraProcesado" +
		",Token,ComentarioRechazo,MontoImpuesto" +
		") VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	/**
	 * Atributo que almacena el <code>String</code> para insertar, con id, un registro en la table.
	 */
	public static final String CREATE_SQL_CON_ID = 
		"INSERT INTO BS_LotePE (" +
		"Instalacion,Lote,TipoConvenio,Convenio,EmpresaODepto" +
		",TipoLote,MontoTotal,NumOperaciones,NombreLote" +
		",FechaEnviado,FechaRecibido,FechaAplicacion,FechaEstatus" +
		",Estatus,NumeroReintento,UsuarioIngreso,Autorizaciones" +
		",AplicacionDebitoHost,Sys_MarcaTiempo,Sys_ProcessId,Sys_LowDateTime" +
		",Sys_HighDateTime,Ponderacion,Firmas,FechaEnvioHost" +
		",ListoParaHost,FechaHoraProcesado,Token,ComentarioRechazo" +
		",MontoImpuesto" +
		") VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	/**
	 * Atributo que almacena el <code>String</code> para la atualizaci&oacute; de un registro en la tabla.
	 */
	public static final String UPDATE_SQL = 
		"UPDATE BS_LotePE" +
		" SET TipoConvenio= ?  , Convenio= ?  , EmpresaODepto= ?  , TipoLote= ?  , " +
		"MontoTotal= ?  , NumOperaciones= ?  , NombreLote= ?  , FechaEnviado= ?  , " +
		"FechaRecibido= ?  , FechaAplicacion= ?  , FechaEstatus= ?  , Estatus= ?  , " +
		"NumeroReintento= ?  , UsuarioIngreso= ?  , Autorizaciones= ?  , AplicacionDebitoHost= ?  , " +
		"Sys_MarcaTiempo= ?  , Sys_ProcessId= ?  , Sys_LowDateTime= ?  , Sys_HighDateTime= ?  , " +
		"Ponderacion= ?  , Firmas= ?  , FechaEnvioHost= ?  , ListoParaHost= ?  , " +
		"FechaHoraProcesado= ?  , Token= ?  , ComentarioRechazo= ?  , MontoImpuesto= ? " +
		" WHERE Instalacion= ?    AND Lote= ? ";
	/**
	 * Atributo que almacena el <code>String</code> para la eliminaci&oacute; de un registro en la tabla.
	 */
	public static final String DELETE_SQL =  
		"DELETE FROM BS_LotePE" +
		" WHERE Instalacion= ?    AND Lote= ? ";
	/**
	 * Atributo que almacena el <code>String</code> para la b&uacute;squeda por id de un registro de la tabla.
	 */
	public static final String FIND_BY_ID_SQL = 
		"SELECT E10.Instalacion AS C10001 , E10.Lote AS C10002 , E10.TipoConvenio AS C10030 , E10.Convenio AS C10003 , " +
		"       E10.EmpresaODepto AS C10004 , E10.TipoLote AS C10005 , E10.MontoTotal AS C10006 , E10.NumOperaciones AS C10007 , " +
		"       E10.NombreLote AS C10008 , E10.FechaEnviado AS C10009 , E10.FechaRecibido AS C10010 , E10.FechaAplicacion AS C10011 , " +
		"       E10.FechaEstatus AS C10012 , E10.Estatus AS C10013 , E10.NumeroReintento AS C10014 , E10.UsuarioIngreso AS C10015 , " +
		"       E10.Autorizaciones AS C10016 , E10.AplicacionDebitoHost AS C10017 , E10.Sys_MarcaTiempo AS C10018 , E10.Sys_ProcessId AS C10019 , " +
		"       E10.Sys_LowDateTime AS C10020 , E10.Sys_HighDateTime AS C10021 , E10.Ponderacion AS C10022 , E10.Firmas AS C10023 , " +
		"       E10.FechaEnvioHost AS C10024 , E10.ListoParaHost AS C10025 , E10.FechaHoraProcesado AS C10026 , E10.Token AS C10027 , " +
		"       E10.ComentarioRechazo AS C10028 , E10.MontoImpuesto AS C10029" +
		"  FROM BS_LotePE E10" +
		" WHERE E10.Instalacion= ? " +
		"   AND E10.Lote= ? ";
	/**
	 * Atributo que almacena el <code>String</code> para la creacion de la tabla en la base de datos.
	 */
	public static final String CREATE_TABLE_DDL = 
		"CREATE TABLE BS_LotePE (" +
		"    Instalacion LONG NOT NULL," +
		"    Lote LONG NOT NULL," +
		"    TipoConvenio LONG NOT NULL," +
		"    Convenio LONG NOT NULL," +
		"    EmpresaODepto LONG NOT NULL," +
		"    TipoLote LONG," +
		"    MontoTotal NUMERIC(19, 0)  NOT NULL," +
		"    NumOperaciones LONG NOT NULL," +
		"    NombreLote VARCHAR(30) ," +
		"    FechaEnviado TIMESTAMP NOT NULL," +
		"    FechaRecibido TIMESTAMP," +
		"    FechaAplicacion TIMESTAMP," +
		"    FechaEstatus TIMESTAMP," +
		"    Estatus LONG NOT NULL," +
		"    NumeroReintento LONG," +
		"    UsuarioIngreso VARCHAR(10) ," +
		"    Autorizaciones LONG," +
		"    AplicacionDebitoHost LONG," +
		"    Sys_MarcaTiempo TIMESTAMP," +
		"    Sys_ProcessId LONG," +
		"    Sys_LowDateTime LONG," +
		"    Sys_HighDateTime LONG," +
		"    Ponderacion LONG NOT NULL," +
		"    Firmas LONG NOT NULL," +
		"    FechaEnvioHost TIMESTAMP," +
		"    ListoParaHost TIMESTAMP," +
		"    FechaHoraProcesado TIMESTAMP," +
		"    Token LONG," +
		"    ComentarioRechazo VARCHAR(100) ," +
		"    MontoImpuesto NUMERIC(19, 0) " +
		")";
	/**
	 * Atributo que almacena un objeto del tipo {@link SQLLenguaje}.
	 */
	private static SQLLenguaje lenguaje;
	
	/**
	 * M&eacute;todo que realiza la creaci&oacute;n de un DTO, del negocio, a partir del resultado de alguna consulta.
	 * @param <code>rst</code> objeto de tipo <code>Resultset</code>, que contiene los resultados de la consulta.
	 * @return <code>Object</code> - representa al DTO creado.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static Object crearDtoST(ResultSet rst)
			throws SQLException, ExcepcionEnDAO {
 	    BsLotepe theDto = new BsLotepe();
     	theDto.setInstalacion(SQLUtils.getLongFromResultSet(rst, "C10001"));
     	theDto.setLote(SQLUtils.getLongFromResultSet(rst, "C10002"));
     	theDto.setTipoconvenio(SQLUtils.getLongFromResultSet(rst, "C10030"));
     	theDto.setConvenio(SQLUtils.getLongFromResultSet(rst, "C10003"));
     	theDto.setEmpresaodepto(SQLUtils.getLongFromResultSet(rst, "C10004"));
     	theDto.setTipolote(SQLUtils.getLongFromResultSet(rst, "C10005"));
		theDto.setMontototal(rst.getBigDecimal("C10006"));
     	theDto.setNumoperaciones(SQLUtils.getLongFromResultSet(rst, "C10007"));
		theDto.setNombrelote(rst.getString("C10008"));
		theDto.setFechaenviado(rst.getTimestamp("C10009"));
		theDto.setFecharecibido(rst.getTimestamp("C10010"));
		theDto.setFechaaplicacion(rst.getTimestamp("C10011"));
		theDto.setFechaestatus(rst.getTimestamp("C10012"));
     	theDto.setEstatus(SQLUtils.getLongFromResultSet(rst, "C10013"));
     	theDto.setNumeroreintento(SQLUtils.getLongFromResultSet(rst, "C10014"));
		theDto.setUsuarioingreso(rst.getString("C10015"));
     	theDto.setAutorizaciones(SQLUtils.getLongFromResultSet(rst, "C10016"));
     	theDto.setAplicaciondebitohost(SQLUtils.getLongFromResultSet(rst, "C10017"));
		theDto.setSysMarcatiempo(rst.getTimestamp("C10018"));
     	theDto.setSysProcessid(SQLUtils.getLongFromResultSet(rst, "C10019"));
     	theDto.setSysLowdatetime(SQLUtils.getLongFromResultSet(rst, "C10020"));
     	theDto.setSysHighdatetime(SQLUtils.getLongFromResultSet(rst, "C10021"));
     	theDto.setPonderacion(SQLUtils.getLongFromResultSet(rst, "C10022"));
     	theDto.setFirmas(SQLUtils.getLongFromResultSet(rst, "C10023"));
		theDto.setFechaenviohost(rst.getTimestamp("C10024"));
		theDto.setListoparahost(rst.getTimestamp("C10025"));
		theDto.setFechahoraprocesado(rst.getTimestamp("C10026"));
     	theDto.setToken(SQLUtils.getLongFromResultSet(rst, "C10027"));
		theDto.setComentariorechazo(rst.getString("C10028"));
		theDto.setMontoimpuesto(rst.getBigDecimal("C10029"));
		return theDto;
	}
	
	


	/**
	 * M&eacute;todo que realiza la creaci&oacute;n de un DTO foraneo, del negocio, a partir del resultado de alguna consulta.
	 * @param <code>rst</code> objeto de tipo <code>Resultset</code>, que contiene los resultados de la consulta.
	 * @return <code>BsLotepe</code> - representa al DTO del negocio con el nuevo DTO foraneo asociado.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static BsLotepe crearDataConValoresForaneosST(ResultSet rst)throws SQLException, ExcepcionEnDAO{
		BsLotepe bsLotepe = (BsLotepe) crearDtoST(rst);
        BsConvenio bsConvenioRel  = (BsConvenio) BsConvenioDaoJdbcSql.crearDtoST(rst);
        bsLotepe.setBsConvenio(bsConvenioRel);
		return bsLotepe;
	}




	/**
	 * M&eacute;todo que realiza la creaci&oacute;n de un DTO foraneo, del negocio, a partir de la b&uacute;squeda por <code>id</code>.
	 * @param <code>rst</code> objeto de tipo <code>Resultset</code>, que contiene los resultados de la consulta.
	 * @return <code>BsLotepe</code> - representa al DTO del negocio con el nuevo DTO foraneo asociado.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static BsLotepe crearDataConValoresForaneosNoRelST(ResultSet rst, Connection conn)throws SQLException, ExcepcionEnDAO{	
		BsLotepe bsLotepe = (BsLotepe) crearDtoST(rst);
        BsConvenio tipoconvenioRelbsConvenio  = (BsConvenio) SQLImplementacionOperacionesComunes.buscarPorIDST(
                bsLotepe.getTipoconvenio(), conn, 
				BsConvenioDaoJdbcSql.SQL_OPERACIONES,
	       		false, 
	       		null);
        bsLotepe.setBsConvenio(tipoconvenioRelbsConvenio);
		return bsLotepe;
	}

	/**
	 * M&eacute;todo que coloca los valores provenientes del objeto Dto en el objeto PreparedStatement,
	 * el cual proviene de la llamada a la implementacion de {@link SQLOperaciones}, este m&eacute;todo
	 * no toma en cuenta el valor del <code>id</code>, puesto que se asume que es un valor secuencial
	 * autonumerico o serial propio de la base de datos.
	 * 
	 * @param <code>theDtoUntyped</code> objeto sin tipo que almacena al DTO del negocio.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaCrearST(Object theDtoUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {			
	        BsLotepe theDto = (BsLotepe)theDtoUntyped;
				Long tipoconvenio = theDto.getTipoconvenio();     
		Long convenio = theDto.getConvenio();     
		Long empresaodepto = theDto.getEmpresaodepto();     
		Long tipolote = theDto.getTipolote();     
		java.math.BigDecimal montototal = theDto.getMontototal();     
		Long numoperaciones = theDto.getNumoperaciones();     
		String nombrelote = theDto.getNombrelote();     
		java.util.Date fechaenviado = theDto.getFechaenviado();     
		java.util.Date fecharecibido = theDto.getFecharecibido();     
		java.util.Date fechaaplicacion = theDto.getFechaaplicacion();     
		java.util.Date fechaestatus = theDto.getFechaestatus();     
		Long estatus = theDto.getEstatus();     
		Long numeroreintento = theDto.getNumeroreintento();     
		String usuarioingreso = theDto.getUsuarioingreso();     
		Long autorizaciones = theDto.getAutorizaciones();     
		Long aplicaciondebitohost = theDto.getAplicaciondebitohost();     
		java.util.Date sysMarcatiempo = theDto.getSysMarcatiempo();     
		Long sysProcessid = theDto.getSysProcessid();     
		Long sysLowdatetime = theDto.getSysLowdatetime();     
		Long sysHighdatetime = theDto.getSysHighdatetime();     
		Long ponderacion = theDto.getPonderacion();     
		Long firmas = theDto.getFirmas();     
		java.util.Date fechaenviohost = theDto.getFechaenviohost();     
		java.util.Date listoparahost = theDto.getListoparahost();     
		java.util.Date fechahoraprocesado = theDto.getFechahoraprocesado();     
		Long token = theDto.getToken();     
		String comentariorechazo = theDto.getComentariorechazo();     
		java.math.BigDecimal montoimpuesto = theDto.getMontoimpuesto();     
		     
		SQLUtils.setIntoStatement( 1, tipoconvenio, smt);  
    		     
		SQLUtils.setIntoStatement( 2, convenio, smt);  
    		     
		SQLUtils.setIntoStatement( 3, empresaodepto, smt);  
    		     
		SQLUtils.setIntoStatement( 4, tipolote, smt);  
    		     
		SQLUtils.setIntoStatement( 5, montototal, smt);  
    		     
		SQLUtils.setIntoStatement( 6, numoperaciones, smt);  
    		     
		SQLUtils.setIntoStatement( 7, nombrelote, smt);  
    		     
		SQLUtils.setIntoStatement( 8, fechaenviado, smt);  
    		     
		SQLUtils.setIntoStatement( 9, fecharecibido, smt);  
    		     
		SQLUtils.setIntoStatement( 10, fechaaplicacion, smt);  
    		     
		SQLUtils.setIntoStatement( 11, fechaestatus, smt);  
    		     
		SQLUtils.setIntoStatement( 12, estatus, smt);  
    		     
		SQLUtils.setIntoStatement( 13, numeroreintento, smt);  
    		     
		SQLUtils.setIntoStatement( 14, usuarioingreso, smt);  
    		     
		SQLUtils.setIntoStatement( 15, autorizaciones, smt);  
    		     
		SQLUtils.setIntoStatement( 16, aplicaciondebitohost, smt);  
    		     
		SQLUtils.setIntoStatement( 17, sysMarcatiempo, smt);  
    		     
		SQLUtils.setIntoStatement( 18, sysProcessid, smt);  
    		     
		SQLUtils.setIntoStatement( 19, sysLowdatetime, smt);  
    		     
		SQLUtils.setIntoStatement( 20, sysHighdatetime, smt);  
    		     
		SQLUtils.setIntoStatement( 21, ponderacion, smt);  
    		     
		SQLUtils.setIntoStatement( 22, firmas, smt);  
    		     
		SQLUtils.setIntoStatement( 23, fechaenviohost, smt);  
    		     
		SQLUtils.setIntoStatement( 24, listoparahost, smt);  
    		     
		SQLUtils.setIntoStatement( 25, fechahoraprocesado, smt);  
    		     
		SQLUtils.setIntoStatement( 26, token, smt);  
    		     
		SQLUtils.setIntoStatement( 27, comentariorechazo, smt);  
    		     
		SQLUtils.setIntoStatement( 28, montoimpuesto, smt);  
    		
	}

	/**
	 * M&eacute;todo que coloca los valores provenientes del objeto Dto en el objeto PreparedStatement,
	 * el cual proviene de la llamada a la implementacion de {@link SQLOperaciones}, este m&eacute;todo
	 * toma en cuenta el <code>id</code>, lo que quiere decir que se usa al momento de  crear registros
	 * en donde el identificador no es un valor secuencial autonumerico o serial propio de la base de datos
	 *
	 * @param <code>theDtoUntyped</code> objeto sin tipo que almacena al DTO del negocio.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaCrearConIdST(Object theDtoUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {			
	        BsLotepe theDto = (BsLotepe)theDtoUntyped;
				Long instalacion = theDto.getInstalacion();     
		Long lote = theDto.getLote();     
		Long tipoconvenio = theDto.getTipoconvenio();     
		Long convenio = theDto.getConvenio();     
		Long empresaodepto = theDto.getEmpresaodepto();     
		Long tipolote = theDto.getTipolote();     
		java.math.BigDecimal montototal = theDto.getMontototal();     
		Long numoperaciones = theDto.getNumoperaciones();     
		String nombrelote = theDto.getNombrelote();     
		java.util.Date fechaenviado = theDto.getFechaenviado();     
		java.util.Date fecharecibido = theDto.getFecharecibido();     
		java.util.Date fechaaplicacion = theDto.getFechaaplicacion();     
		java.util.Date fechaestatus = theDto.getFechaestatus();     
		Long estatus = theDto.getEstatus();     
		Long numeroreintento = theDto.getNumeroreintento();     
		String usuarioingreso = theDto.getUsuarioingreso();     
		Long autorizaciones = theDto.getAutorizaciones();     
		Long aplicaciondebitohost = theDto.getAplicaciondebitohost();     
		java.util.Date sysMarcatiempo = theDto.getSysMarcatiempo();     
		Long sysProcessid = theDto.getSysProcessid();     
		Long sysLowdatetime = theDto.getSysLowdatetime();     
		Long sysHighdatetime = theDto.getSysHighdatetime();     
		Long ponderacion = theDto.getPonderacion();     
		Long firmas = theDto.getFirmas();     
		java.util.Date fechaenviohost = theDto.getFechaenviohost();     
		java.util.Date listoparahost = theDto.getListoparahost();     
		java.util.Date fechahoraprocesado = theDto.getFechahoraprocesado();     
		Long token = theDto.getToken();     
		String comentariorechazo = theDto.getComentariorechazo();     
		java.math.BigDecimal montoimpuesto = theDto.getMontoimpuesto();     
	
		SQLUtils.setIntoStatement( 1, instalacion, smt);
    	
		SQLUtils.setIntoStatement( 2, lote, smt);
    	
		SQLUtils.setIntoStatement( 3, tipoconvenio, smt);
    	
		SQLUtils.setIntoStatement( 4, convenio, smt);
    	
		SQLUtils.setIntoStatement( 5, empresaodepto, smt);
    	
		SQLUtils.setIntoStatement( 6, tipolote, smt);
    	
		SQLUtils.setIntoStatement( 7, montototal, smt);
    	
		SQLUtils.setIntoStatement( 8, numoperaciones, smt);
    	
		SQLUtils.setIntoStatement( 9, nombrelote, smt);
    	
		SQLUtils.setIntoStatement( 10, fechaenviado, smt);
    	
		SQLUtils.setIntoStatement( 11, fecharecibido, smt);
    	
		SQLUtils.setIntoStatement( 12, fechaaplicacion, smt);
    	
		SQLUtils.setIntoStatement( 13, fechaestatus, smt);
    	
		SQLUtils.setIntoStatement( 14, estatus, smt);
    	
		SQLUtils.setIntoStatement( 15, numeroreintento, smt);
    	
		SQLUtils.setIntoStatement( 16, usuarioingreso, smt);
    	
		SQLUtils.setIntoStatement( 17, autorizaciones, smt);
    	
		SQLUtils.setIntoStatement( 18, aplicaciondebitohost, smt);
    	
		SQLUtils.setIntoStatement( 19, sysMarcatiempo, smt);
    	
		SQLUtils.setIntoStatement( 20, sysProcessid, smt);
    	
		SQLUtils.setIntoStatement( 21, sysLowdatetime, smt);
    	
		SQLUtils.setIntoStatement( 22, sysHighdatetime, smt);
    	
		SQLUtils.setIntoStatement( 23, ponderacion, smt);
    	
		SQLUtils.setIntoStatement( 24, firmas, smt);
    	
		SQLUtils.setIntoStatement( 25, fechaenviohost, smt);
    	
		SQLUtils.setIntoStatement( 26, listoparahost, smt);
    	
		SQLUtils.setIntoStatement( 27, fechahoraprocesado, smt);
    	
		SQLUtils.setIntoStatement( 28, token, smt);
    	
		SQLUtils.setIntoStatement( 29, comentariorechazo, smt);
    	
		SQLUtils.setIntoStatement( 30, montoimpuesto, smt);
    		
	}

	/**
	 * M&eacute;todo que coloca el identificador recibido en el objeto PreparedStatement, para poder ser eliminado.
	 * @param <code>theIdUntyped</code> objeto sin tipo que identifica al <code>id</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaEliminarST(Object theIdUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {
			BsLotepeID theId = (BsLotepeID)theIdUntyped;
			Long instalacion = theId.getInstalacion();     
		Long lote = theId.getLote();     
		     
		SQLUtils.setIntoStatement( 1, instalacion, smt);  
    		     
		SQLUtils.setIntoStatement( 2, lote, smt);  
    		}

	/**
	 * M&eacute;todo que coloca el identificador recibido, en el objeto PreparedStatement para poder hacer la consulta de busqueda.
	 * @param <code>theIdUntyped</code> objeto sin tipo que identifica al <code>id</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaBuscarPorIdST(Object theIdUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {
			BsLotepeID theId = (BsLotepeID)theIdUntyped;
				Long instalacion = theId.getInstalacion();     
		Long lote = theId.getLote();     
		     
		SQLUtils.setIntoStatement( 1, instalacion, smt);  
    		     
		SQLUtils.setIntoStatement( 2, lote, smt);  
    			}

	/**
	 * M&eacute;todo que coloca los valores provenientes del objeto Dto en el objeto PreparedStatement,
	 * el cual proviene de la llamada a la implementacion de {@link SQLOperaciones} para poder actualizar
     * un registro en espec&iacute;fico.
	 * 
	 * @param <code>theDtoUntyped</code> objeto sin tipo que almacena al DTO del negocio.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaActualizarST(Object theDtoUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {
			BsLotepe theDto = (BsLotepe)theDtoUntyped;
				Long instalacion = theDto.getInstalacion();     
		Long lote = theDto.getLote();     
		Long tipoconvenio = theDto.getTipoconvenio();     
		Long convenio = theDto.getConvenio();     
		Long empresaodepto = theDto.getEmpresaodepto();     
		Long tipolote = theDto.getTipolote();     
		java.math.BigDecimal montototal = theDto.getMontototal();     
		Long numoperaciones = theDto.getNumoperaciones();     
		String nombrelote = theDto.getNombrelote();     
		java.util.Date fechaenviado = theDto.getFechaenviado();     
		java.util.Date fecharecibido = theDto.getFecharecibido();     
		java.util.Date fechaaplicacion = theDto.getFechaaplicacion();     
		java.util.Date fechaestatus = theDto.getFechaestatus();     
		Long estatus = theDto.getEstatus();     
		Long numeroreintento = theDto.getNumeroreintento();     
		String usuarioingreso = theDto.getUsuarioingreso();     
		Long autorizaciones = theDto.getAutorizaciones();     
		Long aplicaciondebitohost = theDto.getAplicaciondebitohost();     
		java.util.Date sysMarcatiempo = theDto.getSysMarcatiempo();     
		Long sysProcessid = theDto.getSysProcessid();     
		Long sysLowdatetime = theDto.getSysLowdatetime();     
		Long sysHighdatetime = theDto.getSysHighdatetime();     
		Long ponderacion = theDto.getPonderacion();     
		Long firmas = theDto.getFirmas();     
		java.util.Date fechaenviohost = theDto.getFechaenviohost();     
		java.util.Date listoparahost = theDto.getListoparahost();     
		java.util.Date fechahoraprocesado = theDto.getFechahoraprocesado();     
		Long token = theDto.getToken();     
		String comentariorechazo = theDto.getComentariorechazo();     
		java.math.BigDecimal montoimpuesto = theDto.getMontoimpuesto();     
		     
		SQLUtils.setIntoStatement( 1, tipoconvenio, smt);  
        	     
		SQLUtils.setIntoStatement( 2, convenio, smt);  
        	     
		SQLUtils.setIntoStatement( 3, empresaodepto, smt);  
        	     
		SQLUtils.setIntoStatement( 4, tipolote, smt);  
        	     
		SQLUtils.setIntoStatement( 5, montototal, smt);  
        	     
		SQLUtils.setIntoStatement( 6, numoperaciones, smt);  
        	     
		SQLUtils.setIntoStatement( 7, nombrelote, smt);  
        	     
		SQLUtils.setIntoStatement( 8, fechaenviado, smt);  
        	     
		SQLUtils.setIntoStatement( 9, fecharecibido, smt);  
        	     
		SQLUtils.setIntoStatement( 10, fechaaplicacion, smt);  
        	     
		SQLUtils.setIntoStatement( 11, fechaestatus, smt);  
        	     
		SQLUtils.setIntoStatement( 12, estatus, smt);  
        	     
		SQLUtils.setIntoStatement( 13, numeroreintento, smt);  
        	     
		SQLUtils.setIntoStatement( 14, usuarioingreso, smt);  
        	     
		SQLUtils.setIntoStatement( 15, autorizaciones, smt);  
        	     
		SQLUtils.setIntoStatement( 16, aplicaciondebitohost, smt);  
        	     
		SQLUtils.setIntoStatement( 17, sysMarcatiempo, smt);  
        	     
		SQLUtils.setIntoStatement( 18, sysProcessid, smt);  
        	     
		SQLUtils.setIntoStatement( 19, sysLowdatetime, smt);  
        	     
		SQLUtils.setIntoStatement( 20, sysHighdatetime, smt);  
        	     
		SQLUtils.setIntoStatement( 21, ponderacion, smt);  
        	     
		SQLUtils.setIntoStatement( 22, firmas, smt);  
        	     
		SQLUtils.setIntoStatement( 23, fechaenviohost, smt);  
        	     
		SQLUtils.setIntoStatement( 24, listoparahost, smt);  
        	     
		SQLUtils.setIntoStatement( 25, fechahoraprocesado, smt);  
        	     
		SQLUtils.setIntoStatement( 26, token, smt);  
        	     
		SQLUtils.setIntoStatement( 27, comentariorechazo, smt);  
        	     
		SQLUtils.setIntoStatement( 28, montoimpuesto, smt);  
        		     
		SQLUtils.setIntoStatement( 29, instalacion, smt);  
    		     
		SQLUtils.setIntoStatement( 30, lote, smt);  
    			
	}


	/**
	 * M&eacute;todo que ejecuta una petici&oacute;n de datos a trav&eacute;s de un objeto {@link PreparedStatement},
	 * y un objeto del tipo {@link PeticionDeDatos}, el cual en su implementaci&oacute;n define una serie de restricciones
	 * usadas para hacer la petici&oacute;n.
	 *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaPeticionDeDatosST(PeticionDeDatos peticion,
			PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
		SQLImplementacionOperacionesComunes.operacionesParaPeticionDeDatosST( peticion, smt, SQL_OPERACIONES, null);	
	}	
	
	/**
	 * M&eacute;todo que ejecuta una petici&oacute;n de datos a trav&eacute;s de un objeto {@link PreparedStatement},
     * y un objeto del tipo {@link PeticionDeDatos}, el cual en su implementaci&oacute;n define una serie de restricciones
     * usadas para hacer la petici&oacute;n.
	 *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaPeticionDeDatosFST(PeticionDeDatos peticion,
			PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
		SQLImplementacionOperacionesComunes.operacionesParaPeticionDeDatosST( peticion, smt, SQL_OPERACIONES, null);	
	}	

	/**
	 * M&eacute;todo que ejecuta una petici&oacute;n de conteo a trav&eacute;s de un objeto {@link PreparedStatement},
     * y un objeto del tipo {@link PeticionDeDatos}, el cual en su implementaci&oacute;n define una serie de restricciones
     * usadas para hacer la petici&oacute;n.
	 *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaConteoST(PeticionDeDatos peticion,
			PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
		SQLImplementacionOperacionesComunes.operacionesParaPeticionDeDatosST( peticion, smt, SQL_OPERACIONES, null);
	}

	/**
	 * M&eacute;todo que agrega al atributo {@link #SELECT_HEADER_SQL} las condiciones de b&uacute;squeda
     * que vienen en el objeto del tipo {@link PeticionDeDatos}
     *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>, que contiene las condiciones de la consulta.
	 * @return <code>String</code> - la consulta condicionada como cadena de caracteres.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static String getSqlParaPeticionDeDatosST(PeticionDeDatos peticion)
			throws SQLException, ExcepcionEnDAO {
		return SELECT_HEADER_SQL + SQLImplementacionOperacionesComunes.crearCondicionesSQLST( peticion, SQL_OPERACIONES, SQLImplementacionOperacionesComunes.COND_ALIAS_ORDEN_WHERE);
	}
	
	
	
	/**
	 * M&eacute;todo que agrega al atributo {@link #SELECT_HEADER_SQL_F} las condiciones de b&uacute;squeda
     * que vienen en el objeto del tipo {@link PeticionDeDatos}
     *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>, que contiene las condiciones de la consulta.
	 * @return <code>String</code> - la consulta condicionada como cadena de caracteres.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static String getSqlParaPeticionDeDatosFST(PeticionDeDatos peticion)
			throws SQLException, ExcepcionEnDAO {
		return SELECT_HEADER_SQL_F + SQLImplementacionOperacionesComunes.crearCondicionesSQLST( peticion, SQL_OPERACIONES, SQLImplementacionOperacionesComunes.COND_ALIAS_ORDEN_AND);
	}
	

	

	/**
	 * M&eacute;todo que agrega al atributo {@link #COUNT_HEADER_SQL} las condiciones de conteo
	 * que vienen en el objeto de tipo {@link PeticionDeDatos}.
	 *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>, que contiene las condiciones de la consulta.
	 * @return <code>String</code> - la consulta condicionada como cadena de caracteres.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static String getSqlDeConteoST(PeticionDeDatos peticion)
			throws SQLException, ExcepcionEnDAO {
		return COUNT_HEADER_SQL + SQLImplementacionOperacionesComunes.crearCondicionesSQLST( peticion, SQL_OPERACIONES, SQLImplementacionOperacionesComunes.COND_SQL_WHERE);
	}
	
	/**
	 * M&eacute;todo que se utiliza para actualizar un registro a nivel de tabla, recibe como par&aacute;metro un DTO del negocio
	 * que contiene la informaci&oacute;n a actualizar, e invoca la funci&oacute;n <code>actualizar</code> de la clase de la que hereda.
	 *
	 * @param <code>bsLotepe</code> objeto del tipo <code>BsLotepe</code>.
	 * @throws ExcepcionEnDAO
	 * @see {@link SQLImplementacionBasica}
	 */
	public void actualizar(BsLotepe bsLotepe) throws ExcepcionEnDAO {
	    actualizar(bsLotepe, AUTO_COMMIT);
	
	}
	
	/**
	 * M&eacute;todo utilizado para realizar una b&uacute;squeda de un registro espec&iacute;fico, utilizando un identificador.
	 *
	 * @param <code>bsLotepeID</code> objeto del tipo <code>BsLotepeID</code>.
	 * @return <code>BsLotepe</code> - un objeto DTO.
	 * @throws ExcepcionEnDAO
	 * @see {@link SQLImplementacionBasica}
	 */
	public BsLotepe buscarPorID(BsLotepeID bsLotepeID)
			throws ExcepcionEnDAO {
		return (BsLotepe)buscarPorID(bsLotepeID, AUTO_COMMIT);				 
	}

	/**
	 * M&eacute;todo utilizado para la creaci&oacute;n de un registro, a nivel de tabla, a partir de la informaci&oacute;n
	 * recibida en un DTO del objeto de Negocio, e invoca la funci&oacute;n <code>crear</code> de la clase de la que hereda.
	 * 
	 * @param <code>bsLotepe</code> objeto del tipo <code>BsLotepe</code>.
	 * @throws ExcepcionEnDAO
	 * @see {@link SQLImplementacionBasica}
	 */
	public void crear(BsLotepe bsLotepe) throws ExcepcionEnDAO {
	    crear(bsLotepe, AUTO_COMMIT);		
	}

	/**
	 * M&eacute;todo utilizado para la eliminaci&oacute;n de un registro, a nivel de tabla,
	 * que se busca a trav&eacute;s del identificador recibido. Se invoca la funci&oacute;n <code>eliminar</code> de la clase de la que hereda.
	 *
	 * @param <code>bsLotepeID</code> objeto del tipo <code>BsLotepeID</code>.
	 * @throws ExcepcionEnDAO
	 * @see {@link SQLImplementacionBasica}
	 */
	public void eliminar(BsLotepeID bsLotepeID) throws ExcepcionEnDAO {
	    eliminar(bsLotepeID, AUTO_COMMIT);		
	}

	/**
	 * M&eacute;todo utilizado para devolver un &uacute;nico resultado de la petici&oacute;n recibida,
	 * la cual se realiza a trav&eacuate;s del objeto de tipo {@link PeticionDeDatos}.
	 * 
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>, que contiene las condiciones de la consulta.
	 * @return <code>BsLotepe</code> - un objeto DTO.
	 * @throws ExcepcionEnDAO
	 * @throws ExcepcionPorDatoNoEncontrado
	 * @see {@link SQLImplementacionBasica}
	 */
	public BsLotepe resultadoUnico(PeticionDeDatos peticion)
			throws ExcepcionEnDAO, ExcepcionPorDatoNoEncontrado {
		return (BsLotepe)resultadoUnico(peticion, AUTO_COMMIT);
	}
	
	/**
	 * Atributo del tipo {@link SQLOperaciones}, almacena la operaciones disponibles para esta implementaci&oacute;n.<br/>
	 * Algunas de las llamadas a los m&eacute;todos se realiza a trav&eacute;s de la invocaci&oacute;n de estas implementaciones.
	 */
    public static final SQLOperaciones	SQL_OPERACIONES =  new SQLOperaciones() {
			
			public int getManejoForaneosPorDefecto() {
				return Dao.FORANEOS_JOIN;
			}
			
			
			public void operacionesParaPeticionDeDatosF(PeticionDeDatos request,
					PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
				BsLotepeDaoJdbcSql.operacionesParaPeticionDeDatosFST(request, smt);
			}
			
			public void operacionesParaPeticionDeDatos(PeticionDeDatos request,
					PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
				BsLotepeDaoJdbcSql.operacionesParaPeticionDeDatosST(request, smt);
			}
			
			public void operacionesParaEliminar(Object theId, PreparedStatement smt)
					throws SQLException, ExcepcionEnDAO {
				BsLotepeDaoJdbcSql.operacionesParaEliminarST(theId, smt);
			}
			
			public void operacionesParaCrear(Object theDto, PreparedStatement smt)
					throws SQLException, ExcepcionEnDAO {
				BsLotepeDaoJdbcSql.operacionesParaCrearST(theDto, smt);
			}
			
			public void operacionesParaCrearConId(Object theDto, PreparedStatement smt) 
			        throws SQLException, ExcepcionEnDAO {
			    BsLotepeDaoJdbcSql.operacionesParaCrearConIdST(theDto, smt);
            }
			
			public void operacionesParaConteo(PeticionDeDatos request,
					PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
				BsLotepeDaoJdbcSql.operacionesParaConteoST(request, smt);				
			}
			
			public void operacionesParaBuscarPorId(Object theId, PreparedStatement smt)
					throws SQLException, ExcepcionEnDAO {
				BsLotepeDaoJdbcSql.operacionesParaBuscarPorIdST(theId, smt);
			}
			
			public void operacionesParaActualizar(Object theDto, PreparedStatement smt)
					throws SQLException, ExcepcionEnDAO {
				BsLotepeDaoJdbcSql.operacionesParaActualizarST(theDto, smt);
				
			}
			
			public String getSqlParaPeticionDeDatosF(PeticionDeDatos request)
					throws SQLException, ExcepcionEnDAO {	
				return BsLotepeDaoJdbcSql.getSqlParaPeticionDeDatosFST(request);
			}
			
			public String getSqlParaPeticionDeDatos(PeticionDeDatos request)
					throws SQLException, ExcepcionEnDAO {
				return BsLotepeDaoJdbcSql.getSqlParaPeticionDeDatosST(request);
			}
			
			public String getSqlDeConteo(PeticionDeDatos request) throws SQLException,
					ExcepcionEnDAO {
				return BsLotepeDaoJdbcSql.getSqlDeConteoST(request);
			}
			
			public SQLEntidadMeta getSQLEntidadMeta() throws ExcepcionEnDAO {
				return BsLotepeDaoJdbcSql.SQL_ENTIDAD_META;
			}
			
			public String getEliminarSql() {
				return  BsLotepeDaoJdbcSql.DELETE_SQL;
			}
			
			public String getCrearSql() {
				return  BsLotepeDaoJdbcSql.CREATE_SQL;
			}
			
			public String getCrearSqlConId(){
			    return  BsLotepeDaoJdbcSql.CREATE_SQL_CON_ID;
			}			
			
			public String getBuscarPorIdSqlF() {
				return BsLotepeDaoJdbcSql.FIND_BY_ID_SQL_F;
			}
			
			public String getBuscarPorIdSql() {				
				return BsLotepeDaoJdbcSql.FIND_BY_ID_SQL;
			}
			
			public String getActualizarSql() {				
				return BsLotepeDaoJdbcSql.UPDATE_SQL;
			}
			
			public Object crearDto(ResultSet rst) throws SQLException, ExcepcionEnDAO {
				return BsLotepeDaoJdbcSql.crearDtoST(rst);
			}
	        
	        public Object crearDataConValoresForaneos(ResultSet rst)
					throws SQLException, ExcepcionEnDAO {
				return BsLotepeDaoJdbcSql.crearDataConValoresForaneosST(rst);
			}
			
	        public Object crearDataConValoresForaneosNoRel(ResultSet rst, Connection conn)
					throws SQLException, ExcepcionEnDAO {
				return BsLotepeDaoJdbcSql.crearDataConValoresForaneosNoRelST(rst, conn);
			}
			
			
            public boolean isForaneosDisponible(){
				return true;
            
            }
            
            public SQLLenguaje getSQLLenguaje() {
                return BsLotepeDaoJdbcSql.getLenguaje();
            }
            
            public void setIdEnDto(Object theDto, Object dtoId) {
                setIdEnDtoST(theDto, dtoId);
            }
	};
	
	
	
	
	
	/**
	 * Devuelve el atributo {@link #SQL_OPERACIONES}.
	 * @return <code>SQLOperaciones</code> - objeto del tipo {@link SQLOperaciones}
	 */
    public SQLOperaciones getSqlOperaciones() {
       return SQL_OPERACIONES;
	}
	
	/**
	 * Devuelve el atributo {@link #lenguaje}
	 * @return <code>SQLLenguaje</code> - objeto del tipo {@link SQLLenguaje}
	 */	
	public static SQLLenguaje getLenguaje() {
		return lenguaje;
	}
	
	/**
	 * M&eacute;todo que permite obtener a trav&eacute;s de un Servicio la implementaci&oacute;n del objeto de Lenguaje,
	 * y coloca las reglas de negocio almacenadas en el atributo {@link #SQL_ENTIDAD_META}.
	 * @param <code>localizadorLenguaje</code> objeto del tipo {@link LocalizadorDeServicio} que posee informaci&oacute;n para trabajar con la base de datos.
	 */
	public static void setLocalizadorLenguaje(LocalizadorDeServicio localizadorLenguaje) {
		lenguaje = (SQLLenguaje) localizadorLenguaje.getServicio();
		lenguaje.setSqlEntidadMeta(SQL_ENTIDAD_META);
	}

	/**
	 * M&eacute;todo que agrega un identificador a un DTO del tipo <code>BsLotepe</code>.
	 * @param <code>theDtoUntyped</code> objeto gen&eacute;rico, que representa a un <code>DTO</code>.
	 * @param <code>dtoIdUntyped</code> objeto gen&eacute;rico, que representa al identificador.
	 */
	public static void setIdEnDtoST(Object theDtoUntyped, Object dtoIdUntyped){
		BsLotepe dto = (BsLotepe)theDtoUntyped;
		BsLotepeID theId = (BsLotepeID)dtoIdUntyped;
		Long instalacion = theId.getInstalacion();     
		Long lote = theId.getLote();     
		dto.setInstalacion(instalacion);
		dto.setLote(lote);

	}	
	
	
}
