package com.ofbox.davivienda.h2h.orm.jdbc.sql.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.sql.Connection;
import java.util.Collection;

import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Dao;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.ExcepcionEnDAO;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.ExcepcionPorDatoNoEncontrado;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.PeticionDeDatos;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Transaccion;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.foraneos.DataConValoresForaneos;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLColumnaMeta;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLEntidadMeta;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLImplementacionBasica;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLImplementacionOperacionesComunes;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLLenguaje;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLOperaciones;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLUtils;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.TransaccionJdbc;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dependencias.LocalizadorDeServicio;


import com.ofbox.davivienda.h2h.abstraccion.dto.BsEmpresaodepto;
import java.lang.Long;
import com.ofbox.davivienda.h2h.abstraccion.dao.BsEmpresaodeptoDao;




/**
 * Clase que hereda comportamientos de {@link SQLImplementacionBasica} e implementa el comportamiento definido en la capa DAO.<br/>
 * Esta clase es una implementaci&oacute;n intermedia para el DAO de <b>Empresa o Departamento</b>
 * asociado a la tabla <b>BS_EmpresaODepto</b>, en esta clase se construyen todos los sql para las operaciones de listado,
 * creaci&oacute;n, consulta, actualizaci&oacute;n y elminaci&oacute;n.<br/>
 * @author Systems Out of the Box
 * @version f3.1
 *
 */
public class BsEmpresaodeptoDaoJdbcSql extends SQLImplementacionBasica implements BsEmpresaodeptoDao{

	/**
	 * Atributo del tipo {@link SQLEntidadMeta}, que almacena la descripci&oacute;n del negocio como una entidad.<br/>
	 * Entre sus caracter&iacute;sticas tiene el nombre de la tabla, nombre de la secuencia, posici&oacute;n de las columnas,
	 * nombre de las columnas y tipos de datos para la columna.<br/>
	 * El arreglo que recibe son objetos de tipo {@link SQLColumnaMeta}, y definen informaci&oacute;n para obtener las columnas.
	 */
	public static final SQLEntidadMeta SQL_ENTIDAD_META = new SQLEntidadMeta("BS_EmpresaODepto", new SQLColumnaMeta[]{
        new SQLColumnaMeta(7, "BS_EmpresaODepto", "E7", 7001, "EmpresaODepto", "C7001", Types.BIGINT ,true)
     ,  new SQLColumnaMeta(7, "BS_EmpresaODepto", "E7", 7002, "Cliente", "C7002", Types.BIGINT )
     ,  new SQLColumnaMeta(7, "BS_EmpresaODepto", "E7", 7003, "Nombre", "C7003", Types.VARCHAR )
     ,  new SQLColumnaMeta(7, "BS_EmpresaODepto", "E7", 7004, "Direccion", "C7004", Types.VARCHAR )
     ,  new SQLColumnaMeta(7, "BS_EmpresaODepto", "E7", 7005, "Telefono", "C7005", Types.VARCHAR )
     ,  new SQLColumnaMeta(7, "BS_EmpresaODepto", "E7", 7006, "Fax", "C7006", Types.VARCHAR )
     ,  new SQLColumnaMeta(7, "BS_EmpresaODepto", "E7", 7007, "Email", "C7007", Types.VARCHAR )
     ,  new SQLColumnaMeta(7, "BS_EmpresaODepto", "E7", 7008, "NombreContacto", "C7008", Types.VARCHAR )
     ,  new SQLColumnaMeta(7, "BS_EmpresaODepto", "E7", 7009, "ApellidoContacto", "C7009", Types.VARCHAR )
     ,  new SQLColumnaMeta(7, "BS_EmpresaODepto", "E7", 7010, "Proveedor", "C7010", Types.BIGINT )
     ,  new SQLColumnaMeta(7, "BS_EmpresaODepto", "E7", 7011, "NivelSeguridad", "C7011", Types.BIGINT )
     ,  new SQLColumnaMeta(7, "BS_EmpresaODepto", "E7", 7012, "FechaEstatus", "C7012", Types.TIMESTAMP )
     ,  new SQLColumnaMeta(7, "BS_EmpresaODepto", "E7", 7013, "Estatus", "C7013", Types.BIGINT )
     ,  new SQLColumnaMeta(7, "BS_EmpresaODepto", "E7", 7014, "OrigenLotes", "C7014", Types.BIGINT )
     ,  new SQLColumnaMeta(7, "BS_EmpresaODepto", "E7", 7015, "RegistroFiscal", "C7015", Types.VARCHAR )
     ,  new SQLColumnaMeta(7, "BS_EmpresaODepto", "E7", 7016, "NumPatronal", "C7016", Types.NUMERIC )
     ,  new SQLColumnaMeta(7, "BS_EmpresaODepto", "E7", 7017, "RequiereCompro", "C7017", Types.BIGINT )
     ,  new SQLColumnaMeta(7, "BS_EmpresaODepto", "E7", 7018, "EsConsumidorFinal", "C7018", Types.BIGINT )
     ,  new SQLColumnaMeta(7, "BS_EmpresaODepto", "E7", 7019, "GiroEmpresa", "C7019", Types.VARCHAR )
     ,  new SQLColumnaMeta(7, "BS_EmpresaODepto", "E7", 7020, "Ciudad", "C7020", Types.VARCHAR )
     ,  new SQLColumnaMeta(7, "BS_EmpresaODepto", "E7", 7021, "Cargo", "C7021", Types.VARCHAR )
     ,  new SQLColumnaMeta(7, "BS_EmpresaODepto", "E7", 7022, "LimiteXArchivo", "C7022", Types.NUMERIC )
     ,  new SQLColumnaMeta(7, "BS_EmpresaODepto", "E7", 7023, "LimiteXLote", "C7023", Types.NUMERIC )
     ,  new SQLColumnaMeta(7, "BS_EmpresaODepto", "E7", 7024, "LimiteXTransaccion", "C7024", Types.NUMERIC )
	});
	
	


	/**
	 * Atributo que almacena el <code>String</code> para obtener el listado de registros del negocio.
	 */
	public static final String SELECT_HEADER_SQL =  
		"SELECT E7.EmpresaODepto AS C7001 , E7.Cliente AS C7002 , E7.Nombre AS C7003 , E7.Direccion AS C7004 , " +
		"       E7.Telefono AS C7005 , E7.Fax AS C7006 , E7.Email AS C7007 , E7.NombreContacto AS C7008 , " +
		"       E7.ApellidoContacto AS C7009 , E7.Proveedor AS C7010 , E7.NivelSeguridad AS C7011 , E7.FechaEstatus AS C7012 , " +
		"       E7.Estatus AS C7013 , E7.OrigenLotes AS C7014 , E7.RegistroFiscal AS C7015 , E7.NumPatronal AS C7016 , " +
		"       E7.RequiereCompro AS C7017 , E7.EsConsumidorFinal AS C7018 , E7.GiroEmpresa AS C7019 , E7.Ciudad AS C7020 , " +
		"       E7.Cargo AS C7021 , E7.LimiteXArchivo AS C7022 , E7.LimiteXLote AS C7023 , E7.LimiteXTransaccion AS C7024" +
		"  FROM BS_EmpresaODepto E7";
	/**
	 * Atributo que almacena el <code>String</code> para obtener el n&uacute;mero de registros presentes en el negocio.
	 */
	public static final String COUNT_HEADER_SQL =   "SELECT COUNT(*) FROM BS_EmpresaODepto";    												
	/**
	 * Atributo que almacena el <code>String</code> para la inserci&oacute;n, sin id, de un nuevo registro en la tabla.
	 */
	public static final String CREATE_SQL = 
		"INSERT INTO BS_EmpresaODepto (" +
		"Cliente,Nombre,Direccion,Telefono,Fax" +
		",Email,NombreContacto,ApellidoContacto,Proveedor" +
		",NivelSeguridad,FechaEstatus,Estatus,OrigenLotes" +
		",RegistroFiscal,NumPatronal,RequiereCompro,EsConsumidorFinal" +
		",GiroEmpresa,Ciudad,Cargo,LimiteXArchivo" +
		",LimiteXLote,LimiteXTransaccion" +
		") VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	/**
	 * Atributo que almacena el <code>String</code> para insertar, con id, un registro en la table.
	 */
	public static final String CREATE_SQL_CON_ID = 
		"INSERT INTO BS_EmpresaODepto (" +
		"EmpresaODepto,Cliente,Nombre,Direccion,Telefono" +
		",Fax,Email,NombreContacto,ApellidoContacto" +
		",Proveedor,NivelSeguridad,FechaEstatus,Estatus" +
		",OrigenLotes,RegistroFiscal,NumPatronal,RequiereCompro" +
		",EsConsumidorFinal,GiroEmpresa,Ciudad,Cargo" +
		",LimiteXArchivo,LimiteXLote,LimiteXTransaccion" +
		") VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	/**
	 * Atributo que almacena el <code>String</code> para la atualizaci&oacute; de un registro en la tabla.
	 */
	public static final String UPDATE_SQL = 
		"UPDATE BS_EmpresaODepto" +
		" SET Cliente= ?  , Nombre= ?  , Direccion= ?  , Telefono= ?  , " +
		"Fax= ?  , Email= ?  , NombreContacto= ?  , ApellidoContacto= ?  , " +
		"Proveedor= ?  , NivelSeguridad= ?  , FechaEstatus= ?  , Estatus= ?  , " +
		"OrigenLotes= ?  , RegistroFiscal= ?  , NumPatronal= ?  , RequiereCompro= ?  , " +
		"EsConsumidorFinal= ?  , GiroEmpresa= ?  , Ciudad= ?  , Cargo= ?  , " +
		"LimiteXArchivo= ?  , LimiteXLote= ?  , LimiteXTransaccion= ? " +
		" WHERE EmpresaODepto= ? ";
	/**
	 * Atributo que almacena el <code>String</code> para la eliminaci&oacute; de un registro en la tabla.
	 */
	public static final String DELETE_SQL =  
		"DELETE FROM BS_EmpresaODepto" +
		" WHERE EmpresaODepto= ? ";
	/**
	 * Atributo que almacena el <code>String</code> para la b&uacute;squeda por id de un registro de la tabla.
	 */
	public static final String FIND_BY_ID_SQL = 
		"SELECT E7.EmpresaODepto AS C7001 , E7.Cliente AS C7002 , E7.Nombre AS C7003 , E7.Direccion AS C7004 , " +
		"       E7.Telefono AS C7005 , E7.Fax AS C7006 , E7.Email AS C7007 , E7.NombreContacto AS C7008 , " +
		"       E7.ApellidoContacto AS C7009 , E7.Proveedor AS C7010 , E7.NivelSeguridad AS C7011 , E7.FechaEstatus AS C7012 , " +
		"       E7.Estatus AS C7013 , E7.OrigenLotes AS C7014 , E7.RegistroFiscal AS C7015 , E7.NumPatronal AS C7016 , " +
		"       E7.RequiereCompro AS C7017 , E7.EsConsumidorFinal AS C7018 , E7.GiroEmpresa AS C7019 , E7.Ciudad AS C7020 , " +
		"       E7.Cargo AS C7021 , E7.LimiteXArchivo AS C7022 , E7.LimiteXLote AS C7023 , E7.LimiteXTransaccion AS C7024" +
		"  FROM BS_EmpresaODepto E7" +
		" WHERE E7.EmpresaODepto= ? ";
	/**
	 * Atributo que almacena el <code>String</code> para la creacion de la tabla en la base de datos.
	 */
	public static final String CREATE_TABLE_DDL = 
		"CREATE TABLE BS_EmpresaODepto (" +
		"    EmpresaODepto LONG NOT NULL," +
		"    Cliente LONG NOT NULL," +
		"    Nombre VARCHAR(50)  NOT NULL," +
		"    Direccion VARCHAR(50) ," +
		"    Telefono VARCHAR(40) ," +
		"    Fax VARCHAR(30) ," +
		"    Email VARCHAR(75) ," +
		"    NombreContacto VARCHAR(30) ," +
		"    ApellidoContacto VARCHAR(30) ," +
		"    Proveedor LONG," +
		"    NivelSeguridad LONG NOT NULL," +
		"    FechaEstatus TIMESTAMP NOT NULL," +
		"    Estatus LONG NOT NULL," +
		"    OrigenLotes LONG NOT NULL," +
		"    RegistroFiscal VARCHAR(15) ," +
		"    NumPatronal NUMERIC(9, 0) ," +
		"    RequiereCompro LONG NOT NULL," +
		"    EsConsumidorFinal LONG NOT NULL," +
		"    GiroEmpresa VARCHAR(50)  NOT NULL," +
		"    Ciudad VARCHAR(50)  NOT NULL," +
		"    Cargo VARCHAR(100)  NOT NULL," +
		"    LimiteXArchivo NUMERIC(19, 0)  NOT NULL," +
		"    LimiteXLote NUMERIC(19, 0)  NOT NULL," +
		"    LimiteXTransaccion NUMERIC(19, 0)  NOT NULL" +
		")";
	/**
	 * Atributo que almacena un objeto del tipo {@link SQLLenguaje}.
	 */
	private static SQLLenguaje lenguaje;
	
	/**
	 * M&eacute;todo que realiza la creaci&oacute;n de un DTO, del negocio, a partir del resultado de alguna consulta.
	 * @param <code>rst</code> objeto de tipo <code>Resultset</code>, que contiene los resultados de la consulta.
	 * @return <code>Object</code> - representa al DTO creado.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static Object crearDtoST(ResultSet rst)
			throws SQLException, ExcepcionEnDAO {
 	    BsEmpresaodepto theDto = new BsEmpresaodepto();
     	theDto.setEmpresaodepto(SQLUtils.getLongFromResultSet(rst, "C7001"));
     	theDto.setCliente(SQLUtils.getLongFromResultSet(rst, "C7002"));
		theDto.setNombre(rst.getString("C7003"));
		theDto.setDireccion(rst.getString("C7004"));
		theDto.setTelefono(rst.getString("C7005"));
		theDto.setFax(rst.getString("C7006"));
		theDto.setEmail(rst.getString("C7007"));
		theDto.setNombrecontacto(rst.getString("C7008"));
		theDto.setApellidocontacto(rst.getString("C7009"));
     	theDto.setProveedor(SQLUtils.getLongFromResultSet(rst, "C7010"));
     	theDto.setNivelseguridad(SQLUtils.getLongFromResultSet(rst, "C7011"));
		theDto.setFechaestatus(rst.getTimestamp("C7012"));
     	theDto.setEstatus(SQLUtils.getLongFromResultSet(rst, "C7013"));
     	theDto.setOrigenlotes(SQLUtils.getLongFromResultSet(rst, "C7014"));
		theDto.setRegistrofiscal(rst.getString("C7015"));
		theDto.setNumpatronal(rst.getBigDecimal("C7016"));
     	theDto.setRequierecompro(SQLUtils.getLongFromResultSet(rst, "C7017"));
     	theDto.setEsconsumidorfinal(SQLUtils.getLongFromResultSet(rst, "C7018"));
		theDto.setGiroempresa(rst.getString("C7019"));
		theDto.setCiudad(rst.getString("C7020"));
		theDto.setCargo(rst.getString("C7021"));
		theDto.setLimitexarchivo(rst.getBigDecimal("C7022"));
		theDto.setLimitexlote(rst.getBigDecimal("C7023"));
		theDto.setLimitextransaccion(rst.getBigDecimal("C7024"));
		return theDto;
	}
	
	







	/**
	 * M&eacute;todo que coloca los valores provenientes del objeto Dto en el objeto PreparedStatement,
	 * el cual proviene de la llamada a la implementacion de {@link SQLOperaciones}, este m&eacute;todo
	 * no toma en cuenta el valor del <code>id</code>, puesto que se asume que es un valor secuencial
	 * autonumerico o serial propio de la base de datos.
	 * 
	 * @param <code>theDtoUntyped</code> objeto sin tipo que almacena al DTO del negocio.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaCrearST(Object theDtoUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {			
	        BsEmpresaodepto theDto = (BsEmpresaodepto)theDtoUntyped;
				Long cliente = theDto.getCliente();     
		String nombre = theDto.getNombre();     
		String direccion = theDto.getDireccion();     
		String telefono = theDto.getTelefono();     
		String fax = theDto.getFax();     
		String email = theDto.getEmail();     
		String nombrecontacto = theDto.getNombrecontacto();     
		String apellidocontacto = theDto.getApellidocontacto();     
		Long proveedor = theDto.getProveedor();     
		Long nivelseguridad = theDto.getNivelseguridad();     
		java.util.Date fechaestatus = theDto.getFechaestatus();     
		Long estatus = theDto.getEstatus();     
		Long origenlotes = theDto.getOrigenlotes();     
		String registrofiscal = theDto.getRegistrofiscal();     
		java.math.BigDecimal numpatronal = theDto.getNumpatronal();     
		Long requierecompro = theDto.getRequierecompro();     
		Long esconsumidorfinal = theDto.getEsconsumidorfinal();     
		String giroempresa = theDto.getGiroempresa();     
		String ciudad = theDto.getCiudad();     
		String cargo = theDto.getCargo();     
		java.math.BigDecimal limitexarchivo = theDto.getLimitexarchivo();     
		java.math.BigDecimal limitexlote = theDto.getLimitexlote();     
		java.math.BigDecimal limitextransaccion = theDto.getLimitextransaccion();     
		     
		SQLUtils.setIntoStatement( 1, cliente, smt);  
    		     
		SQLUtils.setIntoStatement( 2, nombre, smt);  
    		     
		SQLUtils.setIntoStatement( 3, direccion, smt);  
    		     
		SQLUtils.setIntoStatement( 4, telefono, smt);  
    		     
		SQLUtils.setIntoStatement( 5, fax, smt);  
    		     
		SQLUtils.setIntoStatement( 6, email, smt);  
    		     
		SQLUtils.setIntoStatement( 7, nombrecontacto, smt);  
    		     
		SQLUtils.setIntoStatement( 8, apellidocontacto, smt);  
    		     
		SQLUtils.setIntoStatement( 9, proveedor, smt);  
    		     
		SQLUtils.setIntoStatement( 10, nivelseguridad, smt);  
    		     
		SQLUtils.setIntoStatement( 11, fechaestatus, smt);  
    		     
		SQLUtils.setIntoStatement( 12, estatus, smt);  
    		     
		SQLUtils.setIntoStatement( 13, origenlotes, smt);  
    		     
		SQLUtils.setIntoStatement( 14, registrofiscal, smt);  
    		     
		SQLUtils.setIntoStatement( 15, numpatronal, smt);  
    		     
		SQLUtils.setIntoStatement( 16, requierecompro, smt);  
    		     
		SQLUtils.setIntoStatement( 17, esconsumidorfinal, smt);  
    		     
		SQLUtils.setIntoStatement( 18, giroempresa, smt);  
    		     
		SQLUtils.setIntoStatement( 19, ciudad, smt);  
    		     
		SQLUtils.setIntoStatement( 20, cargo, smt);  
    		     
		SQLUtils.setIntoStatement( 21, limitexarchivo, smt);  
    		     
		SQLUtils.setIntoStatement( 22, limitexlote, smt);  
    		     
		SQLUtils.setIntoStatement( 23, limitextransaccion, smt);  
    		
	}

	/**
	 * M&eacute;todo que coloca los valores provenientes del objeto Dto en el objeto PreparedStatement,
	 * el cual proviene de la llamada a la implementacion de {@link SQLOperaciones}, este m&eacute;todo
	 * toma en cuenta el <code>id</code>, lo que quiere decir que se usa al momento de  crear registros
	 * en donde el identificador no es un valor secuencial autonumerico o serial propio de la base de datos
	 *
	 * @param <code>theDtoUntyped</code> objeto sin tipo que almacena al DTO del negocio.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaCrearConIdST(Object theDtoUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {			
	        BsEmpresaodepto theDto = (BsEmpresaodepto)theDtoUntyped;
				Long empresaodepto = theDto.getEmpresaodepto();     
		Long cliente = theDto.getCliente();     
		String nombre = theDto.getNombre();     
		String direccion = theDto.getDireccion();     
		String telefono = theDto.getTelefono();     
		String fax = theDto.getFax();     
		String email = theDto.getEmail();     
		String nombrecontacto = theDto.getNombrecontacto();     
		String apellidocontacto = theDto.getApellidocontacto();     
		Long proveedor = theDto.getProveedor();     
		Long nivelseguridad = theDto.getNivelseguridad();     
		java.util.Date fechaestatus = theDto.getFechaestatus();     
		Long estatus = theDto.getEstatus();     
		Long origenlotes = theDto.getOrigenlotes();     
		String registrofiscal = theDto.getRegistrofiscal();     
		java.math.BigDecimal numpatronal = theDto.getNumpatronal();     
		Long requierecompro = theDto.getRequierecompro();     
		Long esconsumidorfinal = theDto.getEsconsumidorfinal();     
		String giroempresa = theDto.getGiroempresa();     
		String ciudad = theDto.getCiudad();     
		String cargo = theDto.getCargo();     
		java.math.BigDecimal limitexarchivo = theDto.getLimitexarchivo();     
		java.math.BigDecimal limitexlote = theDto.getLimitexlote();     
		java.math.BigDecimal limitextransaccion = theDto.getLimitextransaccion();     
	
		SQLUtils.setIntoStatement( 1, empresaodepto, smt);
    	
		SQLUtils.setIntoStatement( 2, cliente, smt);
    	
		SQLUtils.setIntoStatement( 3, nombre, smt);
    	
		SQLUtils.setIntoStatement( 4, direccion, smt);
    	
		SQLUtils.setIntoStatement( 5, telefono, smt);
    	
		SQLUtils.setIntoStatement( 6, fax, smt);
    	
		SQLUtils.setIntoStatement( 7, email, smt);
    	
		SQLUtils.setIntoStatement( 8, nombrecontacto, smt);
    	
		SQLUtils.setIntoStatement( 9, apellidocontacto, smt);
    	
		SQLUtils.setIntoStatement( 10, proveedor, smt);
    	
		SQLUtils.setIntoStatement( 11, nivelseguridad, smt);
    	
		SQLUtils.setIntoStatement( 12, fechaestatus, smt);
    	
		SQLUtils.setIntoStatement( 13, estatus, smt);
    	
		SQLUtils.setIntoStatement( 14, origenlotes, smt);
    	
		SQLUtils.setIntoStatement( 15, registrofiscal, smt);
    	
		SQLUtils.setIntoStatement( 16, numpatronal, smt);
    	
		SQLUtils.setIntoStatement( 17, requierecompro, smt);
    	
		SQLUtils.setIntoStatement( 18, esconsumidorfinal, smt);
    	
		SQLUtils.setIntoStatement( 19, giroempresa, smt);
    	
		SQLUtils.setIntoStatement( 20, ciudad, smt);
    	
		SQLUtils.setIntoStatement( 21, cargo, smt);
    	
		SQLUtils.setIntoStatement( 22, limitexarchivo, smt);
    	
		SQLUtils.setIntoStatement( 23, limitexlote, smt);
    	
		SQLUtils.setIntoStatement( 24, limitextransaccion, smt);
    		
	}

	/**
	 * M&eacute;todo que coloca el identificador recibido en el objeto PreparedStatement, para poder ser eliminado.
	 * @param <code>theIdUntyped</code> objeto sin tipo que identifica al <code>id</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaEliminarST(Object theIdUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {
			Long theId = (Long)theIdUntyped;
		SQLUtils.setIntoStatement( 1, theId, smt);
	}

	/**
	 * M&eacute;todo que coloca el identificador recibido, en el objeto PreparedStatement para poder hacer la consulta de busqueda.
	 * @param <code>theIdUntyped</code> objeto sin tipo que identifica al <code>id</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaBuscarPorIdST(Object theIdUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {
			Long theId = (Long)theIdUntyped;
			SQLUtils.setIntoStatement( 1, theId, smt);
		}

	/**
	 * M&eacute;todo que coloca los valores provenientes del objeto Dto en el objeto PreparedStatement,
	 * el cual proviene de la llamada a la implementacion de {@link SQLOperaciones} para poder actualizar
     * un registro en espec&iacute;fico.
	 * 
	 * @param <code>theDtoUntyped</code> objeto sin tipo que almacena al DTO del negocio.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaActualizarST(Object theDtoUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {
			BsEmpresaodepto theDto = (BsEmpresaodepto)theDtoUntyped;
				Long empresaodepto = theDto.getEmpresaodepto();     
		Long cliente = theDto.getCliente();     
		String nombre = theDto.getNombre();     
		String direccion = theDto.getDireccion();     
		String telefono = theDto.getTelefono();     
		String fax = theDto.getFax();     
		String email = theDto.getEmail();     
		String nombrecontacto = theDto.getNombrecontacto();     
		String apellidocontacto = theDto.getApellidocontacto();     
		Long proveedor = theDto.getProveedor();     
		Long nivelseguridad = theDto.getNivelseguridad();     
		java.util.Date fechaestatus = theDto.getFechaestatus();     
		Long estatus = theDto.getEstatus();     
		Long origenlotes = theDto.getOrigenlotes();     
		String registrofiscal = theDto.getRegistrofiscal();     
		java.math.BigDecimal numpatronal = theDto.getNumpatronal();     
		Long requierecompro = theDto.getRequierecompro();     
		Long esconsumidorfinal = theDto.getEsconsumidorfinal();     
		String giroempresa = theDto.getGiroempresa();     
		String ciudad = theDto.getCiudad();     
		String cargo = theDto.getCargo();     
		java.math.BigDecimal limitexarchivo = theDto.getLimitexarchivo();     
		java.math.BigDecimal limitexlote = theDto.getLimitexlote();     
		java.math.BigDecimal limitextransaccion = theDto.getLimitextransaccion();     
		     
		SQLUtils.setIntoStatement( 1, cliente, smt);  
        	     
		SQLUtils.setIntoStatement( 2, nombre, smt);  
        	     
		SQLUtils.setIntoStatement( 3, direccion, smt);  
        	     
		SQLUtils.setIntoStatement( 4, telefono, smt);  
        	     
		SQLUtils.setIntoStatement( 5, fax, smt);  
        	     
		SQLUtils.setIntoStatement( 6, email, smt);  
        	     
		SQLUtils.setIntoStatement( 7, nombrecontacto, smt);  
        	     
		SQLUtils.setIntoStatement( 8, apellidocontacto, smt);  
        	     
		SQLUtils.setIntoStatement( 9, proveedor, smt);  
        	     
		SQLUtils.setIntoStatement( 10, nivelseguridad, smt);  
        	     
		SQLUtils.setIntoStatement( 11, fechaestatus, smt);  
        	     
		SQLUtils.setIntoStatement( 12, estatus, smt);  
        	     
		SQLUtils.setIntoStatement( 13, origenlotes, smt);  
        	     
		SQLUtils.setIntoStatement( 14, registrofiscal, smt);  
        	     
		SQLUtils.setIntoStatement( 15, numpatronal, smt);  
        	     
		SQLUtils.setIntoStatement( 16, requierecompro, smt);  
        	     
		SQLUtils.setIntoStatement( 17, esconsumidorfinal, smt);  
        	     
		SQLUtils.setIntoStatement( 18, giroempresa, smt);  
        	     
		SQLUtils.setIntoStatement( 19, ciudad, smt);  
        	     
		SQLUtils.setIntoStatement( 20, cargo, smt);  
        	     
		SQLUtils.setIntoStatement( 21, limitexarchivo, smt);  
        	     
		SQLUtils.setIntoStatement( 22, limitexlote, smt);  
        	     
		SQLUtils.setIntoStatement( 23, limitextransaccion, smt);  
        		     
		SQLUtils.setIntoStatement( 24, empresaodepto, smt);  
    			
	}


	/**
	 * M&eacute;todo que ejecuta una petici&oacute;n de datos a trav&eacute;s de un objeto {@link PreparedStatement},
	 * y un objeto del tipo {@link PeticionDeDatos}, el cual en su implementaci&oacute;n define una serie de restricciones
	 * usadas para hacer la petici&oacute;n.
	 *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaPeticionDeDatosST(PeticionDeDatos peticion,
			PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
		SQLImplementacionOperacionesComunes.operacionesParaPeticionDeDatosST( peticion, smt, SQL_OPERACIONES, null);	
	}	
	
	/**
	 * M&eacute;todo que ejecuta una petici&oacute;n de datos a trav&eacute;s de un objeto {@link PreparedStatement},
     * y un objeto del tipo {@link PeticionDeDatos}, el cual en su implementaci&oacute;n define una serie de restricciones
     * usadas para hacer la petici&oacute;n.
	 *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaPeticionDeDatosFST(PeticionDeDatos peticion,
			PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
		SQLImplementacionOperacionesComunes.operacionesParaPeticionDeDatosST( peticion, smt, SQL_OPERACIONES, null);	
	}	

	/**
	 * M&eacute;todo que ejecuta una petici&oacute;n de conteo a trav&eacute;s de un objeto {@link PreparedStatement},
     * y un objeto del tipo {@link PeticionDeDatos}, el cual en su implementaci&oacute;n define una serie de restricciones
     * usadas para hacer la petici&oacute;n.
	 *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaConteoST(PeticionDeDatos peticion,
			PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
		SQLImplementacionOperacionesComunes.operacionesParaPeticionDeDatosST( peticion, smt, SQL_OPERACIONES, null);
	}

	/**
	 * M&eacute;todo que agrega al atributo {@link #SELECT_HEADER_SQL} las condiciones de b&uacute;squeda
     * que vienen en el objeto del tipo {@link PeticionDeDatos}
     *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>, que contiene las condiciones de la consulta.
	 * @return <code>String</code> - la consulta condicionada como cadena de caracteres.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static String getSqlParaPeticionDeDatosST(PeticionDeDatos peticion)
			throws SQLException, ExcepcionEnDAO {
		return SELECT_HEADER_SQL + SQLImplementacionOperacionesComunes.crearCondicionesSQLST( peticion, SQL_OPERACIONES, SQLImplementacionOperacionesComunes.COND_ALIAS_ORDEN_WHERE);
	}
	
	
	
	

	/**
	 * M&eacute;todo que agrega al atributo {@link #COUNT_HEADER_SQL} las condiciones de conteo
	 * que vienen en el objeto de tipo {@link PeticionDeDatos}.
	 *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>, que contiene las condiciones de la consulta.
	 * @return <code>String</code> - la consulta condicionada como cadena de caracteres.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static String getSqlDeConteoST(PeticionDeDatos peticion)
			throws SQLException, ExcepcionEnDAO {
		return COUNT_HEADER_SQL + SQLImplementacionOperacionesComunes.crearCondicionesSQLST( peticion, SQL_OPERACIONES, SQLImplementacionOperacionesComunes.COND_SQL_WHERE);
	}
	
	/**
	 * M&eacute;todo que se utiliza para actualizar un registro a nivel de tabla, recibe como par&aacute;metro un DTO del negocio
	 * que contiene la informaci&oacute;n a actualizar, e invoca la funci&oacute;n <code>actualizar</code> de la clase de la que hereda.
	 *
	 * @param <code>bsEmpresaodepto</code> objeto del tipo <code>BsEmpresaodepto</code>.
	 * @throws ExcepcionEnDAO
	 * @see {@link SQLImplementacionBasica}
	 */
	public void actualizar(BsEmpresaodepto bsEmpresaodepto) throws ExcepcionEnDAO {
	    actualizar(bsEmpresaodepto, AUTO_COMMIT);
	
	}
	
	/**
	 * M&eacute;todo utilizado para realizar una b&uacute;squeda de un registro espec&iacute;fico, utilizando un identificador.
	 *
	 * @param <code>empresaodeptoId</code> objeto del tipo <code>Long</code>.
	 * @return <code>BsEmpresaodepto</code> - un objeto DTO.
	 * @throws ExcepcionEnDAO
	 * @see {@link SQLImplementacionBasica}
	 */
	public BsEmpresaodepto buscarPorID(Long empresaodeptoId)
			throws ExcepcionEnDAO {
		return (BsEmpresaodepto)buscarPorID(empresaodeptoId, AUTO_COMMIT);				 
	}

	/**
	 * M&eacute;todo utilizado para la creaci&oacute;n de un registro, a nivel de tabla, a partir de la informaci&oacute;n
	 * recibida en un DTO del objeto de Negocio, e invoca la funci&oacute;n <code>crear</code> de la clase de la que hereda.
	 * 
	 * @param <code>bsEmpresaodepto</code> objeto del tipo <code>BsEmpresaodepto</code>.
	 * @throws ExcepcionEnDAO
	 * @see {@link SQLImplementacionBasica}
	 */
	public void crear(BsEmpresaodepto bsEmpresaodepto) throws ExcepcionEnDAO {
	    crear(bsEmpresaodepto, AUTO_COMMIT);		
	}

	/**
	 * M&eacute;todo utilizado para la eliminaci&oacute;n de un registro, a nivel de tabla,
	 * que se busca a trav&eacute;s del identificador recibido. Se invoca la funci&oacute;n <code>eliminar</code> de la clase de la que hereda.
	 *
	 * @param <code>empresaodeptoId</code> objeto del tipo <code>Long</code>.
	 * @throws ExcepcionEnDAO
	 * @see {@link SQLImplementacionBasica}
	 */
	public void eliminar(Long empresaodeptoId) throws ExcepcionEnDAO {
	    eliminar(empresaodeptoId, AUTO_COMMIT);		
	}

	/**
	 * M&eacute;todo utilizado para devolver un &uacute;nico resultado de la petici&oacute;n recibida,
	 * la cual se realiza a trav&eacuate;s del objeto de tipo {@link PeticionDeDatos}.
	 * 
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>, que contiene las condiciones de la consulta.
	 * @return <code>BsEmpresaodepto</code> - un objeto DTO.
	 * @throws ExcepcionEnDAO
	 * @throws ExcepcionPorDatoNoEncontrado
	 * @see {@link SQLImplementacionBasica}
	 */
	public BsEmpresaodepto resultadoUnico(PeticionDeDatos peticion)
			throws ExcepcionEnDAO, ExcepcionPorDatoNoEncontrado {
		return (BsEmpresaodepto)resultadoUnico(peticion, AUTO_COMMIT);
	}
	
	/**
	 * Atributo del tipo {@link SQLOperaciones}, almacena la operaciones disponibles para esta implementaci&oacute;n.<br/>
	 * Algunas de las llamadas a los m&eacute;todos se realiza a trav&eacute;s de la invocaci&oacute;n de estas implementaciones.
	 */
    public static final SQLOperaciones	SQL_OPERACIONES =  new SQLOperaciones() {
			
			public int getManejoForaneosPorDefecto() {
				return Dao.SIN_FORANEOS;
			}
			
			
			public void operacionesParaPeticionDeDatosF(PeticionDeDatos request,
					PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
				BsEmpresaodeptoDaoJdbcSql.operacionesParaPeticionDeDatosFST(request, smt);
			}
			
			public void operacionesParaPeticionDeDatos(PeticionDeDatos request,
					PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
				BsEmpresaodeptoDaoJdbcSql.operacionesParaPeticionDeDatosST(request, smt);
			}
			
			public void operacionesParaEliminar(Object theId, PreparedStatement smt)
					throws SQLException, ExcepcionEnDAO {
				BsEmpresaodeptoDaoJdbcSql.operacionesParaEliminarST(theId, smt);
			}
			
			public void operacionesParaCrear(Object theDto, PreparedStatement smt)
					throws SQLException, ExcepcionEnDAO {
				BsEmpresaodeptoDaoJdbcSql.operacionesParaCrearST(theDto, smt);
			}
			
			public void operacionesParaCrearConId(Object theDto, PreparedStatement smt) 
			        throws SQLException, ExcepcionEnDAO {
			    BsEmpresaodeptoDaoJdbcSql.operacionesParaCrearConIdST(theDto, smt);
            }
			
			public void operacionesParaConteo(PeticionDeDatos request,
					PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
				BsEmpresaodeptoDaoJdbcSql.operacionesParaConteoST(request, smt);				
			}
			
			public void operacionesParaBuscarPorId(Object theId, PreparedStatement smt)
					throws SQLException, ExcepcionEnDAO {
				BsEmpresaodeptoDaoJdbcSql.operacionesParaBuscarPorIdST(theId, smt);
			}
			
			public void operacionesParaActualizar(Object theDto, PreparedStatement smt)
					throws SQLException, ExcepcionEnDAO {
				BsEmpresaodeptoDaoJdbcSql.operacionesParaActualizarST(theDto, smt);
				
			}
			
			public String getSqlParaPeticionDeDatosF(PeticionDeDatos request)
					throws SQLException, ExcepcionEnDAO {	
				return null;
			}
			
			public String getSqlParaPeticionDeDatos(PeticionDeDatos request)
					throws SQLException, ExcepcionEnDAO {
				return BsEmpresaodeptoDaoJdbcSql.getSqlParaPeticionDeDatosST(request);
			}
			
			public String getSqlDeConteo(PeticionDeDatos request) throws SQLException,
					ExcepcionEnDAO {
				return BsEmpresaodeptoDaoJdbcSql.getSqlDeConteoST(request);
			}
			
			public SQLEntidadMeta getSQLEntidadMeta() throws ExcepcionEnDAO {
				return BsEmpresaodeptoDaoJdbcSql.SQL_ENTIDAD_META;
			}
			
			public String getEliminarSql() {
				return  BsEmpresaodeptoDaoJdbcSql.DELETE_SQL;
			}
			
			public String getCrearSql() {
				return  BsEmpresaodeptoDaoJdbcSql.CREATE_SQL;
			}
			
			public String getCrearSqlConId(){
			    return  BsEmpresaodeptoDaoJdbcSql.CREATE_SQL_CON_ID;
			}			
			
			public String getBuscarPorIdSqlF() {
				return null;
			}
			
			public String getBuscarPorIdSql() {				
				return BsEmpresaodeptoDaoJdbcSql.FIND_BY_ID_SQL;
			}
			
			public String getActualizarSql() {				
				return BsEmpresaodeptoDaoJdbcSql.UPDATE_SQL;
			}
			
			public Object crearDto(ResultSet rst) throws SQLException, ExcepcionEnDAO {
				return BsEmpresaodeptoDaoJdbcSql.crearDtoST(rst);
			}
	        
	        public Object crearDataConValoresForaneos(ResultSet rst)
					throws SQLException, ExcepcionEnDAO {
				return null;
			}
			
	        public Object crearDataConValoresForaneosNoRel(ResultSet rst, Connection conn)
					throws SQLException, ExcepcionEnDAO {
				return null;
			}
			
			
            public boolean isForaneosDisponible(){
				return false;
            
            }
            
            public SQLLenguaje getSQLLenguaje() {
                return BsEmpresaodeptoDaoJdbcSql.getLenguaje();
            }
            
            public void setIdEnDto(Object theDto, Object dtoId) {
                setIdEnDtoST(theDto, dtoId);
            }
	};
	
	
	
	
	
	/**
	 * Devuelve el atributo {@link #SQL_OPERACIONES}.
	 * @return <code>SQLOperaciones</code> - objeto del tipo {@link SQLOperaciones}
	 */
    public SQLOperaciones getSqlOperaciones() {
       return SQL_OPERACIONES;
	}
	
	/**
	 * Devuelve el atributo {@link #lenguaje}
	 * @return <code>SQLLenguaje</code> - objeto del tipo {@link SQLLenguaje}
	 */	
	public static SQLLenguaje getLenguaje() {
		return lenguaje;
	}
	
	/**
	 * M&eacute;todo que permite obtener a trav&eacute;s de un Servicio la implementaci&oacute;n del objeto de Lenguaje,
	 * y coloca las reglas de negocio almacenadas en el atributo {@link #SQL_ENTIDAD_META}.
	 * @param <code>localizadorLenguaje</code> objeto del tipo {@link LocalizadorDeServicio} que posee informaci&oacute;n para trabajar con la base de datos.
	 */
	public static void setLocalizadorLenguaje(LocalizadorDeServicio localizadorLenguaje) {
		lenguaje = (SQLLenguaje) localizadorLenguaje.getServicio();
		lenguaje.setSqlEntidadMeta(SQL_ENTIDAD_META);
	}

	/**
	 * M&eacute;todo que agrega un identificador a un DTO del tipo <code>BsEmpresaodepto</code>.
	 * @param <code>theDtoUntyped</code> objeto gen&eacute;rico, que representa a un <code>DTO</code>.
	 * @param <code>dtoIdUntyped</code> objeto gen&eacute;rico, que representa al identificador.
	 */
	public static void setIdEnDtoST(Object theDtoUntyped, Object dtoIdUntyped){
	    BsEmpresaodepto dto = (BsEmpresaodepto)theDtoUntyped;
        Long dtoId = (Long) dtoIdUntyped;;
        dto.setEmpresaodepto(dtoId);

	}	
	
	
}
