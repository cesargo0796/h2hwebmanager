package com.ofbox.davivienda.h2h.orm.jdbc.sql.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.sql.Connection;
import java.util.Collection;

import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Dao;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.ExcepcionEnDAO;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.ExcepcionPorDatoNoEncontrado;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.PeticionDeDatos;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Transaccion;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.foraneos.DataConValoresForaneos;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLColumnaMeta;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLEntidadMeta;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLImplementacionBasica;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLImplementacionOperacionesComunes;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLLenguaje;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLOperaciones;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLUtils;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.TransaccionJdbc;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dependencias.LocalizadorDeServicio;


import com.ofbox.davivienda.h2h.abstraccion.dto.HthFormato;
import java.lang.Long;
import com.ofbox.davivienda.h2h.abstraccion.dao.HthFormatoDao;




/**
 * Clase que hereda comportamientos de {@link SQLImplementacionBasica} e implementa el comportamiento definido en la capa DAO.<br/>
 * Esta clase es una implementaci&oacute;n intermedia para el DAO de <b>Formato</b>
 * asociado a la tabla <b>HTH_Formato</b>, en esta clase se construyen todos los sql para las operaciones de listado,
 * creaci&oacute;n, consulta, actualizaci&oacute;n y elminaci&oacute;n.<br/>
 * @author Systems Out of the Box
 * @version f3.1
 *
 */
public class HthFormatoDaoJdbcSql extends SQLImplementacionBasica implements HthFormatoDao{

	/**
	 * Atributo del tipo {@link SQLEntidadMeta}, que almacena la descripci&oacute;n del negocio como una entidad.<br/>
	 * Entre sus caracter&iacute;sticas tiene el nombre de la tabla, nombre de la secuencia, posici&oacute;n de las columnas,
	 * nombre de las columnas y tipos de datos para la columna.<br/>
	 * El arreglo que recibe son objetos de tipo {@link SQLColumnaMeta}, y definen informaci&oacute;n para obtener las columnas.
	 */
	public static final SQLEntidadMeta SQL_ENTIDAD_META = new SQLEntidadMeta("HTH_Formato", new SQLColumnaMeta[]{
        new SQLColumnaMeta(16, "HTH_Formato", "E16", 16001, "id_formato", "C16001", Types.BIGINT ,true)
     ,  new SQLColumnaMeta(16, "HTH_Formato", "E16", 16002, "nombre_formato", "C16002", Types.VARCHAR )
     ,  new SQLColumnaMeta(16, "HTH_Formato", "E16", 16003, "tipo_formato", "C16003", Types.VARCHAR )
     ,  new SQLColumnaMeta(16, "HTH_Formato", "E16", 16004, "tipo_separador", "C16004", Types.VARCHAR )
     ,  new SQLColumnaMeta(16, "HTH_Formato", "E16", 16005, "caracter_separador", "C16005", Types.VARCHAR )
     ,  new SQLColumnaMeta(16, "HTH_Formato", "E16", 16006, "lote_destino", "C16006", Types.VARCHAR )
     ,  new SQLColumnaMeta(16, "HTH_Formato", "E16", 16007, "usuario_creacion", "C16007", Types.VARCHAR )
     ,  new SQLColumnaMeta(16, "HTH_Formato", "E16", 16008, "fecha_creacion", "C16008", Types.TIMESTAMP )
     ,  new SQLColumnaMeta(16, "HTH_Formato", "E16", 16009, "usuario_ult_act", "C16009", Types.VARCHAR )
     ,  new SQLColumnaMeta(16, "HTH_Formato", "E16", 16010, "fecha_ult_act", "C16010", Types.TIMESTAMP )
	});
	
	


	/**
	 * Atributo que almacena el <code>String</code> para obtener el listado de registros del negocio.
	 */
	public static final String SELECT_HEADER_SQL =  
		"SELECT E16.id_formato AS C16001 , E16.nombre_formato AS C16002 , E16.tipo_formato AS C16003 , E16.tipo_separador AS C16004 , " +
		"       E16.caracter_separador AS C16005 , E16.lote_destino AS C16006 , E16.usuario_creacion AS C16007 , E16.fecha_creacion AS C16008 , " +
		"       E16.usuario_ult_act AS C16009 , E16.fecha_ult_act AS C16010" +
		"  FROM HTH_Formato E16";
	/**
	 * Atributo que almacena el <code>String</code> para obtener el n&uacute;mero de registros presentes en el negocio.
	 */
	public static final String COUNT_HEADER_SQL =   "SELECT COUNT(*) FROM HTH_Formato";    												
	/**
	 * Atributo que almacena el <code>String</code> para la inserci&oacute;n, sin id, de un nuevo registro en la tabla.
	 */
	public static final String CREATE_SQL = 
		"INSERT INTO HTH_Formato (" +
		"nombre_formato,tipo_formato,tipo_separador,caracter_separador,lote_destino" +
		",usuario_creacion,fecha_creacion,usuario_ult_act,fecha_ult_act" +
		"" +
		") VALUES (?,?,?,?,?,?,?,?,?)";
	/**
	 * Atributo que almacena el <code>String</code> para insertar, con id, un registro en la table.
	 */
	public static final String CREATE_SQL_CON_ID = 
		"INSERT INTO HTH_Formato (" +
		"id_formato,nombre_formato,tipo_formato,tipo_separador,caracter_separador" +
		",lote_destino,usuario_creacion,fecha_creacion,usuario_ult_act" +
		",fecha_ult_act" +
		") VALUES (?,?,?,?,?,?,?,?,?,?)";
	/**
	 * Atributo que almacena el <code>String</code> para la atualizaci&oacute; de un registro en la tabla.
	 */
	public static final String UPDATE_SQL = 
		"UPDATE HTH_Formato" +
		" SET nombre_formato= ?  , tipo_formato= ?  , tipo_separador= ?  , caracter_separador= ?  , " +
		"lote_destino= ?  , usuario_creacion= ?  , fecha_creacion= ?  , usuario_ult_act= ?  , " +
		"fecha_ult_act= ? " +
		" WHERE id_formato= ? ";
	/**
	 * Atributo que almacena el <code>String</code> para la eliminaci&oacute; de un registro en la tabla.
	 */
	public static final String DELETE_SQL =  
		"DELETE FROM HTH_Formato" +
		" WHERE id_formato= ? ";
	/**
	 * Atributo que almacena el <code>String</code> para la b&uacute;squeda por id de un registro de la tabla.
	 */
	public static final String FIND_BY_ID_SQL = 
		"SELECT E16.id_formato AS C16001 , E16.nombre_formato AS C16002 , E16.tipo_formato AS C16003 , E16.tipo_separador AS C16004 , " +
		"       E16.caracter_separador AS C16005 , E16.lote_destino AS C16006 , E16.usuario_creacion AS C16007 , E16.fecha_creacion AS C16008 , " +
		"       E16.usuario_ult_act AS C16009 , E16.fecha_ult_act AS C16010" +
		"  FROM HTH_Formato E16" +
		" WHERE E16.id_formato= ? ";
	/**
	 * Atributo que almacena el <code>String</code> para la creacion de la tabla en la base de datos.
	 */
	public static final String CREATE_TABLE_DDL = 
		"CREATE TABLE HTH_Formato (" +
		"    id_formato LONG NOT NULL," +
		"    nombre_formato VARCHAR(100)  NOT NULL," +
		"    tipo_formato VARCHAR(1)  NOT NULL," +
		"    tipo_separador VARCHAR(1)  NOT NULL," +
		"    caracter_separador VARCHAR(10) ," +
		"    lote_destino VARCHAR(3)  NOT NULL," +
		"    usuario_creacion VARCHAR(100)  NOT NULL," +
		"    fecha_creacion TIMESTAMP NOT NULL," +
		"    usuario_ult_act VARCHAR(100)  NOT NULL," +
		"    fecha_ult_act TIMESTAMP NOT NULL" +
		")";
	/**
	 * Atributo que almacena un objeto del tipo {@link SQLLenguaje}.
	 */
	private static SQLLenguaje lenguaje;
	
	/**
	 * M&eacute;todo que realiza la creaci&oacute;n de un DTO, del negocio, a partir del resultado de alguna consulta.
	 * @param <code>rst</code> objeto de tipo <code>Resultset</code>, que contiene los resultados de la consulta.
	 * @return <code>Object</code> - representa al DTO creado.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static Object crearDtoST(ResultSet rst)
			throws SQLException, ExcepcionEnDAO {
 	    HthFormato theDto = new HthFormato();
     	theDto.setIdFormato(SQLUtils.getLongFromResultSet(rst, "C16001"));
		theDto.setNombreFormato(rst.getString("C16002"));
		theDto.setTipoFormato(rst.getString("C16003"));
		theDto.setTipoSeparador(rst.getString("C16004"));
		theDto.setCaracterSeparador(rst.getString("C16005"));
		theDto.setLoteDestino(rst.getString("C16006"));
		theDto.setUsuarioCreacion(rst.getString("C16007"));
		theDto.setFechaCreacion(rst.getTimestamp("C16008"));
		theDto.setUsuarioUltAct(rst.getString("C16009"));
		theDto.setFechaUltAct(rst.getTimestamp("C16010"));
		return theDto;
	}
	
	







	/**
	 * M&eacute;todo que coloca los valores provenientes del objeto Dto en el objeto PreparedStatement,
	 * el cual proviene de la llamada a la implementacion de {@link SQLOperaciones}, este m&eacute;todo
	 * no toma en cuenta el valor del <code>id</code>, puesto que se asume que es un valor secuencial
	 * autonumerico o serial propio de la base de datos.
	 * 
	 * @param <code>theDtoUntyped</code> objeto sin tipo que almacena al DTO del negocio.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaCrearST(Object theDtoUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {			
	        HthFormato theDto = (HthFormato)theDtoUntyped;
				String nombreFormato = theDto.getNombreFormato();     
		String tipoFormato = theDto.getTipoFormato();     
		String tipoSeparador = theDto.getTipoSeparador();     
		String caracterSeparador = theDto.getCaracterSeparador();     
		String loteDestino = theDto.getLoteDestino();     
		String usuarioCreacion = theDto.getUsuarioCreacion();     
		java.util.Date fechaCreacion = theDto.getFechaCreacion();     
		String usuarioUltAct = theDto.getUsuarioUltAct();     
		java.util.Date fechaUltAct = theDto.getFechaUltAct();     
		     
		SQLUtils.setIntoStatement( 1, nombreFormato, smt);  
    		     
		SQLUtils.setIntoStatement( 2, tipoFormato, smt);  
    		     
		SQLUtils.setIntoStatement( 3, tipoSeparador, smt);  
    		     
		SQLUtils.setIntoStatement( 4, caracterSeparador, smt);  
    		     
		SQLUtils.setIntoStatement( 5, loteDestino, smt);  
    		     
		SQLUtils.setIntoStatement( 6, usuarioCreacion, smt);  
    		     
		SQLUtils.setIntoStatement( 7, fechaCreacion, smt);  
    		     
		SQLUtils.setIntoStatement( 8, usuarioUltAct, smt);  
    		     
		SQLUtils.setIntoStatement( 9, fechaUltAct, smt);  
    		
	}

	/**
	 * M&eacute;todo que coloca los valores provenientes del objeto Dto en el objeto PreparedStatement,
	 * el cual proviene de la llamada a la implementacion de {@link SQLOperaciones}, este m&eacute;todo
	 * toma en cuenta el <code>id</code>, lo que quiere decir que se usa al momento de  crear registros
	 * en donde el identificador no es un valor secuencial autonumerico o serial propio de la base de datos
	 *
	 * @param <code>theDtoUntyped</code> objeto sin tipo que almacena al DTO del negocio.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaCrearConIdST(Object theDtoUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {			
	        HthFormato theDto = (HthFormato)theDtoUntyped;
				Long idFormato = theDto.getIdFormato();     
		String nombreFormato = theDto.getNombreFormato();     
		String tipoFormato = theDto.getTipoFormato();     
		String tipoSeparador = theDto.getTipoSeparador();     
		String caracterSeparador = theDto.getCaracterSeparador();     
		String loteDestino = theDto.getLoteDestino();     
		String usuarioCreacion = theDto.getUsuarioCreacion();     
		java.util.Date fechaCreacion = theDto.getFechaCreacion();     
		String usuarioUltAct = theDto.getUsuarioUltAct();     
		java.util.Date fechaUltAct = theDto.getFechaUltAct();     
	
		SQLUtils.setIntoStatement( 1, idFormato, smt);
    	
		SQLUtils.setIntoStatement( 2, nombreFormato, smt);
    	
		SQLUtils.setIntoStatement( 3, tipoFormato, smt);
    	
		SQLUtils.setIntoStatement( 4, tipoSeparador, smt);
    	
		SQLUtils.setIntoStatement( 5, caracterSeparador, smt);
    	
		SQLUtils.setIntoStatement( 6, loteDestino, smt);
    	
		SQLUtils.setIntoStatement( 7, usuarioCreacion, smt);
    	
		SQLUtils.setIntoStatement( 8, fechaCreacion, smt);
    	
		SQLUtils.setIntoStatement( 9, usuarioUltAct, smt);
    	
		SQLUtils.setIntoStatement( 10, fechaUltAct, smt);
    		
	}

	/**
	 * M&eacute;todo que coloca el identificador recibido en el objeto PreparedStatement, para poder ser eliminado.
	 * @param <code>theIdUntyped</code> objeto sin tipo que identifica al <code>id</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaEliminarST(Object theIdUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {
			Long theId = (Long)theIdUntyped;
		SQLUtils.setIntoStatement( 1, theId, smt);
	}

	/**
	 * M&eacute;todo que coloca el identificador recibido, en el objeto PreparedStatement para poder hacer la consulta de busqueda.
	 * @param <code>theIdUntyped</code> objeto sin tipo que identifica al <code>id</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaBuscarPorIdST(Object theIdUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {
			Long theId = (Long)theIdUntyped;
			SQLUtils.setIntoStatement( 1, theId, smt);
		}

	/**
	 * M&eacute;todo que coloca los valores provenientes del objeto Dto en el objeto PreparedStatement,
	 * el cual proviene de la llamada a la implementacion de {@link SQLOperaciones} para poder actualizar
     * un registro en espec&iacute;fico.
	 * 
	 * @param <code>theDtoUntyped</code> objeto sin tipo que almacena al DTO del negocio.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaActualizarST(Object theDtoUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {
			HthFormato theDto = (HthFormato)theDtoUntyped;
				Long idFormato = theDto.getIdFormato();     
		String nombreFormato = theDto.getNombreFormato();     
		String tipoFormato = theDto.getTipoFormato();     
		String tipoSeparador = theDto.getTipoSeparador();     
		String caracterSeparador = theDto.getCaracterSeparador();     
		String loteDestino = theDto.getLoteDestino();     
		String usuarioCreacion = theDto.getUsuarioCreacion();     
		java.util.Date fechaCreacion = theDto.getFechaCreacion();     
		String usuarioUltAct = theDto.getUsuarioUltAct();     
		java.util.Date fechaUltAct = theDto.getFechaUltAct();     
		     
		SQLUtils.setIntoStatement( 1, nombreFormato, smt);  
        	     
		SQLUtils.setIntoStatement( 2, tipoFormato, smt);  
        	     
		SQLUtils.setIntoStatement( 3, tipoSeparador, smt);  
        	     
		SQLUtils.setIntoStatement( 4, caracterSeparador, smt);  
        	     
		SQLUtils.setIntoStatement( 5, loteDestino, smt);  
        	     
		SQLUtils.setIntoStatement( 6, usuarioCreacion, smt);  
        	     
		SQLUtils.setIntoStatement( 7, fechaCreacion, smt);  
        	     
		SQLUtils.setIntoStatement( 8, usuarioUltAct, smt);  
        	     
		SQLUtils.setIntoStatement( 9, fechaUltAct, smt);  
        		     
		SQLUtils.setIntoStatement( 10, idFormato, smt);  
    			
	}


	/**
	 * M&eacute;todo que ejecuta una petici&oacute;n de datos a trav&eacute;s de un objeto {@link PreparedStatement},
	 * y un objeto del tipo {@link PeticionDeDatos}, el cual en su implementaci&oacute;n define una serie de restricciones
	 * usadas para hacer la petici&oacute;n.
	 *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaPeticionDeDatosST(PeticionDeDatos peticion,
			PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
		SQLImplementacionOperacionesComunes.operacionesParaPeticionDeDatosST( peticion, smt, SQL_OPERACIONES, null);	
	}	
	
	/**
	 * M&eacute;todo que ejecuta una petici&oacute;n de datos a trav&eacute;s de un objeto {@link PreparedStatement},
     * y un objeto del tipo {@link PeticionDeDatos}, el cual en su implementaci&oacute;n define una serie de restricciones
     * usadas para hacer la petici&oacute;n.
	 *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaPeticionDeDatosFST(PeticionDeDatos peticion,
			PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
		SQLImplementacionOperacionesComunes.operacionesParaPeticionDeDatosST( peticion, smt, SQL_OPERACIONES, null);	
	}	

	/**
	 * M&eacute;todo que ejecuta una petici&oacute;n de conteo a trav&eacute;s de un objeto {@link PreparedStatement},
     * y un objeto del tipo {@link PeticionDeDatos}, el cual en su implementaci&oacute;n define una serie de restricciones
     * usadas para hacer la petici&oacute;n.
	 *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaConteoST(PeticionDeDatos peticion,
			PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
		SQLImplementacionOperacionesComunes.operacionesParaPeticionDeDatosST( peticion, smt, SQL_OPERACIONES, null);
	}

	/**
	 * M&eacute;todo que agrega al atributo {@link #SELECT_HEADER_SQL} las condiciones de b&uacute;squeda
     * que vienen en el objeto del tipo {@link PeticionDeDatos}
     *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>, que contiene las condiciones de la consulta.
	 * @return <code>String</code> - la consulta condicionada como cadena de caracteres.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static String getSqlParaPeticionDeDatosST(PeticionDeDatos peticion)
			throws SQLException, ExcepcionEnDAO {
		return SELECT_HEADER_SQL + SQLImplementacionOperacionesComunes.crearCondicionesSQLST( peticion, SQL_OPERACIONES, SQLImplementacionOperacionesComunes.COND_ALIAS_ORDEN_WHERE);
	}
	
	
	
	

	/**
	 * M&eacute;todo que agrega al atributo {@link #COUNT_HEADER_SQL} las condiciones de conteo
	 * que vienen en el objeto de tipo {@link PeticionDeDatos}.
	 *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>, que contiene las condiciones de la consulta.
	 * @return <code>String</code> - la consulta condicionada como cadena de caracteres.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static String getSqlDeConteoST(PeticionDeDatos peticion)
			throws SQLException, ExcepcionEnDAO {
		return COUNT_HEADER_SQL + SQLImplementacionOperacionesComunes.crearCondicionesSQLST( peticion, SQL_OPERACIONES, SQLImplementacionOperacionesComunes.COND_SQL_WHERE);
	}
	
	/**
	 * M&eacute;todo que se utiliza para actualizar un registro a nivel de tabla, recibe como par&aacute;metro un DTO del negocio
	 * que contiene la informaci&oacute;n a actualizar, e invoca la funci&oacute;n <code>actualizar</code> de la clase de la que hereda.
	 *
	 * @param <code>hthFormato</code> objeto del tipo <code>HthFormato</code>.
	 * @throws ExcepcionEnDAO
	 * @see {@link SQLImplementacionBasica}
	 */
	public void actualizar(HthFormato hthFormato) throws ExcepcionEnDAO {
	    actualizar(hthFormato, AUTO_COMMIT);
	
	}
	
	/**
	 * M&eacute;todo utilizado para realizar una b&uacute;squeda de un registro espec&iacute;fico, utilizando un identificador.
	 *
	 * @param <code>idFormatoId</code> objeto del tipo <code>Long</code>.
	 * @return <code>HthFormato</code> - un objeto DTO.
	 * @throws ExcepcionEnDAO
	 * @see {@link SQLImplementacionBasica}
	 */
	public HthFormato buscarPorID(Long idFormatoId)
			throws ExcepcionEnDAO {
		return (HthFormato)buscarPorID(idFormatoId, AUTO_COMMIT);				 
	}

	/**
	 * M&eacute;todo utilizado para la creaci&oacute;n de un registro, a nivel de tabla, a partir de la informaci&oacute;n
	 * recibida en un DTO del objeto de Negocio, e invoca la funci&oacute;n <code>crear</code> de la clase de la que hereda.
	 * 
	 * @param <code>hthFormato</code> objeto del tipo <code>HthFormato</code>.
	 * @throws ExcepcionEnDAO
	 * @see {@link SQLImplementacionBasica}
	 */
	public void crear(HthFormato hthFormato) throws ExcepcionEnDAO {
	    crear(hthFormato, AUTO_COMMIT);		
	}

	/**
	 * M&eacute;todo utilizado para la eliminaci&oacute;n de un registro, a nivel de tabla,
	 * que se busca a trav&eacute;s del identificador recibido. Se invoca la funci&oacute;n <code>eliminar</code> de la clase de la que hereda.
	 *
	 * @param <code>idFormatoId</code> objeto del tipo <code>Long</code>.
	 * @throws ExcepcionEnDAO
	 * @see {@link SQLImplementacionBasica}
	 */
	public void eliminar(Long idFormatoId) throws ExcepcionEnDAO {
	    eliminar(idFormatoId, AUTO_COMMIT);		
	}

	/**
	 * M&eacute;todo utilizado para devolver un &uacute;nico resultado de la petici&oacute;n recibida,
	 * la cual se realiza a trav&eacuate;s del objeto de tipo {@link PeticionDeDatos}.
	 * 
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>, que contiene las condiciones de la consulta.
	 * @return <code>HthFormato</code> - un objeto DTO.
	 * @throws ExcepcionEnDAO
	 * @throws ExcepcionPorDatoNoEncontrado
	 * @see {@link SQLImplementacionBasica}
	 */
	public HthFormato resultadoUnico(PeticionDeDatos peticion)
			throws ExcepcionEnDAO, ExcepcionPorDatoNoEncontrado {
		return (HthFormato)resultadoUnico(peticion, AUTO_COMMIT);
	}
	
	/**
	 * Atributo del tipo {@link SQLOperaciones}, almacena la operaciones disponibles para esta implementaci&oacute;n.<br/>
	 * Algunas de las llamadas a los m&eacute;todos se realiza a trav&eacute;s de la invocaci&oacute;n de estas implementaciones.
	 */
    public static final SQLOperaciones	SQL_OPERACIONES =  new SQLOperaciones() {
			
			public int getManejoForaneosPorDefecto() {
				return Dao.SIN_FORANEOS;
			}
			
			
			public void operacionesParaPeticionDeDatosF(PeticionDeDatos request,
					PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
				HthFormatoDaoJdbcSql.operacionesParaPeticionDeDatosFST(request, smt);
			}
			
			public void operacionesParaPeticionDeDatos(PeticionDeDatos request,
					PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
				HthFormatoDaoJdbcSql.operacionesParaPeticionDeDatosST(request, smt);
			}
			
			public void operacionesParaEliminar(Object theId, PreparedStatement smt)
					throws SQLException, ExcepcionEnDAO {
				HthFormatoDaoJdbcSql.operacionesParaEliminarST(theId, smt);
			}
			
			public void operacionesParaCrear(Object theDto, PreparedStatement smt)
					throws SQLException, ExcepcionEnDAO {
				HthFormatoDaoJdbcSql.operacionesParaCrearST(theDto, smt);
			}
			
			public void operacionesParaCrearConId(Object theDto, PreparedStatement smt) 
			        throws SQLException, ExcepcionEnDAO {
			    HthFormatoDaoJdbcSql.operacionesParaCrearConIdST(theDto, smt);
            }
			
			public void operacionesParaConteo(PeticionDeDatos request,
					PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
				HthFormatoDaoJdbcSql.operacionesParaConteoST(request, smt);				
			}
			
			public void operacionesParaBuscarPorId(Object theId, PreparedStatement smt)
					throws SQLException, ExcepcionEnDAO {
				HthFormatoDaoJdbcSql.operacionesParaBuscarPorIdST(theId, smt);
			}
			
			public void operacionesParaActualizar(Object theDto, PreparedStatement smt)
					throws SQLException, ExcepcionEnDAO {
				HthFormatoDaoJdbcSql.operacionesParaActualizarST(theDto, smt);
				
			}
			
			public String getSqlParaPeticionDeDatosF(PeticionDeDatos request)
					throws SQLException, ExcepcionEnDAO {	
				return null;
			}
			
			public String getSqlParaPeticionDeDatos(PeticionDeDatos request)
					throws SQLException, ExcepcionEnDAO {
				return HthFormatoDaoJdbcSql.getSqlParaPeticionDeDatosST(request);
			}
			
			public String getSqlDeConteo(PeticionDeDatos request) throws SQLException,
					ExcepcionEnDAO {
				return HthFormatoDaoJdbcSql.getSqlDeConteoST(request);
			}
			
			public SQLEntidadMeta getSQLEntidadMeta() throws ExcepcionEnDAO {
				return HthFormatoDaoJdbcSql.SQL_ENTIDAD_META;
			}
			
			public String getEliminarSql() {
				return  HthFormatoDaoJdbcSql.DELETE_SQL;
			}
			
			public String getCrearSql() {
				return  HthFormatoDaoJdbcSql.CREATE_SQL;
			}
			
			public String getCrearSqlConId(){
			    return  HthFormatoDaoJdbcSql.CREATE_SQL_CON_ID;
			}			
			
			public String getBuscarPorIdSqlF() {
				return null;
			}
			
			public String getBuscarPorIdSql() {				
				return HthFormatoDaoJdbcSql.FIND_BY_ID_SQL;
			}
			
			public String getActualizarSql() {				
				return HthFormatoDaoJdbcSql.UPDATE_SQL;
			}
			
			public Object crearDto(ResultSet rst) throws SQLException, ExcepcionEnDAO {
				return HthFormatoDaoJdbcSql.crearDtoST(rst);
			}
	        
	        public Object crearDataConValoresForaneos(ResultSet rst)
					throws SQLException, ExcepcionEnDAO {
				return null;
			}
			
	        public Object crearDataConValoresForaneosNoRel(ResultSet rst, Connection conn)
					throws SQLException, ExcepcionEnDAO {
				return null;
			}
			
			
            public boolean isForaneosDisponible(){
				return false;
            
            }
            
            public SQLLenguaje getSQLLenguaje() {
                return HthFormatoDaoJdbcSql.getLenguaje();
            }
            
            public void setIdEnDto(Object theDto, Object dtoId) {
                setIdEnDtoST(theDto, dtoId);
            }
	};
	
	
	
	
	
	/**
	 * Devuelve el atributo {@link #SQL_OPERACIONES}.
	 * @return <code>SQLOperaciones</code> - objeto del tipo {@link SQLOperaciones}
	 */
    public SQLOperaciones getSqlOperaciones() {
       return SQL_OPERACIONES;
	}
	
	/**
	 * Devuelve el atributo {@link #lenguaje}
	 * @return <code>SQLLenguaje</code> - objeto del tipo {@link SQLLenguaje}
	 */	
	public static SQLLenguaje getLenguaje() {
		return lenguaje;
	}
	
	/**
	 * M&eacute;todo que permite obtener a trav&eacute;s de un Servicio la implementaci&oacute;n del objeto de Lenguaje,
	 * y coloca las reglas de negocio almacenadas en el atributo {@link #SQL_ENTIDAD_META}.
	 * @param <code>localizadorLenguaje</code> objeto del tipo {@link LocalizadorDeServicio} que posee informaci&oacute;n para trabajar con la base de datos.
	 */
	public static void setLocalizadorLenguaje(LocalizadorDeServicio localizadorLenguaje) {
		lenguaje = (SQLLenguaje) localizadorLenguaje.getServicio();
		lenguaje.setSqlEntidadMeta(SQL_ENTIDAD_META);
	}

	/**
	 * M&eacute;todo que agrega un identificador a un DTO del tipo <code>HthFormato</code>.
	 * @param <code>theDtoUntyped</code> objeto gen&eacute;rico, que representa a un <code>DTO</code>.
	 * @param <code>dtoIdUntyped</code> objeto gen&eacute;rico, que representa al identificador.
	 */
	public static void setIdEnDtoST(Object theDtoUntyped, Object dtoIdUntyped){
	    HthFormato dto = (HthFormato)theDtoUntyped;
        Long dtoId = (Long) dtoIdUntyped;;
        dto.setIdFormato(dtoId);

	}	
	
	
}
