package com.ofbox.davivienda.h2h.abstraccion.dao;

import com.ofbox.davivienda.h2h.abstraccion.dto.BsConvenio;
import com.ofbox.davivienda.h2h.abstraccion.dto.BsConvenioID;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.ExcepcionEnDAO;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.ExcepcionPorDatoNoEncontrado;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Paginador;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.PeticionDeDatos;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Dao;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Transaccion;

/**
 * Interfaz que define el comportamiento para un objeto de negocio espec&iacute;fico, para que pueda interactuar con la capa DAO,
 * extiende de la interfaz {@link com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Dao Dao}.
 * @author System Out of the Box
 * @version f3.1
 *  
 */
public interface BsConvenioDao extends Dao{

    /**
     * Atributo que almacena el identificador de la columna TipoConvenio mapeado
	 * al atributo Tipoconvenio.
     */
    int TIPOCONVENIO = 1001;
    /**
     * Atributo que almacena el identificador de la columna Convenio mapeado
	 * al atributo Convenio.
     */
    int CONVENIO = 1002;
    /**
     * Atributo que almacena el identificador de la columna Cliente mapeado
	 * al atributo Cliente.
     */
    int CLIENTE = 1003;
    /**
     * Atributo que almacena el identificador de la columna Nombre mapeado
	 * al atributo Nombre.
     */
    int NOMBRE = 1004;
    /**
     * Atributo que almacena el identificador de la columna Cuenta mapeado
	 * al atributo Cuenta.
     */
    int CUENTA = 1005;
    /**
     * Atributo que almacena el identificador de la columna NombreCta mapeado
	 * al atributo Nombrecta.
     */
    int NOMBRECTA = 1006;
    /**
     * Atributo que almacena el identificador de la columna TipoMoneda mapeado
	 * al atributo Tipomoneda.
     */
    int TIPOMONEDA = 1007;
    /**
     * Atributo que almacena el identificador de la columna Categoria mapeado
	 * al atributo Categoria.
     */
    int CATEGORIA = 1008;
    /**
     * Atributo que almacena el identificador de la columna Descripcion mapeado
	 * al atributo Descripcion.
     */
    int DESCRIPCION = 1009;
    /**
     * Atributo que almacena el identificador de la columna DiaAplicacion mapeado
	 * al atributo Diaaplicacion.
     */
    int DIAAPLICACION = 1010;
    /**
     * Atributo que almacena el identificador de la columna NumReintentos mapeado
	 * al atributo Numreintentos.
     */
    int NUMREINTENTOS = 1011;
    /**
     * Atributo que almacena el identificador de la columna PolParticipante mapeado
	 * al atributo Polparticipante.
     */
    int POLPARTICIPANTE = 1012;
    /**
     * Atributo que almacena el identificador de la columna PolDiaNoExistente mapeado
	 * al atributo Poldianoexistente.
     */
    int POLDIANOEXISTENTE = 1013;
    /**
     * Atributo que almacena el identificador de la columna Estatus mapeado
	 * al atributo Estatus.
     */
    int ESTATUS = 1014;
    /**
     * Atributo que almacena el identificador de la columna FechaEstatus mapeado
	 * al atributo Fechaestatus.
     */
    int FECHAESTATUS = 1015;
    /**
     * Atributo que almacena el identificador de la columna IntervaloReintento mapeado
	 * al atributo Intervaloreintento.
     */
    int INTERVALOREINTENTO = 1016;
    /**
     * Atributo que almacena el identificador de la columna PolMontoFijo mapeado
	 * al atributo Polmontofijo.
     */
    int POLMONTOFIJO = 1017;
    /**
     * Atributo que almacena el identificador de la columna MontoFijo mapeado
	 * al atributo Montofijo.
     */
    int MONTOFIJO = 1018;
    /**
     * Atributo que almacena el identificador de la columna IntervaloAplica mapeado
	 * al atributo Intervaloaplica.
     */
    int INTERVALOAPLICA = 1019;
    /**
     * Atributo que almacena el identificador de la columna CreditosParciales mapeado
	 * al atributo Creditosparciales.
     */
    int CREDITOSPARCIALES = 1020;
    /**
     * Atributo que almacena el identificador de la columna AutorizacionWeb mapeado
	 * al atributo Autorizacionweb.
     */
    int AUTORIZACIONWEB = 1021;
    /**
     * Atributo que almacena el identificador de la columna Clasificacion mapeado
	 * al atributo Clasificacion.
     */
    int CLASIFICACION = 1022;
    /**
     * Atributo que almacena el identificador de la columna ComisionXOperacion mapeado
	 * al atributo Comisionxoperacion.
     */
    int COMISIONXOPERACION = 1023;
    /**
     * Atributo que almacena el identificador de la columna ComisionXOperacionPP mapeado
	 * al atributo Comisionxoperacionpp.
     */
    int COMISIONXOPERACIONPP = 1024;
    /**
     * Atributo que almacena el identificador de la columna MultiplesCuentas mapeado
	 * al atributo Multiplescuentas.
     */
    int MULTIPLESCUENTAS = 1025;

    /**
     * M&eacute;todo  que devuelve un DTO, formado con un objeto devuelto por la capa DAO,
	 * obtenido a trav&eacute;s de la b&uacute;squeda por id.<br/>
     * El DTO devuelto representa un registro a nivel de base de datos.
     * @param <code>id</code> Objeto que identifica la llave del DTO.
     * @return <code>Objeto</code>
     * @throws ExcepcionEnDA
     */
	BsConvenio buscarPorID(BsConvenioID bsConvenioID) throws ExcepcionEnDAO;
    /**
     * M&eacute;todo que debe eliminar un objeto a trav&eacute;s de un id.<br/>
     * Elimina un registro a nivel de la base de datos.
     * @param <code>id</code> Objeto que identifica la llave del DTO. 
     * @throws ExcepcionEnDAO
     */
	void eliminar(BsConvenioID bsConvenioID) throws ExcepcionEnDAO;	
    /**
     * M&eacute;todo que debe actualizar un objeto.
     * Actualiza un registro a nivel de la base de datos.
     * @param <code>Objeto</code> DTO
     * @throws ExcepcionEnDAO
     */	
	void actualizar(BsConvenio bsConvenio) throws ExcepcionEnDAO;
	/**
	 * M&eacute;todo que debe crear un objeto para el negocio. 
     * Crea un nuevo registro a nivel de base de datos.
	 * @param <code>Objeto</code> DTO.
	 * @throws ExcepcionEnDAO
	 */	
	void crear(BsConvenio bsConvenio) throws ExcepcionEnDAO;
    /**
     * M&eacute;todo que devuelve un DTO, solicitado a trav&eacute;s de un objeto de tipo {@link PeticionDeDatos}.
     * @param peticion objeto de tipo <code>PeticionDeDatos</code>
     * @return <code>Objeto</code> DTO.
     * @throws ExcepcionEnDAO
     * @throws ExcepcionPorDatoNoEncontrado
     */
	BsConvenio resultadoUnico(PeticionDeDatos peticion) throws ExcepcionEnDAO, ExcepcionPorDatoNoEncontrado;
	
    	 
}