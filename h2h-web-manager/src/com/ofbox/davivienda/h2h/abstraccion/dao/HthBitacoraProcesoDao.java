package com.ofbox.davivienda.h2h.abstraccion.dao;

import com.ofbox.davivienda.h2h.abstraccion.dto.HthBitacoraProceso;
import java.lang.Long;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.ExcepcionEnDAO;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.ExcepcionPorDatoNoEncontrado;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Paginador;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.PeticionDeDatos;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Dao;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Transaccion;

/**
 * Interfaz que define el comportamiento para un objeto de negocio espec&iacute;fico, para que pueda interactuar con la capa DAO,
 * extiende de la interfaz {@link com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Dao Dao}.
 * @author System Out of the Box
 * @version f3.1
 *  
 */
public interface HthBitacoraProcesoDao extends Dao{

    /**
     * Atributo que almacena el identificador de la columna id_bitacora_proceso mapeado
	 * al atributo IdBitacoraProceso.
     */
    int IDBITACORAPROCESO = 14001;
    /**
     * Atributo que almacena el identificador de la columna fecha_proceso mapeado
	 * al atributo FechaProceso.
     */
    int FECHAPROCESO = 14002;
    /**
     * Atributo que almacena el identificador de la columna resultado_proceso mapeado
	 * al atributo ResultadoProceso.
     */
    int RESULTADOPROCESO = 14003;
    /**
     * Atributo que almacena el identificador de la columna descripcion_resultado mapeado
	 * al atributo DescripcionResultado.
     */
    int DESCRIPCIONRESULTADO = 14004;
    /**
     * Atributo que almacena el identificador de la columna stack_trace mapeado
	 * al atributo StackTrace.
     */
    int STACKTRACE = 14005;
    /**
     * Atributo que almacena el identificador de la columna cliente mapeado
	 * al atributo Cliente.
     */
    int CLIENTE = 14006;
    /**
     * Atributo que almacena el identificador de la columna convenio mapeado
	 * al atributo Convenio.
     */
    int CONVENIO = 14007;
    /**
     * Atributo que almacena el identificador de la columna archivo mapeado
	 * al atributo Archivo.
     */
    int ARCHIVO = 14008;
    /**
     * Atributo que almacena el identificador de la columna lineas_bitacora mapeado
	 * al atributo LineasBitacora.
     */
    int LINEASBITACORA = 14009;

    /**
     * M&eacute;todo  que devuelve un DTO, formado con un objeto devuelto por la capa DAO,
	 * obtenido a trav&eacute;s de la b&uacute;squeda por id.<br/>
     * El DTO devuelto representa un registro a nivel de base de datos.
     * @param <code>id</code> Objeto que identifica la llave del DTO.
     * @return <code>Objeto</code>
     * @throws ExcepcionEnDA
     */
	HthBitacoraProceso buscarPorID(Long idBitacoraProcesoId) throws ExcepcionEnDAO;
    /**
     * M&eacute;todo que debe eliminar un objeto a trav&eacute;s de un id.<br/>
     * Elimina un registro a nivel de la base de datos.
     * @param <code>id</code> Objeto que identifica la llave del DTO. 
     * @throws ExcepcionEnDAO
     */
	void eliminar(Long idBitacoraProcesoId) throws ExcepcionEnDAO;	
    /**
     * M&eacute;todo que debe actualizar un objeto.
     * Actualiza un registro a nivel de la base de datos.
     * @param <code>Objeto</code> DTO
     * @throws ExcepcionEnDAO
     */	
	void actualizar(HthBitacoraProceso hthBitacoraProceso) throws ExcepcionEnDAO;
	/**
	 * M&eacute;todo que debe crear un objeto para el negocio. 
     * Crea un nuevo registro a nivel de base de datos.
	 * @param <code>Objeto</code> DTO.
	 * @throws ExcepcionEnDAO
	 */	
	void crear(HthBitacoraProceso hthBitacoraProceso) throws ExcepcionEnDAO;
    /**
     * M&eacute;todo que devuelve un DTO, solicitado a trav&eacute;s de un objeto de tipo {@link PeticionDeDatos}.
     * @param peticion objeto de tipo <code>PeticionDeDatos</code>
     * @return <code>Objeto</code> DTO.
     * @throws ExcepcionEnDAO
     * @throws ExcepcionPorDatoNoEncontrado
     */
	HthBitacoraProceso resultadoUnico(PeticionDeDatos peticion) throws ExcepcionEnDAO, ExcepcionPorDatoNoEncontrado;
	
    	 
}