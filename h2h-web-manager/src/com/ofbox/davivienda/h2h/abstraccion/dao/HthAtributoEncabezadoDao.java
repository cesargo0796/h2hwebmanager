package com.ofbox.davivienda.h2h.abstraccion.dao;

import com.ofbox.davivienda.h2h.abstraccion.dto.HthAtributoEncabezado;
import java.lang.Long;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.ExcepcionEnDAO;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.ExcepcionPorDatoNoEncontrado;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Paginador;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.PeticionDeDatos;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Dao;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Transaccion;

/**
 * Interfaz que define el comportamiento para un objeto de negocio espec&iacute;fico, para que pueda interactuar con la capa DAO,
 * extiende de la interfaz {@link com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Dao Dao}.
 * @author System Out of the Box
 * @version f3.1
 *  
 */
public interface HthAtributoEncabezadoDao extends Dao{

    /**
     * Atributo que almacena el identificador de la columna id_atributo_encabezado mapeado
	 * al atributo IdAtributoEncabezado.
     */
    int IDATRIBUTOENCABEZADO = 12001;
    /**
     * Atributo que almacena el identificador de la columna nombre_atributo mapeado
	 * al atributo NombreAtributo.
     */
    int NOMBREATRIBUTO = 12002;
    /**
     * Atributo que almacena el identificador de la columna descripcion mapeado
	 * al atributo Descripcion.
     */
    int DESCRIPCION = 12003;
    /**
     * Atributo que almacena el identificador de la columna tabla mapeado
	 * al atributo Tabla.
     */
    int TABLA = 12004;
    /**
     * Atributo que almacena el identificador de la columna columna mapeado
	 * al atributo ColumnaDestino.
     */
    int COLUMNADESTINO = 12005;
    /**
     * Atributo que almacena el identificador de la columna usuario_creacion mapeado
	 * al atributo UsuarioCreacion.
     */
    int USUARIOCREACION = 12006;
    /**
     * Atributo que almacena el identificador de la columna fecha_creacion mapeado
	 * al atributo FechaCreacion.
     */
    int FECHACREACION = 12007;
    /**
     * Atributo que almacena el identificador de la columna usuario_ult_act mapeado
	 * al atributo UsuarioUltAct.
     */
    int USUARIOULTACT = 12008;
    /**
     * Atributo que almacena el identificador de la columna fecha_ult_act mapeado
	 * al atributo FechaUltAct.
     */
    int FECHAULTACT = 12009;

    /**
     * M&eacute;todo  que devuelve un DTO, formado con un objeto devuelto por la capa DAO,
	 * obtenido a trav&eacute;s de la b&uacute;squeda por id.<br/>
     * El DTO devuelto representa un registro a nivel de base de datos.
     * @param <code>id</code> Objeto que identifica la llave del DTO.
     * @return <code>Objeto</code>
     * @throws ExcepcionEnDA
     */
	HthAtributoEncabezado buscarPorID(Long idAtributoEncabezadoId) throws ExcepcionEnDAO;
    /**
     * M&eacute;todo que debe eliminar un objeto a trav&eacute;s de un id.<br/>
     * Elimina un registro a nivel de la base de datos.
     * @param <code>id</code> Objeto que identifica la llave del DTO. 
     * @throws ExcepcionEnDAO
     */
	void eliminar(Long idAtributoEncabezadoId) throws ExcepcionEnDAO;	
    /**
     * M&eacute;todo que debe actualizar un objeto.
     * Actualiza un registro a nivel de la base de datos.
     * @param <code>Objeto</code> DTO
     * @throws ExcepcionEnDAO
     */	
	void actualizar(HthAtributoEncabezado hthAtributoEncabezado) throws ExcepcionEnDAO;
	/**
	 * M&eacute;todo que debe crear un objeto para el negocio. 
     * Crea un nuevo registro a nivel de base de datos.
	 * @param <code>Objeto</code> DTO.
	 * @throws ExcepcionEnDAO
	 */	
	void crear(HthAtributoEncabezado hthAtributoEncabezado) throws ExcepcionEnDAO;
    /**
     * M&eacute;todo que devuelve un DTO, solicitado a trav&eacute;s de un objeto de tipo {@link PeticionDeDatos}.
     * @param peticion objeto de tipo <code>PeticionDeDatos</code>
     * @return <code>Objeto</code> DTO.
     * @throws ExcepcionEnDAO
     * @throws ExcepcionPorDatoNoEncontrado
     */
	HthAtributoEncabezado resultadoUnico(PeticionDeDatos peticion) throws ExcepcionEnDAO, ExcepcionPorDatoNoEncontrado;
	
    	 
}