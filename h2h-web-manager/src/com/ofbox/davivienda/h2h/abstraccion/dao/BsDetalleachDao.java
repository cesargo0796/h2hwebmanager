package com.ofbox.davivienda.h2h.abstraccion.dao;

import com.ofbox.davivienda.h2h.abstraccion.dto.BsDetalleach;
import com.ofbox.davivienda.h2h.abstraccion.dto.BsDetalleachID;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.ExcepcionEnDAO;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.ExcepcionPorDatoNoEncontrado;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Paginador;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.PeticionDeDatos;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Dao;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Transaccion;

/**
 * Interfaz que define el comportamiento para un objeto de negocio espec&iacute;fico, para que pueda interactuar con la capa DAO,
 * extiende de la interfaz {@link com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Dao Dao}.
 * @author System Out of the Box
 * @version f3.1
 *  
 */
public interface BsDetalleachDao extends Dao{

    /**
     * Atributo que almacena el identificador de la columna idDetalle mapeado
	 * al atributo Iddetalle.
     */
    int IDDETALLE = 5001;
    /**
     * Atributo que almacena el identificador de la columna Instalacion mapeado
	 * al atributo Instalacion.
     */
    int INSTALACION = 5002;
    /**
     * Atributo que almacena el identificador de la columna Lote mapeado
	 * al atributo Lote.
     */
    int LOTE = 5003;
    /**
     * Atributo que almacena el identificador de la columna Operacion mapeado
	 * al atributo Operacion.
     */
    int OPERACION = 5004;
    /**
     * Atributo que almacena el identificador de la columna Cuenta mapeado
	 * al atributo Cuenta.
     */
    int CUENTA = 5005;
    /**
     * Atributo que almacena el identificador de la columna TipoOperacion mapeado
	 * al atributo Tipooperacion.
     */
    int TIPOOPERACION = 5006;
    /**
     * Atributo que almacena el identificador de la columna Monto mapeado
	 * al atributo Monto.
     */
    int MONTO = 5007;
    /**
     * Atributo que almacena el identificador de la columna Adenda mapeado
	 * al atributo Adenda.
     */
    int ADENDA = 5008;
    /**
     * Atributo que almacena el identificador de la columna FechaEstatus mapeado
	 * al atributo Fechaestatus.
     */
    int FECHAESTATUS = 5009;
    /**
     * Atributo que almacena el identificador de la columna Estatus mapeado
	 * al atributo Estatus.
     */
    int ESTATUS = 5010;
    /**
     * Atributo que almacena el identificador de la columna IDPago mapeado
	 * al atributo Idpago.
     */
    int IDPAGO = 5011;
    /**
     * Atributo que almacena el identificador de la columna AutorizacionHost mapeado
	 * al atributo Autorizacionhost.
     */
    int AUTORIZACIONHOST = 5012;
    /**
     * Atributo que almacena el identificador de la columna Email mapeado
	 * al atributo Email.
     */
    int EMAIL = 5013;
    /**
     * Atributo que almacena el identificador de la columna ComisionAplicada mapeado
	 * al atributo Comisionaplicada.
     */
    int COMISIONAPLICADA = 5014;
    /**
     * Atributo que almacena el identificador de la columna NombreCuenta mapeado
	 * al atributo Nombrecuenta.
     */
    int NOMBRECUENTA = 5015;
    /**
     * Atributo que almacena el identificador de la columna Autorizador mapeado
	 * al atributo Autorizador.
     */
    int AUTORIZADOR = 5016;
    /**
     * Atributo que almacena el identificador de la columna EstatusPayBank mapeado
	 * al atributo Estatuspaybank.
     */
    int ESTATUSPAYBANK = 5017;
    /**
     * Atributo que almacena el identificador de la columna FechaHoraHOST mapeado
	 * al atributo Fechahorahost.
     */
    int FECHAHORAHOST = 5018;
    /**
     * Atributo que almacena el identificador de la columna CuentaDebito mapeado
	 * al atributo Cuentadebito.
     */
    int CUENTADEBITO = 5019;
    /**
     * Atributo que almacena el identificador de la columna NombreDeCuenta mapeado
	 * al atributo Nombredecuenta.
     */
    int NOMBREDECUENTA = 5020;
    /**
     * Atributo que almacena el identificador de la columna CodBanco mapeado
	 * al atributo Codbanco.
     */
    int CODBANCO = 5021;
    /**
     * Atributo que almacena el identificador de la columna TipoCuenta mapeado
	 * al atributo Tipocuenta.
     */
    int TIPOCUENTA = 5022;
    /**
     * Atributo que almacena el identificador de la columna DescEstatusPayBank mapeado
	 * al atributo Descestatuspaybank.
     */
    int DESCESTATUSPAYBANK = 5023;
    /**
     * Atributo que almacena el identificador de la columna EnviarAHost mapeado
	 * al atributo Enviarahost.
     */
    int ENVIARAHOST = 5024;

    /**
     * M&eacute;todo  que devuelve un DTO, formado con un objeto devuelto por la capa DAO,
	 * obtenido a trav&eacute;s de la b&uacute;squeda por id.<br/>
     * El DTO devuelto representa un registro a nivel de base de datos.
     * @param <code>id</code> Objeto que identifica la llave del DTO.
     * @return <code>Objeto</code>
     * @throws ExcepcionEnDA
     */
	BsDetalleach buscarPorID(BsDetalleachID bsDetalleachID) throws ExcepcionEnDAO;
    /**
     * M&eacute;todo que debe eliminar un objeto a trav&eacute;s de un id.<br/>
     * Elimina un registro a nivel de la base de datos.
     * @param <code>id</code> Objeto que identifica la llave del DTO. 
     * @throws ExcepcionEnDAO
     */
	void eliminar(BsDetalleachID bsDetalleachID) throws ExcepcionEnDAO;	
    /**
     * M&eacute;todo que debe actualizar un objeto.
     * Actualiza un registro a nivel de la base de datos.
     * @param <code>Objeto</code> DTO
     * @throws ExcepcionEnDAO
     */	
	void actualizar(BsDetalleach bsDetalleach) throws ExcepcionEnDAO;
	/**
	 * M&eacute;todo que debe crear un objeto para el negocio. 
     * Crea un nuevo registro a nivel de base de datos.
	 * @param <code>Objeto</code> DTO.
	 * @throws ExcepcionEnDAO
	 */	
	void crear(BsDetalleach bsDetalleach) throws ExcepcionEnDAO;
    /**
     * M&eacute;todo que devuelve un DTO, solicitado a trav&eacute;s de un objeto de tipo {@link PeticionDeDatos}.
     * @param peticion objeto de tipo <code>PeticionDeDatos</code>
     * @return <code>Objeto</code> DTO.
     * @throws ExcepcionEnDAO
     * @throws ExcepcionPorDatoNoEncontrado
     */
	BsDetalleach resultadoUnico(PeticionDeDatos peticion) throws ExcepcionEnDAO, ExcepcionPorDatoNoEncontrado;
	
    	 
}