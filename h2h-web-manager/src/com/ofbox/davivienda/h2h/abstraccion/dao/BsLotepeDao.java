package com.ofbox.davivienda.h2h.abstraccion.dao;

import com.ofbox.davivienda.h2h.abstraccion.dto.BsLotepe;
import com.ofbox.davivienda.h2h.abstraccion.dto.BsLotepeID;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.ExcepcionEnDAO;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.ExcepcionPorDatoNoEncontrado;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Paginador;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.PeticionDeDatos;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Dao;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Transaccion;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.foraneos.DataConValoresForaneos;    
import com.ofbox.davivienda.h2h.abstraccion.dto.BsConvenio;

/**
 * Interfaz que define el comportamiento para un objeto de negocio espec&iacute;fico, para que pueda interactuar con la capa DAO,
 * extiende de la interfaz {@link com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Dao Dao}.
 * @author System Out of the Box
 * @version f3.1
 *  
 */
public interface BsLotepeDao extends Dao{

    /**
     * Atributo que almacena el identificador de la columna Instalacion mapeado
	 * al atributo Instalacion.
     */
    int INSTALACION = 10001;
    /**
     * Atributo que almacena el identificador de la columna Lote mapeado
	 * al atributo Lote.
     */
    int LOTE = 10002;
    /**
     * Atributo que almacena el identificador de la columna TipoConvenio mapeado
	 * al atributo Tipoconvenio.
     */
    int TIPOCONVENIO = 10030;
    /**
     * Atributo que almacena el identificador de la columna Convenio mapeado
	 * al atributo Convenio.
     */
    int CONVENIO = 10003;
    /**
     * Atributo que almacena el identificador de la columna EmpresaODepto mapeado
	 * al atributo Empresaodepto.
     */
    int EMPRESAODEPTO = 10004;
    /**
     * Atributo que almacena el identificador de la columna TipoLote mapeado
	 * al atributo Tipolote.
     */
    int TIPOLOTE = 10005;
    /**
     * Atributo que almacena el identificador de la columna MontoTotal mapeado
	 * al atributo Montototal.
     */
    int MONTOTOTAL = 10006;
    /**
     * Atributo que almacena el identificador de la columna NumOperaciones mapeado
	 * al atributo Numoperaciones.
     */
    int NUMOPERACIONES = 10007;
    /**
     * Atributo que almacena el identificador de la columna NombreLote mapeado
	 * al atributo Nombrelote.
     */
    int NOMBRELOTE = 10008;
    /**
     * Atributo que almacena el identificador de la columna FechaEnviado mapeado
	 * al atributo Fechaenviado.
     */
    int FECHAENVIADO = 10009;
    /**
     * Atributo que almacena el identificador de la columna FechaRecibido mapeado
	 * al atributo Fecharecibido.
     */
    int FECHARECIBIDO = 10010;
    /**
     * Atributo que almacena el identificador de la columna FechaAplicacion mapeado
	 * al atributo Fechaaplicacion.
     */
    int FECHAAPLICACION = 10011;
    /**
     * Atributo que almacena el identificador de la columna FechaEstatus mapeado
	 * al atributo Fechaestatus.
     */
    int FECHAESTATUS = 10012;
    /**
     * Atributo que almacena el identificador de la columna Estatus mapeado
	 * al atributo Estatus.
     */
    int ESTATUS = 10013;
    /**
     * Atributo que almacena el identificador de la columna NumeroReintento mapeado
	 * al atributo Numeroreintento.
     */
    int NUMEROREINTENTO = 10014;
    /**
     * Atributo que almacena el identificador de la columna UsuarioIngreso mapeado
	 * al atributo Usuarioingreso.
     */
    int USUARIOINGRESO = 10015;
    /**
     * Atributo que almacena el identificador de la columna Autorizaciones mapeado
	 * al atributo Autorizaciones.
     */
    int AUTORIZACIONES = 10016;
    /**
     * Atributo que almacena el identificador de la columna AplicacionDebitoHost mapeado
	 * al atributo Aplicaciondebitohost.
     */
    int APLICACIONDEBITOHOST = 10017;
    /**
     * Atributo que almacena el identificador de la columna Sys_MarcaTiempo mapeado
	 * al atributo SysMarcatiempo.
     */
    int SYSMARCATIEMPO = 10018;
    /**
     * Atributo que almacena el identificador de la columna Sys_ProcessId mapeado
	 * al atributo SysProcessid.
     */
    int SYSPROCESSID = 10019;
    /**
     * Atributo que almacena el identificador de la columna Sys_LowDateTime mapeado
	 * al atributo SysLowdatetime.
     */
    int SYSLOWDATETIME = 10020;
    /**
     * Atributo que almacena el identificador de la columna Sys_HighDateTime mapeado
	 * al atributo SysHighdatetime.
     */
    int SYSHIGHDATETIME = 10021;
    /**
     * Atributo que almacena el identificador de la columna Ponderacion mapeado
	 * al atributo Ponderacion.
     */
    int PONDERACION = 10022;
    /**
     * Atributo que almacena el identificador de la columna Firmas mapeado
	 * al atributo Firmas.
     */
    int FIRMAS = 10023;
    /**
     * Atributo que almacena el identificador de la columna FechaEnvioHost mapeado
	 * al atributo Fechaenviohost.
     */
    int FECHAENVIOHOST = 10024;
    /**
     * Atributo que almacena el identificador de la columna ListoParaHost mapeado
	 * al atributo Listoparahost.
     */
    int LISTOPARAHOST = 10025;
    /**
     * Atributo que almacena el identificador de la columna FechaHoraProcesado mapeado
	 * al atributo Fechahoraprocesado.
     */
    int FECHAHORAPROCESADO = 10026;
    /**
     * Atributo que almacena el identificador de la columna Token mapeado
	 * al atributo Token.
     */
    int TOKEN = 10027;
    /**
     * Atributo que almacena el identificador de la columna ComentarioRechazo mapeado
	 * al atributo Comentariorechazo.
     */
    int COMENTARIORECHAZO = 10028;
    /**
     * Atributo que almacena el identificador de la columna MontoImpuesto mapeado
	 * al atributo Montoimpuesto.
     */
    int MONTOIMPUESTO = 10029;

    /**
     * M&eacute;todo  que devuelve un DTO, formado con un objeto devuelto por la capa DAO,
	 * obtenido a trav&eacute;s de la b&uacute;squeda por id.<br/>
     * El DTO devuelto representa un registro a nivel de base de datos.
     * @param <code>id</code> Objeto que identifica la llave del DTO.
     * @return <code>Objeto</code>
     * @throws ExcepcionEnDA
     */
	BsLotepe buscarPorID(BsLotepeID bsLotepeID) throws ExcepcionEnDAO;
    /**
     * M&eacute;todo que debe eliminar un objeto a trav&eacute;s de un id.<br/>
     * Elimina un registro a nivel de la base de datos.
     * @param <code>id</code> Objeto que identifica la llave del DTO. 
     * @throws ExcepcionEnDAO
     */
	void eliminar(BsLotepeID bsLotepeID) throws ExcepcionEnDAO;	
    /**
     * M&eacute;todo que debe actualizar un objeto.
     * Actualiza un registro a nivel de la base de datos.
     * @param <code>Objeto</code> DTO
     * @throws ExcepcionEnDAO
     */	
	void actualizar(BsLotepe bsLotepe) throws ExcepcionEnDAO;
	/**
	 * M&eacute;todo que debe crear un objeto para el negocio. 
     * Crea un nuevo registro a nivel de base de datos.
	 * @param <code>Objeto</code> DTO.
	 * @throws ExcepcionEnDAO
	 */	
	void crear(BsLotepe bsLotepe) throws ExcepcionEnDAO;
    /**
     * M&eacute;todo que devuelve un DTO, solicitado a trav&eacute;s de un objeto de tipo {@link PeticionDeDatos}.
     * @param peticion objeto de tipo <code>PeticionDeDatos</code>
     * @return <code>Objeto</code> DTO.
     * @throws ExcepcionEnDAO
     * @throws ExcepcionPorDatoNoEncontrado
     */
	BsLotepe resultadoUnico(PeticionDeDatos peticion) throws ExcepcionEnDAO, ExcepcionPorDatoNoEncontrado;
	
	/**
	 * M&eacute;todo que devuelve un DTO for&aacute;neo, relacionado con el DTO espec&iacute;ficado.
	 * @param <code>Objeto</code> DTO
	 * @return <code>Objeto</code> - DTO for&aacute;neo
	 * @throws ExcepcionEnDAO
	 */
    BsConvenio getBsConvenio(BsLotepe bsLotepe)throws ExcepcionEnDAO;
    /**
     * M&eacute;todo que devuelve el DTO for&aacute;neo, relacionado con el DTO espec&iacute;ficado,
     * y utilizando un objeto de tipo <code>Transaccion</code>.
     * 
     * @param <code>Objeto</code> DTO
     * @param <code>tx</code> objeto del tipo {@link Transaccion}
     * @return <code>Objeto</code> - DTO for&aacute;neo
     * @throws ExcepcionEnDAO
     */
	BsConvenio getBsConvenio(BsLotepe bsLotepe, Transaccion tx)throws ExcepcionEnDAO;
    	 
}