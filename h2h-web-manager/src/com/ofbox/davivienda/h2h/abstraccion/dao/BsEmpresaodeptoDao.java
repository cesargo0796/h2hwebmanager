package com.ofbox.davivienda.h2h.abstraccion.dao;

import com.ofbox.davivienda.h2h.abstraccion.dto.BsEmpresaodepto;
import java.lang.Long;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.ExcepcionEnDAO;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.ExcepcionPorDatoNoEncontrado;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Paginador;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.PeticionDeDatos;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Dao;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Transaccion;

/**
 * Interfaz que define el comportamiento para un objeto de negocio espec&iacute;fico, para que pueda interactuar con la capa DAO,
 * extiende de la interfaz {@link com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Dao Dao}.
 * @author System Out of the Box
 * @version f3.1
 *  
 */
public interface BsEmpresaodeptoDao extends Dao{

    /**
     * Atributo que almacena el identificador de la columna EmpresaODepto mapeado
	 * al atributo Empresaodepto.
     */
    int EMPRESAODEPTO = 7001;
    /**
     * Atributo que almacena el identificador de la columna Cliente mapeado
	 * al atributo Cliente.
     */
    int CLIENTE = 7002;
    /**
     * Atributo que almacena el identificador de la columna Nombre mapeado
	 * al atributo Nombre.
     */
    int NOMBRE = 7003;
    /**
     * Atributo que almacena el identificador de la columna Direccion mapeado
	 * al atributo Direccion.
     */
    int DIRECCION = 7004;
    /**
     * Atributo que almacena el identificador de la columna Telefono mapeado
	 * al atributo Telefono.
     */
    int TELEFONO = 7005;
    /**
     * Atributo que almacena el identificador de la columna Fax mapeado
	 * al atributo Fax.
     */
    int FAX = 7006;
    /**
     * Atributo que almacena el identificador de la columna Email mapeado
	 * al atributo Email.
     */
    int EMAIL = 7007;
    /**
     * Atributo que almacena el identificador de la columna NombreContacto mapeado
	 * al atributo Nombrecontacto.
     */
    int NOMBRECONTACTO = 7008;
    /**
     * Atributo que almacena el identificador de la columna ApellidoContacto mapeado
	 * al atributo Apellidocontacto.
     */
    int APELLIDOCONTACTO = 7009;
    /**
     * Atributo que almacena el identificador de la columna Proveedor mapeado
	 * al atributo Proveedor.
     */
    int PROVEEDOR = 7010;
    /**
     * Atributo que almacena el identificador de la columna NivelSeguridad mapeado
	 * al atributo Nivelseguridad.
     */
    int NIVELSEGURIDAD = 7011;
    /**
     * Atributo que almacena el identificador de la columna FechaEstatus mapeado
	 * al atributo Fechaestatus.
     */
    int FECHAESTATUS = 7012;
    /**
     * Atributo que almacena el identificador de la columna Estatus mapeado
	 * al atributo Estatus.
     */
    int ESTATUS = 7013;
    /**
     * Atributo que almacena el identificador de la columna OrigenLotes mapeado
	 * al atributo Origenlotes.
     */
    int ORIGENLOTES = 7014;
    /**
     * Atributo que almacena el identificador de la columna RegistroFiscal mapeado
	 * al atributo Registrofiscal.
     */
    int REGISTROFISCAL = 7015;
    /**
     * Atributo que almacena el identificador de la columna NumPatronal mapeado
	 * al atributo Numpatronal.
     */
    int NUMPATRONAL = 7016;
    /**
     * Atributo que almacena el identificador de la columna RequiereCompro mapeado
	 * al atributo Requierecompro.
     */
    int REQUIERECOMPRO = 7017;
    /**
     * Atributo que almacena el identificador de la columna EsConsumidorFinal mapeado
	 * al atributo Esconsumidorfinal.
     */
    int ESCONSUMIDORFINAL = 7018;
    /**
     * Atributo que almacena el identificador de la columna GiroEmpresa mapeado
	 * al atributo Giroempresa.
     */
    int GIROEMPRESA = 7019;
    /**
     * Atributo que almacena el identificador de la columna Ciudad mapeado
	 * al atributo Ciudad.
     */
    int CIUDAD = 7020;
    /**
     * Atributo que almacena el identificador de la columna Cargo mapeado
	 * al atributo Cargo.
     */
    int CARGO = 7021;
    /**
     * Atributo que almacena el identificador de la columna LimiteXArchivo mapeado
	 * al atributo Limitexarchivo.
     */
    int LIMITEXARCHIVO = 7022;
    /**
     * Atributo que almacena el identificador de la columna LimiteXLote mapeado
	 * al atributo Limitexlote.
     */
    int LIMITEXLOTE = 7023;
    /**
     * Atributo que almacena el identificador de la columna LimiteXTransaccion mapeado
	 * al atributo Limitextransaccion.
     */
    int LIMITEXTRANSACCION = 7024;

    /**
     * M&eacute;todo  que devuelve un DTO, formado con un objeto devuelto por la capa DAO,
	 * obtenido a trav&eacute;s de la b&uacute;squeda por id.<br/>
     * El DTO devuelto representa un registro a nivel de base de datos.
     * @param <code>id</code> Objeto que identifica la llave del DTO.
     * @return <code>Objeto</code>
     * @throws ExcepcionEnDA
     */
	BsEmpresaodepto buscarPorID(Long empresaodeptoId) throws ExcepcionEnDAO;
    /**
     * M&eacute;todo que debe eliminar un objeto a trav&eacute;s de un id.<br/>
     * Elimina un registro a nivel de la base de datos.
     * @param <code>id</code> Objeto que identifica la llave del DTO. 
     * @throws ExcepcionEnDAO
     */
	void eliminar(Long empresaodeptoId) throws ExcepcionEnDAO;	
    /**
     * M&eacute;todo que debe actualizar un objeto.
     * Actualiza un registro a nivel de la base de datos.
     * @param <code>Objeto</code> DTO
     * @throws ExcepcionEnDAO
     */	
	void actualizar(BsEmpresaodepto bsEmpresaodepto) throws ExcepcionEnDAO;
	/**
	 * M&eacute;todo que debe crear un objeto para el negocio. 
     * Crea un nuevo registro a nivel de base de datos.
	 * @param <code>Objeto</code> DTO.
	 * @throws ExcepcionEnDAO
	 */	
	void crear(BsEmpresaodepto bsEmpresaodepto) throws ExcepcionEnDAO;
    /**
     * M&eacute;todo que devuelve un DTO, solicitado a trav&eacute;s de un objeto de tipo {@link PeticionDeDatos}.
     * @param peticion objeto de tipo <code>PeticionDeDatos</code>
     * @return <code>Objeto</code> DTO.
     * @throws ExcepcionEnDAO
     * @throws ExcepcionPorDatoNoEncontrado
     */
	BsEmpresaodepto resultadoUnico(PeticionDeDatos peticion) throws ExcepcionEnDAO, ExcepcionPorDatoNoEncontrado;
	
    	 
}