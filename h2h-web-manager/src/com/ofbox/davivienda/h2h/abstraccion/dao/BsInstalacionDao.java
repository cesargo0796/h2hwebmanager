package com.ofbox.davivienda.h2h.abstraccion.dao;

import com.ofbox.davivienda.h2h.abstraccion.dto.BsInstalacion;
import java.lang.Long;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.ExcepcionEnDAO;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.ExcepcionPorDatoNoEncontrado;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Paginador;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.PeticionDeDatos;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Dao;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Transaccion;

/**
 * Interfaz que define el comportamiento para un objeto de negocio espec&iacute;fico, para que pueda interactuar con la capa DAO,
 * extiende de la interfaz {@link com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Dao Dao}.
 * @author System Out of the Box
 * @version f3.1
 *  
 */
public interface BsInstalacionDao extends Dao{

    /**
     * Atributo que almacena el identificador de la columna Instalacion mapeado
	 * al atributo Instalacion.
     */
    int INSTALACION = 8001;
    /**
     * Atributo que almacena el identificador de la columna Cliente mapeado
	 * al atributo Cliente.
     */
    int CLIENTE = 8002;
    /**
     * Atributo que almacena el identificador de la columna Modulos mapeado
	 * al atributo Modulos.
     */
    int MODULOS = 8003;
    /**
     * Atributo que almacena el identificador de la columna Nombre mapeado
	 * al atributo Nombre.
     */
    int NOMBRE = 8004;
    /**
     * Atributo que almacena el identificador de la columna ID mapeado
	 * al atributo Id.
     */
    int ID = 8005;
    /**
     * Atributo que almacena el identificador de la columna Password mapeado
	 * al atributo Password.
     */
    int PASSWORD = 8006;
    /**
     * Atributo que almacena el identificador de la columna PesoA mapeado
	 * al atributo Pesoa.
     */
    int PESOA = 8007;
    /**
     * Atributo que almacena el identificador de la columna PesoB mapeado
	 * al atributo Pesob.
     */
    int PESOB = 8008;
    /**
     * Atributo que almacena el identificador de la columna PesoC mapeado
	 * al atributo Pesoc.
     */
    int PESOC = 8009;
    /**
     * Atributo que almacena el identificador de la columna PesoD mapeado
	 * al atributo Pesod.
     */
    int PESOD = 8010;
    /**
     * Atributo que almacena el identificador de la columna PesoE mapeado
	 * al atributo Pesoe.
     */
    int PESOE = 8011;
    /**
     * Atributo que almacena el identificador de la columna EstatusCom mapeado
	 * al atributo Estatuscom.
     */
    int ESTATUSCOM = 8012;
    /**
     * Atributo que almacena el identificador de la columna FechaEstatus mapeado
	 * al atributo Fechaestatus.
     */
    int FECHAESTATUS = 8013;
    /**
     * Atributo que almacena el identificador de la columna Estatus mapeado
	 * al atributo Estatus.
     */
    int ESTATUS = 8014;
    /**
     * Atributo que almacena el identificador de la columna Envio_Cliente mapeado
	 * al atributo EnvioCliente.
     */
    int ENVIOCLIENTE = 8015;
    /**
     * Atributo que almacena el identificador de la columna version mapeado
	 * al atributo Version.
     */
    int VERSION = 8016;
    /**
     * Atributo que almacena el identificador de la columna versionPE mapeado
	 * al atributo Versionpe.
     */
    int VERSIONPE = 8017;
    /**
     * Atributo que almacena el identificador de la columna OrigenLotes mapeado
	 * al atributo Origenlotes.
     */
    int ORIGENLOTES = 8018;
    /**
     * Atributo que almacena el identificador de la columna SiguienteLoteCC mapeado
	 * al atributo Siguientelotecc.
     */
    int SIGUIENTELOTECC = 8019;
    /**
     * Atributo que almacena el identificador de la columna SiguienteLotePE mapeado
	 * al atributo Siguientelotepe.
     */
    int SIGUIENTELOTEPE = 8020;
    /**
     * Atributo que almacena el identificador de la columna SiguienteLotePP mapeado
	 * al atributo Siguientelotepp.
     */
    int SIGUIENTELOTEPP = 8021;
    /**
     * Atributo que almacena el identificador de la columna SiguienteLotePS mapeado
	 * al atributo Siguienteloteps.
     */
    int SIGUIENTELOTEPS = 8022;
    /**
     * Atributo que almacena el identificador de la columna SiguienteLoteACH mapeado
	 * al atributo Siguienteloteach.
     */
    int SIGUIENTELOTEACH = 8023;

    /**
     * M&eacute;todo  que devuelve un DTO, formado con un objeto devuelto por la capa DAO,
	 * obtenido a trav&eacute;s de la b&uacute;squeda por id.<br/>
     * El DTO devuelto representa un registro a nivel de base de datos.
     * @param <code>id</code> Objeto que identifica la llave del DTO.
     * @return <code>Objeto</code>
     * @throws ExcepcionEnDA
     */
	BsInstalacion buscarPorID(Long instalacionId) throws ExcepcionEnDAO;
    /**
     * M&eacute;todo que debe eliminar un objeto a trav&eacute;s de un id.<br/>
     * Elimina un registro a nivel de la base de datos.
     * @param <code>id</code> Objeto que identifica la llave del DTO. 
     * @throws ExcepcionEnDAO
     */
	void eliminar(Long instalacionId) throws ExcepcionEnDAO;	
    /**
     * M&eacute;todo que debe actualizar un objeto.
     * Actualiza un registro a nivel de la base de datos.
     * @param <code>Objeto</code> DTO
     * @throws ExcepcionEnDAO
     */	
	void actualizar(BsInstalacion bsInstalacion) throws ExcepcionEnDAO;
	/**
	 * M&eacute;todo que debe crear un objeto para el negocio. 
     * Crea un nuevo registro a nivel de base de datos.
	 * @param <code>Objeto</code> DTO.
	 * @throws ExcepcionEnDAO
	 */	
	void crear(BsInstalacion bsInstalacion) throws ExcepcionEnDAO;
    /**
     * M&eacute;todo que devuelve un DTO, solicitado a trav&eacute;s de un objeto de tipo {@link PeticionDeDatos}.
     * @param peticion objeto de tipo <code>PeticionDeDatos</code>
     * @return <code>Objeto</code> DTO.
     * @throws ExcepcionEnDAO
     * @throws ExcepcionPorDatoNoEncontrado
     */
	BsInstalacion resultadoUnico(PeticionDeDatos peticion) throws ExcepcionEnDAO, ExcepcionPorDatoNoEncontrado;
	
    	 
}