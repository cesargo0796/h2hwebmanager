package com.ofbox.davivienda.h2h.abstraccion.dao;

import com.ofbox.davivienda.h2h.abstraccion.dto.BsBancoAch;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Dao;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.ExcepcionEnDAO;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.ExcepcionPorDatoNoEncontrado;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.PeticionDeDatos;

/**
 * Interfaz que define el comportamiento para un objeto de negocio espec&iacute;fico, para que pueda interactuar con la capa DAO,
 * extiende de la interfaz {@link com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Dao Dao}.
 * @author System Out of the Box
 * @version f3.1
 *  
 */
public interface BsBancoAchDao extends Dao{

    /**
     * Atributo que almacena el identificador de la columna CodBanco mapeado
	 * al atributo CodBanco.
     */
    int CODBANCO = 5001;
    /**
     * Atributo que almacena la interpretacion de codPayBank
	 * al atributo codPayBank.
     */
    int CODPAYBANK = 5002;

    /**
     * Atributo que almacena el identificador de la columna DigitoChequeo mapeado
	 * al atributo DigitoChequeo.
     */
    int DIGITOCHEQUEO = 5003;
    /**
     * Atributo que almacena la interpretacion de EnLinea
	 * al atributo EnLinea.
     */
    int ENLINEA = 5004;

    /**
     * Atributo que almacena el identificador de la columna EsActivo mapeado
	 * al atributo EsActivo.
     */
    int ESACTIVO = 5005;
    /**
     * Atributo que almacena la interpretacion de IdAs400
	 * al atributo IdAs400.
     */
    int IDAS400 = 5006;

    /**
     * Atributo que almacena el identificador de la columna LongitudCtaAhorro mapeado
	 * al atributo LongitudCtaAhorro.
     */
    int LONGITUDCTAAHORRO = 5007;
    /**
     * Atributo que almacena la interpretacion de LongitudCtaCorriente
	 * al atributo LongitudCtaCorriente.
     */
    int LONGITUDCTACORRIENTE = 5008;

    /**
     * Atributo que almacena el identificador de la columna LongitudCtaPrestamo mapeado
	 * al atributo LongitudCtaPrestamo.
     */
    int LONGITUDCTAPRESTAMO = 5009;
    /**
     * Atributo que almacena la interpretacion de LongitudCtaTjCredito
	 * al atributo LongitudCtaTjCredito.
     */
    int LONGITUDTJCREDITO = 5010;

    /**
     * Atributo que almacena el identificador de la columna Moneda mapeado
	 * al atributo Moneda.
     */
    int MONEDA = 5011;
    /**
     * Atributo que almacena la interpretacion de Nombre
	 * al atributo Nombre.
     */
    int NOMBRE = 5012;
    
    /**
     * M&eacute;todo  que devuelve un DTO, formado con un objeto devuelto por la capa DAO,
	 * obtenido a trav&eacute;s de la b&uacute;squeda por id.<br/>
     * El DTO devuelto representa un registro a nivel de base de datos.
     * @param <code>id</code> Objeto que identifica la llave del DTO.
     * @return <code>Objeto</code>
     * @throws ExcepcionEnDA
     */
    BsBancoAch buscarPorID(BsBancoAch bsBancoAch) throws ExcepcionEnDAO;
    /**
     * M&eacute;todo que debe eliminar un objeto a trav&eacute;s de un id.<br/>
     * Elimina un registro a nivel de la base de datos.
     * @param <code>id</code> Objeto que identifica la llave del DTO. 
     * @throws ExcepcionEnDAO
     */
	void eliminar(BsBancoAch bsBancoAch) throws ExcepcionEnDAO;	
    /**
     * M&eacute;todo que debe actualizar un objeto.
     * Actualiza un registro a nivel de la base de datos.
     * @param <code>Objeto</code> DTO
     * @throws ExcepcionEnDAO
     */	
	void actualizar(BsBancoAch bsBancoAch) throws ExcepcionEnDAO;
	/**
	 * M&eacute;todo que debe crear un objeto para el negocio. 
     * Crea un nuevo registro a nivel de base de datos.
	 * @param <code>Objeto</code> DTO.
	 * @throws ExcepcionEnDAO
	 */	
	void crear(BsBancoAch bsBancoAch) throws ExcepcionEnDAO;
    /**
     * M&eacute;todo que devuelve un DTO, solicitado a trav&eacute;s de un objeto de tipo {@link PeticionDeDatos}.
     * @param peticion objeto de tipo <code>PeticionDeDatos</code>
     * @return <code>Objeto</code> DTO.
     * @throws ExcepcionEnDAO
     * @throws ExcepcionPorDatoNoEncontrado
     */
	BsBancoAch resultadoUnico(PeticionDeDatos peticion) throws ExcepcionEnDAO, ExcepcionPorDatoNoEncontrado;
	
    	 
}