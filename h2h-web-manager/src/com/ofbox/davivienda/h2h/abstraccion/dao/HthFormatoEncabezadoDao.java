package com.ofbox.davivienda.h2h.abstraccion.dao;

import com.ofbox.davivienda.h2h.abstraccion.dto.HthFormatoEncabezado;
import java.lang.Long;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.ExcepcionEnDAO;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.ExcepcionPorDatoNoEncontrado;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Paginador;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.PeticionDeDatos;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Dao;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Transaccion;

/**
 * Interfaz que define el comportamiento para un objeto de negocio espec&iacute;fico, para que pueda interactuar con la capa DAO,
 * extiende de la interfaz {@link com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Dao Dao}.
 * @author System Out of the Box
 * @version f3.1
 *  
 */
public interface HthFormatoEncabezadoDao extends Dao{

    /**
     * Atributo que almacena el identificador de la columna id_formato mapeado
	 * al atributo IdFormato.
     */
    int IDFORMATO = 15001;
    /**
     * Atributo que almacena el identificador de la columna nombre_formato mapeado
	 * al atributo NombreFormato.
     */
    int NOMBREFORMATO = 15002;
    /**
     * Atributo que almacena el identificador de la columna tipo_formato mapeado
	 * al atributo TipoFormato.
     */
    int TIPOFORMATO = 15003;
    /**
     * Atributo que almacena el identificador de la columna tipo_separador mapeado
	 * al atributo TipoSeparador.
     */
    int TIPOSEPARADOR = 15004;
    /**
     * Atributo que almacena el identificador de la columna caracter_separador mapeado
	 * al atributo CaracterSeparador.
     */
    int CARACTERSEPARADOR = 15005;
    /**
     * Atributo que almacena el identificador de la columna lote_destino mapeado
	 * al atributo LoteDestino.
     */
    int LOTEDESTINO = 15006;
    /**
     * Atributo que almacena el identificador de la columna usuario_creacion mapeado
	 * al atributo UsuarioCreacion.
     */
    int USUARIOCREACION = 15007;
    /**
     * Atributo que almacena el identificador de la columna fecha_creacion mapeado
	 * al atributo FechaCreacion.
     */
    int FECHACREACION = 15008;
    /**
     * Atributo que almacena el identificador de la columna usuario_ult_act mapeado
	 * al atributo UsuarioUltAct.
     */
    int USUARIOULTACT = 15009;
    /**
     * Atributo que almacena el identificador de la columna fecha_ult_act mapeado
	 * al atributo FechaUltAct.
     */
    int FECHAULTACT = 15010;

    /**
     * M&eacute;todo  que devuelve un DTO, formado con un objeto devuelto por la capa DAO,
	 * obtenido a trav&eacute;s de la b&uacute;squeda por id.<br/>
     * El DTO devuelto representa un registro a nivel de base de datos.
     * @param <code>id</code> Objeto que identifica la llave del DTO.
     * @return <code>Objeto</code>
     * @throws ExcepcionEnDA
     */
	HthFormatoEncabezado buscarPorID(Long idFormatoId) throws ExcepcionEnDAO;
    /**
     * M&eacute;todo que debe eliminar un objeto a trav&eacute;s de un id.<br/>
     * Elimina un registro a nivel de la base de datos.
     * @param <code>id</code> Objeto que identifica la llave del DTO. 
     * @throws ExcepcionEnDAO
     */
	void eliminar(Long idFormatoId) throws ExcepcionEnDAO;	
    /**
     * M&eacute;todo que debe actualizar un objeto.
     * Actualiza un registro a nivel de la base de datos.
     * @param <code>Objeto</code> DTO
     * @throws ExcepcionEnDAO
     */	
	void actualizar(HthFormatoEncabezado hthFormatoEncabezado) throws ExcepcionEnDAO;
	/**
	 * M&eacute;todo que debe crear un objeto para el negocio. 
     * Crea un nuevo registro a nivel de base de datos.
	 * @param <code>Objeto</code> DTO.
	 * @throws ExcepcionEnDAO
	 */	
	void crear(HthFormatoEncabezado hthFormatoEncabezado) throws ExcepcionEnDAO;
    /**
     * M&eacute;todo que devuelve un DTO, solicitado a trav&eacute;s de un objeto de tipo {@link PeticionDeDatos}.
     * @param peticion objeto de tipo <code>PeticionDeDatos</code>
     * @return <code>Objeto</code> DTO.
     * @throws ExcepcionEnDAO
     * @throws ExcepcionPorDatoNoEncontrado
     */
	HthFormatoEncabezado resultadoUnico(PeticionDeDatos peticion) throws ExcepcionEnDAO, ExcepcionPorDatoNoEncontrado;
	
    	 
}