package com.ofbox.davivienda.h2h.abstraccion.dao;

import com.ofbox.davivienda.h2h.abstraccion.dto.HthParametroSistema;
import java.lang.Long;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.ExcepcionEnDAO;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.ExcepcionPorDatoNoEncontrado;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Paginador;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.PeticionDeDatos;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Dao;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Transaccion;

/**
 * Interfaz que define el comportamiento para un objeto de negocio espec&iacute;fico, para que pueda interactuar con la capa DAO,
 * extiende de la interfaz {@link com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Dao Dao}.
 * @author System Out of the Box
 * @version f3.1
 *  
 */
public interface HthParametroSistemaDao extends Dao{

    /**
     * Atributo que almacena el identificador de la columna id_parametro_sistema mapeado
	 * al atributo IdParametroSistema.
     */
    int IDPARAMETROSISTEMA = 18001;
    /**
     * Atributo que almacena el identificador de la columna tiempo_monitoreo_default mapeado
	 * al atributo TiempoMonitoreoDefault.
     */
    int TIEMPOMONITOREODEFAULT = 18002;
    /**
     * Atributo que almacena el identificador de la columna hora_inicio_monitoreo mapeado
	 * al atributo HoraInicioMonitoreo.
     */
    int HORAINICIOMONITOREO = 18003;
    /**
     * Atributo que almacena el identificador de la columna hora_fin_monitoreo mapeado
	 * al atributo HoraFinMonitoreo.
     */
    int HORAFINMONITOREO = 18004;
    /**
     * Atributo que almacena el identificador de la columna carpeta_lectura_default mapeado
	 * al atributo CarpetaLecturaDefault.
     */
    int CARPETALECTURADEFAULT = 18005;
    /**
     * Atributo que almacena el identificador de la columna carpeta_escritura_default mapeado
	 * al atributo CarpetaEscrituraDefault.
     */
    int CARPETAESCRITURADEFAULT = 18006;
    /**
     * Atributo que almacena el identificador de la columna ruta_raiz mapeado
	 * al atributo RutaRaiz.
     */
    int RUTARAIZ = 18007;
    /**
     * Atributo que almacena el identificador de la columna car_separador_default mapeado
	 * al atributo CarSeparadorDefault.
     */
    int CARSEPARADORDEFAULT = 18008;
    /**
     * Atributo que almacena el identificador de la columna habilitar_envio_correo mapeado
	 * al atributo HabilitarEnvioCorreo.
     */
    int HABILITARENVIOCORREO = 18009;
    /**
     * Atributo que almacena el identificador de la columna remitente_envio_correo mapeado
	 * al atributo RemitenteEnvioCorreo.
     */
    int REMITENTEENVIOCORREO = 18010;
    /**
     * Atributo que almacena el identificador de la columna asunto_envio_correo mapeado
	 * al atributo AsuntoEnvioCorreo.
     */
    int ASUNTOENVIOCORREO = 18011;
    /**
     * Atributo que almacena el identificador de la columna javamail_jndi mapeado
	 * al atributo JavamailJndi.
     */
    int JAVAMAILJNDI = 18012;
    /**
     * Atributo que almacena el identificador de la columna lista_envio_correo mapeado
	 * al atributo ListaEnvioCorreo.
     */
    int LISTAENVIOCORREO = 18013;
    /**
     * Atributo que almacena el identificador de la columna servidor_envio_correo mapeado
	 * al atributo ServidorEnvioCorreo.
     */
    int SERVIDORENVIOCORREO = 18014;
    /**
     * Atributo que almacena el identificador de la columna usuario_envio_correo mapeado
	 * al atributo UsuarioEnvioCorreo.
     */
    int USUARIOENVIOCORREO = 18015;
    /**
     * Atributo que almacena el identificador de la columna password_envio_correo mapeado
	 * al atributo PasswordEnvioCorreo.
     */
    int PASSWORDENVIOCORREO = 18016;
    /**
     * Atributo que almacena el identificador de la columna puerto_envio_correo mapeado
	 * al atributo PuertoEnvioCorreo.
     */
    int PUERTOENVIOCORREO = 18017;
    /**
     * Atributo que almacena el identificador de la columna directorio_local_destino mapeado
	 * al atributo DirectorioLocalDestino.
     */
    int DIRECTORIOLOCALDESTINO = 18018;
    /**
     * Atributo que almacena el identificador de la columna host_sftp mapeado
	 * al atributo HostSftp.
     */
    int HOSTSFTP = 18019;
    /**
     * Atributo que almacena el identificador de la columna puerto_sftp mapeado
	 * al atributo PuertoSftp.
     */
    int PUERTOSFTP = 18020;
    /**
     * Atributo que almacena el identificador de la columna usuario_sftp mapeado
	 * al atributo UsuarioSftp.
     */
    int USUARIOSFTP = 18021;
    /**
     * Atributo que almacena el identificador de la columna password_sftp mapeado
	 * al atributo PasswordSftp.
     */
    int PASSWORDSFTP = 18022;
    /**
     * Atributo que almacena el identificador de la columna llave_sftp mapeado
	 * al atributo LlaveSftp.
     */
    int LLAVESFTP = 18023;
    /**
     * Atributo que almacena el identificador de la columna tiempo_drenado_bitacora mapeado
	 * al atributo TiempoDrenadoBitacora.
     */
    int TIEMPODRENADOBITACORA = 18024;

    /**
     * M&eacute;todo  que devuelve un DTO, formado con un objeto devuelto por la capa DAO,
	 * obtenido a trav&eacute;s de la b&uacute;squeda por id.<br/>
     * El DTO devuelto representa un registro a nivel de base de datos.
     * @param <code>id</code> Objeto que identifica la llave del DTO.
     * @return <code>Objeto</code>
     * @throws ExcepcionEnDA
     */
	HthParametroSistema buscarPorID(Long idParametroSistemaId) throws ExcepcionEnDAO;
    /**
     * M&eacute;todo que debe eliminar un objeto a trav&eacute;s de un id.<br/>
     * Elimina un registro a nivel de la base de datos.
     * @param <code>id</code> Objeto que identifica la llave del DTO. 
     * @throws ExcepcionEnDAO
     */
	void eliminar(Long idParametroSistemaId) throws ExcepcionEnDAO;	
    /**
     * M&eacute;todo que debe actualizar un objeto.
     * Actualiza un registro a nivel de la base de datos.
     * @param <code>Objeto</code> DTO
     * @throws ExcepcionEnDAO
     */	
	void actualizar(HthParametroSistema hthParametroSistema) throws ExcepcionEnDAO;
	/**
	 * M&eacute;todo que debe crear un objeto para el negocio. 
     * Crea un nuevo registro a nivel de base de datos.
	 * @param <code>Objeto</code> DTO.
	 * @throws ExcepcionEnDAO
	 */	
	void crear(HthParametroSistema hthParametroSistema) throws ExcepcionEnDAO;
    /**
     * M&eacute;todo que devuelve un DTO, solicitado a trav&eacute;s de un objeto de tipo {@link PeticionDeDatos}.
     * @param peticion objeto de tipo <code>PeticionDeDatos</code>
     * @return <code>Objeto</code> DTO.
     * @throws ExcepcionEnDAO
     * @throws ExcepcionPorDatoNoEncontrado
     */
	HthParametroSistema resultadoUnico(PeticionDeDatos peticion) throws ExcepcionEnDAO, ExcepcionPorDatoNoEncontrado;
	
	HthParametroSistema buscarParametrosSistema() throws ExcepcionEnDAO, ExcepcionPorDatoNoEncontrado;
}
