package com.ofbox.davivienda.h2h.abstraccion.dao;

import com.ofbox.davivienda.h2h.abstraccion.dto.BsLoteach;
import com.ofbox.davivienda.h2h.abstraccion.dto.BsLoteachID;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.ExcepcionEnDAO;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.ExcepcionPorDatoNoEncontrado;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Paginador;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.PeticionDeDatos;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Dao;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Transaccion;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.foraneos.DataConValoresForaneos;    
import com.ofbox.davivienda.h2h.abstraccion.dto.BsInstalacion;
import com.ofbox.davivienda.h2h.abstraccion.dto.BsConvenioempresaach;

/**
 * Interfaz que define el comportamiento para un objeto de negocio espec&iacute;fico, para que pueda interactuar con la capa DAO,
 * extiende de la interfaz {@link com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Dao Dao}.
 * @author System Out of the Box
 * @version f3.1
 *  
 */
public interface BsLoteachDao extends Dao{

    /**
     * Atributo que almacena el identificador de la columna CodLote mapeado
	 * al atributo Codlote.
     */
    int CODLOTE = 9001;
    /**
     * Atributo que almacena el identificador de la columna Instalacion mapeado
	 * al atributo Instalacion.
     */
    int INSTALACION = 9002;
    /**
     * Atributo que almacena el identificador de la columna Lote mapeado
	 * al atributo Lote.
     */
    int LOTE = 9003;
    /**
     * Atributo que almacena el identificador de la columna Convenio mapeado
	 * al atributo Convenio.
     */
    int CONVENIO = 9004;
    /**
     * Atributo que almacena el identificador de la columna EmpresaODepto mapeado
	 * al atributo Empresaodepto.
     */
    int EMPRESAODEPTO = 9038;
    /**
     * Atributo que almacena el identificador de la columna TipoLote mapeado
	 * al atributo Tipolote.
     */
    int TIPOLOTE = 9005;
    /**
     * Atributo que almacena el identificador de la columna MontoTotal mapeado
	 * al atributo Montototal.
     */
    int MONTOTOTAL = 9006;
    /**
     * Atributo que almacena el identificador de la columna NumOperaciones mapeado
	 * al atributo Numoperaciones.
     */
    int NUMOPERACIONES = 9007;
    /**
     * Atributo que almacena el identificador de la columna NombreLote mapeado
	 * al atributo Nombrelote.
     */
    int NOMBRELOTE = 9008;
    /**
     * Atributo que almacena el identificador de la columna FechaEnviado mapeado
	 * al atributo Fechaenviado.
     */
    int FECHAENVIADO = 9009;
    /**
     * Atributo que almacena el identificador de la columna FechaRecibido mapeado
	 * al atributo Fecharecibido.
     */
    int FECHARECIBIDO = 9010;
    /**
     * Atributo que almacena el identificador de la columna FechaAplicacion mapeado
	 * al atributo Fechaaplicacion.
     */
    int FECHAAPLICACION = 9011;
    /**
     * Atributo que almacena el identificador de la columna FechaEstatus mapeado
	 * al atributo Fechaestatus.
     */
    int FECHAESTATUS = 9012;
    /**
     * Atributo que almacena el identificador de la columna Estatus mapeado
	 * al atributo Estatus.
     */
    int ESTATUS = 9013;
    /**
     * Atributo que almacena el identificador de la columna UsuarioIngreso mapeado
	 * al atributo Usuarioingreso.
     */
    int USUARIOINGRESO = 9014;
    /**
     * Atributo que almacena el identificador de la columna Autorizaciones mapeado
	 * al atributo Autorizaciones.
     */
    int AUTORIZACIONES = 9015;
    /**
     * Atributo que almacena el identificador de la columna AplicacionDebitoHost mapeado
	 * al atributo Aplicaciondebitohost.
     */
    int APLICACIONDEBITOHOST = 9016;
    /**
     * Atributo que almacena el identificador de la columna Sys_MarcaTiempo mapeado
	 * al atributo SysMarcatiempo.
     */
    int SYSMARCATIEMPO = 9017;
    /**
     * Atributo que almacena el identificador de la columna Sys_ProcessId mapeado
	 * al atributo SysProcessid.
     */
    int SYSPROCESSID = 9018;
    /**
     * Atributo que almacena el identificador de la columna Sys_LowDateTime mapeado
	 * al atributo SysLowdatetime.
     */
    int SYSLOWDATETIME = 9019;
    /**
     * Atributo que almacena el identificador de la columna Sys_HighDateTime mapeado
	 * al atributo SysHighdatetime.
     */
    int SYSHIGHDATETIME = 9020;
    /**
     * Atributo que almacena el identificador de la columna Ponderacion mapeado
	 * al atributo Ponderacion.
     */
    int PONDERACION = 9021;
    /**
     * Atributo que almacena el identificador de la columna Firmas mapeado
	 * al atributo Firmas.
     */
    int FIRMAS = 9022;
    /**
     * Atributo que almacena el identificador de la columna FechaAplicacionHost mapeado
	 * al atributo Fechaaplicacionhost.
     */
    int FECHAAPLICACIONHOST = 9023;
    /**
     * Atributo que almacena el identificador de la columna ListoParaHost mapeado
	 * al atributo Listoparahost.
     */
    int LISTOPARAHOST = 9024;
    /**
     * Atributo que almacena el identificador de la columna FechaHoraProcesado mapeado
	 * al atributo Fechahoraprocesado.
     */
    int FECHAHORAPROCESADO = 9025;
    /**
     * Atributo que almacena el identificador de la columna Token mapeado
	 * al atributo Token.
     */
    int TOKEN = 9026;
    /**
     * Atributo que almacena el identificador de la columna ComentarioRechazo mapeado
	 * al atributo Comentariorechazo.
     */
    int COMENTARIORECHAZO = 9027;
    /**
     * Atributo que almacena el identificador de la columna Descripcion mapeado
	 * al atributo Descripcion.
     */
    int DESCRIPCION = 9028;
    /**
     * Atributo que almacena el identificador de la columna EstatusPayBank mapeado
	 * al atributo Estatuspaybank.
     */
    int ESTATUSPAYBANK = 9029;
    /**
     * Atributo que almacena el identificador de la columna ArchivoGenerado mapeado
	 * al atributo Archivogenerado.
     */
    int ARCHIVOGENERADO = 9030;
    /**
     * Atributo que almacena el identificador de la columna ComisionCobrada mapeado
	 * al atributo Comisioncobrada.
     */
    int COMISIONCOBRADA = 9031;
    /**
     * Atributo que almacena el identificador de la columna CuentaDebito mapeado
	 * al atributo Cuentadebito.
     */
    int CUENTADEBITO = 9032;
    /**
     * Atributo que almacena el identificador de la columna AutorizacionHost mapeado
	 * al atributo Autorizacionhost.
     */
    int AUTORIZACIONHOST = 9033;
    /**
     * Atributo que almacena el identificador de la columna DescEstatus mapeado
	 * al atributo Descestatus.
     */
    int DESCESTATUS = 9034;
    /**
     * Atributo que almacena el identificador de la columna idTransaccion mapeado
	 * al atributo Idtransaccion.
     */
    int IDTRANSACCION = 9035;
    /**
     * Atributo que almacena el identificador de la columna intActualizacion mapeado
	 * al atributo Intactualizacion.
     */
    int INTACTUALIZACION = 9036;
    /**
     * Atributo que almacena el identificador de la columna MontoImpuesto mapeado
	 * al atributo Montoimpuesto.
     */
    int MONTOIMPUESTO = 9037;

    /**
     * M&eacute;todo  que devuelve un DTO, formado con un objeto devuelto por la capa DAO,
	 * obtenido a trav&eacute;s de la b&uacute;squeda por id.<br/>
     * El DTO devuelto representa un registro a nivel de base de datos.
     * @param <code>id</code> Objeto que identifica la llave del DTO.
     * @return <code>Objeto</code>
     * @throws ExcepcionEnDA
     */
	BsLoteach buscarPorID(BsLoteachID bsLoteachID) throws ExcepcionEnDAO;
    /**
     * M&eacute;todo que debe eliminar un objeto a trav&eacute;s de un id.<br/>
     * Elimina un registro a nivel de la base de datos.
     * @param <code>id</code> Objeto que identifica la llave del DTO. 
     * @throws ExcepcionEnDAO
     */
	void eliminar(BsLoteachID bsLoteachID) throws ExcepcionEnDAO;	
    /**
     * M&eacute;todo que debe actualizar un objeto.
     * Actualiza un registro a nivel de la base de datos.
     * @param <code>Objeto</code> DTO
     * @throws ExcepcionEnDAO
     */	
	void actualizar(BsLoteach bsLoteach) throws ExcepcionEnDAO;
	/**
	 * M&eacute;todo que debe crear un objeto para el negocio. 
     * Crea un nuevo registro a nivel de base de datos.
	 * @param <code>Objeto</code> DTO.
	 * @throws ExcepcionEnDAO
	 */	
	void crear(BsLoteach bsLoteach) throws ExcepcionEnDAO;
    /**
     * M&eacute;todo que devuelve un DTO, solicitado a trav&eacute;s de un objeto de tipo {@link PeticionDeDatos}.
     * @param peticion objeto de tipo <code>PeticionDeDatos</code>
     * @return <code>Objeto</code> DTO.
     * @throws ExcepcionEnDAO
     * @throws ExcepcionPorDatoNoEncontrado
     */
	BsLoteach resultadoUnico(PeticionDeDatos peticion) throws ExcepcionEnDAO, ExcepcionPorDatoNoEncontrado;
	
	/**
	 * M&eacute;todo que devuelve un DTO for&aacute;neo, relacionado con el DTO espec&iacute;ficado.
	 * @param <code>Objeto</code> DTO
	 * @return <code>Objeto</code> - DTO for&aacute;neo
	 * @throws ExcepcionEnDAO
	 */
    BsInstalacion getBsInstalacion(BsLoteach bsLoteach)throws ExcepcionEnDAO;
    /**
     * M&eacute;todo que devuelve el DTO for&aacute;neo, relacionado con el DTO espec&iacute;ficado,
     * y utilizando un objeto de tipo <code>Transaccion</code>.
     * 
     * @param <code>Objeto</code> DTO
     * @param <code>tx</code> objeto del tipo {@link Transaccion}
     * @return <code>Objeto</code> - DTO for&aacute;neo
     * @throws ExcepcionEnDAO
     */
	BsInstalacion getBsInstalacion(BsLoteach bsLoteach, Transaccion tx)throws ExcepcionEnDAO;
	/**
	 * M&eacute;todo que devuelve un DTO for&aacute;neo, relacionado con el DTO espec&iacute;ficado.
	 * @param <code>Objeto</code> DTO
	 * @return <code>Objeto</code> - DTO for&aacute;neo
	 * @throws ExcepcionEnDAO
	 */
    BsConvenioempresaach getBsConvenioempresaach(BsLoteach bsLoteach)throws ExcepcionEnDAO;
    /**
     * M&eacute;todo que devuelve el DTO for&aacute;neo, relacionado con el DTO espec&iacute;ficado,
     * y utilizando un objeto de tipo <code>Transaccion</code>.
     * 
     * @param <code>Objeto</code> DTO
     * @param <code>tx</code> objeto del tipo {@link Transaccion}
     * @return <code>Objeto</code> - DTO for&aacute;neo
     * @throws ExcepcionEnDAO
     */
	BsConvenioempresaach getBsConvenioempresaach(BsLoteach bsLoteach, Transaccion tx)throws ExcepcionEnDAO;
    	 
}