package com.ofbox.davivienda.h2h.abstraccion.negocio;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;

import com.ofbox.davivienda.h2h.abstraccion.dao.HthParametroSistemaDao;
import com.ofbox.davivienda.h2h.abstraccion.dto.HthParametroSistema;
import com.ofbox.davivienda.h2h.dependenciasGlobales.H2hResolusor;
import com.ofbox.davivienda.h2h.mail.Correo;
import com.ofbox.davivienda.h2h.mail.ManejadorDeCorreos;
import com.ofbox.davivienda.h2h.proc.PoolProcessManager;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Dao;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Paginador;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.PeticionDeDatos;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.meta.DtoMeta;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.meta.DtoPropiedadMeta;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.estado.DatosAplicacion;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.estado.EstadoAplicacion;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.estado.EstadoNegocio;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.flujo.ProximaEjecucion;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.log.Log;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.log.LogServiceLocator;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.NegocioDesacoplado;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.NegocioOperaciones;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.dependencias.LocalizadorDeServiciosAplicacion;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.enlaces.CreadorDeEnlaceBusqueda;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.enlaces.CreadorDeEnlaceDetalle;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.enlaces.impl.CreadorDeEnlaceBusquedaDinImpl;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.filtro.CreadorDeFiltro;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.filtro.impl.CreadorDeFiltroSobreDtoMeta;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.impl.NegocioOperacionesBase;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.orden.CreadorDeOrdenamiento;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.orden.impl.CreadorDeOrdenamientoSobreDtoMeta;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.validacion.CreadorDeMensajes;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.validacion.ValidadorDeDatos;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.validacion.impl.ValidadorDeDatosSobreDtoMeta;




/**
 * Clase que hereda de {@link NegocioDesacoplado}. Esta clase se encarga de tener toda la informaci&oacute;n
 * y operaciones que pertenecen a un negocio en espec&iacute;fico, y permite modificar comportamientos de cada negocio.<br/>
 *
 * DTO_META: Atributo que almacena todas las reglas que rigen a las propiedades de un objeto de Negocio.
 * En esta secci&oacute;n se definen las reglas que condicionan a cada propiedad de este Negocio.
 * @see {@link DtoMeta}
 *
 * CREADORES_DETALLE: Atributo que alamcena objetos de tipo {@link CreadorDeEnlaceDetalle}, cada uno de estos objetos define la informaci&oacute;n
 * que se enlazar&aacute; a este negocio. <br/>
 * La relaci&oacute;n que surge con este atributo es la denominado maestro-detalle, este Negocio es el maestro u objeto for&aacute;neo
 * de cada uno de los objetos que se definen en el atributo.
 * 
 * Este arreglo tiene implementaciones del tipo {@link CreadorDeEnlaceBusquedaDinImpl} que son instanciados al momento de la creaci&oacute;n del atributo,
 * recibiendo en el constructor el atributo {@link #DTO_META} definido en este negocio y el DTO_META definido en el objeto negocio asociado,
 * conocido como  detalle, al mismo tiempo se definen los datos que enlazar&aacute;n al objeto maestro con el detalle.
 * @see {@link CreadorDeEnlaceDetalle} 
 *
 * CREADORES_BUSQUEDA: Atributo que almacena objetos de tipo {@link CreadorDeEnlaceBusqueda}, cada uno de estos objetos define la informaci&oacute;n
 * que se enlazar&aacute; con los objetos de negocio foraneos, a los cuales se les conoce como maestros.<br/>
 * La relaci&oacute;n que surge con este atributo es la denominado detalle-maestro, cada uno de los objetos definidos en este atributo son objetos
 * de Negocio maestro o foraneos.  
 *
 * Este arreglo tiene implementaciones del tipo {@link CreadorDeEnlaceBusquedaDinImpl}, que son instanciados al momento de la creaci&oacute;n del atributo,
 * recibiendo en el constructor el atributo {@link #DTO_META} definido en este negocio, y el DTO_META definido en el negocio asociado, conocido como maestro,
 * al mismo tiempo se definen las propiedades del maestro que ser&aacute;n visibles al momento de desplegar el listado de datos en modo lista de valores.
 *
 * validadorDeDatos: Atributo que almacena un objeto que contiene la informaci&oacute;n de validaci&oacute;n de datos para este Negocio.<br/>
 *
 * Al crear la instancia de la clase <code>ValidadorDeDatosSobreDtoMeta</code>, que implementa un objeto del tipo {@link ValidadorDeDatos}, 
 * permite sobreescribir cada uno de los m&eacute;todos que posee para validar datos, y de esta manera personalizar la validaci&oacute;n de las
 * propiedades del objeto de Negocio.
 *
 * creadorDeFiltro: Atributo que almacena un objeto que contiene la informaci&oacute;n del filtro aplicado.<br/>
 *
 * Al crear la instancia de la clase <code>CreadorDeFiltroSobreDtoMeta</code>, que implementa un objeto del tipo {@link CreadorDeFiltro}, 
 * permite sobreescribir cada uno de los m&eacute;todos que posee para aplicar filtro, y de esta manera personalizar la aplicaci&oacute;n del
 * filtro.
 *
 * creadorDeOrdenamiento: Atributo que almacena un objeto que contiene la informaci&oacute;n del ordenamiento aplicado.<br/>
 *
 * Al crear la instancia de la clase <code>CreadorDeOrdenamientoSobreDtoMeta</code>, que implementa un objeto del tipo {@link CreadorDeOrdenamiento}, 
 * permite sobreescribir cada uno de los m&eacute;todos que posee para aplicar ordenamiento, y de esta manera personalizar la aplicaci&oacute;n del
 * ordenamiento.
 *
 * negocioOperaciones: Atributo que almacena un objeto que contiene la informaci&oacute;n de la operaciones b&aacute;sicas para esta clase Negocio.<br/>
 *
 * Al crear la instancia de la clase <code>NegocioOperacionesBase</code>, que implementa un objeto del tipo {@link NegocioOperaciones}, 
 * permite sobreescribir cada uno de los m&eacute;todos que posee para aplicar operaciones de llenado de informaci&oacute;n. Entre las operaciones
 * b&aacute;sicas se encuentra el manejo de las colecciones de objetos detalle, objetos maestros y las reglas de negocio para cada propiedad.
 *
 * @author Systems Out of the Box
 * @version f3.1
 */

  public class HthParametroSistemaNegocio extends NegocioDesacoplado {
	protected Log log = LogServiceLocator.getDefaultImplementation().getLog(HthParametroSistemaNegocio.class);
	
	public static final DtoMeta DTO_META = new DtoMeta(H2hResolusor.getSingleton().getContenedorDeDtoMeta(), "hthParametroSistema",  HthParametroSistema.class, 	
			new DtoPropiedadMeta[]{
        new DtoPropiedadMeta(HthParametroSistemaDao.IDPARAMETROSISTEMA,"idParametroSistema",DtoPropiedadMeta.DATO_LLAVE , 10) 
     ,  new DtoPropiedadMeta(HthParametroSistemaDao.TIEMPOMONITOREODEFAULT,"tiempoMonitoreoDefault",DtoPropiedadMeta.NO_NULL , 10) 
     ,  new DtoPropiedadMeta(HthParametroSistemaDao.HORAINICIOMONITOREO,"horaInicioMonitoreo",DtoPropiedadMeta.NO_NULL , 12) 
     ,  new DtoPropiedadMeta(HthParametroSistemaDao.HORAFINMONITOREO,"horaFinMonitoreo",DtoPropiedadMeta.NO_NULL , 12) 
     ,  new DtoPropiedadMeta(HthParametroSistemaDao.CARPETALECTURADEFAULT,"carpetaLecturaDefault",DtoPropiedadMeta.NO_NULL , 100) 
     ,  new DtoPropiedadMeta(HthParametroSistemaDao.CARPETAESCRITURADEFAULT,"carpetaEscrituraDefault",DtoPropiedadMeta.NO_NULL , 100) 
     ,  new DtoPropiedadMeta(HthParametroSistemaDao.RUTARAIZ,"rutaRaiz",DtoPropiedadMeta.NO_NULL , 150) 
     ,  new DtoPropiedadMeta(HthParametroSistemaDao.CARSEPARADORDEFAULT,"carSeparadorDefault",DtoPropiedadMeta.NULL , 10) 
     ,  new DtoPropiedadMeta(HthParametroSistemaDao.HABILITARENVIOCORREO,"habilitarEnvioCorreo",DtoPropiedadMeta.NO_NULL , 1) 
     ,  new DtoPropiedadMeta(HthParametroSistemaDao.REMITENTEENVIOCORREO,"remitenteEnvioCorreo",DtoPropiedadMeta.NO_NULL , 100) 
     ,  new DtoPropiedadMeta(HthParametroSistemaDao.ASUNTOENVIOCORREO,"asuntoEnvioCorreo",DtoPropiedadMeta.NO_NULL , 200) 
     ,  new DtoPropiedadMeta(HthParametroSistemaDao.JAVAMAILJNDI,"javamailJndi",DtoPropiedadMeta.NO_NULL , 100) 
     ,  new DtoPropiedadMeta(HthParametroSistemaDao.LISTAENVIOCORREO,"listaEnvioCorreo",DtoPropiedadMeta.NO_NULL , 500) 
     ,  new DtoPropiedadMeta(HthParametroSistemaDao.SERVIDORENVIOCORREO,"servidorEnvioCorreo",DtoPropiedadMeta.NULL , 100) 
     ,  new DtoPropiedadMeta(HthParametroSistemaDao.USUARIOENVIOCORREO,"usuarioEnvioCorreo",DtoPropiedadMeta.NULL , 100) 
     ,  new DtoPropiedadMeta(HthParametroSistemaDao.PASSWORDENVIOCORREO,"passwordEnvioCorreo",DtoPropiedadMeta.NULL , 250) 
     ,  new DtoPropiedadMeta(HthParametroSistemaDao.PUERTOENVIOCORREO,"puertoEnvioCorreo",DtoPropiedadMeta.NULL , 10) 
     ,  new DtoPropiedadMeta(HthParametroSistemaDao.DIRECTORIOLOCALDESTINO,"directorioLocalDestino",DtoPropiedadMeta.NULL , 100) 
     ,  new DtoPropiedadMeta(HthParametroSistemaDao.HOSTSFTP,"hostSftp",DtoPropiedadMeta.NULL , 100) 
     ,  new DtoPropiedadMeta(HthParametroSistemaDao.PUERTOSFTP,"puertoSftp",DtoPropiedadMeta.NULL , 100) 
     ,  new DtoPropiedadMeta(HthParametroSistemaDao.USUARIOSFTP,"usuarioSftp",DtoPropiedadMeta.NULL , 100) 
     ,  new DtoPropiedadMeta(HthParametroSistemaDao.PASSWORDSFTP,"passwordSftp",DtoPropiedadMeta.NULL , 100) 
     ,  new DtoPropiedadMeta(HthParametroSistemaDao.LLAVESFTP,"llaveSftp",DtoPropiedadMeta.NULL , 200) 
     ,  new DtoPropiedadMeta(HthParametroSistemaDao.TIEMPODRENADOBITACORA,"tiempoDrenadoBitacora",DtoPropiedadMeta.NO_NULL , 10) 
	       },
            DtoMeta.OPERACIONES_BUSQUEDAS_CAMPOS_PRIOPIOS
    );
	
	
	
	public Log getLogNegocio(){
		return log;
	}
    
    
    
    
    
    
	public static final CreadorDeEnlaceDetalle[] CREADORES_DETALLE = new CreadorDeEnlaceDetalle[]{
     };
     
     
     
	public static final CreadorDeEnlaceBusqueda[] CREADORES_BUSQUEDA = new CreadorDeEnlaceBusqueda[]{
     };
     
	    
    /**
     * Atributo que almacena el objeto que permite acceder a la capa DAO para este negocio.
     * @see {@link Dao}
     */
    private  HthParametroSistemaDao hthParametroSistemaDao;
    /**
     * Devuelve el atributo que permite acceder a la capa DAO.
     * @return <code>Objeto</code> - objeto de tipo Dao.
     */
	public HthParametroSistemaDao getHthParametroSistemaDao() {
		if(hthParametroSistemaDao == null){
			hthParametroSistemaDao = (HthParametroSistemaDao)getDao();
		}
		return hthParametroSistemaDao;
	}    

	private ValidadorDeDatos validadorDeDatos = new ValidadorDeDatosSobreDtoMeta(DTO_META);
	
	private CreadorDeFiltro creadorDeFiltro = new CreadorDeFiltroSobreDtoMeta(DTO_META);
	
	private CreadorDeOrdenamiento creadorDeOrdenamiento = new CreadorDeOrdenamientoSobreDtoMeta(DTO_META);
	
	private NegocioOperaciones negocioOperaciones = new NegocioOperacionesBase(CREADORES_DETALLE, CREADORES_BUSQUEDA, DTO_META, H2hResolusor.getSingleton());
	
	/**
	 * Devuelve un objeto de tipo {@link NegocioOperaciones}
	 * @return <code>NegocioOperaciones</code>
	 */
	public NegocioOperaciones getNegocioOperaciones() {
		return negocioOperaciones;
	}	
	/**
	 * Devuelve un objeto de tipo {@link #ValidadorDeDatos}
	 * @return <code>ValidadorDeDatos</code>
	 */
	public ValidadorDeDatos getValidadorDeDatos() {
		return validadorDeDatos;
	}
	/**
	 * Devuelve un objeto de tipo {@link #CreadorDeFiltro}
	 * @return <code>CreadorDeFiltro</code>
	 */
	public CreadorDeFiltro getCreadorDeFiltro(){
	    return creadorDeFiltro;
	}
	/**
	 * Devuelve un objeto de tipo {@link #CreadorDeOrdenamiento}
	 * @return <code>CreadorDeOrdenamiento</code>
	 */
	public CreadorDeOrdenamiento getCreadorDeOrdenamiento(){
		return creadorDeOrdenamiento;
	}
	/**
	 * Devuelve un objeto Resolusor.
	 * @return <code>LocalizadorDeServiciosAplicacion</code>
	 */
	public LocalizadorDeServiciosAplicacion getLocalizadorDeServiciosAplicacion(){
		return H2hResolusor.getSingleton();
	}
	
	

	@Override
	public ProximaEjecucion doListado(DatosAplicacion datosAplicacion)
			throws Exception {
		return doInicio(datosAplicacion);
	}
	
	@Override
	public ProximaEjecucion doCrear(DatosAplicacion datosAplicacion)
			throws Exception {
		return doInicio(datosAplicacion);
	}
	
	@Override
	public ProximaEjecucion doVer(DatosAplicacion datosAplicacion)
			throws Exception {
		return doInicio(datosAplicacion);
	}
	
	@Override
	public ProximaEjecucion doInicio(DatosAplicacion datosAplicacion)
			throws Exception {
		HthParametroSistemaDao dao = (HthParametroSistemaDao) getDao();
		PeticionDeDatos peticion = dao.crearPeticionDeDatos();
		 /*No hay que agregar nada a la peticion de datos, por que en la buena teoria solo hay un registro*/
		Paginador paginador = dao.ejecutarPeticionDeDatos(peticion);
		Collection todosLosDatos = paginador.cargarTodosLosDatosEnMemoria();
		
		CreadorDeMensajes creadorDeMensajes = getCreadorDeMensajes(datosAplicacion);
		
		if(todosLosDatos.isEmpty()){
			creadorDeMensajes.agregarMensajeDeAdvertencia("Todavia no se ha creado el registro de parametros de sistema. Por favor ingrese los siguientes parametros");
			return super.doCrear(datosAplicacion);
		}
		EstadoAplicacion estadoAplicacion = datosAplicacion.getEstadoAplicacion();
		NegocioOperaciones negocioOperaciones = getNegocioOperaciones();
		EstadoNegocio estadoNegocio = negocioOperaciones.getEstadoNegocio(estadoAplicacion);
		estadoNegocio.setEstado(EstadoNegocio.ESTADO_ACTUALIZACION);		
		HthParametroSistema dto = (HthParametroSistema) todosLosDatos.iterator().next();
		estadoNegocio.setCache(dto);
		guardarDtoSiguienteCapa(datosAplicacion, dto);
		
		guardarDtoSiguienteCapa(datosAplicacion, dto);
		return proximaEjecucion("actualizar", datosAplicacion); 
	}
	
	
	@Override
	public ProximaEjecucion doCrearEjecutar(DatosAplicacion datosAplicacion)
			throws Exception {
		super.doCrearEjecutar(datosAplicacion);
		return doInicio(datosAplicacion);
	}
	
	@Override
	public ProximaEjecucion doActualizarEjecutar(DatosAplicacion datosAplicacion)
			throws Exception {
		ProximaEjecucion proxima = super.doActualizarEjecutar(datosAplicacion);
		
		
		log.info("Reiniciando manejador de procesos...");
		PoolProcessManager p = (PoolProcessManager) H2hResolusor.getSingleton().getInstancia("poolProcessManager");
		p.stopAll();
		p.loadSystemParams();
		p.startMonitor();
		log.info("Manejador de procesos reiniciado...");
		
		H2hResolusor.getSingleton().prepararUtileriaCorreo();
		
		CreadorDeMensajes cmv = getCreadorDeMensajes(datosAplicacion);
		if(!cmv.isErrores()){
			cmv.agregarMensajeSatisfactorio("Se han actualizado los parametros del Sistema.");
		}
		
		return proxima;
	}
	
	public ProximaEjecucion doMail(DatosAplicacion datosAplicacion) throws Exception{
		
		EstadoAplicacion estado = datosAplicacion.getEstadoAplicacion();
		NegocioOperaciones negocioOperaciones = getNegocioOperaciones();
		
		EstadoNegocio estadoNegocio = negocioOperaciones.getEstadoNegocio(estado);
		CreadorDeMensajes cmv = getCreadorDeMensajes(datosAplicacion);
		
		HthParametroSistemaDao dao = (HthParametroSistemaDao) getDao();
		PeticionDeDatos peticion = dao.crearPeticionDeDatos();
		Paginador paginador = dao.ejecutarPeticionDeDatos(peticion);
		Collection col = paginador.cargarTodosLosDatosEnMemoria();
		if(col.isEmpty()){
			cmv.agregarMensajeDeAdvertencia("No se han guardado los datos de configuracion de envio de correo electronico");
			return doInicio(datosAplicacion);
		}
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm a");
		HthParametroSistema param = (HthParametroSistema) col.iterator().next();
		Correo correo = new Correo();
		correo.setRemitente(param.getRemitenteEnvioCorreo());
		correo.setAsunto("Test de Correo");
		correo.setMensaje("Test de Correo. Enviado en " + sdf.format(new Date()) + " - Test de envio de correo - Sistema Host to Host ");
		
		String emails = param.getListaEnvioCorreo();
		String[] recpts = emails.split(",");
		if(recpts.length == 1){
			recpts = emails.split(" ");
		}
		for(String r : recpts){
			correo.agregarDestinatarios(r);
		}
		
		ManejadorDeCorreos mailSender = (ManejadorDeCorreos) H2hResolusor.getSingleton().getInstancia("mailSender");
		try{
			mailSender.sendMail(correo);
			cmv.agregarMensajeSatisfactorio("Se ha enviado un correo electronico de Prueba a la lista de destinatarios.  ");
		}catch(Exception e){
			e.printStackTrace();
			cmv.agregarMensajeDeError("No fue posible enviar correo electronico.  Error: " + e.getMessage());
		}
		
		
		
		return doInicio(datosAplicacion);
	}


}
