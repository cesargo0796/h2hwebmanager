package com.ofbox.davivienda.h2h.abstraccion.negocio;

import java.util.Collection;
import java.util.Iterator;



import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.enlaces.CreadorDeEnlaceDetalle;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.enlaces.impl.CreadorDeEnlaceDetalleDinImpl;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.enlaces.CreadorDeEnlaceBusqueda;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.enlaces.impl.CreadorDeEnlaceBusquedaDinImpl;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.enlaces.EnlaceDetalle;

import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.filtro.CreadorDeFiltro;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.filtro.impl.CreadorDeFiltroSobreDtoMeta;

import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.orden.CreadorDeOrdenamiento;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.orden.impl.CreadorDeOrdenamientoSobreDtoMeta;

import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dependencias.ExcepcionEnLocalizacionDeServicio;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dependencias.LocalizadorDeServicio;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dependencias.impl.LocalizadorDeSerivicioBase;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.dependencias.LocalizadorDeServiciosAplicacion;

import com.ofbox.f3.app.distribuibles.abstraccion.comunes.log.Log;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.log.LogServiceLocator;

import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.ExcepcionEnDAO;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Paginador;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.PeticionDeDatos;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.foraneos.DataConValoresForaneos;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.foraneos.DatoEnllavado;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.foraneos.Enllavador;

import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.NegocioOperaciones;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.NegocioDesacoplado;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.impl.NegocioOperacionesBase;

import com.ofbox.f3.app.distribuibles.abstraccion.comunes.validacion.ValidadorDeDatos;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.validacion.impl.ValidadorDeDatosSobreDtoMeta;

import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.meta.DtoMeta;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.meta.DtoPropiedadForaneoMeta;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.meta.DtoPropiedadMeta;

import com.ofbox.davivienda.h2h.dependenciasGlobales.H2hResolusor;
import com.ofbox.davivienda.h2h.abstraccion.dao.BsLoteachDao;
import com.ofbox.davivienda.h2h.abstraccion.dto.BsLoteach;



import com.ofbox.davivienda.h2h.abstraccion.dto.BsLoteachID; 

import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.foraneos.Enllavador;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.foraneos.impl.DatoEnllavadoImpl;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.foraneos.impl.EnllavadorDinImpl;
import com.ofbox.davivienda.h2h.abstraccion.negocio.BsInstalacionNegocio;
import com.ofbox.davivienda.h2h.abstraccion.negocio.BsConvenioempresaachNegocio;
/**
 * Clase que hereda de {@link NegocioDesacoplado}. Esta clase se encarga de tener toda la informaci&oacute;n
 * y operaciones que pertenecen a un negocio en espec&iacute;fico, y permite modificar comportamientos de cada negocio.<br/>
 *
 * DTO_META: Atributo que almacena todas las reglas que rigen a las propiedades de un objeto de Negocio.
 * En esta secci&oacute;n se definen las reglas que condicionan a cada propiedad de este Negocio.
 * @see {@link DtoMeta}
 *
 * CREADORES_DETALLE: Atributo que alamcena objetos de tipo {@link CreadorDeEnlaceDetalle}, cada uno de estos objetos define la informaci&oacute;n
 * que se enlazar&aacute; a este negocio. <br/>
 * La relaci&oacute;n que surge con este atributo es la denominado maestro-detalle, este Negocio es el maestro u objeto for&aacute;neo
 * de cada uno de los objetos que se definen en el atributo.
 * 
 * Este arreglo tiene implementaciones del tipo {@link CreadorDeEnlaceBusquedaDinImpl} que son instanciados al momento de la creaci&oacute;n del atributo,
 * recibiendo en el constructor el atributo {@link #DTO_META} definido en este negocio y el DTO_META definido en el objeto negocio asociado,
 * conocido como  detalle, al mismo tiempo se definen los datos que enlazar&aacute;n al objeto maestro con el detalle.
 * @see {@link CreadorDeEnlaceDetalle} 
 *
 * CREADORES_BUSQUEDA: Atributo que almacena objetos de tipo {@link CreadorDeEnlaceBusqueda}, cada uno de estos objetos define la informaci&oacute;n
 * que se enlazar&aacute; con los objetos de negocio foraneos, a los cuales se les conoce como maestros.<br/>
 * La relaci&oacute;n que surge con este atributo es la denominado detalle-maestro, cada uno de los objetos definidos en este atributo son objetos
 * de Negocio maestro o foraneos.  
 *
 * Este arreglo tiene implementaciones del tipo {@link CreadorDeEnlaceBusquedaDinImpl}, que son instanciados al momento de la creaci&oacute;n del atributo,
 * recibiendo en el constructor el atributo {@link #DTO_META} definido en este negocio, y el DTO_META definido en el negocio asociado, conocido como maestro,
 * al mismo tiempo se definen las propiedades del maestro que ser&aacute;n visibles al momento de desplegar el listado de datos en modo lista de valores.
 *
 * validadorDeDatos: Atributo que almacena un objeto que contiene la informaci&oacute;n de validaci&oacute;n de datos para este Negocio.<br/>
 *
 * Al crear la instancia de la clase <code>ValidadorDeDatosSobreDtoMeta</code>, que implementa un objeto del tipo {@link ValidadorDeDatos}, 
 * permite sobreescribir cada uno de los m&eacute;todos que posee para validar datos, y de esta manera personalizar la validaci&oacute;n de las
 * propiedades del objeto de Negocio.
 *
 * creadorDeFiltro: Atributo que almacena un objeto que contiene la informaci&oacute;n del filtro aplicado.<br/>
 *
 * Al crear la instancia de la clase <code>CreadorDeFiltroSobreDtoMeta</code>, que implementa un objeto del tipo {@link CreadorDeFiltro}, 
 * permite sobreescribir cada uno de los m&eacute;todos que posee para aplicar filtro, y de esta manera personalizar la aplicaci&oacute;n del
 * filtro.
 *
 * creadorDeOrdenamiento: Atributo que almacena un objeto que contiene la informaci&oacute;n del ordenamiento aplicado.<br/>
 *
 * Al crear la instancia de la clase <code>CreadorDeOrdenamientoSobreDtoMeta</code>, que implementa un objeto del tipo {@link CreadorDeOrdenamiento}, 
 * permite sobreescribir cada uno de los m&eacute;todos que posee para aplicar ordenamiento, y de esta manera personalizar la aplicaci&oacute;n del
 * ordenamiento.
 *
 * negocioOperaciones: Atributo que almacena un objeto que contiene la informaci&oacute;n de la operaciones b&aacute;sicas para esta clase Negocio.<br/>
 *
 * Al crear la instancia de la clase <code>NegocioOperacionesBase</code>, que implementa un objeto del tipo {@link NegocioOperaciones}, 
 * permite sobreescribir cada uno de los m&eacute;todos que posee para aplicar operaciones de llenado de informaci&oacute;n. Entre las operaciones
 * b&aacute;sicas se encuentra el manejo de las colecciones de objetos detalle, objetos maestros y las reglas de negocio para cada propiedad.
 *
 * @author Systems Out of the Box
 * @version f3.1
 */

  public class BsLoteachNegocio extends NegocioDesacoplado {
	protected Log log = LogServiceLocator.getDefaultImplementation().getLog(BsLoteachNegocio.class);
	
	public static final DtoMeta DTO_META = new DtoMeta(H2hResolusor.getSingleton().getContenedorDeDtoMeta(), "bsLoteach",  BsLoteach.class,  BsLoteachID.class, 	
			new DtoPropiedadMeta[]{
        new DtoPropiedadMeta(BsLoteachDao.CODLOTE,"codlote",DtoPropiedadMeta.DATO_LLAVE , 19) 
     ,  new DtoPropiedadMeta(BsLoteachDao.INSTALACION,"instalacion",DtoPropiedadMeta.DATO_LLAVE , 10) 
     ,  new DtoPropiedadMeta(BsLoteachDao.LOTE,"lote",DtoPropiedadMeta.DATO_LLAVE , 10) 
     ,  new DtoPropiedadMeta(BsLoteachDao.CONVENIO,"convenio",DtoPropiedadMeta.NO_NULL , 10) 
     ,  new DtoPropiedadMeta(BsLoteachDao.EMPRESAODEPTO,"empresaodepto",DtoPropiedadMeta.DATO_BUSQUEDA, new DtoPropiedadForaneoMeta( "bsConvenioempresaach","empresaodepto","convenio") , false, 10) 
     ,  new DtoPropiedadMeta(BsLoteachDao.TIPOLOTE,"tipolote",DtoPropiedadMeta.NULL , 3) 
     ,  new DtoPropiedadMeta(BsLoteachDao.MONTOTOTAL,"montototal",DtoPropiedadMeta.NO_NULL , 19) 
     ,  new DtoPropiedadMeta(BsLoteachDao.NUMOPERACIONES,"numoperaciones",DtoPropiedadMeta.NO_NULL , 10) 
     ,  new DtoPropiedadMeta(BsLoteachDao.NOMBRELOTE,"nombrelote",DtoPropiedadMeta.NULL , 30) 
     ,  new DtoPropiedadMeta(BsLoteachDao.FECHAENVIADO,"fechaenviado",DtoPropiedadMeta.NO_NULL , 23) 
     ,  new DtoPropiedadMeta(BsLoteachDao.FECHARECIBIDO,"fecharecibido",DtoPropiedadMeta.NULL , 23) 
     ,  new DtoPropiedadMeta(BsLoteachDao.FECHAAPLICACION,"fechaaplicacion",DtoPropiedadMeta.NULL , 23) 
     ,  new DtoPropiedadMeta(BsLoteachDao.FECHAESTATUS,"fechaestatus",DtoPropiedadMeta.NULL , 23) 
     ,  new DtoPropiedadMeta(BsLoteachDao.ESTATUS,"estatus",DtoPropiedadMeta.NO_NULL , 10) 
     ,  new DtoPropiedadMeta(BsLoteachDao.USUARIOINGRESO,"usuarioingreso",DtoPropiedadMeta.NULL , 10) 
     ,  new DtoPropiedadMeta(BsLoteachDao.AUTORIZACIONES,"autorizaciones",DtoPropiedadMeta.NULL , 3) 
     ,  new DtoPropiedadMeta(BsLoteachDao.APLICACIONDEBITOHOST,"aplicaciondebitohost",DtoPropiedadMeta.NULL , 3) 
     ,  new DtoPropiedadMeta(BsLoteachDao.SYSMARCATIEMPO,"sysMarcatiempo",DtoPropiedadMeta.NULL , 23) 
     ,  new DtoPropiedadMeta(BsLoteachDao.SYSPROCESSID,"sysProcessid",DtoPropiedadMeta.NULL , 10) 
     ,  new DtoPropiedadMeta(BsLoteachDao.SYSLOWDATETIME,"sysLowdatetime",DtoPropiedadMeta.NULL , 10) 
     ,  new DtoPropiedadMeta(BsLoteachDao.SYSHIGHDATETIME,"sysHighdatetime",DtoPropiedadMeta.NULL , 10) 
     ,  new DtoPropiedadMeta(BsLoteachDao.PONDERACION,"ponderacion",DtoPropiedadMeta.NO_NULL , 10) 
     ,  new DtoPropiedadMeta(BsLoteachDao.FIRMAS,"firmas",DtoPropiedadMeta.NO_NULL , 10) 
     ,  new DtoPropiedadMeta(BsLoteachDao.FECHAAPLICACIONHOST,"fechaaplicacionhost",DtoPropiedadMeta.NULL , 23) 
     ,  new DtoPropiedadMeta(BsLoteachDao.LISTOPARAHOST,"listoparahost",DtoPropiedadMeta.NULL , 23) 
     ,  new DtoPropiedadMeta(BsLoteachDao.FECHAHORAPROCESADO,"fechahoraprocesado",DtoPropiedadMeta.NULL , 23) 
     ,  new DtoPropiedadMeta(BsLoteachDao.TOKEN,"token",DtoPropiedadMeta.NULL , 10) 
     ,  new DtoPropiedadMeta(BsLoteachDao.COMENTARIORECHAZO,"comentariorechazo",DtoPropiedadMeta.NULL , 100) 
     ,  new DtoPropiedadMeta(BsLoteachDao.DESCRIPCION,"descripcion",DtoPropiedadMeta.NULL , 50) 
     ,  new DtoPropiedadMeta(BsLoteachDao.ESTATUSPAYBANK,"estatuspaybank",DtoPropiedadMeta.NULL , 20) 
     ,  new DtoPropiedadMeta(BsLoteachDao.ARCHIVOGENERADO,"archivogenerado",DtoPropiedadMeta.NULL , 100) 
     ,  new DtoPropiedadMeta(BsLoteachDao.COMISIONCOBRADA,"comisioncobrada",DtoPropiedadMeta.NULL , 19) 
     ,  new DtoPropiedadMeta(BsLoteachDao.CUENTADEBITO,"cuentadebito",DtoPropiedadMeta.NULL , 16) 
     ,  new DtoPropiedadMeta(BsLoteachDao.AUTORIZACIONHOST,"autorizacionhost",DtoPropiedadMeta.NULL , 10) 
     ,  new DtoPropiedadMeta(BsLoteachDao.DESCESTATUS,"descestatus",DtoPropiedadMeta.NULL , 500) 
     ,  new DtoPropiedadMeta(BsLoteachDao.IDTRANSACCION,"idtransaccion",DtoPropiedadMeta.NULL , 10) 
     ,  new DtoPropiedadMeta(BsLoteachDao.INTACTUALIZACION,"intactualizacion",DtoPropiedadMeta.NO_NULL , 10) 
     ,  new DtoPropiedadMeta(BsLoteachDao.MONTOIMPUESTO,"montoimpuesto",DtoPropiedadMeta.NULL , 19) 
	       },
    	   	DtoMeta.OPERACIONES_BUSQUEDAS_COMPLETAS	 
    );
	
	
	
	public Log getLogNegocio(){
		return log;
	}
    
    
    
    
    
    
	public static final CreadorDeEnlaceDetalle[] CREADORES_DETALLE = new CreadorDeEnlaceDetalle[]{
     };
     
     
     
	public static final CreadorDeEnlaceBusqueda[] CREADORES_BUSQUEDA = new CreadorDeEnlaceBusqueda[]{
        new CreadorDeEnlaceBusquedaDinImpl(DTO_META, BsInstalacionNegocio.DTO_META){
			public String[] getPropiedadesVisibles() {
                return new String[]{
                          "instalacion"
                       ,  "cliente"
                       ,  "modulos"
                       ,  "nombre"
                       ,  "password"
                       ,  "pesoa"
                       ,  "pesob"
                       ,  "pesoc"
                       ,  "pesod"
                       ,  "pesoe"
                       ,  "estatuscom"
                       ,  "fechaestatus"
                       ,  "estatus"
                       ,  "envioCliente"
                       ,  "version"
                       ,  "versionpe"
                       ,  "origenlotes"
                       ,  "siguientelotecc"
                       ,  "siguientelotepe"
                       ,  "siguientelotepp"
                       ,  "siguienteloteps"
                       ,  "siguienteloteach"
                };
			}
	     }			
     ,  new CreadorDeEnlaceBusquedaDinImpl(DTO_META, BsConvenioempresaachNegocio.DTO_META){
			public String[] getPropiedadesVisibles() {
                return new String[]{
                          "empresaodepto"
                       ,  "convenio"
                };
			}
	     }			
     };
     
	    
    /**
     * Atributo que almacena el objeto que permite acceder a la capa DAO para este negocio.
     * @see {@link Dao}
     */
    private  BsLoteachDao bsLoteachDao;
    /**
     * Devuelve el atributo que permite acceder a la capa DAO.
     * @return <code>Objeto</code> - objeto de tipo Dao.
     */
	public BsLoteachDao getBsLoteachDao() {
		if(bsLoteachDao == null){
			bsLoteachDao = (BsLoteachDao)getDao();
		}
		return bsLoteachDao;
	}    

	private ValidadorDeDatos validadorDeDatos = new ValidadorDeDatosSobreDtoMeta(DTO_META);
	
	private CreadorDeFiltro creadorDeFiltro = new CreadorDeFiltroSobreDtoMeta(DTO_META);
	
	private CreadorDeOrdenamiento creadorDeOrdenamiento = new CreadorDeOrdenamientoSobreDtoMeta(DTO_META);
	
	private NegocioOperaciones negocioOperaciones = new NegocioOperacionesBase(CREADORES_DETALLE, CREADORES_BUSQUEDA, DTO_META, H2hResolusor.getSingleton());
	
	/**
	 * Devuelve un objeto de tipo {@link NegocioOperaciones}
	 * @return <code>NegocioOperaciones</code>
	 */
	public NegocioOperaciones getNegocioOperaciones() {
		return negocioOperaciones;
	}	
	/**
	 * Devuelve un objeto de tipo {@link #ValidadorDeDatos}
	 * @return <code>ValidadorDeDatos</code>
	 */
	public ValidadorDeDatos getValidadorDeDatos() {
		return validadorDeDatos;
	}
	/**
	 * Devuelve un objeto de tipo {@link #CreadorDeFiltro}
	 * @return <code>CreadorDeFiltro</code>
	 */
	public CreadorDeFiltro getCreadorDeFiltro(){
	    return creadorDeFiltro;
	}
	/**
	 * Devuelve un objeto de tipo {@link #CreadorDeOrdenamiento}
	 * @return <code>CreadorDeOrdenamiento</code>
	 */
	public CreadorDeOrdenamiento getCreadorDeOrdenamiento(){
		return creadorDeOrdenamiento;
	}
	/**
	 * Devuelve un objeto Resolusor.
	 * @return <code>LocalizadorDeServiciosAplicacion</code>
	 */
	public LocalizadorDeServiciosAplicacion getLocalizadorDeServiciosAplicacion(){
		return H2hResolusor.getSingleton();
	}




}