package com.ofbox.davivienda.h2h.abstraccion.negocio;

import java.util.Collection;
import java.util.Iterator;


import com.ofbox.davivienda.h2h.abstraccion.dto.HthParametroGeneral;
import com.ofbox.davivienda.h2h.abstraccion.dto.BsCliente;
import com.ofbox.davivienda.h2h.abstraccion.dto.HthFormato;
import com.ofbox.davivienda.h2h.abstraccion.dto.BsConvenioempresa;
import com.ofbox.davivienda.h2h.abstraccion.dto.BsDetallepe;
import com.ofbox.davivienda.h2h.abstraccion.dto.BsConvenioempresaach;
import com.ofbox.davivienda.h2h.abstraccion.dto.HthAtributoDetalle;
import com.ofbox.davivienda.h2h.abstraccion.dto.HthFormatoEncabezado;
import com.ofbox.davivienda.h2h.abstraccion.dto.HthAtributoEncabezado;
import com.ofbox.davivienda.h2h.abstraccion.dto.BsInstalacion;
import com.ofbox.davivienda.h2h.abstraccion.dto.HthVistaConvenio;
import com.ofbox.davivienda.h2h.abstraccion.dto.HthBitacoraProceso;
import com.ofbox.davivienda.h2h.abstraccion.dto.BsLoteach;
import com.ofbox.davivienda.h2h.abstraccion.dto.BsDetalleach;
import com.ofbox.davivienda.h2h.abstraccion.dto.BsConvenio;
import com.ofbox.davivienda.h2h.abstraccion.dto.HthParametroSistema;
import com.ofbox.davivienda.h2h.abstraccion.dto.BsEmpresaodepto;
import com.ofbox.davivienda.h2h.abstraccion.dto.BsLotepe;
import com.ofbox.davivienda.h2h.abstraccion.dto.HthProcesoMonitoreo;
import com.ofbox.davivienda.h2h.abstraccion.dto.BsConvenioach;
import com.ofbox.davivienda.h2h.abstraccion.dto.HthAtributoFormato;

import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.enlaces.CreadorDeEnlaceDetalle;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.enlaces.impl.CreadorDeEnlaceDetalleDinImpl;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.enlaces.CreadorDeEnlaceBusqueda;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.enlaces.impl.CreadorDeEnlaceBusquedaDinImpl;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.enlaces.EnlaceDetalle;

import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.filtro.CreadorDeFiltro;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.filtro.impl.CreadorDeFiltroSobreDtoMeta;

import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.orden.CreadorDeOrdenamiento;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.orden.impl.CreadorDeOrdenamientoSobreDtoMeta;

import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dependencias.ExcepcionEnLocalizacionDeServicio;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dependencias.LocalizadorDeServicio;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dependencias.impl.LocalizadorDeSerivicioBase;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.dependencias.LocalizadorDeServiciosAplicacion;

import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.ExcepcionEnDAO;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Paginador;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.PeticionDeDatos;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.foraneos.DataConValoresForaneos;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.foraneos.DatoEnllavado;

import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.NegocioOperaciones;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.NegocioDesacoplado;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.impl.NegocioOperacionesBase;

import com.ofbox.f3.app.distribuibles.abstraccion.comunes.validacion.ValidadorDeDatos;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.validacion.impl.ValidadorDeDatosSobreDtoMeta;

import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.meta.DtoMeta;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.meta.DtoPropiedadForaneoMeta;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.meta.DtoPropiedadMeta;

import com.ofbox.davivienda.h2h.dependenciasGlobales.H2hResolusor;

import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Dao;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.ProcesadorDeDto;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.estado.DatosAplicacion;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.estado.EstadoAplicacion;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.estado.EstadoNegocio;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.flujo.ProximaEjecucion;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.ExcepcionEnNegocio;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.seguridad.AmbienteDeSeguridad;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.seguridad.ExcepcionDeSeguridad;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.validacion.CreadorDeMensajes;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.validacion.UtileriaDeTiposDeDatos;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.util.estado.EstadoUtils;

public class TestingNegocio extends NegocioDesacoplado {
	
	public ProximaEjecucion doDeleteHthParametroGeneral(DatosAplicacion datosAplicacion) throws Exception{
		NegocioDesacoplado hthParametroGeneralNegocio = (NegocioDesacoplado) H2hResolusor.getSingleton().getLocMultiNegocio().getServicio("hthParametroGeneralNegocio");
		final Dao hthParametroGeneralDao = hthParametroGeneralNegocio.getDao();
		hthParametroGeneralDao.ejecutarPeticionDeDatos(hthParametroGeneralDao.crearPeticionDeDatos()).procesarTodosLosDatos(new ProcesadorDeDto() {
			
			public void procesarDto(Object dto) throws ExcepcionEnDAO {
				HthParametroGeneral hthParametroGeneral = (HthParametroGeneral) dto;
				hthParametroGeneralDao.eliminar(hthParametroGeneral.getDtoId(), Dao.AUTO_COMMIT);
			}
		});
		
		return hthParametroGeneralNegocio.doListado(datosAplicacion);
	}
	
	public ProximaEjecucion doDeleteBsCliente(DatosAplicacion datosAplicacion) throws Exception{
		NegocioDesacoplado bsClienteNegocio = (NegocioDesacoplado) H2hResolusor.getSingleton().getLocMultiNegocio().getServicio("bsClienteNegocio");
		final Dao bsClienteDao = bsClienteNegocio.getDao();
		bsClienteDao.ejecutarPeticionDeDatos(bsClienteDao.crearPeticionDeDatos()).procesarTodosLosDatos(new ProcesadorDeDto() {
			
			public void procesarDto(Object dto) throws ExcepcionEnDAO {
				BsCliente bsCliente = (BsCliente) dto;
				bsClienteDao.eliminar(bsCliente.getDtoId(), Dao.AUTO_COMMIT);
			}
		});
		
		return bsClienteNegocio.doListado(datosAplicacion);
	}
	
	public ProximaEjecucion doDeleteHthFormato(DatosAplicacion datosAplicacion) throws Exception{
		NegocioDesacoplado hthFormatoNegocio = (NegocioDesacoplado) H2hResolusor.getSingleton().getLocMultiNegocio().getServicio("hthFormatoNegocio");
		final Dao hthFormatoDao = hthFormatoNegocio.getDao();
		hthFormatoDao.ejecutarPeticionDeDatos(hthFormatoDao.crearPeticionDeDatos()).procesarTodosLosDatos(new ProcesadorDeDto() {
			
			public void procesarDto(Object dto) throws ExcepcionEnDAO {
				HthFormato hthFormato = (HthFormato) dto;
				hthFormatoDao.eliminar(hthFormato.getDtoId(), Dao.AUTO_COMMIT);
			}
		});
		
		return hthFormatoNegocio.doListado(datosAplicacion);
	}
	
	public ProximaEjecucion doDeleteBsConvenioempresa(DatosAplicacion datosAplicacion) throws Exception{
		NegocioDesacoplado bsConvenioempresaNegocio = (NegocioDesacoplado) H2hResolusor.getSingleton().getLocMultiNegocio().getServicio("bsConvenioempresaNegocio");
		final Dao bsConvenioempresaDao = bsConvenioempresaNegocio.getDao();
		bsConvenioempresaDao.ejecutarPeticionDeDatos(bsConvenioempresaDao.crearPeticionDeDatos()).procesarTodosLosDatos(new ProcesadorDeDto() {
			
			public void procesarDto(Object dto) throws ExcepcionEnDAO {
				BsConvenioempresa bsConvenioempresa = (BsConvenioempresa) dto;
				bsConvenioempresaDao.eliminar(bsConvenioempresa.getDtoId(), Dao.AUTO_COMMIT);
			}
		});
		
		return bsConvenioempresaNegocio.doListado(datosAplicacion);
	}
	
	public ProximaEjecucion doDeleteBsDetallepe(DatosAplicacion datosAplicacion) throws Exception{
		NegocioDesacoplado bsDetallepeNegocio = (NegocioDesacoplado) H2hResolusor.getSingleton().getLocMultiNegocio().getServicio("bsDetallepeNegocio");
		final Dao bsDetallepeDao = bsDetallepeNegocio.getDao();
		bsDetallepeDao.ejecutarPeticionDeDatos(bsDetallepeDao.crearPeticionDeDatos()).procesarTodosLosDatos(new ProcesadorDeDto() {
			
			public void procesarDto(Object dto) throws ExcepcionEnDAO {
				BsDetallepe bsDetallepe = (BsDetallepe) dto;
				bsDetallepeDao.eliminar(bsDetallepe.getDtoId(), Dao.AUTO_COMMIT);
			}
		});
		
		return bsDetallepeNegocio.doListado(datosAplicacion);
	}
	
	public ProximaEjecucion doDeleteBsConvenioempresaach(DatosAplicacion datosAplicacion) throws Exception{
		NegocioDesacoplado bsConvenioempresaachNegocio = (NegocioDesacoplado) H2hResolusor.getSingleton().getLocMultiNegocio().getServicio("bsConvenioempresaachNegocio");
		final Dao bsConvenioempresaachDao = bsConvenioempresaachNegocio.getDao();
		bsConvenioempresaachDao.ejecutarPeticionDeDatos(bsConvenioempresaachDao.crearPeticionDeDatos()).procesarTodosLosDatos(new ProcesadorDeDto() {
			
			public void procesarDto(Object dto) throws ExcepcionEnDAO {
				BsConvenioempresaach bsConvenioempresaach = (BsConvenioempresaach) dto;
				bsConvenioempresaachDao.eliminar(bsConvenioempresaach.getDtoId(), Dao.AUTO_COMMIT);
			}
		});
		
		return bsConvenioempresaachNegocio.doListado(datosAplicacion);
	}
	
	public ProximaEjecucion doDeleteHthAtributoDetalle(DatosAplicacion datosAplicacion) throws Exception{
		NegocioDesacoplado hthAtributoDetalleNegocio = (NegocioDesacoplado) H2hResolusor.getSingleton().getLocMultiNegocio().getServicio("hthAtributoDetalleNegocio");
		final Dao hthAtributoDetalleDao = hthAtributoDetalleNegocio.getDao();
		hthAtributoDetalleDao.ejecutarPeticionDeDatos(hthAtributoDetalleDao.crearPeticionDeDatos()).procesarTodosLosDatos(new ProcesadorDeDto() {
			
			public void procesarDto(Object dto) throws ExcepcionEnDAO {
				HthAtributoDetalle hthAtributoDetalle = (HthAtributoDetalle) dto;
				hthAtributoDetalleDao.eliminar(hthAtributoDetalle.getDtoId(), Dao.AUTO_COMMIT);
			}
		});
		
		return hthAtributoDetalleNegocio.doListado(datosAplicacion);
	}
	
	public ProximaEjecucion doDeleteHthFormatoEncabezado(DatosAplicacion datosAplicacion) throws Exception{
		NegocioDesacoplado hthFormatoEncabezadoNegocio = (NegocioDesacoplado) H2hResolusor.getSingleton().getLocMultiNegocio().getServicio("hthFormatoEncabezadoNegocio");
		final Dao hthFormatoEncabezadoDao = hthFormatoEncabezadoNegocio.getDao();
		hthFormatoEncabezadoDao.ejecutarPeticionDeDatos(hthFormatoEncabezadoDao.crearPeticionDeDatos()).procesarTodosLosDatos(new ProcesadorDeDto() {
			
			public void procesarDto(Object dto) throws ExcepcionEnDAO {
				HthFormatoEncabezado hthFormatoEncabezado = (HthFormatoEncabezado) dto;
				hthFormatoEncabezadoDao.eliminar(hthFormatoEncabezado.getDtoId(), Dao.AUTO_COMMIT);
			}
		});
		
		return hthFormatoEncabezadoNegocio.doListado(datosAplicacion);
	}
	
	public ProximaEjecucion doDeleteHthAtributoEncabezado(DatosAplicacion datosAplicacion) throws Exception{
		NegocioDesacoplado hthAtributoEncabezadoNegocio = (NegocioDesacoplado) H2hResolusor.getSingleton().getLocMultiNegocio().getServicio("hthAtributoEncabezadoNegocio");
		final Dao hthAtributoEncabezadoDao = hthAtributoEncabezadoNegocio.getDao();
		hthAtributoEncabezadoDao.ejecutarPeticionDeDatos(hthAtributoEncabezadoDao.crearPeticionDeDatos()).procesarTodosLosDatos(new ProcesadorDeDto() {
			
			public void procesarDto(Object dto) throws ExcepcionEnDAO {
				HthAtributoEncabezado hthAtributoEncabezado = (HthAtributoEncabezado) dto;
				hthAtributoEncabezadoDao.eliminar(hthAtributoEncabezado.getDtoId(), Dao.AUTO_COMMIT);
			}
		});
		
		return hthAtributoEncabezadoNegocio.doListado(datosAplicacion);
	}
	
	public ProximaEjecucion doDeleteBsInstalacion(DatosAplicacion datosAplicacion) throws Exception{
		NegocioDesacoplado bsInstalacionNegocio = (NegocioDesacoplado) H2hResolusor.getSingleton().getLocMultiNegocio().getServicio("bsInstalacionNegocio");
		final Dao bsInstalacionDao = bsInstalacionNegocio.getDao();
		bsInstalacionDao.ejecutarPeticionDeDatos(bsInstalacionDao.crearPeticionDeDatos()).procesarTodosLosDatos(new ProcesadorDeDto() {
			
			public void procesarDto(Object dto) throws ExcepcionEnDAO {
				BsInstalacion bsInstalacion = (BsInstalacion) dto;
				bsInstalacionDao.eliminar(bsInstalacion.getDtoId(), Dao.AUTO_COMMIT);
			}
		});
		
		return bsInstalacionNegocio.doListado(datosAplicacion);
	}
	
	public ProximaEjecucion doDeleteHthVistaConvenio(DatosAplicacion datosAplicacion) throws Exception{
		NegocioDesacoplado hthVistaConvenioNegocio = (NegocioDesacoplado) H2hResolusor.getSingleton().getLocMultiNegocio().getServicio("hthVistaConvenioNegocio");
		final Dao hthVistaConvenioDao = hthVistaConvenioNegocio.getDao();
		hthVistaConvenioDao.ejecutarPeticionDeDatos(hthVistaConvenioDao.crearPeticionDeDatos()).procesarTodosLosDatos(new ProcesadorDeDto() {

			public void procesarDto(Object dto) throws ExcepcionEnDAO {
				HthVistaConvenio hthVistaConvenio = (HthVistaConvenio) dto;
				hthVistaConvenioDao.eliminar(hthVistaConvenio.getDtoId(), Dao.AUTO_COMMIT);
			}
		});
		
		return hthVistaConvenioNegocio.doListado(datosAplicacion);
	}
	
	public ProximaEjecucion doDeleteHthBitacoraProceso(DatosAplicacion datosAplicacion) throws Exception{
		NegocioDesacoplado hthBitacoraProcesoNegocio = (NegocioDesacoplado) H2hResolusor.getSingleton().getLocMultiNegocio().getServicio("hthBitacoraProcesoNegocio");
		final Dao hthBitacoraProcesoDao = hthBitacoraProcesoNegocio.getDao();
		hthBitacoraProcesoDao.ejecutarPeticionDeDatos(hthBitacoraProcesoDao.crearPeticionDeDatos()).procesarTodosLosDatos(new ProcesadorDeDto() {
			
			public void procesarDto(Object dto) throws ExcepcionEnDAO {
				HthBitacoraProceso hthBitacoraProceso = (HthBitacoraProceso) dto;
				hthBitacoraProcesoDao.eliminar(hthBitacoraProceso.getDtoId(), Dao.AUTO_COMMIT);
			}
		});
		
		return hthBitacoraProcesoNegocio.doListado(datosAplicacion);
	}
	
	public ProximaEjecucion doDeleteBsLoteach(DatosAplicacion datosAplicacion) throws Exception{
		NegocioDesacoplado bsLoteachNegocio = (NegocioDesacoplado) H2hResolusor.getSingleton().getLocMultiNegocio().getServicio("bsLoteachNegocio");
		final Dao bsLoteachDao = bsLoteachNegocio.getDao();
		bsLoteachDao.ejecutarPeticionDeDatos(bsLoteachDao.crearPeticionDeDatos()).procesarTodosLosDatos(new ProcesadorDeDto() {
			
			public void procesarDto(Object dto) throws ExcepcionEnDAO {
				BsLoteach bsLoteach = (BsLoteach) dto;
				bsLoteachDao.eliminar(bsLoteach.getDtoId(), Dao.AUTO_COMMIT);
			}
		});
		
		return bsLoteachNegocio.doListado(datosAplicacion);
	}
	
	public ProximaEjecucion doDeleteBsDetalleach(DatosAplicacion datosAplicacion) throws Exception{
		NegocioDesacoplado bsDetalleachNegocio = (NegocioDesacoplado) H2hResolusor.getSingleton().getLocMultiNegocio().getServicio("bsDetalleachNegocio");
		final Dao bsDetalleachDao = bsDetalleachNegocio.getDao();
		bsDetalleachDao.ejecutarPeticionDeDatos(bsDetalleachDao.crearPeticionDeDatos()).procesarTodosLosDatos(new ProcesadorDeDto() {
			
			public void procesarDto(Object dto) throws ExcepcionEnDAO {
				BsDetalleach bsDetalleach = (BsDetalleach) dto;
				bsDetalleachDao.eliminar(bsDetalleach.getDtoId(), Dao.AUTO_COMMIT);
			}
		});
		
		return bsDetalleachNegocio.doListado(datosAplicacion);
	}
	
	public ProximaEjecucion doDeleteBsConvenio(DatosAplicacion datosAplicacion) throws Exception{
		NegocioDesacoplado bsConvenioNegocio = (NegocioDesacoplado) H2hResolusor.getSingleton().getLocMultiNegocio().getServicio("bsConvenioNegocio");
		final Dao bsConvenioDao = bsConvenioNegocio.getDao();
		bsConvenioDao.ejecutarPeticionDeDatos(bsConvenioDao.crearPeticionDeDatos()).procesarTodosLosDatos(new ProcesadorDeDto() {
			
			public void procesarDto(Object dto) throws ExcepcionEnDAO {
				BsConvenio bsConvenio = (BsConvenio) dto;
				bsConvenioDao.eliminar(bsConvenio.getDtoId(), Dao.AUTO_COMMIT);
			}
		});
		
		return bsConvenioNegocio.doListado(datosAplicacion);
	}
	
	public ProximaEjecucion doDeleteHthParametroSistema(DatosAplicacion datosAplicacion) throws Exception{
		NegocioDesacoplado hthParametroSistemaNegocio = (NegocioDesacoplado) H2hResolusor.getSingleton().getLocMultiNegocio().getServicio("hthParametroSistemaNegocio");
		final Dao hthParametroSistemaDao = hthParametroSistemaNegocio.getDao();
		hthParametroSistemaDao.ejecutarPeticionDeDatos(hthParametroSistemaDao.crearPeticionDeDatos()).procesarTodosLosDatos(new ProcesadorDeDto() {
			
			public void procesarDto(Object dto) throws ExcepcionEnDAO {
				HthParametroSistema hthParametroSistema = (HthParametroSistema) dto;
				hthParametroSistemaDao.eliminar(hthParametroSistema.getDtoId(), Dao.AUTO_COMMIT);
			}
		});
		
		return hthParametroSistemaNegocio.doListado(datosAplicacion);
	}
	
	public ProximaEjecucion doDeleteBsEmpresaodepto(DatosAplicacion datosAplicacion) throws Exception{
		NegocioDesacoplado bsEmpresaodeptoNegocio = (NegocioDesacoplado) H2hResolusor.getSingleton().getLocMultiNegocio().getServicio("bsEmpresaodeptoNegocio");
		final Dao bsEmpresaodeptoDao = bsEmpresaodeptoNegocio.getDao();
		bsEmpresaodeptoDao.ejecutarPeticionDeDatos(bsEmpresaodeptoDao.crearPeticionDeDatos()).procesarTodosLosDatos(new ProcesadorDeDto() {
			
			public void procesarDto(Object dto) throws ExcepcionEnDAO {
				BsEmpresaodepto bsEmpresaodepto = (BsEmpresaodepto) dto;
				bsEmpresaodeptoDao.eliminar(bsEmpresaodepto.getDtoId(), Dao.AUTO_COMMIT);
			}
		});
		
		return bsEmpresaodeptoNegocio.doListado(datosAplicacion);
	}
	
	public ProximaEjecucion doDeleteBsLotepe(DatosAplicacion datosAplicacion) throws Exception{
		NegocioDesacoplado bsLotepeNegocio = (NegocioDesacoplado) H2hResolusor.getSingleton().getLocMultiNegocio().getServicio("bsLotepeNegocio");
		final Dao bsLotepeDao = bsLotepeNegocio.getDao();
		bsLotepeDao.ejecutarPeticionDeDatos(bsLotepeDao.crearPeticionDeDatos()).procesarTodosLosDatos(new ProcesadorDeDto() {
			
			public void procesarDto(Object dto) throws ExcepcionEnDAO {
				BsLotepe bsLotepe = (BsLotepe) dto;
				bsLotepeDao.eliminar(bsLotepe.getDtoId(), Dao.AUTO_COMMIT);
			}
		});
		
		return bsLotepeNegocio.doListado(datosAplicacion);
	}
	
	public ProximaEjecucion doDeleteHthProcesoMonitoreo(DatosAplicacion datosAplicacion) throws Exception{
		NegocioDesacoplado hthProcesoMonitoreoNegocio = (NegocioDesacoplado) H2hResolusor.getSingleton().getLocMultiNegocio().getServicio("hthProcesoMonitoreoNegocio");
		final Dao hthProcesoMonitoreoDao = hthProcesoMonitoreoNegocio.getDao();
		hthProcesoMonitoreoDao.ejecutarPeticionDeDatos(hthProcesoMonitoreoDao.crearPeticionDeDatos()).procesarTodosLosDatos(new ProcesadorDeDto() {
			
			public void procesarDto(Object dto) throws ExcepcionEnDAO {
				HthProcesoMonitoreo hthProcesoMonitoreo = (HthProcesoMonitoreo) dto;
				hthProcesoMonitoreoDao.eliminar(hthProcesoMonitoreo.getDtoId(), Dao.AUTO_COMMIT);
			}
		});
		
		return hthProcesoMonitoreoNegocio.doListado(datosAplicacion);
	}
	
	public ProximaEjecucion doDeleteBsConvenioach(DatosAplicacion datosAplicacion) throws Exception{
		NegocioDesacoplado bsConvenioachNegocio = (NegocioDesacoplado) H2hResolusor.getSingleton().getLocMultiNegocio().getServicio("bsConvenioachNegocio");
		final Dao bsConvenioachDao = bsConvenioachNegocio.getDao();
		bsConvenioachDao.ejecutarPeticionDeDatos(bsConvenioachDao.crearPeticionDeDatos()).procesarTodosLosDatos(new ProcesadorDeDto() {
			
			public void procesarDto(Object dto) throws ExcepcionEnDAO {
				BsConvenioach bsConvenioach = (BsConvenioach) dto;
				bsConvenioachDao.eliminar(bsConvenioach.getDtoId(), Dao.AUTO_COMMIT);
			}
		});
		
		return bsConvenioachNegocio.doListado(datosAplicacion);
	}
	
	public ProximaEjecucion doDeleteHthAtributoFormato(DatosAplicacion datosAplicacion) throws Exception{
		NegocioDesacoplado hthAtributoFormatoNegocio = (NegocioDesacoplado) H2hResolusor.getSingleton().getLocMultiNegocio().getServicio("hthAtributoFormatoNegocio");
		final Dao hthAtributoFormatoDao = hthAtributoFormatoNegocio.getDao();
		hthAtributoFormatoDao.ejecutarPeticionDeDatos(hthAtributoFormatoDao.crearPeticionDeDatos()).procesarTodosLosDatos(new ProcesadorDeDto() {
			
			public void procesarDto(Object dto) throws ExcepcionEnDAO {
				HthAtributoFormato hthAtributoFormato = (HthAtributoFormato) dto;
				hthAtributoFormatoDao.eliminar(hthAtributoFormato.getDtoId(), Dao.AUTO_COMMIT);
			}
		});
		
		return hthAtributoFormatoNegocio.doListado(datosAplicacion);
	}


	public NegocioOperaciones getNegocioOperaciones() {
		return null;
	}
}
