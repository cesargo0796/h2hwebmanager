package com.ofbox.davivienda.h2h.abstraccion.negocio;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;

import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerMetaData;
import org.quartz.Trigger;
import org.quartz.TriggerKey;
import org.quartz.impl.matchers.GroupMatcher;

import com.ofbox.davivienda.h2h.abstraccion.dao.BsEmpresaodeptoDao;
import com.ofbox.davivienda.h2h.abstraccion.dao.HthParametroSistemaDao;
import com.ofbox.davivienda.h2h.abstraccion.dao.HthProcesoMonitoreoDao;
import com.ofbox.davivienda.h2h.abstraccion.dao.HthVistaConvenioDao;
import com.ofbox.davivienda.h2h.abstraccion.dto.EstadoMonitor;
import com.ofbox.davivienda.h2h.abstraccion.dto.HthParametroSistema;
import com.ofbox.davivienda.h2h.abstraccion.dto.HthProcesoMonitoreo;
import com.ofbox.davivienda.h2h.abstraccion.dto.HthVistaConvenio;
import com.ofbox.davivienda.h2h.dependenciasGlobales.H2hResolusor;
import com.ofbox.davivienda.h2h.proc.PoolProcessManager;
import com.ofbox.davivienda.h2h.proc.Vars;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Dao;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.PeticionDeDatos;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.foraneos.Enllavador;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.foraneos.impl.EnllavadorDinImpl;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.meta.DtoMeta;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.meta.DtoPropiedadForaneoMeta;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.meta.DtoPropiedadMeta;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.estado.DatosAplicacion;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.estado.EstadoAplicacion;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.estado.EstadoNegocio;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.flujo.ProximaEjecucion;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.log.Log;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.log.LogServiceLocator;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.ExcepcionEnNegocio;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.NegocioDesacoplado;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.NegocioOperaciones;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.dependencias.LocalizadorDeServiciosAplicacion;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.enlaces.CreadorDeEnlaceBusqueda;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.enlaces.CreadorDeEnlaceDetalle;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.enlaces.EnlaceBusqueda;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.enlaces.impl.CreadorDeEnlaceBusquedaDinImpl;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.filtro.CreadorDeFiltro;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.filtro.impl.CreadorDeFiltroSobreDtoMeta;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.impl.NegocioOperacionesBase;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.orden.CreadorDeOrdenamiento;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.orden.impl.CreadorDeOrdenamientoSobreDtoMeta;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.seguridad.AmbienteDeSeguridad;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.seguridad.ExcepcionDeSeguridad;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.validacion.CreadorDeMensajes;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.validacion.ValidadorDeDatos;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.validacion.impl.ValidadorDeDatosSobreDtoMeta;
/**
 * Clase que hereda de {@link NegocioDesacoplado}. Esta clase se encarga de tener toda la informaci&oacute;n
 * y operaciones que pertenecen a un negocio en espec&iacute;fico, y permite modificar comportamientos de cada negocio.<br/>
 *
 * DTO_META: Atributo que almacena todas las reglas que rigen a las propiedades de un objeto de Negocio.
 * En esta secci&oacute;n se definen las reglas que condicionan a cada propiedad de este Negocio.
 * @see {@link DtoMeta}
 *
 * CREADORES_DETALLE: Atributo que alamcena objetos de tipo {@link CreadorDeEnlaceDetalle}, cada uno de estos objetos define la informaci&oacute;n
 * que se enlazar&aacute; a este negocio. <br/>
 * La relaci&oacute;n que surge con este atributo es la denominado maestro-detalle, este Negocio es el maestro u objeto for&aacute;neo
 * de cada uno de los objetos que se definen en el atributo.
 * 
 * Este arreglo tiene implementaciones del tipo {@link CreadorDeEnlaceBusquedaDinImpl} que son instanciados al momento de la creaci&oacute;n del atributo,
 * recibiendo en el constructor el atributo {@link #DTO_META} definido en este negocio y el DTO_META definido en el objeto negocio asociado,
 * conocido como  detalle, al mismo tiempo se definen los datos que enlazar&aacute;n al objeto maestro con el detalle.
 * @see {@link CreadorDeEnlaceDetalle} 
 *
 * CREADORES_BUSQUEDA: Atributo que almacena objetos de tipo {@link CreadorDeEnlaceBusqueda}, cada uno de estos objetos define la informaci&oacute;n
 * que se enlazar&aacute; con los objetos de negocio foraneos, a los cuales se les conoce como maestros.<br/>
 * La relaci&oacute;n que surge con este atributo es la denominado detalle-maestro, cada uno de los objetos definidos en este atributo son objetos
 * de Negocio maestro o foraneos.  
 *
 * Este arreglo tiene implementaciones del tipo {@link CreadorDeEnlaceBusquedaDinImpl}, que son instanciados al momento de la creaci&oacute;n del atributo,
 * recibiendo en el constructor el atributo {@link #DTO_META} definido en este negocio, y el DTO_META definido en el negocio asociado, conocido como maestro,
 * al mismo tiempo se definen las propiedades del maestro que ser&aacute;n visibles al momento de desplegar el listado de datos en modo lista de valores.
 *
 * validadorDeDatos: Atributo que almacena un objeto que contiene la informaci&oacute;n de validaci&oacute;n de datos para este Negocio.<br/>
 *
 * Al crear la instancia de la clase <code>ValidadorDeDatosSobreDtoMeta</code>, que implementa un objeto del tipo {@link ValidadorDeDatos}, 
 * permite sobreescribir cada uno de los m&eacute;todos que posee para validar datos, y de esta manera personalizar la validaci&oacute;n de las
 * propiedades del objeto de Negocio.
 *
 * creadorDeFiltro: Atributo que almacena un objeto que contiene la informaci&oacute;n del filtro aplicado.<br/>
 *
 * Al crear la instancia de la clase <code>CreadorDeFiltroSobreDtoMeta</code>, que implementa un objeto del tipo {@link CreadorDeFiltro}, 
 * permite sobreescribir cada uno de los m&eacute;todos que posee para aplicar filtro, y de esta manera personalizar la aplicaci&oacute;n del
 * filtro.
 *
 * creadorDeOrdenamiento: Atributo que almacena un objeto que contiene la informaci&oacute;n del ordenamiento aplicado.<br/>
 *
 * Al crear la instancia de la clase <code>CreadorDeOrdenamientoSobreDtoMeta</code>, que implementa un objeto del tipo {@link CreadorDeOrdenamiento}, 
 * permite sobreescribir cada uno de los m&eacute;todos que posee para aplicar ordenamiento, y de esta manera personalizar la aplicaci&oacute;n del
 * ordenamiento.
 *
 * negocioOperaciones: Atributo que almacena un objeto que contiene la informaci&oacute;n de la operaciones b&aacute;sicas para esta clase Negocio.<br/>
 *
 * Al crear la instancia de la clase <code>NegocioOperacionesBase</code>, que implementa un objeto del tipo {@link NegocioOperaciones}, 
 * permite sobreescribir cada uno de los m&eacute;todos que posee para aplicar operaciones de llenado de informaci&oacute;n. Entre las operaciones
 * b&aacute;sicas se encuentra el manejo de las colecciones de objetos detalle, objetos maestros y las reglas de negocio para cada propiedad.
 *
 * @author Systems Out of the Box
 * @version f3.1
 */

  public class HthProcesoMonitoreoNegocio extends NegocioDesacoplado {
	protected Log log = LogServiceLocator.getDefaultImplementation().getLog(HthProcesoMonitoreoNegocio.class);
	
	public static final DtoMeta DTO_META = new DtoMeta(H2hResolusor.getSingleton().getContenedorDeDtoMeta(), "hthProcesoMonitoreo",  HthProcesoMonitoreo.class, 	
			new DtoPropiedadMeta[]{
        new DtoPropiedadMeta(HthProcesoMonitoreoDao.IDPROCESOMONITOREO,"idProcesoMonitoreo",DtoPropiedadMeta.DATO_LLAVE , 10) 
     ,  new DtoPropiedadMeta(HthProcesoMonitoreoDao.CLIENTE,"cliente",DtoPropiedadMeta.DATO_BUSQUEDA, new DtoPropiedadForaneoMeta( "bsCliente","cliente","nombre") , false, 10) 
     ,  new DtoPropiedadMeta(HthProcesoMonitoreoDao.IDVISTACONVENIO,"idVistaConvenio",DtoPropiedadMeta.DATO_BUSQUEDA, new DtoPropiedadForaneoMeta( "hthVistaConvenio","idVistaConvenio","nombre") , false, 10) 
     ,  new DtoPropiedadMeta(HthProcesoMonitoreoDao.CONVENIO,"convenio",DtoPropiedadMeta.NULL , 10) 
     ,  new DtoPropiedadMeta(HthProcesoMonitoreoDao.TIPOCONVENIO,"tipoconvenio",DtoPropiedadMeta.NULL , 10) 
     ,  new DtoPropiedadMeta(HthProcesoMonitoreoDao.EMPRESAODEPTO,"empresaodepto",DtoPropiedadMeta.DATO_BUSQUEDA, new DtoPropiedadForaneoMeta( "bsEmpresaodepto","empresaodepto","nombre") , false, 10) 
     ,  new DtoPropiedadMeta(HthProcesoMonitoreoDao.CONVENIOPEACH,"convenioPEACH",DtoPropiedadMeta.NO_NULL , 5) 
     ,  new DtoPropiedadMeta(HthProcesoMonitoreoDao.CONVENIONOMBRE,"convenioNombre",DtoPropiedadMeta.NO_NULL , 25) 
     ,  new DtoPropiedadMeta(HthProcesoMonitoreoDao.NOMBREARCHIVO,"nombreArchivo",DtoPropiedadMeta.NO_NULL , 100) 
     ,  new DtoPropiedadMeta(HthProcesoMonitoreoDao.RUTASFTP,"rutaSftp",DtoPropiedadMeta.NO_NULL , 200) 
     ,  new DtoPropiedadMeta(HthProcesoMonitoreoDao.RUTAARCHIVOSIN,"rutaArchivosIn",DtoPropiedadMeta.NO_NULL , 100) 
     ,  new DtoPropiedadMeta(HthProcesoMonitoreoDao.RUTAARCHIVOSOUT,"rutaArchivosOut",DtoPropiedadMeta.NO_NULL , 100) 
     ,  new DtoPropiedadMeta(HthProcesoMonitoreoDao.RUTAPROCESADOS,"rutaProcesados",DtoPropiedadMeta.NO_NULL , 100) 
     ,  new DtoPropiedadMeta(HthProcesoMonitoreoDao.TIEMPOMONITOREO,"tiempoMonitoreo",DtoPropiedadMeta.NO_NULL , 5) 
     ,  new DtoPropiedadMeta(HthProcesoMonitoreoDao.LINEAENCABEZADO,"lineaEncabezado",DtoPropiedadMeta.NO_NULL , 1) 
     ,  new DtoPropiedadMeta(HthProcesoMonitoreoDao.IDFORMATOENCABEZADO,"idFormatoEncabezado",DtoPropiedadMeta.DATO_BUSQUEDA, new DtoPropiedadForaneoMeta( "hthFormatoEncabezado","idFormato","nombreFormato") , true, 10) 
     ,  new DtoPropiedadMeta(HthProcesoMonitoreoDao.PROCESAMIENTOSUPERUSUARIO,"procesamientoSuperusuario",DtoPropiedadMeta.NO_NULL , 1) 
     ,  new DtoPropiedadMeta(HthProcesoMonitoreoDao.IDFORMATODETALLE,"idFormatoDetalle",DtoPropiedadMeta.DATO_BUSQUEDA, new DtoPropiedadForaneoMeta( "hthFormato","idFormato","nombreFormato") , false, 10) 
     ,  new DtoPropiedadMeta(HthProcesoMonitoreoDao.MONTOMAXIMOPROCESAR,"montoMaximoProcesar",DtoPropiedadMeta.NULL , 19) 
     ,  new DtoPropiedadMeta(HthProcesoMonitoreoDao.ESTADO,"estado",DtoPropiedadMeta.NO_NULL , 1) 
     ,  new DtoPropiedadMeta(HthProcesoMonitoreoDao.USUARIOCREACION,"usuarioCreacion",DtoPropiedadMeta.AUDITORIA_USUARIO_CREAR , 100) 
     ,  new DtoPropiedadMeta(HthProcesoMonitoreoDao.FECHACREACION,"fechaCreacion",DtoPropiedadMeta.AUDITORIA_FECHA_CREAR , 23) 
     ,  new DtoPropiedadMeta(HthProcesoMonitoreoDao.USUARIOULTACT,"usuarioUltAct",DtoPropiedadMeta.AUDITORIA_USUARIO_ACTUALIZAR , 100) 
     ,  new DtoPropiedadMeta(HthProcesoMonitoreoDao.FECHAULTACT,"fechaUltAct",DtoPropiedadMeta.AUDITORIA_FECHA_ACTUALIZAR , 23) 
     ,  new DtoPropiedadMeta(HthProcesoMonitoreoDao.EMAILCLIENTE,"emailCliente",DtoPropiedadMeta.NO_NULL , 250) 
     ,  new DtoPropiedadMeta(HthProcesoMonitoreoDao.PMHOSTSFTP,"pmHostSftp",DtoPropiedadMeta.NULL , 100) 
     ,  new DtoPropiedadMeta(HthProcesoMonitoreoDao.PMPUERTOSFTP,"pmPuertoSftp",DtoPropiedadMeta.NULL , 100) 
     ,  new DtoPropiedadMeta(HthProcesoMonitoreoDao.PMUSUARIOSFTP,"pmUsuarioSftp",DtoPropiedadMeta.NULL , 100) 
     ,  new DtoPropiedadMeta(HthProcesoMonitoreoDao.PMPASSWORDSFTP,"pmPasswordSftp",DtoPropiedadMeta.NULL , 100) 
     ,  new DtoPropiedadMeta(HthProcesoMonitoreoDao.PMLLAVESFTP,"pmLlaveSftp",DtoPropiedadMeta.NULL , 200) 
	       },
    	   	DtoMeta.OPERACIONES_BUSQUEDAS_COMPLETAS	 
    );
	
	
	
	public Log getLogNegocio(){
		return log;
	}
    
    
    
    
    
    
	public static final CreadorDeEnlaceDetalle[] CREADORES_DETALLE = new CreadorDeEnlaceDetalle[]{
     };
     
     
     
	public static final CreadorDeEnlaceBusqueda[] CREADORES_BUSQUEDA = new CreadorDeEnlaceBusqueda[]{
        new CreadorDeEnlaceBusquedaDinImpl(DTO_META, BsClienteNegocio.DTO_META){
			public String[] getPropiedadesVisibles() {
                return new String[]{
                          "cliente"
                       ,  "nombre"
                       ,  "modulos"
                       ,  "direccion"
                       ,  "telefono"
                       ,  "fax"
                       ,  "email"
                       ,  "nombrecontacto"
                       ,  "numinstalacion"
                       ,  "fechaestatus"
                       ,  "estatus"
                       ,  "fechacreacion"
                       ,  "solicitudpendiente"
                       ,  "nombrerepresentante"
                       ,  "proveedorinternet"
                       ,  "banca"
                       ,  "politicacobrotex"
                       ,  "comisiontransext"
                       ,  "codigotablatransext"
                       ,  "cargapersonalizable"
                       ,  "tipodoctorepresentante"
                       ,  "doctorepresentante"
                       ,  "ciudad"
                       ,  "cargo"
                       ,  "limitexarchivo"
                       ,  "limitexlote"
                       ,  "limitextransaccion"
                       ,  "permitedebitos"
                       ,  "limitecreditos"
                       ,  "limitedebitos"
                       ,  "comisiontrnach"
                };
			}
	     }			
     ,  new CreadorDeEnlaceBusquedaDinImpl(DTO_META, HthVistaConvenioNegocio.DTO_META){
			public String[] getPropiedadesVisibles() {
                return new String[]{
                          "cliente"
                       ,  "nombre"
                       ,  "cuenta"
                       ,  "convenio"
                       ,  "tipoconvenio"
                };
			}
	     }			
     ,  new CreadorDeEnlaceBusquedaDinImpl(DTO_META, BsEmpresaodeptoNegocio.DTO_META){
			public String[] getPropiedadesVisibles() {
                return new String[]{
                          "empresaodepto"
                       ,  "cliente"
                       ,  "nombre"
                       ,  "direccion"
                       ,  "telefono"
                       ,  "fax"
                       ,  "email"
                       ,  "nombrecontacto"
                       ,  "proveedor"
                };
			}
	     }			
     ,  new CreadorDeEnlaceBusquedaDinImpl(DTO_META, HthFormatoEncabezadoNegocio.DTO_META){
			public String[] getPropiedadesVisibles() {
                return new String[]{
                          "idFormato"
                       ,  "nombreFormato"
                       ,  "tipoFormato"
                       ,  "tipoSeparador"
                       ,  "caracterSeparador"
                       ,  "usuarioCreacion"
                       ,  "fechaCreacion"
                       ,  "fechaUltAct"
                       ,  "loteDestino"
                };
			}
	     }			
     ,  new CreadorDeEnlaceBusquedaDinImpl(DTO_META, HthFormatoNegocio.DTO_META){
			public String[] getPropiedadesVisibles() {
                return new String[]{
                          "idFormato"
                       ,  "nombreFormato"
                       ,  "tipoFormato"
                       ,  "tipoSeparador"
                       ,  "caracterSeparador"
                       ,  "usuarioCreacion"
                       ,  "fechaCreacion"
                       ,  "fechaUltAct"
                       ,  "loteDestino"
                };
			}
	     }			
     };
     
	    
    /**
     * Atributo que almacena el objeto que permite acceder a la capa DAO para este negocio.
     * @see {@link Dao}
     */
    private  HthProcesoMonitoreoDao hthProcesoMonitoreoDao;
    /**
     * Devuelve el atributo que permite acceder a la capa DAO.
     * @return <code>Objeto</code> - objeto de tipo Dao.
     */
	public HthProcesoMonitoreoDao getHthProcesoMonitoreoDao() {
		if(hthProcesoMonitoreoDao == null){
			hthProcesoMonitoreoDao = (HthProcesoMonitoreoDao)getDao();
		}
		return hthProcesoMonitoreoDao;
	}    

	private ValidadorDeDatos validadorDeDatos = new ValidadorDeDatosSobreDtoMeta(DTO_META);
	
	private CreadorDeFiltro creadorDeFiltro = new CreadorDeFiltroSobreDtoMeta(DTO_META);
	
	private CreadorDeOrdenamiento creadorDeOrdenamiento = new CreadorDeOrdenamientoSobreDtoMeta(DTO_META);
	
	private NegocioOperaciones negocioOperaciones = new NegocioOperacionesBase(CREADORES_DETALLE, CREADORES_BUSQUEDA, DTO_META, H2hResolusor.getSingleton());
	
	/**
	 * Devuelve un objeto de tipo {@link NegocioOperaciones}
	 * @return <code>NegocioOperaciones</code>
	 */
	public NegocioOperaciones getNegocioOperaciones() {
		return negocioOperaciones;
	}	
	/**
	 * Devuelve un objeto de tipo {@link #ValidadorDeDatos}
	 * @return <code>ValidadorDeDatos</code>
	 */
	public ValidadorDeDatos getValidadorDeDatos() {
		return validadorDeDatos;
	}
	/**
	 * Devuelve un objeto de tipo {@link #CreadorDeFiltro}
	 * @return <code>CreadorDeFiltro</code>
	 */
	public CreadorDeFiltro getCreadorDeFiltro(){
	    return creadorDeFiltro;
	}
	/**
	 * Devuelve un objeto de tipo {@link #CreadorDeOrdenamiento}
	 * @return <code>CreadorDeOrdenamiento</code>
	 */
	public CreadorDeOrdenamiento getCreadorDeOrdenamiento(){
		return creadorDeOrdenamiento;
	}
	/**
	 * Devuelve un objeto Resolusor.
	 * @return <code>LocalizadorDeServiciosAplicacion</code>
	 */
	public LocalizadorDeServiciosAplicacion getLocalizadorDeServiciosAplicacion(){
		return H2hResolusor.getSingleton();
	}
	

	@Override
	public ProximaEjecucion doCrear(DatosAplicacion datosAplicacion)
			throws Exception {
		HthProcesoMonitoreo dto = new HthProcesoMonitoreo();
		HthParametroSistemaDao paramDao = (HthParametroSistemaDao) H2hResolusor.getSingleton().getInstancia("hthParametroSistemaDao");
		HthParametroSistema parametros = paramDao.buscarParametrosSistema();
		dto.setRutaSftp(parametros.getRutaRaiz());
		dto.setRutaArchivosIn(parametros.getCarpetaLecturaDefault());
		dto.setRutaArchivosOut(parametros.getCarpetaEscrituraDefault());
		dto.setRutaProcesados("backup");
		dto.setTiempoMonitoreo(parametros.getTiempoMonitoreoDefault());
		dto.setProcesamientoSuperusuario(HthProcesoMonitoreo.procesamiento_superusuario_NO);
		dto.setMontoMaximoProcesar(new BigDecimal(0d));
		dto.setLineaEncabezado(HthProcesoMonitoreo.linea_encabezado_NO);
		guardarDtoSiguienteCapa(datosAplicacion, dto);
		
		return super.doCrear(datosAplicacion);
	}
	
	
	@Override
	public void ambientarBusqueda(Object dto, String nombreBusqueda,
			EstadoAplicacion estadoAplicacion) throws ExcepcionEnNegocio {
		NegocioOperaciones negocioOperaciones = getNegocioOperaciones();
		CreadorDeEnlaceBusqueda cemd = negocioOperaciones.getCreadorDeEnlaceBusqueda(nombreBusqueda);
		EnlaceBusqueda enlace = cemd.crearEnlaceBusqueda(dto);
		EstadoNegocio estadoNegocio =  negocioOperaciones.getEstadoNegocio(estadoAplicacion);
		enlace.setEstadoNegocio(estadoNegocio.getEstado());
		LocalizadorDeServiciosAplicacion locApp =  negocioOperaciones.getLocalizadorDeServiciosAplicacion();
		NegocioDesacoplado negocioBusqueda = (NegocioDesacoplado)locApp.getLocMultiNegocio().getServicioONull(nombreBusqueda + "Negocio");
		 /*Saco el DTO para saber el cliente que habia seleccionado previamente...*/
    	HthProcesoMonitoreo proc = (HthProcesoMonitoreo)dto;
    	final Long cliente = proc.getCliente();
		
    	
    	
	    if("hthVistaConvenio".equals(nombreBusqueda)){
	    	if(cliente == null ){
	    		throw new ExcepcionEnNegocio("Debe seleccionar el Cliente para seleccionar cualquier otro valor ");
	    	}
	    	EstadoNegocio estadoNegocioBusqueda = negocioBusqueda.getNegocioOperaciones().getEstadoNegocio(estadoAplicacion);
	    	estadoNegocioBusqueda.agregarEnllavadorPersonalizado("clienteEnllavador", new EnllavadorDinImpl(){
				@Override
				public void applicarValoresEnPeticionDeDatos(PeticionDeDatos dataRequest) {
					dataRequest.agregarRestriccion(PeticionDeDatos.IGUAL, HthVistaConvenioDao.CLIENTE, cliente);
				}
			});
	    }else if("bsEmpresaodepto".equals(nombreBusqueda)){
	    	if(cliente == null ){
	    		throw new ExcepcionEnNegocio("Debe seleccionar el Cliente para seleccionar cualquier otro valor ");
	    	}
	    	 /*Aqui aplicare el enllavador para el negocio */
	    	EstadoNegocio estadoNegocioBusqueda = negocioBusqueda.getNegocioOperaciones().getEstadoNegocio(estadoAplicacion);
	    	estadoNegocioBusqueda.agregarEnllavadorPersonalizado("clienteEnllavadorEmpresaDepto", new EnllavadorDinImpl(){
				@Override
				public void applicarValoresEnPeticionDeDatos(PeticionDeDatos dataRequest) {
					dataRequest.agregarRestriccion(PeticionDeDatos.IGUAL, BsEmpresaodeptoDao.CLIENTE, cliente);
				}
			});
	    }
	    
	    estadoNegocio.putValorSiguienteCapa(negocioOperaciones.getEspacioDeNombre() + "EnlaceBusquedaAsociado", enlace.getNombreBusqueda());	    
	    estadoNegocio.putValorSiguienteCapa(negocioOperaciones.getEspacioDeNombre() + "DtoCabezaAsociado", dto);
	    negocioBusqueda.aplicarEnlaceBusqueda(enlace, estadoAplicacion);
	}
	
	
	@Override
	public void aplicarRetornoEnlaceBusqueda(EnlaceBusqueda enlaceBusqueda,
			EstadoAplicacion estadoAplicacion) {
		NegocioOperaciones negocioOperaciones = getNegocioOperaciones();
		String nombreBusqueda = enlaceBusqueda.getNombreBusqueda();
		CreadorDeEnlaceBusqueda cemd = negocioOperaciones.getCreadorDeEnlaceBusqueda(nombreBusqueda);
		cemd.operacionesPostBusqueda(enlaceBusqueda);
		
		if("hthVistaConvenio".equals(nombreBusqueda)){
			HthProcesoMonitoreo dtoAsociado = (HthProcesoMonitoreo) enlaceBusqueda.getDtoAsociado();
			HthVistaConvenio dtoBuscado = (HthVistaConvenio) enlaceBusqueda.getDtoBuscado();
			dtoAsociado.setConvenio(dtoBuscado.getConvenio());
			dtoAsociado.setTipoconvenio(dtoBuscado.getTipoconvenio());
			/* Esta logica de negocios es medio extrania, pero tiene que ver con la decision
			 de Crear la vista 'HthVistaConvenio' el cual tiene la union de los convenios
			 del cliente, los cuales pueden ser de Pago Electronico o ACH
			 Para la vista se creo un 'ID' artificial, de los cuales los convenios PE terminan
			 en '1' y los convenios ACH terminan en '2'.  Asi que se valida en que finaliza
			 el id de 'HthVistaConvenio' seleccionado y en base a eso, se limita la seleccion
			 del Combo del campo 'ConvenioPEACH'*/
			if(dtoBuscado.getIdVistaConvenio()%2 == 0){
				dtoAsociado.setConvenioPEACH(HthProcesoMonitoreo.convenio_PE_ACH_ACH);
			}else{
				dtoAsociado.setConvenioPEACH(HthProcesoMonitoreo.convenio_PE_ACH_PE);
			}
		}
	}
	
	public ProximaEjecucion doActivarDesactivarProceso(DatosAplicacion datosAplicacion) throws Exception{
		EstadoAplicacion estadoAplicacion = datosAplicacion.getEstadoAplicacion();
		NegocioOperaciones negocioOperaciones = getNegocioOperaciones();
		EstadoNegocio estadoNegocio = negocioOperaciones.getEstadoNegocio(estadoAplicacion);
		CreadorDeMensajes cmv = getCreadorDeMensajes(datosAplicacion);
		AmbienteDeSeguridad seg = getAmbienteDeSeguridad(datosAplicacion);
		HthProcesoMonitoreoDao dao = (HthProcesoMonitoreoDao) getDao();
		
		Object dtoId = validadorDeDatos.leerDtoId(datosAplicacion, cmv, ValidadorDeDatos.SIN_ENLLAVADOR, ValidadorDeDatos.SIN_ESPACIO_NOMBRE);
		HthProcesoMonitoreo dto =  null;
		try{
			dto = (HthProcesoMonitoreo) cargarDtoParaActualizar(dtoId, seg);
			if(esEditable(dto, cmv)){
				estadoNegocio.setCache(dto);
				guardarDtoSiguienteCapa(datosAplicacion, dto);
			}
		}catch(ExcepcionEnNegocio eneg){
			cmv.agregarExcepcionEnNegocio(eneg);			
		}
		
		dto.setFechaUltAct(new Date());
		dto.setUsuarioUltAct(seg.getUsuarioAuditoria());
		
		String nvoEstado = HthProcesoMonitoreo.estado_ACTIVADO.equals(dto.getEstado()) ?
				HthProcesoMonitoreo.estado_DESACTIVADO :
				HthProcesoMonitoreo.estado_ACTIVADO;
		
		log.debug("Actualizando estado del proceso hacia : " + nvoEstado);
		dto.setEstado(nvoEstado);
		dao.actualizar(dto);
			
		if(nvoEstado.equals(HthProcesoMonitoreo.estado_ACTIVADO)){
			cmv.agregarMensajeSatisfactorio("Proceso activado correctamente.  La ejecucion de este proceso ocurrira en la siguiente iteracion del Proceso Principal. ");
		}else{
			PoolProcessManager pm = (PoolProcessManager) H2hResolusor.getSingleton().getInstancia("poolProcessManager");
			try{
				pm.desactivarProceso(dto.getIdProcesoMonitoreo().intValue());
				cmv.agregarMensajeSatisfactorio("El proceso seleccionado ha sido inhabilitado.  No se iniciara su ejecucion mientras se encuentre DESACTIVADO");
			}catch(Exception e){
				log.error("Error al desactivar proceso: " + e.getMessage(), e);
				cmv.agregarMensajeDeError("Error al tratar de desactivar el proceso del calendarizador de procesos. " + e.getMessage());
			}
		}
		
		return doListado(datosAplicacion);
	}
	
	
	public ProximaEjecucion doControlMonitoreo(DatosAplicacion datosAplicacion) throws Exception {
		return proximaEjecucion("control", datosAplicacion);
	}
	
	public ProximaEjecucion doDetenerProcesos(DatosAplicacion datosAplicacion) throws Exception {
		CreadorDeMensajes cmv = getCreadorDeMensajes(datosAplicacion);
		try {
			
			PoolProcessManager.getInstance().stopAll();
			cmv.agregarMensajeSatisfactorio("Se han detenido todos los procesos en ejecucion.");
		} catch (SchedulerException e) {
			cmv.agregarMensajeDeError("Ha ocurrido un error al tratar de detener los proceso: " + e.getMessage());
			e.printStackTrace();
		}
		return doControlMonitoreo(datosAplicacion);
	}
	
	 public ProximaEjecucion doReiniciarProcesos(DatosAplicacion datosAplicacion) throws Exception {
		CreadorDeMensajes cmv = getCreadorDeMensajes(datosAplicacion);
		PoolProcessManager.getInstance().startMonitor();
		cmv.agregarMensajeSatisfactorio("Se ha reiniciado el proceso principal encargado de inicializar los procesos de monitoreo de archivos");
				
		return doControlMonitoreo(datosAplicacion);
	}

	public ProximaEjecucion doEstadoProcesos(DatosAplicacion datosAplicacion) throws Exception{
		
		PoolProcessManager pm = (PoolProcessManager) H2hResolusor.getSingleton().getInstancia("poolProcessManager");
		Scheduler scheduler = pm.getScheduler();
		SchedulerMetaData metaData = scheduler.getMetaData();
		
		EstadoMonitor estado = new EstadoMonitor();
		if(metaData.isStarted()){
			estado.setEstadoCalendarizador("INICIADO");
		}else{
			estado.setEstadoCalendarizador("NO INICIADO");
		}
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy hh:mm a");
		
		Trigger trigger = scheduler.getTrigger(new TriggerKey(Vars.MAIN_MONITOR_TRIGGER_NAME, null));
		if(trigger != null){
			Date start = trigger.getStartTime();
			Date stop = trigger.getEndTime();
			estado.setHoraFin( sdf.format(stop));
			estado.setHoraInicio( sdf.format(start) );
			estado.setEstado("ACTIVO");
		}else{
			estado.setHoraFin("N/A");
			estado.setHoraInicio( "N/A");
			estado.setEstado("INACTIVO");
		}
		

		int procesos = 0;
		for (String jobGroup : scheduler.getJobGroupNames()) {
			if (!Scheduler.DEFAULT_GROUP.equals(jobGroup)) {
				Set<JobKey> names = scheduler.getJobKeys(GroupMatcher.jobGroupEquals(jobGroup));
				procesos += names.size();
			}
		}
		estado.setProcesos(procesos);
		
		Date runningSince = metaData.getRunningSince();
		estado.setCalendarizadorIniciado(sdf.format(runningSince));
		
		
		
		String json = getJson(estado);
		datosAplicacion.setDatoPeticion(JSON_DATA_VAR, json);
		return proximaEjecucionGlobal(JSON_GLOBAL_PAGE, datosAplicacion);
	}


}
