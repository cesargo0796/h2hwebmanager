package com.ofbox.davivienda.h2h.abstraccion.dto;
import java.lang.Long;
import java.io.Serializable;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.validacion.UtileriaDeTiposDeDatos;
/**
 * Clase POJO(Plain Old Java Object), que permite almacenar los valores de las propiedades de un objeto de negocio, tambi&eacute;n conocido como DTO(Data Transfer Object).
 * Este DTO es el que viaja entre la capa DAO y la capa de negocios, y que posteriormente es manipulado en la capa de la vista.  
 * 
 * @author Systems Out of the Box
 * @version f3.1 --
 */
 
public class BsConvenioach implements Serializable{
	/**
     * Atributo que almacena el valor que funciona como llave para un objeto de Negocio.
     */
     private Long convenio;
    /**
     * Devuelve el objeto que almacena el identificador del DTO.
     * @return
     */
     public Long getDtoId(){
        return convenio;
     }
    /**
     * Coloca el objeto que almacena el identificador del DTO.
     * @param dtoId objeto que almacena la llave que identifica al DTO. 
     */
     public void setDtoId(Long dtoId){
     	this.convenio = dtoId;
     }
    
    /** Atributos que almacenan los valores para las propiedades de un objeto DTO correspondiente a un objeto Negocio. */
     
     private Long tipoconvenio;
	 
     private String nombre;
	 
     private String cuenta;
	 
     private String nombrecta;
	 
     private Long tipomoneda;
	 
     private String descripcion;
	 
     private Long polparticipante;
	 
     private Long estatus;
	 
     private java.util.Date fechaestatus;
	 
     private Long autorizacionweb;
	 
     private Long clasificacion;
	 
     private java.math.BigDecimal comisionxoperacionpropiaok;
	 
     private java.math.BigDecimal comisionxoperacionpropiaerror;
	 
     private java.math.BigDecimal comisionxoperacionnopropiaok;
	 
     private java.math.BigDecimal comisionxoperacionnopropiaerror;
	 
     private String multiplescuentas;
	 
     private String activo;
	 
     private String bitborrado;
	 

    /**
     * Atributos que almacenan los DTOs de objetos de negocios que funcionan como objetos foraneos.
     */    		
    private BsCliente bsCliente;


     
    
    /** M&eacute;todos que devuelven y colocan los valores de los atributos respectivos del DTO. */
     
     public Long getConvenio(){
		return this.convenio;         
     }

     public void setConvenio( Long convenio){
		this.convenio = convenio;
     }
     public Long getTipoconvenio(){
		return this.tipoconvenio;         
     }

     public void setTipoconvenio( Long tipoconvenio){
		this.tipoconvenio = tipoconvenio;
     }
     public String getNombre(){
		return this.nombre;         
     }

     public void setNombre( String nombre){
		this.nombre = nombre;
     }
     public String getCuenta(){
		return this.cuenta;         
     }

     public void setCuenta( String cuenta){
		this.cuenta = cuenta;
     }
     public String getNombrecta(){
		return this.nombrecta;         
     }

     public void setNombrecta( String nombrecta){
		this.nombrecta = nombrecta;
     }
     public Long getTipomoneda(){
		return this.tipomoneda;         
     }

     public void setTipomoneda( Long tipomoneda){
		this.tipomoneda = tipomoneda;
     }
     public String getDescripcion(){
		return this.descripcion;         
     }

     public void setDescripcion( String descripcion){
		this.descripcion = descripcion;
     }
     public Long getPolparticipante(){
		return this.polparticipante;         
     }

     public void setPolparticipante( Long polparticipante){
		this.polparticipante = polparticipante;
     }
     public Long getEstatus(){
		return this.estatus;         
     }

     public void setEstatus( Long estatus){
		this.estatus = estatus;
     }
     public java.util.Date getFechaestatus(){
		return this.fechaestatus;         
     }

     public void setFechaestatus( java.util.Date fechaestatus){
		this.fechaestatus = fechaestatus;
     }
     public Long getAutorizacionweb(){
		return this.autorizacionweb;         
     }

     public void setAutorizacionweb( Long autorizacionweb){
		this.autorizacionweb = autorizacionweb;
     }
     public Long getClasificacion(){
		return this.clasificacion;         
     }

     public void setClasificacion( Long clasificacion){
		this.clasificacion = clasificacion;
     }
     public java.math.BigDecimal getComisionxoperacionpropiaok(){
		return this.comisionxoperacionpropiaok;         
     }

     public void setComisionxoperacionpropiaok( java.math.BigDecimal comisionxoperacionpropiaok){
		this.comisionxoperacionpropiaok = comisionxoperacionpropiaok;
     }
     public java.math.BigDecimal getComisionxoperacionpropiaerror(){
		return this.comisionxoperacionpropiaerror;         
     }

     public void setComisionxoperacionpropiaerror( java.math.BigDecimal comisionxoperacionpropiaerror){
		this.comisionxoperacionpropiaerror = comisionxoperacionpropiaerror;
     }
     public java.math.BigDecimal getComisionxoperacionnopropiaok(){
		return this.comisionxoperacionnopropiaok;         
     }

     public void setComisionxoperacionnopropiaok( java.math.BigDecimal comisionxoperacionnopropiaok){
		this.comisionxoperacionnopropiaok = comisionxoperacionnopropiaok;
     }
     public java.math.BigDecimal getComisionxoperacionnopropiaerror(){
		return this.comisionxoperacionnopropiaerror;         
     }

     public void setComisionxoperacionnopropiaerror( java.math.BigDecimal comisionxoperacionnopropiaerror){
		this.comisionxoperacionnopropiaerror = comisionxoperacionnopropiaerror;
     }
     public String getMultiplescuentas(){
		return this.multiplescuentas;         
     }

     public void setMultiplescuentas( String multiplescuentas){
		this.multiplescuentas = multiplescuentas;
     }
     public String getActivo(){
		return this.activo;         
     }

     public void setActivo( String activo){
		this.activo = activo;
     }
     public String getBitborrado(){
		return this.bitborrado;         
     }

     public void setBitborrado( String bitborrado){
		this.bitborrado = bitborrado;
     }

	/**   Metodos que manejan los identificadores de los DTO foraneos */ 
    /**
     * Devuelve el atributo llave del DTO foraneo.
     * Verifica si es null antes de devolverlo.
     * @return llave del DTO del negocio maestro
     */
	public Long getCliente(){
		if(bsCliente!=null){
        	return bsCliente.getCliente();
		}
		return null;        	         
	}
    /**
     * Coloca el valor de la llave en el atributo espec&iacute;fico del DTO foraneo,
	 * crea una nueva instancia del objeto foraneo si este es nulo.
     * @param id Objeto que almacena la llave de un DTO que referencia a un negocio maestro.
     */
    public void setCliente( Long cliente){
		if(bsCliente != null && UtileriaDeTiposDeDatos.isEquals(cliente, bsCliente.getCliente()))
    		return;
			
		if(bsCliente==null || bsCliente.getCliente()!=null){
			bsCliente = new BsCliente();			
		}         
		bsCliente.setCliente(cliente);   
    }

    /** Metodos que manejan los DTO foraneos */	
    /**
     * Devuelve el objeto del DTO foraneo, que es la referencia a un objeto que representa un negocio maestro.
	 * @return
     */
    public BsCliente getBsCliente(){
		if(bsCliente==null){
			bsCliente = new BsCliente();			
		}
        return bsCliente;
    }
    /**
     * Coloca el objeto del DTO foraneo, que es la referencia a un objeto que representa un negocio maestro.
	 * @param DTO objeto foraneo
     */
    public void setBsCliente(BsCliente bsCliente){
        this.bsCliente = bsCliente;
    }

    public boolean equals(Object obj){
        return equals(obj,true);
    }
    
    
    public boolean equals(Object obj, boolean conForaneos) {
    	   if (obj == null) { return false; }
    	   if (obj == this) { return true; }
    	   if (obj.getClass() != getClass()) {
    	     return false;
    	   }
    	   BsConvenioach otherBsConvenioach = (BsConvenioach) obj;
    	   EqualsBuilder eq = new EqualsBuilder();
           eq.append(tipoconvenio, otherBsConvenioach.tipoconvenio);
           eq.append(nombre, otherBsConvenioach.nombre);
           eq.append(cuenta, otherBsConvenioach.cuenta);
           eq.append(nombrecta, otherBsConvenioach.nombrecta);
           eq.append(tipomoneda, otherBsConvenioach.tipomoneda);
           eq.append(descripcion, otherBsConvenioach.descripcion);
           eq.append(polparticipante, otherBsConvenioach.polparticipante);
           eq.append(estatus, otherBsConvenioach.estatus);
           eq.append(fechaestatus, otherBsConvenioach.fechaestatus);
           eq.append(autorizacionweb, otherBsConvenioach.autorizacionweb);
           eq.append(clasificacion, otherBsConvenioach.clasificacion);
           eq.append(comisionxoperacionpropiaok, otherBsConvenioach.comisionxoperacionpropiaok);
           eq.append(comisionxoperacionpropiaerror, otherBsConvenioach.comisionxoperacionpropiaerror);
           eq.append(comisionxoperacionnopropiaok, otherBsConvenioach.comisionxoperacionnopropiaok);
           eq.append(comisionxoperacionnopropiaerror, otherBsConvenioach.comisionxoperacionnopropiaerror);
           eq.append(multiplescuentas, otherBsConvenioach.multiplescuentas);
           eq.append(activo, otherBsConvenioach.activo);
           eq.append(bitborrado, otherBsConvenioach.bitborrado);
           eq.append(convenio, otherBsConvenioach.convenio);
           if(conForaneos){
               eq.append(bsCliente, otherBsConvenioach.bsCliente);
           }
    	   return eq.isEquals();
    }
    
    
     public int hashCode() {
	     return new HashCodeBuilder()
                  .append(tipoconvenio)
                  .append(nombre)
                  .append(cuenta)
                  .append(nombrecta)
                  .append(tipomoneda)
                  .append(descripcion)
                  .append(polparticipante)
                  .append(estatus)
                  .append(fechaestatus)
                  .append(autorizacionweb)
                  .append(clasificacion)
                  .append(comisionxoperacionpropiaok)
                  .append(comisionxoperacionpropiaerror)
                  .append(comisionxoperacionnopropiaok)
                  .append(comisionxoperacionnopropiaerror)
                  .append(multiplescuentas)
                  .append(activo)
                  .append(bitborrado)
                  .append(convenio)
                  .append(bsCliente)
	              .toHashCode();
   }
    
    
   public String toString() {
     return new ToStringBuilder(this)
                  .append("tipoconvenio",tipoconvenio)
                  .append("nombre",nombre)
                  .append("cuenta",cuenta)
                  .append("nombrecta",nombrecta)
                  .append("tipomoneda",tipomoneda)
                  .append("descripcion",descripcion)
                  .append("polparticipante",polparticipante)
                  .append("estatus",estatus)
                  .append("fechaestatus",fechaestatus)
                  .append("autorizacionweb",autorizacionweb)
                  .append("clasificacion",clasificacion)
                  .append("comisionxoperacionpropiaok",comisionxoperacionpropiaok)
                  .append("comisionxoperacionpropiaerror",comisionxoperacionpropiaerror)
                  .append("comisionxoperacionnopropiaok",comisionxoperacionnopropiaok)
                  .append("comisionxoperacionnopropiaerror",comisionxoperacionnopropiaerror)
                  .append("multiplescuentas",multiplescuentas)
                  .append("activo",activo)
                  .append("bitborrado",bitborrado)
                  .append("convenio",convenio)
                  .append("bsCliente", bsCliente)
                  .toString();
   }    
    
    
}
