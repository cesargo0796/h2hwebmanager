package com.ofbox.davivienda.h2h.abstraccion.dto;
import com.ofbox.davivienda.h2h.abstraccion.dto.BsConvenioID;
import java.io.Serializable;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
/**
 * Clase POJO(Plain Old Java Object), que permite almacenar los valores de las propiedades de un objeto de negocio, tambi&eacute;n conocido como DTO(Data Transfer Object).
 * Este DTO es el que viaja entre la capa DAO y la capa de negocios, y que posteriormente es manipulado en la capa de la vista.  
 * 
 * @author Systems Out of the Box
 * @version f3.1 --
 */
 
public class BsConvenio implements Serializable{
    /**
     * Atributo que almacena los valores que funciona como llaves para un objeto de Negocio.
     */
     private BsConvenioID dtoId;
    /**
     * Devuelve el objeto que almacena los identificadores del DTO.
     * @return
     */
     public BsConvenioID getDtoId(){     
     	if(dtoId == null){
     		dtoId = new BsConvenioID();
     	}     	
     	return dtoId;        
     }
    /**
     * Coloca el objeto que almacena los identificadores del DTO.
     * @param dtoId objeto que almacena las llaves que identifican al DTO. 
     */
     public void setDtoId(BsConvenioID dtoId){
     	this.dtoId = dtoId;
     }
    
    /** Atributos que almacenan los valores para las propiedades de un objeto DTO correspondiente a un objeto Negocio. */
     
     private Long cliente;
	 
     private String nombre;
	 
     private String cuenta;
	 
     private String nombrecta;
	 
     private Long tipomoneda;
	 
     private Long categoria;
	 
     private String descripcion;
	 
     private Long diaaplicacion;
	 
     private Long numreintentos;
	 
     private Long polparticipante;
	 
     private Long poldianoexistente;
	 
     private Long estatus;
	 
     private java.util.Date fechaestatus;
	 
     private Long intervaloreintento;
	 
     private Long polmontofijo;
	 
     private java.math.BigDecimal montofijo;
	 
     private Long intervaloaplica;
	 
     private Long creditosparciales;
	 
     private Long autorizacionweb;
	 
     private Long clasificacion;
	 
     private java.math.BigDecimal comisionxoperacion;
	 
     private java.math.BigDecimal comisionxoperacionpp;
	 
     private String multiplescuentas;
	 



     
    
    /** M&eacute;todos que devuelven y colocan los valores de los atributos respectivos del DTO. */
     
     public Long getTipoconvenio(){
		return getDtoId().getTipoconvenio();
     }

     public void setTipoconvenio( Long tipoconvenio){
		getDtoId().setTipoconvenio(tipoconvenio);             	
     }
     public Long getConvenio(){
		return getDtoId().getConvenio();
     }

     public void setConvenio( Long convenio){
		getDtoId().setConvenio(convenio);             	
     }
     public Long getCliente(){
		return this.cliente;         
     }

     public void setCliente( Long cliente){
		this.cliente = cliente;
     }
     public String getNombre(){
		return this.nombre;         
     }

     public void setNombre( String nombre){
		this.nombre = nombre;
     }
     public String getCuenta(){
		return this.cuenta;         
     }

     public void setCuenta( String cuenta){
		this.cuenta = cuenta;
     }
     public String getNombrecta(){
		return this.nombrecta;         
     }

     public void setNombrecta( String nombrecta){
		this.nombrecta = nombrecta;
     }
     public Long getTipomoneda(){
		return this.tipomoneda;         
     }

     public void setTipomoneda( Long tipomoneda){
		this.tipomoneda = tipomoneda;
     }
     public Long getCategoria(){
		return this.categoria;         
     }

     public void setCategoria( Long categoria){
		this.categoria = categoria;
     }
     public String getDescripcion(){
		return this.descripcion;         
     }

     public void setDescripcion( String descripcion){
		this.descripcion = descripcion;
     }
     public Long getDiaaplicacion(){
		return this.diaaplicacion;         
     }

     public void setDiaaplicacion( Long diaaplicacion){
		this.diaaplicacion = diaaplicacion;
     }
     public Long getNumreintentos(){
		return this.numreintentos;         
     }

     public void setNumreintentos( Long numreintentos){
		this.numreintentos = numreintentos;
     }
     public Long getPolparticipante(){
		return this.polparticipante;         
     }

     public void setPolparticipante( Long polparticipante){
		this.polparticipante = polparticipante;
     }
     public Long getPoldianoexistente(){
		return this.poldianoexistente;         
     }

     public void setPoldianoexistente( Long poldianoexistente){
		this.poldianoexistente = poldianoexistente;
     }
     public Long getEstatus(){
		return this.estatus;         
     }

     public void setEstatus( Long estatus){
		this.estatus = estatus;
     }
     public java.util.Date getFechaestatus(){
		return this.fechaestatus;         
     }

     public void setFechaestatus( java.util.Date fechaestatus){
		this.fechaestatus = fechaestatus;
     }
     public Long getIntervaloreintento(){
		return this.intervaloreintento;         
     }

     public void setIntervaloreintento( Long intervaloreintento){
		this.intervaloreintento = intervaloreintento;
     }
     public Long getPolmontofijo(){
		return this.polmontofijo;         
     }

     public void setPolmontofijo( Long polmontofijo){
		this.polmontofijo = polmontofijo;
     }
     public java.math.BigDecimal getMontofijo(){
		return this.montofijo;         
     }

     public void setMontofijo( java.math.BigDecimal montofijo){
		this.montofijo = montofijo;
     }
     public Long getIntervaloaplica(){
		return this.intervaloaplica;         
     }

     public void setIntervaloaplica( Long intervaloaplica){
		this.intervaloaplica = intervaloaplica;
     }
     public Long getCreditosparciales(){
		return this.creditosparciales;         
     }

     public void setCreditosparciales( Long creditosparciales){
		this.creditosparciales = creditosparciales;
     }
     public Long getAutorizacionweb(){
		return this.autorizacionweb;         
     }

     public void setAutorizacionweb( Long autorizacionweb){
		this.autorizacionweb = autorizacionweb;
     }
     public Long getClasificacion(){
		return this.clasificacion;         
     }

     public void setClasificacion( Long clasificacion){
		this.clasificacion = clasificacion;
     }
     public java.math.BigDecimal getComisionxoperacion(){
		return this.comisionxoperacion;         
     }

     public void setComisionxoperacion( java.math.BigDecimal comisionxoperacion){
		this.comisionxoperacion = comisionxoperacion;
     }
     public java.math.BigDecimal getComisionxoperacionpp(){
		return this.comisionxoperacionpp;         
     }

     public void setComisionxoperacionpp( java.math.BigDecimal comisionxoperacionpp){
		this.comisionxoperacionpp = comisionxoperacionpp;
     }
     public String getMultiplescuentas(){
		return this.multiplescuentas;         
     }

     public void setMultiplescuentas( String multiplescuentas){
		this.multiplescuentas = multiplescuentas;
     }



    public boolean equals(Object obj){
        return equals(obj,true);
    }
    
    
    public boolean equals(Object obj, boolean conForaneos) {
    	   if (obj == null) { return false; }
    	   if (obj == this) { return true; }
    	   if (obj.getClass() != getClass()) {
    	     return false;
    	   }
    	   BsConvenio otherBsConvenio = (BsConvenio) obj;
    	   EqualsBuilder eq = new EqualsBuilder();
           eq.append(cliente, otherBsConvenio.cliente);
           eq.append(nombre, otherBsConvenio.nombre);
           eq.append(cuenta, otherBsConvenio.cuenta);
           eq.append(nombrecta, otherBsConvenio.nombrecta);
           eq.append(tipomoneda, otherBsConvenio.tipomoneda);
           eq.append(categoria, otherBsConvenio.categoria);
           eq.append(descripcion, otherBsConvenio.descripcion);
           eq.append(diaaplicacion, otherBsConvenio.diaaplicacion);
           eq.append(numreintentos, otherBsConvenio.numreintentos);
           eq.append(polparticipante, otherBsConvenio.polparticipante);
           eq.append(poldianoexistente, otherBsConvenio.poldianoexistente);
           eq.append(estatus, otherBsConvenio.estatus);
           eq.append(fechaestatus, otherBsConvenio.fechaestatus);
           eq.append(intervaloreintento, otherBsConvenio.intervaloreintento);
           eq.append(polmontofijo, otherBsConvenio.polmontofijo);
           eq.append(montofijo, otherBsConvenio.montofijo);
           eq.append(intervaloaplica, otherBsConvenio.intervaloaplica);
           eq.append(creditosparciales, otherBsConvenio.creditosparciales);
           eq.append(autorizacionweb, otherBsConvenio.autorizacionweb);
           eq.append(clasificacion, otherBsConvenio.clasificacion);
           eq.append(comisionxoperacion, otherBsConvenio.comisionxoperacion);
           eq.append(comisionxoperacionpp, otherBsConvenio.comisionxoperacionpp);
           eq.append(multiplescuentas, otherBsConvenio.multiplescuentas);
           eq.append(dtoId, otherBsConvenio.dtoId);
    	   return eq.isEquals();
    }
    
    
     public int hashCode() {
	     return new HashCodeBuilder()
                  .append(cliente)
                  .append(nombre)
                  .append(cuenta)
                  .append(nombrecta)
                  .append(tipomoneda)
                  .append(categoria)
                  .append(descripcion)
                  .append(diaaplicacion)
                  .append(numreintentos)
                  .append(polparticipante)
                  .append(poldianoexistente)
                  .append(estatus)
                  .append(fechaestatus)
                  .append(intervaloreintento)
                  .append(polmontofijo)
                  .append(montofijo)
                  .append(intervaloaplica)
                  .append(creditosparciales)
                  .append(autorizacionweb)
                  .append(clasificacion)
                  .append(comisionxoperacion)
                  .append(comisionxoperacionpp)
                  .append(multiplescuentas)
                  .append(dtoId)
	              .toHashCode();
   }
    
    
   public String toString() {
     return new ToStringBuilder(this)
                  .append("cliente",cliente)
                  .append("nombre",nombre)
                  .append("cuenta",cuenta)
                  .append("nombrecta",nombrecta)
                  .append("tipomoneda",tipomoneda)
                  .append("categoria",categoria)
                  .append("descripcion",descripcion)
                  .append("diaaplicacion",diaaplicacion)
                  .append("numreintentos",numreintentos)
                  .append("polparticipante",polparticipante)
                  .append("poldianoexistente",poldianoexistente)
                  .append("estatus",estatus)
                  .append("fechaestatus",fechaestatus)
                  .append("intervaloreintento",intervaloreintento)
                  .append("polmontofijo",polmontofijo)
                  .append("montofijo",montofijo)
                  .append("intervaloaplica",intervaloaplica)
                  .append("creditosparciales",creditosparciales)
                  .append("autorizacionweb",autorizacionweb)
                  .append("clasificacion",clasificacion)
                  .append("comisionxoperacion",comisionxoperacion)
                  .append("comisionxoperacionpp",comisionxoperacionpp)
                  .append("multiplescuentas",multiplescuentas)
                  .append("dtoId",dtoId)
                  .toString();
   }    
    
    
}
