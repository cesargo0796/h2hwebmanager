package com.ofbox.davivienda.h2h.abstraccion.dto;
import com.ofbox.davivienda.h2h.abstraccion.dto.BsDetalleachID;
import java.io.Serializable;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
/**
 * Clase POJO(Plain Old Java Object), que permite almacenar los valores de las propiedades de un objeto de negocio, tambi&eacute;n conocido como DTO(Data Transfer Object).
 * Este DTO es el que viaja entre la capa DAO y la capa de negocios, y que posteriormente es manipulado en la capa de la vista.  
 * 
 * @author Systems Out of the Box
 * @version f3.1 --
 */
 
public class BsDetalleach implements Serializable{
    /**
     * Atributo que almacena los valores que funciona como llaves para un objeto de Negocio.
     */
     private BsDetalleachID dtoId;
    /**
     * Devuelve el objeto que almacena los identificadores del DTO.
     * @return
     */
     public BsDetalleachID getDtoId(){     
     	if(dtoId == null){
     		dtoId = new BsDetalleachID();
     	}     	
     	return dtoId;        
     }
    /**
     * Coloca el objeto que almacena los identificadores del DTO.
     * @param dtoId objeto que almacena las llaves que identifican al DTO. 
     */
     public void setDtoId(BsDetalleachID dtoId){
     	this.dtoId = dtoId;
     }
    
    /** Atributos que almacenan los valores para las propiedades de un objeto DTO correspondiente a un objeto Negocio. */
     
     private String cuenta;
	 
     private Long tipooperacion;
	 
     private java.math.BigDecimal monto;
	 
     private String adenda;
	 
     private java.util.Date fechaestatus;
	 
     private Long estatus;
	 
     private String idpago;
	 
     private Long autorizacionhost;
	 
     private String email;
	 
     private java.math.BigDecimal comisionaplicada;
	 
     private String nombrecuenta;
	 
     private String autorizador;
	 
     private String estatuspaybank;
	 
     private java.util.Date fechahorahost;
	 
     private String cuentadebito;
	 
     private String nombredecuenta;
	 
     private Long codbanco;
	 
     private Long tipocuenta;
	 
     private String descestatuspaybank;
	 
     private Long enviarahost;
	 



     
    
    /** M&eacute;todos que devuelven y colocan los valores de los atributos respectivos del DTO. */
     
     public Long getIddetalle(){
		return getDtoId().getIddetalle();
     }

     public void setIddetalle( Long iddetalle){
		getDtoId().setIddetalle(iddetalle);             	
     }
     public Long getInstalacion(){
		return getDtoId().getInstalacion();
     }

     public void setInstalacion( Long instalacion){
		getDtoId().setInstalacion(instalacion);             	
     }
     public Long getLote(){
		return getDtoId().getLote();
     }

     public void setLote( Long lote){
		getDtoId().setLote(lote);             	
     }
     public Long getOperacion(){
		return getDtoId().getOperacion();
     }

     public void setOperacion( Long operacion){
		getDtoId().setOperacion(operacion);             	
     }
     public String getCuenta(){
		return this.cuenta;         
     }

     public void setCuenta( String cuenta){
		this.cuenta = cuenta;
     }
     public Long getTipooperacion(){
		return this.tipooperacion;         
     }

     public void setTipooperacion( Long tipooperacion){
		this.tipooperacion = tipooperacion;
     }
     public java.math.BigDecimal getMonto(){
		return this.monto;         
     }

     public void setMonto( java.math.BigDecimal monto){
		this.monto = monto;
     }
     public String getAdenda(){
		return this.adenda;         
     }

     public void setAdenda( String adenda){
		this.adenda = adenda;
     }
     public java.util.Date getFechaestatus(){
		return this.fechaestatus;         
     }

     public void setFechaestatus( java.util.Date fechaestatus){
		this.fechaestatus = fechaestatus;
     }
     public Long getEstatus(){
		return this.estatus;         
     }

     public void setEstatus( Long estatus){
		this.estatus = estatus;
     }
     public String getIdpago(){
		return this.idpago;         
     }

     public void setIdpago( String idpago){
		this.idpago = idpago;
     }
     public Long getAutorizacionhost(){
		return this.autorizacionhost;         
     }

     public void setAutorizacionhost( Long autorizacionhost){
		this.autorizacionhost = autorizacionhost;
     }
     public String getEmail(){
		return this.email;         
     }

     public void setEmail( String email){
		this.email = email;
     }
     public java.math.BigDecimal getComisionaplicada(){
		return this.comisionaplicada;         
     }

     public void setComisionaplicada( java.math.BigDecimal comisionaplicada){
		this.comisionaplicada = comisionaplicada;
     }
     public String getNombrecuenta(){
		return this.nombrecuenta;         
     }

     public void setNombrecuenta( String nombrecuenta){
		this.nombrecuenta = nombrecuenta;
     }
     public String getAutorizador(){
		return this.autorizador;         
     }

     public void setAutorizador( String autorizador){
		this.autorizador = autorizador;
     }
     public String getEstatuspaybank(){
		return this.estatuspaybank;         
     }

     public void setEstatuspaybank( String estatuspaybank){
		this.estatuspaybank = estatuspaybank;
     }
     public java.util.Date getFechahorahost(){
		return this.fechahorahost;         
     }

     public void setFechahorahost( java.util.Date fechahorahost){
		this.fechahorahost = fechahorahost;
     }
     public String getCuentadebito(){
		return this.cuentadebito;         
     }

     public void setCuentadebito( String cuentadebito){
		this.cuentadebito = cuentadebito;
     }
     public String getNombredecuenta(){
		return this.nombredecuenta;         
     }

     public void setNombredecuenta( String nombredecuenta){
		this.nombredecuenta = nombredecuenta;
     }
     public Long getCodbanco(){
		return this.codbanco;         
     }

     public void setCodbanco( Long codbanco){
		this.codbanco = codbanco;
     }
     public Long getTipocuenta(){
		return this.tipocuenta;         
     }

     public void setTipocuenta( Long tipocuenta){
		this.tipocuenta = tipocuenta;
     }
     public String getDescestatuspaybank(){
		return this.descestatuspaybank;         
     }

     public void setDescestatuspaybank( String descestatuspaybank){
		this.descestatuspaybank = descestatuspaybank;
     }
     public Long getEnviarahost(){
		return this.enviarahost;         
     }

     public void setEnviarahost( Long enviarahost){
		this.enviarahost = enviarahost;
     }



    public boolean equals(Object obj){
        return equals(obj,true);
    }
    
    
    public boolean equals(Object obj, boolean conForaneos) {
    	   if (obj == null) { return false; }
    	   if (obj == this) { return true; }
    	   if (obj.getClass() != getClass()) {
    	     return false;
    	   }
    	   BsDetalleach otherBsDetalleach = (BsDetalleach) obj;
    	   EqualsBuilder eq = new EqualsBuilder();
           eq.append(cuenta, otherBsDetalleach.cuenta);
           eq.append(tipooperacion, otherBsDetalleach.tipooperacion);
           eq.append(monto, otherBsDetalleach.monto);
           eq.append(adenda, otherBsDetalleach.adenda);
           eq.append(fechaestatus, otherBsDetalleach.fechaestatus);
           eq.append(estatus, otherBsDetalleach.estatus);
           eq.append(idpago, otherBsDetalleach.idpago);
           eq.append(autorizacionhost, otherBsDetalleach.autorizacionhost);
           eq.append(email, otherBsDetalleach.email);
           eq.append(comisionaplicada, otherBsDetalleach.comisionaplicada);
           eq.append(nombrecuenta, otherBsDetalleach.nombrecuenta);
           eq.append(autorizador, otherBsDetalleach.autorizador);
           eq.append(estatuspaybank, otherBsDetalleach.estatuspaybank);
           eq.append(fechahorahost, otherBsDetalleach.fechahorahost);
           eq.append(cuentadebito, otherBsDetalleach.cuentadebito);
           eq.append(nombredecuenta, otherBsDetalleach.nombredecuenta);
           eq.append(codbanco, otherBsDetalleach.codbanco);
           eq.append(tipocuenta, otherBsDetalleach.tipocuenta);
           eq.append(descestatuspaybank, otherBsDetalleach.descestatuspaybank);
           eq.append(enviarahost, otherBsDetalleach.enviarahost);
           eq.append(dtoId, otherBsDetalleach.dtoId);
    	   return eq.isEquals();
    }
    
    
     public int hashCode() {
	     return new HashCodeBuilder()
                  .append(cuenta)
                  .append(tipooperacion)
                  .append(monto)
                  .append(adenda)
                  .append(fechaestatus)
                  .append(estatus)
                  .append(idpago)
                  .append(autorizacionhost)
                  .append(email)
                  .append(comisionaplicada)
                  .append(nombrecuenta)
                  .append(autorizador)
                  .append(estatuspaybank)
                  .append(fechahorahost)
                  .append(cuentadebito)
                  .append(nombredecuenta)
                  .append(codbanco)
                  .append(tipocuenta)
                  .append(descestatuspaybank)
                  .append(enviarahost)
                  .append(dtoId)
	              .toHashCode();
   }
    
    
   public String toString() {
     return new ToStringBuilder(this)
                  .append("cuenta",cuenta)
                  .append("tipooperacion",tipooperacion)
                  .append("monto",monto)
                  .append("adenda",adenda)
                  .append("fechaestatus",fechaestatus)
                  .append("estatus",estatus)
                  .append("idpago",idpago)
                  .append("autorizacionhost",autorizacionhost)
                  .append("email",email)
                  .append("comisionaplicada",comisionaplicada)
                  .append("nombrecuenta",nombrecuenta)
                  .append("autorizador",autorizador)
                  .append("estatuspaybank",estatuspaybank)
                  .append("fechahorahost",fechahorahost)
                  .append("cuentadebito",cuentadebito)
                  .append("nombredecuenta",nombredecuenta)
                  .append("codbanco",codbanco)
                  .append("tipocuenta",tipocuenta)
                  .append("descestatuspaybank",descestatuspaybank)
                  .append("enviarahost",enviarahost)
                  .append("dtoId",dtoId)
                  .toString();
   }    
    
    
}
