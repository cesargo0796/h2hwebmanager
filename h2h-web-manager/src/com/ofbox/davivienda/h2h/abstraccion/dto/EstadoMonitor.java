package com.ofbox.davivienda.h2h.abstraccion.dto;

public class EstadoMonitor {

	private String calendarizadorIniciado;
	
	private String estadoCalendarizador;
	
	private String estado;
	
	private String horaInicio;
	
	private String horaFin;
	
	private int procesos;

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getHoraInicio() {
		return horaInicio;
	}

	public void setHoraInicio(String horaInicio) {
		this.horaInicio = horaInicio;
	}

	public String getHoraFin() {
		return horaFin;
	}

	public void setHoraFin(String horaFin) {
		this.horaFin = horaFin;
	}

	public int getProcesos() {
		return procesos;
	}

	public void setProcesos(int procesos) {
		this.procesos = procesos;
	}

	public String getEstadoCalendarizador() {
		return estadoCalendarizador;
	}

	public void setEstadoCalendarizador(String estadoCalendarizador) {
		this.estadoCalendarizador = estadoCalendarizador;
	}

	public String getCalendarizadorIniciado() {
		return calendarizadorIniciado;
	}

	public void setCalendarizadorIniciado(String calendarizadorIniciado) {
		this.calendarizadorIniciado = calendarizadorIniciado;
	}
	
	
	
}
