package com.ofbox.davivienda.h2h.abstraccion.dto;
import java.lang.Long;
import java.io.Serializable;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
/**
 * Clase POJO(Plain Old Java Object), que permite almacenar los valores de las propiedades de un objeto de negocio, tambi&eacute;n conocido como DTO(Data Transfer Object).
 * Este DTO es el que viaja entre la capa DAO y la capa de negocios, y que posteriormente es manipulado en la capa de la vista.  
 * 
 * @author Systems Out of the Box
 * @version f3.1 --
 */
 
public class HthAtributoEncabezado implements Serializable{
	/**
     * Atributo que almacena el valor que funciona como llave para un objeto de Negocio.
     */
     private Long idAtributoEncabezado;
    /**
     * Devuelve el objeto que almacena el identificador del DTO.
     * @return
     */
     public Long getDtoId(){
        return idAtributoEncabezado;
     }
    /**
     * Coloca el objeto que almacena el identificador del DTO.
     * @param dtoId objeto que almacena la llave que identifica al DTO. 
     */
     public void setDtoId(Long dtoId){
     	this.idAtributoEncabezado = dtoId;
     }
    
    /** Atributos que almacenan los valores para las propiedades de un objeto DTO correspondiente a un objeto Negocio. */
     
     private String nombreAtributo;
	 
     private String descripcion;
	 
     private String tabla;
	 
     private String columnaDestino;
	 
     private String usuarioCreacion;
	 
     private java.util.Date fechaCreacion;
	 
     private String usuarioUltAct;
	 
     private java.util.Date fechaUltAct;
	 



     
    
    /** M&eacute;todos que devuelven y colocan los valores de los atributos respectivos del DTO. */
     
     public Long getIdAtributoEncabezado(){
		return this.idAtributoEncabezado;         
     }

     public void setIdAtributoEncabezado( Long idAtributoEncabezado){
		this.idAtributoEncabezado = idAtributoEncabezado;
     }
     public String getNombreAtributo(){
		return this.nombreAtributo;         
     }

     public void setNombreAtributo( String nombreAtributo){
		this.nombreAtributo = nombreAtributo;
     }
     public String getDescripcion(){
		return this.descripcion;         
     }

     public void setDescripcion( String descripcion){
		this.descripcion = descripcion;
     }
     public String getTabla(){
		return this.tabla;         
     }

     public void setTabla( String tabla){
		this.tabla = tabla;
     }
     public String getColumnaDestino(){
		return this.columnaDestino;         
     }

     public void setColumnaDestino( String columnaDestino){
		this.columnaDestino = columnaDestino;
     }
     public String getUsuarioCreacion(){
		return this.usuarioCreacion;         
     }

     public void setUsuarioCreacion( String usuarioCreacion){
		this.usuarioCreacion = usuarioCreacion;
     }
     public java.util.Date getFechaCreacion(){
		return this.fechaCreacion;         
     }

     public void setFechaCreacion( java.util.Date fechaCreacion){
		this.fechaCreacion = fechaCreacion;
     }
     public String getUsuarioUltAct(){
		return this.usuarioUltAct;         
     }

     public void setUsuarioUltAct( String usuarioUltAct){
		this.usuarioUltAct = usuarioUltAct;
     }
     public java.util.Date getFechaUltAct(){
		return this.fechaUltAct;         
     }

     public void setFechaUltAct( java.util.Date fechaUltAct){
		this.fechaUltAct = fechaUltAct;
     }



    public boolean equals(Object obj){
        return equals(obj,true);
    }
    
    
    public boolean equals(Object obj, boolean conForaneos) {
    	   if (obj == null) { return false; }
    	   if (obj == this) { return true; }
    	   if (obj.getClass() != getClass()) {
    	     return false;
    	   }
    	   HthAtributoEncabezado otherHthAtributoEncabezado = (HthAtributoEncabezado) obj;
    	   EqualsBuilder eq = new EqualsBuilder();
           eq.append(nombreAtributo, otherHthAtributoEncabezado.nombreAtributo);
           eq.append(descripcion, otherHthAtributoEncabezado.descripcion);
           eq.append(tabla, otherHthAtributoEncabezado.tabla);
           eq.append(columnaDestino, otherHthAtributoEncabezado.columnaDestino);
           eq.append(usuarioCreacion, otherHthAtributoEncabezado.usuarioCreacion);
           eq.append(fechaCreacion, otherHthAtributoEncabezado.fechaCreacion);
           eq.append(usuarioUltAct, otherHthAtributoEncabezado.usuarioUltAct);
           eq.append(fechaUltAct, otherHthAtributoEncabezado.fechaUltAct);
           eq.append(idAtributoEncabezado, otherHthAtributoEncabezado.idAtributoEncabezado);
    	   return eq.isEquals();
    }
    
    
     public int hashCode() {
	     return new HashCodeBuilder()
                  .append(nombreAtributo)
                  .append(descripcion)
                  .append(tabla)
                  .append(columnaDestino)
                  .append(usuarioCreacion)
                  .append(fechaCreacion)
                  .append(usuarioUltAct)
                  .append(fechaUltAct)
                  .append(idAtributoEncabezado)
	              .toHashCode();
   }
    
    
   public String toString() {
     return new ToStringBuilder(this)
                  .append("nombreAtributo",nombreAtributo)
                  .append("descripcion",descripcion)
                  .append("tabla",tabla)
                  .append("columnaDestino",columnaDestino)
                  .append("usuarioCreacion",usuarioCreacion)
                  .append("fechaCreacion",fechaCreacion)
                  .append("usuarioUltAct",usuarioUltAct)
                  .append("fechaUltAct",fechaUltAct)
                  .append("idAtributoEncabezado",idAtributoEncabezado)
                  .toString();
   }    
    
    
}
