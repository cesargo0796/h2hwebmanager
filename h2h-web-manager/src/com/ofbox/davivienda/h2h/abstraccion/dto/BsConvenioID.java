package com.ofbox.davivienda.h2h.abstraccion.dto;

import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


public class BsConvenioID implements Serializable{


     private Long tipoconvenio;
     
     private Long convenio;
     
   
     
     public Long getTipoconvenio(){
         return this.tipoconvenio;
     }

     public void setTipoconvenio( Long tipoconvenio){
          this.tipoconvenio = tipoconvenio;
     }
     
     
     public Long getConvenio(){
         return this.convenio;
     }

     public void setConvenio( Long convenio){
          this.convenio = convenio;
     }
     

   public boolean equals(Object obj, boolean conForaneos) {
    	   if (obj == null) { return false; }
    	   if (obj == this) { return true; }
    	   if (obj.getClass() != getClass()) {
    	     return false;
    	   }
    	   BsConvenioID otherBsConvenioID = (BsConvenioID) obj;
    	   EqualsBuilder eq = new EqualsBuilder();
           eq.append(tipoconvenio, otherBsConvenioID.tipoconvenio);
           eq.append(convenio, otherBsConvenioID.convenio);
    	   return eq.isEquals();
    }
    
     public int hashCode() {
	     return new HashCodeBuilder()
                  .append(tipoconvenio)
                  .append(convenio)
	              .toHashCode();
   }
    
    
   public String toString() {
     return new ToStringBuilder(this)
                  .append("tipoconvenio",tipoconvenio)
                  .append("convenio",convenio)
                  .toString();
   }
    
}
