package com.ofbox.davivienda.h2h.abstraccion.dto;

import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


public class BsDetallepeID implements Serializable{


     private Long instalacion;
     
     private Long lote;
     
     private Long operacion;
     
   
     
     public Long getInstalacion(){
         return this.instalacion;
     }

     public void setInstalacion( Long instalacion){
          this.instalacion = instalacion;
     }
     
     
     public Long getLote(){
         return this.lote;
     }

     public void setLote( Long lote){
          this.lote = lote;
     }
     
     
     public Long getOperacion(){
         return this.operacion;
     }

     public void setOperacion( Long operacion){
          this.operacion = operacion;
     }
     

   public boolean equals(Object obj, boolean conForaneos) {
    	   if (obj == null) { return false; }
    	   if (obj == this) { return true; }
    	   if (obj.getClass() != getClass()) {
    	     return false;
    	   }
    	   BsDetallepeID otherBsDetallepeID = (BsDetallepeID) obj;
    	   EqualsBuilder eq = new EqualsBuilder();
           eq.append(instalacion, otherBsDetallepeID.instalacion);
           eq.append(lote, otherBsDetallepeID.lote);
           eq.append(operacion, otherBsDetallepeID.operacion);
    	   return eq.isEquals();
    }
    
     public int hashCode() {
	     return new HashCodeBuilder()
                  .append(instalacion)
                  .append(lote)
                  .append(operacion)
	              .toHashCode();
   }
    
    
   public String toString() {
     return new ToStringBuilder(this)
                  .append("instalacion",instalacion)
                  .append("lote",lote)
                  .append("operacion",operacion)
                  .toString();
   }
    
}
