package com.ofbox.davivienda.h2h.abstraccion.dto;
import java.lang.Long;
import java.io.Serializable;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.validacion.UtileriaDeTiposDeDatos;
/**
 * Clase POJO(Plain Old Java Object), que permite almacenar los valores de las propiedades de un objeto de negocio, tambi&eacute;n conocido como DTO(Data Transfer Object).
 * Este DTO es el que viaja entre la capa DAO y la capa de negocios, y que posteriormente es manipulado en la capa de la vista.  
 * 
 * @author Systems Out of the Box
 * @version f3.1 --
 */
 
public class HthAtributoFormato implements Serializable{
public static final String tipo_dato_atributo_TEXTO = "T";
public static final String tipo_dato_atributo_ENTERO = "E";
public static final String tipo_dato_atributo_DECIMAL = "D";
public static final String tipo_dato_atributo_FECHA = "F";
public static final String opcional_SI = "S";
public static final String opcional_NO = "N";
	/**
     * Atributo que almacena el valor que funciona como llave para un objeto de Negocio.
     */
     private Long idAtributoFormato;
    /**
     * Devuelve el objeto que almacena el identificador del DTO.
     * @return
     */
     public Long getDtoId(){
        return idAtributoFormato;
     }
    /**
     * Coloca el objeto que almacena el identificador del DTO.
     * @param dtoId objeto que almacena la llave que identifica al DTO. 
     */
     public void setDtoId(Long dtoId){
     	this.idAtributoFormato = dtoId;
     }
    
    /** Atributos que almacenan los valores para las propiedades de un objeto DTO correspondiente a un objeto Negocio. */
     
     private Long posicion;
	 
     private Long posicionIni;
	 
     private Long posicionFin;
	 
     private String tipoDatoAtributo;
	 
     private String formatoAtributo;
	 
     private String opcional;
	 
     private String usuarioCreacion;
	 
     private java.util.Date fechaCreacin;
	 
     private String usuarioUltAct;
	 
     private java.util.Date fechaUltAct;
	 

    /**
     * Atributos que almacenan los DTOs de objetos de negocios que funcionan como objetos foraneos.
     */    		
    private HthAtributoEncabezado hthAtributoEncabezado;
    private HthAtributoDetalle hthAtributoDetalle;
    private HthFormato hthFormato;


     
    
    /** M&eacute;todos que devuelven y colocan los valores de los atributos respectivos del DTO. */
     
     public Long getIdAtributoFormato(){
		return this.idAtributoFormato;         
     }

     public void setIdAtributoFormato( Long idAtributoFormato){
		this.idAtributoFormato = idAtributoFormato;
     }
     public Long getPosicion(){
		return this.posicion;         
     }

     public void setPosicion( Long posicion){
		this.posicion = posicion;
     }
     public Long getPosicionIni(){
		return this.posicionIni;         
     }

     public void setPosicionIni( Long posicionIni){
		this.posicionIni = posicionIni;
     }
     public Long getPosicionFin(){
		return this.posicionFin;         
     }

     public void setPosicionFin( Long posicionFin){
		this.posicionFin = posicionFin;
     }
     public String getTipoDatoAtributo(){
		return this.tipoDatoAtributo;         
     }

     public void setTipoDatoAtributo( String tipoDatoAtributo){
		this.tipoDatoAtributo = tipoDatoAtributo;
     }
     public String getFormatoAtributo(){
		return this.formatoAtributo;         
     }

     public void setFormatoAtributo( String formatoAtributo){
		this.formatoAtributo = formatoAtributo;
     }
     public String getOpcional(){
		return this.opcional;         
     }

     public void setOpcional( String opcional){
		this.opcional = opcional;
     }
     public String getUsuarioCreacion(){
		return this.usuarioCreacion;         
     }

     public void setUsuarioCreacion( String usuarioCreacion){
		this.usuarioCreacion = usuarioCreacion;
     }
     public java.util.Date getFechaCreacin(){
		return this.fechaCreacin;         
     }

     public void setFechaCreacin( java.util.Date fechaCreacin){
		this.fechaCreacin = fechaCreacin;
     }
     public String getUsuarioUltAct(){
		return this.usuarioUltAct;         
     }

     public void setUsuarioUltAct( String usuarioUltAct){
		this.usuarioUltAct = usuarioUltAct;
     }
     public java.util.Date getFechaUltAct(){
		return this.fechaUltAct;         
     }

     public void setFechaUltAct( java.util.Date fechaUltAct){
		this.fechaUltAct = fechaUltAct;
     }

	/**   Metodos que manejan los identificadores de los DTO foraneos */ 
    /**
     * Devuelve el atributo llave del DTO foraneo.
     * Verifica si es null antes de devolverlo.
     * @return llave del DTO del negocio maestro
     */
	public Long getIdAtributoEncabezado(){
		if(hthAtributoEncabezado!=null){
        	return hthAtributoEncabezado.getIdAtributoEncabezado();
		}
		return null;        	         
	}
    /**
     * Coloca el valor de la llave en el atributo espec&iacute;fico del DTO foraneo,
	 * crea una nueva instancia del objeto foraneo si este es nulo.
     * @param id Objeto que almacena la llave de un DTO que referencia a un negocio maestro.
     */
    public void setIdAtributoEncabezado( Long idAtributoEncabezado){
		if(hthAtributoEncabezado != null && UtileriaDeTiposDeDatos.isEquals(idAtributoEncabezado, hthAtributoEncabezado.getIdAtributoEncabezado()))
    		return;
			
		if(hthAtributoEncabezado==null || hthAtributoEncabezado.getIdAtributoEncabezado()!=null){
			hthAtributoEncabezado = new HthAtributoEncabezado();			
		}         
		hthAtributoEncabezado.setIdAtributoEncabezado(idAtributoEncabezado);   
    }
    /**
     * Devuelve el atributo llave del DTO foraneo.
     * Verifica si es null antes de devolverlo.
     * @return llave del DTO del negocio maestro
     */
	public Long getIdAtributoDetalle(){
		if(hthAtributoDetalle!=null){
        	return hthAtributoDetalle.getIdAtributoDetalle();
		}
		return null;        	         
	}
    /**
     * Coloca el valor de la llave en el atributo espec&iacute;fico del DTO foraneo,
	 * crea una nueva instancia del objeto foraneo si este es nulo.
     * @param id Objeto que almacena la llave de un DTO que referencia a un negocio maestro.
     */
    public void setIdAtributoDetalle( Long idAtributoDetalle){
		if(hthAtributoDetalle != null && UtileriaDeTiposDeDatos.isEquals(idAtributoDetalle, hthAtributoDetalle.getIdAtributoDetalle()))
    		return;
			
		if(hthAtributoDetalle==null || hthAtributoDetalle.getIdAtributoDetalle()!=null){
			hthAtributoDetalle = new HthAtributoDetalle();			
		}         
		hthAtributoDetalle.setIdAtributoDetalle(idAtributoDetalle);   
    }
    /**
     * Devuelve el atributo llave del DTO foraneo.
     * Verifica si es null antes de devolverlo.
     * @return llave del DTO del negocio maestro
     */
	public Long getIdFormato(){
		if(hthFormato!=null){
        	return hthFormato.getIdFormato();
		}
		return null;        	         
	}
    /**
     * Coloca el valor de la llave en el atributo espec&iacute;fico del DTO foraneo,
	 * crea una nueva instancia del objeto foraneo si este es nulo.
     * @param id Objeto que almacena la llave de un DTO que referencia a un negocio maestro.
     */
    public void setIdFormato( Long idFormato){
		if(hthFormato != null && UtileriaDeTiposDeDatos.isEquals(idFormato, hthFormato.getIdFormato()))
    		return;
			
		if(hthFormato==null || hthFormato.getIdFormato()!=null){
			hthFormato = new HthFormato();			
		}         
		hthFormato.setIdFormato(idFormato);   
    }

    /** Metodos que manejan los DTO foraneos */		
    /**
     * Devuelve el objeto del DTO foraneo, que es la referencia a un objeto que representa un negocio maestro.
	 * @return
     */
    public HthAtributoEncabezado getHthAtributoEncabezado(){
		if(hthAtributoEncabezado==null){
			hthAtributoEncabezado = new HthAtributoEncabezado();			
		}
        return hthAtributoEncabezado;
    }
    /**
     * Coloca el objeto del DTO foraneo, que es la referencia a un objeto que representa un negocio maestro.
	 * @param DTO objeto foraneo
     */
    public void setHthAtributoEncabezado(HthAtributoEncabezado hthAtributoEncabezado){
        this.hthAtributoEncabezado = hthAtributoEncabezado;
    }
    /**
     * Devuelve el objeto del DTO foraneo, que es la referencia a un objeto que representa un negocio maestro.
	 * @return
     */
    public HthAtributoDetalle getHthAtributoDetalle(){
		if(hthAtributoDetalle==null){
			hthAtributoDetalle = new HthAtributoDetalle();			
		}
        return hthAtributoDetalle;
    }
    /**
     * Coloca el objeto del DTO foraneo, que es la referencia a un objeto que representa un negocio maestro.
	 * @param DTO objeto foraneo
     */
    public void setHthAtributoDetalle(HthAtributoDetalle hthAtributoDetalle){
        this.hthAtributoDetalle = hthAtributoDetalle;
    }
    /**
     * Devuelve el objeto del DTO foraneo, que es la referencia a un objeto que representa un negocio maestro.
	 * @return
     */
    public HthFormato getHthFormato(){
		if(hthFormato==null){
			hthFormato = new HthFormato();			
		}
        return hthFormato;
    }
    /**
     * Coloca el objeto del DTO foraneo, que es la referencia a un objeto que representa un negocio maestro.
	 * @param DTO objeto foraneo
     */
    public void setHthFormato(HthFormato hthFormato){
        this.hthFormato = hthFormato;
    }

    public boolean equals(Object obj){
        return equals(obj,true);
    }
    
    
    public boolean equals(Object obj, boolean conForaneos) {
    	   if (obj == null) { return false; }
    	   if (obj == this) { return true; }
    	   if (obj.getClass() != getClass()) {
    	     return false;
    	   }
    	   HthAtributoFormato otherHthAtributoFormato = (HthAtributoFormato) obj;
    	   EqualsBuilder eq = new EqualsBuilder();
           eq.append(posicion, otherHthAtributoFormato.posicion);
           eq.append(posicionIni, otherHthAtributoFormato.posicionIni);
           eq.append(posicionFin, otherHthAtributoFormato.posicionFin);
           eq.append(tipoDatoAtributo, otherHthAtributoFormato.tipoDatoAtributo);
           eq.append(formatoAtributo, otherHthAtributoFormato.formatoAtributo);
           eq.append(opcional, otherHthAtributoFormato.opcional);
           eq.append(usuarioCreacion, otherHthAtributoFormato.usuarioCreacion);
           eq.append(fechaCreacin, otherHthAtributoFormato.fechaCreacin);
           eq.append(usuarioUltAct, otherHthAtributoFormato.usuarioUltAct);
           eq.append(fechaUltAct, otherHthAtributoFormato.fechaUltAct);
           eq.append(idAtributoFormato, otherHthAtributoFormato.idAtributoFormato);
           if(conForaneos){
               eq.append(hthAtributoEncabezado, otherHthAtributoFormato.hthAtributoEncabezado);
               eq.append(hthAtributoDetalle, otherHthAtributoFormato.hthAtributoDetalle);
               eq.append(hthFormato, otherHthAtributoFormato.hthFormato);
           }
    	   return eq.isEquals();
    }
    
    
     public int hashCode() {
	     return new HashCodeBuilder()
                  .append(posicion)
                  .append(posicionIni)
                  .append(posicionFin)
                  .append(tipoDatoAtributo)
                  .append(formatoAtributo)
                  .append(opcional)
                  .append(usuarioCreacion)
                  .append(fechaCreacin)
                  .append(usuarioUltAct)
                  .append(fechaUltAct)
                  .append(idAtributoFormato)
                  .append(hthAtributoEncabezado)
                  .append(hthAtributoDetalle)
                  .append(hthFormato)
	              .toHashCode();
   }
    
    
   public String toString() {
     return new ToStringBuilder(this)
                  .append("posicion",posicion)
                  .append("posicionIni",posicionIni)
                  .append("posicionFin",posicionFin)
                  .append("tipoDatoAtributo",tipoDatoAtributo)
                  .append("formatoAtributo",formatoAtributo)
                  .append("opcional",opcional)
                  .append("usuarioCreacion",usuarioCreacion)
                  .append("fechaCreacin",fechaCreacin)
                  .append("usuarioUltAct",usuarioUltAct)
                  .append("fechaUltAct",fechaUltAct)
                  .append("idAtributoFormato",idAtributoFormato)
                  .append("hthAtributoEncabezado", hthAtributoEncabezado)
                  .append("hthAtributoDetalle", hthAtributoDetalle)
                  .append("hthFormato", hthFormato)
                  .toString();
   }    
    
    
}
