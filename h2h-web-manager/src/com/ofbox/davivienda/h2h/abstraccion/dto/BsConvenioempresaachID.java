package com.ofbox.davivienda.h2h.abstraccion.dto;

import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


public class BsConvenioempresaachID implements Serializable{


     private Long empresaodepto;
     
     private Long convenio;
     
   
     
     public Long getEmpresaodepto(){
         return this.empresaodepto;
     }

     public void setEmpresaodepto( Long empresaodepto){
          this.empresaodepto = empresaodepto;
     }
     
     
     public Long getConvenio(){
         return this.convenio;
     }

     public void setConvenio( Long convenio){
          this.convenio = convenio;
     }
     

   public boolean equals(Object obj, boolean conForaneos) {
    	   if (obj == null) { return false; }
    	   if (obj == this) { return true; }
    	   if (obj.getClass() != getClass()) {
    	     return false;
    	   }
    	   BsConvenioempresaachID otherBsConvenioempresaachID = (BsConvenioempresaachID) obj;
    	   EqualsBuilder eq = new EqualsBuilder();
           eq.append(empresaodepto, otherBsConvenioempresaachID.empresaodepto);
           eq.append(convenio, otherBsConvenioempresaachID.convenio);
    	   return eq.isEquals();
    }
    
     public int hashCode() {
	     return new HashCodeBuilder()
                  .append(empresaodepto)
                  .append(convenio)
	              .toHashCode();
   }
    
    
   public String toString() {
     return new ToStringBuilder(this)
                  .append("empresaodepto",empresaodepto)
                  .append("convenio",convenio)
                  .toString();
   }
    
}
