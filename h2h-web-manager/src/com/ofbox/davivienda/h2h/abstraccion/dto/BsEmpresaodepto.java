package com.ofbox.davivienda.h2h.abstraccion.dto;
import java.lang.Long;
import java.io.Serializable;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
/**
 * Clase POJO(Plain Old Java Object), que permite almacenar los valores de las propiedades de un objeto de negocio, tambi&eacute;n conocido como DTO(Data Transfer Object).
 * Este DTO es el que viaja entre la capa DAO y la capa de negocios, y que posteriormente es manipulado en la capa de la vista.  
 * 
 * @author Systems Out of the Box
 * @version f3.1 --
 */
 
public class BsEmpresaodepto implements Serializable{
	/**
     * Atributo que almacena el valor que funciona como llave para un objeto de Negocio.
     */
     private Long empresaodepto;
    /**
     * Devuelve el objeto que almacena el identificador del DTO.
     * @return
     */
     public Long getDtoId(){
        return empresaodepto;
     }
    /**
     * Coloca el objeto que almacena el identificador del DTO.
     * @param dtoId objeto que almacena la llave que identifica al DTO. 
     */
     public void setDtoId(Long dtoId){
     	this.empresaodepto = dtoId;
     }
    
    /** Atributos que almacenan los valores para las propiedades de un objeto DTO correspondiente a un objeto Negocio. */
     
     private Long cliente;
	 
     private String nombre;
	 
     private String direccion;
	 
     private String telefono;
	 
     private String fax;
	 
     private String email;
	 
     private String nombrecontacto;
	 
     private String apellidocontacto;
	 
     private Long proveedor;
	 
     private Long nivelseguridad;
	 
     private java.util.Date fechaestatus;
	 
     private Long estatus;
	 
     private Long origenlotes;
	 
     private String registrofiscal;
	 
     private java.math.BigDecimal numpatronal;
	 
     private Long requierecompro;
	 
     private Long esconsumidorfinal;
	 
     private String giroempresa;
	 
     private String ciudad;
	 
     private String cargo;
	 
     private java.math.BigDecimal limitexarchivo;
	 
     private java.math.BigDecimal limitexlote;
	 
     private java.math.BigDecimal limitextransaccion;
	 



     
    
    /** M&eacute;todos que devuelven y colocan los valores de los atributos respectivos del DTO. */
     
     public Long getEmpresaodepto(){
		return this.empresaodepto;         
     }

     public void setEmpresaodepto( Long empresaodepto){
		this.empresaodepto = empresaodepto;
     }
     public Long getCliente(){
		return this.cliente;         
     }

     public void setCliente( Long cliente){
		this.cliente = cliente;
     }
     public String getNombre(){
		return this.nombre;         
     }

     public void setNombre( String nombre){
		this.nombre = nombre;
     }
     public String getDireccion(){
		return this.direccion;         
     }

     public void setDireccion( String direccion){
		this.direccion = direccion;
     }
     public String getTelefono(){
		return this.telefono;         
     }

     public void setTelefono( String telefono){
		this.telefono = telefono;
     }
     public String getFax(){
		return this.fax;         
     }

     public void setFax( String fax){
		this.fax = fax;
     }
     public String getEmail(){
		return this.email;         
     }

     public void setEmail( String email){
		this.email = email;
     }
     public String getNombrecontacto(){
		return this.nombrecontacto;         
     }

     public void setNombrecontacto( String nombrecontacto){
		this.nombrecontacto = nombrecontacto;
     }
     public String getApellidocontacto(){
		return this.apellidocontacto;         
     }

     public void setApellidocontacto( String apellidocontacto){
		this.apellidocontacto = apellidocontacto;
     }
     public Long getProveedor(){
		return this.proveedor;         
     }

     public void setProveedor( Long proveedor){
		this.proveedor = proveedor;
     }
     public Long getNivelseguridad(){
		return this.nivelseguridad;         
     }

     public void setNivelseguridad( Long nivelseguridad){
		this.nivelseguridad = nivelseguridad;
     }
     public java.util.Date getFechaestatus(){
		return this.fechaestatus;         
     }

     public void setFechaestatus( java.util.Date fechaestatus){
		this.fechaestatus = fechaestatus;
     }
     public Long getEstatus(){
		return this.estatus;         
     }

     public void setEstatus( Long estatus){
		this.estatus = estatus;
     }
     public Long getOrigenlotes(){
		return this.origenlotes;         
     }

     public void setOrigenlotes( Long origenlotes){
		this.origenlotes = origenlotes;
     }
     public String getRegistrofiscal(){
		return this.registrofiscal;         
     }

     public void setRegistrofiscal( String registrofiscal){
		this.registrofiscal = registrofiscal;
     }
     public java.math.BigDecimal getNumpatronal(){
		return this.numpatronal;         
     }

     public void setNumpatronal( java.math.BigDecimal numpatronal){
		this.numpatronal = numpatronal;
     }
     public Long getRequierecompro(){
		return this.requierecompro;         
     }

     public void setRequierecompro( Long requierecompro){
		this.requierecompro = requierecompro;
     }
     public Long getEsconsumidorfinal(){
		return this.esconsumidorfinal;         
     }

     public void setEsconsumidorfinal( Long esconsumidorfinal){
		this.esconsumidorfinal = esconsumidorfinal;
     }
     public String getGiroempresa(){
		return this.giroempresa;         
     }

     public void setGiroempresa( String giroempresa){
		this.giroempresa = giroempresa;
     }
     public String getCiudad(){
		return this.ciudad;         
     }

     public void setCiudad( String ciudad){
		this.ciudad = ciudad;
     }
     public String getCargo(){
		return this.cargo;         
     }

     public void setCargo( String cargo){
		this.cargo = cargo;
     }
     public java.math.BigDecimal getLimitexarchivo(){
		return this.limitexarchivo;         
     }

     public void setLimitexarchivo( java.math.BigDecimal limitexarchivo){
		this.limitexarchivo = limitexarchivo;
     }
     public java.math.BigDecimal getLimitexlote(){
		return this.limitexlote;         
     }

     public void setLimitexlote( java.math.BigDecimal limitexlote){
		this.limitexlote = limitexlote;
     }
     public java.math.BigDecimal getLimitextransaccion(){
		return this.limitextransaccion;         
     }

     public void setLimitextransaccion( java.math.BigDecimal limitextransaccion){
		this.limitextransaccion = limitextransaccion;
     }



    public boolean equals(Object obj){
        return equals(obj,true);
    }
    
    
    public boolean equals(Object obj, boolean conForaneos) {
    	   if (obj == null) { return false; }
    	   if (obj == this) { return true; }
    	   if (obj.getClass() != getClass()) {
    	     return false;
    	   }
    	   BsEmpresaodepto otherBsEmpresaodepto = (BsEmpresaodepto) obj;
    	   EqualsBuilder eq = new EqualsBuilder();
           eq.append(cliente, otherBsEmpresaodepto.cliente);
           eq.append(nombre, otherBsEmpresaodepto.nombre);
           eq.append(direccion, otherBsEmpresaodepto.direccion);
           eq.append(telefono, otherBsEmpresaodepto.telefono);
           eq.append(fax, otherBsEmpresaodepto.fax);
           eq.append(email, otherBsEmpresaodepto.email);
           eq.append(nombrecontacto, otherBsEmpresaodepto.nombrecontacto);
           eq.append(apellidocontacto, otherBsEmpresaodepto.apellidocontacto);
           eq.append(proveedor, otherBsEmpresaodepto.proveedor);
           eq.append(nivelseguridad, otherBsEmpresaodepto.nivelseguridad);
           eq.append(fechaestatus, otherBsEmpresaodepto.fechaestatus);
           eq.append(estatus, otherBsEmpresaodepto.estatus);
           eq.append(origenlotes, otherBsEmpresaodepto.origenlotes);
           eq.append(registrofiscal, otherBsEmpresaodepto.registrofiscal);
           eq.append(numpatronal, otherBsEmpresaodepto.numpatronal);
           eq.append(requierecompro, otherBsEmpresaodepto.requierecompro);
           eq.append(esconsumidorfinal, otherBsEmpresaodepto.esconsumidorfinal);
           eq.append(giroempresa, otherBsEmpresaodepto.giroempresa);
           eq.append(ciudad, otherBsEmpresaodepto.ciudad);
           eq.append(cargo, otherBsEmpresaodepto.cargo);
           eq.append(limitexarchivo, otherBsEmpresaodepto.limitexarchivo);
           eq.append(limitexlote, otherBsEmpresaodepto.limitexlote);
           eq.append(limitextransaccion, otherBsEmpresaodepto.limitextransaccion);
           eq.append(empresaodepto, otherBsEmpresaodepto.empresaodepto);
    	   return eq.isEquals();
    }
    
    
     public int hashCode() {
	     return new HashCodeBuilder()
                  .append(cliente)
                  .append(nombre)
                  .append(direccion)
                  .append(telefono)
                  .append(fax)
                  .append(email)
                  .append(nombrecontacto)
                  .append(apellidocontacto)
                  .append(proveedor)
                  .append(nivelseguridad)
                  .append(fechaestatus)
                  .append(estatus)
                  .append(origenlotes)
                  .append(registrofiscal)
                  .append(numpatronal)
                  .append(requierecompro)
                  .append(esconsumidorfinal)
                  .append(giroempresa)
                  .append(ciudad)
                  .append(cargo)
                  .append(limitexarchivo)
                  .append(limitexlote)
                  .append(limitextransaccion)
                  .append(empresaodepto)
	              .toHashCode();
   }
    
    
   public String toString() {
     return new ToStringBuilder(this)
                  .append("cliente",cliente)
                  .append("nombre",nombre)
                  .append("direccion",direccion)
                  .append("telefono",telefono)
                  .append("fax",fax)
                  .append("email",email)
                  .append("nombrecontacto",nombrecontacto)
                  .append("apellidocontacto",apellidocontacto)
                  .append("proveedor",proveedor)
                  .append("nivelseguridad",nivelseguridad)
                  .append("fechaestatus",fechaestatus)
                  .append("estatus",estatus)
                  .append("origenlotes",origenlotes)
                  .append("registrofiscal",registrofiscal)
                  .append("numpatronal",numpatronal)
                  .append("requierecompro",requierecompro)
                  .append("esconsumidorfinal",esconsumidorfinal)
                  .append("giroempresa",giroempresa)
                  .append("ciudad",ciudad)
                  .append("cargo",cargo)
                  .append("limitexarchivo",limitexarchivo)
                  .append("limitexlote",limitexlote)
                  .append("limitextransaccion",limitextransaccion)
                  .append("empresaodepto",empresaodepto)
                  .toString();
   }    
    
    
}
