package com.ofbox.davivienda.h2h.abstraccion.dto;
import com.ofbox.davivienda.h2h.abstraccion.dto.BsConvenioempresaID;
import java.io.Serializable;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.validacion.UtileriaDeTiposDeDatos;
/**
 * Clase POJO(Plain Old Java Object), que permite almacenar los valores de las propiedades de un objeto de negocio, tambi&eacute;n conocido como DTO(Data Transfer Object).
 * Este DTO es el que viaja entre la capa DAO y la capa de negocios, y que posteriormente es manipulado en la capa de la vista.  
 * 
 * @author Systems Out of the Box
 * @version f3.1 --
 */
 
public class BsConvenioempresa implements Serializable{
    /**
     * Atributo que almacena los valores que funciona como llaves para un objeto de Negocio.
     */
     private BsConvenioempresaID dtoId;
    /**
     * Devuelve el objeto que almacena los identificadores del DTO.
     * @return
     */
     public BsConvenioempresaID getDtoId(){     
     	if(dtoId == null){
     		dtoId = new BsConvenioempresaID();
     	}     	
     	return dtoId;        
     }
    /**
     * Coloca el objeto que almacena los identificadores del DTO.
     * @param dtoId objeto que almacena las llaves que identifican al DTO. 
     */
     public void setDtoId(BsConvenioempresaID dtoId){
     	this.dtoId = dtoId;
     }
    
    /** Atributos que almacenan los valores para las propiedades de un objeto DTO correspondiente a un objeto Negocio. */
     

    /**
     * Atributos que almacenan los DTOs de objetos de negocios que funcionan como objetos foraneos.
     */    		
    private BsConvenio bsConvenio;
    private BsEmpresaodepto bsEmpresaodepto;


     
    
    /** M&eacute;todos que devuelven y colocan los valores de los atributos respectivos del DTO. */
     
     public Long getConvenio(){
		return getDtoId().getConvenio();
     }

     public void setConvenio( Long convenio){
		getDtoId().setConvenio(convenio);             	
     }

	/**   Metodos que manejan los identificadores de los DTO foraneos */ 
    /**
     * Devuelve el atributo llave del DTO foraneo.
     * Verifica si es null antes de devolverlo.
     * @return llave del DTO del negocio maestro
     */
	public Long getTipoconvenio(){
		if(bsConvenio!=null){
        	return bsConvenio.getTipoconvenio();
		}
		return null;        	         
	}
    /**
     * Coloca el valor de la llave en el atributo espec&iacute;fico del DTO foraneo,
	 * crea una nueva instancia del objeto foraneo si este es nulo.
     * @param id Objeto que almacena la llave de un DTO que referencia a un negocio maestro.
     */
    public void setTipoconvenio( Long tipoconvenio){
		if(bsConvenio != null && UtileriaDeTiposDeDatos.isEquals(tipoconvenio, bsConvenio.getTipoconvenio()))
    		return;
			
		if(bsConvenio==null || bsConvenio.getTipoconvenio()!=null){
			bsConvenio = new BsConvenio();			
		}         
		bsConvenio.setTipoconvenio(tipoconvenio);   
		getDtoId().setTipoconvenio(tipoconvenio);             	
    }
    /**
     * Devuelve el atributo llave del DTO foraneo.
     * Verifica si es null antes de devolverlo.
     * @return llave del DTO del negocio maestro
     */
	public Long getEmpresaodepto(){
		if(bsEmpresaodepto!=null){
        	return bsEmpresaodepto.getEmpresaodepto();
		}
		return null;        	         
	}
    /**
     * Coloca el valor de la llave en el atributo espec&iacute;fico del DTO foraneo,
	 * crea una nueva instancia del objeto foraneo si este es nulo.
     * @param id Objeto que almacena la llave de un DTO que referencia a un negocio maestro.
     */
    public void setEmpresaodepto( Long empresaodepto){
		if(bsEmpresaodepto != null && UtileriaDeTiposDeDatos.isEquals(empresaodepto, bsEmpresaodepto.getEmpresaodepto()))
    		return;
			
		if(bsEmpresaodepto==null || bsEmpresaodepto.getEmpresaodepto()!=null){
			bsEmpresaodepto = new BsEmpresaodepto();			
		}         
		bsEmpresaodepto.setEmpresaodepto(empresaodepto);   
		getDtoId().setEmpresaodepto(empresaodepto);             	
    }

    /** Metodos que manejan los DTO foraneos */	
    /**
     * Devuelve el objeto del DTO foraneo, que es la referencia a un objeto que representa un negocio maestro.
	 * @return
     */
    public BsConvenio getBsConvenio(){
		if(bsConvenio==null){
			bsConvenio = new BsConvenio();			
		}
        return bsConvenio;
    }
    /**
     * Coloca el objeto del DTO foraneo, que es la referencia a un objeto que representa un negocio maestro.
	 * @param DTO objeto foraneo
     */
    public void setBsConvenio(BsConvenio bsConvenio){
        this.bsConvenio = bsConvenio;
    }
    /**
     * Devuelve el objeto del DTO foraneo, que es la referencia a un objeto que representa un negocio maestro.
	 * @return
     */
    public BsEmpresaodepto getBsEmpresaodepto(){
		if(bsEmpresaodepto==null){
			bsEmpresaodepto = new BsEmpresaodepto();			
		}
        return bsEmpresaodepto;
    }
    /**
     * Coloca el objeto del DTO foraneo, que es la referencia a un objeto que representa un negocio maestro.
	 * @param DTO objeto foraneo
     */
    public void setBsEmpresaodepto(BsEmpresaodepto bsEmpresaodepto){
        this.bsEmpresaodepto = bsEmpresaodepto;
    }

    public boolean equals(Object obj){
        return equals(obj,true);
    }
    
    
    public boolean equals(Object obj, boolean conForaneos) {
    	   if (obj == null) { return false; }
    	   if (obj == this) { return true; }
    	   if (obj.getClass() != getClass()) {
    	     return false;
    	   }
    	   BsConvenioempresa otherBsConvenioempresa = (BsConvenioempresa) obj;
    	   EqualsBuilder eq = new EqualsBuilder();
           eq.append(dtoId, otherBsConvenioempresa.dtoId);
           if(conForaneos){
               eq.append(bsConvenio, otherBsConvenioempresa.bsConvenio);
               eq.append(bsEmpresaodepto, otherBsConvenioempresa.bsEmpresaodepto);
           }
    	   return eq.isEquals();
    }
    
    
     public int hashCode() {
	     return new HashCodeBuilder()
                  .append(dtoId)
                  .append(bsConvenio)
                  .append(bsEmpresaodepto)
	              .toHashCode();
   }
    
    
   public String toString() {
     return new ToStringBuilder(this)
                  .append("dtoId",dtoId)
                  .append("bsConvenio", bsConvenio)
                  .append("bsEmpresaodepto", bsEmpresaodepto)
                  .toString();
   }    
    
    
}
