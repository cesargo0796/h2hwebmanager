package com.ofbox.davivienda.h2h.abstraccion.dto;
import java.lang.Long;
import java.io.Serializable;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
/**
 * Clase POJO(Plain Old Java Object), que permite almacenar los valores de las propiedades de un objeto de negocio, tambi&eacute;n conocido como DTO(Data Transfer Object).
 * Este DTO es el que viaja entre la capa DAO y la capa de negocios, y que posteriormente es manipulado en la capa de la vista.  
 * 
 * @author Systems Out of the Box
 * @version f3.1 --
 */
 
public class HthParametroSistema implements Serializable{
public static final String habilitar_envio_correo_SI = "S";
public static final String habilitar_envio_correo_NO = "N";
	/**
     * Atributo que almacena el valor que funciona como llave para un objeto de Negocio.
     */
     private Long idParametroSistema;
    /**
     * Devuelve el objeto que almacena el identificador del DTO.
     * @return
     */
     public Long getDtoId(){
        return idParametroSistema;
     }
    /**
     * Coloca el objeto que almacena el identificador del DTO.
     * @param dtoId objeto que almacena la llave que identifica al DTO. 
     */
     public void setDtoId(Long dtoId){
     	this.idParametroSistema = dtoId;
     }
    
    /** Atributos que almacenan los valores para las propiedades de un objeto DTO correspondiente a un objeto Negocio. */
     
     private Long tiempoMonitoreoDefault;
	 
     private String horaInicioMonitoreo;
	 
     private String horaFinMonitoreo;
	 
     private String carpetaLecturaDefault;
	 
     private String carpetaEscrituraDefault;
	 
     private String rutaRaiz;
	 
     private String carSeparadorDefault;
	 
     private String habilitarEnvioCorreo;
	 
     private String remitenteEnvioCorreo;
	 
     private String asuntoEnvioCorreo;
	 
     private String javamailJndi;
	 
     private String listaEnvioCorreo;
	 
     private String servidorEnvioCorreo;
	 
     private String usuarioEnvioCorreo;
	 
     private String passwordEnvioCorreo;
	 
     private Long puertoEnvioCorreo;
	 
     private String directorioLocalDestino;
	 
     private String hostSftp;
	 
     private String puertoSftp;
	 
     private String usuarioSftp;
	 
     private String passwordSftp;
	 
     private String llaveSftp;
	 
     private Long tiempoDrenadoBitacora;
	 



     
    
    /** M&eacute;todos que devuelven y colocan los valores de los atributos respectivos del DTO. */
     
     public Long getIdParametroSistema(){
		return this.idParametroSistema;         
     }

     public void setIdParametroSistema( Long idParametroSistema){
		this.idParametroSistema = idParametroSistema;
     }
     public Long getTiempoMonitoreoDefault(){
		return this.tiempoMonitoreoDefault;         
     }

     public void setTiempoMonitoreoDefault( Long tiempoMonitoreoDefault){
		this.tiempoMonitoreoDefault = tiempoMonitoreoDefault;
     }
     public String getHoraInicioMonitoreo(){
		return this.horaInicioMonitoreo;         
     }

     public void setHoraInicioMonitoreo( String horaInicioMonitoreo){
		this.horaInicioMonitoreo = horaInicioMonitoreo;
     }
     public String getHoraFinMonitoreo(){
		return this.horaFinMonitoreo;         
     }

     public void setHoraFinMonitoreo( String horaFinMonitoreo){
		this.horaFinMonitoreo = horaFinMonitoreo;
     }
     public String getCarpetaLecturaDefault(){
		return this.carpetaLecturaDefault;         
     }

     public void setCarpetaLecturaDefault( String carpetaLecturaDefault){
		this.carpetaLecturaDefault = carpetaLecturaDefault;
     }
     public String getCarpetaEscrituraDefault(){
		return this.carpetaEscrituraDefault;         
     }

     public void setCarpetaEscrituraDefault( String carpetaEscrituraDefault){
		this.carpetaEscrituraDefault = carpetaEscrituraDefault;
     }
     public String getRutaRaiz(){
		return this.rutaRaiz;         
     }

     public void setRutaRaiz( String rutaRaiz){
		this.rutaRaiz = rutaRaiz;
     }
     public String getCarSeparadorDefault(){
		return this.carSeparadorDefault;         
     }

     public void setCarSeparadorDefault( String carSeparadorDefault){
		this.carSeparadorDefault = carSeparadorDefault;
     }
     public String getHabilitarEnvioCorreo(){
		return this.habilitarEnvioCorreo;         
     }

     public void setHabilitarEnvioCorreo( String habilitarEnvioCorreo){
		this.habilitarEnvioCorreo = habilitarEnvioCorreo;
     }
     public String getRemitenteEnvioCorreo(){
		return this.remitenteEnvioCorreo;         
     }

     public void setRemitenteEnvioCorreo( String remitenteEnvioCorreo){
		this.remitenteEnvioCorreo = remitenteEnvioCorreo;
     }
     public String getAsuntoEnvioCorreo(){
		return this.asuntoEnvioCorreo;         
     }

     public void setAsuntoEnvioCorreo( String asuntoEnvioCorreo){
		this.asuntoEnvioCorreo = asuntoEnvioCorreo;
     }
     public String getJavamailJndi(){
		return this.javamailJndi;         
     }

     public void setJavamailJndi( String javamailJndi){
		this.javamailJndi = javamailJndi;
     }
     public String getListaEnvioCorreo(){
		return this.listaEnvioCorreo;         
     }

     public void setListaEnvioCorreo( String listaEnvioCorreo){
		this.listaEnvioCorreo = listaEnvioCorreo;
     }
     public String getServidorEnvioCorreo(){
		return this.servidorEnvioCorreo;         
     }

     public void setServidorEnvioCorreo( String servidorEnvioCorreo){
		this.servidorEnvioCorreo = servidorEnvioCorreo;
     }
     public String getUsuarioEnvioCorreo(){
		return this.usuarioEnvioCorreo;         
     }

     public void setUsuarioEnvioCorreo( String usuarioEnvioCorreo){
		this.usuarioEnvioCorreo = usuarioEnvioCorreo;
     }
     public String getPasswordEnvioCorreo(){
		return this.passwordEnvioCorreo;         
     }

     public void setPasswordEnvioCorreo( String passwordEnvioCorreo){
		this.passwordEnvioCorreo = passwordEnvioCorreo;
     }
     public Long getPuertoEnvioCorreo(){
		return this.puertoEnvioCorreo;         
     }

     public void setPuertoEnvioCorreo( Long puertoEnvioCorreo){
		this.puertoEnvioCorreo = puertoEnvioCorreo;
     }
     public String getDirectorioLocalDestino(){
		return this.directorioLocalDestino;         
     }

     public void setDirectorioLocalDestino( String directorioLocalDestino){
		this.directorioLocalDestino = directorioLocalDestino;
     }
     public String getHostSftp(){
		return this.hostSftp;         
     }

     public void setHostSftp( String hostSftp){
		this.hostSftp = hostSftp;
     }
     public String getPuertoSftp(){
		return this.puertoSftp;         
     }

     public void setPuertoSftp( String puertoSftp){
		this.puertoSftp = puertoSftp;
     }
     public String getUsuarioSftp(){
		return this.usuarioSftp;         
     }

     public void setUsuarioSftp( String usuarioSftp){
		this.usuarioSftp = usuarioSftp;
     }
     public String getPasswordSftp(){
		return this.passwordSftp;         
     }

     public void setPasswordSftp( String passwordSftp){
		this.passwordSftp = passwordSftp;
     }
     public String getLlaveSftp(){
		return this.llaveSftp;         
     }

     public void setLlaveSftp( String llaveSftp){
		this.llaveSftp = llaveSftp;
     }
     public Long getTiempoDrenadoBitacora(){
		return this.tiempoDrenadoBitacora;         
     }

     public void setTiempoDrenadoBitacora( Long tiempoDrenadoBitacora){
		this.tiempoDrenadoBitacora = tiempoDrenadoBitacora;
     }



    public boolean equals(Object obj){
        return equals(obj,true);
    }
    
    
    public boolean equals(Object obj, boolean conForaneos) {
    	   if (obj == null) { return false; }
    	   if (obj == this) { return true; }
    	   if (obj.getClass() != getClass()) {
    	     return false;
    	   }
    	   HthParametroSistema otherHthParametroSistema = (HthParametroSistema) obj;
    	   EqualsBuilder eq = new EqualsBuilder();
           eq.append(tiempoMonitoreoDefault, otherHthParametroSistema.tiempoMonitoreoDefault);
           eq.append(horaInicioMonitoreo, otherHthParametroSistema.horaInicioMonitoreo);
           eq.append(horaFinMonitoreo, otherHthParametroSistema.horaFinMonitoreo);
           eq.append(carpetaLecturaDefault, otherHthParametroSistema.carpetaLecturaDefault);
           eq.append(carpetaEscrituraDefault, otherHthParametroSistema.carpetaEscrituraDefault);
           eq.append(rutaRaiz, otherHthParametroSistema.rutaRaiz);
           eq.append(carSeparadorDefault, otherHthParametroSistema.carSeparadorDefault);
           eq.append(habilitarEnvioCorreo, otherHthParametroSistema.habilitarEnvioCorreo);
           eq.append(remitenteEnvioCorreo, otherHthParametroSistema.remitenteEnvioCorreo);
           eq.append(asuntoEnvioCorreo, otherHthParametroSistema.asuntoEnvioCorreo);
           eq.append(javamailJndi, otherHthParametroSistema.javamailJndi);
           eq.append(listaEnvioCorreo, otherHthParametroSistema.listaEnvioCorreo);
           eq.append(servidorEnvioCorreo, otherHthParametroSistema.servidorEnvioCorreo);
           eq.append(usuarioEnvioCorreo, otherHthParametroSistema.usuarioEnvioCorreo);
           eq.append(passwordEnvioCorreo, otherHthParametroSistema.passwordEnvioCorreo);
           eq.append(puertoEnvioCorreo, otherHthParametroSistema.puertoEnvioCorreo);
           eq.append(directorioLocalDestino, otherHthParametroSistema.directorioLocalDestino);
           eq.append(hostSftp, otherHthParametroSistema.hostSftp);
           eq.append(puertoSftp, otherHthParametroSistema.puertoSftp);
           eq.append(usuarioSftp, otherHthParametroSistema.usuarioSftp);
           eq.append(passwordSftp, otherHthParametroSistema.passwordSftp);
           eq.append(llaveSftp, otherHthParametroSistema.llaveSftp);
           eq.append(tiempoDrenadoBitacora, otherHthParametroSistema.tiempoDrenadoBitacora);
           eq.append(idParametroSistema, otherHthParametroSistema.idParametroSistema);
    	   return eq.isEquals();
    }
    
    
     public int hashCode() {
	     return new HashCodeBuilder()
                  .append(tiempoMonitoreoDefault)
                  .append(horaInicioMonitoreo)
                  .append(horaFinMonitoreo)
                  .append(carpetaLecturaDefault)
                  .append(carpetaEscrituraDefault)
                  .append(rutaRaiz)
                  .append(carSeparadorDefault)
                  .append(habilitarEnvioCorreo)
                  .append(remitenteEnvioCorreo)
                  .append(asuntoEnvioCorreo)
                  .append(javamailJndi)
                  .append(listaEnvioCorreo)
                  .append(servidorEnvioCorreo)
                  .append(usuarioEnvioCorreo)
                  .append(passwordEnvioCorreo)
                  .append(puertoEnvioCorreo)
                  .append(directorioLocalDestino)
                  .append(hostSftp)
                  .append(puertoSftp)
                  .append(usuarioSftp)
                  .append(passwordSftp)
                  .append(llaveSftp)
                  .append(tiempoDrenadoBitacora)
                  .append(idParametroSistema)
	              .toHashCode();
   }
    
    
   public String toString() {
     return new ToStringBuilder(this)
                  .append("tiempoMonitoreoDefault",tiempoMonitoreoDefault)
                  .append("horaInicioMonitoreo",horaInicioMonitoreo)
                  .append("horaFinMonitoreo",horaFinMonitoreo)
                  .append("carpetaLecturaDefault",carpetaLecturaDefault)
                  .append("carpetaEscrituraDefault",carpetaEscrituraDefault)
                  .append("rutaRaiz",rutaRaiz)
                  .append("carSeparadorDefault",carSeparadorDefault)
                  .append("habilitarEnvioCorreo",habilitarEnvioCorreo)
                  .append("remitenteEnvioCorreo",remitenteEnvioCorreo)
                  .append("asuntoEnvioCorreo",asuntoEnvioCorreo)
                  .append("javamailJndi",javamailJndi)
                  .append("listaEnvioCorreo",listaEnvioCorreo)
                  .append("servidorEnvioCorreo",servidorEnvioCorreo)
                  .append("usuarioEnvioCorreo",usuarioEnvioCorreo)
                  .append("passwordEnvioCorreo",passwordEnvioCorreo)
                  .append("puertoEnvioCorreo",puertoEnvioCorreo)
                  .append("directorioLocalDestino",directorioLocalDestino)
                  .append("hostSftp",hostSftp)
                  .append("puertoSftp",puertoSftp)
                  .append("usuarioSftp",usuarioSftp)
                  .append("passwordSftp",passwordSftp)
                  .append("llaveSftp",llaveSftp)
                  .append("tiempoDrenadoBitacora",tiempoDrenadoBitacora)
                  .append("idParametroSistema",idParametroSistema)
                  .toString();
   }    
    
    
}
