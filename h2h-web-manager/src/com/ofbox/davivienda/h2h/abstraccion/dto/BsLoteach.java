package com.ofbox.davivienda.h2h.abstraccion.dto;
import com.ofbox.davivienda.h2h.abstraccion.dto.BsLoteachID;
import java.io.Serializable;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.validacion.UtileriaDeTiposDeDatos;
/**
 * Clase POJO(Plain Old Java Object), que permite almacenar los valores de las propiedades de un objeto de negocio, tambi&eacute;n conocido como DTO(Data Transfer Object).
 * Este DTO es el que viaja entre la capa DAO y la capa de negocios, y que posteriormente es manipulado en la capa de la vista.  
 * 
 * @author Systems Out of the Box
 * @version f3.1 --
 */
 
public class BsLoteach implements Serializable{
    /**
     * Atributo que almacena los valores que funciona como llaves para un objeto de Negocio.
     */
     private BsLoteachID dtoId;
    /**
     * Devuelve el objeto que almacena los identificadores del DTO.
     * @return
     */
     public BsLoteachID getDtoId(){     
     	if(dtoId == null){
     		dtoId = new BsLoteachID();
     	}     	
     	return dtoId;        
     }
    /**
     * Coloca el objeto que almacena los identificadores del DTO.
     * @param dtoId objeto que almacena las llaves que identifican al DTO. 
     */
     public void setDtoId(BsLoteachID dtoId){
     	this.dtoId = dtoId;
     }
    
    /** Atributos que almacenan los valores para las propiedades de un objeto DTO correspondiente a un objeto Negocio. */
     
     private Long convenio;
	 
     private Long tipolote;
	 
     private java.math.BigDecimal montototal;
	 
     private Long numoperaciones;
	 
     private String nombrelote;
	 
     private java.util.Date fechaenviado;
	 
     private java.util.Date fecharecibido;
	 
     private java.util.Date fechaaplicacion;
	 
     private java.util.Date fechaestatus;
	 
     private Long estatus;
	 
     private String usuarioingreso;
	 
     private Long autorizaciones;
	 
     private Long aplicaciondebitohost;
	 
     private java.util.Date sysMarcatiempo;
	 
     private Long sysProcessid;
	 
     private Long sysLowdatetime;
	 
     private Long sysHighdatetime;
	 
     private Long ponderacion;
	 
     private Long firmas;
	 
     private java.util.Date fechaaplicacionhost;
	 
     private java.util.Date listoparahost;
	 
     private java.util.Date fechahoraprocesado;
	 
     private Long token;
	 
     private String comentariorechazo;
	 
     private String descripcion;
	 
     private String estatuspaybank;
	 
     private String archivogenerado;
	 
     private java.math.BigDecimal comisioncobrada;
	 
     private String cuentadebito;
	 
     private Long autorizacionhost;
	 
     private String descestatus;
	 
     private Long idtransaccion;
	 
     private Long intactualizacion;
	 
     private java.math.BigDecimal montoimpuesto;
	 

    /**
     * Atributos que almacenan los DTOs de objetos de negocios que funcionan como objetos foraneos.
     */    		
    private BsInstalacion bsInstalacion;
    private BsConvenioempresaach bsConvenioempresaach;


     
    
    /** M&eacute;todos que devuelven y colocan los valores de los atributos respectivos del DTO. */
     
     public Long getCodlote(){
		return getDtoId().getCodlote();
     }

     public void setCodlote( Long codlote){
		getDtoId().setCodlote(codlote);             	
     }
     public Long getLote(){
		return getDtoId().getLote();
     }

     public void setLote( Long lote){
		getDtoId().setLote(lote);             	
     }
     public Long getConvenio(){
		return this.convenio;         
     }

     public void setConvenio( Long convenio){
		this.convenio = convenio;
     }
     public Long getTipolote(){
		return this.tipolote;         
     }

     public void setTipolote( Long tipolote){
		this.tipolote = tipolote;
     }
     public java.math.BigDecimal getMontototal(){
		return this.montototal;         
     }

     public void setMontototal( java.math.BigDecimal montototal){
		this.montototal = montototal;
     }
     public Long getNumoperaciones(){
		return this.numoperaciones;         
     }

     public void setNumoperaciones( Long numoperaciones){
		this.numoperaciones = numoperaciones;
     }
     public String getNombrelote(){
		return this.nombrelote;         
     }

     public void setNombrelote( String nombrelote){
		this.nombrelote = nombrelote;
     }
     public java.util.Date getFechaenviado(){
		return this.fechaenviado;         
     }

     public void setFechaenviado( java.util.Date fechaenviado){
		this.fechaenviado = fechaenviado;
     }
     public java.util.Date getFecharecibido(){
		return this.fecharecibido;         
     }

     public void setFecharecibido( java.util.Date fecharecibido){
		this.fecharecibido = fecharecibido;
     }
     public java.util.Date getFechaaplicacion(){
		return this.fechaaplicacion;         
     }

     public void setFechaaplicacion( java.util.Date fechaaplicacion){
		this.fechaaplicacion = fechaaplicacion;
     }
     public java.util.Date getFechaestatus(){
		return this.fechaestatus;         
     }

     public void setFechaestatus( java.util.Date fechaestatus){
		this.fechaestatus = fechaestatus;
     }
     public Long getEstatus(){
		return this.estatus;         
     }

     public void setEstatus( Long estatus){
		this.estatus = estatus;
     }
     public String getUsuarioingreso(){
		return this.usuarioingreso;         
     }

     public void setUsuarioingreso( String usuarioingreso){
		this.usuarioingreso = usuarioingreso;
     }
     public Long getAutorizaciones(){
		return this.autorizaciones;         
     }

     public void setAutorizaciones( Long autorizaciones){
		this.autorizaciones = autorizaciones;
     }
     public Long getAplicaciondebitohost(){
		return this.aplicaciondebitohost;         
     }

     public void setAplicaciondebitohost( Long aplicaciondebitohost){
		this.aplicaciondebitohost = aplicaciondebitohost;
     }
     public java.util.Date getSysMarcatiempo(){
		return this.sysMarcatiempo;         
     }

     public void setSysMarcatiempo( java.util.Date sysMarcatiempo){
		this.sysMarcatiempo = sysMarcatiempo;
     }
     public Long getSysProcessid(){
		return this.sysProcessid;         
     }

     public void setSysProcessid( Long sysProcessid){
		this.sysProcessid = sysProcessid;
     }
     public Long getSysLowdatetime(){
		return this.sysLowdatetime;         
     }

     public void setSysLowdatetime( Long sysLowdatetime){
		this.sysLowdatetime = sysLowdatetime;
     }
     public Long getSysHighdatetime(){
		return this.sysHighdatetime;         
     }

     public void setSysHighdatetime( Long sysHighdatetime){
		this.sysHighdatetime = sysHighdatetime;
     }
     public Long getPonderacion(){
		return this.ponderacion;         
     }

     public void setPonderacion( Long ponderacion){
		this.ponderacion = ponderacion;
     }
     public Long getFirmas(){
		return this.firmas;         
     }

     public void setFirmas( Long firmas){
		this.firmas = firmas;
     }
     public java.util.Date getFechaaplicacionhost(){
		return this.fechaaplicacionhost;         
     }

     public void setFechaaplicacionhost( java.util.Date fechaaplicacionhost){
		this.fechaaplicacionhost = fechaaplicacionhost;
     }
     public java.util.Date getListoparahost(){
		return this.listoparahost;         
     }

     public void setListoparahost( java.util.Date listoparahost){
		this.listoparahost = listoparahost;
     }
     public java.util.Date getFechahoraprocesado(){
		return this.fechahoraprocesado;         
     }

     public void setFechahoraprocesado( java.util.Date fechahoraprocesado){
		this.fechahoraprocesado = fechahoraprocesado;
     }
     public Long getToken(){
		return this.token;         
     }

     public void setToken( Long token){
		this.token = token;
     }
     public String getComentariorechazo(){
		return this.comentariorechazo;         
     }

     public void setComentariorechazo( String comentariorechazo){
		this.comentariorechazo = comentariorechazo;
     }
     public String getDescripcion(){
		return this.descripcion;         
     }

     public void setDescripcion( String descripcion){
		this.descripcion = descripcion;
     }
     public String getEstatuspaybank(){
		return this.estatuspaybank;         
     }

     public void setEstatuspaybank( String estatuspaybank){
		this.estatuspaybank = estatuspaybank;
     }
     public String getArchivogenerado(){
		return this.archivogenerado;         
     }

     public void setArchivogenerado( String archivogenerado){
		this.archivogenerado = archivogenerado;
     }
     public java.math.BigDecimal getComisioncobrada(){
		return this.comisioncobrada;         
     }

     public void setComisioncobrada( java.math.BigDecimal comisioncobrada){
		this.comisioncobrada = comisioncobrada;
     }
     public String getCuentadebito(){
		return this.cuentadebito;         
     }

     public void setCuentadebito( String cuentadebito){
		this.cuentadebito = cuentadebito;
     }
     public Long getAutorizacionhost(){
		return this.autorizacionhost;         
     }

     public void setAutorizacionhost( Long autorizacionhost){
		this.autorizacionhost = autorizacionhost;
     }
     public String getDescestatus(){
		return this.descestatus;         
     }

     public void setDescestatus( String descestatus){
		this.descestatus = descestatus;
     }
     public Long getIdtransaccion(){
		return this.idtransaccion;         
     }

     public void setIdtransaccion( Long idtransaccion){
		this.idtransaccion = idtransaccion;
     }
     public Long getIntactualizacion(){
		return this.intactualizacion;         
     }

     public void setIntactualizacion( Long intactualizacion){
		this.intactualizacion = intactualizacion;
     }
     public java.math.BigDecimal getMontoimpuesto(){
		return this.montoimpuesto;         
     }

     public void setMontoimpuesto( java.math.BigDecimal montoimpuesto){
		this.montoimpuesto = montoimpuesto;
     }

	/**   Metodos que manejan los identificadores de los DTO foraneos */ 
    /**
     * Devuelve el atributo llave del DTO foraneo.
     * Verifica si es null antes de devolverlo.
     * @return llave del DTO del negocio maestro
     */
	public Long getInstalacion(){
		if(bsInstalacion!=null){
        	return bsInstalacion.getInstalacion();
		}
		return null;        	         
	}
    /**
     * Coloca el valor de la llave en el atributo espec&iacute;fico del DTO foraneo,
	 * crea una nueva instancia del objeto foraneo si este es nulo.
     * @param id Objeto que almacena la llave de un DTO que referencia a un negocio maestro.
     */
    public void setInstalacion( Long instalacion){
		if(bsInstalacion != null && UtileriaDeTiposDeDatos.isEquals(instalacion, bsInstalacion.getInstalacion()))
    		return;
			
		if(bsInstalacion==null || bsInstalacion.getInstalacion()!=null){
			bsInstalacion = new BsInstalacion();			
		}         
		bsInstalacion.setInstalacion(instalacion);   
		getDtoId().setInstalacion(instalacion);             	
    }
    /**
     * Devuelve el atributo llave del DTO foraneo.
     * Verifica si es null antes de devolverlo.
     * @return llave del DTO del negocio maestro
     */
	public Long getEmpresaodepto(){
		if(bsConvenioempresaach!=null){
        	return bsConvenioempresaach.getEmpresaodepto();
		}
		return null;        	         
	}
    /**
     * Coloca el valor de la llave en el atributo espec&iacute;fico del DTO foraneo,
	 * crea una nueva instancia del objeto foraneo si este es nulo.
     * @param id Objeto que almacena la llave de un DTO que referencia a un negocio maestro.
     */
    public void setEmpresaodepto( Long empresaodepto){
		if(bsConvenioempresaach != null && UtileriaDeTiposDeDatos.isEquals(empresaodepto, bsConvenioempresaach.getEmpresaodepto()))
    		return;
			
		if(bsConvenioempresaach==null || bsConvenioempresaach.getEmpresaodepto()!=null){
			bsConvenioempresaach = new BsConvenioempresaach();			
		}         
		bsConvenioempresaach.setEmpresaodepto(empresaodepto);   
    }

    /** Metodos que manejan los DTO foraneos */		
    /**
     * Devuelve el objeto del DTO foraneo, que es la referencia a un objeto que representa un negocio maestro.
	 * @return
     */
    public BsInstalacion getBsInstalacion(){
		if(bsInstalacion==null){
			bsInstalacion = new BsInstalacion();			
		}
        return bsInstalacion;
    }
    /**
     * Coloca el objeto del DTO foraneo, que es la referencia a un objeto que representa un negocio maestro.
	 * @param DTO objeto foraneo
     */
    public void setBsInstalacion(BsInstalacion bsInstalacion){
        this.bsInstalacion = bsInstalacion;
    }
    /**
     * Devuelve el objeto del DTO foraneo, que es la referencia a un objeto que representa un negocio maestro.
	 * @return
     */
    public BsConvenioempresaach getBsConvenioempresaach(){
		if(bsConvenioempresaach==null){
			bsConvenioempresaach = new BsConvenioempresaach();			
		}
        return bsConvenioempresaach;
    }
    /**
     * Coloca el objeto del DTO foraneo, que es la referencia a un objeto que representa un negocio maestro.
	 * @param DTO objeto foraneo
     */
    public void setBsConvenioempresaach(BsConvenioempresaach bsConvenioempresaach){
        this.bsConvenioempresaach = bsConvenioempresaach;
    }

    public boolean equals(Object obj){
        return equals(obj,true);
    }
    
    
    public boolean equals(Object obj, boolean conForaneos) {
    	   if (obj == null) { return false; }
    	   if (obj == this) { return true; }
    	   if (obj.getClass() != getClass()) {
    	     return false;
    	   }
    	   BsLoteach otherBsLoteach = (BsLoteach) obj;
    	   EqualsBuilder eq = new EqualsBuilder();
           eq.append(convenio, otherBsLoteach.convenio);
           eq.append(tipolote, otherBsLoteach.tipolote);
           eq.append(montototal, otherBsLoteach.montototal);
           eq.append(numoperaciones, otherBsLoteach.numoperaciones);
           eq.append(nombrelote, otherBsLoteach.nombrelote);
           eq.append(fechaenviado, otherBsLoteach.fechaenviado);
           eq.append(fecharecibido, otherBsLoteach.fecharecibido);
           eq.append(fechaaplicacion, otherBsLoteach.fechaaplicacion);
           eq.append(fechaestatus, otherBsLoteach.fechaestatus);
           eq.append(estatus, otherBsLoteach.estatus);
           eq.append(usuarioingreso, otherBsLoteach.usuarioingreso);
           eq.append(autorizaciones, otherBsLoteach.autorizaciones);
           eq.append(aplicaciondebitohost, otherBsLoteach.aplicaciondebitohost);
           eq.append(sysMarcatiempo, otherBsLoteach.sysMarcatiempo);
           eq.append(sysProcessid, otherBsLoteach.sysProcessid);
           eq.append(sysLowdatetime, otherBsLoteach.sysLowdatetime);
           eq.append(sysHighdatetime, otherBsLoteach.sysHighdatetime);
           eq.append(ponderacion, otherBsLoteach.ponderacion);
           eq.append(firmas, otherBsLoteach.firmas);
           eq.append(fechaaplicacionhost, otherBsLoteach.fechaaplicacionhost);
           eq.append(listoparahost, otherBsLoteach.listoparahost);
           eq.append(fechahoraprocesado, otherBsLoteach.fechahoraprocesado);
           eq.append(token, otherBsLoteach.token);
           eq.append(comentariorechazo, otherBsLoteach.comentariorechazo);
           eq.append(descripcion, otherBsLoteach.descripcion);
           eq.append(estatuspaybank, otherBsLoteach.estatuspaybank);
           eq.append(archivogenerado, otherBsLoteach.archivogenerado);
           eq.append(comisioncobrada, otherBsLoteach.comisioncobrada);
           eq.append(cuentadebito, otherBsLoteach.cuentadebito);
           eq.append(autorizacionhost, otherBsLoteach.autorizacionhost);
           eq.append(descestatus, otherBsLoteach.descestatus);
           eq.append(idtransaccion, otherBsLoteach.idtransaccion);
           eq.append(intactualizacion, otherBsLoteach.intactualizacion);
           eq.append(montoimpuesto, otherBsLoteach.montoimpuesto);
           eq.append(dtoId, otherBsLoteach.dtoId);
           if(conForaneos){
               eq.append(bsInstalacion, otherBsLoteach.bsInstalacion);
               eq.append(bsConvenioempresaach, otherBsLoteach.bsConvenioempresaach);
           }
    	   return eq.isEquals();
    }
    
    
     public int hashCode() {
	     return new HashCodeBuilder()
                  .append(convenio)
                  .append(tipolote)
                  .append(montototal)
                  .append(numoperaciones)
                  .append(nombrelote)
                  .append(fechaenviado)
                  .append(fecharecibido)
                  .append(fechaaplicacion)
                  .append(fechaestatus)
                  .append(estatus)
                  .append(usuarioingreso)
                  .append(autorizaciones)
                  .append(aplicaciondebitohost)
                  .append(sysMarcatiempo)
                  .append(sysProcessid)
                  .append(sysLowdatetime)
                  .append(sysHighdatetime)
                  .append(ponderacion)
                  .append(firmas)
                  .append(fechaaplicacionhost)
                  .append(listoparahost)
                  .append(fechahoraprocesado)
                  .append(token)
                  .append(comentariorechazo)
                  .append(descripcion)
                  .append(estatuspaybank)
                  .append(archivogenerado)
                  .append(comisioncobrada)
                  .append(cuentadebito)
                  .append(autorizacionhost)
                  .append(descestatus)
                  .append(idtransaccion)
                  .append(intactualizacion)
                  .append(montoimpuesto)
                  .append(dtoId)
                  .append(bsInstalacion)
                  .append(bsConvenioempresaach)
	              .toHashCode();
   }
    
    
   public String toString() {
     return new ToStringBuilder(this)
                  .append("convenio",convenio)
                  .append("tipolote",tipolote)
                  .append("montototal",montototal)
                  .append("numoperaciones",numoperaciones)
                  .append("nombrelote",nombrelote)
                  .append("fechaenviado",fechaenviado)
                  .append("fecharecibido",fecharecibido)
                  .append("fechaaplicacion",fechaaplicacion)
                  .append("fechaestatus",fechaestatus)
                  .append("estatus",estatus)
                  .append("usuarioingreso",usuarioingreso)
                  .append("autorizaciones",autorizaciones)
                  .append("aplicaciondebitohost",aplicaciondebitohost)
                  .append("sysMarcatiempo",sysMarcatiempo)
                  .append("sysProcessid",sysProcessid)
                  .append("sysLowdatetime",sysLowdatetime)
                  .append("sysHighdatetime",sysHighdatetime)
                  .append("ponderacion",ponderacion)
                  .append("firmas",firmas)
                  .append("fechaaplicacionhost",fechaaplicacionhost)
                  .append("listoparahost",listoparahost)
                  .append("fechahoraprocesado",fechahoraprocesado)
                  .append("token",token)
                  .append("comentariorechazo",comentariorechazo)
                  .append("descripcion",descripcion)
                  .append("estatuspaybank",estatuspaybank)
                  .append("archivogenerado",archivogenerado)
                  .append("comisioncobrada",comisioncobrada)
                  .append("cuentadebito",cuentadebito)
                  .append("autorizacionhost",autorizacionhost)
                  .append("descestatus",descestatus)
                  .append("idtransaccion",idtransaccion)
                  .append("intactualizacion",intactualizacion)
                  .append("montoimpuesto",montoimpuesto)
                  .append("dtoId",dtoId)
                  .append("bsInstalacion", bsInstalacion)
                  .append("bsConvenioempresaach", bsConvenioempresaach)
                  .toString();
   }    
    
    
}
