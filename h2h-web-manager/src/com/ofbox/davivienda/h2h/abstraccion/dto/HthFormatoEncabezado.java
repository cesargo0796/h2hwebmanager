package com.ofbox.davivienda.h2h.abstraccion.dto;
import java.lang.Long;
import java.io.Serializable;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
/**
 * Clase POJO(Plain Old Java Object), que permite almacenar los valores de las propiedades de un objeto de negocio, tambi&eacute;n conocido como DTO(Data Transfer Object).
 * Este DTO es el que viaja entre la capa DAO y la capa de negocios, y que posteriormente es manipulado en la capa de la vista.  
 * 
 * @author Systems Out of the Box
 * @version f3.1 --
 */
 
public class HthFormatoEncabezado implements Serializable{
public static final String lote_destino_LOTE_PE = "PE";
public static final String lote_destino_LOTE_ACH = "ACH";
	/**
     * Atributo que almacena el valor que funciona como llave para un objeto de Negocio.
     */
     private Long idFormato;
    /**
     * Devuelve el objeto que almacena el identificador del DTO.
     * @return
     */
     public Long getDtoId(){
        return idFormato;
     }
    /**
     * Coloca el objeto que almacena el identificador del DTO.
     * @param dtoId objeto que almacena la llave que identifica al DTO. 
     */
     public void setDtoId(Long dtoId){
     	this.idFormato = dtoId;
     }
    
    /** Atributos que almacenan los valores para las propiedades de un objeto DTO correspondiente a un objeto Negocio. */
     
     private String nombreFormato;
	 
     private String tipoFormato;
	 
     private String tipoSeparador;
	 
     private String caracterSeparador;
	 
     private String loteDestino;
	 
     private String usuarioCreacion;
	 
     private java.util.Date fechaCreacion;
	 
     private String usuarioUltAct;
	 
     private java.util.Date fechaUltAct;
	 



     
    
    /** M&eacute;todos que devuelven y colocan los valores de los atributos respectivos del DTO. */
     
     public Long getIdFormato(){
		return this.idFormato;         
     }

     public void setIdFormato( Long idFormato){
		this.idFormato = idFormato;
     }
     public String getNombreFormato(){
		return this.nombreFormato;         
     }

     public void setNombreFormato( String nombreFormato){
		this.nombreFormato = nombreFormato;
     }
     public String getTipoFormato(){
		return this.tipoFormato;         
     }

     public void setTipoFormato( String tipoFormato){
		this.tipoFormato = tipoFormato;
     }
     public String getTipoSeparador(){
		return this.tipoSeparador;         
     }

     public void setTipoSeparador( String tipoSeparador){
		this.tipoSeparador = tipoSeparador;
     }
     public String getCaracterSeparador(){
		return this.caracterSeparador;         
     }

     public void setCaracterSeparador( String caracterSeparador){
		this.caracterSeparador = caracterSeparador;
     }
     public String getLoteDestino(){
		return this.loteDestino;         
     }

     public void setLoteDestino( String loteDestino){
		this.loteDestino = loteDestino;
     }
     public String getUsuarioCreacion(){
		return this.usuarioCreacion;         
     }

     public void setUsuarioCreacion( String usuarioCreacion){
		this.usuarioCreacion = usuarioCreacion;
     }
     public java.util.Date getFechaCreacion(){
		return this.fechaCreacion;         
     }

     public void setFechaCreacion( java.util.Date fechaCreacion){
		this.fechaCreacion = fechaCreacion;
     }
     public String getUsuarioUltAct(){
		return this.usuarioUltAct;         
     }

     public void setUsuarioUltAct( String usuarioUltAct){
		this.usuarioUltAct = usuarioUltAct;
     }
     public java.util.Date getFechaUltAct(){
		return this.fechaUltAct;         
     }

     public void setFechaUltAct( java.util.Date fechaUltAct){
		this.fechaUltAct = fechaUltAct;
     }



    public boolean equals(Object obj){
        return equals(obj,true);
    }
    
    
    public boolean equals(Object obj, boolean conForaneos) {
    	   if (obj == null) { return false; }
    	   if (obj == this) { return true; }
    	   if (obj.getClass() != getClass()) {
    	     return false;
    	   }
    	   HthFormatoEncabezado otherHthFormatoEncabezado = (HthFormatoEncabezado) obj;
    	   EqualsBuilder eq = new EqualsBuilder();
           eq.append(nombreFormato, otherHthFormatoEncabezado.nombreFormato);
           eq.append(tipoFormato, otherHthFormatoEncabezado.tipoFormato);
           eq.append(tipoSeparador, otherHthFormatoEncabezado.tipoSeparador);
           eq.append(caracterSeparador, otherHthFormatoEncabezado.caracterSeparador);
           eq.append(loteDestino, otherHthFormatoEncabezado.loteDestino);
           eq.append(usuarioCreacion, otherHthFormatoEncabezado.usuarioCreacion);
           eq.append(fechaCreacion, otherHthFormatoEncabezado.fechaCreacion);
           eq.append(usuarioUltAct, otherHthFormatoEncabezado.usuarioUltAct);
           eq.append(fechaUltAct, otherHthFormatoEncabezado.fechaUltAct);
           eq.append(idFormato, otherHthFormatoEncabezado.idFormato);
    	   return eq.isEquals();
    }
    
    
     public int hashCode() {
	     return new HashCodeBuilder()
                  .append(nombreFormato)
                  .append(tipoFormato)
                  .append(tipoSeparador)
                  .append(caracterSeparador)
                  .append(loteDestino)
                  .append(usuarioCreacion)
                  .append(fechaCreacion)
                  .append(usuarioUltAct)
                  .append(fechaUltAct)
                  .append(idFormato)
	              .toHashCode();
   }
    
    
   public String toString() {
     return new ToStringBuilder(this)
                  .append("nombreFormato",nombreFormato)
                  .append("tipoFormato",tipoFormato)
                  .append("tipoSeparador",tipoSeparador)
                  .append("caracterSeparador",caracterSeparador)
                  .append("loteDestino",loteDestino)
                  .append("usuarioCreacion",usuarioCreacion)
                  .append("fechaCreacion",fechaCreacion)
                  .append("usuarioUltAct",usuarioUltAct)
                  .append("fechaUltAct",fechaUltAct)
                  .append("idFormato",idFormato)
                  .toString();
   }    
    
    
}
