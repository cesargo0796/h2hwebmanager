package com.ofbox.davivienda.h2h.abstraccion.dto;

import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.ofbox.f3.app.distribuibles.abstraccion.comunes.validacion.UtileriaDeTiposDeDatos;

/**
 *
 * @author ASUS
 */

public class BsCodigosBanco implements Serializable {

    private static final long serialVersionUID = 1L;
    
    private Long idCodigoBanco;
    //private Long codiBanco;
    private String codigoSwift;
    private String codigoUni;
    private Long codigoTransfer;
    private String status;
    

    /**
     * Devuelve el objeto que almacena el identificador del DTO.
     * @return
     */
     public Long getDtoId(){
        return idCodigoBanco;
     }
    /**
     * Coloca el objeto que almacena el identificador del DTO.
     * @param dtoId objeto que almacena la llave que identifica al DTO. 
     */
     public void setDtoId(Long dtoId){
     	this.idCodigoBanco = dtoId;
     }
    
    /**
     * Atributos que almacenan los DTOs de objetos de negocios que funcionan como objetos foraneos.
     */    		
    private BsRedOperacionBanco bsRedOperacionBanco;
    private BsBancoAch bsBancoAch;   
    
    
	public BsRedOperacionBanco getBsRedOperacionBanco() {
		if(bsRedOperacionBanco==null){
			bsRedOperacionBanco = new BsRedOperacionBanco();			
		}
		return bsRedOperacionBanco;
	}
	public void setBsRedOperacionBanco(BsRedOperacionBanco bsRedOperacionBanco) {
		this.bsRedOperacionBanco = bsRedOperacionBanco;
	}
    
    
	public BsBancoAch getBsBancoAch() {
		if(bsBancoAch==null){
			bsBancoAch = new BsBancoAch();			
		}
		return bsBancoAch;
	}
	public void setBsBancoAch(BsBancoAch bsBancoAch) {
		this.bsBancoAch = bsBancoAch;
	}
	
	
	/**   Metodos que manejan los identificadores de los DTO foraneos */ 
    /**
     * Devuelve el atributo llave del DTO foraneo.
     * Verifica si es null antes de devolverlo.
     * @return llave del DTO del negocio maestro
     */
	public Long getIdRedOperacion(){
		if(bsRedOperacionBanco!=null){
        	return bsRedOperacionBanco.getIdRedOperacion();
		}
		return null;        	         
	}
    /**
     * Coloca el valor de la llave en el atributo espec&iacute;fico del DTO foraneo,
	 * crea una nueva instancia del objeto foraneo si este es nulo.
     * @param id Objeto que almacena la llave de un DTO que referencia a un negocio maestro.
     */
    public void setIdRedOperacion( Long idRedOperacion){
		if(bsRedOperacionBanco != null && UtileriaDeTiposDeDatos.isEquals(idRedOperacion, bsRedOperacionBanco.getIdRedOperacion()))
    		return;
			
		if(bsRedOperacionBanco==null || bsRedOperacionBanco.getIdRedOperacion()!=null){
			bsRedOperacionBanco = new BsRedOperacionBanco();			
		}         
		bsRedOperacionBanco.setIdRedOperacion(idRedOperacion);   
    }
	
	public Long getIdCodigoBanco() {
		return idCodigoBanco;
	}
	public void setIdCodigoBanco(Long idCodigoBanco) {
		this.idCodigoBanco = idCodigoBanco;
	}
	public Long getCodiBanco() {
		if(bsBancoAch!=null){
        	return bsBancoAch.getCodBanco();
		}
		return null; 
	}
	public void setCodiBanco(Long codiBanco) {
		if(bsBancoAch != null && UtileriaDeTiposDeDatos.isEquals(codiBanco, bsBancoAch.getCodBanco()))
    		return;
			
		if(bsBancoAch==null || bsBancoAch.getCodBanco()!=null){
			bsBancoAch = new BsBancoAch();			
		}         
		bsBancoAch.setCodBanco(codiBanco);
	}
	public String getCodigoSwift() {
		return codigoSwift;
	}
	public void setCodigoSwift(String codigoSwift) {
		this.codigoSwift = codigoSwift;
	}
	public String getCodigoUni() {
		return codigoUni;
	}
	public void setCodigoUni(String codigoUni) {
		this.codigoUni = codigoUni;
	}
	public Long getCodigoTransfer() {
		return codigoTransfer;
	}
	public void setCodigoTransfer(Long codigoTransfer) {
		this.codigoTransfer = codigoTransfer;
	}
//	public Long getIdRedOperacion() {
//		return idRedOperacion;
//	}
//	public void setIdRedOperacion(Long idRedOperacion) {
//		this.idRedOperacion = idRedOperacion;
//	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
    public boolean equals(Object obj){
        return equals(obj,true);
    }
    
    
    public boolean equals(Object obj, boolean conForaneos) {
    	   if (obj == null) { return false; }
    	   if (obj == this) { return true; }
    	   if (obj.getClass() != getClass()) {
    	     return false;
    	   }
    	   BsCodigosBanco bsCodigosBanco = (BsCodigosBanco) obj;
    	   EqualsBuilder eq = new EqualsBuilder();
           eq.append(idCodigoBanco, bsCodigosBanco.idCodigoBanco);
           eq.append(codigoSwift, bsCodigosBanco.codigoSwift);
           eq.append(codigoUni, bsCodigosBanco.codigoUni);
           eq.append(codigoTransfer, bsCodigosBanco.codigoTransfer);
           eq.append(status, bsCodigosBanco.status);
           if(conForaneos){
               eq.append(bsRedOperacionBanco, bsCodigosBanco.bsRedOperacionBanco);
               eq.append(bsBancoAch, bsCodigosBanco.bsBancoAch);
           }
    	   return eq.isEquals();
    }
    
    
     public int hashCode() {
	     return new HashCodeBuilder()
                  .append(idCodigoBanco)
                  .append(bsBancoAch)
                  .append(codigoSwift)
                  .append(codigoUni)
                  .append(codigoTransfer)
                  .append(status)
                  .append(bsRedOperacionBanco)
	              .toHashCode();
   }
    
    
   public String toString() {
     return new ToStringBuilder(this)
                  .append("idCodigoBanco",idCodigoBanco)
                  .append("bsBancoAch",bsBancoAch)
                  .append("codigoSwift",codigoSwift)
                  .append("codigoUni",codigoUni)
                  .append("codigoTransfer",codigoTransfer)
                  .append("status",status)
                  .append("bsRedOperacionBanco",bsRedOperacionBanco)
                  .toString();
   }    
	
}
