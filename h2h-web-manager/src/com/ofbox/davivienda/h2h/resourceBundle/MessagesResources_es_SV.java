package com.ofbox.davivienda.h2h.resourceBundle;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.dependencias.Recursos;


import java.util.Enumeration;
import java.util.Hashtable;

public class MessagesResources_es_SV extends Recursos {
	private static Hashtable<String, String> properties = null;

	@Override	public Enumeration<String> getKeys() {
		return properties.elements();
	}

	@Override	protected Object handleGetObject(String key) {
		if(properties == null){
			inicializar();
		}
		return properties.get(key);
	}

	protected void inicializar() {
		properties = new Hashtable<String, String>();
		System.out.println("a inicializar properties");
		cargarProperties(properties,
				"es", "SV",
				new String[]{
		        "com.ofbox.davivienda.h2h.resourceBundle.props.HthParametroGeneral",
		        "com.ofbox.davivienda.h2h.resourceBundle.props.BsCliente",
		        "com.ofbox.davivienda.h2h.resourceBundle.props.HthFormato",
		        "com.ofbox.davivienda.h2h.resourceBundle.props.BsConvenioempresa",
		        "com.ofbox.davivienda.h2h.resourceBundle.props.BsDetallepe",
		        "com.ofbox.davivienda.h2h.resourceBundle.props.BsConvenioempresaach",
		        "com.ofbox.davivienda.h2h.resourceBundle.props.HthAtributoDetalle",
		        "com.ofbox.davivienda.h2h.resourceBundle.props.HthFormatoEncabezado",
		        "com.ofbox.davivienda.h2h.resourceBundle.props.HthAtributoEncabezado",
		        "com.ofbox.davivienda.h2h.resourceBundle.props.BsInstalacion",
		        "com.ofbox.davivienda.h2h.resourceBundle.props.HthVistaConvenio",
		        "com.ofbox.davivienda.h2h.resourceBundle.props.HthBitacoraProceso",
		        "com.ofbox.davivienda.h2h.resourceBundle.props.BsLoteach",
		        "com.ofbox.davivienda.h2h.resourceBundle.props.BsDetalleach",
		        "com.ofbox.davivienda.h2h.resourceBundle.props.BsConvenio",
		        "com.ofbox.davivienda.h2h.resourceBundle.props.HthParametroSistema",
		        "com.ofbox.davivienda.h2h.resourceBundle.props.BsEmpresaodepto",
		        "com.ofbox.davivienda.h2h.resourceBundle.props.BsLotepe",
		        "com.ofbox.davivienda.h2h.resourceBundle.props.HthProcesoMonitoreo",
		        "com.ofbox.davivienda.h2h.resourceBundle.props.BsConvenioach",
		        "com.ofbox.davivienda.h2h.resourceBundle.props.HthAtributoFormato",
		        "com.ofbox.davivienda.h2h.resourceBundle.props.BsCodigosBanco",
		        "com.ofbox.davivienda.h2h.resourceBundle.props.BsRedOperacionBanco",
		        "com.ofbox.davivienda.h2h.resourceBundle.props.BsBancoAch",
				"com.ofbox.davivienda.h2h.resourceBundle.props.Globales"});
	}
}
