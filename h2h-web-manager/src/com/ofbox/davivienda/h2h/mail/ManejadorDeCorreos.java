package com.ofbox.davivienda.h2h.mail;


import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;
import javax.naming.InitialContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
 

public class ManejadorDeCorreos {
	
	Logger log = LoggerFactory.getLogger(ManejadorDeCorreos.class);
	
	private String nombreJndiMailSession;

	public ManejadorDeCorreos(String mailSessionJndi) {
		super();
		this.nombreJndiMailSession = mailSessionJndi;
	}

	public void sendMail(Correo correo) throws Exception{
		log.debug("Invocacion de envio de correo electronico...");
		
		InitialContext context= new InitialContext();
		
		log.debug("Configuracion JNDI para la obtencion de javax.mail.Session: [{}] ", nombreJndiMailSession);
		Session session= (Session) context.lookup(nombreJndiMailSession);
		MimeMessage message = new MimeMessage(session);
		
		message.setFrom(new InternetAddress(correo.getRemitente()));
		for(String r : correo.getDestinatarios()){
			log.debug("Recipiente de correo:  [{}]", r);
			message.addRecipient(RecipientType.TO,new InternetAddress(r));
		}
		
		message.setSubject(correo.getAsunto());
		
		if(correo.isHtml()){
			message.setContent(correo.getMensaje(), "text/html; charset=utf-8");
		}else{
			message.setText(correo.getMensaje());	
		}
			
		log.debug("Iniciando envio de correo electronico ...");
		Transport.send(message);
		log.debug("Correo electronico enviado...");
		
	}	
	
}
