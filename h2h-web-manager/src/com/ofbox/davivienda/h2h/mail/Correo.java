package com.ofbox.davivienda.h2h.mail;

import java.util.ArrayList;
import java.util.List;

public class Correo {

	private String remitente;
	private String asunto;
	private String mensaje;
	private String destinoUnico;
	private List<String> destinatarios;
	private boolean html;
	
	public Correo(){
		
	}
	
	public void agregarDestinatarios(String r){
		if(destinatarios == null){
			destinatarios = new ArrayList<String>();
		}
		destinatarios.add(r);
	}

	public String getRemitente() {
		return remitente;
	}

	public void setRemitente(String remitente) {
		this.remitente = remitente;
	}

	public String getAsunto() {
		return asunto;
	}

	public void setAsunto(String asunto) {
		this.asunto = asunto;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public String getDestinoUnico() {
		return destinoUnico;
	}

	public void setDestinoUnico(String destinoUnico) {
		this.destinoUnico = destinoUnico;
	}

	public List<String> getDestinatarios() {
		return destinatarios;
	}

	public void setDestinatarios(List<String> destinatarios) {
		this.destinatarios = destinatarios;
	}

	public boolean isHtml() {
		return html;
	}

	public void setHtml(boolean html) {
		this.html = html;
	}
	
	
}
