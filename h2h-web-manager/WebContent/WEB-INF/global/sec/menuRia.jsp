<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<c:set var="path" value="${pageContext.request.servletPath}"/>
<c:set var="theme" value="${initParam.theme}"/>
<c:set var="ui" value="${initParam.ui}"/>

<c:set var="themecss" value="jquery-ui-custom.css"/>
<html>
<head>
	<title>Menu</title>
	<link rel="stylesheet" type="text/css" href="${ctx}/include/themes/${theme}/${themecss}">
	
	<script language="javascript" type="text/javascript" src="${ctx}/include/scripts/html/main.js"> </script>
	<script type="text/javascript" language="javascript" src="${ctx}/include/scripts/ria/jquery.js"></script>
	<script type="text/javascript" language="javascript" src="${ctx}/include/scripts/ria/jquery-ui-1.8.13.custom.min.js"></script>
	<script type="text/javascript" language="javascript" src="${ctx}/include/scripts/ria/jquery.accmenu.js"></script>
	
	
	
	<script type="text/javascript">
	$(document).ready(function($){
		$('#menulist').accmenu({keepOpen:false, startingOpen: false});
	});
	</script>
	
	<style type="text/css">
		p a:hover{ color: #000000;}
		#menulist{
			-moz-border-radius:5px;
			-webkit-border-radius:5px;
			border-radius:5px;
			/*-webkit-box-shadow:1px 1px 3px #888;*/
			/*-moz-box-shadow:1px 1px 3px #888;*/
		}
		#menulist li{border-bottom:1px solid #FFF;}
		#menulist ul li, #menulist li:last-child{border:none}
		#menulist ul li li{padding-left: 10px;}
		a{
			display:block;
			color:#FFF;
			text-decoration:none;
			font-family:'Helvetica', Arial, sans-serif;
			font-size:13px;
			padding:3px 5px;
			text-shadow:1px 1px 1px #325179;
		}
		#menulist a:hover{
			color:#F9B855;
			-webkit-transition: color 0.2s linear;
		}
		#menulist ul a{
			width: inherit;
			background-color:#CDCDCD;
			height: 20px;
			line-height: 20px;
			text-decoration: none;
			font-size: 11px;
			white-space: pre-wrap; /* css-3 */
			white-space: -moz-pre-wrap; /* Mozilla, since 1999 */
			white-space: -pre-wrap; /* Opera 4-6 */
			white-space: -o-pre-wrap; /* Opera 7 */
			word-wrap: break-word; /* Internet Explorer 5.5+ */
		}
		#menulist ul a:hover{
			background-color:#FFF;
			color:#000000;
			text-shadow:none;
			-webkit-transition: color, background-color 0.2s linear;
		}
		ul{
			display:block;
			background-color:#000000;
			margin:0;
			padding:0;
			width:180px;
			list-style:none;
		}
		#menulist ul{background-color:#6594D1; }
		#menulist li ul {display:none;}
		#menulist li ul li{padding: 0px 0px 0px 0px; list-style: none; }
	</style>
</head>
<body>

<div class="ui-widget ui-widget-content ui-state-default" style="font-size: 0.7em; width: 180px;">
	<span class="ui-icon ui-icon-wrench" style="float: left; margin-right: 3px;"><a title="Actualizar Informaci&oacute;n Usuario" href="${ctx}/app/usuario/actualizarInformacion/${ui}" class="ui-icon ui-icon-wrench" onclick="doSubmit('usuario/actualizarInformacion');" target="main" ></a></span>
	<span style="font-weight: bolder; margin-top: 2px;"> Usuario </span> <br />
	<span style="margin-left: 8px;"> <c:out value="${seguridadUsuarioAplicacion.nombre}"/> </span>
</div>

	
<br />
<ul id="menulist" class="accordion">
	<li style="text-align: center;">  <a href="#">  Opciones </a> </li>
	<c:set scope="request" var="itemsDeMenu" value="${itemsDeMenu}"  />
	<jsp:include page="recursiveMenuRia.jsp"></jsp:include>
	<li style="text-align: center;">  <a href="#" onclick="$('#logoutForm').submit();">  Salir </a> </li>
</ul>




<!-- Create Menu Settings: (Menu ID, Is Vertical, Show Timer, Hide Timer, On Click ('all' or 'lev2'), Right to Left, Horizontal Subs, Flush Left, Flush Top) -->

	<br>
	<form id="logoutForm" name="logoutForm" target="_parent" method="post" action="${ctx}/inicio/ria">
		<input type="hidden" name="action" value="logout"/>
	</form>
</body>
</html>