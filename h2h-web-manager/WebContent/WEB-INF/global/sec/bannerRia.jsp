<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"       uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn"     uri="http://java.sun.com/jsp/jstl/functions" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<c:set var="theme" value="${initParam.theme}"/>
<c:set var="themecss" value="jquery-ui-custom.css"/>
<html> 
<head>  
	<title>Colecturia del Fondo Social para la Vivienda</title> 
	<link href="include/styles/ria/banner.css" rel="stylesheet" type="text/css">
	<c:if test="${not fn:contains(header['User-Agent'],'MSIE')}">
	<link rel="stylesheet" type="text/css" href="${ctx}/include/themes/${theme}/${themecss}"/>
	</c:if>
	<c:if test="${fn:contains(header['User-Agent'],'MSIE')}">
	<link rel="stylesheet" type="text/css" href="${ctx}/include/themes/graymint/${themecss}"/>
	</c:if>
	<style>
		body{
			background-image: url('${ctx}/include/images/banner_bg.png');
			background-repeat: repeat-x; 
		}
		.ui-widget{
			font-size: 85% !important;
		}
	</style>
</head> 
<body topmargin="0" leftmargin="0"> 
	
		<div class="ui-widget ui-corner-bottom ui-widget-content ui-state-highlight" align="left" 
		style="margin-left: 8px; margin-right: 8px; height: 73px; background-image:url('${ctx}/include/images/banner_back.png'); " >
		<img src="${ctx}/include/images/banner.png"/>
		</div>
	
</body> 
</html>