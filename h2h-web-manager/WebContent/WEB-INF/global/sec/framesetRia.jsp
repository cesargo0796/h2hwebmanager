<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"       uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt"     uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page import="java.util.Iterator"%><html>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>

<head>
	<link rel="shortcut icon" href="${ctx}/include/images/favicon.ico"/>
	<title>Administracion de Procesos Sistema Host to Host para Banca Empresa Plus</title>
<META HTTP-EQUIV="Cache-Control" CONTENT="no-cache">
<META HTTP-EQUIV="Cache-Control" CONTENT="no-store">
<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
<META HTTP-EQUIV="Expires" CONTENT="-1">

<script type="text/javascript" language="javascript" src="${ctx}/include/scripts/ria/jquery.js"></script>

<script>

	function loaded(){
		$.ajax({
			  async : false,
			  url: '${ctx}/inicio/',
			  data: $.param({
				  sesion: "isActiva"
			  }),
			  context: document.mainForm,
			  type: "POST",
	          dataType: "json",
			  success: function(data){
			  		if(!data.valor){
			  			window.location.href =	'${ctx}/inicio/redirect/ria';
			  		}
			  }
			});
	}
</script>
</head>

<frameset onload="loaded();" rows="*" cols="*" frameborder="NO" border="0" framespacing="0">
  <%-- <frame src="${ctx}/inicio/banner/ria" name="banner" scrolling="NO" noresize > --%>
  <frame src="${ctx}/inicio/blank/ria" name="main">
</frameset>

<noframes>
	<body class="fondo" ></body>
</noframes>
</html>