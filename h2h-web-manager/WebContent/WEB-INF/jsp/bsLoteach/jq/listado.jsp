<%@ taglib prefix="c"       uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt"     uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:if test="${ not empty locale}">
	<fmt:setLocale value="${ locale }" />
</c:if>
<fmt:bundle basename="com.ofbox.davivienda.h2h.resourceBundle.MessagesResources">

<c:if test="${ not empty bsLoteachEstado.enlaceBusqueda.enllavador}">
	<c:set var="busquedaEnllavador" value="${bsLoteachEstado.enlaceBusqueda.enllavador}"/>	
</c:if>	
<c:if test="${ not empty bsLoteachEstado.enlaceDetalle}">
	<c:set var="detalle" value="${bsLoteachEstado.enlaceDetalle}"/>	
</c:if>	
<c:if test="${bsLoteachEstado.ordenamientoAplicado}">
	<c:set var="orden" value="${bsLoteachEstado.ordenamiento}"/>	
</c:if>		
<c:if test="${not empty bsLoteachSeg}">
	<c:set var="restriccion" value="${bsLoteachSeg}"/>
</c:if>

<c:set var="nombre" value="bsLoteach"/>
<c:set var="ubicacion" value="listado"/>

<% int counter = 0; %>
<%@ include file="/WEB-INF/global/jsp/jq-header.jsp" %>
<!-- Elementos no visibles -->	
	
<form  name="mainForm" id="mainForm" action="${NewtPageRenderData.actionUrl}" method="post">

<div id="mainContent">

<div class="breadCrumbHolder module" style="margin-bottom: 3px;">
<div id="breadcrumb" class="breadCrumb module ui-widget ui-widget-content" style="width: 99%;">
<ul>
	<li><a href="${ctx}/inicio/blank/ria"> HOME </a> </li>
<c:if test="${ not empty ubicacionAplicacion.ruta }">
	<input type="hidden" name="idUbicacionAplicacionActual" id="idUbicacionAplicacionActual" value="${ ubicacionAplicacion.idUbicacionAplicacionActual }"/>
	<c:forEach items="${ ubicacionAplicacion.ruta }" var="ub">
		<li><a href="javascript: doSubmitBreadcrumb('<c:out value="${ ub.submit }"/>', '<c:out value="${ ub.id }"/>')"><c:out value="${ ub.label }" escapeXml="false" /></a>  </li> 
	</c:forEach>			
</c:if>
	<li><fmt:message key="bsLoteach.bc.etiqueta.principal" /></li>
</ul>
</div>
	<input type="hidden" name="id_ubicacion_retorno" id="id_ubicacion_retorno" value=""/>
</div>

<c:if test="${ not empty detalle.cabecera}">
    <jsp:include page="/WEB-INF/jsp/${detalle.cabecera.locacion}/jq/cabecera.jsp" />
</c:if>

<%-- <div id="iTitle" class="ui-state-default ui-widget-content ui-corner-all">
	<fmt:message key="bsLoteach.listado.etiqueta.titulo"/>
</div>--%>
	<input type="hidden" id="maxRowNum" name="maxRowNum" value="<c:out value="${navegacion.registroFinal - 1}"/>">
	<input type="hidden" id="pageLoc" name="pageLoc" value="listado"/>
    <input type="hidden" id="eag" name="eag" value="${eag}"/>
	<c:if test="${ not empty busquedaEnllavador}">
	<input type="hidden" id="listadoLov" value="bsLoteach"/>
	</c:if>
	<c:if test="${ not empty esAsync}">
	<input type="hidden" id="esAsync" value="${esAsync}"/>
	</c:if>
    <input type="hidden" id="movimientoPagina" name="movimientoPagina"/>
    <input type="hidden" id="columna" name="columna"/>
    <input type="hidden" id="fenixf31seguridad" name="fenixf31seguridad" value="<c:out value='${fenixf31seguridad}' />" />
	<!-- valores de llaves -->
    <input type="hidden" alt="notForSubmit" id="codlote" name="codlote" value=""/>
    <input type="hidden" alt="notForSubmit" id="instalacion" name="instalacion" value=""/>
    <input type="hidden" alt="notForSubmit" id="lote" name="lote" value=""/>
	 <input type="hidden" id="expo" name="expo" value=""/>  



<%@ include file="/WEB-INF/global/jsp/mensajes-globales.jsp" %>

<c:if test="${ not empty busquedaEnllavador}">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em; font-size: 85%;"> 
	<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
 	<strong><fmt:message key="globales.listado.etiqueta.tipLov.titulo"/></strong>&nbsp;<fmt:message key="globales.listado.etiqueta.tipLov.mensaje"/>
	</p>
</div>
</c:if>
<!-- botones de acciones basicas -->
<div id="action-buttons">
<div id="action-bar">
<c:if test="${ empty busquedaEnllavador}">
	<c:if test="${restriccion.verDisponible}">
	<button id="btnVer" type="button" class="ui-button ui-corner-all ui-button-color" name="ver" onClick="if(isValueSelected()){ doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsLoteach/ver'); }"><fmt:message key="globales.listado.boton.ver"/></button>
	</c:if>
	<c:if test="${restriccion.crearDisponible}">
	<button id="btnCrear" type="button" class="ui-button ui-corner-all ui-button-color" name="crear" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsLoteach/crear');"><fmt:message key="globales.listado.boton.crear"/></button>
	</c:if>
	<c:if test="${restriccion.actualizarDisponible}">	
	<button id="btnActualizar" type="button" class="ui-button ui-corner-all ui-button-color" name="actualizar" onClick="if(isValueSelected()){ doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsLoteach/actualizar'); }"><fmt:message key="globales.listado.boton.actualizar"/></button>
	</c:if>
	<c:if test="${restriccion.eliminarDisponible}">
	<button id="btnEliminar" type="button" class="ui-button ui-corner-all ui-button-color" name="eliminar" onClick="if(isValueSelected()){$('#dialog-confirm').dialog('open');}"><fmt:message key="globales.listado.boton.eliminar"/></button>
	</c:if>
	</c:if>
<c:if test="${ not empty busquedaEnllavador}">
	<c:if test="${restriccion.crearDisponible}">
	<button id="btnCrear" type="button" class="ui-button ui-corner-all ui-button-color" name="crear" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsLoteach/crear');"><fmt:message key="globales.listado.boton.crear"/></button>
	</c:if>
	<button id="btnSeleccionar" type="button" class="ui-button ui-corner-all ui-button-color" name="seleccionar" onClick="if(isValueSelected()){ doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsLoteach/buscarSeleccionar'); }"><fmt:message key="globales.listado.boton.seleccionar"/></button>
	<button id="btnCancelar" type="button" class="ui-button ui-corner-all ui-button-color" name="cancel" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsLoteach/cancelarBuscar');"><fmt:message key="globales.listado.boton.cancelar"/></button>
</c:if>
	<c:if test="${restriccion.filtrarDisponible}">	
	<button id="btnFiltrar" type="button" class="ui-button ui-corner-all ui-button-color" name="filter" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsLoteach/filtrar');"><fmt:message key="globales.listado.boton.filtrar"/></button>
	</c:if>	
	
</div>
<div id="navigation-bar">	
<c:if test="${navegacion.paginaAnterior}">				
	<button id="btnAnterior" type="button" name="anterior" onClick="$('#movimientoPagina').val('anterior');doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsLoteach/listado')"><fmt:message key="globales.listado.boton.anterior"/></button>
</c:if>
<c:if test="${!navegacion.paginaAnterior}">			
	<button id="btnAnterior" type="button" name="anterior" disabled="disabled"><fmt:message key="globales.listado.boton.anterior"/></button>
</c:if>
<c:if test="${not empty navegacion.listadoPaginas}">
<c:forEach items="${navegacion.listadoPaginas}" var="p">
	<c:if test="${p == navegacion.noPaginaActual}">
	<button id="pag<c:out value="${p}" />" class="ui-button-text ui-state-highlight" type="button" name="pag<c:out value="${p}" />" disabled="disabled"><c:out value="${p}" /></button>
	</c:if>
	<c:if test="${p != navegacion.noPaginaActual}">
	<button id="pag<c:out value="${p}" />"  onClick="$('#movimientoPagina').val('${p}');doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsLoteach/listado')" type="button" name="pag<c:out value="${p}"/>"><c:out value="${p}" /></button>	
	</c:if> 
</c:forEach>
</c:if>
<c:if test="${navegacion.paginaSiguiente}">			
	<button id="btnSiguiente" type="button" name="siguiente" onClick="$('#movimientoPagina').val('siguiente');doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsLoteach/listado')" ><fmt:message key="globales.listado.boton.siguiente"/></button>
</c:if>
<c:if test="${!navegacion.paginaSiguiente}">			
	<button id="btnSiguiente" type="button" name="siguiente" disabled="disabled"><fmt:message key="globales.listado.boton.siguiente"/></button>
</c:if>
	
</div>
</div>
<br/>

<div class="ui-widget fg-toolbar ui-toolbar ui-widget-header ui-corner-tl ui-corner-tr ui-helper-clearfix">

<c:if test="${ not empty orden}">
	<div style="display:block; float: left;">
		<button id="btnRemoverOrden" type="button" class="ui-button ui-corner-all ui-button-color" name="cancel" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsLoteach/removerOrdenamiento');"><fmt:message key="globales.listado.boton.removerOrden"/></button>
	</div>
</c:if>
<c:if test="${navegacion.filtroAplicado}">
	<div style="display:block; float: left;">
		<button id="btnRemoverFiltro" type="button" class="ui-button ui-corner-all ui-button-color" name="nofilter" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsLoteach/removerFiltrar');"><fmt:message key="globales.listado.boton.removerFiltro"/></button>
	</div>			
</c:if>
	<div id="crudTable_filter" style="display: block; text-align: right; float: right; margin-top: 6px; margin-bottom: 6px;"> 
		<fmt:message key="globales.listado.etiqueta.busquedaRapida"/>
		<select id="campoBusqueda" name="campoBusqueda" onchange="doBusquedaRapida('#busqueda', this.value, $('option:selected', this));" class="ui-widget ui-widget-content ui-state-default">
			<option value="convenio"><fmt:message key="bsLoteach.listado.etiqueta.Convenio"/></option>
			<option value="tipolote"><fmt:message key="bsLoteach.listado.etiqueta.Tipolote"/></option>
			<option value="numoperaciones"><fmt:message key="bsLoteach.listado.etiqueta.Numoperaciones"/></option>
			<option value="nombrelote"><fmt:message key="bsLoteach.listado.etiqueta.Nombrelote"/></option>
			<option value="estatus"><fmt:message key="bsLoteach.listado.etiqueta.Estatus"/></option>
			<option value="usuarioingreso"><fmt:message key="bsLoteach.listado.etiqueta.Usuarioingreso"/></option>
			<option value="autorizaciones"><fmt:message key="bsLoteach.listado.etiqueta.Autorizaciones"/></option>
			<option value="aplicaciondebitohost"><fmt:message key="bsLoteach.listado.etiqueta.Aplicaciondebitohost"/></option>
			<option value="sysProcessid"><fmt:message key="bsLoteach.listado.etiqueta.SysProcessid"/></option>
			<option value="sysLowdatetime"><fmt:message key="bsLoteach.listado.etiqueta.SysLowdatetime"/></option>
			<option value="sysHighdatetime"><fmt:message key="bsLoteach.listado.etiqueta.SysHighdatetime"/></option>
			<option value="ponderacion"><fmt:message key="bsLoteach.listado.etiqueta.Ponderacion"/></option>
			<option value="firmas"><fmt:message key="bsLoteach.listado.etiqueta.Firmas"/></option>
			<option value="token"><fmt:message key="bsLoteach.listado.etiqueta.Token"/></option>
			<option value="comentariorechazo"><fmt:message key="bsLoteach.listado.etiqueta.Comentariorechazo"/></option>
			<option value="descripcion"><fmt:message key="bsLoteach.listado.etiqueta.Descripcion"/></option>
			<option value="estatuspaybank"><fmt:message key="bsLoteach.listado.etiqueta.Estatuspaybank"/></option>
			<option value="archivogenerado"><fmt:message key="bsLoteach.listado.etiqueta.Archivogenerado"/></option>
			<option value="cuentadebito"><fmt:message key="bsLoteach.listado.etiqueta.Cuentadebito"/></option>
			<option value="autorizacionhost"><fmt:message key="bsLoteach.listado.etiqueta.Autorizacionhost"/></option>
			<option value="descestatus"><fmt:message key="bsLoteach.listado.etiqueta.Descestatus"/></option>
			<option value="idtransaccion"><fmt:message key="bsLoteach.listado.etiqueta.Idtransaccion"/></option>
			<option value="intactualizacion"><fmt:message key="bsLoteach.listado.etiqueta.Intactualizacion"/></option>
		</select> &nbsp;
		<input onkeypress="doFiltrarPor('bsLoteach', event)" type="text" id="busqueda" name="busqueda" class="ui-widget ui-widget-content " style="width:150px; height: 18px;"/>
	</div>
</div>
<c:if test="${restriccion.listadoDisponible or not empty busquedaEnllavador}">
<table id="crudTable" width="100%">
	<thead>
		<tr>
  			<th onclick="doSort('convenio','${nombre}')" sort="${orden.mapaDeValores['convenio'].strOrden}"><fmt:message key="bsLoteach.listado.etiqueta.Convenio"/></th>
  			<th onclick="doSort('empresaodepto','${nombre}')" sort="${orden.mapaDeValores['empresaodepto'].strOrden}"><fmt:message key="bsLoteach.listado.etiqueta.Empresaodepto"/></th>
  			<th onclick="doSort('tipolote','${nombre}')" sort="${orden.mapaDeValores['tipolote'].strOrden}"><fmt:message key="bsLoteach.listado.etiqueta.Tipolote"/></th>
  			<th onclick="doSort('montototal','${nombre}')" sort="${orden.mapaDeValores['montototal'].strOrden}"><fmt:message key="bsLoteach.listado.etiqueta.Montototal"/></th>
  			<th onclick="doSort('numoperaciones','${nombre}')" sort="${orden.mapaDeValores['numoperaciones'].strOrden}"><fmt:message key="bsLoteach.listado.etiqueta.Numoperaciones"/></th>
  			<th onclick="doSort('nombrelote','${nombre}')" sort="${orden.mapaDeValores['nombrelote'].strOrden}"><fmt:message key="bsLoteach.listado.etiqueta.Nombrelote"/></th>
  			<th onclick="doSort('fechaenviado','${nombre}')" sort="${orden.mapaDeValores['fechaenviado'].strOrden}"><fmt:message key="bsLoteach.listado.etiqueta.Fechaenviado"/></th>
  			<th onclick="doSort('fecharecibido','${nombre}')" sort="${orden.mapaDeValores['fecharecibido'].strOrden}"><fmt:message key="bsLoteach.listado.etiqueta.Fecharecibido"/></th>
  			<th onclick="doSort('fechaaplicacion','${nombre}')" sort="${orden.mapaDeValores['fechaaplicacion'].strOrden}"><fmt:message key="bsLoteach.listado.etiqueta.Fechaaplicacion"/></th>
  			<th onclick="doSort('fechaestatus','${nombre}')" sort="${orden.mapaDeValores['fechaestatus'].strOrden}"><fmt:message key="bsLoteach.listado.etiqueta.Fechaestatus"/></th>
  			<th onclick="doSort('estatus','${nombre}')" sort="${orden.mapaDeValores['estatus'].strOrden}"><fmt:message key="bsLoteach.listado.etiqueta.Estatus"/></th>
  			<th onclick="doSort('usuarioingreso','${nombre}')" sort="${orden.mapaDeValores['usuarioingreso'].strOrden}"><fmt:message key="bsLoteach.listado.etiqueta.Usuarioingreso"/></th>
  			<th onclick="doSort('autorizaciones','${nombre}')" sort="${orden.mapaDeValores['autorizaciones'].strOrden}"><fmt:message key="bsLoteach.listado.etiqueta.Autorizaciones"/></th>
  			<th onclick="doSort('aplicaciondebitohost','${nombre}')" sort="${orden.mapaDeValores['aplicaciondebitohost'].strOrden}"><fmt:message key="bsLoteach.listado.etiqueta.Aplicaciondebitohost"/></th>
  			<th onclick="doSort('sysMarcatiempo','${nombre}')" sort="${orden.mapaDeValores['sysMarcatiempo'].strOrden}"><fmt:message key="bsLoteach.listado.etiqueta.SysMarcatiempo"/></th>
  			<th onclick="doSort('sysProcessid','${nombre}')" sort="${orden.mapaDeValores['sysProcessid'].strOrden}"><fmt:message key="bsLoteach.listado.etiqueta.SysProcessid"/></th>
  			<th onclick="doSort('sysLowdatetime','${nombre}')" sort="${orden.mapaDeValores['sysLowdatetime'].strOrden}"><fmt:message key="bsLoteach.listado.etiqueta.SysLowdatetime"/></th>
  			<th onclick="doSort('sysHighdatetime','${nombre}')" sort="${orden.mapaDeValores['sysHighdatetime'].strOrden}"><fmt:message key="bsLoteach.listado.etiqueta.SysHighdatetime"/></th>
  			<th onclick="doSort('ponderacion','${nombre}')" sort="${orden.mapaDeValores['ponderacion'].strOrden}"><fmt:message key="bsLoteach.listado.etiqueta.Ponderacion"/></th>
  			<th onclick="doSort('firmas','${nombre}')" sort="${orden.mapaDeValores['firmas'].strOrden}"><fmt:message key="bsLoteach.listado.etiqueta.Firmas"/></th>
  			<th onclick="doSort('fechaaplicacionhost','${nombre}')" sort="${orden.mapaDeValores['fechaaplicacionhost'].strOrden}"><fmt:message key="bsLoteach.listado.etiqueta.Fechaaplicacionhost"/></th>
  			<th onclick="doSort('listoparahost','${nombre}')" sort="${orden.mapaDeValores['listoparahost'].strOrden}"><fmt:message key="bsLoteach.listado.etiqueta.Listoparahost"/></th>
  			<th onclick="doSort('fechahoraprocesado','${nombre}')" sort="${orden.mapaDeValores['fechahoraprocesado'].strOrden}"><fmt:message key="bsLoteach.listado.etiqueta.Fechahoraprocesado"/></th>
  			<th onclick="doSort('token','${nombre}')" sort="${orden.mapaDeValores['token'].strOrden}"><fmt:message key="bsLoteach.listado.etiqueta.Token"/></th>
  			<th onclick="doSort('comentariorechazo','${nombre}')" sort="${orden.mapaDeValores['comentariorechazo'].strOrden}"><fmt:message key="bsLoteach.listado.etiqueta.Comentariorechazo"/></th>
  			<th onclick="doSort('descripcion','${nombre}')" sort="${orden.mapaDeValores['descripcion'].strOrden}"><fmt:message key="bsLoteach.listado.etiqueta.Descripcion"/></th>
  			<th onclick="doSort('estatuspaybank','${nombre}')" sort="${orden.mapaDeValores['estatuspaybank'].strOrden}"><fmt:message key="bsLoteach.listado.etiqueta.Estatuspaybank"/></th>
  			<th onclick="doSort('archivogenerado','${nombre}')" sort="${orden.mapaDeValores['archivogenerado'].strOrden}"><fmt:message key="bsLoteach.listado.etiqueta.Archivogenerado"/></th>
  			<th onclick="doSort('comisioncobrada','${nombre}')" sort="${orden.mapaDeValores['comisioncobrada'].strOrden}"><fmt:message key="bsLoteach.listado.etiqueta.Comisioncobrada"/></th>
  			<th onclick="doSort('cuentadebito','${nombre}')" sort="${orden.mapaDeValores['cuentadebito'].strOrden}"><fmt:message key="bsLoteach.listado.etiqueta.Cuentadebito"/></th>
  			<th onclick="doSort('autorizacionhost','${nombre}')" sort="${orden.mapaDeValores['autorizacionhost'].strOrden}"><fmt:message key="bsLoteach.listado.etiqueta.Autorizacionhost"/></th>
  			<th onclick="doSort('descestatus','${nombre}')" sort="${orden.mapaDeValores['descestatus'].strOrden}"><fmt:message key="bsLoteach.listado.etiqueta.Descestatus"/></th>
  			<th onclick="doSort('idtransaccion','${nombre}')" sort="${orden.mapaDeValores['idtransaccion'].strOrden}"><fmt:message key="bsLoteach.listado.etiqueta.Idtransaccion"/></th>
  			<th onclick="doSort('intactualizacion','${nombre}')" sort="${orden.mapaDeValores['intactualizacion'].strOrden}"><fmt:message key="bsLoteach.listado.etiqueta.Intactualizacion"/></th>
  			<th onclick="doSort('montoimpuesto','${nombre}')" sort="${orden.mapaDeValores['montoimpuesto'].strOrden}"><fmt:message key="bsLoteach.listado.etiqueta.Montoimpuesto"/></th>
		</tr>
	</thead>	
	<tbody>
<c:forEach items="${coleccionResultados}" var="dto">
		<tr onclick="$('#codlote').val('${dto.codlote}');$('#instalacion').val('${dto.instalacion}');$('#lote').val('${dto.lote}'); selectDeSelectObject(this, 'ui-state-active'); sel = true; currentRow = $(this).parent().children().index($(this));">  
		<td><c:out value="${dto.convenio}"/></td>
	    <td><c:out value="${dto.bsConvenioempresaach.convenio}"/></td>
		<td><c:out value="${dto.tipolote}"/></td>
		<td><c:out value="${dto.montototal}"/></td>
		<td><c:out value="${dto.numoperaciones}"/></td>
		<td><c:out value="${dto.nombrelote}"/></td>
		<td><fmt:formatDate pattern="dd-MM-yyyy" value="${dto.fechaenviado}" /></td>
		<td><fmt:formatDate pattern="dd-MM-yyyy" value="${dto.fecharecibido}" /></td>
		<td><fmt:formatDate pattern="dd-MM-yyyy" value="${dto.fechaaplicacion}" /></td>
		<td><fmt:formatDate pattern="dd-MM-yyyy" value="${dto.fechaestatus}" /></td>
		<td><c:out value="${dto.estatus}"/></td>
		<td><c:out value="${dto.usuarioingreso}"/></td>
		<td><c:out value="${dto.autorizaciones}"/></td>
		<td><c:out value="${dto.aplicaciondebitohost}"/></td>
		<td><fmt:formatDate pattern="dd-MM-yyyy" value="${dto.sysMarcatiempo}" /></td>
		<td><c:out value="${dto.sysProcessid}"/></td>
		<td><c:out value="${dto.sysLowdatetime}"/></td>
		<td><c:out value="${dto.sysHighdatetime}"/></td>
		<td><c:out value="${dto.ponderacion}"/></td>
		<td><c:out value="${dto.firmas}"/></td>
		<td><fmt:formatDate pattern="dd-MM-yyyy" value="${dto.fechaaplicacionhost}" /></td>
		<td><fmt:formatDate pattern="dd-MM-yyyy" value="${dto.listoparahost}" /></td>
		<td><fmt:formatDate pattern="dd-MM-yyyy" value="${dto.fechahoraprocesado}" /></td>
		<td><c:out value="${dto.token}"/></td>
		<td><c:out value="${dto.comentariorechazo}"/></td>
		<td><c:out value="${dto.descripcion}"/></td>
		<td><c:out value="${dto.estatuspaybank}"/></td>
		<td><c:out value="${dto.archivogenerado}"/></td>
		<td><c:out value="${dto.comisioncobrada}"/></td>
		<td><c:out value="${dto.cuentadebito}"/></td>
		<td><c:out value="${dto.autorizacionhost}"/></td>
		<td><c:out value="${dto.descestatus}"/></td>
		<td><c:out value="${dto.idtransaccion}"/></td>
		<td><c:out value="${dto.intactualizacion}"/></td>
		<td><c:out value="${dto.montoimpuesto}"/></td>
		</tr>
</c:forEach>
	</tbody>
</table>
</c:if>
<div class="ui-widget fg-toolbar ui-toolbar ui-widget-header ui-corner-bl ui-corner-br ui-helper-clearfix">
	<c:if test="${navegacion.totalRegistros <= 0}">
		<fmt:message key="globales.listado.etiqueta.sinRegistros" />
    </c:if>
    <c:if test="${navegacion.totalRegistros > 0}">
    	<fmt:message key="globales.listado.etiqueta.numRegistros">
    		<fmt:param><c:out value="${navegacion.registroInicial}"/></fmt:param>
    		<fmt:param><c:out value="${navegacion.registroFinal}"/></fmt:param>
    		<fmt:param><c:out value="${navegacion.totalRegistros}"/></fmt:param>
    	</fmt:message>
    </c:if>	
</div>
<br/>
</div>
</form>
<%@ include file="/WEB-INF/global/jsp/jq-footer.jsp" %>
</fmt:bundle>