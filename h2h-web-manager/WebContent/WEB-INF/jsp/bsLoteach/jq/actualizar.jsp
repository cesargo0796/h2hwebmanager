<%@ taglib prefix="c"       uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt"     uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:if test="${ not empty locale}">
	<fmt:setLocale value="${ locale }" />
</c:if>
<fmt:bundle basename="com.ofbox.davivienda.h2h.resourceBundle.MessagesResources">
	
<c:if test="${ not empty bsLoteachEstado.enlaceDetalle}">
	<c:set var="detalle" value="${bsLoteachEstado.enlaceDetalle}"/>	
	<c:set var="detalleEnllavador" value="${detalle.enllavador}"/>
</c:if>		

<c:set var="nombre" value="bsLoteach"/>
<c:set var="ubicacion" value="actualizar"/>	
<c:set var="primerCampo" value="convenio"/>
<%@ include file="/WEB-INF/global/jsp/jq-header.jsp" %>
	
<form  name="mainForm" id="mainForm" action="${NewtPageRenderData.actionUrl}" method="post">
<c:if test="${ not empty detalle.cabecera}">
    <jsp:include page="/WEB-INF/jsp/${detalle.cabecera.locacion}/jq/cabecera.jsp" />
</c:if>
<div id="mainContent">

	<div class="breadCrumbHolder module" style="margin-bottom: 3px;">
<div id="breadcrumb" class="breadCrumb module ui-widget ui-widget-content" style="width: 99%;">
<ul>
	<li><a href="${ctx}/inicio/blank/ria"> HOME </a> </li>
<c:if test="${ not empty ubicacionAplicacion.ruta }">
	<input type="hidden" name="idUbicacionAplicacionActual" id="idUbicacionAplicacionActual" value="${ ubicacionAplicacion.idUbicacionAplicacionActual }"/>
	<c:forEach items="${ ubicacionAplicacion.ruta }" var="ub">
		<li><a href="javascript: doSubmitBreadcrumb('<c:out value="${ ub.submit }"/>', '<c:out value="${ ub.id }"/>')"><c:out value="${ ub.label }" escapeXml="false" /></a>  </li> 
	</c:forEach>			
</c:if>
	<li><fmt:message key="bsLoteach.bc.etiqueta.actualizar" /></li>
</ul>
</div>
	<input type="hidden" name="id_ubicacion_retorno" id="id_ubicacion_retorno" value=""/>
</div>	
	
<div id="iProcessing" style="visibility: hidden" align="center">
	<img src="${ctx}/include/images/design/processing_request.gif"></img>
</div>

<%--<div id="iTitle" class="ui-state-default ui-widget-content ui-corner-all">
	<fmt:message key="bsLoteach.formulario.etiqueta.actualizar.titulo"/>
</div> --%>
	<input type="hidden" id="pageLoc" name="pageLoc" value="actualizar"/>
	<input type="hidden" id="primerCampo" name="primerCampo" value="convenio"/>
    <input type="hidden" id="eag" name="eag" value="${eag}"/>
    <input type="hidden" id="movimientoPagina" name="movimientoPagina"/>
    <input type="hidden" id="columna" name="columna"/>
    <input type="hidden" id="fenixf31seguridad" name="fenixf31seguridad" value="<c:out value='${fenixf31seguridad}' />" />
	<c:if test="${not empty scrollTo }">
    <input type="hidden" id="scrollSmooth" value="<c:out value='${scrollTo}' />" />
    </c:if>
	<!-- valores de llaves -->
	<input type="hidden" id="codlote" name="codlote" value="${bsLoteach.codlote}"/>	
	<input type="hidden" id="instalacion" name="instalacion" value="${bsLoteach.instalacion}"/>	
	<input type="hidden" id="lote" name="lote" value="${bsLoteach.lote}"/>	
	
	<input type="hidden" id="nombreBusqueda" name="nombreBusqueda"/>


<!-- mensajes de error-->
<c:if test="${mensajesUsuario.erroresGlobales}">
<div class="ui-state-error ui-corner-all" style="padding: 0 .7em; font-size: 0.8em;"> 
	<span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span> 
	<strong><fmt:message key="globales.formulario.etiqueta.error"/></strong> <br /> 
	<ul>
		<c:forEach items="${mensajesUsuario.mensajesDeError}" var="mensaje">
        	<li><c:out value="${mensaje}" escapeXml="false"/></li>
     	</c:forEach>
	</ul>
</div>
</c:if>

<div id="action-buttons">
<div id="action-bar">
	<button tabindex="36" id="btnGuardar" type="submit" name="actualizar" onClick="doSubmit<c:if test="${ not empty detalle.cabecera}">Async</c:if>('bsLoteach/actualizarEjecutar');"><fmt:message key="globales.formulario.boton.actualizar.guardar"/></button>
	<button tabindex="37" id="btnCancelar" type="button" name="cancel" onClick="doSubmit<c:if test="${ not empty detalle.cabecera}">Async</c:if>('bsLoteach/cancelarActualizar');"><fmt:message key="globales.formulario.boton.actualizar.cancelar"/></button>
</div>
</div>
<br />

<!-- datos de formulario -->
<div><span style="font-size: small;">-Los campos marcados con * son obligatorios</span></div>
<div class="ui-widget ui-widget-content ui-corner-all" style="width: 99%">
		<div id="formInputHeader" class="ui-widget ui-widget-header ui-corner-top" style="padding-left: 5px;"> 
			<fmt:message key="bsLoteach.formulario.etiqueta.actualizar.titulo"/> 
		</div>
		<div style="display: inline;" id="formInput">
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['convenio']}">	
		<div class="row">
			<span class="label" style="color: #AA0707;"> 
				<fmt:message key="bsLoteach.formulario.etiqueta.Convenio"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="1" alt="integer" type="text" id="convenio" name="convenio"  value="${bsLoteach.convenio}" size="10" maxlength="10"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsLoteach/ayudaPropiedadJson','convenio')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['convenio'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['empresaodepto']}">	
		<div class="row">
			<span class="label" style="color: #AA0707;"> 
				<fmt:message key="bsLoteach.formulario.etiqueta.Empresaodepto"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
	            <input readonly="readonly" type="text" id="empresaodepto" name="empresaodepto" value="${bsLoteach.empresaodepto}"  size="10"  />
				<input readonly="readonly" type="text" id="bsConvenioempresaach.convenio" name="bsConvenioempresaach.convenio" value="${bsLoteach.bsConvenioempresaach.convenio}"  size="10"  />
				<button tabindex="2" id="btnbsConvenioempresaach" class="sq" onClick="doForaneo('bsConvenioempresaach', 'bsLoteach/buscar');"><fmt:message key='globales.formulario.boton.lov'/></button>

			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsLoteach/ayudaPropiedadJson','empresaodepto')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['empresaodepto'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['tipolote']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsLoteach.formulario.etiqueta.Tipolote"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="3" alt="integer" type="text" id="tipolote" name="tipolote"  value="${bsLoteach.tipolote}" size="10" maxlength="3"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsLoteach/ayudaPropiedadJson','tipolote')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['tipolote'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['montototal']}">	
		<div class="row">
			<span class="label" style="color: #AA0707;"> 
				<fmt:message key="bsLoteach.formulario.etiqueta.Montototal"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="4" alt="numeric" type="text" id="montototal" name="montototal"  value="${bsLoteach.montototal}" size="10" maxlength="19"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsLoteach/ayudaPropiedadJson','montototal')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['montototal'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['numoperaciones']}">	
		<div class="row">
			<span class="label" style="color: #AA0707;"> 
				<fmt:message key="bsLoteach.formulario.etiqueta.Numoperaciones"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="5" alt="integer" type="text" id="numoperaciones" name="numoperaciones"  value="${bsLoteach.numoperaciones}" size="10" maxlength="10"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsLoteach/ayudaPropiedadJson','numoperaciones')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['numoperaciones'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['nombrelote']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsLoteach.formulario.etiqueta.Nombrelote"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="6" type="text" id="nombrelote" name="nombrelote"  value="${bsLoteach.nombrelote}" size="50"  maxlength="30"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsLoteach/ayudaPropiedadJson','nombrelote')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['nombrelote'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['fechaenviado']}">	
		<div class="row">
			<span class="label" style="color: #AA0707;"> 
				<fmt:message key="bsLoteach.formulario.etiqueta.Fechaenviado"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="7" type="text" cdate="fechaenviado" id="fechaenviado" name="fechaenviado"  value="<fmt:formatDate pattern="dd-MM-yyyy" value="${bsLoteach.fechaenviado}"/>" size="10" />
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsLoteach/ayudaPropiedadJson','fechaenviado')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['fechaenviado'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['fecharecibido']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsLoteach.formulario.etiqueta.Fecharecibido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="8" type="text" cdate="fecharecibido" id="fecharecibido" name="fecharecibido"  value="<fmt:formatDate pattern="dd-MM-yyyy" value="${bsLoteach.fecharecibido}"/>" size="10" />
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsLoteach/ayudaPropiedadJson','fecharecibido')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['fecharecibido'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['fechaaplicacion']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsLoteach.formulario.etiqueta.Fechaaplicacion"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="9" type="text" cdate="fechaaplicacion" id="fechaaplicacion" name="fechaaplicacion"  value="<fmt:formatDate pattern="dd-MM-yyyy" value="${bsLoteach.fechaaplicacion}"/>" size="10" />
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsLoteach/ayudaPropiedadJson','fechaaplicacion')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['fechaaplicacion'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['fechaestatus']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsLoteach.formulario.etiqueta.Fechaestatus"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="10" type="text" cdate="fechaestatus" id="fechaestatus" name="fechaestatus"  value="<fmt:formatDate pattern="dd-MM-yyyy" value="${bsLoteach.fechaestatus}"/>" size="10" />
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsLoteach/ayudaPropiedadJson','fechaestatus')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['fechaestatus'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['estatus']}">	
		<div class="row">
			<span class="label" style="color: #AA0707;"> 
				<fmt:message key="bsLoteach.formulario.etiqueta.Estatus"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="11" alt="integer" type="text" id="estatus" name="estatus"  value="${bsLoteach.estatus}" size="10" maxlength="10"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsLoteach/ayudaPropiedadJson','estatus')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['estatus'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['usuarioingreso']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsLoteach.formulario.etiqueta.Usuarioingreso"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="12" type="text" id="usuarioingreso" name="usuarioingreso"  value="${bsLoteach.usuarioingreso}" size="50"  maxlength="10"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsLoteach/ayudaPropiedadJson','usuarioingreso')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['usuarioingreso'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['autorizaciones']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsLoteach.formulario.etiqueta.Autorizaciones"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="13" alt="integer" type="text" id="autorizaciones" name="autorizaciones"  value="${bsLoteach.autorizaciones}" size="10" maxlength="3"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsLoteach/ayudaPropiedadJson','autorizaciones')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['autorizaciones'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['aplicaciondebitohost']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsLoteach.formulario.etiqueta.Aplicaciondebitohost"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="14" alt="integer" type="text" id="aplicaciondebitohost" name="aplicaciondebitohost"  value="${bsLoteach.aplicaciondebitohost}" size="10" maxlength="3"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsLoteach/ayudaPropiedadJson','aplicaciondebitohost')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['aplicaciondebitohost'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['sysMarcatiempo']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsLoteach.formulario.etiqueta.SysMarcatiempo"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="15" type="text" cdate="sysMarcatiempo" id="sysMarcatiempo" name="sysMarcatiempo"  value="<fmt:formatDate pattern="dd-MM-yyyy" value="${bsLoteach.sysMarcatiempo}"/>" size="10" />
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsLoteach/ayudaPropiedadJson','sysMarcatiempo')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['sysMarcatiempo'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['sysProcessid']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsLoteach.formulario.etiqueta.SysProcessid"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="16" alt="integer" type="text" id="sysProcessid" name="sysProcessid"  value="${bsLoteach.sysProcessid}" size="10" maxlength="10"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsLoteach/ayudaPropiedadJson','sysProcessid')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['sysProcessid'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['sysLowdatetime']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsLoteach.formulario.etiqueta.SysLowdatetime"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="17" alt="integer" type="text" id="sysLowdatetime" name="sysLowdatetime"  value="${bsLoteach.sysLowdatetime}" size="10" maxlength="10"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsLoteach/ayudaPropiedadJson','sysLowdatetime')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['sysLowdatetime'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['sysHighdatetime']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsLoteach.formulario.etiqueta.SysHighdatetime"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="18" alt="integer" type="text" id="sysHighdatetime" name="sysHighdatetime"  value="${bsLoteach.sysHighdatetime}" size="10" maxlength="10"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsLoteach/ayudaPropiedadJson','sysHighdatetime')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['sysHighdatetime'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['ponderacion']}">	
		<div class="row">
			<span class="label" style="color: #AA0707;"> 
				<fmt:message key="bsLoteach.formulario.etiqueta.Ponderacion"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="19" alt="integer" type="text" id="ponderacion" name="ponderacion"  value="${bsLoteach.ponderacion}" size="10" maxlength="10"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsLoteach/ayudaPropiedadJson','ponderacion')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['ponderacion'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['firmas']}">	
		<div class="row">
			<span class="label" style="color: #AA0707;"> 
				<fmt:message key="bsLoteach.formulario.etiqueta.Firmas"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="20" alt="integer" type="text" id="firmas" name="firmas"  value="${bsLoteach.firmas}" size="10" maxlength="10"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsLoteach/ayudaPropiedadJson','firmas')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['firmas'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['fechaaplicacionhost']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsLoteach.formulario.etiqueta.Fechaaplicacionhost"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="21" type="text" cdate="fechaaplicacionhost" id="fechaaplicacionhost" name="fechaaplicacionhost"  value="<fmt:formatDate pattern="dd-MM-yyyy" value="${bsLoteach.fechaaplicacionhost}"/>" size="10" />
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsLoteach/ayudaPropiedadJson','fechaaplicacionhost')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['fechaaplicacionhost'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['listoparahost']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsLoteach.formulario.etiqueta.Listoparahost"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="22" type="text" cdate="listoparahost" id="listoparahost" name="listoparahost"  value="<fmt:formatDate pattern="dd-MM-yyyy" value="${bsLoteach.listoparahost}"/>" size="10" />
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsLoteach/ayudaPropiedadJson','listoparahost')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['listoparahost'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['fechahoraprocesado']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsLoteach.formulario.etiqueta.Fechahoraprocesado"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="23" type="text" cdate="fechahoraprocesado" id="fechahoraprocesado" name="fechahoraprocesado"  value="<fmt:formatDate pattern="dd-MM-yyyy" value="${bsLoteach.fechahoraprocesado}"/>" size="10" />
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsLoteach/ayudaPropiedadJson','fechahoraprocesado')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['fechahoraprocesado'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['token']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsLoteach.formulario.etiqueta.Token"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="24" alt="integer" type="text" id="token" name="token"  value="${bsLoteach.token}" size="10" maxlength="10"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsLoteach/ayudaPropiedadJson','token')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['token'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['comentariorechazo']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsLoteach.formulario.etiqueta.Comentariorechazo"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="25" type="text" id="comentariorechazo" name="comentariorechazo"  value="${bsLoteach.comentariorechazo}" size="50"  maxlength="100"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsLoteach/ayudaPropiedadJson','comentariorechazo')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['comentariorechazo'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['descripcion']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsLoteach.formulario.etiqueta.Descripcion"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="26" type="text" id="descripcion" name="descripcion"  value="${bsLoteach.descripcion}" size="50"  maxlength="50"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsLoteach/ayudaPropiedadJson','descripcion')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['descripcion'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['estatuspaybank']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsLoteach.formulario.etiqueta.Estatuspaybank"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="27" type="text" id="estatuspaybank" name="estatuspaybank"  value="${bsLoteach.estatuspaybank}" size="50"  maxlength="20"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsLoteach/ayudaPropiedadJson','estatuspaybank')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['estatuspaybank'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['archivogenerado']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsLoteach.formulario.etiqueta.Archivogenerado"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="28" type="text" id="archivogenerado" name="archivogenerado"  value="${bsLoteach.archivogenerado}" size="50"  maxlength="100"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsLoteach/ayudaPropiedadJson','archivogenerado')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['archivogenerado'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['comisioncobrada']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsLoteach.formulario.etiqueta.Comisioncobrada"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="29" alt="numeric" type="text" id="comisioncobrada" name="comisioncobrada"  value="${bsLoteach.comisioncobrada}" size="10" maxlength="19"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsLoteach/ayudaPropiedadJson','comisioncobrada')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['comisioncobrada'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['cuentadebito']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsLoteach.formulario.etiqueta.Cuentadebito"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="30" type="text" id="cuentadebito" name="cuentadebito"  value="${bsLoteach.cuentadebito}" size="50"  maxlength="16"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsLoteach/ayudaPropiedadJson','cuentadebito')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['cuentadebito'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['autorizacionhost']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsLoteach.formulario.etiqueta.Autorizacionhost"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="31" alt="integer" type="text" id="autorizacionhost" name="autorizacionhost"  value="${bsLoteach.autorizacionhost}" size="10" maxlength="10"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsLoteach/ayudaPropiedadJson','autorizacionhost')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['autorizacionhost'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['descestatus']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsLoteach.formulario.etiqueta.Descestatus"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  		      <textarea tabindex="32" id="descestatus" name="descestatus" cols="45" rows="3"><c:out value="${bsLoteach.descestatus}"/></textarea>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsLoteach/ayudaPropiedadJson','descestatus')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['descestatus'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['idtransaccion']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsLoteach.formulario.etiqueta.Idtransaccion"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="33" alt="integer" type="text" id="idtransaccion" name="idtransaccion"  value="${bsLoteach.idtransaccion}" size="10" maxlength="10"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsLoteach/ayudaPropiedadJson','idtransaccion')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['idtransaccion'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['intactualizacion']}">	
		<div class="row">
			<span class="label" style="color: #AA0707;"> 
				<fmt:message key="bsLoteach.formulario.etiqueta.Intactualizacion"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="34" alt="integer" type="text" id="intactualizacion" name="intactualizacion"  value="${bsLoteach.intactualizacion}" size="10" maxlength="10"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsLoteach/ayudaPropiedadJson','intactualizacion')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['intactualizacion'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['montoimpuesto']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsLoteach.formulario.etiqueta.Montoimpuesto"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="35" alt="numeric" type="text" id="montoimpuesto" name="montoimpuesto"  value="${bsLoteach.montoimpuesto}" size="10" maxlength="19"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsLoteach/ayudaPropiedadJson','montoimpuesto')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['montoimpuesto'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
		</div>
</div>		
</div>
</form>

<%@ include file="/WEB-INF/global/jsp/jq-footer.jsp" %>
</fmt:bundle>
