<%@ taglib prefix="c"       uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt"     uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:if test="${ not empty locale}">
	<fmt:setLocale value="${ locale }" />
</c:if>
<fmt:bundle basename="com.ofbox.davivienda.h2h.resourceBundle.MessagesResources">
<input type="hidden" id="nombreDetalle" name="nombreDetalle" value=""/>
<!-- valores de llaves del padre -->
  <input type="hidden" id="idAtributoEncabezado" name="idAtributoEncabezado" value="${hthAtributoEncabezadoDtoCabeza.idAtributoEncabezado}"/>

<c:set var="ubicacion" value="cabecera"/>
<c:if test="${hthAtributoEncabezadoEnlaceDetalle == 'hthAtributoFormato'}">
	<c:set var="nombre" value="hthAtributoFormato"/>
</c:if>

<script>
$(document).ready(function(){
	$('#iHeader').accordion({collapsible: true, active: 0});
	$('#backLink').button().removeClass('ui-button-text-only').css({'padding':'0.2em 0.5em', 'margin-right':'20px'}).bind("click", function(e) {e.stopPropagation();});;
	$('#backLinkMod').button().removeClass('ui-button-text-only').css({'padding':'0.2em 0.5em', 'margin-right':'20px'}).bind("click", function(e) {e.stopPropagation();});
});
</script>  

<!-- datos de formulario -->
<div id="iHeader">
	<h3><a id="headerInfo" href="#">
			<span id="backLink" onclick="doSubmit('hthAtributoEncabezado/cancelarDetalle');"><fmt:message key="globales.cabecera.boton.regresar"/></span>
			<span id="backLinkMod" onclick="doSubmit('hthAtributoEncabezado/actualizar');"><fmt:message key="globales.cabecera.boton.actualizar"/></span>	
			 <fmt:message key="hthAtributoEncabezado.encabezado.etiqueta.titulo"/> ${hthAtributoEncabezadoDtoCabeza.idAtributoEncabezado} &nbsp;|			<span style="font-weight: normal;"><fmt:message key="hthAtributoEncabezado.encabezado.etiqueta.NombreAtributo"/>:</span>
		    ${hthAtributoEncabezadoDtoCabeza.nombreAtributo} 
 &nbsp;| 			<span style="font-weight: normal;"><fmt:message key="hthAtributoEncabezado.encabezado.etiqueta.Descripcion"/>:</span>
		    ${hthAtributoEncabezadoDtoCabeza.descripcion} 
 &nbsp;|       	</a></h3>
	<div>
	    <div>
		<strong><fmt:message key="hthAtributoEncabezado.encabezado.etiqueta.NombreAtributo"/></strong>&nbsp;
		${hthAtributoEncabezadoDtoCabeza.nombreAtributo}
		</div>
		
	    <div>
		<strong><fmt:message key="hthAtributoEncabezado.encabezado.etiqueta.Descripcion"/></strong>&nbsp;
		${hthAtributoEncabezadoDtoCabeza.descripcion}
		</div>
		
	</div>
</div>

<div id="iTab" class="ui-tabs ui-widget ui-widget-content ui-corner-top" >
 	<ul id="detailTabs" class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
	<c:if test="${hthAtributoEncabezadoEnlaceDetalle == 'hthAtributoFormato'}">
		<li class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active">
			<a onClick="$('#nombreDetalle').val('hthAtributoFormato');doSubmit('hthAtributoEncabezado/detalle')" class="ui-selected"><fmt:message key="hthAtributoEncabezado.encabezado.etiqueta.hthAtributoFormatoDrillDown"/></a>
		</li>
	</c:if> 			
	<c:if test="${hthAtributoEncabezadoEnlaceDetalle != 'hthAtributoFormato'}">
		<li>
			<a onClick="$('#nombreDetalle').val('hthAtributoFormato');doSubmit('hthAtributoEncabezado/detalle')"><fmt:message key="hthAtributoEncabezado.encabezado.etiqueta.hthAtributoFormatoDrillDown"/></a>
		</li>
	</c:if>		
	</ul>
</div>
<div id="iTabline"></div>
</fmt:bundle>