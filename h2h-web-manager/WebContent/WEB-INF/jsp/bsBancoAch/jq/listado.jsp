<%@ taglib prefix="c"       uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt"     uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:if test="${ not empty locale}">
	<fmt:setLocale value="${ locale }" />
</c:if>
<fmt:bundle basename="com.ofbox.davivienda.h2h.resourceBundle.MessagesResources">

<c:if test="${ not empty bsBancoAchEstado.enlaceBusqueda.enllavador}">
	<c:set var="busquedaEnllavador" value="${bsBancoAchEstado.enlaceBusqueda.enllavador}"/>	
</c:if>	
<c:if test="${ not empty bsBancoAchEstado.enlaceDetalle}">
	<c:set var="detalle" value="${bsBancoAchEstado.enlaceDetalle}"/>	
</c:if>	
<c:if test="${ bsBancoAchEstado.ordenamientoAplicado}">
	<c:set var="orden" value="${bsBancoAchEstado.ordenamiento}"/>	
</c:if>		
<c:if test="${not empty  bsBancoAchSeg}">
	<c:set var="restriccion" value="${ bsBancoAchSeg}"/>
</c:if>

<c:set var="nombre" value="bsBancoAch"/>
<c:set var="ubicacion" value="listado"/>

<% int counter = 0; %>
<%@ include file="/WEB-INF/global/jsp/jq-header.jsp" %>
<!-- Elementos no visibles -->	
	
<form  name="mainForm" id="mainForm" action="${NewtPageRenderData.actionUrl}" method="post">

<div class="breadCrumbHolder module" style="margin-bottom: 3px;">
<div id="breadcrumb" class="breadCrumb module ui-widget ui-widget-content" style="width: 99%;">
<ul>
	<li><a href="${ctx}/inicio/blank/ria"> <fmt:message key="globales.bc.etiqueta.home" /> </a> </li>
<c:if test="${ not empty ubicacionAplicacion.ruta }">
	<input type="hidden" name="idUbicacionAplicacionActual" id="idUbicacionAplicacionActual" value="${ ubicacionAplicacion.idUbicacionAplicacionActual }"/>
	<c:forEach items="${ ubicacionAplicacion.ruta }" var="ub">
		<li><a href="javascript: doSubmitBreadcrumb('<c:out value="${ ub.submit }"/>', '<c:out value="${ ub.id }"/>')"><c:out value="${ ub.label }" escapeXml="false" /></a>  </li> 
	</c:forEach>			
</c:if>
	<li><fmt:message key="bsBancoAch.bc.etiqueta.principal" /></li>
</ul>
</div>
	<input type="hidden" name="id_ubicacion_retorno" id="id_ubicacion_retorno" value=""/>
</div>

<c:if test="${ not empty detalle.cabecera}">
    <jsp:include page="/WEB-INF/jsp/${detalle.cabecera.locacion}/jq/cabecera.jsp" />
</c:if>
	
<div id="mainContent">
    <div id="iTitle" class="ui-state-default ui-widget-content ui-corner-all">
    	<fmt:message key="bsBancoAch.listado.etiqueta.titulo"/>
    </div>
	<input type="hidden" id="maxRowNum" name="maxRowNum" value="<c:out value="${navegacion.registroFinal - 1}"/>">
	<input type="hidden" id="pageLoc" name="pageLoc" value="listado"/>
    <input type="hidden" id="eag" name="eag" value="${eag}"/>
	<c:if test="${ not empty busquedaEnllavador}">
	<input type="hidden" id="listadoLov" value="bsBancoAch"/>
	</c:if>
	<c:if test="${ not empty esAsync}">
	<input type="hidden" id="esAsync" value="${esAsync}"/>
	</c:if>
    <input type="hidden" id="movimientoPagina" name="movimientoPagina"/>
    <input type="hidden" id="columna" name="columna"/>
    <input type="hidden" id="fenixf31seguridad" name="fenixf31seguridad" value="<c:out value='${fenixf31seguridad}' />" />
	<!-- valores de llaves -->
    <input type="hidden" alt="notForSubmit" id="codBanco" name="codBanco" value=""/>
	 <input type="hidden" id="expo" name="expo" value=""/>  



<%@ include file="/WEB-INF/global/jsp/mensajes-globales.jsp" %>

<c:if test="${ not empty busquedaEnllavador}">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em; font-size: 85%;"> 
	<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
 	<strong><fmt:message key="globales.listado.etiqueta.tipLov.titulo"/></strong>&nbsp;<fmt:message key="globales.listado.etiqueta.tipLov.mensaje"/>
	</p>
</div>
</c:if>
<!-- botones de acciones basicas -->
<div id="action-buttons">
 <div id="action-bar">
	<c:if test="${ empty busquedaEnllavador}">
		</c:if>
	<c:if test="${ not empty busquedaEnllavador}">
		<button id="btnSeleccionar" type="button" class="ui-button ui-corner-all ui-button-color" name="seleccionar" onClick="if(isValueSelected()){ doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsBancoAch/buscarSeleccionar'); }"><fmt:message key="globales.listado.boton.seleccionar"/></button>
		<button id="btnCancelar" type="button" class="ui-button ui-corner-all ui-button-color" name="cancel" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsBancoAch/cancelarBuscar');"><fmt:message key="globales.listado.boton.cancelar"/></button>
	</c:if>
 </div>
</div>
<div id="navigation-bar">	
<c:if test="${navegacion.paginaAnterior}">				
	<button id="btnAnterior" type="button" name="anterior" onClick="$('#movimientoPagina').val('anterior');doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsBancoAch/listado')"><fmt:message key="globales.listado.boton.anterior"/></button>
</c:if>
<c:if test="${!navegacion.paginaAnterior}">			
	<button id="btnAnterior" type="button" name="anterior" disabled="disabled"><fmt:message key="globales.listado.boton.anterior"/></button>
</c:if>
<c:if test="${not empty navegacion.listadoPaginas}">
<c:forEach items="${navegacion.listadoPaginas}" var="p">
	<c:if test="${p == navegacion.noPaginaActual}">
	<button id="pag<c:out value="${p}" />" class="ui-button-text ui-state-highlight" type="button" name="pag<c:out value="${p}" />" disabled="disabled"><c:out value="${p}" /></button>
	</c:if>
	<c:if test="${p != navegacion.noPaginaActual}">
	<button id="pag<c:out value="${p}" />"  onClick="$('#movimientoPagina').val('${p}');doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsBancoAch/listado')" type="button" name="pag<c:out value="${p}"/>"><c:out value="${p}" /></button>	
	</c:if> 
</c:forEach>
</c:if>
<c:if test="${navegacion.paginaSiguiente}">			
	<button id="btnSiguiente" type="button" name="siguiente" onClick="$('#movimientoPagina').val('siguiente');doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsBancoAch/listado')" ><fmt:message key="globales.listado.boton.siguiente"/></button>
</c:if>
<c:if test="${!navegacion.paginaSiguiente}">			
	<button id="btnSiguiente" type="button" name="siguiente" disabled="disabled"><fmt:message key="globales.listado.boton.siguiente"/></button>
</c:if>
	
</div>
</div>
<br/>

<div class="ui-widget fg-toolbar ui-toolbar ui-widget-header ui-corner-tl ui-corner-tr ui-helper-clearfix">

<c:if test="${ not empty orden}">
	<div style="display:block; float: left;">
		<button id="btnRemoverOrden" type="button" class="ui-button ui-corner-all ui-button-color" name="cancel" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsBancoAch/removerOrdenamiento');"><fmt:message key="globales.listado.boton.removerOrden"/></button>
	</div>
</c:if>
<c:if test="${navegacion.filtroAplicado}">
	<div style="display:block; float: left;">
		<button id="btnRemoverFiltro" type="button" class="ui-button ui-corner-all ui-button-color" name="nofilter" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsBancoAch/removerFiltrar');"><fmt:message key="globales.listado.boton.removerFiltro"/></button>
	</div>			
</c:if>
	<div id="crudTable_filter" style="display: block; text-align: right; float: right; margin-top: 6px; margin-bottom: 6px;"> 
		<fmt:message key="globales.listado.etiqueta.busquedaRapida"/>
		<select id="campoBusqueda" name="campoBusqueda" onchange="doBusquedaRapida('#busqueda', this.value, $('option:selected', this));" class="ui-widget ui-widget-content ui-state-default">
			<option value="codBanco"><fmt:message key="bsBancoAch.listado.etiqueta.codBanco"/></option>			
			<option value="nombre"><fmt:message key="bsBancoAch.listado.etiqueta.nombre"/></option>		
			<option value="moneda"><fmt:message key="bsBancoAch.listado.etiqueta.moneda"/></option>		
			<option value="esActivo"><fmt:message key="bsBancoAch.listado.etiqueta.esActivo"/></option>		
			<option value="codPayBank"><fmt:message key="bsBancoAch.listado.etiqueta.codPayBank"/></option>	
			<option value="digitoChequeo"><fmt:message key="bsBancoAch.listado.etiqueta.digitoChequeo"/></option>
			<option value="longitudCtaAhorro"><fmt:message key="bsBancoAch.listado.etiqueta.longitudCtaAhorro"/></option>			
			<option value="longitudCtaCorriente"><fmt:message key="bsBancoAch.listado.etiqueta.longitudCtaCorriente"/></option>	
			<option value="longitudCtaPrestamo"><fmt:message key="bsBancoAch.listado.etiqueta.longitudCtaPrestamo"/></option>			
			<option value="longitudTjCredito"><fmt:message key="bsBancoAch.listado.etiqueta.longitudTjCredito"/></option>			
			<option value="idAs400"><fmt:message key="bsBancoAch.listado.etiqueta.idAs400"/></option>			
			<option value="enlinea"><fmt:message key="bsBancoAch.listado.etiqueta.enlinea"/></option>	
		</select> &nbsp;
                <select  id="resultadoProceso" name="resultadoProceso" onchange="$('#busqueda').val(this.value); doFiltrarPor('bsBancoAch');" class="ui-widget ui-state-default" style="display: none;">
				    <option value=""/>			
				    <option value="O"><fmt:message key="bsBancoAch.formulario.comboLabel.OK"/></option>
				    <option value="E"><fmt:message key="bsBancoAch.formulario.comboLabel.ERROR"/></option>
				    <option value="A"><fmt:message key="bsBancoAch.formulario.comboLabel.ADVERTENCIA"/></option>
                </select>
		<input onkeypress="doFiltrarPor('bsBancoAch', event)" type="text" id="busqueda" name="busqueda" class="ui-widget ui-widget-content " style="width:150px; height: 18px;"/>
	</div>
</div>
<c:if test="${restriccion.listadoDisponible or not empty busquedaEnllavador}">
<table id="crudTable" width="100%">
	<thead>
		<tr>
			<th onclick="doSort('codBanco','${nombre}')" sort="${orden.mapaDeValores['codBanco'].strOrden}"><fmt:message key="bsBancoAch.listado.etiqueta.codBanco"/></th> 		
			<th onclick="doSort('nombre','${nombre}')" sort="${orden.mapaDeValores['nombre'].strOrden}"><fmt:message key="bsBancoAch.listado.etiqueta.nombre"/></th> 
			<th onclick="doSort('moneda','${nombre}')" sort="${orden.mapaDeValores['moneda'].strOrden}"><fmt:message key="bsBancoAch.listado.etiqueta.moneda"/></th>  
			<th onclick="doSort('esActivo','${nombre}')" sort="${orden.mapaDeValores['esActivo'].strOrden}"><fmt:message key="bsBancoAch.listado.etiqueta.esActivo"/></th> 				 			
			<th onclick="doSort('codPayBank','${nombre}')" sort="${orden.mapaDeValores['codPayBank'].strOrden}"><fmt:message key="bsBancoAch.listado.etiqueta.codPayBank"/></th>
			<th onclick="doSort('digitoChequeo','${nombre}')" sort="${orden.mapaDeValores['digitoChequeo'].strOrden}"><fmt:message key="bsBancoAch.listado.etiqueta.digitoChequeo"/></th>
			<th onclick="doSort('longitudCtaAhorro','${nombre}')" sort="${orden.mapaDeValores['longitudCtaAhorro'].strOrden}"><fmt:message key="bsBancoAch.listado.etiqueta.longitudCtaAhorro"/></th>  			
			<th onclick="doSort('longitudCtaCorriente','${nombre}')" sort="${orden.mapaDeValores['longitudCtaCorriente'].strOrden}"><fmt:message key="bsBancoAch.listado.etiqueta.longitudCtaCorriente"/></th>
			<th onclick="doSort('longitudCtaPrestamo','${nombre}')" sort="${orden.mapaDeValores['longitudCtaPrestamo'].strOrden}"><fmt:message key="bsBancoAch.listado.etiqueta.longitudCtaPrestamo"/></th>  			
			<th onclick="doSort('longitudTjCredito','${nombre}')" sort="${orden.mapaDeValores['longitudTjCredito'].strOrden}"><fmt:message key="bsBancoAch.listado.etiqueta.longitudTjCredito"/></th>		
			<th onclick="doSort('idAs400','${nombre}')" sort="${orden.mapaDeValores['idAs400'].strOrden}"><fmt:message key="bsBancoAch.listado.etiqueta.idAs400"/></th>  			
			<th onclick="doSort('enlinea','${nombre}')" sort="${orden.mapaDeValores['enlinea'].strOrden}"><fmt:message key="bsBancoAch.listado.etiqueta.enlinea"/></th> 	
		</tr>
	</thead>	
	<tbody>
<c:forEach items="${coleccionResultados}" var="dto"> 
		<tr onclick="$('#codBanco').val('${dto.codBanco}'); selectDeSelectObject(this, 'ui-state-active'); sel = true; currentRow = $(this).parent().children().index($(this));">  
			<td><c:out value="${dto.codBanco}"/></td>
			<td><c:out value="${dto.nombre}"/></td>		
			<td><c:out value="${dto.moneda}"/></td>
			<td><c:out value="${dto.esActivo}"/></td>
			<td><c:out value="${dto.codPayBank}"/></td>
			<td><c:out value="${dto.digitoChequeo}"/></td>
			<td><c:out value="${dto.longitudCtaAhorro}"/></td>
			<td><c:out value="${dto.longitudCtaCorriente}"/></td>
			<td><c:out value="${dto.longitudCtaPrestamo}"/></td>
			<td><c:out value="${dto.longitudTjCredito}"/></td>
			<td><c:out value="${dto.idAs400}"/></td>
			<td><c:out value="${dto.enlinea}"/></td>
		</tr>
</c:forEach>
	</tbody>
</table>
</c:if>
<div class="ui-widget fg-toolbar ui-toolbar ui-widget-header ui-corner-bl ui-corner-br ui-helper-clearfix">
	<c:if test="${navegacion.totalRegistros <= 0}">
		<fmt:message key="globales.listado.etiqueta.sinRegistros" />
    </c:if>
    <c:if test="${navegacion.totalRegistros > 0}">
    	<fmt:message key="globales.listado.etiqueta.numRegistros">
    		<fmt:param><c:out value="${navegacion.registroInicial}"/></fmt:param>
    		<fmt:param><c:out value="${navegacion.registroFinal}"/></fmt:param>
    		<fmt:param><c:out value="${navegacion.totalRegistros}"/></fmt:param>
    	</fmt:message>
    </c:if>	
</div>
<br/>
</div>
</form>
<%@ include file="/WEB-INF/global/jsp/jq-footer.jsp" %>
</fmt:bundle>
