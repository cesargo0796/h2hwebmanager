<%@ taglib prefix="c"       uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt"     uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:if test="${ not empty locale}">
	<fmt:setLocale value="${ locale }" />
</c:if>
<fmt:bundle basename="com.ofbox.davivienda.h2h.resourceBundle.MessagesResources">
<c:if test="${ not empty bsClienteEstado.enlaceDetalle}">
	<c:set var="detalle" value="${bsClienteEstado.enlaceDetalle}"/>	
	<c:set var="detalleEnllavador" value="${detalle.enllavador}"/>
</c:if>		

<c:set var="nombre" value="bsCliente"/>
<c:set var="ubicacion" value="ver"/>

<%@ include file="/WEB-INF/global/jsp/jq-header.jsp" %>

<form  name="mainForm" id="mainForm" action="${NewtPageRenderData.actionUrl}" method="post">
<c:if test="${ not empty detalle.cabecera}">
    <jsp:include page="/WEB-INF/jsp/${detalle.cabecera.locacion}/jq/cabecera.jsp" />
</c:if>
<div id="mainContent">
	<div class="breadCrumbHolder module" style="margin-bottom: 3px;">
<div id="breadcrumb" class="breadCrumb module ui-widget ui-widget-content" style="width: 99%;">
<ul>
	<li><a href="${ctx}/inicio/blank/ria"> HOME </a> </li>
<c:if test="${ not empty ubicacionAplicacion.ruta }">
	<input type="hidden" name="idUbicacionAplicacionActual" id="idUbicacionAplicacionActual" value="${ ubicacionAplicacion.idUbicacionAplicacionActual }"/>
	<c:forEach items="${ ubicacionAplicacion.ruta }" var="ub">
		<li><a href="javascript: doSubmitBreadcrumb('<c:out value="${ ub.submit }"/>', '<c:out value="${ ub.id }"/>')"><c:out value="${ ub.label }" escapeXml="false" /></a>  </li> 
	</c:forEach>			
</c:if>
	<li><fmt:message key="bsCliente.bc.etiqueta.ver" /></li>
</ul>
</div>
	<input type="hidden" name="id_ubicacion_retorno" id="id_ubicacion_retorno" value=""/>
</div>
	
<%--<div id="iTitle" class="ui-state-default ui-widget-content ui-corner-all">
	<fmt:message key="bsCliente.formulario.etiqueta.ver.titulo"/>
</div>--%>

    <div id="iProcessing" style="visibility: hidden" align="center">
    	<img src="${ctx}/include/images/design/processing_request.gif"></img>
    </div>
	<input type="hidden" id="pageLoc" name="pageLoc" value="ver"/>
    <input type="hidden" id="eag" name="eag" value="${eag}"/>
    <input type="hidden" id="movimientoPagina" name="movimientoPagina"/>
    <input type="hidden" id="columna" name="columna"/>
    <input type="hidden" id="fenixf31seguridad" name="fenixf31seguridad" value="<c:out value='${fenixf31seguridad}' />" />
	<!-- valores de llaves -->
	
	

<div id="action-buttons">
<div id="action-bar">
	<button id="btnCancelar" type="button" name="regresar" onClick="doSubmit<c:if test="${ not empty detalle.cabecera}">Async</c:if>('bsCliente/cancelarVer');"><fmt:message key="globales.formulario.boton.regresar"/></button>
</div>
</div>
<br />

<!-- datos de formulario -->
<div class="ui-widget ui-widget-content ui-corner-all" style="width: 99%">
		<div id="formInputHeader" class="ui-widget ui-widget-header ui-corner-top" style="padding-left: 5px;"> 
			<fmt:message key="bsCliente.formulario.etiqueta.ver.titulo"/> 
		</div>
		
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsCliente.formulario.etiqueta.Nombre"/>
			</span>	 
			
			<span class="formvalue">
                ${bsCliente.nombre}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsCliente.formulario.etiqueta.Modulos"/>
			</span>	 
			
			<span class="formvalue">
                ${bsCliente.modulos}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsCliente.formulario.etiqueta.Direccion"/>
			</span>	 
			
			<span class="formvalue">
                ${bsCliente.direccion}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsCliente.formulario.etiqueta.Telefono"/>
			</span>	 
			
			<span class="formvalue">
                ${bsCliente.telefono}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsCliente.formulario.etiqueta.Fax"/>
			</span>	 
			
			<span class="formvalue">
                ${bsCliente.fax}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsCliente.formulario.etiqueta.Email"/>
			</span>	 
			
			<span class="formvalue">
                ${bsCliente.email}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsCliente.formulario.etiqueta.Nombrecontacto"/>
			</span>	 
			
			<span class="formvalue">
                ${bsCliente.nombrecontacto}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsCliente.formulario.etiqueta.Apellidocontacto"/>
			</span>	 
			
			<span class="formvalue">
                ${bsCliente.apellidocontacto}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsCliente.formulario.etiqueta.Numinstalacion"/>
			</span>	 
			
			<span class="formvalue">
                ${bsCliente.numinstalacion}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsCliente.formulario.etiqueta.Fechaestatus"/>
			</span>	 
			
			<span class="formvalue">
                <fmt:formatDate pattern="dd-MM-yyyy" value="${bsCliente.fechaestatus}" />
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsCliente.formulario.etiqueta.Estatus"/>
			</span>	 
			
			<span class="formvalue">
                ${bsCliente.estatus}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsCliente.formulario.etiqueta.Fechacreacion"/>
			</span>	 
			
			<span class="formvalue">
                <fmt:formatDate pattern="dd-MM-yyyy" value="${bsCliente.fechacreacion}" />
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsCliente.formulario.etiqueta.Solicitudpendiente"/>
			</span>	 
			
			<span class="formvalue">
                ${bsCliente.solicitudpendiente}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsCliente.formulario.etiqueta.Nivelseguridadtex"/>
			</span>	 
			
			<span class="formvalue">
                ${bsCliente.nivelseguridadtex}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsCliente.formulario.etiqueta.NivelseguridadPIMP"/>
			</span>	 
			
			<span class="formvalue">
                ${bsCliente.nivelseguridadPIMP}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsCliente.formulario.etiqueta.Nombrerepresentante"/>
			</span>	 
			
			<span class="formvalue">
                ${bsCliente.nombrerepresentante}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsCliente.formulario.etiqueta.Apellidorepresentante"/>
			</span>	 
			
			<span class="formvalue">
                ${bsCliente.apellidorepresentante}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsCliente.formulario.etiqueta.Proveedorinternet"/>
			</span>	 
			
			<span class="formvalue">
                ${bsCliente.proveedorinternet}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsCliente.formulario.etiqueta.Banca"/>
			</span>	 
			
			<span class="formvalue">
                ${bsCliente.banca}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsCliente.formulario.etiqueta.Politicacobrotex"/>
			</span>	 
			
			<span class="formvalue">
                ${bsCliente.politicacobrotex}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsCliente.formulario.etiqueta.Comisiontransext"/>
			</span>	 
			
			<span class="formvalue">
                ${bsCliente.comisiontransext}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsCliente.formulario.etiqueta.Codigotablatransext"/>
			</span>	 
			
			<span class="formvalue">
                ${bsCliente.codigotablatransext}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsCliente.formulario.etiqueta.Nivelseguridadisss"/>
			</span>	 
			
			<span class="formvalue">
                ${bsCliente.nivelseguridadisss}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsCliente.formulario.etiqueta.Nivelseguridadafp"/>
			</span>	 
			
			<span class="formvalue">
                ${bsCliente.nivelseguridadafp}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsCliente.formulario.etiqueta.Nivelseguridadccre"/>
			</span>	 
			
			<span class="formvalue">
                ${bsCliente.nivelseguridadccre}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsCliente.formulario.etiqueta.Nivelseguridadppag"/>
			</span>	 
			
			<span class="formvalue">
                ${bsCliente.nivelseguridadppag}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsCliente.formulario.etiqueta.Cargapersonalizable"/>
			</span>	 
			
			<span class="formvalue">
                ${bsCliente.cargapersonalizable}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsCliente.formulario.etiqueta.Tipodoctorepresentante"/>
			</span>	 
			
			<span class="formvalue">
                ${bsCliente.tipodoctorepresentante}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsCliente.formulario.etiqueta.Doctorepresentante"/>
			</span>	 
			
			<span class="formvalue">
                ${bsCliente.doctorepresentante}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsCliente.formulario.etiqueta.Ciudad"/>
			</span>	 
			
			<span class="formvalue">
                ${bsCliente.ciudad}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsCliente.formulario.etiqueta.Cargo"/>
			</span>	 
			
			<span class="formvalue">
                ${bsCliente.cargo}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsCliente.formulario.etiqueta.Limitexarchivo"/>
			</span>	 
			
			<span class="formvalue">
                ${bsCliente.limitexarchivo}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsCliente.formulario.etiqueta.Limitexlote"/>
			</span>	 
			
			<span class="formvalue">
                ${bsCliente.limitexlote}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsCliente.formulario.etiqueta.Limitextransaccion"/>
			</span>	 
			
			<span class="formvalue">
                ${bsCliente.limitextransaccion}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsCliente.formulario.etiqueta.Permitedebitos"/>
			</span>	 
			
			<span class="formvalue">
                ${bsCliente.permitedebitos}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsCliente.formulario.etiqueta.Limitecreditos"/>
			</span>	 
			
			<span class="formvalue">
                ${bsCliente.limitecreditos}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsCliente.formulario.etiqueta.Limitedebitos"/>
			</span>	 
			
			<span class="formvalue">
                ${bsCliente.limitedebitos}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsCliente.formulario.etiqueta.Comisiontrnach"/>
			</span>	 
			
			<span class="formvalue">
                ${bsCliente.comisiontrnach}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
		
</div>		
</div>
</form>

<%@ include file="/WEB-INF/global/jsp/jq-footer.jsp" %>
</fmt:bundle>
