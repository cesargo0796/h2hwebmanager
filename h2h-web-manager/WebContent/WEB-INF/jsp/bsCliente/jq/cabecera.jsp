<%@ taglib prefix="c"       uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt"     uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:if test="${ not empty locale}">
	<fmt:setLocale value="${ locale }" />
</c:if>
<fmt:bundle basename="com.ofbox.davivienda.h2h.resourceBundle.MessagesResources">
<input type="hidden" id="nombreDetalle" name="nombreDetalle" value=""/>
<!-- valores de llaves del padre -->
  <input type="hidden" id="cliente" name="cliente" value="${bsClienteDtoCabeza.cliente}"/>

<c:set var="ubicacion" value="cabecera"/>
<c:if test="${bsClienteEnlaceDetalle == 'bsConvenioach'}">
	<c:set var="nombre" value="bsConvenioach"/>
</c:if>
<c:if test="${bsClienteEnlaceDetalle == 'hthProcesoMonitoreo'}">
	<c:set var="nombre" value="hthProcesoMonitoreo"/>
</c:if>

<script>
$(document).ready(function(){
	$('#iHeader').accordion({collapsible: true, active: 0});
	$('#backLink').button().removeClass('ui-button-text-only').css({'padding':'0.2em 0.5em', 'margin-right':'20px'}).bind("click", function(e) {e.stopPropagation();});;
	$('#backLinkMod').button().removeClass('ui-button-text-only').css({'padding':'0.2em 0.5em', 'margin-right':'20px'}).bind("click", function(e) {e.stopPropagation();});
});
</script>  

<!-- datos de formulario -->
<div id="iHeader">
	<h3><a id="headerInfo" href="#">
			<span id="backLink" onclick="doSubmit('bsCliente/cancelarDetalle');"><fmt:message key="globales.cabecera.boton.regresar"/></span>
			<span id="backLinkMod" onclick="doSubmit('bsCliente/actualizar');"><fmt:message key="globales.cabecera.boton.actualizar"/></span>	
			 <fmt:message key="bsCliente.encabezado.etiqueta.titulo"/> ${bsClienteDtoCabeza.cliente} &nbsp;|			<span style="font-weight: normal;"><fmt:message key="bsCliente.encabezado.etiqueta.Nombre"/>:</span>
		    ${bsClienteDtoCabeza.nombre} 
 &nbsp;| 			<span style="font-weight: normal;"><fmt:message key="bsCliente.encabezado.etiqueta.Modulos"/>:</span>
		    ${bsClienteDtoCabeza.modulos} 
 &nbsp;|                                     	</a></h3>
	<div>
	    <div>
		<strong><fmt:message key="bsCliente.encabezado.etiqueta.Nombre"/></strong>&nbsp;
		${bsClienteDtoCabeza.nombre}
		</div>
		
	    <div>
		<strong><fmt:message key="bsCliente.encabezado.etiqueta.Modulos"/></strong>&nbsp;
		${bsClienteDtoCabeza.modulos}
		</div>
		
	</div>
</div>

<div id="iTab" class="ui-tabs ui-widget ui-widget-content ui-corner-top" >
 	<ul id="detailTabs" class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
	<c:if test="${bsClienteEnlaceDetalle == 'bsConvenioach'}">
		<li class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active">
			<a onClick="$('#nombreDetalle').val('bsConvenioach');doSubmit('bsCliente/detalle')" class="ui-selected"><fmt:message key="bsCliente.encabezado.etiqueta.bsConvenioachDrillDown"/></a>
		</li>
	</c:if> 			
	<c:if test="${bsClienteEnlaceDetalle != 'bsConvenioach'}">
		<li>
			<a onClick="$('#nombreDetalle').val('bsConvenioach');doSubmit('bsCliente/detalle')"><fmt:message key="bsCliente.encabezado.etiqueta.bsConvenioachDrillDown"/></a>
		</li>
	</c:if>		
	<c:if test="${bsClienteEnlaceDetalle == 'hthProcesoMonitoreo'}">
		<li class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active">
			<a onClick="$('#nombreDetalle').val('hthProcesoMonitoreo');doSubmit('bsCliente/detalle')" class="ui-selected"><fmt:message key="bsCliente.encabezado.etiqueta.hthProcesoMonitoreoDrillDown"/></a>
		</li>
	</c:if> 			
	<c:if test="${bsClienteEnlaceDetalle != 'hthProcesoMonitoreo'}">
		<li>
			<a onClick="$('#nombreDetalle').val('hthProcesoMonitoreo');doSubmit('bsCliente/detalle')"><fmt:message key="bsCliente.encabezado.etiqueta.hthProcesoMonitoreoDrillDown"/></a>
		</li>
	</c:if>		
	</ul>
</div>
<div id="iTabline"></div>
</fmt:bundle>