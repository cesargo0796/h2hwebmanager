<%@ taglib prefix="c"       uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt"     uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:if test="${ not empty locale}">
	<fmt:setLocale value="${ locale }" />
</c:if>
<fmt:bundle basename="com.ofbox.davivienda.h2h.resourceBundle.MessagesResources">

<c:if test="${ not empty bsClienteEstado.enlaceBusqueda.enllavador}">
	<c:set var="busquedaEnllavador" value="${bsClienteEstado.enlaceBusqueda.enllavador}"/>	
</c:if>	
<c:if test="${ not empty bsClienteEstado.enlaceDetalle}">
	<c:set var="detalle" value="${bsClienteEstado.enlaceDetalle}"/>	
</c:if>	
<c:if test="${bsClienteEstado.ordenamientoAplicado}">
	<c:set var="orden" value="${bsClienteEstado.ordenamiento}"/>	
</c:if>		
<c:if test="${not empty bsClienteSeg}">
	<c:set var="restriccion" value="${bsClienteSeg}"/>
</c:if>

<c:set var="nombre" value="bsCliente"/>
<c:set var="ubicacion" value="listado"/>

<% int counter = 0; %>
<%@ include file="/WEB-INF/global/jsp/jq-header.jsp" %>
<!-- Elementos no visibles -->	
	
<form  name="mainForm" id="mainForm" action="${NewtPageRenderData.actionUrl}" method="post">

<div id="mainContent">

<div class="breadCrumbHolder module" style="margin-bottom: 3px;">
<div id="breadcrumb" class="breadCrumb module ui-widget ui-widget-content" style="width: 99%;">
<ul>
	<li><a href="${ctx}/inicio/blank/ria"> HOME </a> </li>
<c:if test="${ not empty ubicacionAplicacion.ruta }">
	<input type="hidden" name="idUbicacionAplicacionActual" id="idUbicacionAplicacionActual" value="${ ubicacionAplicacion.idUbicacionAplicacionActual }"/>
	<c:forEach items="${ ubicacionAplicacion.ruta }" var="ub">
		<li><a href="javascript: doSubmitBreadcrumb('<c:out value="${ ub.submit }"/>', '<c:out value="${ ub.id }"/>')"><c:out value="${ ub.label }" escapeXml="false" /></a>  </li> 
	</c:forEach>			
</c:if>
	<li><fmt:message key="bsCliente.bc.etiqueta.principal" /></li>
</ul>
</div>
	<input type="hidden" name="id_ubicacion_retorno" id="id_ubicacion_retorno" value=""/>
</div>

<c:if test="${ not empty detalle.cabecera}">
    <jsp:include page="/WEB-INF/jsp/${detalle.cabecera.locacion}/jq/cabecera.jsp" />
</c:if>

<%-- <div id="iTitle" class="ui-state-default ui-widget-content ui-corner-all">
	<fmt:message key="bsCliente.listado.etiqueta.titulo"/>
</div>--%>
	<input type="hidden" id="maxRowNum" name="maxRowNum" value="<c:out value="${navegacion.registroFinal - 1}"/>">
	<input type="hidden" id="pageLoc" name="pageLoc" value="listado"/>
    <input type="hidden" id="eag" name="eag" value="${eag}"/>
	<c:if test="${ not empty busquedaEnllavador}">
	<input type="hidden" id="listadoLov" value="bsCliente"/>
	</c:if>
	<c:if test="${ not empty esAsync}">
	<input type="hidden" id="esAsync" value="${esAsync}"/>
	</c:if>
    <input type="hidden" id="movimientoPagina" name="movimientoPagina"/>
    <input type="hidden" id="columna" name="columna"/>
    <input type="hidden" id="fenixf31seguridad" name="fenixf31seguridad" value="<c:out value='${fenixf31seguridad}' />" />
	<!-- valores de llaves -->
    <input type="hidden" alt="notForSubmit" id="cliente" name="cliente" value=""/>
	 <input type="hidden" id="expo" name="expo" value=""/>  



<%@ include file="/WEB-INF/global/jsp/mensajes-globales.jsp" %>

<c:if test="${ not empty busquedaEnllavador}">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em; font-size: 85%;"> 
	<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
 	<strong><fmt:message key="globales.listado.etiqueta.tipLov.titulo"/></strong>&nbsp;<fmt:message key="globales.listado.etiqueta.tipLov.mensaje"/>
	</p>
</div>
</c:if>
<!-- botones de acciones basicas -->
<div id="action-buttons">
<div id="action-bar">
<c:if test="${ empty busquedaEnllavador}">
	<c:if test="${restriccion.verDisponible}">
	<button id="btnVer" type="button" class="ui-button ui-corner-all ui-button-color" name="ver" onClick="if(isValueSelected()){ doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsCliente/ver'); }"><fmt:message key="globales.listado.boton.ver"/></button>
	</c:if>
	<c:if test="${restriccion.crearDisponible}">
	<button id="btnCrear" type="button" class="ui-button ui-corner-all ui-button-color" name="crear" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsCliente/crear');"><fmt:message key="globales.listado.boton.crear"/></button>
	</c:if>
	<c:if test="${restriccion.actualizarDisponible}">	
	<button id="btnActualizar" type="button" class="ui-button ui-corner-all ui-button-color" name="actualizar" onClick="if(isValueSelected()){ doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsCliente/actualizar'); }"><fmt:message key="globales.listado.boton.actualizar"/></button>
	</c:if>
	<c:if test="${restriccion.eliminarDisponible}">
	<button id="btnEliminar" type="button" class="ui-button ui-corner-all ui-button-color" name="eliminar" onClick="if(isValueSelected()){$('#dialog-confirm').dialog('open');}"><fmt:message key="globales.listado.boton.eliminar"/></button>
	</c:if>
	<button id="btnDetalle" type="button" class="ui-button ui-corner-all ui-button-color" name="detalle" onClick="if(isValueSelected()){ doSubmit('bsCliente/detalle'); }"><fmt:message key="globales.listado.boton.detalle"/></button>
	</c:if>
<c:if test="${ not empty busquedaEnllavador}">
	<c:if test="${restriccion.crearDisponible}">
	<button id="btnCrear" type="button" class="ui-button ui-corner-all ui-button-color" name="crear" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsCliente/crear');"><fmt:message key="globales.listado.boton.crear"/></button>
	</c:if>
	<button id="btnSeleccionar" type="button" class="ui-button ui-corner-all ui-button-color" name="seleccionar" onClick="if(isValueSelected()){ doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsCliente/buscarSeleccionar'); }"><fmt:message key="globales.listado.boton.seleccionar"/></button>
	<button id="btnCancelar" type="button" class="ui-button ui-corner-all ui-button-color" name="cancel" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsCliente/cancelarBuscar');"><fmt:message key="globales.listado.boton.cancelar"/></button>
</c:if>
	<c:if test="${restriccion.filtrarDisponible}">	
	<button id="btnFiltrar" type="button" class="ui-button ui-corner-all ui-button-color" name="filter" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsCliente/filtrar');"><fmt:message key="globales.listado.boton.filtrar"/></button>
	</c:if>	
	
</div>
<div id="navigation-bar">	
<c:if test="${navegacion.paginaAnterior}">				
	<button id="btnAnterior" type="button" name="anterior" onClick="$('#movimientoPagina').val('anterior');doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsCliente/listado')"><fmt:message key="globales.listado.boton.anterior"/></button>
</c:if>
<c:if test="${!navegacion.paginaAnterior}">			
	<button id="btnAnterior" type="button" name="anterior" disabled="disabled"><fmt:message key="globales.listado.boton.anterior"/></button>
</c:if>
<c:if test="${not empty navegacion.listadoPaginas}">
<c:forEach items="${navegacion.listadoPaginas}" var="p">
	<c:if test="${p == navegacion.noPaginaActual}">
	<button id="pag<c:out value="${p}" />" class="ui-button-text ui-state-highlight" type="button" name="pag<c:out value="${p}" />" disabled="disabled"><c:out value="${p}" /></button>
	</c:if>
	<c:if test="${p != navegacion.noPaginaActual}">
	<button id="pag<c:out value="${p}" />"  onClick="$('#movimientoPagina').val('${p}');doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsCliente/listado')" type="button" name="pag<c:out value="${p}"/>"><c:out value="${p}" /></button>	
	</c:if> 
</c:forEach>
</c:if>
<c:if test="${navegacion.paginaSiguiente}">			
	<button id="btnSiguiente" type="button" name="siguiente" onClick="$('#movimientoPagina').val('siguiente');doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsCliente/listado')" ><fmt:message key="globales.listado.boton.siguiente"/></button>
</c:if>
<c:if test="${!navegacion.paginaSiguiente}">			
	<button id="btnSiguiente" type="button" name="siguiente" disabled="disabled"><fmt:message key="globales.listado.boton.siguiente"/></button>
</c:if>
	
</div>
</div>
<br/>

<div class="ui-widget fg-toolbar ui-toolbar ui-widget-header ui-corner-tl ui-corner-tr ui-helper-clearfix">

<c:if test="${ not empty orden}">
	<div style="display:block; float: left;">
		<button id="btnRemoverOrden" type="button" class="ui-button ui-corner-all ui-button-color" name="cancel" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsCliente/removerOrdenamiento');"><fmt:message key="globales.listado.boton.removerOrden"/></button>
	</div>
</c:if>
<c:if test="${navegacion.filtroAplicado}">
	<div style="display:block; float: left;">
		<button id="btnRemoverFiltro" type="button" class="ui-button ui-corner-all ui-button-color" name="nofilter" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsCliente/removerFiltrar');"><fmt:message key="globales.listado.boton.removerFiltro"/></button>
	</div>			
</c:if>
	<div id="crudTable_filter" style="display: block; text-align: right; float: right; margin-top: 6px; margin-bottom: 6px;"> 
		<fmt:message key="globales.listado.etiqueta.busquedaRapida"/>
		<select id="campoBusqueda" name="campoBusqueda" onchange="doBusquedaRapida('#busqueda', this.value, $('option:selected', this));" class="ui-widget ui-widget-content ui-state-default">
			<option value="nombre"><fmt:message key="bsCliente.listado.etiqueta.Nombre"/></option>
			<option value="modulos"><fmt:message key="bsCliente.listado.etiqueta.Modulos"/></option>
			<option value="direccion"><fmt:message key="bsCliente.listado.etiqueta.Direccion"/></option>
			<option value="telefono"><fmt:message key="bsCliente.listado.etiqueta.Telefono"/></option>
			<option value="fax"><fmt:message key="bsCliente.listado.etiqueta.Fax"/></option>
			<option value="email"><fmt:message key="bsCliente.listado.etiqueta.Email"/></option>
			<option value="nombrecontacto"><fmt:message key="bsCliente.listado.etiqueta.Nombrecontacto"/></option>
			<option value="apellidocontacto"><fmt:message key="bsCliente.listado.etiqueta.Apellidocontacto"/></option>
			<option value="numinstalacion"><fmt:message key="bsCliente.listado.etiqueta.Numinstalacion"/></option>
			<option value="estatus"><fmt:message key="bsCliente.listado.etiqueta.Estatus"/></option>
			<option value="solicitudpendiente"><fmt:message key="bsCliente.listado.etiqueta.Solicitudpendiente"/></option>
			<option value="nivelseguridadtex"><fmt:message key="bsCliente.listado.etiqueta.Nivelseguridadtex"/></option>
			<option value="nivelseguridadPIMP"><fmt:message key="bsCliente.listado.etiqueta.NivelseguridadPIMP"/></option>
			<option value="nombrerepresentante"><fmt:message key="bsCliente.listado.etiqueta.Nombrerepresentante"/></option>
			<option value="apellidorepresentante"><fmt:message key="bsCliente.listado.etiqueta.Apellidorepresentante"/></option>
			<option value="proveedorinternet"><fmt:message key="bsCliente.listado.etiqueta.Proveedorinternet"/></option>
			<option value="banca"><fmt:message key="bsCliente.listado.etiqueta.Banca"/></option>
			<option value="politicacobrotex"><fmt:message key="bsCliente.listado.etiqueta.Politicacobrotex"/></option>
			<option value="codigotablatransext"><fmt:message key="bsCliente.listado.etiqueta.Codigotablatransext"/></option>
			<option value="nivelseguridadisss"><fmt:message key="bsCliente.listado.etiqueta.Nivelseguridadisss"/></option>
			<option value="nivelseguridadafp"><fmt:message key="bsCliente.listado.etiqueta.Nivelseguridadafp"/></option>
			<option value="nivelseguridadccre"><fmt:message key="bsCliente.listado.etiqueta.Nivelseguridadccre"/></option>
			<option value="nivelseguridadppag"><fmt:message key="bsCliente.listado.etiqueta.Nivelseguridadppag"/></option>
			<option value="cargapersonalizable"><fmt:message key="bsCliente.listado.etiqueta.Cargapersonalizable"/></option>
			<option value="tipodoctorepresentante"><fmt:message key="bsCliente.listado.etiqueta.Tipodoctorepresentante"/></option>
			<option value="doctorepresentante"><fmt:message key="bsCliente.listado.etiqueta.Doctorepresentante"/></option>
			<option value="ciudad"><fmt:message key="bsCliente.listado.etiqueta.Ciudad"/></option>
			<option value="cargo"><fmt:message key="bsCliente.listado.etiqueta.Cargo"/></option>
			<option value="permitedebitos"><fmt:message key="bsCliente.listado.etiqueta.Permitedebitos"/></option>
		</select> &nbsp;
		<input onkeypress="doFiltrarPor('bsCliente', event)" type="text" id="busqueda" name="busqueda" class="ui-widget ui-widget-content " style="width:150px; height: 18px;"/>
	</div>
</div>
<c:if test="${restriccion.listadoDisponible or not empty busquedaEnllavador}">
<table id="crudTable" width="100%">
	<thead>
		<tr>
  			<th onclick="doSort('nombre','${nombre}')" sort="${orden.mapaDeValores['nombre'].strOrden}"><fmt:message key="bsCliente.listado.etiqueta.Nombre"/></th>
  			<th onclick="doSort('modulos','${nombre}')" sort="${orden.mapaDeValores['modulos'].strOrden}"><fmt:message key="bsCliente.listado.etiqueta.Modulos"/></th>
  			<th onclick="doSort('direccion','${nombre}')" sort="${orden.mapaDeValores['direccion'].strOrden}"><fmt:message key="bsCliente.listado.etiqueta.Direccion"/></th>
  			<th onclick="doSort('telefono','${nombre}')" sort="${orden.mapaDeValores['telefono'].strOrden}"><fmt:message key="bsCliente.listado.etiqueta.Telefono"/></th>
  			<th onclick="doSort('fax','${nombre}')" sort="${orden.mapaDeValores['fax'].strOrden}"><fmt:message key="bsCliente.listado.etiqueta.Fax"/></th>
  			<th onclick="doSort('email','${nombre}')" sort="${orden.mapaDeValores['email'].strOrden}"><fmt:message key="bsCliente.listado.etiqueta.Email"/></th>
  			<th onclick="doSort('nombrecontacto','${nombre}')" sort="${orden.mapaDeValores['nombrecontacto'].strOrden}"><fmt:message key="bsCliente.listado.etiqueta.Nombrecontacto"/></th>
  			<th onclick="doSort('apellidocontacto','${nombre}')" sort="${orden.mapaDeValores['apellidocontacto'].strOrden}"><fmt:message key="bsCliente.listado.etiqueta.Apellidocontacto"/></th>
  			<th onclick="doSort('numinstalacion','${nombre}')" sort="${orden.mapaDeValores['numinstalacion'].strOrden}"><fmt:message key="bsCliente.listado.etiqueta.Numinstalacion"/></th>
  			<th onclick="doSort('fechaestatus','${nombre}')" sort="${orden.mapaDeValores['fechaestatus'].strOrden}"><fmt:message key="bsCliente.listado.etiqueta.Fechaestatus"/></th>
  			<th onclick="doSort('estatus','${nombre}')" sort="${orden.mapaDeValores['estatus'].strOrden}"><fmt:message key="bsCliente.listado.etiqueta.Estatus"/></th>
  			<%-- <th onclick="doSort('fechacreacion','${nombre}')" sort="${orden.mapaDeValores['fechacreacion'].strOrden}"><fmt:message key="bsCliente.listado.etiqueta.Fechacreacion"/></th>
  			<th onclick="doSort('solicitudpendiente','${nombre}')" sort="${orden.mapaDeValores['solicitudpendiente'].strOrden}"><fmt:message key="bsCliente.listado.etiqueta.Solicitudpendiente"/></th>
  			<th onclick="doSort('nivelseguridadtex','${nombre}')" sort="${orden.mapaDeValores['nivelseguridadtex'].strOrden}"><fmt:message key="bsCliente.listado.etiqueta.Nivelseguridadtex"/></th>
  			<th onclick="doSort('nivelseguridadPIMP','${nombre}')" sort="${orden.mapaDeValores['nivelseguridadPIMP'].strOrden}"><fmt:message key="bsCliente.listado.etiqueta.NivelseguridadPIMP"/></th>
  			<th onclick="doSort('nombrerepresentante','${nombre}')" sort="${orden.mapaDeValores['nombrerepresentante'].strOrden}"><fmt:message key="bsCliente.listado.etiqueta.Nombrerepresentante"/></th>
  			<th onclick="doSort('apellidorepresentante','${nombre}')" sort="${orden.mapaDeValores['apellidorepresentante'].strOrden}"><fmt:message key="bsCliente.listado.etiqueta.Apellidorepresentante"/></th>
  			<th onclick="doSort('proveedorinternet','${nombre}')" sort="${orden.mapaDeValores['proveedorinternet'].strOrden}"><fmt:message key="bsCliente.listado.etiqueta.Proveedorinternet"/></th>
  			<th onclick="doSort('banca','${nombre}')" sort="${orden.mapaDeValores['banca'].strOrden}"><fmt:message key="bsCliente.listado.etiqueta.Banca"/></th>
  			<th onclick="doSort('politicacobrotex','${nombre}')" sort="${orden.mapaDeValores['politicacobrotex'].strOrden}"><fmt:message key="bsCliente.listado.etiqueta.Politicacobrotex"/></th>
  			<th onclick="doSort('comisiontransext','${nombre}')" sort="${orden.mapaDeValores['comisiontransext'].strOrden}"><fmt:message key="bsCliente.listado.etiqueta.Comisiontransext"/></th>
  			<th onclick="doSort('codigotablatransext','${nombre}')" sort="${orden.mapaDeValores['codigotablatransext'].strOrden}"><fmt:message key="bsCliente.listado.etiqueta.Codigotablatransext"/></th>
  			<th onclick="doSort('nivelseguridadisss','${nombre}')" sort="${orden.mapaDeValores['nivelseguridadisss'].strOrden}"><fmt:message key="bsCliente.listado.etiqueta.Nivelseguridadisss"/></th>
  			<th onclick="doSort('nivelseguridadafp','${nombre}')" sort="${orden.mapaDeValores['nivelseguridadafp'].strOrden}"><fmt:message key="bsCliente.listado.etiqueta.Nivelseguridadafp"/></th>
  			<th onclick="doSort('nivelseguridadccre','${nombre}')" sort="${orden.mapaDeValores['nivelseguridadccre'].strOrden}"><fmt:message key="bsCliente.listado.etiqueta.Nivelseguridadccre"/></th>
  			<th onclick="doSort('nivelseguridadppag','${nombre}')" sort="${orden.mapaDeValores['nivelseguridadppag'].strOrden}"><fmt:message key="bsCliente.listado.etiqueta.Nivelseguridadppag"/></th>
  			<th onclick="doSort('cargapersonalizable','${nombre}')" sort="${orden.mapaDeValores['cargapersonalizable'].strOrden}"><fmt:message key="bsCliente.listado.etiqueta.Cargapersonalizable"/></th>
  			<th onclick="doSort('tipodoctorepresentante','${nombre}')" sort="${orden.mapaDeValores['tipodoctorepresentante'].strOrden}"><fmt:message key="bsCliente.listado.etiqueta.Tipodoctorepresentante"/></th>
  			<th onclick="doSort('doctorepresentante','${nombre}')" sort="${orden.mapaDeValores['doctorepresentante'].strOrden}"><fmt:message key="bsCliente.listado.etiqueta.Doctorepresentante"/></th>
  			<th onclick="doSort('ciudad','${nombre}')" sort="${orden.mapaDeValores['ciudad'].strOrden}"><fmt:message key="bsCliente.listado.etiqueta.Ciudad"/></th>
  			<th onclick="doSort('cargo','${nombre}')" sort="${orden.mapaDeValores['cargo'].strOrden}"><fmt:message key="bsCliente.listado.etiqueta.Cargo"/></th>
  			<th onclick="doSort('limitexarchivo','${nombre}')" sort="${orden.mapaDeValores['limitexarchivo'].strOrden}"><fmt:message key="bsCliente.listado.etiqueta.Limitexarchivo"/></th>
  			<th onclick="doSort('limitexlote','${nombre}')" sort="${orden.mapaDeValores['limitexlote'].strOrden}"><fmt:message key="bsCliente.listado.etiqueta.Limitexlote"/></th>
  			<th onclick="doSort('limitextransaccion','${nombre}')" sort="${orden.mapaDeValores['limitextransaccion'].strOrden}"><fmt:message key="bsCliente.listado.etiqueta.Limitextransaccion"/></th>
  			<th onclick="doSort('permitedebitos','${nombre}')" sort="${orden.mapaDeValores['permitedebitos'].strOrden}"><fmt:message key="bsCliente.listado.etiqueta.Permitedebitos"/></th>
  			<th onclick="doSort('limitecreditos','${nombre}')" sort="${orden.mapaDeValores['limitecreditos'].strOrden}"><fmt:message key="bsCliente.listado.etiqueta.Limitecreditos"/></th>
  			<th onclick="doSort('limitedebitos','${nombre}')" sort="${orden.mapaDeValores['limitedebitos'].strOrden}"><fmt:message key="bsCliente.listado.etiqueta.Limitedebitos"/></th>
  			<th onclick="doSort('comisiontrnach','${nombre}')" sort="${orden.mapaDeValores['comisiontrnach'].strOrden}"><fmt:message key="bsCliente.listado.etiqueta.Comisiontrnach"/></th> --%>
		</tr>
	</thead>	
	<tbody>
<c:forEach items="${coleccionResultados}" var="dto">
		<tr onclick="$('#cliente').val('${dto.cliente}'); selectDeSelectObject(this, 'ui-state-active'); sel = true; currentRow = $(this).parent().children().index($(this));">  
		<td><c:out value="${dto.nombre}"/></td>
		<td><c:out value="${dto.modulos}"/></td>
		<td><c:out value="${dto.direccion}"/></td>
		<td><c:out value="${dto.telefono}"/></td>
		<td><c:out value="${dto.fax}"/></td>
		<td><c:out value="${dto.email}"/></td>
		<td><c:out value="${dto.nombrecontacto}"/></td>
		<td><c:out value="${dto.apellidocontacto}"/></td>
		<td><c:out value="${dto.numinstalacion}"/></td>
		<td><fmt:formatDate pattern="dd-MM-yyyy" value="${dto.fechaestatus}" /></td>
		<td><c:out value="${dto.estatus}"/></td>
		<%-- <td><fmt:formatDate pattern="dd-MM-yyyy" value="${dto.fechacreacion}" /></td>
		<td><c:out value="${dto.solicitudpendiente}"/></td>
		<td><c:out value="${dto.nivelseguridadtex}"/></td>
		<td><c:out value="${dto.nivelseguridadPIMP}"/></td>
		<td><c:out value="${dto.nombrerepresentante}"/></td>
		<td><c:out value="${dto.apellidorepresentante}"/></td>
		<td><c:out value="${dto.proveedorinternet}"/></td>
		<td><c:out value="${dto.banca}"/></td>
		<td><c:out value="${dto.politicacobrotex}"/></td>
		<td><c:out value="${dto.comisiontransext}"/></td>
		<td><c:out value="${dto.codigotablatransext}"/></td>
		<td><c:out value="${dto.nivelseguridadisss}"/></td>
		<td><c:out value="${dto.nivelseguridadafp}"/></td>
		<td><c:out value="${dto.nivelseguridadccre}"/></td>
		<td><c:out value="${dto.nivelseguridadppag}"/></td>
		<td><c:out value="${dto.cargapersonalizable}"/></td>
		<td><c:out value="${dto.tipodoctorepresentante}"/></td>
		<td><c:out value="${dto.doctorepresentante}"/></td>
		<td><c:out value="${dto.ciudad}"/></td>
		<td><c:out value="${dto.cargo}"/></td>
		<td><c:out value="${dto.limitexarchivo}"/></td>
		<td><c:out value="${dto.limitexlote}"/></td>
		<td><c:out value="${dto.limitextransaccion}"/></td>
		<td><c:out value="${dto.permitedebitos}"/></td>
		<td><c:out value="${dto.limitecreditos}"/></td>
		<td><c:out value="${dto.limitedebitos}"/></td>
		<td><c:out value="${dto.comisiontrnach}"/></td> --%>
		</tr>
</c:forEach>
	</tbody>
</table>
</c:if>
<div class="ui-widget fg-toolbar ui-toolbar ui-widget-header ui-corner-bl ui-corner-br ui-helper-clearfix">
	<c:if test="${navegacion.totalRegistros <= 0}">
		<fmt:message key="globales.listado.etiqueta.sinRegistros" />
    </c:if>
    <c:if test="${navegacion.totalRegistros > 0}">
    	<fmt:message key="globales.listado.etiqueta.numRegistros">
    		<fmt:param><c:out value="${navegacion.registroInicial}"/></fmt:param>
    		<fmt:param><c:out value="${navegacion.registroFinal}"/></fmt:param>
    		<fmt:param><c:out value="${navegacion.totalRegistros}"/></fmt:param>
    	</fmt:message>
    </c:if>	
</div>
<br/>
</div>
</form>
<%@ include file="/WEB-INF/global/jsp/jq-footer.jsp" %>
</fmt:bundle>