<%@ taglib prefix="c"       uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt"     uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:if test="${ not empty locale}">
	<fmt:setLocale value="${ locale }" />
</c:if>
<fmt:bundle basename="com.ofbox.davivienda.h2h.resourceBundle.MessagesResources">
	
<c:if test="${ not empty bsClienteEstado.enlaceDetalle}">
	<c:set var="detalle" value="${bsClienteEstado.enlaceDetalle}"/>	
	<c:set var="detalleEnllavador" value="${detalle.enllavador}"/>
</c:if>		

<c:set var="nombre" value="bsCliente"/>
<c:set var="ubicacion" value="actualizar"/>	
<c:set var="primerCampo" value="nombre"/>
<%@ include file="/WEB-INF/global/jsp/jq-header.jsp" %>
	
<form  name="mainForm" id="mainForm" action="${NewtPageRenderData.actionUrl}" method="post">
<c:if test="${ not empty detalle.cabecera}">
    <jsp:include page="/WEB-INF/jsp/${detalle.cabecera.locacion}/jq/cabecera.jsp" />
</c:if>
<div id="mainContent">

	<div class="breadCrumbHolder module" style="margin-bottom: 3px;">
<div id="breadcrumb" class="breadCrumb module ui-widget ui-widget-content" style="width: 99%;">
<ul>
	<li><a href="${ctx}/inicio/blank/ria"> HOME </a> </li>
<c:if test="${ not empty ubicacionAplicacion.ruta }">
	<input type="hidden" name="idUbicacionAplicacionActual" id="idUbicacionAplicacionActual" value="${ ubicacionAplicacion.idUbicacionAplicacionActual }"/>
	<c:forEach items="${ ubicacionAplicacion.ruta }" var="ub">
		<li><a href="javascript: doSubmitBreadcrumb('<c:out value="${ ub.submit }"/>', '<c:out value="${ ub.id }"/>')"><c:out value="${ ub.label }" escapeXml="false" /></a>  </li> 
	</c:forEach>			
</c:if>
	<li><fmt:message key="bsCliente.bc.etiqueta.actualizar" /></li>
</ul>
</div>
	<input type="hidden" name="id_ubicacion_retorno" id="id_ubicacion_retorno" value=""/>
</div>	
	
<div id="iProcessing" style="visibility: hidden" align="center">
	<img src="${ctx}/include/images/design/processing_request.gif"></img>
</div>

<%--<div id="iTitle" class="ui-state-default ui-widget-content ui-corner-all">
	<fmt:message key="bsCliente.formulario.etiqueta.actualizar.titulo"/>
</div> --%>
	<input type="hidden" id="pageLoc" name="pageLoc" value="actualizar"/>
	<input type="hidden" id="primerCampo" name="primerCampo" value="nombre"/>
    <input type="hidden" id="eag" name="eag" value="${eag}"/>
    <input type="hidden" id="movimientoPagina" name="movimientoPagina"/>
    <input type="hidden" id="columna" name="columna"/>
    <input type="hidden" id="fenixf31seguridad" name="fenixf31seguridad" value="<c:out value='${fenixf31seguridad}' />" />
	<c:if test="${not empty scrollTo }">
    <input type="hidden" id="scrollSmooth" value="<c:out value='${scrollTo}' />" />
    </c:if>
	<!-- valores de llaves -->
	<input type="hidden" id="cliente" name="cliente" value="${bsCliente.cliente}"/>	
	


<!-- mensajes de error-->
<c:if test="${mensajesUsuario.erroresGlobales}">
<div class="ui-state-error ui-corner-all" style="padding: 0 .7em; font-size: 0.8em;"> 
	<span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span> 
	<strong><fmt:message key="globales.formulario.etiqueta.error"/></strong> <br /> 
	<ul>
		<c:forEach items="${mensajesUsuario.mensajesDeError}" var="mensaje">
        	<li><c:out value="${mensaje}" escapeXml="false"/></li>
     	</c:forEach>
	</ul>
</div>
</c:if>

<div id="action-buttons">
<div id="action-bar">
	<button tabindex="39" id="btnGuardar" type="submit" name="actualizar" onClick="doSubmit<c:if test="${ not empty detalle.cabecera}">Async</c:if>('bsCliente/actualizarEjecutar');"><fmt:message key="globales.formulario.boton.actualizar.guardar"/></button>
	<button tabindex="40" id="btnCancelar" type="button" name="cancel" onClick="doSubmit<c:if test="${ not empty detalle.cabecera}">Async</c:if>('bsCliente/cancelarActualizar');"><fmt:message key="globales.formulario.boton.actualizar.cancelar"/></button>
</div>
</div>
<br />

<!-- datos de formulario -->
<div><span style="font-size: small;">-Los campos marcados con * son obligatorios</span></div>
<div class="ui-widget ui-widget-content ui-corner-all" style="width: 99%">
		<div id="formInputHeader" class="ui-widget ui-widget-header ui-corner-top" style="padding-left: 5px;"> 
			<fmt:message key="bsCliente.formulario.etiqueta.actualizar.titulo"/> 
		</div>
		<div style="display: inline;" id="formInput">
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['nombre']}">	
		<div class="row">
			<span class="label" style="color: #AA0707;"> 
				<fmt:message key="bsCliente.formulario.etiqueta.Nombre"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="1" type="text" id="nombre" name="nombre"  value="${bsCliente.nombre}" size="50"  maxlength="50"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsCliente/ayudaPropiedadJson','nombre')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['nombre'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['modulos']}">	
		<div class="row">
			<span class="label" style="color: #AA0707;"> 
				<fmt:message key="bsCliente.formulario.etiqueta.Modulos"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="2" alt="integer" type="text" id="modulos" name="modulos"  value="${bsCliente.modulos}" size="10" maxlength="10"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsCliente/ayudaPropiedadJson','modulos')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['modulos'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['direccion']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsCliente.formulario.etiqueta.Direccion"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="3" type="text" id="direccion" name="direccion"  value="${bsCliente.direccion}" size="50"  maxlength="50"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsCliente/ayudaPropiedadJson','direccion')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['direccion'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['telefono']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsCliente.formulario.etiqueta.Telefono"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="4" type="text" id="telefono" name="telefono"  value="${bsCliente.telefono}" size="50"  maxlength="40"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsCliente/ayudaPropiedadJson','telefono')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['telefono'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['fax']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsCliente.formulario.etiqueta.Fax"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="5" type="text" id="fax" name="fax"  value="${bsCliente.fax}" size="50"  maxlength="30"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsCliente/ayudaPropiedadJson','fax')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['fax'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['email']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsCliente.formulario.etiqueta.Email"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="6" type="text" id="email" name="email"  value="${bsCliente.email}" size="50"  maxlength="45"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsCliente/ayudaPropiedadJson','email')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['email'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['nombrecontacto']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsCliente.formulario.etiqueta.Nombrecontacto"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="7" type="text" id="nombrecontacto" name="nombrecontacto"  value="${bsCliente.nombrecontacto}" size="50"  maxlength="30"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsCliente/ayudaPropiedadJson','nombrecontacto')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['nombrecontacto'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['apellidocontacto']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsCliente.formulario.etiqueta.Apellidocontacto"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="8" type="text" id="apellidocontacto" name="apellidocontacto"  value="${bsCliente.apellidocontacto}" size="50"  maxlength="30"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsCliente/ayudaPropiedadJson','apellidocontacto')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['apellidocontacto'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['numinstalacion']}">	
		<div class="row">
			<span class="label" style="color: #AA0707;"> 
				<fmt:message key="bsCliente.formulario.etiqueta.Numinstalacion"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="9" alt="integer" type="text" id="numinstalacion" name="numinstalacion"  value="${bsCliente.numinstalacion}" size="10" maxlength="3"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsCliente/ayudaPropiedadJson','numinstalacion')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['numinstalacion'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['fechaestatus']}">	
		<div class="row">
			<span class="label" style="color: #AA0707;"> 
				<fmt:message key="bsCliente.formulario.etiqueta.Fechaestatus"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="10" type="text" cdate="fechaestatus" id="fechaestatus" name="fechaestatus"  value="<fmt:formatDate pattern="dd-MM-yyyy" value="${bsCliente.fechaestatus}"/>" size="10" />
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsCliente/ayudaPropiedadJson','fechaestatus')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['fechaestatus'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['estatus']}">	
		<div class="row">
			<span class="label" style="color: #AA0707;"> 
				<fmt:message key="bsCliente.formulario.etiqueta.Estatus"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="11" alt="integer" type="text" id="estatus" name="estatus"  value="${bsCliente.estatus}" size="10" maxlength="3"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsCliente/ayudaPropiedadJson','estatus')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['estatus'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['fechacreacion']}">	
		<div class="row">
			<span class="label" style="color: #AA0707;"> 
				<fmt:message key="bsCliente.formulario.etiqueta.Fechacreacion"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="12" type="text" cdate="fechacreacion" id="fechacreacion" name="fechacreacion"  value="<fmt:formatDate pattern="dd-MM-yyyy" value="${bsCliente.fechacreacion}"/>" size="10" />
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsCliente/ayudaPropiedadJson','fechacreacion')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['fechacreacion'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['solicitudpendiente']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsCliente.formulario.etiqueta.Solicitudpendiente"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="13" alt="integer" type="text" id="solicitudpendiente" name="solicitudpendiente"  value="${bsCliente.solicitudpendiente}" size="10" maxlength="3"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsCliente/ayudaPropiedadJson','solicitudpendiente')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['solicitudpendiente'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['nivelseguridadtex']}">	
		<div class="row">
			<span class="label" style="color: #AA0707;"> 
				<fmt:message key="bsCliente.formulario.etiqueta.Nivelseguridadtex"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="14" alt="integer" type="text" id="nivelseguridadtex" name="nivelseguridadtex"  value="${bsCliente.nivelseguridadtex}" size="10" maxlength="3"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsCliente/ayudaPropiedadJson','nivelseguridadtex')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['nivelseguridadtex'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['nivelseguridadPIMP']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsCliente.formulario.etiqueta.NivelseguridadPIMP"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="15" alt="integer" type="text" id="nivelseguridadPIMP" name="nivelseguridadPIMP"  value="${bsCliente.nivelseguridadPIMP}" size="10" maxlength="3"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsCliente/ayudaPropiedadJson','nivelseguridadPIMP')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['nivelseguridadPIMP'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['nombrerepresentante']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsCliente.formulario.etiqueta.Nombrerepresentante"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="16" type="text" id="nombrerepresentante" name="nombrerepresentante"  value="${bsCliente.nombrerepresentante}" size="50"  maxlength="30"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsCliente/ayudaPropiedadJson','nombrerepresentante')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['nombrerepresentante'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['apellidorepresentante']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsCliente.formulario.etiqueta.Apellidorepresentante"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="17" type="text" id="apellidorepresentante" name="apellidorepresentante"  value="${bsCliente.apellidorepresentante}" size="50"  maxlength="30"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsCliente/ayudaPropiedadJson','apellidorepresentante')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['apellidorepresentante'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['proveedorinternet']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsCliente.formulario.etiqueta.Proveedorinternet"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="18" type="text" id="proveedorinternet" name="proveedorinternet"  value="${bsCliente.proveedorinternet}" size="50"  maxlength="30"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsCliente/ayudaPropiedadJson','proveedorinternet')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['proveedorinternet'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['banca']}">	
		<div class="row">
			<span class="label" style="color: #AA0707;"> 
				<fmt:message key="bsCliente.formulario.etiqueta.Banca"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="19" alt="integer" type="text" id="banca" name="banca"  value="${bsCliente.banca}" size="10" maxlength="10"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsCliente/ayudaPropiedadJson','banca')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['banca'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['politicacobrotex']}">	
		<div class="row">
			<span class="label" style="color: #AA0707;"> 
				<fmt:message key="bsCliente.formulario.etiqueta.Politicacobrotex"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="20" alt="integer" type="text" id="politicacobrotex" name="politicacobrotex"  value="${bsCliente.politicacobrotex}" size="10" maxlength="3"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsCliente/ayudaPropiedadJson','politicacobrotex')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['politicacobrotex'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['comisiontransext']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsCliente.formulario.etiqueta.Comisiontransext"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="21" alt="numeric" type="text" id="comisiontransext" name="comisiontransext"  value="${bsCliente.comisiontransext}" size="10" maxlength="19"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsCliente/ayudaPropiedadJson','comisiontransext')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['comisiontransext'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['codigotablatransext']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsCliente.formulario.etiqueta.Codigotablatransext"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="22" alt="integer" type="text" id="codigotablatransext" name="codigotablatransext"  value="${bsCliente.codigotablatransext}" size="10" maxlength="5"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsCliente/ayudaPropiedadJson','codigotablatransext')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['codigotablatransext'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['nivelseguridadisss']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsCliente.formulario.etiqueta.Nivelseguridadisss"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="23" alt="integer" type="text" id="nivelseguridadisss" name="nivelseguridadisss"  value="${bsCliente.nivelseguridadisss}" size="10" maxlength="3"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsCliente/ayudaPropiedadJson','nivelseguridadisss')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['nivelseguridadisss'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['nivelseguridadafp']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsCliente.formulario.etiqueta.Nivelseguridadafp"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="24" alt="integer" type="text" id="nivelseguridadafp" name="nivelseguridadafp"  value="${bsCliente.nivelseguridadafp}" size="10" maxlength="3"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsCliente/ayudaPropiedadJson','nivelseguridadafp')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['nivelseguridadafp'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['nivelseguridadccre']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsCliente.formulario.etiqueta.Nivelseguridadccre"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="25" alt="integer" type="text" id="nivelseguridadccre" name="nivelseguridadccre"  value="${bsCliente.nivelseguridadccre}" size="10" maxlength="3"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsCliente/ayudaPropiedadJson','nivelseguridadccre')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['nivelseguridadccre'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['nivelseguridadppag']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsCliente.formulario.etiqueta.Nivelseguridadppag"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="26" alt="integer" type="text" id="nivelseguridadppag" name="nivelseguridadppag"  value="${bsCliente.nivelseguridadppag}" size="10" maxlength="3"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsCliente/ayudaPropiedadJson','nivelseguridadppag')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['nivelseguridadppag'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['cargapersonalizable']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsCliente.formulario.etiqueta.Cargapersonalizable"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="27" type="text" id="cargapersonalizable" name="cargapersonalizable"  value="${bsCliente.cargapersonalizable}" size="50"  maxlength="1"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsCliente/ayudaPropiedadJson','cargapersonalizable')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['cargapersonalizable'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['tipodoctorepresentante']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsCliente.formulario.etiqueta.Tipodoctorepresentante"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="28" alt="integer" type="text" id="tipodoctorepresentante" name="tipodoctorepresentante"  value="${bsCliente.tipodoctorepresentante}" size="10" maxlength="10"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsCliente/ayudaPropiedadJson','tipodoctorepresentante')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['tipodoctorepresentante'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['doctorepresentante']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsCliente.formulario.etiqueta.Doctorepresentante"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="29" type="text" id="doctorepresentante" name="doctorepresentante"  value="${bsCliente.doctorepresentante}" size="50"  maxlength="35"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsCliente/ayudaPropiedadJson','doctorepresentante')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['doctorepresentante'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['ciudad']}">	
		<div class="row">
			<span class="label" style="color: #AA0707;"> 
				<fmt:message key="bsCliente.formulario.etiqueta.Ciudad"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="30" type="text" id="ciudad" name="ciudad"  value="${bsCliente.ciudad}" size="50"  maxlength="50"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsCliente/ayudaPropiedadJson','ciudad')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['ciudad'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['cargo']}">	
		<div class="row">
			<span class="label" style="color: #AA0707;"> 
				<fmt:message key="bsCliente.formulario.etiqueta.Cargo"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="31" type="text" id="cargo" name="cargo"  value="${bsCliente.cargo}" size="50"  maxlength="100"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsCliente/ayudaPropiedadJson','cargo')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['cargo'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['limitexarchivo']}">	
		<div class="row">
			<span class="label" style="color: #AA0707;"> 
				<fmt:message key="bsCliente.formulario.etiqueta.Limitexarchivo"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="32" alt="numeric" type="text" id="limitexarchivo" name="limitexarchivo"  value="${bsCliente.limitexarchivo}" size="10" maxlength="19"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsCliente/ayudaPropiedadJson','limitexarchivo')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['limitexarchivo'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['limitexlote']}">	
		<div class="row">
			<span class="label" style="color: #AA0707;"> 
				<fmt:message key="bsCliente.formulario.etiqueta.Limitexlote"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="33" alt="numeric" type="text" id="limitexlote" name="limitexlote"  value="${bsCliente.limitexlote}" size="10" maxlength="19"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsCliente/ayudaPropiedadJson','limitexlote')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['limitexlote'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['limitextransaccion']}">	
		<div class="row">
			<span class="label" style="color: #AA0707;"> 
				<fmt:message key="bsCliente.formulario.etiqueta.Limitextransaccion"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="34" alt="numeric" type="text" id="limitextransaccion" name="limitextransaccion"  value="${bsCliente.limitextransaccion}" size="10" maxlength="19"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsCliente/ayudaPropiedadJson','limitextransaccion')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['limitextransaccion'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['permitedebitos']}">	
		<div class="row">
			<span class="label" style="color: #AA0707;"> 
				<fmt:message key="bsCliente.formulario.etiqueta.Permitedebitos"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="35" type="text" id="permitedebitos" name="permitedebitos"  value="${bsCliente.permitedebitos}" size="50"  maxlength="1"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsCliente/ayudaPropiedadJson','permitedebitos')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['permitedebitos'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['limitecreditos']}">	
		<div class="row">
			<span class="label" style="color: #AA0707;"> 
				<fmt:message key="bsCliente.formulario.etiqueta.Limitecreditos"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="36" alt="numeric" type="text" id="limitecreditos" name="limitecreditos"  value="${bsCliente.limitecreditos}" size="10" maxlength="19"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsCliente/ayudaPropiedadJson','limitecreditos')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['limitecreditos'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['limitedebitos']}">	
		<div class="row">
			<span class="label" style="color: #AA0707;"> 
				<fmt:message key="bsCliente.formulario.etiqueta.Limitedebitos"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="37" alt="numeric" type="text" id="limitedebitos" name="limitedebitos"  value="${bsCliente.limitedebitos}" size="10" maxlength="19"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsCliente/ayudaPropiedadJson','limitedebitos')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['limitedebitos'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['comisiontrnach']}">	
		<div class="row">
			<span class="label" style="color: #AA0707;"> 
				<fmt:message key="bsCliente.formulario.etiqueta.Comisiontrnach"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="38" alt="numeric" type="text" id="comisiontrnach" name="comisiontrnach"  value="${bsCliente.comisiontrnach}" size="10" maxlength="19"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsCliente/ayudaPropiedadJson','comisiontrnach')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['comisiontrnach'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
		</div>
</div>		
</div>
</form>

<%@ include file="/WEB-INF/global/jsp/jq-footer.jsp" %>
</fmt:bundle>
