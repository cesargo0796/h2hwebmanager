<%@ taglib prefix="c"       uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt"     uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:if test="${ not empty locale}">
	<fmt:setLocale value="${ locale }" />
</c:if>
<fmt:bundle basename="com.ofbox.davivienda.h2h.resourceBundle.MessagesResources">
<c:if test="${ not empty bsLotepeEstado.enlaceDetalle}">
	<c:set var="detalle" value="${bsLotepeEstado.enlaceDetalle}"/>	
	<c:set var="detalleEnllavador" value="${detalle.enllavador}"/>
</c:if>		

<c:set var="nombre" value="bsLotepe"/>
<c:set var="ubicacion" value="ver"/>

<%@ include file="/WEB-INF/global/jsp/jq-header.jsp" %>

<form  name="mainForm" id="mainForm" action="${NewtPageRenderData.actionUrl}" method="post">
<c:if test="${ not empty detalle.cabecera}">
    <jsp:include page="/WEB-INF/jsp/${detalle.cabecera.locacion}/jq/cabecera.jsp" />
</c:if>
<div id="mainContent">
	<div class="breadCrumbHolder module" style="margin-bottom: 3px;">
<div id="breadcrumb" class="breadCrumb module ui-widget ui-widget-content" style="width: 99%;">
<ul>
	<li><a href="${ctx}/inicio/blank/ria"> HOME </a> </li>
<c:if test="${ not empty ubicacionAplicacion.ruta }">
	<input type="hidden" name="idUbicacionAplicacionActual" id="idUbicacionAplicacionActual" value="${ ubicacionAplicacion.idUbicacionAplicacionActual }"/>
	<c:forEach items="${ ubicacionAplicacion.ruta }" var="ub">
		<li><a href="javascript: doSubmitBreadcrumb('<c:out value="${ ub.submit }"/>', '<c:out value="${ ub.id }"/>')"><c:out value="${ ub.label }" escapeXml="false" /></a>  </li> 
	</c:forEach>			
</c:if>
	<li><fmt:message key="bsLotepe.bc.etiqueta.ver" /></li>
</ul>
</div>
	<input type="hidden" name="id_ubicacion_retorno" id="id_ubicacion_retorno" value=""/>
</div>
	
<%--<div id="iTitle" class="ui-state-default ui-widget-content ui-corner-all">
	<fmt:message key="bsLotepe.formulario.etiqueta.ver.titulo"/>
</div>--%>

    <div id="iProcessing" style="visibility: hidden" align="center">
    	<img src="${ctx}/include/images/design/processing_request.gif"></img>
    </div>
	<input type="hidden" id="pageLoc" name="pageLoc" value="ver"/>
    <input type="hidden" id="eag" name="eag" value="${eag}"/>
    <input type="hidden" id="movimientoPagina" name="movimientoPagina"/>
    <input type="hidden" id="columna" name="columna"/>
    <input type="hidden" id="fenixf31seguridad" name="fenixf31seguridad" value="<c:out value='${fenixf31seguridad}' />" />
	<!-- valores de llaves -->
	
	
	<input type="hidden" id="nombreBusqueda" name="nombreBusqueda"/>

<div id="action-buttons">
<div id="action-bar">
	<button id="btnCancelar" type="button" name="regresar" onClick="doSubmit<c:if test="${ not empty detalle.cabecera}">Async</c:if>('bsLotepe/cancelarVer');"><fmt:message key="globales.formulario.boton.regresar"/></button>
</div>
</div>
<br />

<!-- datos de formulario -->
<div class="ui-widget ui-widget-content ui-corner-all" style="width: 99%">
		<div id="formInputHeader" class="ui-widget ui-widget-header ui-corner-top" style="padding-left: 5px;"> 
			<fmt:message key="bsLotepe.formulario.etiqueta.ver.titulo"/> 
		</div>
		
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsLotepe.formulario.etiqueta.Tipoconvenio"/>
			</span>	 
			
			<span class="formvalue">
	            [${bsLotepe.tipoconvenio}] - ${bsLotepe.bsConvenio.nombre}
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsLotepe.formulario.etiqueta.Convenio"/>
			</span>	 
			
			<span class="formvalue">
                ${bsLotepe.convenio}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsLotepe.formulario.etiqueta.Empresaodepto"/>
			</span>	 
			
			<span class="formvalue">
                ${bsLotepe.empresaodepto}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsLotepe.formulario.etiqueta.Tipolote"/>
			</span>	 
			
			<span class="formvalue">
                ${bsLotepe.tipolote}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsLotepe.formulario.etiqueta.Montototal"/>
			</span>	 
			
			<span class="formvalue">
                ${bsLotepe.montototal}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsLotepe.formulario.etiqueta.Numoperaciones"/>
			</span>	 
			
			<span class="formvalue">
                ${bsLotepe.numoperaciones}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsLotepe.formulario.etiqueta.Nombrelote"/>
			</span>	 
			
			<span class="formvalue">
                ${bsLotepe.nombrelote}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsLotepe.formulario.etiqueta.Fechaenviado"/>
			</span>	 
			
			<span class="formvalue">
                <fmt:formatDate pattern="dd-MM-yyyy" value="${bsLotepe.fechaenviado}" />
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsLotepe.formulario.etiqueta.Fecharecibido"/>
			</span>	 
			
			<span class="formvalue">
                <fmt:formatDate pattern="dd-MM-yyyy" value="${bsLotepe.fecharecibido}" />
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsLotepe.formulario.etiqueta.Fechaaplicacion"/>
			</span>	 
			
			<span class="formvalue">
                <fmt:formatDate pattern="dd-MM-yyyy" value="${bsLotepe.fechaaplicacion}" />
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsLotepe.formulario.etiqueta.Fechaestatus"/>
			</span>	 
			
			<span class="formvalue">
                <fmt:formatDate pattern="dd-MM-yyyy" value="${bsLotepe.fechaestatus}" />
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsLotepe.formulario.etiqueta.Estatus"/>
			</span>	 
			
			<span class="formvalue">
                ${bsLotepe.estatus}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsLotepe.formulario.etiqueta.Numeroreintento"/>
			</span>	 
			
			<span class="formvalue">
                ${bsLotepe.numeroreintento}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsLotepe.formulario.etiqueta.Usuarioingreso"/>
			</span>	 
			
			<span class="formvalue">
                ${bsLotepe.usuarioingreso}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsLotepe.formulario.etiqueta.Autorizaciones"/>
			</span>	 
			
			<span class="formvalue">
                ${bsLotepe.autorizaciones}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsLotepe.formulario.etiqueta.Aplicaciondebitohost"/>
			</span>	 
			
			<span class="formvalue">
                ${bsLotepe.aplicaciondebitohost}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsLotepe.formulario.etiqueta.SysMarcatiempo"/>
			</span>	 
			
			<span class="formvalue">
                <fmt:formatDate pattern="dd-MM-yyyy" value="${bsLotepe.sysMarcatiempo}" />
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsLotepe.formulario.etiqueta.SysProcessid"/>
			</span>	 
			
			<span class="formvalue">
                ${bsLotepe.sysProcessid}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsLotepe.formulario.etiqueta.SysLowdatetime"/>
			</span>	 
			
			<span class="formvalue">
                ${bsLotepe.sysLowdatetime}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsLotepe.formulario.etiqueta.SysHighdatetime"/>
			</span>	 
			
			<span class="formvalue">
                ${bsLotepe.sysHighdatetime}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsLotepe.formulario.etiqueta.Ponderacion"/>
			</span>	 
			
			<span class="formvalue">
                ${bsLotepe.ponderacion}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsLotepe.formulario.etiqueta.Firmas"/>
			</span>	 
			
			<span class="formvalue">
                ${bsLotepe.firmas}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsLotepe.formulario.etiqueta.Fechaenviohost"/>
			</span>	 
			
			<span class="formvalue">
                <fmt:formatDate pattern="dd-MM-yyyy" value="${bsLotepe.fechaenviohost}" />
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsLotepe.formulario.etiqueta.Listoparahost"/>
			</span>	 
			
			<span class="formvalue">
                <fmt:formatDate pattern="dd-MM-yyyy" value="${bsLotepe.listoparahost}" />
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsLotepe.formulario.etiqueta.Fechahoraprocesado"/>
			</span>	 
			
			<span class="formvalue">
                <fmt:formatDate pattern="dd-MM-yyyy" value="${bsLotepe.fechahoraprocesado}" />
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsLotepe.formulario.etiqueta.Token"/>
			</span>	 
			
			<span class="formvalue">
                ${bsLotepe.token}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsLotepe.formulario.etiqueta.Comentariorechazo"/>
			</span>	 
			
			<span class="formvalue">
                ${bsLotepe.comentariorechazo}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsLotepe.formulario.etiqueta.Montoimpuesto"/>
			</span>	 
			
			<span class="formvalue">
                ${bsLotepe.montoimpuesto}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
		
</div>		
</div>
</form>

<%@ include file="/WEB-INF/global/jsp/jq-footer.jsp" %>
</fmt:bundle>
