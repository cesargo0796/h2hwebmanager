<%@ taglib prefix="c"       uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt"     uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:if test="${ not empty locale}">
	<fmt:setLocale value="${locale}" />
</c:if>
<fmt:bundle basename="com.ofbox.davivienda.h2h.resourceBundle.MessagesResources">
	
<c:if test="${ not empty bsLotepeEstado.enlaceDetalle}">
	<c:set var="detalle" value="${bsLotepeEstado.enlaceDetalle}"/>	
	<c:set var="detalleEnllavador" value="${detalle.enllavador}"/>
</c:if>

<c:set var="nombre" value="bsLotepe"/>
<c:set var="ubicacion" value="crear"/>
<c:set var="primerCampo" value="btnbsConvenio"/>
<%@ include file="/WEB-INF/global/jsp/jq-header.jsp" %>

	
<form  name="mainForm" id="mainForm" action="${NewtPageRenderData.actionUrl}" method="post">
<c:if test="${ not empty detalle.cabecera}">
    <jsp:include page="/WEB-INF/jsp/${detalle.cabecera.locacion}/jq/cabecera.jsp" />
</c:if>
<div id="mainContent">
	<div class="breadCrumbHolder module" style="margin-bottom: 3px;">
    <div id="breadcrumb" class="breadCrumb module ui-widget ui-widget-content" style="width: 99%;">
    <ul>
    	<li><a href="${ctx}/inicio/blank/ria"> HOME </a> </li>
    <c:if test="${ not empty ubicacionAplicacion.ruta }">
    	<input type="hidden" name="idUbicacionAplicacionActual" id="idUbicacionAplicacionActual" value="${ ubicacionAplicacion.idUbicacionAplicacionActual }"/>
    	<c:forEach items="${ ubicacionAplicacion.ruta }" var="ub">
    		<li><a href="javascript: doSubmitBreadcrumb('<c:out value="${ ub.submit }"/>', '<c:out value="${ ub.id }"/>')"><c:out value="${ ub.label }" escapeXml="false" /></a>  </li> 
    	</c:forEach>			
    </c:if>
    	<li><fmt:message key="bsLotepe.bc.etiqueta.crear" /></li>
    </ul>
    </div>
    	<input type="hidden" name="id_ubicacion_retorno" id="id_ubicacion_retorno" value=""/>
    </div>	
	
	
<%-- <div id="iTitle" class="ui-state-default ui-widget-content ui-corner-all">
	<fmt:message key="bsLotepe.formulario.etiqueta.crear.titulo"/>
</div> --%>
	<input type="hidden" id="pageLoc" name="pageLoc" value="crear"/>
	<input type="hidden" id="primerCampo" name="primerCampo" value="btnbsConvenio"/>
    <input type="hidden" id="eag" name="eag" value="${eag}"/>
    <input type="hidden" id="movimientoPagina" name="movimientoPagina"/>
    <input type="hidden" id="columna" name="columna"/>
    <input type="hidden" id="fenixf31seguridad" name="fenixf31seguridad" value="<c:out value='${fenixf31seguridad}' />" />
	<c:if test="${not empty scrollTo }">
    <input type="hidden" id="scrollSmooth" value="<c:out value='${scrollTo}' />" />
    </c:if>
	<!-- valores de llaves -->
	
	
	<input type="hidden" id="nombreBusqueda" name="nombreBusqueda"/>


<!-- mensajes de error-->
<c:if test="${mensajesUsuario.erroresGlobales}">
<div class="ui-state-error ui-corner-all" style="padding: 0 .7em; font-size: 0.8em;"> 
	<span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em; margin-top: .3em;"></span> 
	<strong><fmt:message key="globales.formulario.etiqueta.error"/></strong> <br /> 
	<ul>
		<c:forEach items="${mensajesUsuario.mensajesDeError}" var="mensaje">
        	<li><c:out value="${mensaje}" escapeXml="false"/></li>
     	</c:forEach>
	</ul>
</div>
</c:if>

<div id="action-buttons">
<div id="action-bar">
    <button tabindex="29" id="btnGuardar" type="submit" name="ok" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsLotepe/crearEjecutar');"><fmt:message key="globales.formulario.boton.crear.guardar"/></button>
	<button tabindex="30" id="btnCancelar" type="button" name="cancel" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsLotepe/cancelarCrear');"><fmt:message key="globales.formulario.boton.crear.cancelar"/></button>
</div>
</div>
<br />

<!-- datos de formulario -->
<div><span style="font-size: small;">-Los campos marcados con * son obligatorios</span></div>
<div class="ui-widget ui-widget-content ui-corner-all" style="width: 99%">
		<div id="formInputHeader" class="ui-widget ui-widget-header ui-corner-top" style="padding-left: 5px;"> 
			<fmt:message key="bsLotepe.formulario.etiqueta.crear.titulo"/> 
		</div>
		<div style="display: inline;" id="formInput">
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['tipoconvenio']}">	
		<div class="row" >
			<span class="label" style="color: #AA0707;"> 
                <fmt:message key="bsLotepe.formulario.etiqueta.Tipoconvenio"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
	            <input readonly="readonly" type="text" id="tipoconvenio" name="tipoconvenio" value="${bsLotepe.tipoconvenio}"  size="3"  />
				<input readonly="readonly" type="text" id="bsConvenio.nombre" name="bsConvenio.nombre" value="${bsLotepe.bsConvenio.nombre}"  size="40"  />
				<button tabindex="1" id="btnbsConvenio" class="sq" onClick="doForaneo('bsConvenio', 'bsLotepe/buscar');"><fmt:message key='globales.formulario.boton.lov'/></button>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsLotepe/ayudaPropiedadJson','tipoconvenio')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['tipoconvenio'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['convenio']}">	
		<div class="row" >
			<span class="label" style="color: #AA0707;"> 
                <fmt:message key="bsLotepe.formulario.etiqueta.Convenio"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="2" alt="integer" type="text" id="convenio" name="convenio"  value="${bsLotepe.convenio}" size="10" maxlength="10"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsLotepe/ayudaPropiedadJson','convenio')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['convenio'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['empresaodepto']}">	
		<div class="row" >
			<span class="label" style="color: #AA0707;"> 
                <fmt:message key="bsLotepe.formulario.etiqueta.Empresaodepto"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="3" alt="integer" type="text" id="empresaodepto" name="empresaodepto"  value="${bsLotepe.empresaodepto}" size="10" maxlength="10"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsLotepe/ayudaPropiedadJson','empresaodepto')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['empresaodepto'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['tipolote']}">	
		<div class="row" >
			<span class="label" > 
                <fmt:message key="bsLotepe.formulario.etiqueta.Tipolote"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="4" alt="integer" type="text" id="tipolote" name="tipolote"  value="${bsLotepe.tipolote}" size="10" maxlength="3"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsLotepe/ayudaPropiedadJson','tipolote')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['tipolote'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['montototal']}">	
		<div class="row" >
			<span class="label" style="color: #AA0707;"> 
                <fmt:message key="bsLotepe.formulario.etiqueta.Montototal"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="5" alt="numeric" type="text" id="montototal" name="montototal"  value="${bsLotepe.montototal}" size="10" maxlength="19"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsLotepe/ayudaPropiedadJson','montototal')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['montototal'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['numoperaciones']}">	
		<div class="row" >
			<span class="label" style="color: #AA0707;"> 
                <fmt:message key="bsLotepe.formulario.etiqueta.Numoperaciones"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="6" alt="integer" type="text" id="numoperaciones" name="numoperaciones"  value="${bsLotepe.numoperaciones}" size="10" maxlength="10"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsLotepe/ayudaPropiedadJson','numoperaciones')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['numoperaciones'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['nombrelote']}">	
		<div class="row" >
			<span class="label" > 
                <fmt:message key="bsLotepe.formulario.etiqueta.Nombrelote"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="7" type="text" id="nombrelote" name="nombrelote"  value="${bsLotepe.nombrelote}" size="50"  maxlength="30"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsLotepe/ayudaPropiedadJson','nombrelote')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['nombrelote'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['fechaenviado']}">	
		<div class="row" >
			<span class="label" style="color: #AA0707;"> 
                <fmt:message key="bsLotepe.formulario.etiqueta.Fechaenviado"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="8" type="text" cdate="fechaenviado" id="fechaenviado" name="fechaenviado"  value='<fmt:formatDate value="${bsLotepe.fechaenviado}" pattern="dd-MM-yyyy"/>' size="12" />
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsLotepe/ayudaPropiedadJson','fechaenviado')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['fechaenviado'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['fecharecibido']}">	
		<div class="row" >
			<span class="label" > 
                <fmt:message key="bsLotepe.formulario.etiqueta.Fecharecibido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="9" type="text" cdate="fecharecibido" id="fecharecibido" name="fecharecibido"  value='<fmt:formatDate value="${bsLotepe.fecharecibido}" pattern="dd-MM-yyyy"/>' size="12" />
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsLotepe/ayudaPropiedadJson','fecharecibido')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['fecharecibido'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['fechaaplicacion']}">	
		<div class="row" >
			<span class="label" > 
                <fmt:message key="bsLotepe.formulario.etiqueta.Fechaaplicacion"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="10" type="text" cdate="fechaaplicacion" id="fechaaplicacion" name="fechaaplicacion"  value='<fmt:formatDate value="${bsLotepe.fechaaplicacion}" pattern="dd-MM-yyyy"/>' size="12" />
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsLotepe/ayudaPropiedadJson','fechaaplicacion')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['fechaaplicacion'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['fechaestatus']}">	
		<div class="row" >
			<span class="label" > 
                <fmt:message key="bsLotepe.formulario.etiqueta.Fechaestatus"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="11" type="text" cdate="fechaestatus" id="fechaestatus" name="fechaestatus"  value='<fmt:formatDate value="${bsLotepe.fechaestatus}" pattern="dd-MM-yyyy"/>' size="12" />
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsLotepe/ayudaPropiedadJson','fechaestatus')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['fechaestatus'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['estatus']}">	
		<div class="row" >
			<span class="label" style="color: #AA0707;"> 
                <fmt:message key="bsLotepe.formulario.etiqueta.Estatus"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="12" alt="integer" type="text" id="estatus" name="estatus"  value="${bsLotepe.estatus}" size="10" maxlength="3"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsLotepe/ayudaPropiedadJson','estatus')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['estatus'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['numeroreintento']}">	
		<div class="row" >
			<span class="label" > 
                <fmt:message key="bsLotepe.formulario.etiqueta.Numeroreintento"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="13" alt="integer" type="text" id="numeroreintento" name="numeroreintento"  value="${bsLotepe.numeroreintento}" size="10" maxlength="3"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsLotepe/ayudaPropiedadJson','numeroreintento')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['numeroreintento'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['usuarioingreso']}">	
		<div class="row" >
			<span class="label" > 
                <fmt:message key="bsLotepe.formulario.etiqueta.Usuarioingreso"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="14" type="text" id="usuarioingreso" name="usuarioingreso"  value="${bsLotepe.usuarioingreso}" size="50"  maxlength="10"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsLotepe/ayudaPropiedadJson','usuarioingreso')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['usuarioingreso'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['autorizaciones']}">	
		<div class="row" >
			<span class="label" > 
                <fmt:message key="bsLotepe.formulario.etiqueta.Autorizaciones"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="15" alt="integer" type="text" id="autorizaciones" name="autorizaciones"  value="${bsLotepe.autorizaciones}" size="10" maxlength="3"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsLotepe/ayudaPropiedadJson','autorizaciones')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['autorizaciones'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['aplicaciondebitohost']}">	
		<div class="row" >
			<span class="label" > 
                <fmt:message key="bsLotepe.formulario.etiqueta.Aplicaciondebitohost"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="16" alt="integer" type="text" id="aplicaciondebitohost" name="aplicaciondebitohost"  value="${bsLotepe.aplicaciondebitohost}" size="10" maxlength="3"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsLotepe/ayudaPropiedadJson','aplicaciondebitohost')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['aplicaciondebitohost'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['sysMarcatiempo']}">	
		<div class="row" >
			<span class="label" > 
                <fmt:message key="bsLotepe.formulario.etiqueta.SysMarcatiempo"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="17" type="text" cdate="sysMarcatiempo" id="sysMarcatiempo" name="sysMarcatiempo"  value='<fmt:formatDate value="${bsLotepe.sysMarcatiempo}" pattern="dd-MM-yyyy"/>' size="12" />
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsLotepe/ayudaPropiedadJson','sysMarcatiempo')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['sysMarcatiempo'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['sysProcessid']}">	
		<div class="row" >
			<span class="label" > 
                <fmt:message key="bsLotepe.formulario.etiqueta.SysProcessid"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="18" alt="integer" type="text" id="sysProcessid" name="sysProcessid"  value="${bsLotepe.sysProcessid}" size="10" maxlength="10"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsLotepe/ayudaPropiedadJson','sysProcessid')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['sysProcessid'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['sysLowdatetime']}">	
		<div class="row" >
			<span class="label" > 
                <fmt:message key="bsLotepe.formulario.etiqueta.SysLowdatetime"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="19" alt="integer" type="text" id="sysLowdatetime" name="sysLowdatetime"  value="${bsLotepe.sysLowdatetime}" size="10" maxlength="10"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsLotepe/ayudaPropiedadJson','sysLowdatetime')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['sysLowdatetime'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['sysHighdatetime']}">	
		<div class="row" >
			<span class="label" > 
                <fmt:message key="bsLotepe.formulario.etiqueta.SysHighdatetime"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="20" alt="integer" type="text" id="sysHighdatetime" name="sysHighdatetime"  value="${bsLotepe.sysHighdatetime}" size="10" maxlength="10"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsLotepe/ayudaPropiedadJson','sysHighdatetime')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['sysHighdatetime'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['ponderacion']}">	
		<div class="row" >
			<span class="label" style="color: #AA0707;"> 
                <fmt:message key="bsLotepe.formulario.etiqueta.Ponderacion"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="21" alt="integer" type="text" id="ponderacion" name="ponderacion"  value="${bsLotepe.ponderacion}" size="10" maxlength="10"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsLotepe/ayudaPropiedadJson','ponderacion')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['ponderacion'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['firmas']}">	
		<div class="row" >
			<span class="label" style="color: #AA0707;"> 
                <fmt:message key="bsLotepe.formulario.etiqueta.Firmas"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="22" alt="integer" type="text" id="firmas" name="firmas"  value="${bsLotepe.firmas}" size="10" maxlength="10"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsLotepe/ayudaPropiedadJson','firmas')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['firmas'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['fechaenviohost']}">	
		<div class="row" >
			<span class="label" > 
                <fmt:message key="bsLotepe.formulario.etiqueta.Fechaenviohost"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="23" type="text" cdate="fechaenviohost" id="fechaenviohost" name="fechaenviohost"  value='<fmt:formatDate value="${bsLotepe.fechaenviohost}" pattern="dd-MM-yyyy"/>' size="12" />
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsLotepe/ayudaPropiedadJson','fechaenviohost')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['fechaenviohost'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['listoparahost']}">	
		<div class="row" >
			<span class="label" > 
                <fmt:message key="bsLotepe.formulario.etiqueta.Listoparahost"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="24" type="text" cdate="listoparahost" id="listoparahost" name="listoparahost"  value='<fmt:formatDate value="${bsLotepe.listoparahost}" pattern="dd-MM-yyyy"/>' size="12" />
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsLotepe/ayudaPropiedadJson','listoparahost')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['listoparahost'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['fechahoraprocesado']}">	
		<div class="row" >
			<span class="label" > 
                <fmt:message key="bsLotepe.formulario.etiqueta.Fechahoraprocesado"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="25" type="text" cdate="fechahoraprocesado" id="fechahoraprocesado" name="fechahoraprocesado"  value='<fmt:formatDate value="${bsLotepe.fechahoraprocesado}" pattern="dd-MM-yyyy"/>' size="12" />
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsLotepe/ayudaPropiedadJson','fechahoraprocesado')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['fechahoraprocesado'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['token']}">	
		<div class="row" >
			<span class="label" > 
                <fmt:message key="bsLotepe.formulario.etiqueta.Token"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="26" alt="integer" type="text" id="token" name="token"  value="${bsLotepe.token}" size="10" maxlength="10"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsLotepe/ayudaPropiedadJson','token')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['token'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['comentariorechazo']}">	
		<div class="row" >
			<span class="label" > 
                <fmt:message key="bsLotepe.formulario.etiqueta.Comentariorechazo"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="27" type="text" id="comentariorechazo" name="comentariorechazo"  value="${bsLotepe.comentariorechazo}" size="50"  maxlength="100"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsLotepe/ayudaPropiedadJson','comentariorechazo')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['comentariorechazo'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['montoimpuesto']}">	
		<div class="row" >
			<span class="label" > 
                <fmt:message key="bsLotepe.formulario.etiqueta.Montoimpuesto"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="28" alt="numeric" type="text" id="montoimpuesto" name="montoimpuesto"  value="${bsLotepe.montoimpuesto}" size="10" maxlength="19"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsLotepe/ayudaPropiedadJson','montoimpuesto')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['montoimpuesto'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
		</div>
</div>		
</div>
</form>

<%@ include file="/WEB-INF/global/jsp/jq-footer.jsp" %>
</fmt:bundle>
