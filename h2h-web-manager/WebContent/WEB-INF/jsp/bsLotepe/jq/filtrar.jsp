<%@ taglib prefix="c"       uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt"     uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:if test="${ not empty locale}">
	<fmt:setLocale value="${ locale }" />
</c:if>
<fmt:bundle basename="com.ofbox.davivienda.h2h.resourceBundle.MessagesResources">
<c:if test="${ not empty bsLotepeEstado.enlaceDetalle}">
	<c:set var="detalle" value="${bsLotepeEstado.enlaceDetalle}"/>	
	<c:set var="detalleEnllavador" value="${detalle.enllavador}"/>
</c:if>	

<c:set var="nombre" value="bsLotepe"/>
<c:set var="ubicacion" value="filtrar"/>
<c:set var="primerCampo" value="btnbsConvenio"/>
<%@ include file="/WEB-INF/global/jsp/jq-header.jsp" %>

<form  name="mainForm" id="mainForm" action="${NewtPageRenderData.actionUrl}" method="post">
<c:if test="${ not empty detalle.cabecera}">
    <jsp:include page="/WEB-INF/jsp/${detalle.cabecera.locacion}/jq/cabecera.jsp" />
</c:if>	
<div id="mainContent">
	<div class="breadCrumbHolder module" style="margin-bottom: 3px;">
    <div id="breadcrumb" class="breadCrumb module ui-widget ui-widget-content" style="width: 99%;">
    <ul>
    	<li><a href="${ctx}/inicio/blank/ria"> HOME </a> </li>
    <c:if test="${ not empty ubicacionAplicacion.ruta }">
    	<input type="hidden" name="idUbicacionAplicacionActual" id="idUbicacionAplicacionActual" value="${ ubicacionAplicacion.idUbicacionAplicacionActual }"/>
    	<c:forEach items="${ ubicacionAplicacion.ruta }" var="ub">
    		<li><a href="javascript: doSubmitBreadcrumb('<c:out value="${ ub.submit }"/>', '<c:out value="${ ub.id }"/>')"><c:out value="${ ub.label }" escapeXml="false" /></a>  </li> 
    	</c:forEach>			
    </c:if>
    	<li><fmt:message key="bsLotepe.bc.etiqueta.filtrar" /></li>
    </ul>
    </div>
    	<input type="hidden" name="id_ubicacion_retorno" id="id_ubicacion_retorno" value=""/>
    </div>
	
<%--<div id="iTitle" class="ui-state-default ui-widget-content ui-corner-all">
	<fmt:message key="bsLotepe.formulario.etiqueta.filtrar.titulo"/>
</div>--%>
<input type="hidden" id="pageLoc" name="pageLoc" value="filtrar"/>
	<input type="hidden" id="primerCampo" name="primerCampo" value="btnbsConvenio"/>
    <input type="hidden" id="eag" name="eag" value="${eag}"/>
    <input type="hidden" id="movimientoPagina" name="movimientoPagina"/>
    <input type="hidden" id="columna" name="columna"/>
    <input type="hidden" id="fenixf31seguridad" name="fenixf31seguridad" value="<c:out value='${fenixf31seguridad}' />" />
	<c:if test="${not empty scrollTo }">
    <input type="hidden" id="scrollSmooth" value="<c:out value='${scrollTo}' />" />
    </c:if>
	<!-- valores de llaves -->
<input type="hidden" id="nombreBusqueda" name="nombreBusqueda"/>


<!-- mensajes de error-->
<c:if test="${mensajesUsuario.erroresGlobales}">
<div id="iError">
	<b class="ui-round-top"> 
	<b class="ui-line-one"></b><b class="ui-line-two"></b><b class="ui-line-three"></b><b class="ui-line-four"></b> 
    </b> 
  	<table>
     	<tr>
       		<td>
         	<ol>
	     	<c:forEach items="${mensajesUsuario.mensajesDeError}" var="mensaje">
	        	<li><c:out value="${mensaje}" escapeXml="false"/></li>
	     	</c:forEach>
	     	</ol>
       		</td>
     	</tr>
  	</table>
  	<b class="ui-round-bottom"> 
	<b class="ui-line-four"></b><b class="ui-line-three"></b><b class="ui-line-two"></b><b class="ui-line-one"></b> 
  	</b> 
</div>
</c:if>

<div id="action-buttons">
<div id="action-bar">
	<button tabindex="57" id="btnAplicarFiltro" type="submit" name="Filtrar" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsLotepe/filtrarEjecutar');"><fmt:message key="globales.filtro.boton.aplicarFiltro"/></button>
	<button tabindex="58" id="btnLimpiar" type="button" name="Limpiar" onClick="limpiarCampos('mainForm');"><fmt:message key="globales.filtro.boton.limpiar"/></button>
	<button tabindex="59" id="btnCancelar" type="button" name="Cancelar" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsLotepe/cancelarFiltrar');"><fmt:message key="globales.filtro.boton.cancelar"/></button>
</div>
</div>
<br />

<!-- datos de formulario -->
<div class="ui-widget ui-widget-content ui-corner-all" style="width: 99%">
		<div id="formInputHeader" class="ui-widget ui-widget-header ui-corner-top" style="padding-left: 5px;"> 
			<fmt:message key="bsLotepe.formulario.etiqueta.filtrar.titulo"/> 
		</div>
		<div style="display: inline;" id="formInput">
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['tipoconvenio']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsLotepe.filtro.etiqueta.Tipoconvenio"/>
			</span>
			<span class="forminput" style="display: inline">
	            	            <input type="text" id="tipoconvenio" name="tipoconvenio" value="${bsLotepeFiltro['bsConvenio'].tipoconvenio}"  size="3"  readonly="readonly"/>
                <input type="text" id="bsConvenio.nombre" name="bsConvenio.nombre" value="${bsLotepeFiltro['bsConvenio'].nombre}"  size="40"  readonly="readonly"/>		
				<button tabindex="3" id="btnbsConvenio" class="sq" onClick="doForaneo('bsConvenio', 'bsLotepe/buscar');"><fmt:message key='globales.formulario.boton.lov'/></button>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['tipoconvenio'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['convenio']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsLotepe.filtro.etiqueta.Convenio"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="4" alt="integerMin" type="text" id="convenioMin" name="convenioMin" value="${bsLotepeFiltro['convenioMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="4" alt="integerMax" type="text" id="convenioMax" name="convenioMax" value="${bsLotepeFiltro['convenioMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['convenio'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['empresaodepto']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsLotepe.filtro.etiqueta.Empresaodepto"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="5" alt="integerMin" type="text" id="empresaodeptoMin" name="empresaodeptoMin" value="${bsLotepeFiltro['empresaodeptoMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="5" alt="integerMax" type="text" id="empresaodeptoMax" name="empresaodeptoMax" value="${bsLotepeFiltro['empresaodeptoMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['empresaodepto'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['tipolote']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsLotepe.filtro.etiqueta.Tipolote"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="6" alt="integerMin" type="text" id="tipoloteMin" name="tipoloteMin" value="${bsLotepeFiltro['tipoloteMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="6" alt="integerMax" type="text" id="tipoloteMax" name="tipoloteMax" value="${bsLotepeFiltro['tipoloteMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['tipolote'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['montototal']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsLotepe.filtro.etiqueta.Montototal"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="7" alt="numericMin" type="text" id="montototalMin" name="montototalMin" value="${bsLotepeFiltro['montototalMin']}"/>
				&nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
				<input tabindex="7" alt="numericMax" type="text" id="montototalMax" name="montototalMax" value="${bsLotepeFiltro['montototalMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['montototal'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['numoperaciones']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsLotepe.filtro.etiqueta.Numoperaciones"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="8" alt="integerMin" type="text" id="numoperacionesMin" name="numoperacionesMin" value="${bsLotepeFiltro['numoperacionesMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="8" alt="integerMax" type="text" id="numoperacionesMax" name="numoperacionesMax" value="${bsLotepeFiltro['numoperacionesMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['numoperaciones'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['nombrelote']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsLotepe.filtro.etiqueta.Nombrelote"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="9" type="text" id="nombrelote" name="nombrelote" value="${bsLotepeFiltro['nombrelote']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['nombrelote'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['fechaenviado']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsLotepe.filtro.etiqueta.Fechaenviado"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="10" type="text" cdate="fechaenviadoMax" cattr="minDate" id="fechaenviadoMin" name="fechaenviadoMin" value="<fmt:formatDate pattern="dd-MM-yyyy" value="${bsLotepeFiltro['fechaenviadoMin']}"/>"/>
				&nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
				<input tabindex="10" type="text" cdate="fechaenviadoMin" cattr="maxDate" id="fechaenviadoMax" name="fechaenviadoMax" value="<fmt:formatDate pattern="dd-MM-yyyy" value="${bsLotepeFiltro['fechaenviadoMax']}"/>"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['fechaenviado'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['fecharecibido']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsLotepe.filtro.etiqueta.Fecharecibido"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="11" type="text" cdate="fecharecibidoMax" cattr="minDate" id="fecharecibidoMin" name="fecharecibidoMin" value="<fmt:formatDate pattern="dd-MM-yyyy" value="${bsLotepeFiltro['fecharecibidoMin']}"/>"/>
				&nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
				<input tabindex="11" type="text" cdate="fecharecibidoMin" cattr="maxDate" id="fecharecibidoMax" name="fecharecibidoMax" value="<fmt:formatDate pattern="dd-MM-yyyy" value="${bsLotepeFiltro['fecharecibidoMax']}"/>"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['fecharecibido'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['fechaaplicacion']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsLotepe.filtro.etiqueta.Fechaaplicacion"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="12" type="text" cdate="fechaaplicacionMax" cattr="minDate" id="fechaaplicacionMin" name="fechaaplicacionMin" value="<fmt:formatDate pattern="dd-MM-yyyy" value="${bsLotepeFiltro['fechaaplicacionMin']}"/>"/>
				&nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
				<input tabindex="12" type="text" cdate="fechaaplicacionMin" cattr="maxDate" id="fechaaplicacionMax" name="fechaaplicacionMax" value="<fmt:formatDate pattern="dd-MM-yyyy" value="${bsLotepeFiltro['fechaaplicacionMax']}"/>"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['fechaaplicacion'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['fechaestatus']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsLotepe.filtro.etiqueta.Fechaestatus"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="13" type="text" cdate="fechaestatusMax" cattr="minDate" id="fechaestatusMin" name="fechaestatusMin" value="<fmt:formatDate pattern="dd-MM-yyyy" value="${bsLotepeFiltro['fechaestatusMin']}"/>"/>
				&nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
				<input tabindex="13" type="text" cdate="fechaestatusMin" cattr="maxDate" id="fechaestatusMax" name="fechaestatusMax" value="<fmt:formatDate pattern="dd-MM-yyyy" value="${bsLotepeFiltro['fechaestatusMax']}"/>"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['fechaestatus'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['estatus']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsLotepe.filtro.etiqueta.Estatus"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="14" alt="integerMin" type="text" id="estatusMin" name="estatusMin" value="${bsLotepeFiltro['estatusMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="14" alt="integerMax" type="text" id="estatusMax" name="estatusMax" value="${bsLotepeFiltro['estatusMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['estatus'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['numeroreintento']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsLotepe.filtro.etiqueta.Numeroreintento"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="15" alt="integerMin" type="text" id="numeroreintentoMin" name="numeroreintentoMin" value="${bsLotepeFiltro['numeroreintentoMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="15" alt="integerMax" type="text" id="numeroreintentoMax" name="numeroreintentoMax" value="${bsLotepeFiltro['numeroreintentoMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['numeroreintento'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['usuarioingreso']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsLotepe.filtro.etiqueta.Usuarioingreso"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="16" type="text" id="usuarioingreso" name="usuarioingreso" value="${bsLotepeFiltro['usuarioingreso']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['usuarioingreso'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['autorizaciones']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsLotepe.filtro.etiqueta.Autorizaciones"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="17" alt="integerMin" type="text" id="autorizacionesMin" name="autorizacionesMin" value="${bsLotepeFiltro['autorizacionesMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="17" alt="integerMax" type="text" id="autorizacionesMax" name="autorizacionesMax" value="${bsLotepeFiltro['autorizacionesMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['autorizaciones'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['aplicaciondebitohost']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsLotepe.filtro.etiqueta.Aplicaciondebitohost"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="18" alt="integerMin" type="text" id="aplicaciondebitohostMin" name="aplicaciondebitohostMin" value="${bsLotepeFiltro['aplicaciondebitohostMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="18" alt="integerMax" type="text" id="aplicaciondebitohostMax" name="aplicaciondebitohostMax" value="${bsLotepeFiltro['aplicaciondebitohostMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['aplicaciondebitohost'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['sysMarcatiempo']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsLotepe.filtro.etiqueta.SysMarcatiempo"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="19" type="text" cdate="sysMarcatiempoMax" cattr="minDate" id="sysMarcatiempoMin" name="sysMarcatiempoMin" value="<fmt:formatDate pattern="dd-MM-yyyy" value="${bsLotepeFiltro['sysMarcatiempoMin']}"/>"/>
				&nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
				<input tabindex="19" type="text" cdate="sysMarcatiempoMin" cattr="maxDate" id="sysMarcatiempoMax" name="sysMarcatiempoMax" value="<fmt:formatDate pattern="dd-MM-yyyy" value="${bsLotepeFiltro['sysMarcatiempoMax']}"/>"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['sysMarcatiempo'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['sysProcessid']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsLotepe.filtro.etiqueta.SysProcessid"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="20" alt="integerMin" type="text" id="sysProcessidMin" name="sysProcessidMin" value="${bsLotepeFiltro['sysProcessidMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="20" alt="integerMax" type="text" id="sysProcessidMax" name="sysProcessidMax" value="${bsLotepeFiltro['sysProcessidMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['sysProcessid'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['sysLowdatetime']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsLotepe.filtro.etiqueta.SysLowdatetime"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="21" alt="integerMin" type="text" id="sysLowdatetimeMin" name="sysLowdatetimeMin" value="${bsLotepeFiltro['sysLowdatetimeMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="21" alt="integerMax" type="text" id="sysLowdatetimeMax" name="sysLowdatetimeMax" value="${bsLotepeFiltro['sysLowdatetimeMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['sysLowdatetime'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['sysHighdatetime']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsLotepe.filtro.etiqueta.SysHighdatetime"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="22" alt="integerMin" type="text" id="sysHighdatetimeMin" name="sysHighdatetimeMin" value="${bsLotepeFiltro['sysHighdatetimeMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="22" alt="integerMax" type="text" id="sysHighdatetimeMax" name="sysHighdatetimeMax" value="${bsLotepeFiltro['sysHighdatetimeMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['sysHighdatetime'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['ponderacion']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsLotepe.filtro.etiqueta.Ponderacion"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="23" alt="integerMin" type="text" id="ponderacionMin" name="ponderacionMin" value="${bsLotepeFiltro['ponderacionMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="23" alt="integerMax" type="text" id="ponderacionMax" name="ponderacionMax" value="${bsLotepeFiltro['ponderacionMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['ponderacion'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['firmas']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsLotepe.filtro.etiqueta.Firmas"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="24" alt="integerMin" type="text" id="firmasMin" name="firmasMin" value="${bsLotepeFiltro['firmasMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="24" alt="integerMax" type="text" id="firmasMax" name="firmasMax" value="${bsLotepeFiltro['firmasMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['firmas'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['fechaenviohost']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsLotepe.filtro.etiqueta.Fechaenviohost"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="25" type="text" cdate="fechaenviohostMax" cattr="minDate" id="fechaenviohostMin" name="fechaenviohostMin" value="<fmt:formatDate pattern="dd-MM-yyyy" value="${bsLotepeFiltro['fechaenviohostMin']}"/>"/>
				&nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
				<input tabindex="25" type="text" cdate="fechaenviohostMin" cattr="maxDate" id="fechaenviohostMax" name="fechaenviohostMax" value="<fmt:formatDate pattern="dd-MM-yyyy" value="${bsLotepeFiltro['fechaenviohostMax']}"/>"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['fechaenviohost'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['listoparahost']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsLotepe.filtro.etiqueta.Listoparahost"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="26" type="text" cdate="listoparahostMax" cattr="minDate" id="listoparahostMin" name="listoparahostMin" value="<fmt:formatDate pattern="dd-MM-yyyy" value="${bsLotepeFiltro['listoparahostMin']}"/>"/>
				&nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
				<input tabindex="26" type="text" cdate="listoparahostMin" cattr="maxDate" id="listoparahostMax" name="listoparahostMax" value="<fmt:formatDate pattern="dd-MM-yyyy" value="${bsLotepeFiltro['listoparahostMax']}"/>"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['listoparahost'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['fechahoraprocesado']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsLotepe.filtro.etiqueta.Fechahoraprocesado"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="27" type="text" cdate="fechahoraprocesadoMax" cattr="minDate" id="fechahoraprocesadoMin" name="fechahoraprocesadoMin" value="<fmt:formatDate pattern="dd-MM-yyyy" value="${bsLotepeFiltro['fechahoraprocesadoMin']}"/>"/>
				&nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
				<input tabindex="27" type="text" cdate="fechahoraprocesadoMin" cattr="maxDate" id="fechahoraprocesadoMax" name="fechahoraprocesadoMax" value="<fmt:formatDate pattern="dd-MM-yyyy" value="${bsLotepeFiltro['fechahoraprocesadoMax']}"/>"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['fechahoraprocesado'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['token']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsLotepe.filtro.etiqueta.Token"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="28" alt="integerMin" type="text" id="tokenMin" name="tokenMin" value="${bsLotepeFiltro['tokenMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="28" alt="integerMax" type="text" id="tokenMax" name="tokenMax" value="${bsLotepeFiltro['tokenMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['token'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['comentariorechazo']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsLotepe.filtro.etiqueta.Comentariorechazo"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="29" type="text" id="comentariorechazo" name="comentariorechazo" value="${bsLotepeFiltro['comentariorechazo']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['comentariorechazo'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['montoimpuesto']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsLotepe.filtro.etiqueta.Montoimpuesto"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="30" alt="numericMin" type="text" id="montoimpuestoMin" name="montoimpuestoMin" value="${bsLotepeFiltro['montoimpuestoMin']}"/>
				&nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
				<input tabindex="30" alt="numericMax" type="text" id="montoimpuestoMax" name="montoimpuestoMax" value="${bsLotepeFiltro['montoimpuestoMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['montoimpuesto'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
</div>	


</div>
</div>
</form>

<%@ include file="/WEB-INF/global/jsp/jq-footer.jsp" %>
</fmt:bundle>
</body>