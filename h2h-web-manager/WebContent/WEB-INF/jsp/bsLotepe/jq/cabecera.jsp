<%@ taglib prefix="c"       uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt"     uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:if test="${ not empty locale}">
	<fmt:setLocale value="${ locale }" />
</c:if>
<fmt:bundle basename="com.ofbox.davivienda.h2h.resourceBundle.MessagesResources">
<input type="hidden" id="nombreDetalle" name="nombreDetalle" value=""/>
<!-- valores de llaves del padre -->
  <input type="hidden" id="instalacion" name="instalacion" value="${bsLotepeDtoCabeza.instalacion}"/>
  <input type="hidden" id="lote" name="lote" value="${bsLotepeDtoCabeza.lote}"/>

<c:set var="ubicacion" value="cabecera"/>
<c:if test="${bsLotepeEnlaceDetalle == 'bsDetallepe'}">
	<c:set var="nombre" value="bsDetallepe"/>
</c:if>

<script>
$(document).ready(function(){
	$('#iHeader').accordion({collapsible: true, active: 0});
	$('#backLink').button().removeClass('ui-button-text-only').css({'padding':'0.2em 0.5em', 'margin-right':'20px'}).bind("click", function(e) {e.stopPropagation();});;
	$('#backLinkMod').button().removeClass('ui-button-text-only').css({'padding':'0.2em 0.5em', 'margin-right':'20px'}).bind("click", function(e) {e.stopPropagation();});
});
</script>  

<!-- datos de formulario -->
<div id="iHeader">
	<h3><a id="headerInfo" href="#">
			<span id="backLink" onclick="doSubmit('bsLotepe/cancelarDetalle');"><fmt:message key="globales.cabecera.boton.regresar"/></span>
			<span id="backLinkMod" onclick="doSubmit('bsLotepe/actualizar');"><fmt:message key="globales.cabecera.boton.actualizar"/></span>	
			 <fmt:message key="bsLotepe.encabezado.etiqueta.titulo"/> ${bsLotepeDtoCabeza.instalacion} &nbsp;|${bsLotepeDtoCabeza.lote} &nbsp;|			<span style="font-weight: normal;"><fmt:message key="bsLotepe.encabezado.etiqueta.Tipoconvenio"/>:</span>
	        [${bsLotepeDtoCabeza.tipoconvenio}] - ${bsLotepeDtoCabeza.bsConvenio.nombre}
 &nbsp;| 			<span style="font-weight: normal;"><fmt:message key="bsLotepe.encabezado.etiqueta.Convenio"/>:</span>
		    ${bsLotepeDtoCabeza.convenio} 
 &nbsp;|                           	</a></h3>
	<div>
	    <div>
		<strong><fmt:message key="bsLotepe.encabezado.etiqueta.Tipoconvenio"/></strong>&nbsp;
		[${bsLotepeDtoCabeza.tipoconvenio}] - ${bsLotepeDtoCabeza.bsConvenio.nombre}
		</div>
		
	    <div>
		<strong><fmt:message key="bsLotepe.encabezado.etiqueta.Convenio"/></strong>&nbsp;
		${bsLotepeDtoCabeza.convenio}
		</div>
		
	</div>
</div>

<div id="iTab" class="ui-tabs ui-widget ui-widget-content ui-corner-top" >
 	<ul id="detailTabs" class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
	<c:if test="${bsLotepeEnlaceDetalle == 'bsDetallepe'}">
		<li class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active">
			<a onClick="$('#nombreDetalle').val('bsDetallepe');doSubmit('bsLotepe/detalle')" class="ui-selected"><fmt:message key="bsLotepe.encabezado.etiqueta.bsDetallepeDrillDown"/></a>
		</li>
	</c:if> 			
	<c:if test="${bsLotepeEnlaceDetalle != 'bsDetallepe'}">
		<li>
			<a onClick="$('#nombreDetalle').val('bsDetallepe');doSubmit('bsLotepe/detalle')"><fmt:message key="bsLotepe.encabezado.etiqueta.bsDetallepeDrillDown"/></a>
		</li>
	</c:if>		
	</ul>
</div>
<div id="iTabline"></div>
</fmt:bundle>