<%@ taglib prefix="c"       uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt"     uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:if test="${ not empty locale}">
	<fmt:setLocale value="${ locale }" />
</c:if>
<fmt:bundle basename="com.ofbox.davivienda.h2h.resourceBundle.MessagesResources">

<c:if test="${ not empty bsConvenioachEstado.enlaceBusqueda.enllavador}">
	<c:set var="busquedaEnllavador" value="${bsConvenioachEstado.enlaceBusqueda.enllavador}"/>	
</c:if>	
<c:if test="${ not empty bsConvenioachEstado.enlaceDetalle}">
	<c:set var="detalle" value="${bsConvenioachEstado.enlaceDetalle}"/>	
</c:if>	
<c:if test="${bsConvenioachEstado.ordenamientoAplicado}">
	<c:set var="orden" value="${bsConvenioachEstado.ordenamiento}"/>	
</c:if>		
<c:if test="${not empty bsConvenioachSeg}">
	<c:set var="restriccion" value="${bsConvenioachSeg}"/>
</c:if>

<c:set var="nombre" value="bsConvenioach"/>
<c:set var="ubicacion" value="listado"/>

<% int counter = 0; %>
<%@ include file="/WEB-INF/global/jsp/jq-header.jsp" %>
<!-- Elementos no visibles -->	
	
<form  name="mainForm" id="mainForm" action="${NewtPageRenderData.actionUrl}" method="post">

<div id="mainContent">

<div class="breadCrumbHolder module" style="margin-bottom: 3px;">
<div id="breadcrumb" class="breadCrumb module ui-widget ui-widget-content" style="width: 99%;">
<ul>
	<li><a href="${ctx}/inicio/blank/ria"> HOME </a> </li>
<c:if test="${ not empty ubicacionAplicacion.ruta }">
	<input type="hidden" name="idUbicacionAplicacionActual" id="idUbicacionAplicacionActual" value="${ ubicacionAplicacion.idUbicacionAplicacionActual }"/>
	<c:forEach items="${ ubicacionAplicacion.ruta }" var="ub">
		<li><a href="javascript: doSubmitBreadcrumb('<c:out value="${ ub.submit }"/>', '<c:out value="${ ub.id }"/>')"><c:out value="${ ub.label }" escapeXml="false" /></a>  </li> 
	</c:forEach>			
</c:if>
	<li><fmt:message key="bsConvenioach.bc.etiqueta.principal" /></li>
</ul>
</div>
	<input type="hidden" name="id_ubicacion_retorno" id="id_ubicacion_retorno" value=""/>
</div>

<c:if test="${ not empty detalle.cabecera}">
    <jsp:include page="/WEB-INF/jsp/${detalle.cabecera.locacion}/jq/cabecera.jsp" />
</c:if>

<%-- <div id="iTitle" class="ui-state-default ui-widget-content ui-corner-all">
	<fmt:message key="bsConvenioach.listado.etiqueta.titulo"/>
</div>--%>
	<input type="hidden" id="maxRowNum" name="maxRowNum" value="<c:out value="${navegacion.registroFinal - 1}"/>">
	<input type="hidden" id="pageLoc" name="pageLoc" value="listado"/>
    <input type="hidden" id="eag" name="eag" value="${eag}"/>
	<c:if test="${ not empty busquedaEnllavador}">
	<input type="hidden" id="listadoLov" value="bsConvenioach"/>
	</c:if>
	<c:if test="${ not empty esAsync}">
	<input type="hidden" id="esAsync" value="${esAsync}"/>
	</c:if>
    <input type="hidden" id="movimientoPagina" name="movimientoPagina"/>
    <input type="hidden" id="columna" name="columna"/>
    <input type="hidden" id="fenixf31seguridad" name="fenixf31seguridad" value="<c:out value='${fenixf31seguridad}' />" />
	<!-- valores de llaves -->
    <input type="hidden" alt="notForSubmit" id="convenio" name="convenio" value=""/>
	 <input type="hidden" id="expo" name="expo" value=""/>  



<%@ include file="/WEB-INF/global/jsp/mensajes-globales.jsp" %>

<c:if test="${ not empty busquedaEnllavador}">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em; font-size: 85%;"> 
	<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
 	<strong><fmt:message key="globales.listado.etiqueta.tipLov.titulo"/></strong>&nbsp;<fmt:message key="globales.listado.etiqueta.tipLov.mensaje"/>
	</p>
</div>
</c:if>
<!-- botones de acciones basicas -->
<div id="action-buttons">
<div id="action-bar">
<c:if test="${ empty busquedaEnllavador}">
	<c:if test="${restriccion.verDisponible}">
	<button id="btnVer" type="button" class="ui-button ui-corner-all ui-button-color" name="ver" onClick="if(isValueSelected()){ doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsConvenioach/ver'); }"><fmt:message key="globales.listado.boton.ver"/></button>
	</c:if>
	<c:if test="${restriccion.crearDisponible}">
	<button id="btnCrear" type="button" class="ui-button ui-corner-all ui-button-color" name="crear" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsConvenioach/crear');"><fmt:message key="globales.listado.boton.crear"/></button>
	</c:if>
	<c:if test="${restriccion.actualizarDisponible}">	
	<button id="btnActualizar" type="button" class="ui-button ui-corner-all ui-button-color" name="actualizar" onClick="if(isValueSelected()){ doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsConvenioach/actualizar'); }"><fmt:message key="globales.listado.boton.actualizar"/></button>
	</c:if>
	<c:if test="${restriccion.eliminarDisponible}">
	<button id="btnEliminar" type="button" class="ui-button ui-corner-all ui-button-color" name="eliminar" onClick="if(isValueSelected()){$('#dialog-confirm').dialog('open');}"><fmt:message key="globales.listado.boton.eliminar"/></button>
	</c:if>
	<button id="btnDetalle" type="button" class="ui-button ui-corner-all ui-button-color" name="detalle" onClick="if(isValueSelected()){ doSubmit('bsConvenioach/detalle'); }"><fmt:message key="globales.listado.boton.detalle"/></button>
	</c:if>
<c:if test="${ not empty busquedaEnllavador}">
	<c:if test="${restriccion.crearDisponible}">
	<button id="btnCrear" type="button" class="ui-button ui-corner-all ui-button-color" name="crear" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsConvenioach/crear');"><fmt:message key="globales.listado.boton.crear"/></button>
	</c:if>
	<button id="btnSeleccionar" type="button" class="ui-button ui-corner-all ui-button-color" name="seleccionar" onClick="if(isValueSelected()){ doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsConvenioach/buscarSeleccionar'); }"><fmt:message key="globales.listado.boton.seleccionar"/></button>
	<button id="btnCancelar" type="button" class="ui-button ui-corner-all ui-button-color" name="cancel" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsConvenioach/cancelarBuscar');"><fmt:message key="globales.listado.boton.cancelar"/></button>
</c:if>
	<c:if test="${restriccion.filtrarDisponible}">	
	<button id="btnFiltrar" type="button" class="ui-button ui-corner-all ui-button-color" name="filter" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsConvenioach/filtrar');"><fmt:message key="globales.listado.boton.filtrar"/></button>
	</c:if>	
	
</div>
<div id="navigation-bar">	
<c:if test="${navegacion.paginaAnterior}">				
	<button id="btnAnterior" type="button" name="anterior" onClick="$('#movimientoPagina').val('anterior');doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsConvenioach/listado')"><fmt:message key="globales.listado.boton.anterior"/></button>
</c:if>
<c:if test="${!navegacion.paginaAnterior}">			
	<button id="btnAnterior" type="button" name="anterior" disabled="disabled"><fmt:message key="globales.listado.boton.anterior"/></button>
</c:if>
<c:if test="${not empty navegacion.listadoPaginas}">
<c:forEach items="${navegacion.listadoPaginas}" var="p">
	<c:if test="${p == navegacion.noPaginaActual}">
	<button id="pag<c:out value="${p}" />" class="ui-button-text ui-state-highlight" type="button" name="pag<c:out value="${p}" />" disabled="disabled"><c:out value="${p}" /></button>
	</c:if>
	<c:if test="${p != navegacion.noPaginaActual}">
	<button id="pag<c:out value="${p}" />"  onClick="$('#movimientoPagina').val('${p}');doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsConvenioach/listado')" type="button" name="pag<c:out value="${p}"/>"><c:out value="${p}" /></button>	
	</c:if> 
</c:forEach>
</c:if>
<c:if test="${navegacion.paginaSiguiente}">			
	<button id="btnSiguiente" type="button" name="siguiente" onClick="$('#movimientoPagina').val('siguiente');doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsConvenioach/listado')" ><fmt:message key="globales.listado.boton.siguiente"/></button>
</c:if>
<c:if test="${!navegacion.paginaSiguiente}">			
	<button id="btnSiguiente" type="button" name="siguiente" disabled="disabled"><fmt:message key="globales.listado.boton.siguiente"/></button>
</c:if>
	
</div>
</div>
<br/>

<div class="ui-widget fg-toolbar ui-toolbar ui-widget-header ui-corner-tl ui-corner-tr ui-helper-clearfix">

<c:if test="${ not empty orden}">
	<div style="display:block; float: left;">
		<button id="btnRemoverOrden" type="button" class="ui-button ui-corner-all ui-button-color" name="cancel" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsConvenioach/removerOrdenamiento');"><fmt:message key="globales.listado.boton.removerOrden"/></button>
	</div>
</c:if>
<c:if test="${navegacion.filtroAplicado}">
	<div style="display:block; float: left;">
		<button id="btnRemoverFiltro" type="button" class="ui-button ui-corner-all ui-button-color" name="nofilter" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsConvenioach/removerFiltrar');"><fmt:message key="globales.listado.boton.removerFiltro"/></button>
	</div>			
</c:if>
	<div id="crudTable_filter" style="display: block; text-align: right; float: right; margin-top: 6px; margin-bottom: 6px;"> 
		<fmt:message key="globales.listado.etiqueta.busquedaRapida"/>
		<select id="campoBusqueda" name="campoBusqueda" onchange="doBusquedaRapida('#busqueda', this.value, $('option:selected', this));" class="ui-widget ui-widget-content ui-state-default">
			<option value="tipoconvenio"><fmt:message key="bsConvenioach.listado.etiqueta.Tipoconvenio"/></option>
			<option value="nombre"><fmt:message key="bsConvenioach.listado.etiqueta.Nombre"/></option>
			<option value="cuenta"><fmt:message key="bsConvenioach.listado.etiqueta.Cuenta"/></option>
			<option value="nombrecta"><fmt:message key="bsConvenioach.listado.etiqueta.Nombrecta"/></option>
			<option value="tipomoneda"><fmt:message key="bsConvenioach.listado.etiqueta.Tipomoneda"/></option>
			<option value="descripcion"><fmt:message key="bsConvenioach.listado.etiqueta.Descripcion"/></option>
			<option value="polparticipante"><fmt:message key="bsConvenioach.listado.etiqueta.Polparticipante"/></option>
			<option value="estatus"><fmt:message key="bsConvenioach.listado.etiqueta.Estatus"/></option>
			<option value="autorizacionweb"><fmt:message key="bsConvenioach.listado.etiqueta.Autorizacionweb"/></option>
			<option value="clasificacion"><fmt:message key="bsConvenioach.listado.etiqueta.Clasificacion"/></option>
			<option value="multiplescuentas"><fmt:message key="bsConvenioach.listado.etiqueta.Multiplescuentas"/></option>
			<option value="activo"><fmt:message key="bsConvenioach.listado.etiqueta.Activo"/></option>
			<option value="bitborrado"><fmt:message key="bsConvenioach.listado.etiqueta.Bitborrado"/></option>
		</select> &nbsp;
		<input onkeypress="doFiltrarPor('bsConvenioach', event)" type="text" id="busqueda" name="busqueda" class="ui-widget ui-widget-content " style="width:150px; height: 18px;"/>
	</div>
</div>
<c:if test="${restriccion.listadoDisponible or not empty busquedaEnllavador}">
<table id="crudTable" width="100%">
	<thead>
		<tr>
  			<th onclick="doSort('tipoconvenio','${nombre}')" sort="${orden.mapaDeValores['tipoconvenio'].strOrden}"><fmt:message key="bsConvenioach.listado.etiqueta.Tipoconvenio"/></th>
  			<th onclick="doSort('cliente','${nombre}')" sort="${orden.mapaDeValores['cliente'].strOrden}"><fmt:message key="bsConvenioach.listado.etiqueta.Cliente"/></th>
  			<th onclick="doSort('nombre','${nombre}')" sort="${orden.mapaDeValores['nombre'].strOrden}"><fmt:message key="bsConvenioach.listado.etiqueta.Nombre"/></th>
  			<th onclick="doSort('cuenta','${nombre}')" sort="${orden.mapaDeValores['cuenta'].strOrden}"><fmt:message key="bsConvenioach.listado.etiqueta.Cuenta"/></th>
  			<th onclick="doSort('nombrecta','${nombre}')" sort="${orden.mapaDeValores['nombrecta'].strOrden}"><fmt:message key="bsConvenioach.listado.etiqueta.Nombrecta"/></th>
  			<th onclick="doSort('tipomoneda','${nombre}')" sort="${orden.mapaDeValores['tipomoneda'].strOrden}"><fmt:message key="bsConvenioach.listado.etiqueta.Tipomoneda"/></th>
  			<th onclick="doSort('descripcion','${nombre}')" sort="${orden.mapaDeValores['descripcion'].strOrden}"><fmt:message key="bsConvenioach.listado.etiqueta.Descripcion"/></th>
  			<th onclick="doSort('polparticipante','${nombre}')" sort="${orden.mapaDeValores['polparticipante'].strOrden}"><fmt:message key="bsConvenioach.listado.etiqueta.Polparticipante"/></th>
  			<th onclick="doSort('estatus','${nombre}')" sort="${orden.mapaDeValores['estatus'].strOrden}"><fmt:message key="bsConvenioach.listado.etiqueta.Estatus"/></th>
  			<th onclick="doSort('fechaestatus','${nombre}')" sort="${orden.mapaDeValores['fechaestatus'].strOrden}"><fmt:message key="bsConvenioach.listado.etiqueta.Fechaestatus"/></th>
  			<th onclick="doSort('autorizacionweb','${nombre}')" sort="${orden.mapaDeValores['autorizacionweb'].strOrden}"><fmt:message key="bsConvenioach.listado.etiqueta.Autorizacionweb"/></th>
  			<th onclick="doSort('clasificacion','${nombre}')" sort="${orden.mapaDeValores['clasificacion'].strOrden}"><fmt:message key="bsConvenioach.listado.etiqueta.Clasificacion"/></th>
  			<th onclick="doSort('comisionxoperacionpropiaok','${nombre}')" sort="${orden.mapaDeValores['comisionxoperacionpropiaok'].strOrden}"><fmt:message key="bsConvenioach.listado.etiqueta.Comisionxoperacionpropiaok"/></th>
  			<th onclick="doSort('comisionxoperacionpropiaerror','${nombre}')" sort="${orden.mapaDeValores['comisionxoperacionpropiaerror'].strOrden}"><fmt:message key="bsConvenioach.listado.etiqueta.Comisionxoperacionpropiaerror"/></th>
  			<th onclick="doSort('comisionxoperacionnopropiaok','${nombre}')" sort="${orden.mapaDeValores['comisionxoperacionnopropiaok'].strOrden}"><fmt:message key="bsConvenioach.listado.etiqueta.Comisionxoperacionnopropiaok"/></th>
  			<th onclick="doSort('comisionxoperacionnopropiaerror','${nombre}')" sort="${orden.mapaDeValores['comisionxoperacionnopropiaerror'].strOrden}"><fmt:message key="bsConvenioach.listado.etiqueta.Comisionxoperacionnopropiaerror"/></th>
  			<th onclick="doSort('multiplescuentas','${nombre}')" sort="${orden.mapaDeValores['multiplescuentas'].strOrden}"><fmt:message key="bsConvenioach.listado.etiqueta.Multiplescuentas"/></th>
  			<th onclick="doSort('activo','${nombre}')" sort="${orden.mapaDeValores['activo'].strOrden}"><fmt:message key="bsConvenioach.listado.etiqueta.Activo"/></th>
  			<th onclick="doSort('bitborrado','${nombre}')" sort="${orden.mapaDeValores['bitborrado'].strOrden}"><fmt:message key="bsConvenioach.listado.etiqueta.Bitborrado"/></th>
		</tr>
	</thead>	
	<tbody>
<c:forEach items="${coleccionResultados}" var="dto">
		<tr onclick="$('#convenio').val('${dto.convenio}'); selectDeSelectObject(this, 'ui-state-active'); sel = true; currentRow = $(this).parent().children().index($(this));">  
		<td><c:out value="${dto.tipoconvenio}"/></td>
	    <td><c:out value="${dto.bsCliente.nombre}"/></td>
		<td><c:out value="${dto.nombre}"/></td>
		<td><c:out value="${dto.cuenta}"/></td>
		<td><c:out value="${dto.nombrecta}"/></td>
		<td><c:out value="${dto.tipomoneda}"/></td>
		<td><c:out value="${dto.descripcion}"/></td>
		<td><c:out value="${dto.polparticipante}"/></td>
		<td><c:out value="${dto.estatus}"/></td>
		<td><fmt:formatDate pattern="dd-MM-yyyy" value="${dto.fechaestatus}" /></td>
		<td><c:out value="${dto.autorizacionweb}"/></td>
		<td><c:out value="${dto.clasificacion}"/></td>
		<td><c:out value="${dto.comisionxoperacionpropiaok}"/></td>
		<td><c:out value="${dto.comisionxoperacionpropiaerror}"/></td>
		<td><c:out value="${dto.comisionxoperacionnopropiaok}"/></td>
		<td><c:out value="${dto.comisionxoperacionnopropiaerror}"/></td>
		<td><c:out value="${dto.multiplescuentas}"/></td>
		<td><c:out value="${dto.activo}"/></td>
		<td><c:out value="${dto.bitborrado}"/></td>
		</tr>
</c:forEach>
	</tbody>
</table>
</c:if>
<div class="ui-widget fg-toolbar ui-toolbar ui-widget-header ui-corner-bl ui-corner-br ui-helper-clearfix">
	<c:if test="${navegacion.totalRegistros <= 0}">
		<fmt:message key="globales.listado.etiqueta.sinRegistros" />
    </c:if>
    <c:if test="${navegacion.totalRegistros > 0}">
    	<fmt:message key="globales.listado.etiqueta.numRegistros">
    		<fmt:param><c:out value="${navegacion.registroInicial}"/></fmt:param>
    		<fmt:param><c:out value="${navegacion.registroFinal}"/></fmt:param>
    		<fmt:param><c:out value="${navegacion.totalRegistros}"/></fmt:param>
    	</fmt:message>
    </c:if>	
</div>
<br/>
</div>
</form>
<%@ include file="/WEB-INF/global/jsp/jq-footer.jsp" %>
</fmt:bundle>