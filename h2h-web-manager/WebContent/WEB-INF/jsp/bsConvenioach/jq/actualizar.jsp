<%@ taglib prefix="c"       uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt"     uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:if test="${ not empty locale}">
	<fmt:setLocale value="${ locale }" />
</c:if>
<fmt:bundle basename="com.ofbox.davivienda.h2h.resourceBundle.MessagesResources">
	
<c:if test="${ not empty bsConvenioachEstado.enlaceDetalle}">
	<c:set var="detalle" value="${bsConvenioachEstado.enlaceDetalle}"/>	
	<c:set var="detalleEnllavador" value="${detalle.enllavador}"/>
</c:if>		

<c:set var="nombre" value="bsConvenioach"/>
<c:set var="ubicacion" value="actualizar"/>	
<c:set var="primerCampo" value="tipoconvenio"/>
<%@ include file="/WEB-INF/global/jsp/jq-header.jsp" %>
	
<form  name="mainForm" id="mainForm" action="${NewtPageRenderData.actionUrl}" method="post">
<c:if test="${ not empty detalle.cabecera}">
    <jsp:include page="/WEB-INF/jsp/${detalle.cabecera.locacion}/jq/cabecera.jsp" />
</c:if>
<div id="mainContent">

	<div class="breadCrumbHolder module" style="margin-bottom: 3px;">
<div id="breadcrumb" class="breadCrumb module ui-widget ui-widget-content" style="width: 99%;">
<ul>
	<li><a href="${ctx}/inicio/blank/ria"> HOME </a> </li>
<c:if test="${ not empty ubicacionAplicacion.ruta }">
	<input type="hidden" name="idUbicacionAplicacionActual" id="idUbicacionAplicacionActual" value="${ ubicacionAplicacion.idUbicacionAplicacionActual }"/>
	<c:forEach items="${ ubicacionAplicacion.ruta }" var="ub">
		<li><a href="javascript: doSubmitBreadcrumb('<c:out value="${ ub.submit }"/>', '<c:out value="${ ub.id }"/>')"><c:out value="${ ub.label }" escapeXml="false" /></a>  </li> 
	</c:forEach>			
</c:if>
	<li><fmt:message key="bsConvenioach.bc.etiqueta.actualizar" /></li>
</ul>
</div>
	<input type="hidden" name="id_ubicacion_retorno" id="id_ubicacion_retorno" value=""/>
</div>	
	
<div id="iProcessing" style="visibility: hidden" align="center">
	<img src="${ctx}/include/images/design/processing_request.gif"></img>
</div>

<%--<div id="iTitle" class="ui-state-default ui-widget-content ui-corner-all">
	<fmt:message key="bsConvenioach.formulario.etiqueta.actualizar.titulo"/>
</div> --%>
	<input type="hidden" id="pageLoc" name="pageLoc" value="actualizar"/>
	<input type="hidden" id="primerCampo" name="primerCampo" value="tipoconvenio"/>
    <input type="hidden" id="eag" name="eag" value="${eag}"/>
    <input type="hidden" id="movimientoPagina" name="movimientoPagina"/>
    <input type="hidden" id="columna" name="columna"/>
    <input type="hidden" id="fenixf31seguridad" name="fenixf31seguridad" value="<c:out value='${fenixf31seguridad}' />" />
	<c:if test="${not empty scrollTo }">
    <input type="hidden" id="scrollSmooth" value="<c:out value='${scrollTo}' />" />
    </c:if>
	<!-- valores de llaves -->
	<input type="hidden" id="convenio" name="convenio" value="${bsConvenioach.convenio}"/>	
	
	<input type="hidden" id="nombreBusqueda" name="nombreBusqueda"/>


<!-- mensajes de error-->
<c:if test="${mensajesUsuario.erroresGlobales}">
<div class="ui-state-error ui-corner-all" style="padding: 0 .7em; font-size: 0.8em;"> 
	<span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span> 
	<strong><fmt:message key="globales.formulario.etiqueta.error"/></strong> <br /> 
	<ul>
		<c:forEach items="${mensajesUsuario.mensajesDeError}" var="mensaje">
        	<li><c:out value="${mensaje}" escapeXml="false"/></li>
     	</c:forEach>
	</ul>
</div>
</c:if>

<div id="action-buttons">
<div id="action-bar">
	<button tabindex="20" id="btnGuardar" type="submit" name="actualizar" onClick="doSubmit<c:if test="${ not empty detalle.cabecera}">Async</c:if>('bsConvenioach/actualizarEjecutar');"><fmt:message key="globales.formulario.boton.actualizar.guardar"/></button>
	<button tabindex="21" id="btnCancelar" type="button" name="cancel" onClick="doSubmit<c:if test="${ not empty detalle.cabecera}">Async</c:if>('bsConvenioach/cancelarActualizar');"><fmt:message key="globales.formulario.boton.actualizar.cancelar"/></button>
</div>
</div>
<br />

<!-- datos de formulario -->
<div><span style="font-size: small;">-Los campos marcados con * son obligatorios</span></div>
<div class="ui-widget ui-widget-content ui-corner-all" style="width: 99%">
		<div id="formInputHeader" class="ui-widget ui-widget-header ui-corner-top" style="padding-left: 5px;"> 
			<fmt:message key="bsConvenioach.formulario.etiqueta.actualizar.titulo"/> 
		</div>
		<div style="display: inline;" id="formInput">
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['tipoconvenio']}">	
		<div class="row">
			<span class="label" style="color: #AA0707;"> 
				<fmt:message key="bsConvenioach.formulario.etiqueta.Tipoconvenio"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="1" alt="integer" type="text" id="tipoconvenio" name="tipoconvenio"  value="${bsConvenioach.tipoconvenio}" size="10" maxlength="10"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsConvenioach/ayudaPropiedadJson','tipoconvenio')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['tipoconvenio'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['cliente']}">	
		<div class="row">
			<span class="label" style="color: #AA0707;"> 
				<fmt:message key="bsConvenioach.formulario.etiqueta.Cliente"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
	            <input readonly="readonly" type="text" id="cliente" name="cliente" value="${bsConvenioach.cliente}"  size="10"  />
				<input readonly="readonly" type="text" id="bsCliente.nombre" name="bsCliente.nombre" value="${bsConvenioach.bsCliente.nombre}"  size="40"  />
				<button tabindex="2" id="btnbsCliente" class="sq" onClick="doForaneo('bsCliente', 'bsConvenioach/buscar');"><fmt:message key='globales.formulario.boton.lov'/></button>

			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsConvenioach/ayudaPropiedadJson','cliente')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['cliente'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['nombre']}">	
		<div class="row">
			<span class="label" style="color: #AA0707;"> 
				<fmt:message key="bsConvenioach.formulario.etiqueta.Nombre"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="3" type="text" id="nombre" name="nombre"  value="${bsConvenioach.nombre}" size="50"  maxlength="50"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsConvenioach/ayudaPropiedadJson','nombre')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['nombre'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['cuenta']}">	
		<div class="row">
			<span class="label" style="color: #AA0707;"> 
				<fmt:message key="bsConvenioach.formulario.etiqueta.Cuenta"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="4" type="text" id="cuenta" name="cuenta"  value="${bsConvenioach.cuenta}" size="50"  maxlength="16"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsConvenioach/ayudaPropiedadJson','cuenta')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['cuenta'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['nombrecta']}">	
		<div class="row">
			<span class="label" style="color: #AA0707;"> 
				<fmt:message key="bsConvenioach.formulario.etiqueta.Nombrecta"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="5" type="text" id="nombrecta" name="nombrecta"  value="${bsConvenioach.nombrecta}" size="50"  maxlength="50"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsConvenioach/ayudaPropiedadJson','nombrecta')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['nombrecta'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['tipomoneda']}">	
		<div class="row">
			<span class="label" style="color: #AA0707;"> 
				<fmt:message key="bsConvenioach.formulario.etiqueta.Tipomoneda"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="6" alt="integer" type="text" id="tipomoneda" name="tipomoneda"  value="${bsConvenioach.tipomoneda}" size="10" maxlength="3"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsConvenioach/ayudaPropiedadJson','tipomoneda')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['tipomoneda'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['descripcion']}">	
		<div class="row">
			<span class="label" style="color: #AA0707;"> 
				<fmt:message key="bsConvenioach.formulario.etiqueta.Descripcion"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="7" type="text" id="descripcion" name="descripcion"  value="${bsConvenioach.descripcion}" size="50"  maxlength="50"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsConvenioach/ayudaPropiedadJson','descripcion')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['descripcion'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['polparticipante']}">	
		<div class="row">
			<span class="label" style="color: #AA0707;"> 
				<fmt:message key="bsConvenioach.formulario.etiqueta.Polparticipante"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="8" alt="integer" type="text" id="polparticipante" name="polparticipante"  value="${bsConvenioach.polparticipante}" size="10" maxlength="3"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsConvenioach/ayudaPropiedadJson','polparticipante')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['polparticipante'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['estatus']}">	
		<div class="row">
			<span class="label" style="color: #AA0707;"> 
				<fmt:message key="bsConvenioach.formulario.etiqueta.Estatus"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="9" alt="integer" type="text" id="estatus" name="estatus"  value="${bsConvenioach.estatus}" size="10" maxlength="3"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsConvenioach/ayudaPropiedadJson','estatus')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['estatus'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['fechaestatus']}">	
		<div class="row">
			<span class="label" style="color: #AA0707;"> 
				<fmt:message key="bsConvenioach.formulario.etiqueta.Fechaestatus"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="10" type="text" cdate="fechaestatus" id="fechaestatus" name="fechaestatus"  value="<fmt:formatDate pattern="dd-MM-yyyy" value="${bsConvenioach.fechaestatus}"/>" size="10" />
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsConvenioach/ayudaPropiedadJson','fechaestatus')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['fechaestatus'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['autorizacionweb']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsConvenioach.formulario.etiqueta.Autorizacionweb"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="11" alt="integer" type="text" id="autorizacionweb" name="autorizacionweb"  value="${bsConvenioach.autorizacionweb}" size="10" maxlength="3"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsConvenioach/ayudaPropiedadJson','autorizacionweb')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['autorizacionweb'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['clasificacion']}">	
		<div class="row">
			<span class="label" style="color: #AA0707;"> 
				<fmt:message key="bsConvenioach.formulario.etiqueta.Clasificacion"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="12" alt="integer" type="text" id="clasificacion" name="clasificacion"  value="${bsConvenioach.clasificacion}" size="10" maxlength="3"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsConvenioach/ayudaPropiedadJson','clasificacion')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['clasificacion'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['comisionxoperacionpropiaok']}">	
		<div class="row">
			<span class="label" style="color: #AA0707;"> 
				<fmt:message key="bsConvenioach.formulario.etiqueta.Comisionxoperacionpropiaok"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="13" alt="numeric" type="text" id="comisionxoperacionpropiaok" name="comisionxoperacionpropiaok"  value="${bsConvenioach.comisionxoperacionpropiaok}" size="10" maxlength="19"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsConvenioach/ayudaPropiedadJson','comisionxoperacionpropiaok')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['comisionxoperacionpropiaok'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['comisionxoperacionpropiaerror']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsConvenioach.formulario.etiqueta.Comisionxoperacionpropiaerror"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="14" alt="numeric" type="text" id="comisionxoperacionpropiaerror" name="comisionxoperacionpropiaerror"  value="${bsConvenioach.comisionxoperacionpropiaerror}" size="10" maxlength="19"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsConvenioach/ayudaPropiedadJson','comisionxoperacionpropiaerror')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['comisionxoperacionpropiaerror'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['comisionxoperacionnopropiaok']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsConvenioach.formulario.etiqueta.Comisionxoperacionnopropiaok"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="15" alt="numeric" type="text" id="comisionxoperacionnopropiaok" name="comisionxoperacionnopropiaok"  value="${bsConvenioach.comisionxoperacionnopropiaok}" size="10" maxlength="19"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsConvenioach/ayudaPropiedadJson','comisionxoperacionnopropiaok')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['comisionxoperacionnopropiaok'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['comisionxoperacionnopropiaerror']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsConvenioach.formulario.etiqueta.Comisionxoperacionnopropiaerror"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="16" alt="numeric" type="text" id="comisionxoperacionnopropiaerror" name="comisionxoperacionnopropiaerror"  value="${bsConvenioach.comisionxoperacionnopropiaerror}" size="10" maxlength="19"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsConvenioach/ayudaPropiedadJson','comisionxoperacionnopropiaerror')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['comisionxoperacionnopropiaerror'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['multiplescuentas']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsConvenioach.formulario.etiqueta.Multiplescuentas"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="17" type="text" id="multiplescuentas" name="multiplescuentas"  value="${bsConvenioach.multiplescuentas}" size="50"  maxlength="1"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsConvenioach/ayudaPropiedadJson','multiplescuentas')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['multiplescuentas'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['activo']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsConvenioach.formulario.etiqueta.Activo"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="18" type="text" id="activo" name="activo"  value="${bsConvenioach.activo}" size="50"  maxlength="1"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsConvenioach/ayudaPropiedadJson','activo')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['activo'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['bitborrado']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsConvenioach.formulario.etiqueta.Bitborrado"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="19" type="text" id="bitborrado" name="bitborrado"  value="${bsConvenioach.bitborrado}" size="50"  maxlength="1"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsConvenioach/ayudaPropiedadJson','bitborrado')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['bitborrado'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
		</div>
</div>		
</div>
</form>

<%@ include file="/WEB-INF/global/jsp/jq-footer.jsp" %>
</fmt:bundle>
