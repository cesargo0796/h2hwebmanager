<%@ taglib prefix="c"       uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt"     uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:if test="${ not empty locale}">
	<fmt:setLocale value="${ locale }" />
</c:if>
<fmt:bundle basename="com.ofbox.davivienda.h2h.resourceBundle.MessagesResources">
<input type="hidden" id="nombreDetalle" name="nombreDetalle" value=""/>
<!-- valores de llaves del padre -->
  <input type="hidden" id="idAtributoDetalle" name="idAtributoDetalle" value="${hthAtributoDetalleDtoCabeza.idAtributoDetalle}"/>

<c:set var="ubicacion" value="cabecera"/>
<c:if test="${hthAtributoDetalleEnlaceDetalle == 'hthAtributoFormato'}">
	<c:set var="nombre" value="hthAtributoFormato"/>
</c:if>

<script>
$(document).ready(function(){
	$('#iHeader').accordion({collapsible: true, active: 0});
	$('#backLink').button().removeClass('ui-button-text-only').css({'padding':'0.2em 0.5em', 'margin-right':'20px'}).bind("click", function(e) {e.stopPropagation();});;
	$('#backLinkMod').button().removeClass('ui-button-text-only').css({'padding':'0.2em 0.5em', 'margin-right':'20px'}).bind("click", function(e) {e.stopPropagation();});
});
</script>  

<!-- datos de formulario -->
<div id="iHeader">
	<h3><a id="headerInfo" href="#">
			<span id="backLink" onclick="doSubmit('hthAtributoDetalle/cancelarDetalle');"><fmt:message key="globales.cabecera.boton.regresar"/></span>
			<span id="backLinkMod" onclick="doSubmit('hthAtributoDetalle/actualizar');"><fmt:message key="globales.cabecera.boton.actualizar"/></span>	
			 <fmt:message key="hthAtributoDetalle.encabezado.etiqueta.titulo"/> ${hthAtributoDetalleDtoCabeza.idAtributoDetalle} &nbsp;|			<span style="font-weight: normal;"><fmt:message key="hthAtributoDetalle.encabezado.etiqueta.NombreAtributo"/>:</span>
		    ${hthAtributoDetalleDtoCabeza.nombreAtributo} 
 &nbsp;| 			<span style="font-weight: normal;"><fmt:message key="hthAtributoDetalle.encabezado.etiqueta.Descripcion"/>:</span>
		    ${hthAtributoDetalleDtoCabeza.descripcion} 
 &nbsp;|       	</a></h3>
	<div>
	    <div>
		<strong><fmt:message key="hthAtributoDetalle.encabezado.etiqueta.NombreAtributo"/></strong>&nbsp;
		${hthAtributoDetalleDtoCabeza.nombreAtributo}
		</div>
		
	    <div>
		<strong><fmt:message key="hthAtributoDetalle.encabezado.etiqueta.Descripcion"/></strong>&nbsp;
		${hthAtributoDetalleDtoCabeza.descripcion}
		</div>
		
	</div>
</div>

<div id="iTab" class="ui-tabs ui-widget ui-widget-content ui-corner-top" >
 	<ul id="detailTabs" class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
	<c:if test="${hthAtributoDetalleEnlaceDetalle == 'hthAtributoFormato'}">
		<li class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active">
			<a onClick="$('#nombreDetalle').val('hthAtributoFormato');doSubmit('hthAtributoDetalle/detalle')" class="ui-selected"><fmt:message key="hthAtributoDetalle.encabezado.etiqueta.hthAtributoFormatoDrillDown"/></a>
		</li>
	</c:if> 			
	<c:if test="${hthAtributoDetalleEnlaceDetalle != 'hthAtributoFormato'}">
		<li>
			<a onClick="$('#nombreDetalle').val('hthAtributoFormato');doSubmit('hthAtributoDetalle/detalle')"><fmt:message key="hthAtributoDetalle.encabezado.etiqueta.hthAtributoFormatoDrillDown"/></a>
		</li>
	</c:if>		
	</ul>
</div>
<div id="iTabline"></div>
</fmt:bundle>