<%@ taglib prefix="c"       uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt"     uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:if test="${ not empty locale}">
	<fmt:setLocale value="${ locale }" />
</c:if>
<fmt:bundle basename="com.ofbox.davivienda.h2h.resourceBundle.MessagesResources">
<c:if test="${ not empty bsConvenioEstado.enlaceDetalle}">
	<c:set var="detalle" value="${bsConvenioEstado.enlaceDetalle}"/>	
	<c:set var="detalleEnllavador" value="${detalle.enllavador}"/>
</c:if>	

<c:set var="nombre" value="bsConvenio"/>
<c:set var="ubicacion" value="filtrar"/>
<c:set var="primerCampo" value="clienteMin"/>
<%@ include file="/WEB-INF/global/jsp/jq-header.jsp" %>

<form  name="mainForm" id="mainForm" action="${NewtPageRenderData.actionUrl}" method="post">
<c:if test="${ not empty detalle.cabecera}">
    <jsp:include page="/WEB-INF/jsp/${detalle.cabecera.locacion}/jq/cabecera.jsp" />
</c:if>	
<div id="mainContent">
	<div class="breadCrumbHolder module" style="margin-bottom: 3px;">
    <div id="breadcrumb" class="breadCrumb module ui-widget ui-widget-content" style="width: 99%;">
    <ul>
    	<li><a href="${ctx}/inicio/blank/ria"> HOME </a> </li>
    <c:if test="${ not empty ubicacionAplicacion.ruta }">
    	<input type="hidden" name="idUbicacionAplicacionActual" id="idUbicacionAplicacionActual" value="${ ubicacionAplicacion.idUbicacionAplicacionActual }"/>
    	<c:forEach items="${ ubicacionAplicacion.ruta }" var="ub">
    		<li><a href="javascript: doSubmitBreadcrumb('<c:out value="${ ub.submit }"/>', '<c:out value="${ ub.id }"/>')"><c:out value="${ ub.label }" escapeXml="false" /></a>  </li> 
    	</c:forEach>			
    </c:if>
    	<li><fmt:message key="bsConvenio.bc.etiqueta.filtrar" /></li>
    </ul>
    </div>
    	<input type="hidden" name="id_ubicacion_retorno" id="id_ubicacion_retorno" value=""/>
    </div>
	
<%--<div id="iTitle" class="ui-state-default ui-widget-content ui-corner-all">
	<fmt:message key="bsConvenio.formulario.etiqueta.filtrar.titulo"/>
</div>--%>
<input type="hidden" id="pageLoc" name="pageLoc" value="filtrar"/>
	<input type="hidden" id="primerCampo" name="primerCampo" value="clienteMin"/>
    <input type="hidden" id="eag" name="eag" value="${eag}"/>
    <input type="hidden" id="movimientoPagina" name="movimientoPagina"/>
    <input type="hidden" id="columna" name="columna"/>
    <input type="hidden" id="fenixf31seguridad" name="fenixf31seguridad" value="<c:out value='${fenixf31seguridad}' />" />
	<c:if test="${not empty scrollTo }">
    <input type="hidden" id="scrollSmooth" value="<c:out value='${scrollTo}' />" />
    </c:if>
	<!-- valores de llaves -->


<!-- mensajes de error-->
<c:if test="${mensajesUsuario.erroresGlobales}">
<div id="iError">
	<b class="ui-round-top"> 
	<b class="ui-line-one"></b><b class="ui-line-two"></b><b class="ui-line-three"></b><b class="ui-line-four"></b> 
    </b> 
  	<table>
     	<tr>
       		<td>
         	<ol>
	     	<c:forEach items="${mensajesUsuario.mensajesDeError}" var="mensaje">
	        	<li><c:out value="${mensaje}" escapeXml="false"/></li>
	     	</c:forEach>
	     	</ol>
       		</td>
     	</tr>
  	</table>
  	<b class="ui-round-bottom"> 
	<b class="ui-line-four"></b><b class="ui-line-three"></b><b class="ui-line-two"></b><b class="ui-line-one"></b> 
  	</b> 
</div>
</c:if>

<div id="action-buttons">
<div id="action-bar">
	<button tabindex="47" id="btnAplicarFiltro" type="submit" name="Filtrar" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsConvenio/filtrarEjecutar');"><fmt:message key="globales.filtro.boton.aplicarFiltro"/></button>
	<button tabindex="48" id="btnLimpiar" type="button" name="Limpiar" onClick="limpiarCampos('mainForm');"><fmt:message key="globales.filtro.boton.limpiar"/></button>
	<button tabindex="49" id="btnCancelar" type="button" name="Cancelar" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsConvenio/cancelarFiltrar');"><fmt:message key="globales.filtro.boton.cancelar"/></button>
</div>
</div>
<br />

<!-- datos de formulario -->
<div class="ui-widget ui-widget-content ui-corner-all" style="width: 99%">
		<div id="formInputHeader" class="ui-widget ui-widget-header ui-corner-top" style="padding-left: 5px;"> 
			<fmt:message key="bsConvenio.formulario.etiqueta.filtrar.titulo"/> 
		</div>
		<div style="display: inline;" id="formInput">
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['cliente']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsConvenio.filtro.etiqueta.Cliente"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="3" alt="integerMin" type="text" id="clienteMin" name="clienteMin" value="${bsConvenioFiltro['clienteMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="3" alt="integerMax" type="text" id="clienteMax" name="clienteMax" value="${bsConvenioFiltro['clienteMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['cliente'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['nombre']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsConvenio.filtro.etiqueta.Nombre"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="4" type="text" id="nombre" name="nombre" value="${bsConvenioFiltro['nombre']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['nombre'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['cuenta']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsConvenio.filtro.etiqueta.Cuenta"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="5" type="text" id="cuenta" name="cuenta" value="${bsConvenioFiltro['cuenta']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['cuenta'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['nombrecta']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsConvenio.filtro.etiqueta.Nombrecta"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="6" type="text" id="nombrecta" name="nombrecta" value="${bsConvenioFiltro['nombrecta']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['nombrecta'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['tipomoneda']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsConvenio.filtro.etiqueta.Tipomoneda"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="7" alt="integerMin" type="text" id="tipomonedaMin" name="tipomonedaMin" value="${bsConvenioFiltro['tipomonedaMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="7" alt="integerMax" type="text" id="tipomonedaMax" name="tipomonedaMax" value="${bsConvenioFiltro['tipomonedaMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['tipomoneda'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['categoria']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsConvenio.filtro.etiqueta.Categoria"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="8" alt="integerMin" type="text" id="categoriaMin" name="categoriaMin" value="${bsConvenioFiltro['categoriaMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="8" alt="integerMax" type="text" id="categoriaMax" name="categoriaMax" value="${bsConvenioFiltro['categoriaMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['categoria'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['descripcion']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsConvenio.filtro.etiqueta.Descripcion"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="9" type="text" id="descripcion" name="descripcion" value="${bsConvenioFiltro['descripcion']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['descripcion'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['diaaplicacion']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsConvenio.filtro.etiqueta.Diaaplicacion"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="10" alt="integerMin" type="text" id="diaaplicacionMin" name="diaaplicacionMin" value="${bsConvenioFiltro['diaaplicacionMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="10" alt="integerMax" type="text" id="diaaplicacionMax" name="diaaplicacionMax" value="${bsConvenioFiltro['diaaplicacionMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['diaaplicacion'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['numreintentos']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsConvenio.filtro.etiqueta.Numreintentos"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="11" alt="integerMin" type="text" id="numreintentosMin" name="numreintentosMin" value="${bsConvenioFiltro['numreintentosMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="11" alt="integerMax" type="text" id="numreintentosMax" name="numreintentosMax" value="${bsConvenioFiltro['numreintentosMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['numreintentos'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['polparticipante']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsConvenio.filtro.etiqueta.Polparticipante"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="12" alt="integerMin" type="text" id="polparticipanteMin" name="polparticipanteMin" value="${bsConvenioFiltro['polparticipanteMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="12" alt="integerMax" type="text" id="polparticipanteMax" name="polparticipanteMax" value="${bsConvenioFiltro['polparticipanteMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['polparticipante'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['poldianoexistente']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsConvenio.filtro.etiqueta.Poldianoexistente"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="13" alt="integerMin" type="text" id="poldianoexistenteMin" name="poldianoexistenteMin" value="${bsConvenioFiltro['poldianoexistenteMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="13" alt="integerMax" type="text" id="poldianoexistenteMax" name="poldianoexistenteMax" value="${bsConvenioFiltro['poldianoexistenteMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['poldianoexistente'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['estatus']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsConvenio.filtro.etiqueta.Estatus"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="14" alt="integerMin" type="text" id="estatusMin" name="estatusMin" value="${bsConvenioFiltro['estatusMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="14" alt="integerMax" type="text" id="estatusMax" name="estatusMax" value="${bsConvenioFiltro['estatusMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['estatus'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['fechaestatus']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsConvenio.filtro.etiqueta.Fechaestatus"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="15" type="text" cdate="fechaestatusMax" cattr="minDate" id="fechaestatusMin" name="fechaestatusMin" value="<fmt:formatDate pattern="dd-MM-yyyy" value="${bsConvenioFiltro['fechaestatusMin']}"/>"/>
				&nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
				<input tabindex="15" type="text" cdate="fechaestatusMin" cattr="maxDate" id="fechaestatusMax" name="fechaestatusMax" value="<fmt:formatDate pattern="dd-MM-yyyy" value="${bsConvenioFiltro['fechaestatusMax']}"/>"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['fechaestatus'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['intervaloreintento']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsConvenio.filtro.etiqueta.Intervaloreintento"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="16" alt="integerMin" type="text" id="intervaloreintentoMin" name="intervaloreintentoMin" value="${bsConvenioFiltro['intervaloreintentoMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="16" alt="integerMax" type="text" id="intervaloreintentoMax" name="intervaloreintentoMax" value="${bsConvenioFiltro['intervaloreintentoMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['intervaloreintento'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['polmontofijo']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsConvenio.filtro.etiqueta.Polmontofijo"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="17" alt="integerMin" type="text" id="polmontofijoMin" name="polmontofijoMin" value="${bsConvenioFiltro['polmontofijoMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="17" alt="integerMax" type="text" id="polmontofijoMax" name="polmontofijoMax" value="${bsConvenioFiltro['polmontofijoMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['polmontofijo'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['montofijo']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsConvenio.filtro.etiqueta.Montofijo"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="18" alt="numericMin" type="text" id="montofijoMin" name="montofijoMin" value="${bsConvenioFiltro['montofijoMin']}"/>
				&nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
				<input tabindex="18" alt="numericMax" type="text" id="montofijoMax" name="montofijoMax" value="${bsConvenioFiltro['montofijoMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['montofijo'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['intervaloaplica']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsConvenio.filtro.etiqueta.Intervaloaplica"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="19" alt="integerMin" type="text" id="intervaloaplicaMin" name="intervaloaplicaMin" value="${bsConvenioFiltro['intervaloaplicaMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="19" alt="integerMax" type="text" id="intervaloaplicaMax" name="intervaloaplicaMax" value="${bsConvenioFiltro['intervaloaplicaMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['intervaloaplica'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['creditosparciales']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsConvenio.filtro.etiqueta.Creditosparciales"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="20" alt="integerMin" type="text" id="creditosparcialesMin" name="creditosparcialesMin" value="${bsConvenioFiltro['creditosparcialesMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="20" alt="integerMax" type="text" id="creditosparcialesMax" name="creditosparcialesMax" value="${bsConvenioFiltro['creditosparcialesMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['creditosparciales'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['autorizacionweb']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsConvenio.filtro.etiqueta.Autorizacionweb"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="21" alt="integerMin" type="text" id="autorizacionwebMin" name="autorizacionwebMin" value="${bsConvenioFiltro['autorizacionwebMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="21" alt="integerMax" type="text" id="autorizacionwebMax" name="autorizacionwebMax" value="${bsConvenioFiltro['autorizacionwebMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['autorizacionweb'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['clasificacion']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsConvenio.filtro.etiqueta.Clasificacion"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="22" alt="integerMin" type="text" id="clasificacionMin" name="clasificacionMin" value="${bsConvenioFiltro['clasificacionMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="22" alt="integerMax" type="text" id="clasificacionMax" name="clasificacionMax" value="${bsConvenioFiltro['clasificacionMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['clasificacion'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['comisionxoperacion']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsConvenio.filtro.etiqueta.Comisionxoperacion"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="23" alt="numericMin" type="text" id="comisionxoperacionMin" name="comisionxoperacionMin" value="${bsConvenioFiltro['comisionxoperacionMin']}"/>
				&nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
				<input tabindex="23" alt="numericMax" type="text" id="comisionxoperacionMax" name="comisionxoperacionMax" value="${bsConvenioFiltro['comisionxoperacionMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['comisionxoperacion'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['comisionxoperacionpp']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsConvenio.filtro.etiqueta.Comisionxoperacionpp"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="24" alt="numericMin" type="text" id="comisionxoperacionppMin" name="comisionxoperacionppMin" value="${bsConvenioFiltro['comisionxoperacionppMin']}"/>
				&nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
				<input tabindex="24" alt="numericMax" type="text" id="comisionxoperacionppMax" name="comisionxoperacionppMax" value="${bsConvenioFiltro['comisionxoperacionppMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['comisionxoperacionpp'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['multiplescuentas']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsConvenio.filtro.etiqueta.Multiplescuentas"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="25" type="text" id="multiplescuentas" name="multiplescuentas" value="${bsConvenioFiltro['multiplescuentas']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['multiplescuentas'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
</div>	


</div>
</div>
</form>

<%@ include file="/WEB-INF/global/jsp/jq-footer.jsp" %>
</fmt:bundle>
</body>