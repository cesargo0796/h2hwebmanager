<%@ taglib prefix="c"       uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt"     uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:if test="${ not empty locale}">
	<fmt:setLocale value="${ locale }" />
</c:if>
<fmt:bundle basename="com.ofbox.davivienda.h2h.resourceBundle.MessagesResources">

<c:if test="${ not empty bsConvenioEstado.enlaceBusqueda.enllavador}">
	<c:set var="busquedaEnllavador" value="${bsConvenioEstado.enlaceBusqueda.enllavador}"/>	
</c:if>	
<c:if test="${ not empty bsConvenioEstado.enlaceDetalle}">
	<c:set var="detalle" value="${bsConvenioEstado.enlaceDetalle}"/>	
</c:if>	
<c:if test="${bsConvenioEstado.ordenamientoAplicado}">
	<c:set var="orden" value="${bsConvenioEstado.ordenamiento}"/>	
</c:if>		
<c:if test="${not empty bsConvenioSeg}">
	<c:set var="restriccion" value="${bsConvenioSeg}"/>
</c:if>

<c:set var="nombre" value="bsConvenio"/>
<c:set var="ubicacion" value="listado"/>

<% int counter = 0; %>
<%@ include file="/WEB-INF/global/jsp/jq-header.jsp" %>
<!-- Elementos no visibles -->	
	
<form  name="mainForm" id="mainForm" action="${NewtPageRenderData.actionUrl}" method="post">

<div id="mainContent">

<div class="breadCrumbHolder module" style="margin-bottom: 3px;">
<div id="breadcrumb" class="breadCrumb module ui-widget ui-widget-content" style="width: 99%;">
<ul>
	<li><a href="${ctx}/inicio/blank/ria"> HOME </a> </li>
<c:if test="${ not empty ubicacionAplicacion.ruta }">
	<input type="hidden" name="idUbicacionAplicacionActual" id="idUbicacionAplicacionActual" value="${ ubicacionAplicacion.idUbicacionAplicacionActual }"/>
	<c:forEach items="${ ubicacionAplicacion.ruta }" var="ub">
		<li><a href="javascript: doSubmitBreadcrumb('<c:out value="${ ub.submit }"/>', '<c:out value="${ ub.id }"/>')"><c:out value="${ ub.label }" escapeXml="false" /></a>  </li> 
	</c:forEach>			
</c:if>
	<li><fmt:message key="bsConvenio.bc.etiqueta.principal" /></li>
</ul>
</div>
	<input type="hidden" name="id_ubicacion_retorno" id="id_ubicacion_retorno" value=""/>
</div>

<c:if test="${ not empty detalle.cabecera}">
    <jsp:include page="/WEB-INF/jsp/${detalle.cabecera.locacion}/jq/cabecera.jsp" />
</c:if>

<%-- <div id="iTitle" class="ui-state-default ui-widget-content ui-corner-all">
	<fmt:message key="bsConvenio.listado.etiqueta.titulo"/>
</div>--%>
	<input type="hidden" id="maxRowNum" name="maxRowNum" value="<c:out value="${navegacion.registroFinal - 1}"/>">
	<input type="hidden" id="pageLoc" name="pageLoc" value="listado"/>
    <input type="hidden" id="eag" name="eag" value="${eag}"/>
	<c:if test="${ not empty busquedaEnllavador}">
	<input type="hidden" id="listadoLov" value="bsConvenio"/>
	</c:if>
	<c:if test="${ not empty esAsync}">
	<input type="hidden" id="esAsync" value="${esAsync}"/>
	</c:if>
    <input type="hidden" id="movimientoPagina" name="movimientoPagina"/>
    <input type="hidden" id="columna" name="columna"/>
    <input type="hidden" id="fenixf31seguridad" name="fenixf31seguridad" value="<c:out value='${fenixf31seguridad}' />" />
	<!-- valores de llaves -->
    <input type="hidden" alt="notForSubmit" id="tipoconvenio" name="tipoconvenio" value=""/>
    <input type="hidden" alt="notForSubmit" id="convenio" name="convenio" value=""/>
	 <input type="hidden" id="expo" name="expo" value=""/>  



<%@ include file="/WEB-INF/global/jsp/mensajes-globales.jsp" %>

<c:if test="${ not empty busquedaEnllavador}">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em; font-size: 85%;"> 
	<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
 	<strong><fmt:message key="globales.listado.etiqueta.tipLov.titulo"/></strong>&nbsp;<fmt:message key="globales.listado.etiqueta.tipLov.mensaje"/>
	</p>
</div>
</c:if>
<!-- botones de acciones basicas -->
<div id="action-buttons">
<div id="action-bar">
<c:if test="${ empty busquedaEnllavador}">
	<c:if test="${restriccion.verDisponible}">
	<button id="btnVer" type="button" class="ui-button ui-corner-all ui-button-color" name="ver" onClick="if(isValueSelected()){ doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsConvenio/ver'); }"><fmt:message key="globales.listado.boton.ver"/></button>
	</c:if>
	<c:if test="${restriccion.crearDisponible}">
	<button id="btnCrear" type="button" class="ui-button ui-corner-all ui-button-color" name="crear" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsConvenio/crear');"><fmt:message key="globales.listado.boton.crear"/></button>
	</c:if>
	<c:if test="${restriccion.actualizarDisponible}">	
	<button id="btnActualizar" type="button" class="ui-button ui-corner-all ui-button-color" name="actualizar" onClick="if(isValueSelected()){ doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsConvenio/actualizar'); }"><fmt:message key="globales.listado.boton.actualizar"/></button>
	</c:if>
	<c:if test="${restriccion.eliminarDisponible}">
	<button id="btnEliminar" type="button" class="ui-button ui-corner-all ui-button-color" name="eliminar" onClick="if(isValueSelected()){$('#dialog-confirm').dialog('open');}"><fmt:message key="globales.listado.boton.eliminar"/></button>
	</c:if>
	<button id="btnDetalle" type="button" class="ui-button ui-corner-all ui-button-color" name="detalle" onClick="if(isValueSelected()){ doSubmit('bsConvenio/detalle'); }"><fmt:message key="globales.listado.boton.detalle"/></button>
	</c:if>
<c:if test="${ not empty busquedaEnllavador}">
	<c:if test="${restriccion.crearDisponible}">
	<button id="btnCrear" type="button" class="ui-button ui-corner-all ui-button-color" name="crear" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsConvenio/crear');"><fmt:message key="globales.listado.boton.crear"/></button>
	</c:if>
	<button id="btnSeleccionar" type="button" class="ui-button ui-corner-all ui-button-color" name="seleccionar" onClick="if(isValueSelected()){ doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsConvenio/buscarSeleccionar'); }"><fmt:message key="globales.listado.boton.seleccionar"/></button>
	<button id="btnCancelar" type="button" class="ui-button ui-corner-all ui-button-color" name="cancel" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsConvenio/cancelarBuscar');"><fmt:message key="globales.listado.boton.cancelar"/></button>
</c:if>
	<c:if test="${restriccion.filtrarDisponible}">	
	<button id="btnFiltrar" type="button" class="ui-button ui-corner-all ui-button-color" name="filter" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsConvenio/filtrar');"><fmt:message key="globales.listado.boton.filtrar"/></button>
	</c:if>	
	
</div>
<div id="navigation-bar">	
<c:if test="${navegacion.paginaAnterior}">				
	<button id="btnAnterior" type="button" name="anterior" onClick="$('#movimientoPagina').val('anterior');doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsConvenio/listado')"><fmt:message key="globales.listado.boton.anterior"/></button>
</c:if>
<c:if test="${!navegacion.paginaAnterior}">			
	<button id="btnAnterior" type="button" name="anterior" disabled="disabled"><fmt:message key="globales.listado.boton.anterior"/></button>
</c:if>
<c:if test="${not empty navegacion.listadoPaginas}">
<c:forEach items="${navegacion.listadoPaginas}" var="p">
	<c:if test="${p == navegacion.noPaginaActual}">
	<button id="pag<c:out value="${p}" />" class="ui-button-text ui-state-highlight" type="button" name="pag<c:out value="${p}" />" disabled="disabled"><c:out value="${p}" /></button>
	</c:if>
	<c:if test="${p != navegacion.noPaginaActual}">
	<button id="pag<c:out value="${p}" />"  onClick="$('#movimientoPagina').val('${p}');doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsConvenio/listado')" type="button" name="pag<c:out value="${p}"/>"><c:out value="${p}" /></button>	
	</c:if> 
</c:forEach>
</c:if>
<c:if test="${navegacion.paginaSiguiente}">			
	<button id="btnSiguiente" type="button" name="siguiente" onClick="$('#movimientoPagina').val('siguiente');doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsConvenio/listado')" ><fmt:message key="globales.listado.boton.siguiente"/></button>
</c:if>
<c:if test="${!navegacion.paginaSiguiente}">			
	<button id="btnSiguiente" type="button" name="siguiente" disabled="disabled"><fmt:message key="globales.listado.boton.siguiente"/></button>
</c:if>
	
</div>
</div>
<br/>

<div class="ui-widget fg-toolbar ui-toolbar ui-widget-header ui-corner-tl ui-corner-tr ui-helper-clearfix">

<c:if test="${ not empty orden}">
	<div style="display:block; float: left;">
		<button id="btnRemoverOrden" type="button" class="ui-button ui-corner-all ui-button-color" name="cancel" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsConvenio/removerOrdenamiento');"><fmt:message key="globales.listado.boton.removerOrden"/></button>
	</div>
</c:if>
<c:if test="${navegacion.filtroAplicado}">
	<div style="display:block; float: left;">
		<button id="btnRemoverFiltro" type="button" class="ui-button ui-corner-all ui-button-color" name="nofilter" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsConvenio/removerFiltrar');"><fmt:message key="globales.listado.boton.removerFiltro"/></button>
	</div>			
</c:if>
	<div id="crudTable_filter" style="display: block; text-align: right; float: right; margin-top: 6px; margin-bottom: 6px;"> 
		<fmt:message key="globales.listado.etiqueta.busquedaRapida"/>
		<select id="campoBusqueda" name="campoBusqueda" onchange="doBusquedaRapida('#busqueda', this.value, $('option:selected', this));" class="ui-widget ui-widget-content ui-state-default">
			<option value="cliente"><fmt:message key="bsConvenio.listado.etiqueta.Cliente"/></option>
			<option value="nombre"><fmt:message key="bsConvenio.listado.etiqueta.Nombre"/></option>
			<option value="cuenta"><fmt:message key="bsConvenio.listado.etiqueta.Cuenta"/></option>
			<option value="nombrecta"><fmt:message key="bsConvenio.listado.etiqueta.Nombrecta"/></option>
			<option value="tipomoneda"><fmt:message key="bsConvenio.listado.etiqueta.Tipomoneda"/></option>
			<option value="categoria"><fmt:message key="bsConvenio.listado.etiqueta.Categoria"/></option>
			<option value="descripcion"><fmt:message key="bsConvenio.listado.etiqueta.Descripcion"/></option>
			<option value="diaaplicacion"><fmt:message key="bsConvenio.listado.etiqueta.Diaaplicacion"/></option>
			<option value="numreintentos"><fmt:message key="bsConvenio.listado.etiqueta.Numreintentos"/></option>
			<option value="polparticipante"><fmt:message key="bsConvenio.listado.etiqueta.Polparticipante"/></option>
			<option value="poldianoexistente"><fmt:message key="bsConvenio.listado.etiqueta.Poldianoexistente"/></option>
			<option value="estatus"><fmt:message key="bsConvenio.listado.etiqueta.Estatus"/></option>
			<option value="intervaloreintento"><fmt:message key="bsConvenio.listado.etiqueta.Intervaloreintento"/></option>
			<option value="polmontofijo"><fmt:message key="bsConvenio.listado.etiqueta.Polmontofijo"/></option>
			<option value="intervaloaplica"><fmt:message key="bsConvenio.listado.etiqueta.Intervaloaplica"/></option>
			<option value="creditosparciales"><fmt:message key="bsConvenio.listado.etiqueta.Creditosparciales"/></option>
			<option value="autorizacionweb"><fmt:message key="bsConvenio.listado.etiqueta.Autorizacionweb"/></option>
			<option value="clasificacion"><fmt:message key="bsConvenio.listado.etiqueta.Clasificacion"/></option>
			<option value="multiplescuentas"><fmt:message key="bsConvenio.listado.etiqueta.Multiplescuentas"/></option>
		</select> &nbsp;
		<input onkeypress="doFiltrarPor('bsConvenio', event)" type="text" id="busqueda" name="busqueda" class="ui-widget ui-widget-content " style="width:150px; height: 18px;"/>
	</div>
</div>
<c:if test="${restriccion.listadoDisponible or not empty busquedaEnllavador}">
<table id="crudTable" width="100%">
	<thead>
		<tr>
  			<th onclick="doSort('cliente','${nombre}')" sort="${orden.mapaDeValores['cliente'].strOrden}"><fmt:message key="bsConvenio.listado.etiqueta.Cliente"/></th>
  			<th onclick="doSort('nombre','${nombre}')" sort="${orden.mapaDeValores['nombre'].strOrden}"><fmt:message key="bsConvenio.listado.etiqueta.Nombre"/></th>
  			<th onclick="doSort('cuenta','${nombre}')" sort="${orden.mapaDeValores['cuenta'].strOrden}"><fmt:message key="bsConvenio.listado.etiqueta.Cuenta"/></th>
  			<th onclick="doSort('nombrecta','${nombre}')" sort="${orden.mapaDeValores['nombrecta'].strOrden}"><fmt:message key="bsConvenio.listado.etiqueta.Nombrecta"/></th>
  			<th onclick="doSort('tipomoneda','${nombre}')" sort="${orden.mapaDeValores['tipomoneda'].strOrden}"><fmt:message key="bsConvenio.listado.etiqueta.Tipomoneda"/></th>
  			<th onclick="doSort('categoria','${nombre}')" sort="${orden.mapaDeValores['categoria'].strOrden}"><fmt:message key="bsConvenio.listado.etiqueta.Categoria"/></th>
  			<th onclick="doSort('descripcion','${nombre}')" sort="${orden.mapaDeValores['descripcion'].strOrden}"><fmt:message key="bsConvenio.listado.etiqueta.Descripcion"/></th>
  			<th onclick="doSort('diaaplicacion','${nombre}')" sort="${orden.mapaDeValores['diaaplicacion'].strOrden}"><fmt:message key="bsConvenio.listado.etiqueta.Diaaplicacion"/></th>
  			<th onclick="doSort('numreintentos','${nombre}')" sort="${orden.mapaDeValores['numreintentos'].strOrden}"><fmt:message key="bsConvenio.listado.etiqueta.Numreintentos"/></th>
  			<th onclick="doSort('polparticipante','${nombre}')" sort="${orden.mapaDeValores['polparticipante'].strOrden}"><fmt:message key="bsConvenio.listado.etiqueta.Polparticipante"/></th>
  			<th onclick="doSort('poldianoexistente','${nombre}')" sort="${orden.mapaDeValores['poldianoexistente'].strOrden}"><fmt:message key="bsConvenio.listado.etiqueta.Poldianoexistente"/></th>
  			<th onclick="doSort('estatus','${nombre}')" sort="${orden.mapaDeValores['estatus'].strOrden}"><fmt:message key="bsConvenio.listado.etiqueta.Estatus"/></th>
  			<th onclick="doSort('fechaestatus','${nombre}')" sort="${orden.mapaDeValores['fechaestatus'].strOrden}"><fmt:message key="bsConvenio.listado.etiqueta.Fechaestatus"/></th>
  			<th onclick="doSort('intervaloreintento','${nombre}')" sort="${orden.mapaDeValores['intervaloreintento'].strOrden}"><fmt:message key="bsConvenio.listado.etiqueta.Intervaloreintento"/></th>
  			<th onclick="doSort('polmontofijo','${nombre}')" sort="${orden.mapaDeValores['polmontofijo'].strOrden}"><fmt:message key="bsConvenio.listado.etiqueta.Polmontofijo"/></th>
  			<th onclick="doSort('montofijo','${nombre}')" sort="${orden.mapaDeValores['montofijo'].strOrden}"><fmt:message key="bsConvenio.listado.etiqueta.Montofijo"/></th>
  			<th onclick="doSort('intervaloaplica','${nombre}')" sort="${orden.mapaDeValores['intervaloaplica'].strOrden}"><fmt:message key="bsConvenio.listado.etiqueta.Intervaloaplica"/></th>
  			<th onclick="doSort('creditosparciales','${nombre}')" sort="${orden.mapaDeValores['creditosparciales'].strOrden}"><fmt:message key="bsConvenio.listado.etiqueta.Creditosparciales"/></th>
  			<th onclick="doSort('autorizacionweb','${nombre}')" sort="${orden.mapaDeValores['autorizacionweb'].strOrden}"><fmt:message key="bsConvenio.listado.etiqueta.Autorizacionweb"/></th>
  			<th onclick="doSort('clasificacion','${nombre}')" sort="${orden.mapaDeValores['clasificacion'].strOrden}"><fmt:message key="bsConvenio.listado.etiqueta.Clasificacion"/></th>
  			<th onclick="doSort('comisionxoperacion','${nombre}')" sort="${orden.mapaDeValores['comisionxoperacion'].strOrden}"><fmt:message key="bsConvenio.listado.etiqueta.Comisionxoperacion"/></th>
  			<th onclick="doSort('comisionxoperacionpp','${nombre}')" sort="${orden.mapaDeValores['comisionxoperacionpp'].strOrden}"><fmt:message key="bsConvenio.listado.etiqueta.Comisionxoperacionpp"/></th>
  			<th onclick="doSort('multiplescuentas','${nombre}')" sort="${orden.mapaDeValores['multiplescuentas'].strOrden}"><fmt:message key="bsConvenio.listado.etiqueta.Multiplescuentas"/></th>
		</tr>
	</thead>	
	<tbody>
<c:forEach items="${coleccionResultados}" var="dto">
		<tr onclick="$('#tipoconvenio').val('${dto.tipoconvenio}');$('#convenio').val('${dto.convenio}'); selectDeSelectObject(this, 'ui-state-active'); sel = true; currentRow = $(this).parent().children().index($(this));">  
		<td><c:out value="${dto.cliente}"/></td>
		<td><c:out value="${dto.nombre}"/></td>
		<td><c:out value="${dto.cuenta}"/></td>
		<td><c:out value="${dto.nombrecta}"/></td>
		<td><c:out value="${dto.tipomoneda}"/></td>
		<td><c:out value="${dto.categoria}"/></td>
		<td><c:out value="${dto.descripcion}"/></td>
		<td><c:out value="${dto.diaaplicacion}"/></td>
		<td><c:out value="${dto.numreintentos}"/></td>
		<td><c:out value="${dto.polparticipante}"/></td>
		<td><c:out value="${dto.poldianoexistente}"/></td>
		<td><c:out value="${dto.estatus}"/></td>
		<td><fmt:formatDate pattern="dd-MM-yyyy" value="${dto.fechaestatus}" /></td>
		<td><c:out value="${dto.intervaloreintento}"/></td>
		<td><c:out value="${dto.polmontofijo}"/></td>
		<td><c:out value="${dto.montofijo}"/></td>
		<td><c:out value="${dto.intervaloaplica}"/></td>
		<td><c:out value="${dto.creditosparciales}"/></td>
		<td><c:out value="${dto.autorizacionweb}"/></td>
		<td><c:out value="${dto.clasificacion}"/></td>
		<td><c:out value="${dto.comisionxoperacion}"/></td>
		<td><c:out value="${dto.comisionxoperacionpp}"/></td>
		<td><c:out value="${dto.multiplescuentas}"/></td>
		</tr>
</c:forEach>
	</tbody>
</table>
</c:if>
<div class="ui-widget fg-toolbar ui-toolbar ui-widget-header ui-corner-bl ui-corner-br ui-helper-clearfix">
	<c:if test="${navegacion.totalRegistros <= 0}">
		<fmt:message key="globales.listado.etiqueta.sinRegistros" />
    </c:if>
    <c:if test="${navegacion.totalRegistros > 0}">
    	<fmt:message key="globales.listado.etiqueta.numRegistros">
    		<fmt:param><c:out value="${navegacion.registroInicial}"/></fmt:param>
    		<fmt:param><c:out value="${navegacion.registroFinal}"/></fmt:param>
    		<fmt:param><c:out value="${navegacion.totalRegistros}"/></fmt:param>
    	</fmt:message>
    </c:if>	
</div>
<br/>
</div>
</form>
<%@ include file="/WEB-INF/global/jsp/jq-footer.jsp" %>
</fmt:bundle>