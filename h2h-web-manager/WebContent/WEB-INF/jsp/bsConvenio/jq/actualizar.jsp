<%@ taglib prefix="c"       uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt"     uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:if test="${ not empty locale}">
	<fmt:setLocale value="${ locale }" />
</c:if>
<fmt:bundle basename="com.ofbox.davivienda.h2h.resourceBundle.MessagesResources">
	
<c:if test="${ not empty bsConvenioEstado.enlaceDetalle}">
	<c:set var="detalle" value="${bsConvenioEstado.enlaceDetalle}"/>	
	<c:set var="detalleEnllavador" value="${detalle.enllavador}"/>
</c:if>		

<c:set var="nombre" value="bsConvenio"/>
<c:set var="ubicacion" value="actualizar"/>	
<c:set var="primerCampo" value="cliente"/>
<%@ include file="/WEB-INF/global/jsp/jq-header.jsp" %>
	
<form  name="mainForm" id="mainForm" action="${NewtPageRenderData.actionUrl}" method="post">
<c:if test="${ not empty detalle.cabecera}">
    <jsp:include page="/WEB-INF/jsp/${detalle.cabecera.locacion}/jq/cabecera.jsp" />
</c:if>
<div id="mainContent">

	<div class="breadCrumbHolder module" style="margin-bottom: 3px;">
<div id="breadcrumb" class="breadCrumb module ui-widget ui-widget-content" style="width: 99%;">
<ul>
	<li><a href="${ctx}/inicio/blank/ria"> HOME </a> </li>
<c:if test="${ not empty ubicacionAplicacion.ruta }">
	<input type="hidden" name="idUbicacionAplicacionActual" id="idUbicacionAplicacionActual" value="${ ubicacionAplicacion.idUbicacionAplicacionActual }"/>
	<c:forEach items="${ ubicacionAplicacion.ruta }" var="ub">
		<li><a href="javascript: doSubmitBreadcrumb('<c:out value="${ ub.submit }"/>', '<c:out value="${ ub.id }"/>')"><c:out value="${ ub.label }" escapeXml="false" /></a>  </li> 
	</c:forEach>			
</c:if>
	<li><fmt:message key="bsConvenio.bc.etiqueta.actualizar" /></li>
</ul>
</div>
	<input type="hidden" name="id_ubicacion_retorno" id="id_ubicacion_retorno" value=""/>
</div>	
	
<div id="iProcessing" style="visibility: hidden" align="center">
	<img src="${ctx}/include/images/design/processing_request.gif"></img>
</div>

<%--<div id="iTitle" class="ui-state-default ui-widget-content ui-corner-all">
	<fmt:message key="bsConvenio.formulario.etiqueta.actualizar.titulo"/>
</div> --%>
	<input type="hidden" id="pageLoc" name="pageLoc" value="actualizar"/>
	<input type="hidden" id="primerCampo" name="primerCampo" value="cliente"/>
    <input type="hidden" id="eag" name="eag" value="${eag}"/>
    <input type="hidden" id="movimientoPagina" name="movimientoPagina"/>
    <input type="hidden" id="columna" name="columna"/>
    <input type="hidden" id="fenixf31seguridad" name="fenixf31seguridad" value="<c:out value='${fenixf31seguridad}' />" />
	<c:if test="${not empty scrollTo }">
    <input type="hidden" id="scrollSmooth" value="<c:out value='${scrollTo}' />" />
    </c:if>
	<!-- valores de llaves -->
	<input type="hidden" id="tipoconvenio" name="tipoconvenio" value="${bsConvenio.tipoconvenio}"/>	
	<input type="hidden" id="convenio" name="convenio" value="${bsConvenio.convenio}"/>	
	


<!-- mensajes de error-->
<c:if test="${mensajesUsuario.erroresGlobales}">
<div class="ui-state-error ui-corner-all" style="padding: 0 .7em; font-size: 0.8em;"> 
	<span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span> 
	<strong><fmt:message key="globales.formulario.etiqueta.error"/></strong> <br /> 
	<ul>
		<c:forEach items="${mensajesUsuario.mensajesDeError}" var="mensaje">
        	<li><c:out value="${mensaje}" escapeXml="false"/></li>
     	</c:forEach>
	</ul>
</div>
</c:if>

<div id="action-buttons">
<div id="action-bar">
	<button tabindex="24" id="btnGuardar" type="submit" name="actualizar" onClick="doSubmit<c:if test="${ not empty detalle.cabecera}">Async</c:if>('bsConvenio/actualizarEjecutar');"><fmt:message key="globales.formulario.boton.actualizar.guardar"/></button>
	<button tabindex="25" id="btnCancelar" type="button" name="cancel" onClick="doSubmit<c:if test="${ not empty detalle.cabecera}">Async</c:if>('bsConvenio/cancelarActualizar');"><fmt:message key="globales.formulario.boton.actualizar.cancelar"/></button>
</div>
</div>
<br />

<!-- datos de formulario -->
<div><span style="font-size: small;">-Los campos marcados con * son obligatorios</span></div>
<div class="ui-widget ui-widget-content ui-corner-all" style="width: 99%">
		<div id="formInputHeader" class="ui-widget ui-widget-header ui-corner-top" style="padding-left: 5px;"> 
			<fmt:message key="bsConvenio.formulario.etiqueta.actualizar.titulo"/> 
		</div>
		<div style="display: inline;" id="formInput">
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['cliente']}">	
		<div class="row">
			<span class="label" style="color: #AA0707;"> 
				<fmt:message key="bsConvenio.formulario.etiqueta.Cliente"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="1" alt="integer" type="text" id="cliente" name="cliente"  value="${bsConvenio.cliente}" size="10" maxlength="10"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsConvenio/ayudaPropiedadJson','cliente')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['cliente'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['nombre']}">	
		<div class="row">
			<span class="label" style="color: #AA0707;"> 
				<fmt:message key="bsConvenio.formulario.etiqueta.Nombre"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="2" type="text" id="nombre" name="nombre"  value="${bsConvenio.nombre}" size="50"  maxlength="50"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsConvenio/ayudaPropiedadJson','nombre')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['nombre'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['cuenta']}">	
		<div class="row">
			<span class="label" style="color: #AA0707;"> 
				<fmt:message key="bsConvenio.formulario.etiqueta.Cuenta"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="3" type="text" id="cuenta" name="cuenta"  value="${bsConvenio.cuenta}" size="50"  maxlength="16"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsConvenio/ayudaPropiedadJson','cuenta')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['cuenta'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['nombrecta']}">	
		<div class="row">
			<span class="label" style="color: #AA0707;"> 
				<fmt:message key="bsConvenio.formulario.etiqueta.Nombrecta"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="4" type="text" id="nombrecta" name="nombrecta"  value="${bsConvenio.nombrecta}" size="50"  maxlength="50"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsConvenio/ayudaPropiedadJson','nombrecta')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['nombrecta'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['tipomoneda']}">	
		<div class="row">
			<span class="label" style="color: #AA0707;"> 
				<fmt:message key="bsConvenio.formulario.etiqueta.Tipomoneda"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="5" alt="integer" type="text" id="tipomoneda" name="tipomoneda"  value="${bsConvenio.tipomoneda}" size="10" maxlength="3"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsConvenio/ayudaPropiedadJson','tipomoneda')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['tipomoneda'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['categoria']}">	
		<div class="row">
			<span class="label" style="color: #AA0707;"> 
				<fmt:message key="bsConvenio.formulario.etiqueta.Categoria"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="6" alt="integer" type="text" id="categoria" name="categoria"  value="${bsConvenio.categoria}" size="10" maxlength="3"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsConvenio/ayudaPropiedadJson','categoria')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['categoria'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['descripcion']}">	
		<div class="row">
			<span class="label" style="color: #AA0707;"> 
				<fmt:message key="bsConvenio.formulario.etiqueta.Descripcion"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="7" type="text" id="descripcion" name="descripcion"  value="${bsConvenio.descripcion}" size="50"  maxlength="50"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsConvenio/ayudaPropiedadJson','descripcion')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['descripcion'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['diaaplicacion']}">	
		<div class="row">
			<span class="label" style="color: #AA0707;"> 
				<fmt:message key="bsConvenio.formulario.etiqueta.Diaaplicacion"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="8" alt="integer" type="text" id="diaaplicacion" name="diaaplicacion"  value="${bsConvenio.diaaplicacion}" size="10" maxlength="3"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsConvenio/ayudaPropiedadJson','diaaplicacion')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['diaaplicacion'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['numreintentos']}">	
		<div class="row">
			<span class="label" style="color: #AA0707;"> 
				<fmt:message key="bsConvenio.formulario.etiqueta.Numreintentos"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="9" alt="integer" type="text" id="numreintentos" name="numreintentos"  value="${bsConvenio.numreintentos}" size="10" maxlength="3"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsConvenio/ayudaPropiedadJson','numreintentos')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['numreintentos'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['polparticipante']}">	
		<div class="row">
			<span class="label" style="color: #AA0707;"> 
				<fmt:message key="bsConvenio.formulario.etiqueta.Polparticipante"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="10" alt="integer" type="text" id="polparticipante" name="polparticipante"  value="${bsConvenio.polparticipante}" size="10" maxlength="3"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsConvenio/ayudaPropiedadJson','polparticipante')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['polparticipante'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['poldianoexistente']}">	
		<div class="row">
			<span class="label" style="color: #AA0707;"> 
				<fmt:message key="bsConvenio.formulario.etiqueta.Poldianoexistente"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="11" alt="integer" type="text" id="poldianoexistente" name="poldianoexistente"  value="${bsConvenio.poldianoexistente}" size="10" maxlength="3"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsConvenio/ayudaPropiedadJson','poldianoexistente')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['poldianoexistente'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['estatus']}">	
		<div class="row">
			<span class="label" style="color: #AA0707;"> 
				<fmt:message key="bsConvenio.formulario.etiqueta.Estatus"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="12" alt="integer" type="text" id="estatus" name="estatus"  value="${bsConvenio.estatus}" size="10" maxlength="3"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsConvenio/ayudaPropiedadJson','estatus')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['estatus'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['fechaestatus']}">	
		<div class="row">
			<span class="label" style="color: #AA0707;"> 
				<fmt:message key="bsConvenio.formulario.etiqueta.Fechaestatus"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="13" type="text" cdate="fechaestatus" id="fechaestatus" name="fechaestatus"  value="<fmt:formatDate pattern="dd-MM-yyyy" value="${bsConvenio.fechaestatus}"/>" size="10" />
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsConvenio/ayudaPropiedadJson','fechaestatus')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['fechaestatus'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['intervaloreintento']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsConvenio.formulario.etiqueta.Intervaloreintento"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="14" alt="integer" type="text" id="intervaloreintento" name="intervaloreintento"  value="${bsConvenio.intervaloreintento}" size="10" maxlength="3"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsConvenio/ayudaPropiedadJson','intervaloreintento')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['intervaloreintento'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['polmontofijo']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsConvenio.formulario.etiqueta.Polmontofijo"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="15" alt="integer" type="text" id="polmontofijo" name="polmontofijo"  value="${bsConvenio.polmontofijo}" size="10" maxlength="3"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsConvenio/ayudaPropiedadJson','polmontofijo')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['polmontofijo'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['montofijo']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsConvenio.formulario.etiqueta.Montofijo"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="16" alt="numeric" type="text" id="montofijo" name="montofijo"  value="${bsConvenio.montofijo}" size="10" maxlength="19"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsConvenio/ayudaPropiedadJson','montofijo')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['montofijo'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['intervaloaplica']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsConvenio.formulario.etiqueta.Intervaloaplica"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="17" alt="integer" type="text" id="intervaloaplica" name="intervaloaplica"  value="${bsConvenio.intervaloaplica}" size="10" maxlength="3"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsConvenio/ayudaPropiedadJson','intervaloaplica')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['intervaloaplica'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['creditosparciales']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsConvenio.formulario.etiqueta.Creditosparciales"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="18" alt="integer" type="text" id="creditosparciales" name="creditosparciales"  value="${bsConvenio.creditosparciales}" size="10" maxlength="3"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsConvenio/ayudaPropiedadJson','creditosparciales')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['creditosparciales'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['autorizacionweb']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsConvenio.formulario.etiqueta.Autorizacionweb"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="19" alt="integer" type="text" id="autorizacionweb" name="autorizacionweb"  value="${bsConvenio.autorizacionweb}" size="10" maxlength="3"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsConvenio/ayudaPropiedadJson','autorizacionweb')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['autorizacionweb'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['clasificacion']}">	
		<div class="row">
			<span class="label" style="color: #AA0707;"> 
				<fmt:message key="bsConvenio.formulario.etiqueta.Clasificacion"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="20" alt="integer" type="text" id="clasificacion" name="clasificacion"  value="${bsConvenio.clasificacion}" size="10" maxlength="3"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsConvenio/ayudaPropiedadJson','clasificacion')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['clasificacion'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['comisionxoperacion']}">	
		<div class="row">
			<span class="label" style="color: #AA0707;"> 
				<fmt:message key="bsConvenio.formulario.etiqueta.Comisionxoperacion"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="21" alt="numeric" type="text" id="comisionxoperacion" name="comisionxoperacion"  value="${bsConvenio.comisionxoperacion}" size="10" maxlength="19"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsConvenio/ayudaPropiedadJson','comisionxoperacion')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['comisionxoperacion'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['comisionxoperacionpp']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsConvenio.formulario.etiqueta.Comisionxoperacionpp"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="22" alt="numeric" type="text" id="comisionxoperacionpp" name="comisionxoperacionpp"  value="${bsConvenio.comisionxoperacionpp}" size="10" maxlength="19"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsConvenio/ayudaPropiedadJson','comisionxoperacionpp')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['comisionxoperacionpp'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['multiplescuentas']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsConvenio.formulario.etiqueta.Multiplescuentas"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="23" type="text" id="multiplescuentas" name="multiplescuentas"  value="${bsConvenio.multiplescuentas}" size="50"  maxlength="1"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsConvenio/ayudaPropiedadJson','multiplescuentas')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['multiplescuentas'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
		</div>
</div>		
</div>
</form>

<%@ include file="/WEB-INF/global/jsp/jq-footer.jsp" %>
</fmt:bundle>
