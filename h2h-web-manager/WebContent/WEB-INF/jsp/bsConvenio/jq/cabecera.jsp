<%@ taglib prefix="c"       uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt"     uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:if test="${ not empty locale}">
	<fmt:setLocale value="${ locale }" />
</c:if>
<fmt:bundle basename="com.ofbox.davivienda.h2h.resourceBundle.MessagesResources">
<input type="hidden" id="nombreDetalle" name="nombreDetalle" value=""/>
<!-- valores de llaves del padre -->
  <input type="hidden" id="tipoconvenio" name="tipoconvenio" value="${bsConvenioDtoCabeza.tipoconvenio}"/>
  <input type="hidden" id="convenio" name="convenio" value="${bsConvenioDtoCabeza.convenio}"/>

<c:set var="ubicacion" value="cabecera"/>
<c:if test="${bsConvenioEnlaceDetalle == 'bsLotepe'}">
	<c:set var="nombre" value="bsLotepe"/>
</c:if>
<c:if test="${bsConvenioEnlaceDetalle == 'bsConvenioempresa'}">
	<c:set var="nombre" value="bsConvenioempresa"/>
</c:if>

<script>
$(document).ready(function(){
	$('#iHeader').accordion({collapsible: true, active: 0});
	$('#backLink').button().removeClass('ui-button-text-only').css({'padding':'0.2em 0.5em', 'margin-right':'20px'}).bind("click", function(e) {e.stopPropagation();});;
	$('#backLinkMod').button().removeClass('ui-button-text-only').css({'padding':'0.2em 0.5em', 'margin-right':'20px'}).bind("click", function(e) {e.stopPropagation();});
});
</script>  

<!-- datos de formulario -->
<div id="iHeader">
	<h3><a id="headerInfo" href="#">
			<span id="backLink" onclick="doSubmit('bsConvenio/cancelarDetalle');"><fmt:message key="globales.cabecera.boton.regresar"/></span>
			<span id="backLinkMod" onclick="doSubmit('bsConvenio/actualizar');"><fmt:message key="globales.cabecera.boton.actualizar"/></span>	
			 <fmt:message key="bsConvenio.encabezado.etiqueta.titulo"/> ${bsConvenioDtoCabeza.tipoconvenio} &nbsp;|${bsConvenioDtoCabeza.convenio} &nbsp;|			<span style="font-weight: normal;"><fmt:message key="bsConvenio.encabezado.etiqueta.Cliente"/>:</span>
		    ${bsConvenioDtoCabeza.cliente} 
 &nbsp;| 			<span style="font-weight: normal;"><fmt:message key="bsConvenio.encabezado.etiqueta.Nombre"/>:</span>
		    ${bsConvenioDtoCabeza.nombre} 
 &nbsp;|                      	</a></h3>
	<div>
	    <div>
		<strong><fmt:message key="bsConvenio.encabezado.etiqueta.Cliente"/></strong>&nbsp;
		${bsConvenioDtoCabeza.cliente}
		</div>
		
	    <div>
		<strong><fmt:message key="bsConvenio.encabezado.etiqueta.Nombre"/></strong>&nbsp;
		${bsConvenioDtoCabeza.nombre}
		</div>
		
	</div>
</div>

<div id="iTab" class="ui-tabs ui-widget ui-widget-content ui-corner-top" >
 	<ul id="detailTabs" class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
	<c:if test="${bsConvenioEnlaceDetalle == 'bsLotepe'}">
		<li class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active">
			<a onClick="$('#nombreDetalle').val('bsLotepe');doSubmit('bsConvenio/detalle')" class="ui-selected"><fmt:message key="bsConvenio.encabezado.etiqueta.bsLotepeDrillDown"/></a>
		</li>
	</c:if> 			
	<c:if test="${bsConvenioEnlaceDetalle != 'bsLotepe'}">
		<li>
			<a onClick="$('#nombreDetalle').val('bsLotepe');doSubmit('bsConvenio/detalle')"><fmt:message key="bsConvenio.encabezado.etiqueta.bsLotepeDrillDown"/></a>
		</li>
	</c:if>		
	<c:if test="${bsConvenioEnlaceDetalle == 'bsConvenioempresa'}">
		<li class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active">
			<a onClick="$('#nombreDetalle').val('bsConvenioempresa');doSubmit('bsConvenio/detalle')" class="ui-selected"><fmt:message key="bsConvenio.encabezado.etiqueta.bsConvenioempresaDrillDown"/></a>
		</li>
	</c:if> 			
	<c:if test="${bsConvenioEnlaceDetalle != 'bsConvenioempresa'}">
		<li>
			<a onClick="$('#nombreDetalle').val('bsConvenioempresa');doSubmit('bsConvenio/detalle')"><fmt:message key="bsConvenio.encabezado.etiqueta.bsConvenioempresaDrillDown"/></a>
		</li>
	</c:if>		
	</ul>
</div>
<div id="iTabline"></div>
</fmt:bundle>