<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:if test="${ not empty locale}">
	<fmt:setLocale value="${ locale }" />
</c:if>
<fmt:bundle
	basename="com.ofbox.davivienda.h2h.resourceBundle.MessagesResources">

	<c:if test="${ not empty bsCodigosBancoEstado.enlaceDetalle}">
		<c:set var="detalle" value="${bsCodigosBancoEstado.enlaceDetalle}" />
		<c:set var="detalleEnllavador" value="${detalle.enllavador}" />
	</c:if>

	<c:set var="nombre" value="bsCodigosBanco" />
	<c:set var="ubicacion" value="actualizar" />
	<c:set var="primerCampo" value="codigoBanco" />
	<%@ include file="/WEB-INF/global/jsp/jq-header.jsp"%>

	<form name="mainForm" id="mainForm"
		action="${NewtPageRenderData.actionUrl}" method="post">
		<c:if test="${ not empty detalle.cabecera}">
			<jsp:include
				page="/WEB-INF/jsp/${detalle.cabecera.locacion}/jq/cabecera.jsp" />
		</c:if>
		<div id="mainContent">

			<div class="breadCrumbHolder module" style="margin-bottom: 3px;">
				<div id="breadcrumb"
					class="breadCrumb module ui-widget ui-widget-content"
					style="width: 99%;">
					<ul>
						<li><a href="${ctx}/inicio/blank/ria"> HOME </a></li>
						<c:if test="${ not empty ubicacionAplicacion.ruta }">
							<input type="hidden" name="idUbicacionAplicacionActual"
								id="idUbicacionAplicacionActual"
								value="${ ubicacionAplicacion.idUbicacionAplicacionActual }" />
							<c:forEach items="${ ubicacionAplicacion.ruta }" var="ub">
								<li><a
									href="javascript: doSubmitBreadcrumb('<c:out value="${ ub.submit }"/>', '<c:out value="${ ub.id }"/>')"><c:out
											value="${ ub.label }" escapeXml="false" /></a></li>
							</c:forEach>
						</c:if>
						<li><fmt:message key="bsCodigosBanco.bc.etiqueta.actualizar" /></li>
					</ul>
				</div>
				<input type="hidden" name="id_ubicacion_retorno"
					id="id_ubicacion_retorno" value="" />
			</div>

			<div id="iProcessing" style="visibility: hidden" align="center">
				<img src="${ctx}/include/images/design/processing_request.gif"></img>
			</div>

			<div id="iTitle"
				class="ui-state-default ui-widget-content ui-corner-all">
				<fmt:message
					key="bsCodigosBanco.formulario.etiqueta.actualizar.titulo" />
			</div>
			<input type="hidden" id="pageLoc" name="pageLoc" value="actualizar" />
			<input type="hidden" id="primerCampo" name="primerCampo" value="codigoBanco" /> 
			<input type="hidden" id="eag" name="eag" value="${eag}" /> 
			<input type="hidden" id="movimientoPagina" name="movimientoPagina" /> 
			<input type="hidden" id="columna" name="columna" /> 
			<input type="hidden" id="fenixf31seguridad" name="fenixf31seguridad" value="<c:out value='${fenixf31seguridad}' />" />
			<c:if test="${not empty scrollTo }">
				<input type="hidden" id="scrollSmooth"
					value="<c:out value='${scrollTo}' />" />
			</c:if>
			<!-- valores de llaves -->
			<input type="hidden" id="idCodigoBanco" name="idCodigoBanco"
				value="${bsCodigosBanco.idCodigoBanco}" /> 
			<input type="hidden" id="nombreBusqueda" name="nombreBusqueda"/>


			<!-- mensajes de error-->
			<c:if test="${mensajesUsuario.erroresGlobales}">
				<div class="ui-state-error ui-corner-all"
					style="padding: 0 .7em; font-size: 0.8em;">
					<span class="ui-icon ui-icon-alert"
						style="float: left; margin-right: .3em;"></span> <strong><fmt:message
							key="globales.formulario.etiqueta.error" /></strong> <br />
					<ul>
						<c:forEach items="${mensajesUsuario.mensajesDeError}"
							var="mensaje">
							<li><c:out value="${mensaje}" escapeXml="false" /></li>
						</c:forEach>
					</ul>
				</div>
			</c:if>

			<div id="action-buttons">
				<div id="action-bar">
					<button tabindex="21" id="btnGuardar" type="submit"
						name="actualizar"
						onClick="doSubmit<c:if test="${ not empty detalle.cabecera}">Async</c:if>('bsCodigosBanco/actualizarEjecutar');">
						<fmt:message key="globales.formulario.boton.actualizar.guardar" />
					</button>
					<button tabindex="22" id="btnCancelar" type="button" name="cancel"
						onClick="doSubmit<c:if test="${ not empty detalle.cabecera}">Async</c:if>('bsCodigosBanco/cancelarActualizar');">
						<fmt:message key="globales.formulario.boton.actualizar.cancelar" />
					</button>
				</div>
			</div>
			<br />

			<!-- datos de formulario -->
			<div>
				<span style="font-size: small;">-Los campos marcados con * son obligatorios</span>
			</div>
			<div class="ui-widget ui-widget-content ui-corner-all"
				style="width: 99%">
				<div id="formInputHeader"
					class="ui-widget ui-widget-header ui-corner-top"
					style="padding-left: 5px;">
					<fmt:message
						key="bsCodigosBanco.formulario.etiqueta.actualizar.titulo" />
				</div>
				<div style="display: inline;" id="formInput">
					<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['codiBanco']}">	
							<div class="row" >
								<span class="label"> 
					                <fmt:message key="bsCodigosBanco.formulario.etiqueta.CodigoBanco"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
								
								<span class="forminput" style="display: inline">
						            <input readonly="readonly" type="text" id="codiBanco" name="codiBanco" value="${bsCodigosBanco.codiBanco}"  size="10"  />
									<input readonly="readonly" type="text" id="bsBancoAch.nombre" name="bsBancoAch.nombre" value="${bsCodigosBanco.bsBancoAch.nombre}"  size="50"  />
									<button tabindex="1" id="btnbsBancoAch" class="sq" onClick="doForaneo('bsBancoAch', 'bsCodigosBanco/buscar');"><fmt:message key='globales.formulario.boton.lov'/></button>
								</span>
								<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['codiBanco'].mensaje}" var="mensaje">
								<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
								</c:forEach>
								
							</div>
					</c:if>	
					<c:if
						test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['codigoUni']}">
						<div class="row">
							<span class="label"> <fmt:message key="bsCodigosBanco.formulario.etiqueta.CodigoUni" />
							</span> 
							<span class="forminput" style="display: inline"> 
								<input tabindex="2" readonly="readonly" type="text" id="bsBancoAch.codPayBank" name="codigoUni" value="${bsCodigosBanco.bsBancoAch.codPayBank}" />
							</span>
							<c:forEach
								items="${mensajesUsuario.mapaDeErroresEnPropiedades['codigoUni'].mensaje}" var="mensaje">
								<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false" />
								</span>
							</c:forEach>

						</div>
					</c:if>
					<c:if
						test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['codigoSwift']}">
						<div class="row">
							<span class="label"> <fmt:message key="bsCodigosBanco.formulario.etiqueta.CodigoSwift" />
							</span> 
							<span class="forminput" style="display: inline"> 
								<input tabindex="3" type="text" id="idCodigoSwift" name="codigoSwift" value="${bsCodigosBanco.codigoSwift}" />
							</span>
							<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['codigoSwift'].mensaje}" var="mensaje">
								<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false" />
								</span>
							</c:forEach>

						</div>
					</c:if>
					<c:if
						test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['codigoTransfer']}">
						<div class="row">
							<span class="label"> <fmt:message key="bsCodigosBanco.formulario.etiqueta.CodigoTransfer" />
							</span> 
							<span class="forminput" style="display: inline"> 
								<input tabindex="4" type="text" id="idCodigoTransfer" name="codigoTransfer" value="${bsCodigosBanco.codigoTransfer}" />
							</span>
							<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['codigoTransfer'].mensaje}" var="mensaje">
								<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false" />
								</span>
							</c:forEach>
						</div>
					</c:if>
					<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['idRedOperacion']}">	
							<div class="row" >
								<span class="label"> 
					                <fmt:message key="bsCodigosBanco.formulario.etiqueta.IdRedOperacion"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
								
								<span class="forminput" style="display: inline">
						            <input readonly="readonly" type="text" id="idRedOperacion" name="idRedOperacion" value="${bsCodigosBanco.idRedOperacion}"  size="10"  />
									<input readonly="readonly" type="text" id="bsRedOperacionBanco.interpretacion" name="bsRedOperacionBanco.interpretacion" value="${bsCodigosBanco.bsRedOperacionBanco.interpretacion}"  size="50"  />
									<button tabindex="5" id="btnbsRedOperacionBanco" class="sq" onClick="doForaneo('bsRedOperacionBanco', 'bsCodigosBanco/buscar');"><fmt:message key='globales.formulario.boton.lov'/></button>
								</span>
								<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['idRedOperacion'].mensaje}" var="mensaje">
								<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
								</c:forEach>
								
							</div>
					</c:if>	
					<c:if
						test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['status']}">
						<div class="row">
							<span class="label"> <fmt:message key="bsCodigosBanco.formulario.etiqueta.Estado" />
							</span> 
							<span class="forminput" style="display: inline"> 											
				                <select  id="idStatus" name="status" tabindex="6">			                
			            			<option value="ACTIVO" 
			            				<c:if test="${bsCodigosBanco.status == 'ACTIVO'}">selected</c:if>
			            			>
			            				<fmt:message key="bsCodigosBanco.formulario.comboLabel.EstadoActivo"/>
			            			</option>	                
			            			<option value="INACTIVO" 
			            				<c:if test="${bsCodigosBanco.status == 'INACTIVO'}">selected</c:if>
			            			>
			            				<fmt:message key="bsCodigosBanco.formulario.comboLabel.EstadoInactivo"/>
			            			</option>
				                </select>
							</span>
							<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['status'].mensaje}" var="mensaje">
								<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false" />
								</span>
							</c:forEach>
						</div>
					</c:if>
				</div>
			</div>
		</div>
	</form>

	<%@ include file="/WEB-INF/global/jsp/jq-footer.jsp"%>
</fmt:bundle>
