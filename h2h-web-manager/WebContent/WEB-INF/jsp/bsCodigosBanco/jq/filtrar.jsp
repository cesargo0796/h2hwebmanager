<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:if test="${ not empty locale}">
	<fmt:setLocale value="${ locale }" />
</c:if>
<fmt:bundle
	basename="com.ofbox.davivienda.h2h.resourceBundle.MessagesResources">
	<c:if test="${ not empty bsCodigosBancoEstado.enlaceDetalle}">
		<c:set var="detalle" value="${bsCodigosBancoEstado.enlaceDetalle}" />
		<c:set var="detalleEnllavador" value="${detalle.enllavador}" />
	</c:if>

	<c:set var="nombre" value="bsCodigosBanco" />
	<c:set var="ubicacion" value="filtrar" />
	<c:set var="primerCampo" value="cuenta" />
	<%@ include file="/WEB-INF/global/jsp/jq-header.jsp"%>

	<form name="mainForm" id="mainForm"
		action="${NewtPageRenderData.actionUrl}" method="post">
		<c:if test="${ not empty detalle.cabecera}">
			<jsp:include
				page="/WEB-INF/jsp/${detalle.cabecera.locacion}/jq/cabecera.jsp" />
		</c:if>
		<div id="mainContent">
			<div class="breadCrumbHolder module" style="margin-bottom: 3px;">
				<div id="breadcrumb"
					class="breadCrumb module ui-widget ui-widget-content"
					style="width: 99%;">
					<ul>
						<li><a href="${ctx}/inicio/blank/ria"> HOME </a></li>
						<c:if test="${ not empty ubicacionAplicacion.ruta }">
							<input type="hidden" name="idUbicacionAplicacionActual"
								id="idUbicacionAplicacionActual"
								value="${ ubicacionAplicacion.idUbicacionAplicacionActual }" />
							<c:forEach items="${ ubicacionAplicacion.ruta }" var="ub">
								<li><a
									href="javascript: doSubmitBreadcrumb('<c:out value="${ ub.submit }"/>', '<c:out value="${ ub.id }"/>')"><c:out
											value="${ ub.label }" escapeXml="false" /></a></li>
							</c:forEach>
						</c:if>
						<li><fmt:message key="bsCodigosBanco.bc.etiqueta.filtrar" /></li>
					</ul>
				</div>
				<input type="hidden" name="id_ubicacion_retorno"
					id="id_ubicacion_retorno" value="" />
			</div>

			<%--<div id="iTitle" class="ui-state-default ui-widget-content ui-corner-all">
	<fmt:message key="bsCodigosBanco.formulario.etiqueta.filtrar.titulo"/>
</div>--%>
			<input type="hidden" id="pageLoc" name="pageLoc" value="filtrar" /> <input
				type="hidden" id="primerCampo" name="primerCampo" value="cuenta" />
			<input type="hidden" id="eag" name="eag" value="${eag}" /> <input
				type="hidden" id="movimientoPagina" name="movimientoPagina" /> <input
				type="hidden" id="columna" name="columna" /> <input type="hidden"
				id="fenixf31seguridad" name="fenixf31seguridad"
				value="<c:out value='${fenixf31seguridad}' />" />
			<c:if test="${not empty scrollTo }">
				<input type="hidden" id="scrollSmooth"
					value="<c:out value='${scrollTo}' />" />
			</c:if>
			<!-- valores de llaves -->
			<input type="hidden" id="nombreBusqueda" name="nombreBusqueda"/>


			<!-- mensajes de error-->
			<c:if test="${mensajesUsuario.erroresGlobales}">
				<div id="iError">
					<b class="ui-round-top"> <b class="ui-line-one"></b><b
						class="ui-line-two"></b><b class="ui-line-three"></b><b
						class="ui-line-four"></b>
					</b>
					<table>
						<tr>
							<td>
								<ol>
									<c:forEach items="${mensajesUsuario.mensajesDeError}"
										var="mensaje">
										<li><c:out value="${mensaje}" escapeXml="false" /></li>
									</c:forEach>
								</ol>
							</td>
						</tr>
					</table>
					<b class="ui-round-bottom"> <b class="ui-line-four"></b><b
						class="ui-line-three"></b><b class="ui-line-two"></b><b
						class="ui-line-one"></b>
					</b>
				</div>
			</c:if>

			<div id="action-buttons">
				<div id="action-bar">
					<button tabindex="41" id="btnAplicarFiltro" type="submit"
						name="Filtrar"
						onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsCodigosBanco/filtrarEjecutar');">
						<fmt:message key="globales.filtro.boton.aplicarFiltro" />
					</button>
					<button tabindex="42" id="btnLimpiar" type="button" name="Limpiar"
						onClick="limpiarCampos('mainForm');">
						<fmt:message key="globales.filtro.boton.limpiar" />
					</button>
					<button tabindex="43" id="btnCancelar" type="button"
						name="Cancelar"
						onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsCodigosBanco/cancelarFiltrar');">
						<fmt:message key="globales.filtro.boton.cancelar" />
					</button>
				</div>
			</div>
			<br />

			<!-- datos de formulario -->
			<div class="ui-widget ui-widget-content ui-corner-all"
				style="width: 99%">
				<div id="formInputHeader"
					class="ui-widget ui-widget-header ui-corner-top"
					style="padding-left: 5px;">
					<fmt:message key="bsCodigosBanco.formulario.etiqueta.filtrar.titulo" />
				</div>
				<div style="display: inline;" id="formInput">					
					<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['codiBanco']}">	
							<div class="row" >
								<span class="label"> 
					                <fmt:message key="bsCodigosBanco.formulario.etiqueta.CodigoBanco"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
								
								<span class="forminput" style="display: inline">
						            <input readonly="readonly" type="text" id="codiBanco" name="codiBanco" value="${bsCodigosBancoFiltro['bsBancoAch'].codBanco}"  size="10"  />
									<input readonly="readonly" type="text" id="bsBancoAch.nombre" name="bsBancoAch.nombre" value="${bsCodigosBancoFiltro['bsBancoAch'].nombre}"  size="50"  />
									<button tabindex="1" id="btnbsBancoAch" class="sq" onClick="doForaneo('bsBancoAch', 'bsCodigosBanco/buscar');"><fmt:message key='globales.formulario.boton.lov'/></button>
								</span>
								<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['codiBanco'].mensaje}" var="mensaje">
								<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
								</c:forEach>
								
							</div>
					</c:if>	
					<c:if
						test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['codigoUni']}">
						<div class="row">
							<span class="label"> <fmt:message key="bsCodigosBanco.formulario.etiqueta.CodigoUni" />
							</span> 
							<span class="forminput" style="display: inline"> 
								<input tabindex="2" readonly="readonly" type="text" id="bsBancoAch.codPayBank" name="codigoUni" value="${bsCodigosBancoFiltro['bsBancoAch'].codPayBank}" />
							</span>
							<c:forEach
								items="${mensajesUsuario.mapaDeErroresEnPropiedades['codigoUni'].mensaje}" var="mensaje">
								<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false" />
								</span>
							</c:forEach>

						</div>
					</c:if>
					
					<c:if
						test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['codigoSwift']}">
						<div class="row">
							<span class="label"> <fmt:message
									key="bsCodigosBanco.filtro.etiqueta.CodigoSwift" />
							</span> <span class="forminput" style="display: inline"> <input
								tabindex="3" type="text" id="idCodigoSwiftlb" name="codigoSwift"
								value="${bsCodigosBancoFiltro['codigoSwift']}" />
							</span>
							<c:forEach
								items="${mensajesUsuario.mapaDeErroresEnPropiedades['codigoSwift'].mensaje}"
								var="mensaje">
								<span style="font-size: 90%;" class="ui-state-error"> <c:out
										value="${mensaje}" escapeXml="false" />
								</span>
							</c:forEach>
						</div>
					</c:if>
					<c:if
						test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['codigoTransfer']}">
						<div class="row">
							<span class="label"> <fmt:message
									key="bsCodigosBanco.filtro.etiqueta.CodigoTransfer" />
							</span> <span class="forminput" style="display: inline"> <input
								tabindex="4" type="text" id="idCodigoTransferLb"
								name="codigoTransfer" value="${bsCodigosBancoFiltro['codigoTransfer']}" />
							</span>
							<c:forEach
								items="${mensajesUsuario.mapaDeErroresEnPropiedades['codigoTransfer'].mensaje}"
								var="mensaje">
								<span style="font-size: 90%;" class="ui-state-error"> <c:out
										value="${mensaje}" escapeXml="false" />
								</span>
							</c:forEach>
						</div>
					</c:if>
					<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['idRedOperacion']}">
							<div class="row" >
								<span class="label"> 
									<fmt:message key="hthProcesoMonitoreo.filtro.etiqueta.Cliente"/>
								</span>
								<span class="forminput" style="display: inline">
						            	            <input type="text" id="idRedOperacion" name="idRedOperacion" value="${bsCodigosBancoFiltro['bsRedOperacionBanco'].idRedOperacion}"  size="10"  readonly="readonly"/>
					                <input type="text" id="bsRedOperacionBanco.interpretacion" name="bsRedOperacionBanco.interpretacion" value="${bsCodigosBancoFiltro['bsRedOperacionBanco'].interpretacion}"  size="50"  readonly="readonly"/>		
									<button tabindex="5" id="btnbsRedOperacionBanco" class="sq" onClick="doForaneo('bsRedOperacionBanco', 'bsCodigosBanco/buscar');"><fmt:message key='globales.formulario.boton.lov'/></button>
								</span>
								<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['idRedOperacion'].mensaje}" var="mensaje">
								<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
								</c:forEach>
							</div>	
					</c:if>
					<c:if
						test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['status']}">
						<div class="row">
							<span class="label"> <fmt:message key="bsCodigosBanco.formulario.etiqueta.Estado" />
							</span> 
							<span class="forminput" style="display: inline"> 											
				                <select  id="idStatus" name="status" tabindex="6">			                
			            			<option value="ACTIVO" 
			            				<c:if test="${bsCodigosBancoFiltro['status'] == 'ACTIVO'}">selected="selected">selected</c:if>
			            			>
			            				<fmt:message key="bsCodigosBanco.formulario.comboLabel.EstadoActivo"/>
			            			</option>	                
			            			<option value="INACTIVO" 
			            				<c:if test="${bsCodigosBancoFiltro['status'] == 'INACTIVO'}">selected="selected">selected</c:if>
			            			>
			            				<fmt:message key="bsCodigosBanco.formulario.comboLabel.EstadoInactivo"/>
			            			</option>
				                </select>
							</span>
							<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['status'].mensaje}" var="mensaje">
								<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false" />
								</span>
							</c:forEach>
						</div>
					</c:if>
				</div>


			</div>
		</div>
	</form>

	<%@ include file="/WEB-INF/global/jsp/jq-footer.jsp"%>
</fmt:bundle>
</body>