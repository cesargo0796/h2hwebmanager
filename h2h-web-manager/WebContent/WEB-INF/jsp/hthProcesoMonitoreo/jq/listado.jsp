<%@ taglib prefix="c"       uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt"     uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:if test="${ not empty locale}">
	<fmt:setLocale value="${ locale }" />
</c:if>
<fmt:bundle basename="com.ofbox.davivienda.h2h.resourceBundle.MessagesResources">

<c:if test="${ not empty hthProcesoMonitoreoEstado.enlaceBusqueda.enllavador}">
	<c:set var="busquedaEnllavador" value="${hthProcesoMonitoreoEstado.enlaceBusqueda.enllavador}"/>	
</c:if>	
<c:if test="${ not empty hthProcesoMonitoreoEstado.enlaceDetalle}">
	<c:set var="detalle" value="${hthProcesoMonitoreoEstado.enlaceDetalle}"/>	
</c:if>	
<c:if test="${hthProcesoMonitoreoEstado.ordenamientoAplicado}">
	<c:set var="orden" value="${hthProcesoMonitoreoEstado.ordenamiento}"/>	
</c:if>		
<c:if test="${not empty hthProcesoMonitoreoSeg}">
	<c:set var="restriccion" value="${hthProcesoMonitoreoSeg}"/>
</c:if>

<c:set var="nombre" value="hthProcesoMonitoreo"/>
<c:set var="ubicacion" value="listado"/>

<% int counter = 0; %>
<%@ include file="/WEB-INF/global/jsp/jq-header.jsp" %>
<!-- Elementos no visibles -->	
	
<form  name="mainForm" id="mainForm" action="${NewtPageRenderData.actionUrl}" method="post">

<div id="mainContent">

<div class="breadCrumbHolder module" style="margin-bottom: 3px;">
<div id="breadcrumb" class="breadCrumb module ui-widget ui-widget-content" style="width: 99%;">
<ul>
	<li><a href="${ctx}/inicio/blank/ria"> HOME </a> </li>
<c:if test="${ not empty ubicacionAplicacion.ruta }">
	<input type="hidden" name="idUbicacionAplicacionActual" id="idUbicacionAplicacionActual" value="${ ubicacionAplicacion.idUbicacionAplicacionActual }"/>
	<c:forEach items="${ ubicacionAplicacion.ruta }" var="ub">
		<li><a href="javascript: doSubmitBreadcrumb('<c:out value="${ ub.submit }"/>', '<c:out value="${ ub.id }"/>')"><c:out value="${ ub.label }" escapeXml="false" /></a>  </li> 
	</c:forEach>			
</c:if>
	<li><fmt:message key="hthProcesoMonitoreo.bc.etiqueta.principal" /></li>
</ul>
</div>
	<input type="hidden" name="id_ubicacion_retorno" id="id_ubicacion_retorno" value=""/>
</div>

<c:if test="${ not empty detalle.cabecera}">
    <jsp:include page="/WEB-INF/jsp/${detalle.cabecera.locacion}/jq/cabecera.jsp" />
</c:if>

<%-- <div id="iTitle" class="ui-state-default ui-widget-content ui-corner-all">
	<fmt:message key="hthProcesoMonitoreo.listado.etiqueta.titulo"/>
</div>--%>
	<input type="hidden" id="maxRowNum" name="maxRowNum" value="<c:out value="${navegacion.registroFinal - 1}"/>">
	<input type="hidden" id="pageLoc" name="pageLoc" value="listado"/>
    <input type="hidden" id="eag" name="eag" value="${eag}"/>
	<c:if test="${ not empty busquedaEnllavador}">
	<input type="hidden" id="listadoLov" value="hthProcesoMonitoreo"/>
	</c:if>
	<c:if test="${ not empty esAsync}">
	<input type="hidden" id="esAsync" value="${esAsync}"/>
	</c:if>
    <input type="hidden" id="movimientoPagina" name="movimientoPagina"/>
    <input type="hidden" id="columna" name="columna"/>
    <input type="hidden" id="fenixf31seguridad" name="fenixf31seguridad" value="<c:out value='${fenixf31seguridad}' />" />
	<!-- valores de llaves -->
    <input type="hidden" alt="notForSubmit" id="idProcesoMonitoreo" name="idProcesoMonitoreo" value=""/>
	 <input type="hidden" id="expo" name="expo" value=""/>  



<%@ include file="/WEB-INF/global/jsp/mensajes-globales.jsp" %>

<c:if test="${ not empty busquedaEnllavador}">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em; font-size: 85%;"> 
	<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
 	<strong><fmt:message key="globales.listado.etiqueta.tipLov.titulo"/></strong>&nbsp;<fmt:message key="globales.listado.etiqueta.tipLov.mensaje"/>
	</p>
</div>
</c:if>
<!-- botones de acciones basicas -->
<div id="action-buttons">
<div id="action-bar">
<c:if test="${ empty busquedaEnllavador}">
	<c:if test="${restriccion.verDisponible}">
	<button id="btnVer" type="button" class="ui-button ui-corner-all ui-button-color" name="ver" onClick="if(isValueSelected()){ doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('hthProcesoMonitoreo/ver'); }"><fmt:message key="globales.listado.boton.ver"/></button>
	</c:if>
	<c:if test="${restriccion.crearDisponible}">
	<button id="btnCrear" type="button" class="ui-button ui-corner-all ui-button-color" name="crear" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('hthProcesoMonitoreo/crear');"><fmt:message key="globales.listado.boton.crear"/></button>
	</c:if>
	<c:if test="${restriccion.actualizarDisponible}">	
	<button id="btnActualizar" type="button" class="ui-button ui-corner-all ui-button-color" name="actualizar" onClick="if(isValueSelected()){ doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('hthProcesoMonitoreo/actualizar'); }"><fmt:message key="globales.listado.boton.actualizar"/></button>
	</c:if>
	<c:if test="${restriccion.eliminarDisponible}">
	<button id="btnEliminar" type="button" class="ui-button ui-corner-all ui-button-color" name="eliminar" onClick="if(isValueSelected()){$('#dialog-confirm').dialog('open');}"><fmt:message key="globales.listado.boton.eliminar"/></button>
	</c:if>
	<button id="btnActivar" type="button" class="ui-button ui-corner-all ui-button-color" name="activar" onClick="if(isValueSelected()){ doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('hthProcesoMonitoreo/activarDesactivarProceso'); }">Activacion</button>
	</c:if>
<c:if test="${ not empty busquedaEnllavador}">
	<c:if test="${restriccion.crearDisponible}">
	<button id="btnCrear" type="button" class="ui-button ui-corner-all ui-button-color" name="crear" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('hthProcesoMonitoreo/crear');"><fmt:message key="globales.listado.boton.crear"/></button>
	</c:if>
	<button id="btnSeleccionar" type="button" class="ui-button ui-corner-all ui-button-color" name="seleccionar" onClick="if(isValueSelected()){ doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('hthProcesoMonitoreo/buscarSeleccionar'); }"><fmt:message key="globales.listado.boton.seleccionar"/></button>
	<button id="btnCancelar" type="button" class="ui-button ui-corner-all ui-button-color" name="cancel" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('hthProcesoMonitoreo/cancelarBuscar');"><fmt:message key="globales.listado.boton.cancelar"/></button>
</c:if>
	<c:if test="${restriccion.filtrarDisponible}">	
	<button id="btnFiltrar" type="button" class="ui-button ui-corner-all ui-button-color" name="filter" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('hthProcesoMonitoreo/filtrar');"><fmt:message key="globales.listado.boton.filtrar"/></button>
	</c:if>	
	
</div>
<div id="navigation-bar">	
<c:if test="${navegacion.paginaAnterior}">				
	<button id="btnAnterior" type="button" name="anterior" onClick="$('#movimientoPagina').val('anterior');doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('hthProcesoMonitoreo/listado')"><fmt:message key="globales.listado.boton.anterior"/></button>
</c:if>
<c:if test="${!navegacion.paginaAnterior}">			
	<button id="btnAnterior" type="button" name="anterior" disabled="disabled"><fmt:message key="globales.listado.boton.anterior"/></button>
</c:if>
<c:if test="${not empty navegacion.listadoPaginas}">
<c:forEach items="${navegacion.listadoPaginas}" var="p">
	<c:if test="${p == navegacion.noPaginaActual}">
	<button id="pag<c:out value="${p}" />" class="ui-button-text ui-state-highlight" type="button" name="pag<c:out value="${p}" />" disabled="disabled"><c:out value="${p}" /></button>
	</c:if>
	<c:if test="${p != navegacion.noPaginaActual}">
	<button id="pag<c:out value="${p}" />"  onClick="$('#movimientoPagina').val('${p}');doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('hthProcesoMonitoreo/listado')" type="button" name="pag<c:out value="${p}"/>"><c:out value="${p}" /></button>	
	</c:if> 
</c:forEach>
</c:if>
<c:if test="${navegacion.paginaSiguiente}">			
	<button id="btnSiguiente" type="button" name="siguiente" onClick="$('#movimientoPagina').val('siguiente');doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('hthProcesoMonitoreo/listado')" ><fmt:message key="globales.listado.boton.siguiente"/></button>
</c:if>
<c:if test="${!navegacion.paginaSiguiente}">			
	<button id="btnSiguiente" type="button" name="siguiente" disabled="disabled"><fmt:message key="globales.listado.boton.siguiente"/></button>
</c:if>
	
</div>
</div>
<br/>

<div class="ui-widget fg-toolbar ui-toolbar ui-widget-header ui-corner-tl ui-corner-tr ui-helper-clearfix">

<c:if test="${ not empty orden}">
	<div style="display:block; float: left;">
		<button id="btnRemoverOrden" type="button" class="ui-button ui-corner-all ui-button-color" name="cancel" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('hthProcesoMonitoreo/removerOrdenamiento');"><fmt:message key="globales.listado.boton.removerOrden"/></button>
	</div>
</c:if>
<c:if test="${navegacion.filtroAplicado}">
	<div style="display:block; float: left;">
		<button id="btnRemoverFiltro" type="button" class="ui-button ui-corner-all ui-button-color" name="nofilter" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('hthProcesoMonitoreo/removerFiltrar');"><fmt:message key="globales.listado.boton.removerFiltro"/></button>
	</div>			
</c:if>
	<div id="crudTable_filter" style="display: block; text-align: right; float: right; margin-top: 6px; margin-bottom: 6px;"> 
		<fmt:message key="globales.listado.etiqueta.busquedaRapida"/>
		<select id="campoBusqueda" name="campoBusqueda" onchange="doBusquedaRapida('#busqueda', this.value, $('option:selected', this));" class="ui-widget ui-widget-content ui-state-default">
			<option value="convenio"><fmt:message key="hthProcesoMonitoreo.listado.etiqueta.Convenio"/></option>
			<option value="tipoconvenio"><fmt:message key="hthProcesoMonitoreo.listado.etiqueta.Tipoconvenio"/></option>
			<option value="convenioPEACH"ctype="combo"><fmt:message key="hthProcesoMonitoreo.listado.etiqueta.ConvenioPEACH"/></option>
			<option value="convenioNombre"ctype="combo"><fmt:message key="hthProcesoMonitoreo.listado.etiqueta.ConvenioNombre"/></option>
			<option value="lineaEncabezado"ctype="combo"><fmt:message key="hthProcesoMonitoreo.listado.etiqueta.LineaEncabezado"/></option>
			<option value="procesamientoSuperusuario"ctype="combo"><fmt:message key="hthProcesoMonitoreo.listado.etiqueta.ProcesamientoSuperusuario"/></option>
			<option value="estado"ctype="combo"><fmt:message key="hthProcesoMonitoreo.listado.etiqueta.Estado"/></option>
			<option value="emailCliente"><fmt:message key="hthProcesoMonitoreo.listado.etiqueta.EmailCliente"/></option>
		</select> &nbsp;
                <select  id="convenioPEACH" name="convenioPEACH" onchange="$('#busqueda').val(this.value); doFiltrarPor('hthProcesoMonitoreo');" class="ui-widget ui-state-default" style="display: none;">
				    <option value=""/>			
				    <option value="PE"><fmt:message key="hthProcesoMonitoreo.formulario.comboLabel.PE"/></option>
				    <option value="ACH"><fmt:message key="hthProcesoMonitoreo.formulario.comboLabel.ACH"/></option>
				    <option value="PS"><fmt:message key="hthProcesoMonitoreo.formulario.comboLabel.PS"/></option>
				    <option value="TEXT"><fmt:message key="hthProcesoMonitoreo.formulario.comboLabel.TEXT"/></option>
                </select>
                <select  id="convenioNombre" name="convenioNombre" onchange="$('#busqueda').val(this.value); doFiltrarPor('hthProcesoMonitoreo');" class="ui-widget ui-state-default" style="display: none;">
				    <option value=""/>			
				    <option value="PLANILLA"><fmt:message key="hthProcesoMonitoreo.formulario.comboLabel.PLANILLA"/></option>
				    <option value="PROVEEDORES"><fmt:message key="hthProcesoMonitoreo.formulario.comboLabel.PROVEEDORES"/></option>
				    <option value="COBROS"><fmt:message key="hthProcesoMonitoreo.formulario.comboLabel.COBROS"/></option>
				    <option value="CHEQUES"><fmt:message key="hthProcesoMonitoreo.formulario.comboLabel.CHEQUES"/></option>
				    <option value="ACH_PLANILLA"><fmt:message key="hthProcesoMonitoreo.formulario.comboLabel.PAGOPLANILLASACH"/></option>
				    <option value="ACH_PROVEEDORES"><fmt:message key="hthProcesoMonitoreo.formulario.comboLabel.PAGOPROVEEDORESACH"/></option>
                </select>
                <select  id="lineaEncabezado" name="lineaEncabezado" onchange="$('#busqueda').val(this.value); doFiltrarPor('hthProcesoMonitoreo');" class="ui-widget ui-state-default" style="display: none;">
				    <option value=""/>			
				    <option value="S"><fmt:message key="hthProcesoMonitoreo.formulario.comboLabel.Si"/></option>
				    <option value="N"><fmt:message key="hthProcesoMonitoreo.formulario.comboLabel.No"/></option>
                </select>
                <select  id="procesamientoSuperusuario" name="procesamientoSuperusuario" onchange="$('#busqueda').val(this.value); doFiltrarPor('hthProcesoMonitoreo');" class="ui-widget ui-state-default" style="display: none;">
				    <option value=""/>			
				    <option value="S"><fmt:message key="hthProcesoMonitoreo.formulario.comboLabel.Si"/></option>
				    <option value="N"><fmt:message key="hthProcesoMonitoreo.formulario.comboLabel.No"/></option>
                </select>
                <select  id="estado" name="estado" onchange="$('#busqueda').val(this.value); doFiltrarPor('hthProcesoMonitoreo');" class="ui-widget ui-state-default" style="display: none;">
				    <option value=""/>			
				    <option value="A"><fmt:message key="hthProcesoMonitoreo.formulario.comboLabel.Activado"/></option>
				    <option value="D"><fmt:message key="hthProcesoMonitoreo.formulario.comboLabel.Desactivado"/></option>
                </select>
		<input onkeypress="doFiltrarPor('hthProcesoMonitoreo', event)" type="text" id="busqueda" name="busqueda" class="ui-widget ui-widget-content " style="width:150px; height: 18px;"/>
	</div>
</div>
<c:if test="${restriccion.listadoDisponible or not empty busquedaEnllavador}">
<table id="crudTable" width="100%">
	<thead>
		<tr>
  			<th onclick="doSort('cliente','${nombre}')" sort="${orden.mapaDeValores['cliente'].strOrden}"><fmt:message key="hthProcesoMonitoreo.listado.etiqueta.Cliente"/></th>
  			<th onclick="doSort('idVistaConvenio','${nombre}')" sort="${orden.mapaDeValores['idVistaConvenio'].strOrden}"><fmt:message key="hthProcesoMonitoreo.listado.etiqueta.IdVistaConvenio"/></th>
  			<th onclick="doSort('convenio','${nombre}')" sort="${orden.mapaDeValores['convenio'].strOrden}"><fmt:message key="hthProcesoMonitoreo.listado.etiqueta.Convenio"/></th>
  			<th onclick="doSort('tipoconvenio','${nombre}')" sort="${orden.mapaDeValores['tipoconvenio'].strOrden}"><fmt:message key="hthProcesoMonitoreo.listado.etiqueta.Tipoconvenio"/></th>
  			<th onclick="doSort('empresaodepto','${nombre}')" sort="${orden.mapaDeValores['empresaodepto'].strOrden}"><fmt:message key="hthProcesoMonitoreo.listado.etiqueta.Empresaodepto"/></th>
  			<th onclick="doSort('nombreArchivo','${nombre}')" sort="${orden.mapaDeValores['nombreArchivo'].strOrden}"><fmt:message key="hthProcesoMonitoreo.listado.etiqueta.NombreArchivo"/></th>
  			<%-- <th onclick="doSort('rutaSftp','${nombre}')" sort="${orden.mapaDeValores['rutaSftp'].strOrden}"><fmt:message key="hthProcesoMonitoreo.listado.etiqueta.RutaSftp"/></th>
  			<th onclick="doSort('rutaArchivosIn','${nombre}')" sort="${orden.mapaDeValores['rutaArchivosIn'].strOrden}"><fmt:message key="hthProcesoMonitoreo.listado.etiqueta.RutaArchivosIn"/></th>
  			<th onclick="doSort('rutaArchivosOut','${nombre}')" sort="${orden.mapaDeValores['rutaArchivosOut'].strOrden}"><fmt:message key="hthProcesoMonitoreo.listado.etiqueta.RutaArchivosOut"/></th>
  			<th onclick="doSort('rutaProcesados','${nombre}')" sort="${orden.mapaDeValores['rutaProcesados'].strOrden}"><fmt:message key="hthProcesoMonitoreo.listado.etiqueta.RutaProcesados"/></th>
  			<th onclick="doSort('tiempoMonitoreo','${nombre}')" sort="${orden.mapaDeValores['tiempoMonitoreo'].strOrden}"><fmt:message key="hthProcesoMonitoreo.listado.etiqueta.TiempoMonitoreo"/></th> --%>
  			<th onclick="doSort('lineaEncabezado','${nombre}')" sort="${orden.mapaDeValores['lineaEncabezado'].strOrden}"><fmt:message key="hthProcesoMonitoreo.listado.etiqueta.LineaEncabezado"/></th>
  			<th onclick="doSort('procesamientoSuperusuario','${nombre}')" sort="${orden.mapaDeValores['procesamientoSuperusuario'].strOrden}"><fmt:message key="hthProcesoMonitoreo.listado.etiqueta.ProcesamientoSuperusuario"/></th>
  			<th onclick="doSort('idFormatoEncabezado','${nombre}')" sort="${orden.mapaDeValores['idFormatoEncabezado'].strOrden}"><fmt:message key="hthProcesoMonitoreo.listado.etiqueta.IdFormatoEncabezado"/></th>
  			<th onclick="doSort('idFormatoDetalle','${nombre}')" sort="${orden.mapaDeValores['idFormatoDetalle'].strOrden}"><fmt:message key="hthProcesoMonitoreo.listado.etiqueta.IdFormatoDetalle"/></th>
  			<th onclick="doSort('montoMaximoProcesar','${nombre}')" sort="${orden.mapaDeValores['montoMaximoProcesar'].strOrden}"><fmt:message key="hthProcesoMonitoreo.listado.etiqueta.MontoMaximoProcesar"/></th>
  			<th onclick="doSort('estado','${nombre}')" sort="${orden.mapaDeValores['estado'].strOrden}"><fmt:message key="hthProcesoMonitoreo.listado.etiqueta.Estado"/></th>
  			<%-- <th onclick="doSort('usuarioCreacion','${nombre}')" sort="${orden.mapaDeValores['usuarioCreacion'].strOrden}"><fmt:message key="hthProcesoMonitoreo.listado.etiqueta.UsuarioCreacion"/></th>
  			<th onclick="doSort('fechaCreacion','${nombre}')" sort="${orden.mapaDeValores['fechaCreacion'].strOrden}"><fmt:message key="hthProcesoMonitoreo.listado.etiqueta.FechaCreacion"/></th>
  			<th onclick="doSort('usuarioUltAct','${nombre}')" sort="${orden.mapaDeValores['usuarioUltAct'].strOrden}"><fmt:message key="hthProcesoMonitoreo.listado.etiqueta.UsuarioUltAct"/></th>
  			<th onclick="doSort('fechaUltAct','${nombre}')" sort="${orden.mapaDeValores['fechaUltAct'].strOrden}"><fmt:message key="hthProcesoMonitoreo.listado.etiqueta.FechaUltAct"/></th> 
  			<th onclick="doSort('emailCliente','${nombre}')" sort="${orden.mapaDeValores['emailCliente'].strOrden}"><fmt:message key="hthProcesoMonitoreo.listado.etiqueta.EmailCliente"/></th>--%>
		</tr>
	</thead>	
	<tbody>
<c:forEach items="${coleccionResultados}" var="dto">
		<tr onclick="$('#idProcesoMonitoreo').val('${dto.idProcesoMonitoreo}'); selectDeSelectObject(this, 'ui-state-active'); sel = true; currentRow = $(this).parent().children().index($(this));">  
	    <td><c:out value="${dto.bsCliente.nombre}"/></td>
	    <td><c:out value="${dto.hthVistaConvenio.nombre}"/></td>
		<td><c:out value="${dto.convenio}"/></td>
		<td><c:out value="${dto.tipoconvenio}"/></td>
	    <td><c:out value="${dto.bsEmpresaodepto.nombre}"/></td>
		<td><c:out value="${dto.nombreArchivo}"/></td>
		<%-- <td><c:out value="${dto.rutaSftp}"/></td>
		<td><c:out value="${dto.rutaArchivosIn}"/></td>
		<td><c:out value="${dto.rutaArchivosOut}"/></td>
		<td><c:out value="${dto.rutaProcesados}"/></td>
		<td><c:out value="${dto.tiempoMonitoreo}"/></td> --%>
		<td>
		<c:if test="${dto.lineaEncabezado == 'S'}"><fmt:message key="hthProcesoMonitoreo.formulario.comboLabel.Si"/></c:if>
		<c:if test="${dto.lineaEncabezado == 'N'}"><fmt:message key="hthProcesoMonitoreo.formulario.comboLabel.No"/></c:if>
	    </td>
	    <td>
		<c:if test="${dto.procesamientoSuperusuario == 'S'}"><fmt:message key="hthProcesoMonitoreo.formulario.comboLabel.Si"/></c:if>
		<c:if test="${dto.procesamientoSuperusuario == 'N'}"><fmt:message key="hthProcesoMonitoreo.formulario.comboLabel.No"/></c:if>
	    </td>
	    <td><c:out value="${dto.hthFormatoEncabezado.nombreFormato}"/></td>
	    <td><c:out value="${dto.hthFormato.nombreFormato}"/></td>
		<td align="right"><fmt:formatNumber pattern="#,##0.00"  value="${dto.montoMaximoProcesar}" /></td>
		<td><strong>
		<c:if test="${dto.estado == 'A'}"><fmt:message key="hthProcesoMonitoreo.formulario.comboLabel.Activado"/></c:if>
		<c:if test="${dto.estado == 'D'}"><fmt:message key="hthProcesoMonitoreo.formulario.comboLabel.Desactivado"/></c:if>
	    </strong></td>
		<%-- <td><c:out value="${dto.usuarioCreacion}"/></td>
		<td><fmt:formatDate pattern="dd-MM-yyyy" value="${dto.fechaCreacion}" /></td>
		<td><c:out value="${dto.usuarioUltAct}"/></td>
		<td><fmt:formatDate pattern="dd-MM-yyyy" value="${dto.fechaUltAct}" /></td> 
		<td><c:out value="${dto.emailCliente}"/></td>--%>
		</tr>
</c:forEach>
	</tbody>
</table>
</c:if>
<div class="ui-widget fg-toolbar ui-toolbar ui-widget-header ui-corner-bl ui-corner-br ui-helper-clearfix">
	<c:if test="${navegacion.totalRegistros <= 0}">
		<fmt:message key="globales.listado.etiqueta.sinRegistros" />
    </c:if>
    <c:if test="${navegacion.totalRegistros > 0}">
    	<fmt:message key="globales.listado.etiqueta.numRegistros">
    		<fmt:param><c:out value="${navegacion.registroInicial}"/></fmt:param>
    		<fmt:param><c:out value="${navegacion.registroFinal}"/></fmt:param>
    		<fmt:param><c:out value="${navegacion.totalRegistros}"/></fmt:param>
    	</fmt:message>
    </c:if>	
</div>
<br/>
</div>
</form>
<%@ include file="/WEB-INF/global/jsp/jq-footer.jsp" %>
</fmt:bundle>
