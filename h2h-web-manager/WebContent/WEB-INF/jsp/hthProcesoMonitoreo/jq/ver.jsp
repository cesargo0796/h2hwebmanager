<%@ taglib prefix="c"       uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt"     uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:if test="${ not empty locale}">
	<fmt:setLocale value="${ locale }" />
</c:if>
<fmt:bundle basename="com.ofbox.davivienda.h2h.resourceBundle.MessagesResources">
<c:if test="${ not empty hthProcesoMonitoreoEstado.enlaceDetalle}">
	<c:set var="detalle" value="${hthProcesoMonitoreoEstado.enlaceDetalle}"/>	
	<c:set var="detalleEnllavador" value="${detalle.enllavador}"/>
</c:if>		

<c:set var="nombre" value="hthProcesoMonitoreo"/>
<c:set var="ubicacion" value="ver"/>

<%@ include file="/WEB-INF/global/jsp/jq-header.jsp" %>

<form  name="mainForm" id="mainForm" action="${NewtPageRenderData.actionUrl}" method="post">
<c:if test="${ not empty detalle.cabecera}">
    <jsp:include page="/WEB-INF/jsp/${detalle.cabecera.locacion}/jq/cabecera.jsp" />
</c:if>
<div id="mainContent">
	<div class="breadCrumbHolder module" style="margin-bottom: 3px;">
<div id="breadcrumb" class="breadCrumb module ui-widget ui-widget-content" style="width: 99%;">
<ul>
	<li><a href="${ctx}/inicio/blank/ria"> HOME </a> </li>
<c:if test="${ not empty ubicacionAplicacion.ruta }">
	<input type="hidden" name="idUbicacionAplicacionActual" id="idUbicacionAplicacionActual" value="${ ubicacionAplicacion.idUbicacionAplicacionActual }"/>
	<c:forEach items="${ ubicacionAplicacion.ruta }" var="ub">
		<li><a href="javascript: doSubmitBreadcrumb('<c:out value="${ ub.submit }"/>', '<c:out value="${ ub.id }"/>')"><c:out value="${ ub.label }" escapeXml="false" /></a>  </li> 
	</c:forEach>			
</c:if>
	<li><fmt:message key="hthProcesoMonitoreo.bc.etiqueta.ver" /></li>
</ul>
</div>
	<input type="hidden" name="id_ubicacion_retorno" id="id_ubicacion_retorno" value=""/>
</div>
	
<%--<div id="iTitle" class="ui-state-default ui-widget-content ui-corner-all">
	<fmt:message key="hthProcesoMonitoreo.formulario.etiqueta.ver.titulo"/>
</div>--%>

    <div id="iProcessing" style="visibility: hidden" align="center">
    	<img src="${ctx}/include/images/design/processing_request.gif"></img>
    </div>
	<input type="hidden" id="pageLoc" name="pageLoc" value="ver"/>
    <input type="hidden" id="eag" name="eag" value="${eag}"/>
    <input type="hidden" id="movimientoPagina" name="movimientoPagina"/>
    <input type="hidden" id="columna" name="columna"/>
    <input type="hidden" id="fenixf31seguridad" name="fenixf31seguridad" value="<c:out value='${fenixf31seguridad}' />" />
	<!-- valores de llaves -->
	
	
	<input type="hidden" id="nombreBusqueda" name="nombreBusqueda"/>

<div id="action-buttons">
<div id="action-bar">
	<button id="btnCancelar" type="button" name="regresar" onClick="doSubmit<c:if test="${ not empty detalle.cabecera}">Async</c:if>('hthProcesoMonitoreo/cancelarVer');"><fmt:message key="globales.formulario.boton.regresar"/></button>
</div>
</div>
<br />

<!-- datos de formulario -->
<div class="ui-widget ui-widget-content ui-corner-all" style="width: 99%">
		<div id="formInputHeader" class="ui-widget ui-widget-header ui-corner-top" style="padding-left: 5px;"> 
			<fmt:message key="hthProcesoMonitoreo.formulario.etiqueta.ver.titulo"/> 
		</div>
		
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="hthProcesoMonitoreo.formulario.etiqueta.Cliente"/>
			</span>	 
			
			<span class="formvalue">
	            [${hthProcesoMonitoreo.cliente}] - ${hthProcesoMonitoreo.bsCliente.nombre}
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="hthProcesoMonitoreo.formulario.etiqueta.IdVistaConvenio"/>
			</span>	 
			
			<span class="formvalue">
	            [${hthProcesoMonitoreo.idVistaConvenio}] - ${hthProcesoMonitoreo.hthVistaConvenio.nombre}
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="hthProcesoMonitoreo.formulario.etiqueta.Empresaodepto"/>
			</span>	 
			
			<span class="formvalue">
	            [${hthProcesoMonitoreo.empresaodepto}] - ${hthProcesoMonitoreo.bsEmpresaodepto.nombre}
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="hthProcesoMonitoreo.formulario.etiqueta.ConvenioPEACH"/>
			</span>	 
			
			<span class="formvalue">
			<c:if test="${hthProcesoMonitoreo.convenioPEACH == 'PE'}"><fmt:message key="hthProcesoMonitoreo.formulario.comboLabel.PE"/></c:if>	
			<c:if test="${hthProcesoMonitoreo.convenioPEACH == 'ACH'}"><fmt:message key="hthProcesoMonitoreo.formulario.comboLabel.ACH"/></c:if>
			<c:if test="${hthProcesoMonitoreo.convenioPEACH == 'PS'}"><fmt:message key="hthProcesoMonitoreo.formulario.comboLabel.PS"/></c:if>
			<c:if test="${hthProcesoMonitoreo.convenioPEACH == 'TEXT'}"><fmt:message key="hthProcesoMonitoreo.formulario.comboLabel.TEXT"/></c:if>	
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="hthProcesoMonitoreo.formulario.etiqueta.ConvenioNombre"/>
			</span>	 
			
			<span class="formvalue">
			<c:if test="${hthProcesoMonitoreo.convenioNombre == 'PLANILLA'}"><fmt:message key="hthProcesoMonitoreo.formulario.comboLabel.PLANILLA"/></c:if>	
			<c:if test="${hthProcesoMonitoreo.convenioNombre == 'PROVEEDORES'}"><fmt:message key="hthProcesoMonitoreo.formulario.comboLabel.PROVEEDORES"/></c:if>	
			<c:if test="${hthProcesoMonitoreo.convenioNombre == 'COBROS'}"><fmt:message key="hthProcesoMonitoreo.formulario.comboLabel.COBROS"/></c:if>	
			<c:if test="${hthProcesoMonitoreo.convenioNombre == 'CHEQUES'}"><fmt:message key="hthProcesoMonitoreo.formulario.comboLabel.CHEQUES"/></c:if>	
			<c:if test="${hthProcesoMonitoreo.convenioNombre == 'ACH_PLANILLA'}"><fmt:message key="hthProcesoMonitoreo.formulario.comboLabel.PAGOPLANILLASACH"/></c:if>	
			<c:if test="${hthProcesoMonitoreo.convenioNombre == 'ACH_PROVEEDORES'}"><fmt:message key="hthProcesoMonitoreo.formulario.comboLabel.PAGOPROVEEDORESACH"/></c:if>	
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="hthProcesoMonitoreo.formulario.etiqueta.NombreArchivo"/>
			</span>	 
			
			<span class="formvalue">
                ${hthProcesoMonitoreo.nombreArchivo}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="hthProcesoMonitoreo.formulario.etiqueta.RutaSftp"/>
			</span>	 
			
			<span class="formvalue">
                ${hthProcesoMonitoreo.rutaSftp}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="hthProcesoMonitoreo.formulario.etiqueta.RutaArchivosIn"/>
			</span>	 
			
			<span class="formvalue">
                ${hthProcesoMonitoreo.rutaArchivosIn}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="hthProcesoMonitoreo.formulario.etiqueta.RutaArchivosOut"/>
			</span>	 
			
			<span class="formvalue">
                ${hthProcesoMonitoreo.rutaArchivosOut}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="hthProcesoMonitoreo.formulario.etiqueta.RutaProcesados"/>
			</span>	 
			
			<span class="formvalue">
                ${hthProcesoMonitoreo.rutaProcesados}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="hthProcesoMonitoreo.formulario.etiqueta.TiempoMonitoreo"/>
			</span>	 
			
			<span class="formvalue">
                ${hthProcesoMonitoreo.tiempoMonitoreo}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="hthProcesoMonitoreo.formulario.etiqueta.LineaEncabezado"/>
			</span>	 
			
			<span class="formvalue">
			<c:if test="${hthProcesoMonitoreo.lineaEncabezado == 'S'}"><fmt:message key="hthProcesoMonitoreo.formulario.comboLabel.Si"/></c:if>	
			<c:if test="${hthProcesoMonitoreo.lineaEncabezado == 'N'}"><fmt:message key="hthProcesoMonitoreo.formulario.comboLabel.No"/></c:if>	
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="hthProcesoMonitoreo.formulario.etiqueta.IdFormatoEncabezado"/>
			</span>	 
			
			<span class="formvalue">
	            [${hthProcesoMonitoreo.idFormatoEncabezado}] - ${hthProcesoMonitoreo.hthFormatoEncabezado.nombreFormato}
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="hthProcesoMonitoreo.formulario.etiqueta.ProcesamientoSuperusuario"/>
			</span>	 
			
			<span class="formvalue">
			<c:if test="${hthProcesoMonitoreo.procesamientoSuperusuario == 'S'}"><fmt:message key="hthProcesoMonitoreo.formulario.comboLabel.Si"/></c:if>	
			<c:if test="${hthProcesoMonitoreo.procesamientoSuperusuario == 'N'}"><fmt:message key="hthProcesoMonitoreo.formulario.comboLabel.No"/></c:if>	
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="hthProcesoMonitoreo.formulario.etiqueta.IdFormatoDetalle"/>
			</span>	 
			
			<span class="formvalue">
	            [${hthProcesoMonitoreo.idFormatoDetalle}] - ${hthProcesoMonitoreo.hthFormato.nombreFormato}
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="hthProcesoMonitoreo.formulario.etiqueta.MontoMaximoProcesar"/>
			</span>	 
			
			<span class="formvalue">
                ${hthProcesoMonitoreo.montoMaximoProcesar}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="hthProcesoMonitoreo.formulario.etiqueta.Estado"/>
			</span>	 
			
			<span class="formvalue">
			<c:if test="${hthProcesoMonitoreo.estado == 'A'}"><fmt:message key="hthProcesoMonitoreo.formulario.comboLabel.Activado"/></c:if>	
			<c:if test="${hthProcesoMonitoreo.estado == 'D'}"><fmt:message key="hthProcesoMonitoreo.formulario.comboLabel.Desactivado"/></c:if>	
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="hthProcesoMonitoreo.formulario.etiqueta.UsuarioCreacion"/>
			</span>	 
			
			<span class="formvalue">
                ${hthProcesoMonitoreo.usuarioCreacion}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="hthProcesoMonitoreo.formulario.etiqueta.FechaCreacion"/>
			</span>	 
			
			<span class="formvalue">
                <fmt:formatDate pattern="dd-MM-yyyy" value="${hthProcesoMonitoreo.fechaCreacion}" />
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="hthProcesoMonitoreo.formulario.etiqueta.UsuarioUltAct"/>
			</span>	 
			
			<span class="formvalue">
                ${hthProcesoMonitoreo.usuarioUltAct}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="hthProcesoMonitoreo.formulario.etiqueta.FechaUltAct"/>
			</span>	 
			
			<span class="formvalue">
                <fmt:formatDate pattern="dd-MM-yyyy" value="${hthProcesoMonitoreo.fechaUltAct}" />
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="hthProcesoMonitoreo.formulario.etiqueta.EmailCliente"/>
			</span>	 
			
			<span class="formvalue">
                ${hthProcesoMonitoreo.emailCliente}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
		
</div>		
</div>
</form>

<%@ include file="/WEB-INF/global/jsp/jq-footer.jsp" %>
</fmt:bundle>
