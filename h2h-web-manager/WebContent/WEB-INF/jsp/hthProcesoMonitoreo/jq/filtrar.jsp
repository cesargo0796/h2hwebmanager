<%@ taglib prefix="c"       uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt"     uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:if test="${ not empty locale}">
	<fmt:setLocale value="${ locale }" />
</c:if>
<fmt:bundle basename="com.ofbox.davivienda.h2h.resourceBundle.MessagesResources">
<c:if test="${ not empty hthProcesoMonitoreoEstado.enlaceDetalle}">
	<c:set var="detalle" value="${hthProcesoMonitoreoEstado.enlaceDetalle}"/>	
	<c:set var="detalleEnllavador" value="${detalle.enllavador}"/>
</c:if>	

<c:set var="nombre" value="hthProcesoMonitoreo"/>
<c:set var="ubicacion" value="filtrar"/>
<c:set var="primerCampo" value="btnbsCliente"/>
<%@ include file="/WEB-INF/global/jsp/jq-header.jsp" %>

<form  name="mainForm" id="mainForm" action="${NewtPageRenderData.actionUrl}" method="post">
<c:if test="${ not empty detalle.cabecera}">
    <jsp:include page="/WEB-INF/jsp/${detalle.cabecera.locacion}/jq/cabecera.jsp" />
</c:if>	
<div id="mainContent">
	<div class="breadCrumbHolder module" style="margin-bottom: 3px;">
    <div id="breadcrumb" class="breadCrumb module ui-widget ui-widget-content" style="width: 99%;">
    <ul>
    	<li><a href="${ctx}/inicio/blank/ria"> HOME </a> </li>
    <c:if test="${ not empty ubicacionAplicacion.ruta }">
    	<input type="hidden" name="idUbicacionAplicacionActual" id="idUbicacionAplicacionActual" value="${ ubicacionAplicacion.idUbicacionAplicacionActual }"/>
    	<c:forEach items="${ ubicacionAplicacion.ruta }" var="ub">
    		<li><a href="javascript: doSubmitBreadcrumb('<c:out value="${ ub.submit }"/>', '<c:out value="${ ub.id }"/>')"><c:out value="${ ub.label }" escapeXml="false" /></a>  </li> 
    	</c:forEach>			
    </c:if>
    	<li><fmt:message key="hthProcesoMonitoreo.bc.etiqueta.filtrar" /></li>
    </ul>
    </div>
    	<input type="hidden" name="id_ubicacion_retorno" id="id_ubicacion_retorno" value=""/>
    </div>
	
<%--<div id="iTitle" class="ui-state-default ui-widget-content ui-corner-all">
	<fmt:message key="hthProcesoMonitoreo.formulario.etiqueta.filtrar.titulo"/>
</div>--%>
<input type="hidden" id="pageLoc" name="pageLoc" value="filtrar"/>
	<input type="hidden" id="primerCampo" name="primerCampo" value="btnbsCliente"/>
    <input type="hidden" id="eag" name="eag" value="${eag}"/>
    <input type="hidden" id="movimientoPagina" name="movimientoPagina"/>
    <input type="hidden" id="columna" name="columna"/>
    <input type="hidden" id="fenixf31seguridad" name="fenixf31seguridad" value="<c:out value='${fenixf31seguridad}' />" />
	<c:if test="${not empty scrollTo }">
    <input type="hidden" id="scrollSmooth" value="<c:out value='${scrollTo}' />" />
    </c:if>
	<!-- valores de llaves -->
<input type="hidden" id="nombreBusqueda" name="nombreBusqueda"/>


<!-- mensajes de error-->
<c:if test="${mensajesUsuario.erroresGlobales}">
<div id="iError">
	<b class="ui-round-top"> 
	<b class="ui-line-one"></b><b class="ui-line-two"></b><b class="ui-line-three"></b><b class="ui-line-four"></b> 
    </b> 
  	<table>
     	<tr>
       		<td>
         	<ol>
	     	<c:forEach items="${mensajesUsuario.mensajesDeError}" var="mensaje">
	        	<li><c:out value="${mensaje}" escapeXml="false"/></li>
	     	</c:forEach>
	     	</ol>
       		</td>
     	</tr>
  	</table>
  	<b class="ui-round-bottom"> 
	<b class="ui-line-four"></b><b class="ui-line-three"></b><b class="ui-line-two"></b><b class="ui-line-one"></b> 
  	</b> 
</div>
</c:if>

<div id="action-buttons">
<div id="action-bar">
	<button tabindex="45" id="btnAplicarFiltro" type="submit" name="Filtrar" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('hthProcesoMonitoreo/filtrarEjecutar');"><fmt:message key="globales.filtro.boton.aplicarFiltro"/></button>
	<button tabindex="46" id="btnLimpiar" type="button" name="Limpiar" onClick="limpiarCampos('mainForm');"><fmt:message key="globales.filtro.boton.limpiar"/></button>
	<button tabindex="47" id="btnCancelar" type="button" name="Cancelar" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('hthProcesoMonitoreo/cancelarFiltrar');"><fmt:message key="globales.filtro.boton.cancelar"/></button>
</div>
</div>
<br />

<!-- datos de formulario -->
<div class="ui-widget ui-widget-content ui-corner-all" style="width: 99%">
		<div id="formInputHeader" class="ui-widget ui-widget-header ui-corner-top" style="padding-left: 5px;"> 
			<fmt:message key="hthProcesoMonitoreo.formulario.etiqueta.filtrar.titulo"/> 
		</div>
		<div style="display: inline;" id="formInput">
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['cliente']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="hthProcesoMonitoreo.filtro.etiqueta.Cliente"/>
			</span>
			<span class="forminput" style="display: inline">
	            	            <input type="text" id="cliente" name="cliente" value="${hthProcesoMonitoreoFiltro['bsCliente'].cliente}"  size="10"  readonly="readonly"/>
                <input type="text" id="bsCliente.nombre" name="bsCliente.nombre" value="${hthProcesoMonitoreoFiltro['bsCliente'].nombre}"  size="40"  readonly="readonly"/>		
				<button tabindex="2" id="btnbsCliente" class="sq" onClick="doForaneo('bsCliente', 'hthProcesoMonitoreo/buscar');"><fmt:message key='globales.formulario.boton.lov'/></button>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['cliente'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['idVistaConvenio']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="hthProcesoMonitoreo.filtro.etiqueta.IdVistaConvenio"/>
			</span>
			<span class="forminput" style="display: inline">
	            	            <input type="text" id="idVistaConvenio" name="idVistaConvenio" value="${hthProcesoMonitoreoFiltro['hthVistaConvenio'].idVistaConvenio}"  size="10"  readonly="readonly"/>
                <input type="text" id="hthVistaConvenio.nombre" name="hthVistaConvenio.nombre" value="${hthProcesoMonitoreoFiltro['hthVistaConvenio'].nombre}"  size="40"  readonly="readonly"/>		
				<button tabindex="3" id="btnhthVistaConvenio" class="sq" onClick="doForaneo('hthVistaConvenio', 'hthProcesoMonitoreo/buscar');"><fmt:message key='globales.formulario.boton.lov'/></button>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['idVistaConvenio'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['convenio']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="hthProcesoMonitoreo.filtro.etiqueta.Convenio"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="4" alt="integerMin" type="text" id="convenioMin" name="convenioMin" value="${hthProcesoMonitoreoFiltro['convenioMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="4" alt="integerMax" type="text" id="convenioMax" name="convenioMax" value="${hthProcesoMonitoreoFiltro['convenioMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['convenio'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['tipoconvenio']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="hthProcesoMonitoreo.filtro.etiqueta.Tipoconvenio"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="5" alt="integerMin" type="text" id="tipoconvenioMin" name="tipoconvenioMin" value="${hthProcesoMonitoreoFiltro['tipoconvenioMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="5" alt="integerMax" type="text" id="tipoconvenioMax" name="tipoconvenioMax" value="${hthProcesoMonitoreoFiltro['tipoconvenioMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['tipoconvenio'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['empresaodepto']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="hthProcesoMonitoreo.filtro.etiqueta.Empresaodepto"/>
			</span>
			<span class="forminput" style="display: inline">
	            	            <input type="text" id="empresaodepto" name="empresaodepto" value="${hthProcesoMonitoreoFiltro['bsEmpresaodepto'].empresaodepto}"  size="10"  readonly="readonly"/>
                <input type="text" id="bsEmpresaodepto.nombre" name="bsEmpresaodepto.nombre" value="${hthProcesoMonitoreoFiltro['bsEmpresaodepto'].nombre}"  size="40"  readonly="readonly"/>		
				<button tabindex="6" id="btnbsEmpresaodepto" class="sq" onClick="doForaneo('bsEmpresaodepto', 'hthProcesoMonitoreo/buscar');"><fmt:message key='globales.formulario.boton.lov'/></button>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['empresaodepto'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['convenioPEACH']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="hthProcesoMonitoreo.filtro.etiqueta.ConvenioPEACH"/>
			</span>
			<span class="forminput" style="display: inline">
                <select  tabindex="7" id="convenioPEACH" name="convenioPEACH" class="ui-widget ui-state-default">
				    <option value="" display=""/>			
				    <option value="PE"<c:if test="${hthProcesoMonitoreoFiltro['convenioPEACH'] == 'PE'}">selected="selected"</c:if>	><fmt:message key="hthProcesoMonitoreo.formulario.comboLabel.PE"/></option>
				    <option value="ACH"<c:if test="${hthProcesoMonitoreoFiltro['convenioPEACH'] == 'ACH'}">selected="selected"</c:if>	><fmt:message key="hthProcesoMonitoreo.formulario.comboLabel.ACH"/></option>
				    <option value="PS"<c:if test="${hthProcesoMonitoreoFiltro['convenioPEACH'] == 'PS'}">selected="selected"</c:if>	><fmt:message key="hthProcesoMonitoreo.formulario.comboLabel.PS"/></option>
				    <option value="TEXT"<c:if test="${hthProcesoMonitoreoFiltro['convenioPEACH'] == 'TEXT'}">selected="selected"</c:if>	><fmt:message key="hthProcesoMonitoreo.formulario.comboLabel.TEXT"/></option>
			    </select>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['convenioPEACH'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['convenioNombre']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="hthProcesoMonitoreo.filtro.etiqueta.ConvenioNombre"/>
			</span>
			<span class="forminput" style="display: inline">
                <select  tabindex="8" id="convenioNombre" name="convenioNombre" class="ui-widget ui-state-default">
				    <option value="" display=""/>			
				    <option value="PLANILLA"<c:if test="${hthProcesoMonitoreoFiltro['convenioNombre'] == 'PLANILLA'}">selected="selected"</c:if>	><fmt:message key="hthProcesoMonitoreo.formulario.comboLabel.PLANILLA"/></option>
				    <option value="PROVEEDORES"<c:if test="${hthProcesoMonitoreoFiltro['convenioNombre'] == 'PROVEEDORES'}">selected="selected"</c:if>	><fmt:message key="hthProcesoMonitoreo.formulario.comboLabel.PROVEEDORES"/></option>
				    <option value="COBROS"<c:if test="${hthProcesoMonitoreoFiltro['convenioNombre'] == 'COBROS'}">selected="selected"</c:if>	><fmt:message key="hthProcesoMonitoreo.formulario.comboLabel.COBROS"/></option>
				    <option value="CHEQUES"<c:if test="${hthProcesoMonitoreoFiltro['convenioNombre'] == 'CHEQUES'}">selected="selected"</c:if>	><fmt:message key="hthProcesoMonitoreo.formulario.comboLabel.CHEQUES"/></option>
				    <option value="ACH_PLANILLA"<c:if test="${hthProcesoMonitoreoFiltro['convenioNombre'] == 'ACH_PLANILLA'}">selected="selected"</c:if>	><fmt:message key="hthProcesoMonitoreo.formulario.comboLabel.PAGOPLANILLASACH"/></option>
				    <option value="ACH_PROVEEDORES"<c:if test="${hthProcesoMonitoreoFiltro['convenioNombre'] == 'ACH_PROVEEDORES'}">selected="selected"</c:if>	><fmt:message key="hthProcesoMonitoreo.formulario.comboLabel.PAGOPROVEEDORESACH"/></option>
			    </select>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['convenioNombre'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['nombreArchivo']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="hthProcesoMonitoreo.filtro.etiqueta.NombreArchivo"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="9" type="text" id="nombreArchivo" name="nombreArchivo" value="${hthProcesoMonitoreoFiltro['nombreArchivo']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['nombreArchivo'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['rutaSftp']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="hthProcesoMonitoreo.filtro.etiqueta.RutaSftp"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="10" type="text" id="rutaSftp" name="rutaSftp" value="${hthProcesoMonitoreoFiltro['rutaSftp']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['rutaSftp'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['rutaArchivosIn']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="hthProcesoMonitoreo.filtro.etiqueta.RutaArchivosIn"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="11" type="text" id="rutaArchivosIn" name="rutaArchivosIn" value="${hthProcesoMonitoreoFiltro['rutaArchivosIn']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['rutaArchivosIn'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['rutaArchivosOut']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="hthProcesoMonitoreo.filtro.etiqueta.RutaArchivosOut"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="12" type="text" id="rutaArchivosOut" name="rutaArchivosOut" value="${hthProcesoMonitoreoFiltro['rutaArchivosOut']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['rutaArchivosOut'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['rutaProcesados']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="hthProcesoMonitoreo.filtro.etiqueta.RutaProcesados"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="13" type="text" id="rutaProcesados" name="rutaProcesados" value="${hthProcesoMonitoreoFiltro['rutaProcesados']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['rutaProcesados'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['tiempoMonitoreo']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="hthProcesoMonitoreo.filtro.etiqueta.TiempoMonitoreo"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="14" alt="integerMin" type="text" id="tiempoMonitoreoMin" name="tiempoMonitoreoMin" value="${hthProcesoMonitoreoFiltro['tiempoMonitoreoMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="14" alt="integerMax" type="text" id="tiempoMonitoreoMax" name="tiempoMonitoreoMax" value="${hthProcesoMonitoreoFiltro['tiempoMonitoreoMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['tiempoMonitoreo'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['lineaEncabezado']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="hthProcesoMonitoreo.filtro.etiqueta.LineaEncabezado"/>
			</span>
			<span class="forminput" style="display: inline">
                <select  tabindex="15" id="lineaEncabezado" name="lineaEncabezado" class="ui-widget ui-state-default">
				    <option value="" display=""/>			
				    <option value="S"<c:if test="${hthProcesoMonitoreoFiltro['lineaEncabezado'] == 'S'}">selected="selected"</c:if>	><fmt:message key="hthProcesoMonitoreo.formulario.comboLabel.Si"/></option>
				    <option value="N"<c:if test="${hthProcesoMonitoreoFiltro['lineaEncabezado'] == 'N'}">selected="selected"</c:if>	><fmt:message key="hthProcesoMonitoreo.formulario.comboLabel.No"/></option>
			    </select>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['lineaEncabezado'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['idFormatoEncabezado']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="hthProcesoMonitoreo.filtro.etiqueta.IdFormatoEncabezado"/>
			</span>
			<span class="forminput" style="display: inline">
	            	            <input type="text" id="idFormatoEncabezado" name="idFormatoEncabezado" value="${hthProcesoMonitoreoFiltro['hthFormatoEncabezado'].idFormato}"  size="10"  readonly="readonly"/>
                <input type="text" id="hthFormatoEncabezado.nombreFormato" name="hthFormatoEncabezado.nombreFormato" value="${hthProcesoMonitoreoFiltro['hthFormatoEncabezado'].nombreFormato}"  size="40"  readonly="readonly"/>		
				<button tabindex="16" id="btnhthFormatoEncabezado" class="sq" onClick="doForaneo('hthFormatoEncabezado', 'hthProcesoMonitoreo/buscar');"><fmt:message key='globales.formulario.boton.lov'/></button>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['idFormatoEncabezado'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['procesamientoSuperusuario']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="hthProcesoMonitoreo.filtro.etiqueta.ProcesamientoSuperusuario"/>
			</span>
			<span class="forminput" style="display: inline">
                <select  tabindex="17" id="procesamientoSuperusuario" name="procesamientoSuperusuario" class="ui-widget ui-state-default">
				    <option value="" display=""/>			
				    <option value="S"<c:if test="${hthProcesoMonitoreoFiltro['procesamientoSuperusuario'] == 'S'}">selected="selected"</c:if>	><fmt:message key="hthProcesoMonitoreo.formulario.comboLabel.Si"/></option>
				    <option value="N"<c:if test="${hthProcesoMonitoreoFiltro['procesamientoSuperusuario'] == 'N'}">selected="selected"</c:if>	><fmt:message key="hthProcesoMonitoreo.formulario.comboLabel.No"/></option>
			    </select>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['procesamientoSuperusuario'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['idFormatoDetalle']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="hthProcesoMonitoreo.filtro.etiqueta.IdFormatoDetalle"/>
			</span>
			<span class="forminput" style="display: inline">
	            	            <input type="text" id="idFormatoDetalle" name="idFormatoDetalle" value="${hthProcesoMonitoreoFiltro['hthFormato'].idFormato}"  size="10"  readonly="readonly"/>
                <input type="text" id="hthFormato.nombreFormato" name="hthFormato.nombreFormato" value="${hthProcesoMonitoreoFiltro['hthFormato'].nombreFormato}"  size="40"  readonly="readonly"/>		
				<button tabindex="18" id="btnhthFormato" class="sq" onClick="doForaneo('hthFormato', 'hthProcesoMonitoreo/buscar');"><fmt:message key='globales.formulario.boton.lov'/></button>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['idFormatoDetalle'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['montoMaximoProcesar']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="hthProcesoMonitoreo.filtro.etiqueta.MontoMaximoProcesar"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="19" alt="numericMin" type="text" id="montoMaximoProcesarMin" name="montoMaximoProcesarMin" value="${hthProcesoMonitoreoFiltro['montoMaximoProcesarMin']}"/>
				&nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
				<input tabindex="19" alt="numericMax" type="text" id="montoMaximoProcesarMax" name="montoMaximoProcesarMax" value="${hthProcesoMonitoreoFiltro['montoMaximoProcesarMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['montoMaximoProcesar'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['estado']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="hthProcesoMonitoreo.filtro.etiqueta.Estado"/>
			</span>
			<span class="forminput" style="display: inline">
                <select  tabindex="20" id="estado" name="estado" class="ui-widget ui-state-default">
				    <option value="" display=""/>			
				    <option value="A"<c:if test="${hthProcesoMonitoreoFiltro['estado'] == 'A'}">selected="selected"</c:if>	><fmt:message key="hthProcesoMonitoreo.formulario.comboLabel.Activado"/></option>
				    <option value="D"<c:if test="${hthProcesoMonitoreoFiltro['estado'] == 'D'}">selected="selected"</c:if>	><fmt:message key="hthProcesoMonitoreo.formulario.comboLabel.Desactivado"/></option>
			    </select>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['estado'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['emailCliente']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="hthProcesoMonitoreo.filtro.etiqueta.EmailCliente"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="25" type="text" id="emailCliente" name="emailCliente" value="${hthProcesoMonitoreoFiltro['emailCliente']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['emailCliente'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
</div>	


</div>
</div>
</form>

<%@ include file="/WEB-INF/global/jsp/jq-footer.jsp" %>
</fmt:bundle>
</body>