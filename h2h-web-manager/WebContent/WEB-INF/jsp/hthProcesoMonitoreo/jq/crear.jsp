<%@ taglib prefix="c"       uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt"     uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:if test="${ not empty locale}">
	<fmt:setLocale value="${locale}" />
</c:if>
<fmt:bundle basename="com.ofbox.davivienda.h2h.resourceBundle.MessagesResources">
	
<c:if test="${ not empty hthProcesoMonitoreoEstado.enlaceDetalle}">
	<c:set var="detalle" value="${hthProcesoMonitoreoEstado.enlaceDetalle}"/>	
	<c:set var="detalleEnllavador" value="${detalle.enllavador}"/>
</c:if>

<c:set var="nombre" value="hthProcesoMonitoreo"/>
<c:set var="ubicacion" value="crear"/>
<c:set var="primerCampo" value="btnbsCliente"/>
<%@ include file="/WEB-INF/global/jsp/jq-header.jsp" %>
<script>
	$(document).ready(function (){
		restringirValoresConvenioNombre();
		seleccionLineaEncabezado();
		
	});

	function restringirValoresConvenioNombre(){
		var convenioPEACH = $('#convenioPEACH').val();
		if(convenioPEACH == 'ACH'){
			$('#convenioNombre')
				    .find('option')
				    .remove()
				    .end()
				    .append('<option value="ACH_PLANILLA"><fmt:message key="hthProcesoMonitoreo.formulario.comboLabel.PAGOPLANILLASACH"/></option>')
				    .append('<option value="ACH_PROVEEDORES"><fmt:message key="hthProcesoMonitoreo.formulario.comboLabel.PAGOPROVEEDORESACH"/></option>')
				    
				;
		}
		else if(convenioPEACH == 'BEP'){
			$('#convenioNombre')
				    .find('option')
				    .remove()
				    .end()
				    .append('<option value="ACH_PLANILLA"><fmt:message key="hthProcesoMonitoreo.formulario.comboLabel.PAGOPLANILLASACH"/></option>')
				    .append('<option value="ACH_PROVEEDORES"><fmt:message key="hthProcesoMonitoreo.formulario.comboLabel.PAGOPROVEEDORESACH"/></option>')
				    
				;
		}
		else if(convenioPEACH == 'PS'){
			$('#convenioNombre')
				    .find('option')
				    .remove()
				    .end()
				    .append('<option value="ACH_PLANILLA"><fmt:message key="hthProcesoMonitoreo.formulario.comboLabel.PAGOPLANILLASACH"/></option>')
				    .append('<option value="ACH_PROVEEDORES"><fmt:message key="hthProcesoMonitoreo.formulario.comboLabel.PAGOPROVEEDORESACH"/></option>')
				    
				;
		}
		else if(convenioPEACH == 'TEXT'){
			$('#convenioNombre')
				    .find('option')
				    .remove()
				    .end()
				    .append('<option value="ACH_PLANILLA"><fmt:message key="hthProcesoMonitoreo.formulario.comboLabel.PAGOPLANILLASACH"/></option>')
				    .append('<option value="ACH_PROVEEDORES"><fmt:message key="hthProcesoMonitoreo.formulario.comboLabel.PAGOPROVEEDORESACH"/></option>')
				    
				;
		}		
		else{
			$('#convenioNombre')
				    .find('option')
				    .remove()
				    .end()
				    .append('<option value="PLANILLA"><fmt:message key="hthProcesoMonitoreo.formulario.comboLabel.PLANILLA"/></option>')
				    .append('<option value="PROVEEDORES"><fmt:message key="hthProcesoMonitoreo.formulario.comboLabel.PROVEEDORES"/></option>')
				    .append('<option value="COBROS"><fmt:message key="hthProcesoMonitoreo.formulario.comboLabel.COBROS"/></option>')
				    .append('<option value="CHEQUES"><fmt:message key="hthProcesoMonitoreo.formulario.comboLabel.CHEQUES"/></option>')
				    
				;
		}
		
	}
	
	function cambioNombreArchivo(){
		var convenioNombre = $('#convenioNombre').val();
		var convenio = $('#convenio').val();
		$('#nombreArchivo').val(convenioNombre + '_$D(\'yyyyMMdd\')_' + convenio + '.csv');
	}
	
	function seleccionLineaEncabezado(){
		var lineaEncabezado = $('#lineaEncabezado').val();
		if(lineaEncabezado == 'S'){
			$('#divIdFormatoEncabezado').show();
		}else{
			$('#divIdFormatoEncabezado').hide();
		}	
	}
</script>
	
<form  name="mainForm" id="mainForm" action="${NewtPageRenderData.actionUrl}" method="post">
<c:if test="${ not empty detalle.cabecera}">
    <jsp:include page="/WEB-INF/jsp/${detalle.cabecera.locacion}/jq/cabecera.jsp" />
</c:if>
<div id="mainContent">
	<div class="breadCrumbHolder module" style="margin-bottom: 3px;">
    <div id="breadcrumb" class="breadCrumb module ui-widget ui-widget-content" style="width: 99%;">
    <ul>
    	<li><a href="${ctx}/inicio/blank/ria"> HOME </a> </li>
    <c:if test="${ not empty ubicacionAplicacion.ruta }">
    	<input type="hidden" name="idUbicacionAplicacionActual" id="idUbicacionAplicacionActual" value="${ ubicacionAplicacion.idUbicacionAplicacionActual }"/>
    	<c:forEach items="${ ubicacionAplicacion.ruta }" var="ub">
    		<li><a href="javascript: doSubmitBreadcrumb('<c:out value="${ ub.submit }"/>', '<c:out value="${ ub.id }"/>')"><c:out value="${ ub.label }" escapeXml="false" /></a>  </li> 
    	</c:forEach>			
    </c:if>
    	<li><fmt:message key="hthProcesoMonitoreo.bc.etiqueta.crear" /></li>
    </ul>
    </div>
    	<input type="hidden" name="id_ubicacion_retorno" id="id_ubicacion_retorno" value=""/>
    </div>	
	
	
<%-- <div id="iTitle" class="ui-state-default ui-widget-content ui-corner-all">
	<fmt:message key="hthProcesoMonitoreo.formulario.etiqueta.crear.titulo"/>
</div> --%>
	<input type="hidden" id="pageLoc" name="pageLoc" value="crear"/>
	<input type="hidden" id="primerCampo" name="primerCampo" value="btnbsCliente"/>
    <input type="hidden" id="eag" name="eag" value="${eag}"/>
    <input type="hidden" id="movimientoPagina" name="movimientoPagina"/>
    <input type="hidden" id="columna" name="columna"/>
    <input type="hidden" id="fenixf31seguridad" name="fenixf31seguridad" value="<c:out value='${fenixf31seguridad}' />" />
	<c:if test="${not empty scrollTo }">
    <input type="hidden" id="scrollSmooth" value="<c:out value='${scrollTo}' />" />
    </c:if>
	<!-- valores de llaves -->
	
	
	<input type="hidden" id="nombreBusqueda" name="nombreBusqueda"/>


<!-- mensajes de error-->
<c:if test="${mensajesUsuario.erroresGlobales}">
<div class="ui-state-error ui-corner-all" style="padding: 0 .7em; font-size: 0.8em;"> 
	<span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em; margin-top: .3em;"></span> 
	<strong><fmt:message key="globales.formulario.etiqueta.error"/></strong> <br /> 
	<ul>
		<c:forEach items="${mensajesUsuario.mensajesDeError}" var="mensaje">
        	<li><c:out value="${mensaje}" escapeXml="false"/></li>
     	</c:forEach>
	</ul>
</div>
</c:if>

<div id="action-buttons">
<div id="action-bar">
    <button tabindex="23" id="btnGuardar" type="submit" name="ok" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('hthProcesoMonitoreo/crearEjecutar');"><fmt:message key="globales.formulario.boton.crear.guardar"/></button>
	<button tabindex="24" id="btnCancelar" type="button" name="cancel" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('hthProcesoMonitoreo/cancelarCrear');"><fmt:message key="globales.formulario.boton.crear.cancelar"/></button>
</div>
</div>
<br />

<!-- datos de formulario -->
<div><span style="font-size: small;">-Los campos marcados con * son obligatorios</span></div>
<div class="ui-widget ui-widget-content ui-corner-all" style="width: 99%">
		<div id="formInputHeader" class="ui-widget ui-widget-header ui-corner-top" style="padding-left: 5px;"> 
			<fmt:message key="hthProcesoMonitoreo.formulario.etiqueta.crear.titulo"/> 
		</div>
		<div style="display: inline;" id="formInput">
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['cliente']}">	
		<div class="row" >
			<span class="label" style="color: #AA0707;"> 
                <fmt:message key="hthProcesoMonitoreo.formulario.etiqueta.Cliente"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
	            <input readonly="readonly" type="text" id="cliente" name="cliente" value="${hthProcesoMonitoreo.cliente}"  size="10"  />
				<input readonly="readonly" type="text" id="bsCliente.nombre" name="bsCliente.nombre" value="${hthProcesoMonitoreo.bsCliente.nombre}"  size="40"  />
				<button tabindex="1" id="btnbsCliente" class="sq" onClick="doForaneo('bsCliente', 'hthProcesoMonitoreo/buscar');"><fmt:message key='globales.formulario.boton.lov'/></button>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('hthProcesoMonitoreo/ayudaPropiedadJson','cliente')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['cliente'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['idVistaConvenio']}">	
		<div class="row" >

			<span class="label" style="color: #AA0707;"> 
                <fmt:message key="hthProcesoMonitoreo.formulario.etiqueta.IdVistaConvenio"/><fmt:message key="globales.formulario.etiqueta.requerido"/>		
            </span>	 
			<span class="forminput" style="display: inline">
	            <input readonly="readonly" type="text" id="idVistaConvenio" name="idVistaConvenio" value="${hthProcesoMonitoreo.idVistaConvenio}"  size="10"  />
				<input readonly="readonly" type="text" id="hthVistaConvenio.nombre" name="hthVistaConvenio.nombre" value="${hthProcesoMonitoreo.hthVistaConvenio.nombre}"  size="40"  />
				<button tabindex="2" id="btnhthVistaConvenio" class="sq" onClick="doForaneo('hthVistaConvenio', 'hthProcesoMonitoreo/buscar');"><fmt:message key='globales.formulario.boton.lov'/></button>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('hthProcesoMonitoreo/ayudaPropiedadJson','idVistaConvenio')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['idVistaConvenio'].mensaje}" var="mensaje">

			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			<input type="hidden" id="convenio" name="convenio" value="${hthProcesoMonitoreo.convenio}"/>			
			<input type="hidden" id="tipoconvenio" name="tipoconvenio" value="${hthProcesoMonitoreo.tipoconvenio}"/>
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['empresaodepto']}">	
		<div class="row" >
			<span class="label" style="color: #AA0707;"> 
                <fmt:message key="hthProcesoMonitoreo.formulario.etiqueta.Empresaodepto"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
	            <input readonly="readonly" type="text" id="empresaodepto" name="empresaodepto" value="${hthProcesoMonitoreo.empresaodepto}"  size="10"  />
				<input readonly="readonly" type="text" id="bsEmpresaodepto.nombre" name="bsEmpresaodepto.nombre" value="${hthProcesoMonitoreo.bsEmpresaodepto.nombre}"  size="40"  />
				<button tabindex="3" id="btnbsEmpresaodepto" class="sq" onClick="doForaneo('bsEmpresaodepto', 'hthProcesoMonitoreo/buscar');"><fmt:message key='globales.formulario.boton.lov'/></button>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('hthProcesoMonitoreo/ayudaPropiedadJson','empresaodepto')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['empresaodepto'].mensaje}" var="mensaje">
				<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	

<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['convenioPEACH']}">	
<div class="row" >
			<span class="label" style="color: #AA0707;">
                <fmt:message key="hthProcesoMonitoreo.formulario.etiqueta.ConvenioPEACH"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 

			<span class="forminput" style="display: inline">
	            <select style="width: 160px;" tabindex="4" id="convenioPEACH" name="convenioPEACH" class="ui-widget ui-state-default" onchange="restringirValoresConvenioNombre();">
		            <option value="PE" <c:if test="${hthProcesoMonitoreo.convenioPEACH == 'PE'}">selected</c:if>><fmt:message key="hthProcesoMonitoreo.formulario.comboLabel.PE"/></option>
		            <option value="ACH" <c:if test="${hthProcesoMonitoreo.convenioPEACH == 'ACH'}">selected</c:if>><fmt:message key="hthProcesoMonitoreo.formulario.comboLabel.ACH"/></option>
		            <option value="PS" <c:if test="${hthProcesoMonitoreo.convenioPEACH == 'PS'}">selected</c:if>><fmt:message key="hthProcesoMonitoreo.formulario.comboLabel.PS"/></option>
		            <option value="TEXT" <c:if test="${hthProcesoMonitoreo.convenioPEACH == 'TEXT'}">selected</c:if>><fmt:message key="hthProcesoMonitoreo.formulario.comboLabel.TEXT"/></option>
			    </select>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('hthProcesoMonitoreo/ayudaPropiedadJson','convenioPEACH')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['convenioPEACH'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['convenioNombre']}">	
		<div class="row" >
			<span class="label" style="color: #AA0707;"> 
                <fmt:message key="hthProcesoMonitoreo.formulario.etiqueta.ConvenioNombre"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
	            <select style="width: 160px;" tabindex="5" id="convenioNombre" name="convenioNombre" class="ui-widget ui-state-default" onchange="cambioNombreArchivo();">
		            <option value="PLANILLA" <c:if test="${hthProcesoMonitoreo.convenioNombre == 'PLANILLA'}">selected</c:if>><fmt:message key="hthProcesoMonitoreo.formulario.comboLabel.PLANILLA"/></option>
		            <option value="PROVEEDORES" <c:if test="${hthProcesoMonitoreo.convenioNombre == 'PROVEEDORES'}">selected</c:if>><fmt:message key="hthProcesoMonitoreo.formulario.comboLabel.PROVEEDORES"/></option>
		            <option value="COBROS" <c:if test="${hthProcesoMonitoreo.convenioNombre == 'COBROS'}">selected</c:if>><fmt:message key="hthProcesoMonitoreo.formulario.comboLabel.COBROS"/></option>
		            <option value="CHEQUES" <c:if test="${hthProcesoMonitoreo.convenioNombre == 'CHEQUES'}">selected</c:if>><fmt:message key="hthProcesoMonitoreo.formulario.comboLabel.CHEQUES"/></option>
		            <option value="ACH_PLANILLA" <c:if test="${hthProcesoMonitoreo.convenioNombre == 'ACH_PLANILLA'}">selected</c:if>><fmt:message key="hthProcesoMonitoreo.formulario.comboLabel.PAGOPLANILLASACH"/></option>
		            <option value="ACH_PROVEEDORES" <c:if test="${hthProcesoMonitoreo.convenioNombre == 'ACH_PROVEEDORES'}">selected</c:if>><fmt:message key="hthProcesoMonitoreo.formulario.comboLabel.PAGOPROVEEDORESACH"/></option>
			    </select>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('hthProcesoMonitoreo/ayudaPropiedadJson','convenioNombre')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['convenioNombre'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['nombreArchivo']}">	
		<div class="row" >
			<span class="label" style="color: #AA0707;"> 
                <fmt:message key="hthProcesoMonitoreo.formulario.etiqueta.NombreArchivo"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="6" type="text" id="nombreArchivo" name="nombreArchivo"  value="${hthProcesoMonitoreo.nombreArchivo}" size="50"  maxlength="100"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('hthProcesoMonitoreo/ayudaPropiedadJson','nombreArchivo')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['nombreArchivo'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['rutaSftp']}">	
		<div class="row" >
			<span class="label" style="color: #AA0707;"> 
                <fmt:message key="hthProcesoMonitoreo.formulario.etiqueta.RutaSftp"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  		      <textarea tabindex="7" id="rutaSftp" name="rutaSftp" cols="45" rows="3"><c:out value="${hthProcesoMonitoreo.rutaSftp}"/></textarea>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('hthProcesoMonitoreo/ayudaPropiedadJson','rutaSftp')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['rutaSftp'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['rutaArchivosIn']}">	
		<div class="row" >
			<span class="label" style="color: #AA0707;"> 
                <fmt:message key="hthProcesoMonitoreo.formulario.etiqueta.RutaArchivosIn"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="8" type="text" id="rutaArchivosIn" name="rutaArchivosIn"  value="${hthProcesoMonitoreo.rutaArchivosIn}" size="50"  maxlength="100"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('hthProcesoMonitoreo/ayudaPropiedadJson','rutaArchivosIn')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['rutaArchivosIn'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['rutaArchivosOut']}">	
		<div class="row" >
			<span class="label" style="color: #AA0707;"> 
                <fmt:message key="hthProcesoMonitoreo.formulario.etiqueta.RutaArchivosOut"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="9" type="text" id="rutaArchivosOut" name="rutaArchivosOut"  value="${hthProcesoMonitoreo.rutaArchivosOut}" size="50"  maxlength="100"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('hthProcesoMonitoreo/ayudaPropiedadJson','rutaArchivosOut')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['rutaArchivosOut'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['rutaProcesados']}">	
		<div class="row" >
			<span class="label" style="color: #AA0707;"> 
                <fmt:message key="hthProcesoMonitoreo.formulario.etiqueta.RutaProcesados"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="10" type="text" id="rutaProcesados" name="rutaProcesados"  value="${hthProcesoMonitoreo.rutaProcesados}" size="50"  maxlength="100"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('hthProcesoMonitoreo/ayudaPropiedadJson','rutaProcesados')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['rutaProcesados'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['tiempoMonitoreo']}">	
		<div class="row" >
			<span class="label" style="color: #AA0707;"> 
                <fmt:message key="hthProcesoMonitoreo.formulario.etiqueta.TiempoMonitoreo"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="11" alt="integer" type="text" id="tiempoMonitoreo" name="tiempoMonitoreo"  value="${hthProcesoMonitoreo.tiempoMonitoreo}" size="10" maxlength="5"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('hthProcesoMonitoreo/ayudaPropiedadJson','tiempoMonitoreo')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['tiempoMonitoreo'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['lineaEncabezado']}">	
		<div class="row" >
			<span class="label" style="color: #AA0707;"> 
                <fmt:message key="hthProcesoMonitoreo.formulario.etiqueta.LineaEncabezado"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
	            <select style="width: 160px;" tabindex="12" id="lineaEncabezado" name="lineaEncabezado" class="ui-widget ui-state-default" onchange="seleccionLineaEncabezado();">
		            <option value="S" <c:if test="${hthProcesoMonitoreo.lineaEncabezado == 'S'}">selected</c:if>><fmt:message key="hthProcesoMonitoreo.formulario.comboLabel.Si"/></option>
		            <option value="N" <c:if test="${hthProcesoMonitoreo.lineaEncabezado == 'N'}">selected</c:if>><fmt:message key="hthProcesoMonitoreo.formulario.comboLabel.No"/></option>
			    </select>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('hthProcesoMonitoreo/ayudaPropiedadJson','lineaEncabezado')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['lineaEncabezado'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['idFormatoEncabezado']}">	
		<div id="divIdFormatoEncabezado" class="row" style="display: none;">
			<span class="label" > 
                <fmt:message key="hthProcesoMonitoreo.formulario.etiqueta.IdFormatoEncabezado"/>			</span>	 
			
			<span class="forminput" style="display: inline">
	            <input readonly="readonly" type="text" id="idFormatoEncabezado" name="idFormatoEncabezado" value="${hthProcesoMonitoreo.idFormatoEncabezado}"  size="10"  />
				<input readonly="readonly" type="text" id="hthFormatoEncabezado.nombreFormato" name="hthFormatoEncabezado.nombreFormato" value="${hthProcesoMonitoreo.hthFormatoEncabezado.nombreFormato}"  size="40"  />
				<button tabindex="13" id="btnhthFormatoEncabezado" class="sq" onClick="doForaneo('hthFormatoEncabezado', 'hthProcesoMonitoreo/buscar');"><fmt:message key='globales.formulario.boton.lov'/></button>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('hthProcesoMonitoreo/ayudaPropiedadJson','idFormatoEncabezado')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['idFormatoEncabezado'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['idFormatoDetalle']}">	
		<div class="row" >
			<span class="label" style="color: #AA0707;"> 
                <fmt:message key="hthProcesoMonitoreo.formulario.etiqueta.IdFormatoDetalle"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
	            <input readonly="readonly" type="text" id="idFormatoDetalle" name="idFormatoDetalle" value="${hthProcesoMonitoreo.idFormatoDetalle}"  size="10"  />
				<input readonly="readonly" type="text" id="hthFormato.nombreFormato" name="hthFormato.nombreFormato" value="${hthProcesoMonitoreo.hthFormato.nombreFormato}"  size="40"  />
				<button tabindex="14" id="btnhthFormato" class="sq" onClick="doForaneo('hthFormato', 'hthProcesoMonitoreo/buscar');"><fmt:message key='globales.formulario.boton.lov'/></button>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('hthProcesoMonitoreo/ayudaPropiedadJson','idFormatoDetalle')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['idFormatoDetalle'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['procesamientoSuperusuario']}">	
		<div class="row" >
			<span class="label" style="color: #AA0707;"> 
                <fmt:message key="hthProcesoMonitoreo.formulario.etiqueta.ProcesamientoSuperusuario"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
	            <select style="width: 160px;" tabindex="15" id="procesamientoSuperusuario" name="procesamientoSuperusuario" class="ui-widget ui-state-default">
		            <option value="S" <c:if test="${hthProcesoMonitoreo.procesamientoSuperusuario == 'S'}">selected</c:if>><fmt:message key="hthProcesoMonitoreo.formulario.comboLabel.Si"/></option>
		            <option value="N" <c:if test="${hthProcesoMonitoreo.procesamientoSuperusuario == 'N'}">selected</c:if>><fmt:message key="hthProcesoMonitoreo.formulario.comboLabel.No"/></option>
			    </select>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('hthProcesoMonitoreo/ayudaPropiedadJson','procesamientoSuperusuario')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['procesamientoSuperusuario'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['montoMaximoProcesar']}">	
		<div class="row" >
			<span class="label" > 
                <fmt:message key="hthProcesoMonitoreo.formulario.etiqueta.MontoMaximoProcesar"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="16" alt="numeric" type="text" id="montoMaximoProcesar" name="montoMaximoProcesar"  value="${hthProcesoMonitoreo.montoMaximoProcesar}" size="10" maxlength="19"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('hthProcesoMonitoreo/ayudaPropiedadJson','montoMaximoProcesar')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['montoMaximoProcesar'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	

	<input type="hidden" name="estado" id="estado" value="A" />

<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['emailCliente']}">	
		<div class="row" >
			<span class="label" style="color: #AA0707;"> 
                <fmt:message key="hthProcesoMonitoreo.formulario.etiqueta.EmailCliente"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  		      <textarea tabindex="22" id="emailCliente" name="emailCliente" cols="45" rows="3"><c:out value="${hthProcesoMonitoreo.emailCliente}"/></textarea>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('hthProcesoMonitoreo/ayudaPropiedadJson','emailCliente')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['emailCliente'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['pmHostSftp']}">	
		<div class="row" >
			<span class="label" > 
                <fmt:message key="hthParametroSistema.formulario.etiqueta.HostSftp"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="23" type="text" id="pmHostSftp" name="pmHostSftp"  value="${hthProcesoMonitoreo.pmHostSftp}" size="50"  maxlength="100"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('hthProcesoMonitoreo/ayudaPropiedadJson','pmHostSftp')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['pmHostSftp'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['pmPuertoSftp']}">	
		<div class="row" >
			<span class="label" > 
                <fmt:message key="hthParametroSistema.formulario.etiqueta.PuertoSftp"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="24" type="text" id="pmPuertoSftp" name="pmPuertoSftp"  value="${hthProcesoMonitoreo.pmPuertoSftp}" size="50"  maxlength="100"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('hthProcesoMonitoreo/ayudaPropiedadJson','pmPuertoSftp')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['pmPuertoSftp'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['pmUsuarioSftp']}">	
		<div class="row" >
			<span class="label" > 
                <fmt:message key="hthParametroSistema.formulario.etiqueta.UsuarioSftp"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="25" type="text" id="pmUsuarioSftp" name="pmUsuarioSftp"  value="${hthProcesoMonitoreo.pmUsuarioSftp}" size="50"  maxlength="100"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('hthProcesoMonitoreo/ayudaPropiedadJson','pmUsuarioSftp')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['pmUsuarioSftp'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['pmPasswordSftp']}">	
		<div class="row" >
			<span class="label" > 
                <fmt:message key="hthParametroSistema.formulario.etiqueta.PasswordSftp"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="26" type="text" id="pmPasswordSftp" name="pmPasswordSftp"  value="${hthProcesoMonitoreo.pmPasswordSftp}" size="50"  maxlength="100"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('hthParametroSistema/ayudaPropiedadJson','pmPasswordSftp')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['pmPasswordSftp'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['pmLlaveSftp']}">	
		<div class="row" >
			<span class="label" > 
                <fmt:message key="hthParametroSistema.formulario.etiqueta.LlaveSftp"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  		      <textarea tabindex="27" id="pmLlaveSftp" name="pmLlaveSftp" cols="45" rows="3"><c:out value="${hthProcesoMonitoreo.pmLlaveSftp}"/></textarea>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('hthProcesoMonitoreo/ayudaPropiedadJson','pmLlaveSftp')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['pmLlaveSftp'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>		
</div>		
</div>
</form>

<%@ include file="/WEB-INF/global/jsp/jq-footer.jsp" %>
</fmt:bundle>