<%@ taglib prefix="c"       uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt"     uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:if test="${ not empty locale}">
	<fmt:setLocale value="${locale}" />
</c:if>
<fmt:bundle basename="com.ofbox.davivienda.h2h.resourceBundle.MessagesResources">
	
<c:if test="${ not empty hthTablaDestinoEstado.enlaceDetalle}">
	<c:set var="detalle" value="${hthTablaDestinoEstado.enlaceDetalle}"/>	
	<c:set var="detalleEnllavador" value="${detalle.enllavador}"/>
</c:if>

<c:set var="nombre" value="hthTablaDestino"/>
<c:set var="ubicacion" value="crear"/>
<c:set var="primerCampo" value="nombreTabla"/>
<%@ include file="/WEB-INF/global/jsp/jq-header.jsp" %>

	
<form  name="mainForm" id="mainForm" action="${NewtPageRenderData.actionUrl}" method="post">
<c:if test="${ not empty detalle.cabecera}">
    <jsp:include page="/WEB-INF/jsp/${detalle.cabecera.locacion}/jq/cabecera.jsp" />
</c:if>
<div id="mainContent">
	<div class="breadCrumbHolder module" style="margin-bottom: 3px;">
    <div id="breadcrumb" class="breadCrumb module ui-widget ui-widget-content" style="width: 99%;">
    <ul>
    	<li><a href="${ctx}/inicio/blank/ria"> HOME </a> </li>
    <c:if test="${ not empty ubicacionAplicacion.ruta }">
    	<input type="hidden" name="idUbicacionAplicacionActual" id="idUbicacionAplicacionActual" value="${ ubicacionAplicacion.idUbicacionAplicacionActual }"/>
    	<c:forEach items="${ ubicacionAplicacion.ruta }" var="ub">
    		<li><a href="javascript: doSubmitBreadcrumb('<c:out value="${ ub.submit }"/>', '<c:out value="${ ub.id }"/>')"><c:out value="${ ub.label }" escapeXml="false" /></a>  </li> 
    	</c:forEach>			
    </c:if>
    	<li><fmt:message key="hthTablaDestino.bc.etiqueta.crear" /></li>
    </ul>
    </div>
    	<input type="hidden" name="id_ubicacion_retorno" id="id_ubicacion_retorno" value=""/>
    </div>	
	
	
<%-- <div id="iTitle" class="ui-state-default ui-widget-content ui-corner-all">
	<fmt:message key="hthTablaDestino.formulario.etiqueta.crear.titulo"/>
</div> --%>
	<input type="hidden" id="pageLoc" name="pageLoc" value="crear"/>
	<input type="hidden" id="primerCampo" name="primerCampo" value="nombreTabla"/>
    <input type="hidden" id="eag" name="eag" value="${eag}"/>
    <input type="hidden" id="movimientoPagina" name="movimientoPagina"/>
    <input type="hidden" id="columna" name="columna"/>
    <input type="hidden" id="fenixf31seguridad" name="fenixf31seguridad" value="<c:out value='${fenixf31seguridad}' />" />
	<c:if test="${not empty scrollTo }">
    <input type="hidden" id="scrollSmooth" value="<c:out value='${scrollTo}' />" />
    </c:if>
	<!-- valores de llaves -->
	
	


<!-- mensajes de error-->
<c:if test="${mensajesUsuario.erroresGlobales}">
<div class="ui-state-error ui-corner-all" style="padding: 0 .7em; font-size: 0.8em;"> 
	<span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em; margin-top: .3em;"></span> 
	<strong><fmt:message key="globales.formulario.etiqueta.error"/></strong> <br /> 
	<ul>
		<c:forEach items="${mensajesUsuario.mensajesDeError}" var="mensaje">
        	<li><c:out value="${mensaje}" escapeXml="false"/></li>
     	</c:forEach>
	</ul>
</div>
</c:if>

<div id="action-buttons">
<div id="action-bar">
    <button tabindex="3" id="btnGuardar" type="submit" name="ok" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('hthTablaDestino/crearEjecutar');"><fmt:message key="globales.formulario.boton.crear.guardar"/></button>
	<button tabindex="4" id="btnCancelar" type="button" name="cancel" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('hthTablaDestino/cancelarCrear');"><fmt:message key="globales.formulario.boton.crear.cancelar"/></button>
</div>
</div>
<br />

<!-- datos de formulario -->
<div><span style="font-size: small;">-Los campos marcados con * son obligatorios</span></div>
<div class="ui-widget ui-widget-content ui-corner-all" style="width: 99%">
		<div id="formInputHeader" class="ui-widget ui-widget-header ui-corner-top" style="padding-left: 5px;"> 
			<fmt:message key="hthTablaDestino.formulario.etiqueta.crear.titulo"/> 
		</div>
		<div style="display: inline;" id="formInput">
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['nombreTabla']}">	
		<div class="row" >
			<span class="label" style="color: #AA0707;"> 
                <fmt:message key="hthTablaDestino.formulario.etiqueta.NombreTabla"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="1" type="text" id="nombreTabla" name="nombreTabla"  value="${hthTablaDestino.nombreTabla}" size="50"  maxlength="64"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('hthTablaDestino/ayudaPropiedadJson','nombreTabla')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['nombreTabla'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['descripcion']}">	
		<div class="row" >
			<span class="label" > 
                <fmt:message key="hthTablaDestino.formulario.etiqueta.Descripcion"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  		      <textarea tabindex="2" id="descripcion" name="descripcion" cols="45" rows="3"><c:out value="${hthTablaDestino.descripcion}"/></textarea>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('hthTablaDestino/ayudaPropiedadJson','descripcion')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['descripcion'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
		</div>
</div>		
</div>
</form>

<%@ include file="/WEB-INF/global/jsp/jq-footer.jsp" %>
</fmt:bundle>
