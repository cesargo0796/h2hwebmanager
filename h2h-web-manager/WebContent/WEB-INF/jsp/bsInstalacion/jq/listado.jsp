<%@ taglib prefix="c"       uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt"     uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:if test="${ not empty locale}">
	<fmt:setLocale value="${ locale }" />
</c:if>
<fmt:bundle basename="com.ofbox.davivienda.h2h.resourceBundle.MessagesResources">

<c:if test="${ not empty bsInstalacionEstado.enlaceBusqueda.enllavador}">
	<c:set var="busquedaEnllavador" value="${bsInstalacionEstado.enlaceBusqueda.enllavador}"/>	
</c:if>	
<c:if test="${ not empty bsInstalacionEstado.enlaceDetalle}">
	<c:set var="detalle" value="${bsInstalacionEstado.enlaceDetalle}"/>	
</c:if>	
<c:if test="${bsInstalacionEstado.ordenamientoAplicado}">
	<c:set var="orden" value="${bsInstalacionEstado.ordenamiento}"/>	
</c:if>		
<c:if test="${not empty bsInstalacionSeg}">
	<c:set var="restriccion" value="${bsInstalacionSeg}"/>
</c:if>

<c:set var="nombre" value="bsInstalacion"/>
<c:set var="ubicacion" value="listado"/>

<% int counter = 0; %>
<%@ include file="/WEB-INF/global/jsp/jq-header.jsp" %>
<!-- Elementos no visibles -->	
	
<form  name="mainForm" id="mainForm" action="${NewtPageRenderData.actionUrl}" method="post">

<div id="mainContent">

<div class="breadCrumbHolder module" style="margin-bottom: 3px;">
<div id="breadcrumb" class="breadCrumb module ui-widget ui-widget-content" style="width: 99%;">
<ul>
	<li><a href="${ctx}/inicio/blank/ria"> HOME </a> </li>
<c:if test="${ not empty ubicacionAplicacion.ruta }">
	<input type="hidden" name="idUbicacionAplicacionActual" id="idUbicacionAplicacionActual" value="${ ubicacionAplicacion.idUbicacionAplicacionActual }"/>
	<c:forEach items="${ ubicacionAplicacion.ruta }" var="ub">
		<li><a href="javascript: doSubmitBreadcrumb('<c:out value="${ ub.submit }"/>', '<c:out value="${ ub.id }"/>')"><c:out value="${ ub.label }" escapeXml="false" /></a>  </li> 
	</c:forEach>			
</c:if>
	<li><fmt:message key="bsInstalacion.bc.etiqueta.principal" /></li>
</ul>
</div>
	<input type="hidden" name="id_ubicacion_retorno" id="id_ubicacion_retorno" value=""/>
</div>

<c:if test="${ not empty detalle.cabecera}">
    <jsp:include page="/WEB-INF/jsp/${detalle.cabecera.locacion}/jq/cabecera.jsp" />
</c:if>

<%-- <div id="iTitle" class="ui-state-default ui-widget-content ui-corner-all">
	<fmt:message key="bsInstalacion.listado.etiqueta.titulo"/>
</div>--%>
	<input type="hidden" id="maxRowNum" name="maxRowNum" value="<c:out value="${navegacion.registroFinal - 1}"/>">
	<input type="hidden" id="pageLoc" name="pageLoc" value="listado"/>
    <input type="hidden" id="eag" name="eag" value="${eag}"/>
	<c:if test="${ not empty busquedaEnllavador}">
	<input type="hidden" id="listadoLov" value="bsInstalacion"/>
	</c:if>
	<c:if test="${ not empty esAsync}">
	<input type="hidden" id="esAsync" value="${esAsync}"/>
	</c:if>
    <input type="hidden" id="movimientoPagina" name="movimientoPagina"/>
    <input type="hidden" id="columna" name="columna"/>
    <input type="hidden" id="fenixf31seguridad" name="fenixf31seguridad" value="<c:out value='${fenixf31seguridad}' />" />
	<!-- valores de llaves -->
    <input type="hidden" alt="notForSubmit" id="instalacion" name="instalacion" value=""/>
	 <input type="hidden" id="expo" name="expo" value=""/>  



<%@ include file="/WEB-INF/global/jsp/mensajes-globales.jsp" %>

<c:if test="${ not empty busquedaEnllavador}">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em; font-size: 85%;"> 
	<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
 	<strong><fmt:message key="globales.listado.etiqueta.tipLov.titulo"/></strong>&nbsp;<fmt:message key="globales.listado.etiqueta.tipLov.mensaje"/>
	</p>
</div>
</c:if>
<!-- botones de acciones basicas -->
<div id="action-buttons">
<div id="action-bar">
<c:if test="${ empty busquedaEnllavador}">
	<c:if test="${restriccion.verDisponible}">
	<button id="btnVer" type="button" class="ui-button ui-corner-all ui-button-color" name="ver" onClick="if(isValueSelected()){ doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsInstalacion/ver'); }"><fmt:message key="globales.listado.boton.ver"/></button>
	</c:if>
	<c:if test="${restriccion.crearDisponible}">
	<button id="btnCrear" type="button" class="ui-button ui-corner-all ui-button-color" name="crear" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsInstalacion/crear');"><fmt:message key="globales.listado.boton.crear"/></button>
	</c:if>
	<c:if test="${restriccion.actualizarDisponible}">	
	<button id="btnActualizar" type="button" class="ui-button ui-corner-all ui-button-color" name="actualizar" onClick="if(isValueSelected()){ doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsInstalacion/actualizar'); }"><fmt:message key="globales.listado.boton.actualizar"/></button>
	</c:if>
	<c:if test="${restriccion.eliminarDisponible}">
	<button id="btnEliminar" type="button" class="ui-button ui-corner-all ui-button-color" name="eliminar" onClick="if(isValueSelected()){$('#dialog-confirm').dialog('open');}"><fmt:message key="globales.listado.boton.eliminar"/></button>
	</c:if>
	<button id="btnDetalle" type="button" class="ui-button ui-corner-all ui-button-color" name="detalle" onClick="if(isValueSelected()){ doSubmit('bsInstalacion/detalle'); }"><fmt:message key="globales.listado.boton.detalle"/></button>
	</c:if>
<c:if test="${ not empty busquedaEnllavador}">
	<c:if test="${restriccion.crearDisponible}">
	<button id="btnCrear" type="button" class="ui-button ui-corner-all ui-button-color" name="crear" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsInstalacion/crear');"><fmt:message key="globales.listado.boton.crear"/></button>
	</c:if>
	<button id="btnSeleccionar" type="button" class="ui-button ui-corner-all ui-button-color" name="seleccionar" onClick="if(isValueSelected()){ doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsInstalacion/buscarSeleccionar'); }"><fmt:message key="globales.listado.boton.seleccionar"/></button>
	<button id="btnCancelar" type="button" class="ui-button ui-corner-all ui-button-color" name="cancel" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsInstalacion/cancelarBuscar');"><fmt:message key="globales.listado.boton.cancelar"/></button>
</c:if>
	<c:if test="${restriccion.filtrarDisponible}">	
	<button id="btnFiltrar" type="button" class="ui-button ui-corner-all ui-button-color" name="filter" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsInstalacion/filtrar');"><fmt:message key="globales.listado.boton.filtrar"/></button>
	</c:if>	
	
</div>
<div id="navigation-bar">	
<c:if test="${navegacion.paginaAnterior}">				
	<button id="btnAnterior" type="button" name="anterior" onClick="$('#movimientoPagina').val('anterior');doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsInstalacion/listado')"><fmt:message key="globales.listado.boton.anterior"/></button>
</c:if>
<c:if test="${!navegacion.paginaAnterior}">			
	<button id="btnAnterior" type="button" name="anterior" disabled="disabled"><fmt:message key="globales.listado.boton.anterior"/></button>
</c:if>
<c:if test="${not empty navegacion.listadoPaginas}">
<c:forEach items="${navegacion.listadoPaginas}" var="p">
	<c:if test="${p == navegacion.noPaginaActual}">
	<button id="pag<c:out value="${p}" />" class="ui-button-text ui-state-highlight" type="button" name="pag<c:out value="${p}" />" disabled="disabled"><c:out value="${p}" /></button>
	</c:if>
	<c:if test="${p != navegacion.noPaginaActual}">
	<button id="pag<c:out value="${p}" />"  onClick="$('#movimientoPagina').val('${p}');doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsInstalacion/listado')" type="button" name="pag<c:out value="${p}"/>"><c:out value="${p}" /></button>	
	</c:if> 
</c:forEach>
</c:if>
<c:if test="${navegacion.paginaSiguiente}">			
	<button id="btnSiguiente" type="button" name="siguiente" onClick="$('#movimientoPagina').val('siguiente');doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsInstalacion/listado')" ><fmt:message key="globales.listado.boton.siguiente"/></button>
</c:if>
<c:if test="${!navegacion.paginaSiguiente}">			
	<button id="btnSiguiente" type="button" name="siguiente" disabled="disabled"><fmt:message key="globales.listado.boton.siguiente"/></button>
</c:if>
	
</div>
</div>
<br/>

<div class="ui-widget fg-toolbar ui-toolbar ui-widget-header ui-corner-tl ui-corner-tr ui-helper-clearfix">

<c:if test="${ not empty orden}">
	<div style="display:block; float: left;">
		<button id="btnRemoverOrden" type="button" class="ui-button ui-corner-all ui-button-color" name="cancel" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsInstalacion/removerOrdenamiento');"><fmt:message key="globales.listado.boton.removerOrden"/></button>
	</div>
</c:if>
<c:if test="${navegacion.filtroAplicado}">
	<div style="display:block; float: left;">
		<button id="btnRemoverFiltro" type="button" class="ui-button ui-corner-all ui-button-color" name="nofilter" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsInstalacion/removerFiltrar');"><fmt:message key="globales.listado.boton.removerFiltro"/></button>
	</div>			
</c:if>
	<div id="crudTable_filter" style="display: block; text-align: right; float: right; margin-top: 6px; margin-bottom: 6px;"> 
		<fmt:message key="globales.listado.etiqueta.busquedaRapida"/>
		<select id="campoBusqueda" name="campoBusqueda" onchange="doBusquedaRapida('#busqueda', this.value, $('option:selected', this));" class="ui-widget ui-widget-content ui-state-default">
			<option value="cliente"><fmt:message key="bsInstalacion.listado.etiqueta.Cliente"/></option>
			<option value="modulos"><fmt:message key="bsInstalacion.listado.etiqueta.Modulos"/></option>
			<option value="nombre"><fmt:message key="bsInstalacion.listado.etiqueta.Nombre"/></option>
			<option value="id"><fmt:message key="bsInstalacion.listado.etiqueta.Id"/></option>
			<option value="password"><fmt:message key="bsInstalacion.listado.etiqueta.Password"/></option>
			<option value="pesoa"><fmt:message key="bsInstalacion.listado.etiqueta.Pesoa"/></option>
			<option value="pesob"><fmt:message key="bsInstalacion.listado.etiqueta.Pesob"/></option>
			<option value="pesoc"><fmt:message key="bsInstalacion.listado.etiqueta.Pesoc"/></option>
			<option value="pesod"><fmt:message key="bsInstalacion.listado.etiqueta.Pesod"/></option>
			<option value="pesoe"><fmt:message key="bsInstalacion.listado.etiqueta.Pesoe"/></option>
			<option value="estatuscom"><fmt:message key="bsInstalacion.listado.etiqueta.Estatuscom"/></option>
			<option value="estatus"><fmt:message key="bsInstalacion.listado.etiqueta.Estatus"/></option>
			<option value="envioCliente"><fmt:message key="bsInstalacion.listado.etiqueta.EnvioCliente"/></option>
			<option value="version"><fmt:message key="bsInstalacion.listado.etiqueta.Version"/></option>
			<option value="versionpe"><fmt:message key="bsInstalacion.listado.etiqueta.Versionpe"/></option>
			<option value="origenlotes"><fmt:message key="bsInstalacion.listado.etiqueta.Origenlotes"/></option>
			<option value="siguientelotecc"><fmt:message key="bsInstalacion.listado.etiqueta.Siguientelotecc"/></option>
			<option value="siguientelotepe"><fmt:message key="bsInstalacion.listado.etiqueta.Siguientelotepe"/></option>
			<option value="siguientelotepp"><fmt:message key="bsInstalacion.listado.etiqueta.Siguientelotepp"/></option>
			<option value="siguienteloteps"><fmt:message key="bsInstalacion.listado.etiqueta.Siguienteloteps"/></option>
			<option value="siguienteloteach"><fmt:message key="bsInstalacion.listado.etiqueta.Siguienteloteach"/></option>
		</select> &nbsp;
		<input onkeypress="doFiltrarPor('bsInstalacion', event)" type="text" id="busqueda" name="busqueda" class="ui-widget ui-widget-content " style="width:150px; height: 18px;"/>
	</div>
</div>
<c:if test="${restriccion.listadoDisponible or not empty busquedaEnllavador}">
<table id="crudTable" width="100%">
	<thead>
		<tr>
  			<th onclick="doSort('cliente','${nombre}')" sort="${orden.mapaDeValores['cliente'].strOrden}"><fmt:message key="bsInstalacion.listado.etiqueta.Cliente"/></th>
  			<th onclick="doSort('modulos','${nombre}')" sort="${orden.mapaDeValores['modulos'].strOrden}"><fmt:message key="bsInstalacion.listado.etiqueta.Modulos"/></th>
  			<th onclick="doSort('nombre','${nombre}')" sort="${orden.mapaDeValores['nombre'].strOrden}"><fmt:message key="bsInstalacion.listado.etiqueta.Nombre"/></th>
  			<th onclick="doSort('id','${nombre}')" sort="${orden.mapaDeValores['id'].strOrden}"><fmt:message key="bsInstalacion.listado.etiqueta.Id"/></th>
  			<th onclick="doSort('password','${nombre}')" sort="${orden.mapaDeValores['password'].strOrden}"><fmt:message key="bsInstalacion.listado.etiqueta.Password"/></th>
  			<th onclick="doSort('pesoa','${nombre}')" sort="${orden.mapaDeValores['pesoa'].strOrden}"><fmt:message key="bsInstalacion.listado.etiqueta.Pesoa"/></th>
  			<th onclick="doSort('pesob','${nombre}')" sort="${orden.mapaDeValores['pesob'].strOrden}"><fmt:message key="bsInstalacion.listado.etiqueta.Pesob"/></th>
  			<th onclick="doSort('pesoc','${nombre}')" sort="${orden.mapaDeValores['pesoc'].strOrden}"><fmt:message key="bsInstalacion.listado.etiqueta.Pesoc"/></th>
  			<th onclick="doSort('pesod','${nombre}')" sort="${orden.mapaDeValores['pesod'].strOrden}"><fmt:message key="bsInstalacion.listado.etiqueta.Pesod"/></th>
  			<th onclick="doSort('pesoe','${nombre}')" sort="${orden.mapaDeValores['pesoe'].strOrden}"><fmt:message key="bsInstalacion.listado.etiqueta.Pesoe"/></th>
  			<th onclick="doSort('estatuscom','${nombre}')" sort="${orden.mapaDeValores['estatuscom'].strOrden}"><fmt:message key="bsInstalacion.listado.etiqueta.Estatuscom"/></th>
  			<th onclick="doSort('fechaestatus','${nombre}')" sort="${orden.mapaDeValores['fechaestatus'].strOrden}"><fmt:message key="bsInstalacion.listado.etiqueta.Fechaestatus"/></th>
  			<th onclick="doSort('estatus','${nombre}')" sort="${orden.mapaDeValores['estatus'].strOrden}"><fmt:message key="bsInstalacion.listado.etiqueta.Estatus"/></th>
  			<th onclick="doSort('envioCliente','${nombre}')" sort="${orden.mapaDeValores['envioCliente'].strOrden}"><fmt:message key="bsInstalacion.listado.etiqueta.EnvioCliente"/></th>
  			<th onclick="doSort('version','${nombre}')" sort="${orden.mapaDeValores['version'].strOrden}"><fmt:message key="bsInstalacion.listado.etiqueta.Version"/></th>
  			<th onclick="doSort('versionpe','${nombre}')" sort="${orden.mapaDeValores['versionpe'].strOrden}"><fmt:message key="bsInstalacion.listado.etiqueta.Versionpe"/></th>
  			<th onclick="doSort('origenlotes','${nombre}')" sort="${orden.mapaDeValores['origenlotes'].strOrden}"><fmt:message key="bsInstalacion.listado.etiqueta.Origenlotes"/></th>
  			<th onclick="doSort('siguientelotecc','${nombre}')" sort="${orden.mapaDeValores['siguientelotecc'].strOrden}"><fmt:message key="bsInstalacion.listado.etiqueta.Siguientelotecc"/></th>
  			<th onclick="doSort('siguientelotepe','${nombre}')" sort="${orden.mapaDeValores['siguientelotepe'].strOrden}"><fmt:message key="bsInstalacion.listado.etiqueta.Siguientelotepe"/></th>
  			<th onclick="doSort('siguientelotepp','${nombre}')" sort="${orden.mapaDeValores['siguientelotepp'].strOrden}"><fmt:message key="bsInstalacion.listado.etiqueta.Siguientelotepp"/></th>
  			<th onclick="doSort('siguienteloteps','${nombre}')" sort="${orden.mapaDeValores['siguienteloteps'].strOrden}"><fmt:message key="bsInstalacion.listado.etiqueta.Siguienteloteps"/></th>
  			<th onclick="doSort('siguienteloteach','${nombre}')" sort="${orden.mapaDeValores['siguienteloteach'].strOrden}"><fmt:message key="bsInstalacion.listado.etiqueta.Siguienteloteach"/></th>
		</tr>
	</thead>	
	<tbody>
<c:forEach items="${coleccionResultados}" var="dto">
		<tr onclick="$('#instalacion').val('${dto.instalacion}'); selectDeSelectObject(this, 'ui-state-active'); sel = true; currentRow = $(this).parent().children().index($(this));">  
		<td><c:out value="${dto.cliente}"/></td>
		<td><c:out value="${dto.modulos}"/></td>
		<td><c:out value="${dto.nombre}"/></td>
		<td><c:out value="${dto.id}"/></td>
		<td><c:out value="${dto.password}"/></td>
		<td><c:out value="${dto.pesoa}"/></td>
		<td><c:out value="${dto.pesob}"/></td>
		<td><c:out value="${dto.pesoc}"/></td>
		<td><c:out value="${dto.pesod}"/></td>
		<td><c:out value="${dto.pesoe}"/></td>
		<td><c:out value="${dto.estatuscom}"/></td>
		<td><fmt:formatDate pattern="dd-MM-yyyy" value="${dto.fechaestatus}" /></td>
		<td><c:out value="${dto.estatus}"/></td>
		<td><c:out value="${dto.envioCliente}"/></td>
		<td><c:out value="${dto.version}"/></td>
		<td><c:out value="${dto.versionpe}"/></td>
		<td><c:out value="${dto.origenlotes}"/></td>
		<td><c:out value="${dto.siguientelotecc}"/></td>
		<td><c:out value="${dto.siguientelotepe}"/></td>
		<td><c:out value="${dto.siguientelotepp}"/></td>
		<td><c:out value="${dto.siguienteloteps}"/></td>
		<td><c:out value="${dto.siguienteloteach}"/></td>
		</tr>
</c:forEach>
	</tbody>
</table>
</c:if>
<div class="ui-widget fg-toolbar ui-toolbar ui-widget-header ui-corner-bl ui-corner-br ui-helper-clearfix">
	<c:if test="${navegacion.totalRegistros <= 0}">
		<fmt:message key="globales.listado.etiqueta.sinRegistros" />
    </c:if>
    <c:if test="${navegacion.totalRegistros > 0}">
    	<fmt:message key="globales.listado.etiqueta.numRegistros">
    		<fmt:param><c:out value="${navegacion.registroInicial}"/></fmt:param>
    		<fmt:param><c:out value="${navegacion.registroFinal}"/></fmt:param>
    		<fmt:param><c:out value="${navegacion.totalRegistros}"/></fmt:param>
    	</fmt:message>
    </c:if>	
</div>
<br/>
</div>
</form>
<%@ include file="/WEB-INF/global/jsp/jq-footer.jsp" %>
</fmt:bundle>