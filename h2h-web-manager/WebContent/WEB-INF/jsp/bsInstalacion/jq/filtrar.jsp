<%@ taglib prefix="c"       uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt"     uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:if test="${ not empty locale}">
	<fmt:setLocale value="${ locale }" />
</c:if>
<fmt:bundle basename="com.ofbox.davivienda.h2h.resourceBundle.MessagesResources">
<c:if test="${ not empty bsInstalacionEstado.enlaceDetalle}">
	<c:set var="detalle" value="${bsInstalacionEstado.enlaceDetalle}"/>	
	<c:set var="detalleEnllavador" value="${detalle.enllavador}"/>
</c:if>	

<c:set var="nombre" value="bsInstalacion"/>
<c:set var="ubicacion" value="filtrar"/>
<c:set var="primerCampo" value="clienteMin"/>
<%@ include file="/WEB-INF/global/jsp/jq-header.jsp" %>

<form  name="mainForm" id="mainForm" action="${NewtPageRenderData.actionUrl}" method="post">
<c:if test="${ not empty detalle.cabecera}">
    <jsp:include page="/WEB-INF/jsp/${detalle.cabecera.locacion}/jq/cabecera.jsp" />
</c:if>	
<div id="mainContent">
	<div class="breadCrumbHolder module" style="margin-bottom: 3px;">
    <div id="breadcrumb" class="breadCrumb module ui-widget ui-widget-content" style="width: 99%;">
    <ul>
    	<li><a href="${ctx}/inicio/blank/ria"> HOME </a> </li>
    <c:if test="${ not empty ubicacionAplicacion.ruta }">
    	<input type="hidden" name="idUbicacionAplicacionActual" id="idUbicacionAplicacionActual" value="${ ubicacionAplicacion.idUbicacionAplicacionActual }"/>
    	<c:forEach items="${ ubicacionAplicacion.ruta }" var="ub">
    		<li><a href="javascript: doSubmitBreadcrumb('<c:out value="${ ub.submit }"/>', '<c:out value="${ ub.id }"/>')"><c:out value="${ ub.label }" escapeXml="false" /></a>  </li> 
    	</c:forEach>			
    </c:if>
    	<li><fmt:message key="bsInstalacion.bc.etiqueta.filtrar" /></li>
    </ul>
    </div>
    	<input type="hidden" name="id_ubicacion_retorno" id="id_ubicacion_retorno" value=""/>
    </div>
	
<%--<div id="iTitle" class="ui-state-default ui-widget-content ui-corner-all">
	<fmt:message key="bsInstalacion.formulario.etiqueta.filtrar.titulo"/>
</div>--%>
<input type="hidden" id="pageLoc" name="pageLoc" value="filtrar"/>
	<input type="hidden" id="primerCampo" name="primerCampo" value="clienteMin"/>
    <input type="hidden" id="eag" name="eag" value="${eag}"/>
    <input type="hidden" id="movimientoPagina" name="movimientoPagina"/>
    <input type="hidden" id="columna" name="columna"/>
    <input type="hidden" id="fenixf31seguridad" name="fenixf31seguridad" value="<c:out value='${fenixf31seguridad}' />" />
	<c:if test="${not empty scrollTo }">
    <input type="hidden" id="scrollSmooth" value="<c:out value='${scrollTo}' />" />
    </c:if>
	<!-- valores de llaves -->


<!-- mensajes de error-->
<c:if test="${mensajesUsuario.erroresGlobales}">
<div id="iError">
	<b class="ui-round-top"> 
	<b class="ui-line-one"></b><b class="ui-line-two"></b><b class="ui-line-three"></b><b class="ui-line-four"></b> 
    </b> 
  	<table>
     	<tr>
       		<td>
         	<ol>
	     	<c:forEach items="${mensajesUsuario.mensajesDeError}" var="mensaje">
	        	<li><c:out value="${mensaje}" escapeXml="false"/></li>
	     	</c:forEach>
	     	</ol>
       		</td>
     	</tr>
  	</table>
  	<b class="ui-round-bottom"> 
	<b class="ui-line-four"></b><b class="ui-line-three"></b><b class="ui-line-two"></b><b class="ui-line-one"></b> 
  	</b> 
</div>
</c:if>

<div id="action-buttons">
<div id="action-bar">
	<button tabindex="45" id="btnAplicarFiltro" type="submit" name="Filtrar" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsInstalacion/filtrarEjecutar');"><fmt:message key="globales.filtro.boton.aplicarFiltro"/></button>
	<button tabindex="46" id="btnLimpiar" type="button" name="Limpiar" onClick="limpiarCampos('mainForm');"><fmt:message key="globales.filtro.boton.limpiar"/></button>
	<button tabindex="47" id="btnCancelar" type="button" name="Cancelar" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsInstalacion/cancelarFiltrar');"><fmt:message key="globales.filtro.boton.cancelar"/></button>
</div>
</div>
<br />

<!-- datos de formulario -->
<div class="ui-widget ui-widget-content ui-corner-all" style="width: 99%">
		<div id="formInputHeader" class="ui-widget ui-widget-header ui-corner-top" style="padding-left: 5px;"> 
			<fmt:message key="bsInstalacion.formulario.etiqueta.filtrar.titulo"/> 
		</div>
		<div style="display: inline;" id="formInput">
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['cliente']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsInstalacion.filtro.etiqueta.Cliente"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="2" alt="integerMin" type="text" id="clienteMin" name="clienteMin" value="${bsInstalacionFiltro['clienteMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="2" alt="integerMax" type="text" id="clienteMax" name="clienteMax" value="${bsInstalacionFiltro['clienteMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['cliente'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['modulos']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsInstalacion.filtro.etiqueta.Modulos"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="3" alt="integerMin" type="text" id="modulosMin" name="modulosMin" value="${bsInstalacionFiltro['modulosMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="3" alt="integerMax" type="text" id="modulosMax" name="modulosMax" value="${bsInstalacionFiltro['modulosMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['modulos'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['nombre']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsInstalacion.filtro.etiqueta.Nombre"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="4" type="text" id="nombre" name="nombre" value="${bsInstalacionFiltro['nombre']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['nombre'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['id']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsInstalacion.filtro.etiqueta.Id"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="5" type="text" id="id" name="id" value="${bsInstalacionFiltro['id']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['id'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['password']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsInstalacion.filtro.etiqueta.Password"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="6" type="text" id="password" name="password" value="${bsInstalacionFiltro['password']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['password'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['pesoa']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsInstalacion.filtro.etiqueta.Pesoa"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="7" alt="integerMin" type="text" id="pesoaMin" name="pesoaMin" value="${bsInstalacionFiltro['pesoaMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="7" alt="integerMax" type="text" id="pesoaMax" name="pesoaMax" value="${bsInstalacionFiltro['pesoaMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['pesoa'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['pesob']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsInstalacion.filtro.etiqueta.Pesob"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="8" alt="integerMin" type="text" id="pesobMin" name="pesobMin" value="${bsInstalacionFiltro['pesobMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="8" alt="integerMax" type="text" id="pesobMax" name="pesobMax" value="${bsInstalacionFiltro['pesobMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['pesob'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['pesoc']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsInstalacion.filtro.etiqueta.Pesoc"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="9" alt="integerMin" type="text" id="pesocMin" name="pesocMin" value="${bsInstalacionFiltro['pesocMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="9" alt="integerMax" type="text" id="pesocMax" name="pesocMax" value="${bsInstalacionFiltro['pesocMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['pesoc'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['pesod']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsInstalacion.filtro.etiqueta.Pesod"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="10" alt="integerMin" type="text" id="pesodMin" name="pesodMin" value="${bsInstalacionFiltro['pesodMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="10" alt="integerMax" type="text" id="pesodMax" name="pesodMax" value="${bsInstalacionFiltro['pesodMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['pesod'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['pesoe']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsInstalacion.filtro.etiqueta.Pesoe"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="11" alt="integerMin" type="text" id="pesoeMin" name="pesoeMin" value="${bsInstalacionFiltro['pesoeMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="11" alt="integerMax" type="text" id="pesoeMax" name="pesoeMax" value="${bsInstalacionFiltro['pesoeMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['pesoe'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['estatuscom']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsInstalacion.filtro.etiqueta.Estatuscom"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="12" alt="integerMin" type="text" id="estatuscomMin" name="estatuscomMin" value="${bsInstalacionFiltro['estatuscomMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="12" alt="integerMax" type="text" id="estatuscomMax" name="estatuscomMax" value="${bsInstalacionFiltro['estatuscomMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['estatuscom'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['fechaestatus']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsInstalacion.filtro.etiqueta.Fechaestatus"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="13" type="text" cdate="fechaestatusMax" cattr="minDate" id="fechaestatusMin" name="fechaestatusMin" value="<fmt:formatDate pattern="dd-MM-yyyy" value="${bsInstalacionFiltro['fechaestatusMin']}"/>"/>
				&nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
				<input tabindex="13" type="text" cdate="fechaestatusMin" cattr="maxDate" id="fechaestatusMax" name="fechaestatusMax" value="<fmt:formatDate pattern="dd-MM-yyyy" value="${bsInstalacionFiltro['fechaestatusMax']}"/>"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['fechaestatus'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['estatus']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsInstalacion.filtro.etiqueta.Estatus"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="14" alt="integerMin" type="text" id="estatusMin" name="estatusMin" value="${bsInstalacionFiltro['estatusMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="14" alt="integerMax" type="text" id="estatusMax" name="estatusMax" value="${bsInstalacionFiltro['estatusMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['estatus'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['envioCliente']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsInstalacion.filtro.etiqueta.EnvioCliente"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="15" alt="integerMin" type="text" id="envioClienteMin" name="envioClienteMin" value="${bsInstalacionFiltro['envioClienteMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="15" alt="integerMax" type="text" id="envioClienteMax" name="envioClienteMax" value="${bsInstalacionFiltro['envioClienteMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['envioCliente'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['version']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsInstalacion.filtro.etiqueta.Version"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="16" type="text" id="version" name="version" value="${bsInstalacionFiltro['version']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['version'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['versionpe']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsInstalacion.filtro.etiqueta.Versionpe"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="17" type="text" id="versionpe" name="versionpe" value="${bsInstalacionFiltro['versionpe']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['versionpe'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['origenlotes']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsInstalacion.filtro.etiqueta.Origenlotes"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="18" alt="integerMin" type="text" id="origenlotesMin" name="origenlotesMin" value="${bsInstalacionFiltro['origenlotesMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="18" alt="integerMax" type="text" id="origenlotesMax" name="origenlotesMax" value="${bsInstalacionFiltro['origenlotesMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['origenlotes'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['siguientelotecc']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsInstalacion.filtro.etiqueta.Siguientelotecc"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="19" alt="integerMin" type="text" id="siguienteloteccMin" name="siguienteloteccMin" value="${bsInstalacionFiltro['siguienteloteccMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="19" alt="integerMax" type="text" id="siguienteloteccMax" name="siguienteloteccMax" value="${bsInstalacionFiltro['siguienteloteccMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['siguientelotecc'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['siguientelotepe']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsInstalacion.filtro.etiqueta.Siguientelotepe"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="20" alt="integerMin" type="text" id="siguientelotepeMin" name="siguientelotepeMin" value="${bsInstalacionFiltro['siguientelotepeMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="20" alt="integerMax" type="text" id="siguientelotepeMax" name="siguientelotepeMax" value="${bsInstalacionFiltro['siguientelotepeMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['siguientelotepe'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['siguientelotepp']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsInstalacion.filtro.etiqueta.Siguientelotepp"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="21" alt="integerMin" type="text" id="siguienteloteppMin" name="siguienteloteppMin" value="${bsInstalacionFiltro['siguienteloteppMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="21" alt="integerMax" type="text" id="siguienteloteppMax" name="siguienteloteppMax" value="${bsInstalacionFiltro['siguienteloteppMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['siguientelotepp'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['siguienteloteps']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsInstalacion.filtro.etiqueta.Siguienteloteps"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="22" alt="integerMin" type="text" id="siguientelotepsMin" name="siguientelotepsMin" value="${bsInstalacionFiltro['siguientelotepsMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="22" alt="integerMax" type="text" id="siguientelotepsMax" name="siguientelotepsMax" value="${bsInstalacionFiltro['siguientelotepsMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['siguienteloteps'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['siguienteloteach']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsInstalacion.filtro.etiqueta.Siguienteloteach"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="23" alt="integerMin" type="text" id="siguienteloteachMin" name="siguienteloteachMin" value="${bsInstalacionFiltro['siguienteloteachMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="23" alt="integerMax" type="text" id="siguienteloteachMax" name="siguienteloteachMax" value="${bsInstalacionFiltro['siguienteloteachMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['siguienteloteach'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
</div>	


</div>
</div>
</form>

<%@ include file="/WEB-INF/global/jsp/jq-footer.jsp" %>
</fmt:bundle>
</body>