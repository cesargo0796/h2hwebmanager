<%@ taglib prefix="c"       uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt"     uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:if test="${ not empty locale}">
	<fmt:setLocale value="${locale}" />
</c:if>
<fmt:bundle basename="com.ofbox.davivienda.h2h.resourceBundle.MessagesResources">
	
<c:if test="${ not empty bsInstalacionEstado.enlaceDetalle}">
	<c:set var="detalle" value="${bsInstalacionEstado.enlaceDetalle}"/>	
	<c:set var="detalleEnllavador" value="${detalle.enllavador}"/>
</c:if>

<c:set var="nombre" value="bsInstalacion"/>
<c:set var="ubicacion" value="crear"/>
<c:set var="primerCampo" value="cliente"/>
<%@ include file="/WEB-INF/global/jsp/jq-header.jsp" %>

	
<form  name="mainForm" id="mainForm" action="${NewtPageRenderData.actionUrl}" method="post">
<c:if test="${ not empty detalle.cabecera}">
    <jsp:include page="/WEB-INF/jsp/${detalle.cabecera.locacion}/jq/cabecera.jsp" />
</c:if>
<div id="mainContent">
	<div class="breadCrumbHolder module" style="margin-bottom: 3px;">
    <div id="breadcrumb" class="breadCrumb module ui-widget ui-widget-content" style="width: 99%;">
    <ul>
    	<li><a href="${ctx}/inicio/blank/ria"> HOME </a> </li>
    <c:if test="${ not empty ubicacionAplicacion.ruta }">
    	<input type="hidden" name="idUbicacionAplicacionActual" id="idUbicacionAplicacionActual" value="${ ubicacionAplicacion.idUbicacionAplicacionActual }"/>
    	<c:forEach items="${ ubicacionAplicacion.ruta }" var="ub">
    		<li><a href="javascript: doSubmitBreadcrumb('<c:out value="${ ub.submit }"/>', '<c:out value="${ ub.id }"/>')"><c:out value="${ ub.label }" escapeXml="false" /></a>  </li> 
    	</c:forEach>			
    </c:if>
    	<li><fmt:message key="bsInstalacion.bc.etiqueta.crear" /></li>
    </ul>
    </div>
    	<input type="hidden" name="id_ubicacion_retorno" id="id_ubicacion_retorno" value=""/>
    </div>	
	
	
<%-- <div id="iTitle" class="ui-state-default ui-widget-content ui-corner-all">
	<fmt:message key="bsInstalacion.formulario.etiqueta.crear.titulo"/>
</div> --%>
	<input type="hidden" id="pageLoc" name="pageLoc" value="crear"/>
	<input type="hidden" id="primerCampo" name="primerCampo" value="cliente"/>
    <input type="hidden" id="eag" name="eag" value="${eag}"/>
    <input type="hidden" id="movimientoPagina" name="movimientoPagina"/>
    <input type="hidden" id="columna" name="columna"/>
    <input type="hidden" id="fenixf31seguridad" name="fenixf31seguridad" value="<c:out value='${fenixf31seguridad}' />" />
	<c:if test="${not empty scrollTo }">
    <input type="hidden" id="scrollSmooth" value="<c:out value='${scrollTo}' />" />
    </c:if>
	<!-- valores de llaves -->
	
	


<!-- mensajes de error-->
<c:if test="${mensajesUsuario.erroresGlobales}">
<div class="ui-state-error ui-corner-all" style="padding: 0 .7em; font-size: 0.8em;"> 
	<span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em; margin-top: .3em;"></span> 
	<strong><fmt:message key="globales.formulario.etiqueta.error"/></strong> <br /> 
	<ul>
		<c:forEach items="${mensajesUsuario.mensajesDeError}" var="mensaje">
        	<li><c:out value="${mensaje}" escapeXml="false"/></li>
     	</c:forEach>
	</ul>
</div>
</c:if>

<div id="action-buttons">
<div id="action-bar">
    <button tabindex="23" id="btnGuardar" type="submit" name="ok" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsInstalacion/crearEjecutar');"><fmt:message key="globales.formulario.boton.crear.guardar"/></button>
	<button tabindex="24" id="btnCancelar" type="button" name="cancel" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsInstalacion/cancelarCrear');"><fmt:message key="globales.formulario.boton.crear.cancelar"/></button>
</div>
</div>
<br />

<!-- datos de formulario -->
<div><span style="font-size: small;">-Los campos marcados con * son obligatorios</span></div>
<div class="ui-widget ui-widget-content ui-corner-all" style="width: 99%">
		<div id="formInputHeader" class="ui-widget ui-widget-header ui-corner-top" style="padding-left: 5px;"> 
			<fmt:message key="bsInstalacion.formulario.etiqueta.crear.titulo"/> 
		</div>
		<div style="display: inline;" id="formInput">
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['cliente']}">	
		<div class="row" >
			<span class="label" style="color: #AA0707;"> 
                <fmt:message key="bsInstalacion.formulario.etiqueta.Cliente"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="1" alt="integer" type="text" id="cliente" name="cliente"  value="${bsInstalacion.cliente}" size="10" maxlength="10"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsInstalacion/ayudaPropiedadJson','cliente')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['cliente'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['modulos']}">	
		<div class="row" >
			<span class="label" style="color: #AA0707;"> 
                <fmt:message key="bsInstalacion.formulario.etiqueta.Modulos"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="2" alt="integer" type="text" id="modulos" name="modulos"  value="${bsInstalacion.modulos}" size="10" maxlength="10"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsInstalacion/ayudaPropiedadJson','modulos')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['modulos'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['nombre']}">	
		<div class="row" >
			<span class="label" style="color: #AA0707;"> 
                <fmt:message key="bsInstalacion.formulario.etiqueta.Nombre"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="3" type="text" id="nombre" name="nombre"  value="${bsInstalacion.nombre}" size="50"  maxlength="50"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsInstalacion/ayudaPropiedadJson','nombre')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['nombre'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['id']}">	
		<div class="row" >
			<span class="label" style="color: #AA0707;"> 
                <fmt:message key="bsInstalacion.formulario.etiqueta.Id"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="4" type="text" id="id" name="id"  value="${bsInstalacion.id}" size="50"  maxlength="1"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsInstalacion/ayudaPropiedadJson','id')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['id'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['password']}">	
		<div class="row" >
			<span class="label" style="color: #AA0707;"> 
                <fmt:message key="bsInstalacion.formulario.etiqueta.Password"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="5" type="text" id="password" name="password"  value="${bsInstalacion.password}" size="50"  maxlength="10"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsInstalacion/ayudaPropiedadJson','password')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['password'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['pesoa']}">	
		<div class="row" >
			<span class="label" style="color: #AA0707;"> 
                <fmt:message key="bsInstalacion.formulario.etiqueta.Pesoa"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="6" alt="integer" type="text" id="pesoa" name="pesoa"  value="${bsInstalacion.pesoa}" size="10" maxlength="3"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsInstalacion/ayudaPropiedadJson','pesoa')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['pesoa'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['pesob']}">	
		<div class="row" >
			<span class="label" style="color: #AA0707;"> 
                <fmt:message key="bsInstalacion.formulario.etiqueta.Pesob"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="7" alt="integer" type="text" id="pesob" name="pesob"  value="${bsInstalacion.pesob}" size="10" maxlength="3"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsInstalacion/ayudaPropiedadJson','pesob')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['pesob'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['pesoc']}">	
		<div class="row" >
			<span class="label" style="color: #AA0707;"> 
                <fmt:message key="bsInstalacion.formulario.etiqueta.Pesoc"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="8" alt="integer" type="text" id="pesoc" name="pesoc"  value="${bsInstalacion.pesoc}" size="10" maxlength="3"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsInstalacion/ayudaPropiedadJson','pesoc')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['pesoc'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['pesod']}">	
		<div class="row" >
			<span class="label" style="color: #AA0707;"> 
                <fmt:message key="bsInstalacion.formulario.etiqueta.Pesod"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="9" alt="integer" type="text" id="pesod" name="pesod"  value="${bsInstalacion.pesod}" size="10" maxlength="3"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsInstalacion/ayudaPropiedadJson','pesod')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['pesod'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['pesoe']}">	
		<div class="row" >
			<span class="label" style="color: #AA0707;"> 
                <fmt:message key="bsInstalacion.formulario.etiqueta.Pesoe"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="10" alt="integer" type="text" id="pesoe" name="pesoe"  value="${bsInstalacion.pesoe}" size="10" maxlength="3"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsInstalacion/ayudaPropiedadJson','pesoe')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['pesoe'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['estatuscom']}">	
		<div class="row" >
			<span class="label" > 
                <fmt:message key="bsInstalacion.formulario.etiqueta.Estatuscom"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="11" alt="integer" type="text" id="estatuscom" name="estatuscom"  value="${bsInstalacion.estatuscom}" size="10" maxlength="3"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsInstalacion/ayudaPropiedadJson','estatuscom')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['estatuscom'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['fechaestatus']}">	
		<div class="row" >
			<span class="label" style="color: #AA0707;"> 
                <fmt:message key="bsInstalacion.formulario.etiqueta.Fechaestatus"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="12" type="text" cdate="fechaestatus" id="fechaestatus" name="fechaestatus"  value='<fmt:formatDate value="${bsInstalacion.fechaestatus}" pattern="dd-MM-yyyy"/>' size="12" />
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsInstalacion/ayudaPropiedadJson','fechaestatus')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['fechaestatus'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['estatus']}">	
		<div class="row" >
			<span class="label" style="color: #AA0707;"> 
                <fmt:message key="bsInstalacion.formulario.etiqueta.Estatus"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="13" alt="integer" type="text" id="estatus" name="estatus"  value="${bsInstalacion.estatus}" size="10" maxlength="3"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsInstalacion/ayudaPropiedadJson','estatus')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['estatus'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['envioCliente']}">	
		<div class="row" >
			<span class="label" > 
                <fmt:message key="bsInstalacion.formulario.etiqueta.EnvioCliente"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="14" alt="integer" type="text" id="envioCliente" name="envioCliente"  value="${bsInstalacion.envioCliente}" size="10" maxlength="10"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsInstalacion/ayudaPropiedadJson','envioCliente')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['envioCliente'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['version']}">	
		<div class="row" >
			<span class="label" > 
                <fmt:message key="bsInstalacion.formulario.etiqueta.Version"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="15" type="text" id="version" name="version"  value="${bsInstalacion.version}" size="50"  maxlength="11"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsInstalacion/ayudaPropiedadJson','version')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['version'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['versionpe']}">	
		<div class="row" >
			<span class="label" > 
                <fmt:message key="bsInstalacion.formulario.etiqueta.Versionpe"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="16" type="text" id="versionpe" name="versionpe"  value="${bsInstalacion.versionpe}" size="50"  maxlength="11"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsInstalacion/ayudaPropiedadJson','versionpe')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['versionpe'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['origenlotes']}">	
		<div class="row" >
			<span class="label" style="color: #AA0707;"> 
                <fmt:message key="bsInstalacion.formulario.etiqueta.Origenlotes"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="17" alt="integer" type="text" id="origenlotes" name="origenlotes"  value="${bsInstalacion.origenlotes}" size="10" maxlength="3"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsInstalacion/ayudaPropiedadJson','origenlotes')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['origenlotes'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['siguientelotecc']}">	
		<div class="row" >
			<span class="label" > 
                <fmt:message key="bsInstalacion.formulario.etiqueta.Siguientelotecc"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="18" alt="integer" type="text" id="siguientelotecc" name="siguientelotecc"  value="${bsInstalacion.siguientelotecc}" size="10" maxlength="19"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsInstalacion/ayudaPropiedadJson','siguientelotecc')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['siguientelotecc'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['siguientelotepe']}">	
		<div class="row" >
			<span class="label" > 
                <fmt:message key="bsInstalacion.formulario.etiqueta.Siguientelotepe"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="19" alt="integer" type="text" id="siguientelotepe" name="siguientelotepe"  value="${bsInstalacion.siguientelotepe}" size="10" maxlength="19"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsInstalacion/ayudaPropiedadJson','siguientelotepe')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['siguientelotepe'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['siguientelotepp']}">	
		<div class="row" >
			<span class="label" > 
                <fmt:message key="bsInstalacion.formulario.etiqueta.Siguientelotepp"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="20" alt="integer" type="text" id="siguientelotepp" name="siguientelotepp"  value="${bsInstalacion.siguientelotepp}" size="10" maxlength="19"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsInstalacion/ayudaPropiedadJson','siguientelotepp')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['siguientelotepp'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['siguienteloteps']}">	
		<div class="row" >
			<span class="label" > 
                <fmt:message key="bsInstalacion.formulario.etiqueta.Siguienteloteps"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="21" alt="integer" type="text" id="siguienteloteps" name="siguienteloteps"  value="${bsInstalacion.siguienteloteps}" size="10" maxlength="19"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsInstalacion/ayudaPropiedadJson','siguienteloteps')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['siguienteloteps'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['siguienteloteach']}">	
		<div class="row" >
			<span class="label" > 
                <fmt:message key="bsInstalacion.formulario.etiqueta.Siguienteloteach"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="22" alt="integer" type="text" id="siguienteloteach" name="siguienteloteach"  value="${bsInstalacion.siguienteloteach}" size="10" maxlength="19"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsInstalacion/ayudaPropiedadJson','siguienteloteach')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['siguienteloteach'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
		</div>
</div>		
</div>
</form>

<%@ include file="/WEB-INF/global/jsp/jq-footer.jsp" %>
</fmt:bundle>
