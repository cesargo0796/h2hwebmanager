<%@ taglib prefix="c"       uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt"     uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:if test="${ not empty locale}">
	<fmt:setLocale value="${ locale }" />
</c:if>
<fmt:bundle basename="com.ofbox.davivienda.h2h.resourceBundle.MessagesResources">
	
<c:if test="${ not empty hthAtributoFormatoEstado.enlaceDetalle}">
	<c:set var="detalle" value="${hthAtributoFormatoEstado.enlaceDetalle}"/>	
	<c:set var="detalleEnllavador" value="${detalle.enllavador}"/>
</c:if>		

<c:set var="nombre" value="hthAtributoFormato"/>
<c:set var="ubicacion" value="actualizar"/>	
<c:set var="primerCampo" value="btnhthAtributoEncabezado"/>
<%@ include file="/WEB-INF/global/jsp/jq-header.jsp" %>
	
<form  name="mainForm" id="mainForm" action="${NewtPageRenderData.actionUrl}" method="post">
<div class="breadCrumbHolder module" style="margin-bottom: 3px;">
<div id="breadcrumb" class="breadCrumb module ui-widget ui-widget-content" style="width: 99%;">
<ul>
	<li><a href="${ctx}/inicio/blank/ria"> <fmt:message key="globales.bc.etiqueta.home" /> </a> </li>
<c:if test="${ not empty ubicacionAplicacion.ruta }">
	<input type="hidden" name="idUbicacionAplicacionActual" id="idUbicacionAplicacionActual" value="${ ubicacionAplicacion.idUbicacionAplicacionActual }"/>
	<c:forEach items="${ ubicacionAplicacion.ruta }" var="ub">
		<li><a href="javascript: doSubmitBreadcrumb('<c:out value="${ ub.submit }"/>', '<c:out value="${ ub.id }"/>')"><c:out value="${ ub.label }" escapeXml="false" /></a>  </li> 
	</c:forEach>			
</c:if>
	<li><fmt:message key="hthAtributoFormato.bc.etiqueta.actualizar" /></li>
</ul>
</div>
	<input type="hidden" name="id_ubicacion_retorno" id="id_ubicacion_retorno" value=""/>
</div>	
<c:if test="${ not empty detalle.cabecera}">
    <jsp:include page="/WEB-INF/jsp/${detalle.cabecera.locacion}/jq/cabecera.jsp" />
</c:if>
<div id="mainContent">

    <div id="iTitle" class="ui-state-default ui-widget-content ui-corner-all">
    	<fmt:message key="hthAtributoFormato.formulario.etiqueta.actualizar.titulo"/>
    </div>
    
    <div id="iProcessing" style="visibility: hidden" align="center">
    	<img src="${ctx}/include/images/design/processing_request.gif"></img>
    </div>

	<input type="hidden" id="pageLoc" name="pageLoc" value="actualizar"/>
	<input type="hidden" id="primerCampo" name="primerCampo" value="btnhthAtributoEncabezado"/>
    <input type="hidden" id="eag" name="eag" value="${eag}"/>
    <input type="hidden" id="movimientoPagina" name="movimientoPagina"/>
    <input type="hidden" id="columna" name="columna"/>
    <input type="hidden" id="fenixf31seguridad" name="fenixf31seguridad" value="<c:out value='${fenixf31seguridad}' />" />
	<c:if test="${not empty scrollTo }">
    <input type="hidden" id="scrollSmooth" value="<c:out value='${scrollTo}' />" />
    </c:if>
	<!-- valores de llaves -->
	<input type="hidden" id="idAtributoFormato" name="idAtributoFormato" value="${hthAtributoFormato.idAtributoFormato}"/>	
	
	<input type="hidden" id="nombreBusqueda" name="nombreBusqueda"/>


<!-- mensajes de error-->
<c:if test="${mensajesUsuario.erroresGlobales}">
<div class="ui-state-error ui-corner-all" style="padding: 0 .7em; font-size: 0.8em;"> 
	<span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span> 
	<strong><fmt:message key="globales.formulario.etiqueta.error"/></strong> <br /> 
	<ul>
		<c:forEach items="${mensajesUsuario.mensajesDeError}" var="mensaje">
        	<li><c:out value="${mensaje}" escapeXml="false"/></li>
     	</c:forEach>
	</ul>
</div>
</c:if>

<div id="action-buttons">
<div id="action-bar">
	<button tabindex="10" id="btnGuardar" type="submit" name="actualizar" onClick="doSubmit<c:if test="${ not empty detalle.cabecera}">Async</c:if>('hthAtributoFormato/actualizarEjecutar');"><fmt:message key="globales.formulario.boton.actualizar.guardar"/></button>
	<button tabindex="11" id="btnCancelar" type="button" name="cancel" onClick="doSubmit<c:if test="${ not empty detalle.cabecera}">Async</c:if>('hthAtributoFormato/cancelarActualizar');"><fmt:message key="globales.formulario.boton.actualizar.cancelar"/></button>
</div>
</div>
<br />

<!-- datos de formulario -->
<div><span style="font-size: small;">-Los campos marcados con * son obligatorios</span></div>
<div class="ui-widget ui-widget-content ui-corner-all" style="width: 99%">
		<div id="formInputHeader" class="ui-widget ui-widget-header ui-corner-top" style="padding-left: 5px;"> 
			<fmt:message key="hthAtributoFormato.formulario.etiqueta.actualizar.titulo"/> 
		</div>
		<div style="display: inline;" id="formInput">
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['idAtributoEncabezado'] and hthFormatoDtoCabeza.tipoFormato == 'E'}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="hthAtributoFormato.formulario.etiqueta.IdAtributoEncabezado"/>			</span>	 
			
			<span class="forminput" style="display: inline">
	            <input readonly="readonly" type="text" id="idAtributoEncabezado" name="idAtributoEncabezado" value="${hthAtributoFormato.idAtributoEncabezado}"  size="10"  />
				<input readonly="readonly" type="text" id="hthAtributoEncabezado.nombreAtributo" name="hthAtributoEncabezado.nombreAtributo" value="${hthAtributoFormato.hthAtributoEncabezado.nombreAtributo}"  size="40"  />
				<button tabindex="1" id="btnhthAtributoEncabezado" class="sq" onClick="doForaneo('hthAtributoEncabezado', 'hthAtributoFormato/buscar');"><fmt:message key='globales.formulario.boton.lov'/></button>

			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('hthAtributoFormato/ayudaPropiedadJson','idAtributoEncabezado')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['idAtributoEncabezado'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['idAtributoDetalle'] and hthFormatoDtoCabeza.tipoFormato == 'D'}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="hthAtributoFormato.formulario.etiqueta.IdAtributoDetalle"/>			</span>	 
			
			<span class="forminput" style="display: inline">
	            <input readonly="readonly" type="text" id="idAtributoDetalle" name="idAtributoDetalle" value="${hthAtributoFormato.idAtributoDetalle}"  size="10"  />
				<input readonly="readonly" type="text" id="hthAtributoDetalle.nombreAtributo" name="hthAtributoDetalle.nombreAtributo" value="${hthAtributoFormato.hthAtributoDetalle.nombreAtributo}"  size="40"  />
				<button tabindex="2" id="btnhthAtributoDetalle" class="sq" onClick="doForaneo('hthAtributoDetalle', 'hthAtributoFormato/buscar');"><fmt:message key='globales.formulario.boton.lov'/></button>

			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('hthAtributoFormato/ayudaPropiedadJson','idAtributoDetalle')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['idAtributoDetalle'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['idFormato']}">	
		<div class="row">
			<span class="label" style="color: #AA0707;"> 
				<fmt:message key="hthAtributoFormato.formulario.etiqueta.IdFormato"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
	            <input readonly="readonly" type="text" id="idFormato" name="idFormato" value="${hthAtributoFormato.idFormato}"  size="10"  />
				<input readonly="readonly" type="text" id="hthFormato.nombreFormato" name="hthFormato.nombreFormato" value="${hthAtributoFormato.hthFormato.nombreFormato}"  size="40"  />
				<button tabindex="3" id="btnhthFormato" class="sq" onClick="doForaneo('hthFormato', 'hthAtributoFormato/buscar');"><fmt:message key='globales.formulario.boton.lov'/></button>

			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('hthAtributoFormato/ayudaPropiedadJson','idFormato')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['idFormato'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['posicion']}">	
		<div class="row">
			<span class="label" style="color: #AA0707;"> 
				<fmt:message key="hthAtributoFormato.formulario.etiqueta.Posicion"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="4" alt="integer" type="text" id="posicion" name="posicion"  value="${hthAtributoFormato.posicion}" size="10" maxlength="10"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('hthAtributoFormato/ayudaPropiedadJson','posicion')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['posicion'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['posicionIni']}">	
		<div class="row">
			<span class="label" style="color: #AA0707;"> 
				<fmt:message key="hthAtributoFormato.formulario.etiqueta.PosicionIni"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="5" alt="integer" type="text" id="posicionIni" name="posicionIni"  value="${hthAtributoFormato.posicionIni}" size="10" maxlength="10"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('hthAtributoFormato/ayudaPropiedadJson','posicionIni')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['posicionIni'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['posicionFin']}">	
		<div class="row">
			<span class="label" style="color: #AA0707;"> 
				<fmt:message key="hthAtributoFormato.formulario.etiqueta.PosicionFin"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="6" alt="integer" type="text" id="posicionFin" name="posicionFin"  value="${hthAtributoFormato.posicionFin}" size="10" maxlength="10"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('hthAtributoFormato/ayudaPropiedadJson','posicionFin')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['posicionFin'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['tipoDatoAtributo']}">	
		<div class="row">
			<span class="label" style="color: #AA0707;"> 
				<fmt:message key="hthAtributoFormato.formulario.etiqueta.TipoDatoAtributo"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
	            <select  tabindex="7" id="tipoDatoAtributo" name="tipoDatoAtributo" class="ui-widget ui-state-default">
		            <option value="T" <c:if test="${hthAtributoFormato.tipoDatoAtributo == 'T'}">selected</c:if>><fmt:message key="hthAtributoFormato.formulario.comboLabel.Texto"/></option>
		            <option value="E" <c:if test="${hthAtributoFormato.tipoDatoAtributo == 'E'}">selected</c:if>><fmt:message key="hthAtributoFormato.formulario.comboLabel.Entero"/></option>
		            <option value="D" <c:if test="${hthAtributoFormato.tipoDatoAtributo == 'D'}">selected</c:if>><fmt:message key="hthAtributoFormato.formulario.comboLabel.Decimal"/></option>
		            <option value="F" <c:if test="${hthAtributoFormato.tipoDatoAtributo == 'F'}">selected</c:if>><fmt:message key="hthAtributoFormato.formulario.comboLabel.Fecha"/></option>
			    </select>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('hthAtributoFormato/ayudaPropiedadJson','tipoDatoAtributo')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['tipoDatoAtributo'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['formatoAtributo']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="hthAtributoFormato.formulario.etiqueta.FormatoAtributo"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="8" type="text" id="formatoAtributo" name="formatoAtributo"  value="${hthAtributoFormato.formatoAtributo}" size="50"  maxlength="25"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('hthAtributoFormato/ayudaPropiedadJson','formatoAtributo')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['formatoAtributo'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['opcional']}">	
		<div class="row">
			<span class="label" style="color: #AA0707;"> 
				<fmt:message key="hthAtributoFormato.formulario.etiqueta.Opcional"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
	            <select  tabindex="9" id="opcional" name="opcional" class="ui-widget ui-state-default">
		            <option value="S" <c:if test="${hthAtributoFormato.opcional == 'S'}">selected</c:if>><fmt:message key="hthAtributoFormato.formulario.comboLabel.Si"/></option>
		            <option value="N" <c:if test="${hthAtributoFormato.opcional == 'N'}">selected</c:if>><fmt:message key="hthAtributoFormato.formulario.comboLabel.No"/></option>
			    </select>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('hthAtributoFormato/ayudaPropiedadJson','opcional')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['opcional'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
		</div>
</div>		
</div>
</form>

<%@ include file="/WEB-INF/global/jsp/jq-footer.jsp" %>
</fmt:bundle>
