<%@ taglib prefix="c"       uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt"     uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:if test="${ not empty locale}">
	<fmt:setLocale value="${ locale }" />
</c:if>
<fmt:bundle basename="com.ofbox.davivienda.h2h.resourceBundle.MessagesResources">
<c:if test="${ not empty bsDetallepeEstado.enlaceDetalle}">
	<c:set var="detalle" value="${bsDetallepeEstado.enlaceDetalle}"/>	
	<c:set var="detalleEnllavador" value="${detalle.enllavador}"/>
</c:if>		

<c:set var="nombre" value="bsDetallepe"/>
<c:set var="ubicacion" value="ver"/>

<%@ include file="/WEB-INF/global/jsp/jq-header.jsp" %>

<form  name="mainForm" id="mainForm" action="${NewtPageRenderData.actionUrl}" method="post">
<c:if test="${ not empty detalle.cabecera}">
    <jsp:include page="/WEB-INF/jsp/${detalle.cabecera.locacion}/jq/cabecera.jsp" />
</c:if>
<div id="mainContent">
	<div class="breadCrumbHolder module" style="margin-bottom: 3px;">
<div id="breadcrumb" class="breadCrumb module ui-widget ui-widget-content" style="width: 99%;">
<ul>
	<li><a href="${ctx}/inicio/blank/ria"> HOME </a> </li>
<c:if test="${ not empty ubicacionAplicacion.ruta }">
	<input type="hidden" name="idUbicacionAplicacionActual" id="idUbicacionAplicacionActual" value="${ ubicacionAplicacion.idUbicacionAplicacionActual }"/>
	<c:forEach items="${ ubicacionAplicacion.ruta }" var="ub">
		<li><a href="javascript: doSubmitBreadcrumb('<c:out value="${ ub.submit }"/>', '<c:out value="${ ub.id }"/>')"><c:out value="${ ub.label }" escapeXml="false" /></a>  </li> 
	</c:forEach>			
</c:if>
	<li><fmt:message key="bsDetallepe.bc.etiqueta.ver" /></li>
</ul>
</div>
	<input type="hidden" name="id_ubicacion_retorno" id="id_ubicacion_retorno" value=""/>
</div>
	
<%--<div id="iTitle" class="ui-state-default ui-widget-content ui-corner-all">
	<fmt:message key="bsDetallepe.formulario.etiqueta.ver.titulo"/>
</div>--%>

    <div id="iProcessing" style="visibility: hidden" align="center">
    	<img src="${ctx}/include/images/design/processing_request.gif"></img>
    </div>
	<input type="hidden" id="pageLoc" name="pageLoc" value="ver"/>
    <input type="hidden" id="eag" name="eag" value="${eag}"/>
    <input type="hidden" id="movimientoPagina" name="movimientoPagina"/>
    <input type="hidden" id="columna" name="columna"/>
    <input type="hidden" id="fenixf31seguridad" name="fenixf31seguridad" value="<c:out value='${fenixf31seguridad}' />" />
	<!-- valores de llaves -->
	
	
	<input type="hidden" id="nombreBusqueda" name="nombreBusqueda"/>

<div id="action-buttons">
<div id="action-bar">
	<button id="btnCancelar" type="button" name="regresar" onClick="doSubmit<c:if test="${ not empty detalle.cabecera}">Async</c:if>('bsDetallepe/cancelarVer');"><fmt:message key="globales.formulario.boton.regresar"/></button>
</div>
</div>
<br />

<!-- datos de formulario -->
<div class="ui-widget ui-widget-content ui-corner-all" style="width: 99%">
		<div id="formInputHeader" class="ui-widget ui-widget-header ui-corner-top" style="padding-left: 5px;"> 
			<fmt:message key="bsDetallepe.formulario.etiqueta.ver.titulo"/> 
		</div>
		
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsDetallepe.formulario.etiqueta.Cuenta"/>
			</span>	 
			
			<span class="formvalue">
                ${bsDetallepe.cuenta}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsDetallepe.formulario.etiqueta.Tipooperacion"/>
			</span>	 
			
			<span class="formvalue">
                ${bsDetallepe.tipooperacion}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsDetallepe.formulario.etiqueta.Monto"/>
			</span>	 
			
			<span class="formvalue">
                ${bsDetallepe.monto}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsDetallepe.formulario.etiqueta.Montoaplicado"/>
			</span>	 
			
			<span class="formvalue">
                ${bsDetallepe.montoaplicado}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsDetallepe.formulario.etiqueta.Adenda"/>
			</span>	 
			
			<span class="formvalue">
                ${bsDetallepe.adenda}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsDetallepe.formulario.etiqueta.Fechaestatus"/>
			</span>	 
			
			<span class="formvalue">
                <fmt:formatDate pattern="dd-MM-yyyy" value="${bsDetallepe.fechaestatus}" />
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsDetallepe.formulario.etiqueta.Estatus"/>
			</span>	 
			
			<span class="formvalue">
                ${bsDetallepe.estatus}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsDetallepe.formulario.etiqueta.Idpago"/>
			</span>	 
			
			<span class="formvalue">
                ${bsDetallepe.idpago}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsDetallepe.formulario.etiqueta.Autorizacionhost"/>
			</span>	 
			
			<span class="formvalue">
                ${bsDetallepe.autorizacionhost}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsDetallepe.formulario.etiqueta.Enviarahost"/>
			</span>	 
			
			<span class="formvalue">
                ${bsDetallepe.enviarahost}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsDetallepe.formulario.etiqueta.Contrasena"/>
			</span>	 
			
			<span class="formvalue">
                ${bsDetallepe.contrasena}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsDetallepe.formulario.etiqueta.Nombrebenef"/>
			</span>	 
			
			<span class="formvalue">
                ${bsDetallepe.nombrebenef}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsDetallepe.formulario.etiqueta.Direccionbenef"/>
			</span>	 
			
			<span class="formvalue">
                ${bsDetallepe.direccionbenef}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsDetallepe.formulario.etiqueta.Email"/>
			</span>	 
			
			<span class="formvalue">
                ${bsDetallepe.email}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsDetallepe.formulario.etiqueta.Comisionaplicada"/>
			</span>	 
			
			<span class="formvalue">
                ${bsDetallepe.comisionaplicada}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsDetallepe.formulario.etiqueta.Ivaaplicado"/>
			</span>	 
			
			<span class="formvalue">
                ${bsDetallepe.ivaaplicado}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsDetallepe.formulario.etiqueta.Nombrecuenta"/>
			</span>	 
			
			<span class="formvalue">
                ${bsDetallepe.nombrecuenta}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsDetallepe.formulario.etiqueta.Niu"/>
			</span>	 
			
			<span class="formvalue">
                ${bsDetallepe.niu}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsDetallepe.formulario.etiqueta.Autorizador"/>
			</span>	 
			
			<span class="formvalue">
                ${bsDetallepe.autorizador}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsDetallepe.formulario.etiqueta.Sublote"/>
			</span>	 
			
			<span class="formvalue">
                ${bsDetallepe.sublote}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsDetallepe.formulario.etiqueta.Cuentadebito"/>
			</span>	 
			
			<span class="formvalue">
                ${bsDetallepe.cuentadebito}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsDetallepe.formulario.etiqueta.Nombredecuenta"/>
			</span>	 
			
			<span class="formvalue">
                ${bsDetallepe.nombredecuenta}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsDetallepe.formulario.etiqueta.Montoimpuesto"/>
			</span>	 
			
			<span class="formvalue">
                ${bsDetallepe.montoimpuesto}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsDetallepe.formulario.etiqueta.Codigoorefint"/>
			</span>	 
			
			<span class="formvalue">
                ${bsDetallepe.codigoorefint}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
		
</div>		
</div>
</form>

<%@ include file="/WEB-INF/global/jsp/jq-footer.jsp" %>
</fmt:bundle>
