<%@ taglib prefix="c"       uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt"     uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:if test="${ not empty locale}">
	<fmt:setLocale value="${ locale }" />
</c:if>
<fmt:bundle basename="com.ofbox.davivienda.h2h.resourceBundle.MessagesResources">
	
<c:if test="${ not empty bsDetallepeEstado.enlaceDetalle}">
	<c:set var="detalle" value="${bsDetallepeEstado.enlaceDetalle}"/>	
	<c:set var="detalleEnllavador" value="${detalle.enllavador}"/>
</c:if>		

<c:set var="nombre" value="bsDetallepe"/>
<c:set var="ubicacion" value="actualizar"/>	
<c:set var="primerCampo" value="cuenta"/>
<%@ include file="/WEB-INF/global/jsp/jq-header.jsp" %>
	
<form  name="mainForm" id="mainForm" action="${NewtPageRenderData.actionUrl}" method="post">
<c:if test="${ not empty detalle.cabecera}">
    <jsp:include page="/WEB-INF/jsp/${detalle.cabecera.locacion}/jq/cabecera.jsp" />
</c:if>
<div id="mainContent">

	<div class="breadCrumbHolder module" style="margin-bottom: 3px;">
<div id="breadcrumb" class="breadCrumb module ui-widget ui-widget-content" style="width: 99%;">
<ul>
	<li><a href="${ctx}/inicio/blank/ria"> HOME </a> </li>
<c:if test="${ not empty ubicacionAplicacion.ruta }">
	<input type="hidden" name="idUbicacionAplicacionActual" id="idUbicacionAplicacionActual" value="${ ubicacionAplicacion.idUbicacionAplicacionActual }"/>
	<c:forEach items="${ ubicacionAplicacion.ruta }" var="ub">
		<li><a href="javascript: doSubmitBreadcrumb('<c:out value="${ ub.submit }"/>', '<c:out value="${ ub.id }"/>')"><c:out value="${ ub.label }" escapeXml="false" /></a>  </li> 
	</c:forEach>			
</c:if>
	<li><fmt:message key="bsDetallepe.bc.etiqueta.actualizar" /></li>
</ul>
</div>
	<input type="hidden" name="id_ubicacion_retorno" id="id_ubicacion_retorno" value=""/>
</div>	
	
<div id="iProcessing" style="visibility: hidden" align="center">
	<img src="${ctx}/include/images/design/processing_request.gif"></img>
</div>

<%--<div id="iTitle" class="ui-state-default ui-widget-content ui-corner-all">
	<fmt:message key="bsDetallepe.formulario.etiqueta.actualizar.titulo"/>
</div> --%>
	<input type="hidden" id="pageLoc" name="pageLoc" value="actualizar"/>
	<input type="hidden" id="primerCampo" name="primerCampo" value="cuenta"/>
    <input type="hidden" id="eag" name="eag" value="${eag}"/>
    <input type="hidden" id="movimientoPagina" name="movimientoPagina"/>
    <input type="hidden" id="columna" name="columna"/>
    <input type="hidden" id="fenixf31seguridad" name="fenixf31seguridad" value="<c:out value='${fenixf31seguridad}' />" />
	<c:if test="${not empty scrollTo }">
    <input type="hidden" id="scrollSmooth" value="<c:out value='${scrollTo}' />" />
    </c:if>
	<!-- valores de llaves -->
	<input type="hidden" id="instalacion" name="instalacion" value="${bsDetallepe.instalacion}"/>	
	<input type="hidden" id="lote" name="lote" value="${bsDetallepe.lote}"/>	
	<input type="hidden" id="operacion" name="operacion" value="${bsDetallepe.operacion}"/>	
	
	<input type="hidden" id="nombreBusqueda" name="nombreBusqueda"/>


<!-- mensajes de error-->
<c:if test="${mensajesUsuario.erroresGlobales}">
<div class="ui-state-error ui-corner-all" style="padding: 0 .7em; font-size: 0.8em;"> 
	<span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span> 
	<strong><fmt:message key="globales.formulario.etiqueta.error"/></strong> <br /> 
	<ul>
		<c:forEach items="${mensajesUsuario.mensajesDeError}" var="mensaje">
        	<li><c:out value="${mensaje}" escapeXml="false"/></li>
     	</c:forEach>
	</ul>
</div>
</c:if>

<div id="action-buttons">
<div id="action-bar">
	<button tabindex="25" id="btnGuardar" type="submit" name="actualizar" onClick="doSubmit<c:if test="${ not empty detalle.cabecera}">Async</c:if>('bsDetallepe/actualizarEjecutar');"><fmt:message key="globales.formulario.boton.actualizar.guardar"/></button>
	<button tabindex="26" id="btnCancelar" type="button" name="cancel" onClick="doSubmit<c:if test="${ not empty detalle.cabecera}">Async</c:if>('bsDetallepe/cancelarActualizar');"><fmt:message key="globales.formulario.boton.actualizar.cancelar"/></button>
</div>
</div>
<br />

<!-- datos de formulario -->
<div><span style="font-size: small;">-Los campos marcados con * son obligatorios</span></div>
<div class="ui-widget ui-widget-content ui-corner-all" style="width: 99%">
		<div id="formInputHeader" class="ui-widget ui-widget-header ui-corner-top" style="padding-left: 5px;"> 
			<fmt:message key="bsDetallepe.formulario.etiqueta.actualizar.titulo"/> 
		</div>
		<div style="display: inline;" id="formInput">
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['cuenta']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsDetallepe.formulario.etiqueta.Cuenta"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="1" type="text" id="cuenta" name="cuenta"  value="${bsDetallepe.cuenta}" size="50"  maxlength="12"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsDetallepe/ayudaPropiedadJson','cuenta')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['cuenta'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['tipooperacion']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsDetallepe.formulario.etiqueta.Tipooperacion"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="2" alt="integer" type="text" id="tipooperacion" name="tipooperacion"  value="${bsDetallepe.tipooperacion}" size="10" maxlength="3"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsDetallepe/ayudaPropiedadJson','tipooperacion')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['tipooperacion'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['monto']}">	
		<div class="row">
			<span class="label" style="color: #AA0707;"> 
				<fmt:message key="bsDetallepe.formulario.etiqueta.Monto"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="3" alt="numeric" type="text" id="monto" name="monto"  value="${bsDetallepe.monto}" size="10" maxlength="19"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsDetallepe/ayudaPropiedadJson','monto')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['monto'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['montoaplicado']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsDetallepe.formulario.etiqueta.Montoaplicado"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="4" alt="numeric" type="text" id="montoaplicado" name="montoaplicado"  value="${bsDetallepe.montoaplicado}" size="10" maxlength="19"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsDetallepe/ayudaPropiedadJson','montoaplicado')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['montoaplicado'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['adenda']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsDetallepe.formulario.etiqueta.Adenda"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  		      <textarea tabindex="5" id="adenda" name="adenda" cols="45" rows="3"><c:out value="${bsDetallepe.adenda}"/></textarea>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsDetallepe/ayudaPropiedadJson','adenda')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['adenda'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['fechaestatus']}">	
		<div class="row">
			<span class="label" style="color: #AA0707;"> 
				<fmt:message key="bsDetallepe.formulario.etiqueta.Fechaestatus"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="6" type="text" cdate="fechaestatus" id="fechaestatus" name="fechaestatus"  value="<fmt:formatDate pattern="dd-MM-yyyy" value="${bsDetallepe.fechaestatus}"/>" size="10" />
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsDetallepe/ayudaPropiedadJson','fechaestatus')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['fechaestatus'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['estatus']}">	
		<div class="row">
			<span class="label" style="color: #AA0707;"> 
				<fmt:message key="bsDetallepe.formulario.etiqueta.Estatus"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="7" alt="integer" type="text" id="estatus" name="estatus"  value="${bsDetallepe.estatus}" size="10" maxlength="3"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsDetallepe/ayudaPropiedadJson','estatus')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['estatus'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['idpago']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsDetallepe.formulario.etiqueta.Idpago"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="8" type="text" id="idpago" name="idpago"  value="${bsDetallepe.idpago}" size="50"  maxlength="50"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsDetallepe/ayudaPropiedadJson','idpago')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['idpago'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['autorizacionhost']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsDetallepe.formulario.etiqueta.Autorizacionhost"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="9" alt="integer" type="text" id="autorizacionhost" name="autorizacionhost"  value="${bsDetallepe.autorizacionhost}" size="10" maxlength="10"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsDetallepe/ayudaPropiedadJson','autorizacionhost')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['autorizacionhost'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['enviarahost']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsDetallepe.formulario.etiqueta.Enviarahost"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="10" alt="integer" type="text" id="enviarahost" name="enviarahost"  value="${bsDetallepe.enviarahost}" size="10" maxlength="3"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsDetallepe/ayudaPropiedadJson','enviarahost')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['enviarahost'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['contrasena']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsDetallepe.formulario.etiqueta.Contrasena"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="11" type="text" id="contrasena" name="contrasena"  value="${bsDetallepe.contrasena}" size="50"  maxlength="50"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsDetallepe/ayudaPropiedadJson','contrasena')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['contrasena'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['nombrebenef']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsDetallepe.formulario.etiqueta.Nombrebenef"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="12" type="text" id="nombrebenef" name="nombrebenef"  value="${bsDetallepe.nombrebenef}" size="50"  maxlength="60"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsDetallepe/ayudaPropiedadJson','nombrebenef')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['nombrebenef'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['direccionbenef']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsDetallepe.formulario.etiqueta.Direccionbenef"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="13" type="text" id="direccionbenef" name="direccionbenef"  value="${bsDetallepe.direccionbenef}" size="50"  maxlength="50"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsDetallepe/ayudaPropiedadJson','direccionbenef')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['direccionbenef'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['email']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsDetallepe.formulario.etiqueta.Email"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="14" type="text" id="email" name="email"  value="${bsDetallepe.email}" size="50"  maxlength="75"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsDetallepe/ayudaPropiedadJson','email')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['email'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['comisionaplicada']}">	
		<div class="row">
			<span class="label" style="color: #AA0707;"> 
				<fmt:message key="bsDetallepe.formulario.etiqueta.Comisionaplicada"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="15" alt="numeric" type="text" id="comisionaplicada" name="comisionaplicada"  value="${bsDetallepe.comisionaplicada}" size="10" maxlength="19"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsDetallepe/ayudaPropiedadJson','comisionaplicada')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['comisionaplicada'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['ivaaplicado']}">	
		<div class="row">
			<span class="label" style="color: #AA0707;"> 
				<fmt:message key="bsDetallepe.formulario.etiqueta.Ivaaplicado"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="16" alt="numeric" type="text" id="ivaaplicado" name="ivaaplicado"  value="${bsDetallepe.ivaaplicado}" size="10" maxlength="19"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsDetallepe/ayudaPropiedadJson','ivaaplicado')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['ivaaplicado'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['nombrecuenta']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsDetallepe.formulario.etiqueta.Nombrecuenta"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="17" type="text" id="nombrecuenta" name="nombrecuenta"  value="${bsDetallepe.nombrecuenta}" size="50"  maxlength="50"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsDetallepe/ayudaPropiedadJson','nombrecuenta')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['nombrecuenta'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['niu']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsDetallepe.formulario.etiqueta.Niu"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="18" alt="integer" type="text" id="niu" name="niu"  value="${bsDetallepe.niu}" size="10" maxlength="10"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsDetallepe/ayudaPropiedadJson','niu')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['niu'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['autorizador']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsDetallepe.formulario.etiqueta.Autorizador"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="19" type="text" id="autorizador" name="autorizador"  value="${bsDetallepe.autorizador}" size="50"  maxlength="10"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsDetallepe/ayudaPropiedadJson','autorizador')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['autorizador'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['sublote']}">	
		<div class="row">
			<span class="label" style="color: #AA0707;"> 
				<fmt:message key="bsDetallepe.formulario.etiqueta.Sublote"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="20" alt="integer" type="text" id="sublote" name="sublote"  value="${bsDetallepe.sublote}" size="10" maxlength="10"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsDetallepe/ayudaPropiedadJson','sublote')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['sublote'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['cuentadebito']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsDetallepe.formulario.etiqueta.Cuentadebito"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="21" type="text" id="cuentadebito" name="cuentadebito"  value="${bsDetallepe.cuentadebito}" size="50"  maxlength="16"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsDetallepe/ayudaPropiedadJson','cuentadebito')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['cuentadebito'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['nombredecuenta']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsDetallepe.formulario.etiqueta.Nombredecuenta"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="22" type="text" id="nombredecuenta" name="nombredecuenta"  value="${bsDetallepe.nombredecuenta}" size="50"  maxlength="50"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsDetallepe/ayudaPropiedadJson','nombredecuenta')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['nombredecuenta'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['montoimpuesto']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsDetallepe.formulario.etiqueta.Montoimpuesto"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="23" alt="numeric" type="text" id="montoimpuesto" name="montoimpuesto"  value="${bsDetallepe.montoimpuesto}" size="10" maxlength="19"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsDetallepe/ayudaPropiedadJson','montoimpuesto')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['montoimpuesto'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['codigoorefint']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsDetallepe.formulario.etiqueta.Codigoorefint"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="24" type="text" id="codigoorefint" name="codigoorefint"  value="${bsDetallepe.codigoorefint}" size="50"  maxlength="60"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsDetallepe/ayudaPropiedadJson','codigoorefint')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['codigoorefint'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
		</div>
</div>		
</div>
</form>

<%@ include file="/WEB-INF/global/jsp/jq-footer.jsp" %>
</fmt:bundle>
