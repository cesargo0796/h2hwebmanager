<%@ taglib prefix="c"       uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt"     uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:if test="${ not empty locale}">
	<fmt:setLocale value="${ locale }" />
</c:if>
<fmt:bundle basename="com.ofbox.davivienda.h2h.resourceBundle.MessagesResources">
<c:if test="${ not empty bsDetalleachEstado.enlaceDetalle}">
	<c:set var="detalle" value="${bsDetalleachEstado.enlaceDetalle}"/>	
	<c:set var="detalleEnllavador" value="${detalle.enllavador}"/>
</c:if>	

<c:set var="nombre" value="bsDetalleach"/>
<c:set var="ubicacion" value="filtrar"/>
<c:set var="primerCampo" value="cuenta"/>
<%@ include file="/WEB-INF/global/jsp/jq-header.jsp" %>

<form  name="mainForm" id="mainForm" action="${NewtPageRenderData.actionUrl}" method="post">
<c:if test="${ not empty detalle.cabecera}">
    <jsp:include page="/WEB-INF/jsp/${detalle.cabecera.locacion}/jq/cabecera.jsp" />
</c:if>	
<div id="mainContent">
	<div class="breadCrumbHolder module" style="margin-bottom: 3px;">
    <div id="breadcrumb" class="breadCrumb module ui-widget ui-widget-content" style="width: 99%;">
    <ul>
    	<li><a href="${ctx}/inicio/blank/ria"> HOME </a> </li>
    <c:if test="${ not empty ubicacionAplicacion.ruta }">
    	<input type="hidden" name="idUbicacionAplicacionActual" id="idUbicacionAplicacionActual" value="${ ubicacionAplicacion.idUbicacionAplicacionActual }"/>
    	<c:forEach items="${ ubicacionAplicacion.ruta }" var="ub">
    		<li><a href="javascript: doSubmitBreadcrumb('<c:out value="${ ub.submit }"/>', '<c:out value="${ ub.id }"/>')"><c:out value="${ ub.label }" escapeXml="false" /></a>  </li> 
    	</c:forEach>			
    </c:if>
    	<li><fmt:message key="bsDetalleach.bc.etiqueta.filtrar" /></li>
    </ul>
    </div>
    	<input type="hidden" name="id_ubicacion_retorno" id="id_ubicacion_retorno" value=""/>
    </div>
	
<%--<div id="iTitle" class="ui-state-default ui-widget-content ui-corner-all">
	<fmt:message key="bsDetalleach.formulario.etiqueta.filtrar.titulo"/>
</div>--%>
<input type="hidden" id="pageLoc" name="pageLoc" value="filtrar"/>
	<input type="hidden" id="primerCampo" name="primerCampo" value="cuenta"/>
    <input type="hidden" id="eag" name="eag" value="${eag}"/>
    <input type="hidden" id="movimientoPagina" name="movimientoPagina"/>
    <input type="hidden" id="columna" name="columna"/>
    <input type="hidden" id="fenixf31seguridad" name="fenixf31seguridad" value="<c:out value='${fenixf31seguridad}' />" />
	<c:if test="${not empty scrollTo }">
    <input type="hidden" id="scrollSmooth" value="<c:out value='${scrollTo}' />" />
    </c:if>
	<!-- valores de llaves -->


<!-- mensajes de error-->
<c:if test="${mensajesUsuario.erroresGlobales}">
<div id="iError">
	<b class="ui-round-top"> 
	<b class="ui-line-one"></b><b class="ui-line-two"></b><b class="ui-line-three"></b><b class="ui-line-four"></b> 
    </b> 
  	<table>
     	<tr>
       		<td>
         	<ol>
	     	<c:forEach items="${mensajesUsuario.mensajesDeError}" var="mensaje">
	        	<li><c:out value="${mensaje}" escapeXml="false"/></li>
	     	</c:forEach>
	     	</ol>
       		</td>
     	</tr>
  	</table>
  	<b class="ui-round-bottom"> 
	<b class="ui-line-four"></b><b class="ui-line-three"></b><b class="ui-line-two"></b><b class="ui-line-one"></b> 
  	</b> 
</div>
</c:if>

<div id="action-buttons">
<div id="action-bar">
	<button tabindex="41" id="btnAplicarFiltro" type="submit" name="Filtrar" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsDetalleach/filtrarEjecutar');"><fmt:message key="globales.filtro.boton.aplicarFiltro"/></button>
	<button tabindex="42" id="btnLimpiar" type="button" name="Limpiar" onClick="limpiarCampos('mainForm');"><fmt:message key="globales.filtro.boton.limpiar"/></button>
	<button tabindex="43" id="btnCancelar" type="button" name="Cancelar" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsDetalleach/cancelarFiltrar');"><fmt:message key="globales.filtro.boton.cancelar"/></button>
</div>
</div>
<br />

<!-- datos de formulario -->
<div class="ui-widget ui-widget-content ui-corner-all" style="width: 99%">
		<div id="formInputHeader" class="ui-widget ui-widget-header ui-corner-top" style="padding-left: 5px;"> 
			<fmt:message key="bsDetalleach.formulario.etiqueta.filtrar.titulo"/> 
		</div>
		<div style="display: inline;" id="formInput">
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['cuenta']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsDetalleach.filtro.etiqueta.Cuenta"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="5" type="text" id="cuenta" name="cuenta" value="${bsDetalleachFiltro['cuenta']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['cuenta'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['tipooperacion']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsDetalleach.filtro.etiqueta.Tipooperacion"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="6" alt="integerMin" type="text" id="tipooperacionMin" name="tipooperacionMin" value="${bsDetalleachFiltro['tipooperacionMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="6" alt="integerMax" type="text" id="tipooperacionMax" name="tipooperacionMax" value="${bsDetalleachFiltro['tipooperacionMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['tipooperacion'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['monto']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsDetalleach.filtro.etiqueta.Monto"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="7" alt="numericMin" type="text" id="montoMin" name="montoMin" value="${bsDetalleachFiltro['montoMin']}"/>
				&nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
				<input tabindex="7" alt="numericMax" type="text" id="montoMax" name="montoMax" value="${bsDetalleachFiltro['montoMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['monto'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['adenda']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsDetalleach.filtro.etiqueta.Adenda"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="8" type="text" id="adenda" name="adenda" value="${bsDetalleachFiltro['adenda']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['adenda'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['fechaestatus']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsDetalleach.filtro.etiqueta.Fechaestatus"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="9" type="text" cdate="fechaestatusMax" cattr="minDate" id="fechaestatusMin" name="fechaestatusMin" value="<fmt:formatDate pattern="dd-MM-yyyy" value="${bsDetalleachFiltro['fechaestatusMin']}"/>"/>
				&nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
				<input tabindex="9" type="text" cdate="fechaestatusMin" cattr="maxDate" id="fechaestatusMax" name="fechaestatusMax" value="<fmt:formatDate pattern="dd-MM-yyyy" value="${bsDetalleachFiltro['fechaestatusMax']}"/>"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['fechaestatus'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['estatus']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsDetalleach.filtro.etiqueta.Estatus"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="10" alt="integerMin" type="text" id="estatusMin" name="estatusMin" value="${bsDetalleachFiltro['estatusMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="10" alt="integerMax" type="text" id="estatusMax" name="estatusMax" value="${bsDetalleachFiltro['estatusMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['estatus'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['idpago']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsDetalleach.filtro.etiqueta.Idpago"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="11" type="text" id="idpago" name="idpago" value="${bsDetalleachFiltro['idpago']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['idpago'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['autorizacionhost']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsDetalleach.filtro.etiqueta.Autorizacionhost"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="12" alt="integerMin" type="text" id="autorizacionhostMin" name="autorizacionhostMin" value="${bsDetalleachFiltro['autorizacionhostMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="12" alt="integerMax" type="text" id="autorizacionhostMax" name="autorizacionhostMax" value="${bsDetalleachFiltro['autorizacionhostMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['autorizacionhost'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['email']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsDetalleach.filtro.etiqueta.Email"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="13" type="text" id="email" name="email" value="${bsDetalleachFiltro['email']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['email'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['comisionaplicada']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsDetalleach.filtro.etiqueta.Comisionaplicada"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="14" alt="numericMin" type="text" id="comisionaplicadaMin" name="comisionaplicadaMin" value="${bsDetalleachFiltro['comisionaplicadaMin']}"/>
				&nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
				<input tabindex="14" alt="numericMax" type="text" id="comisionaplicadaMax" name="comisionaplicadaMax" value="${bsDetalleachFiltro['comisionaplicadaMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['comisionaplicada'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['nombrecuenta']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsDetalleach.filtro.etiqueta.Nombrecuenta"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="15" type="text" id="nombrecuenta" name="nombrecuenta" value="${bsDetalleachFiltro['nombrecuenta']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['nombrecuenta'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['autorizador']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsDetalleach.filtro.etiqueta.Autorizador"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="16" type="text" id="autorizador" name="autorizador" value="${bsDetalleachFiltro['autorizador']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['autorizador'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['estatuspaybank']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsDetalleach.filtro.etiqueta.Estatuspaybank"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="17" type="text" id="estatuspaybank" name="estatuspaybank" value="${bsDetalleachFiltro['estatuspaybank']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['estatuspaybank'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['fechahorahost']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsDetalleach.filtro.etiqueta.Fechahorahost"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="18" type="text" cdate="fechahorahostMax" cattr="minDate" id="fechahorahostMin" name="fechahorahostMin" value="<fmt:formatDate pattern="dd-MM-yyyy" value="${bsDetalleachFiltro['fechahorahostMin']}"/>"/>
				&nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
				<input tabindex="18" type="text" cdate="fechahorahostMin" cattr="maxDate" id="fechahorahostMax" name="fechahorahostMax" value="<fmt:formatDate pattern="dd-MM-yyyy" value="${bsDetalleachFiltro['fechahorahostMax']}"/>"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['fechahorahost'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['cuentadebito']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsDetalleach.filtro.etiqueta.Cuentadebito"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="19" type="text" id="cuentadebito" name="cuentadebito" value="${bsDetalleachFiltro['cuentadebito']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['cuentadebito'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['nombredecuenta']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsDetalleach.filtro.etiqueta.Nombredecuenta"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="20" type="text" id="nombredecuenta" name="nombredecuenta" value="${bsDetalleachFiltro['nombredecuenta']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['nombredecuenta'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['codbanco']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsDetalleach.filtro.etiqueta.Codbanco"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="21" alt="integerMin" type="text" id="codbancoMin" name="codbancoMin" value="${bsDetalleachFiltro['codbancoMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="21" alt="integerMax" type="text" id="codbancoMax" name="codbancoMax" value="${bsDetalleachFiltro['codbancoMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['codbanco'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['tipocuenta']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsDetalleach.filtro.etiqueta.Tipocuenta"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="22" alt="integerMin" type="text" id="tipocuentaMin" name="tipocuentaMin" value="${bsDetalleachFiltro['tipocuentaMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="22" alt="integerMax" type="text" id="tipocuentaMax" name="tipocuentaMax" value="${bsDetalleachFiltro['tipocuentaMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['tipocuenta'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['descestatuspaybank']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsDetalleach.filtro.etiqueta.Descestatuspaybank"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="23" type="text" id="descestatuspaybank" name="descestatuspaybank" value="${bsDetalleachFiltro['descestatuspaybank']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['descestatuspaybank'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['enviarahost']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsDetalleach.filtro.etiqueta.Enviarahost"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="24" alt="integerMin" type="text" id="enviarahostMin" name="enviarahostMin" value="${bsDetalleachFiltro['enviarahostMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="24" alt="integerMax" type="text" id="enviarahostMax" name="enviarahostMax" value="${bsDetalleachFiltro['enviarahostMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['enviarahost'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
</div>	


</div>
</div>
</form>

<%@ include file="/WEB-INF/global/jsp/jq-footer.jsp" %>
</fmt:bundle>
</body>