<%@ taglib prefix="c"       uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt"     uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:if test="${ not empty locale}">
	<fmt:setLocale value="${locale}" />
</c:if>
<fmt:bundle basename="com.ofbox.davivienda.h2h.resourceBundle.MessagesResources">
	
<c:if test="${ not empty bsDetalleachEstado.enlaceDetalle}">
	<c:set var="detalle" value="${bsDetalleachEstado.enlaceDetalle}"/>	
	<c:set var="detalleEnllavador" value="${detalle.enllavador}"/>
</c:if>

<c:set var="nombre" value="bsDetalleach"/>
<c:set var="ubicacion" value="crear"/>
<c:set var="primerCampo" value="cuenta"/>
<%@ include file="/WEB-INF/global/jsp/jq-header.jsp" %>

	
<form  name="mainForm" id="mainForm" action="${NewtPageRenderData.actionUrl}" method="post">
<c:if test="${ not empty detalle.cabecera}">
    <jsp:include page="/WEB-INF/jsp/${detalle.cabecera.locacion}/jq/cabecera.jsp" />
</c:if>
<div id="mainContent">
	<div class="breadCrumbHolder module" style="margin-bottom: 3px;">
    <div id="breadcrumb" class="breadCrumb module ui-widget ui-widget-content" style="width: 99%;">
    <ul>
    	<li><a href="${ctx}/inicio/blank/ria"> HOME </a> </li>
    <c:if test="${ not empty ubicacionAplicacion.ruta }">
    	<input type="hidden" name="idUbicacionAplicacionActual" id="idUbicacionAplicacionActual" value="${ ubicacionAplicacion.idUbicacionAplicacionActual }"/>
    	<c:forEach items="${ ubicacionAplicacion.ruta }" var="ub">
    		<li><a href="javascript: doSubmitBreadcrumb('<c:out value="${ ub.submit }"/>', '<c:out value="${ ub.id }"/>')"><c:out value="${ ub.label }" escapeXml="false" /></a>  </li> 
    	</c:forEach>			
    </c:if>
    	<li><fmt:message key="bsDetalleach.bc.etiqueta.crear" /></li>
    </ul>
    </div>
    	<input type="hidden" name="id_ubicacion_retorno" id="id_ubicacion_retorno" value=""/>
    </div>	
	
	
<%-- <div id="iTitle" class="ui-state-default ui-widget-content ui-corner-all">
	<fmt:message key="bsDetalleach.formulario.etiqueta.crear.titulo"/>
</div> --%>
	<input type="hidden" id="pageLoc" name="pageLoc" value="crear"/>
	<input type="hidden" id="primerCampo" name="primerCampo" value="cuenta"/>
    <input type="hidden" id="eag" name="eag" value="${eag}"/>
    <input type="hidden" id="movimientoPagina" name="movimientoPagina"/>
    <input type="hidden" id="columna" name="columna"/>
    <input type="hidden" id="fenixf31seguridad" name="fenixf31seguridad" value="<c:out value='${fenixf31seguridad}' />" />
	<c:if test="${not empty scrollTo }">
    <input type="hidden" id="scrollSmooth" value="<c:out value='${scrollTo}' />" />
    </c:if>
	<!-- valores de llaves -->
	
	


<!-- mensajes de error-->
<c:if test="${mensajesUsuario.erroresGlobales}">
<div class="ui-state-error ui-corner-all" style="padding: 0 .7em; font-size: 0.8em;"> 
	<span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em; margin-top: .3em;"></span> 
	<strong><fmt:message key="globales.formulario.etiqueta.error"/></strong> <br /> 
	<ul>
		<c:forEach items="${mensajesUsuario.mensajesDeError}" var="mensaje">
        	<li><c:out value="${mensaje}" escapeXml="false"/></li>
     	</c:forEach>
	</ul>
</div>
</c:if>

<div id="action-buttons">
<div id="action-bar">
    <button tabindex="21" id="btnGuardar" type="submit" name="ok" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsDetalleach/crearEjecutar');"><fmt:message key="globales.formulario.boton.crear.guardar"/></button>
	<button tabindex="22" id="btnCancelar" type="button" name="cancel" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsDetalleach/cancelarCrear');"><fmt:message key="globales.formulario.boton.crear.cancelar"/></button>
</div>
</div>
<br />

<!-- datos de formulario -->
<div><span style="font-size: small;">-Los campos marcados con * son obligatorios</span></div>
<div class="ui-widget ui-widget-content ui-corner-all" style="width: 99%">
		<div id="formInputHeader" class="ui-widget ui-widget-header ui-corner-top" style="padding-left: 5px;"> 
			<fmt:message key="bsDetalleach.formulario.etiqueta.crear.titulo"/> 
		</div>
		<div style="display: inline;" id="formInput">
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['cuenta']}">	
		<div class="row" >
			<span class="label" > 
                <fmt:message key="bsDetalleach.formulario.etiqueta.Cuenta"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="1" type="text" id="cuenta" name="cuenta"  value="${bsDetalleach.cuenta}" size="50"  maxlength="16"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsDetalleach/ayudaPropiedadJson','cuenta')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['cuenta'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['tipooperacion']}">	
		<div class="row" >
			<span class="label" > 
                <fmt:message key="bsDetalleach.formulario.etiqueta.Tipooperacion"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="2" alt="integer" type="text" id="tipooperacion" name="tipooperacion"  value="${bsDetalleach.tipooperacion}" size="10" maxlength="3"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsDetalleach/ayudaPropiedadJson','tipooperacion')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['tipooperacion'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['monto']}">	
		<div class="row" >
			<span class="label" style="color: #AA0707;"> 
                <fmt:message key="bsDetalleach.formulario.etiqueta.Monto"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="3" alt="numeric" type="text" id="monto" name="monto"  value="${bsDetalleach.monto}" size="10" maxlength="19"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsDetalleach/ayudaPropiedadJson','monto')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['monto'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['adenda']}">	
		<div class="row" >
			<span class="label" > 
                <fmt:message key="bsDetalleach.formulario.etiqueta.Adenda"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  		      <textarea tabindex="4" id="adenda" name="adenda" cols="45" rows="3"><c:out value="${bsDetalleach.adenda}"/></textarea>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsDetalleach/ayudaPropiedadJson','adenda')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['adenda'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['fechaestatus']}">	
		<div class="row" >
			<span class="label" style="color: #AA0707;"> 
                <fmt:message key="bsDetalleach.formulario.etiqueta.Fechaestatus"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="5" type="text" cdate="fechaestatus" id="fechaestatus" name="fechaestatus"  value='<fmt:formatDate value="${bsDetalleach.fechaestatus}" pattern="dd-MM-yyyy"/>' size="12" />
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsDetalleach/ayudaPropiedadJson','fechaestatus')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['fechaestatus'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['estatus']}">	
		<div class="row" >
			<span class="label" style="color: #AA0707;"> 
                <fmt:message key="bsDetalleach.formulario.etiqueta.Estatus"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="6" alt="integer" type="text" id="estatus" name="estatus"  value="${bsDetalleach.estatus}" size="10" maxlength="10"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsDetalleach/ayudaPropiedadJson','estatus')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['estatus'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['idpago']}">	
		<div class="row" >
			<span class="label" > 
                <fmt:message key="bsDetalleach.formulario.etiqueta.Idpago"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="7" type="text" id="idpago" name="idpago"  value="${bsDetalleach.idpago}" size="50"  maxlength="50"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsDetalleach/ayudaPropiedadJson','idpago')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['idpago'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['autorizacionhost']}">	
		<div class="row" >
			<span class="label" > 
                <fmt:message key="bsDetalleach.formulario.etiqueta.Autorizacionhost"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="8" alt="integer" type="text" id="autorizacionhost" name="autorizacionhost"  value="${bsDetalleach.autorizacionhost}" size="10" maxlength="10"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsDetalleach/ayudaPropiedadJson','autorizacionhost')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['autorizacionhost'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['email']}">	
		<div class="row" >
			<span class="label" > 
                <fmt:message key="bsDetalleach.formulario.etiqueta.Email"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="9" type="text" id="email" name="email"  value="${bsDetalleach.email}" size="50"  maxlength="75"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsDetalleach/ayudaPropiedadJson','email')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['email'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['comisionaplicada']}">	
		<div class="row" >
			<span class="label" style="color: #AA0707;"> 
                <fmt:message key="bsDetalleach.formulario.etiqueta.Comisionaplicada"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="10" alt="numeric" type="text" id="comisionaplicada" name="comisionaplicada"  value="${bsDetalleach.comisionaplicada}" size="10" maxlength="19"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsDetalleach/ayudaPropiedadJson','comisionaplicada')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['comisionaplicada'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['nombrecuenta']}">	
		<div class="row" >
			<span class="label" > 
                <fmt:message key="bsDetalleach.formulario.etiqueta.Nombrecuenta"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="11" type="text" id="nombrecuenta" name="nombrecuenta"  value="${bsDetalleach.nombrecuenta}" size="50"  maxlength="50"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsDetalleach/ayudaPropiedadJson','nombrecuenta')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['nombrecuenta'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['autorizador']}">	
		<div class="row" >
			<span class="label" > 
                <fmt:message key="bsDetalleach.formulario.etiqueta.Autorizador"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="12" type="text" id="autorizador" name="autorizador"  value="${bsDetalleach.autorizador}" size="50"  maxlength="10"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsDetalleach/ayudaPropiedadJson','autorizador')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['autorizador'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['estatuspaybank']}">	
		<div class="row" >
			<span class="label" > 
                <fmt:message key="bsDetalleach.formulario.etiqueta.Estatuspaybank"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="13" type="text" id="estatuspaybank" name="estatuspaybank"  value="${bsDetalleach.estatuspaybank}" size="50"  maxlength="10"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsDetalleach/ayudaPropiedadJson','estatuspaybank')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['estatuspaybank'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['fechahorahost']}">	
		<div class="row" >
			<span class="label" > 
                <fmt:message key="bsDetalleach.formulario.etiqueta.Fechahorahost"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="14" type="text" cdate="fechahorahost" id="fechahorahost" name="fechahorahost"  value='<fmt:formatDate value="${bsDetalleach.fechahorahost}" pattern="dd-MM-yyyy"/>' size="12" />
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsDetalleach/ayudaPropiedadJson','fechahorahost')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['fechahorahost'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['cuentadebito']}">	
		<div class="row" >
			<span class="label" > 
                <fmt:message key="bsDetalleach.formulario.etiqueta.Cuentadebito"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="15" type="text" id="cuentadebito" name="cuentadebito"  value="${bsDetalleach.cuentadebito}" size="50"  maxlength="16"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsDetalleach/ayudaPropiedadJson','cuentadebito')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['cuentadebito'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['nombredecuenta']}">	
		<div class="row" >
			<span class="label" > 
                <fmt:message key="bsDetalleach.formulario.etiqueta.Nombredecuenta"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="16" type="text" id="nombredecuenta" name="nombredecuenta"  value="${bsDetalleach.nombredecuenta}" size="50"  maxlength="50"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsDetalleach/ayudaPropiedadJson','nombredecuenta')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['nombredecuenta'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['codbanco']}">	
		<div class="row" >
			<span class="label" style="color: #AA0707;"> 
                <fmt:message key="bsDetalleach.formulario.etiqueta.Codbanco"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="17" alt="integer" type="text" id="codbanco" name="codbanco"  value="${bsDetalleach.codbanco}" size="10" maxlength="10"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsDetalleach/ayudaPropiedadJson','codbanco')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['codbanco'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['tipocuenta']}">	
		<div class="row" >
			<span class="label" style="color: #AA0707;"> 
                <fmt:message key="bsDetalleach.formulario.etiqueta.Tipocuenta"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="18" alt="integer" type="text" id="tipocuenta" name="tipocuenta"  value="${bsDetalleach.tipocuenta}" size="10" maxlength="10"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsDetalleach/ayudaPropiedadJson','tipocuenta')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['tipocuenta'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['descestatuspaybank']}">	
		<div class="row" >
			<span class="label" > 
                <fmt:message key="bsDetalleach.formulario.etiqueta.Descestatuspaybank"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="19" type="text" id="descestatuspaybank" name="descestatuspaybank"  value="${bsDetalleach.descestatuspaybank}" size="50"  maxlength="100"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsDetalleach/ayudaPropiedadJson','descestatuspaybank')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['descestatuspaybank'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['enviarahost']}">	
		<div class="row" >
			<span class="label" > 
                <fmt:message key="bsDetalleach.formulario.etiqueta.Enviarahost"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="20" alt="integer" type="text" id="enviarahost" name="enviarahost"  value="${bsDetalleach.enviarahost}" size="10" maxlength="10"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsDetalleach/ayudaPropiedadJson','enviarahost')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['enviarahost'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
		</div>
</div>		
</div>
</form>

<%@ include file="/WEB-INF/global/jsp/jq-footer.jsp" %>
</fmt:bundle>
