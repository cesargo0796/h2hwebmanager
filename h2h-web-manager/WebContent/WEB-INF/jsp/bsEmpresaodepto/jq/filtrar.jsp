<%@ taglib prefix="c"       uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt"     uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:if test="${ not empty locale}">
	<fmt:setLocale value="${ locale }" />
</c:if>
<fmt:bundle basename="com.ofbox.davivienda.h2h.resourceBundle.MessagesResources">
<c:if test="${ not empty bsEmpresaodeptoEstado.enlaceDetalle}">
	<c:set var="detalle" value="${bsEmpresaodeptoEstado.enlaceDetalle}"/>	
	<c:set var="detalleEnllavador" value="${detalle.enllavador}"/>
</c:if>	

<c:set var="nombre" value="bsEmpresaodepto"/>
<c:set var="ubicacion" value="filtrar"/>
<c:set var="primerCampo" value="clienteMin"/>
<%@ include file="/WEB-INF/global/jsp/jq-header.jsp" %>

<form  name="mainForm" id="mainForm" action="${NewtPageRenderData.actionUrl}" method="post">
<c:if test="${ not empty detalle.cabecera}">
    <jsp:include page="/WEB-INF/jsp/${detalle.cabecera.locacion}/jq/cabecera.jsp" />
</c:if>	
<div id="mainContent">
	<div class="breadCrumbHolder module" style="margin-bottom: 3px;">
    <div id="breadcrumb" class="breadCrumb module ui-widget ui-widget-content" style="width: 99%;">
    <ul>
    	<li><a href="${ctx}/inicio/blank/ria"> HOME </a> </li>
    <c:if test="${ not empty ubicacionAplicacion.ruta }">
    	<input type="hidden" name="idUbicacionAplicacionActual" id="idUbicacionAplicacionActual" value="${ ubicacionAplicacion.idUbicacionAplicacionActual }"/>
    	<c:forEach items="${ ubicacionAplicacion.ruta }" var="ub">
    		<li><a href="javascript: doSubmitBreadcrumb('<c:out value="${ ub.submit }"/>', '<c:out value="${ ub.id }"/>')"><c:out value="${ ub.label }" escapeXml="false" /></a>  </li> 
    	</c:forEach>			
    </c:if>
    	<li><fmt:message key="bsEmpresaodepto.bc.etiqueta.filtrar" /></li>
    </ul>
    </div>
    	<input type="hidden" name="id_ubicacion_retorno" id="id_ubicacion_retorno" value=""/>
    </div>
	
<%--<div id="iTitle" class="ui-state-default ui-widget-content ui-corner-all">
	<fmt:message key="bsEmpresaodepto.formulario.etiqueta.filtrar.titulo"/>
</div>--%>
<input type="hidden" id="pageLoc" name="pageLoc" value="filtrar"/>
	<input type="hidden" id="primerCampo" name="primerCampo" value="clienteMin"/>
    <input type="hidden" id="eag" name="eag" value="${eag}"/>
    <input type="hidden" id="movimientoPagina" name="movimientoPagina"/>
    <input type="hidden" id="columna" name="columna"/>
    <input type="hidden" id="fenixf31seguridad" name="fenixf31seguridad" value="<c:out value='${fenixf31seguridad}' />" />
	<c:if test="${not empty scrollTo }">
    <input type="hidden" id="scrollSmooth" value="<c:out value='${scrollTo}' />" />
    </c:if>
	<!-- valores de llaves -->


<!-- mensajes de error-->
<c:if test="${mensajesUsuario.erroresGlobales}">
<div id="iError">
	<b class="ui-round-top"> 
	<b class="ui-line-one"></b><b class="ui-line-two"></b><b class="ui-line-three"></b><b class="ui-line-four"></b> 
    </b> 
  	<table>
     	<tr>
       		<td>
         	<ol>
	     	<c:forEach items="${mensajesUsuario.mensajesDeError}" var="mensaje">
	        	<li><c:out value="${mensaje}" escapeXml="false"/></li>
	     	</c:forEach>
	     	</ol>
       		</td>
     	</tr>
  	</table>
  	<b class="ui-round-bottom"> 
	<b class="ui-line-four"></b><b class="ui-line-three"></b><b class="ui-line-two"></b><b class="ui-line-one"></b> 
  	</b> 
</div>
</c:if>

<div id="action-buttons">
<div id="action-bar">
	<button tabindex="47" id="btnAplicarFiltro" type="submit" name="Filtrar" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsEmpresaodepto/filtrarEjecutar');"><fmt:message key="globales.filtro.boton.aplicarFiltro"/></button>
	<button tabindex="48" id="btnLimpiar" type="button" name="Limpiar" onClick="limpiarCampos('mainForm');"><fmt:message key="globales.filtro.boton.limpiar"/></button>
	<button tabindex="49" id="btnCancelar" type="button" name="Cancelar" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsEmpresaodepto/cancelarFiltrar');"><fmt:message key="globales.filtro.boton.cancelar"/></button>
</div>
</div>
<br />

<!-- datos de formulario -->
<div class="ui-widget ui-widget-content ui-corner-all" style="width: 99%">
		<div id="formInputHeader" class="ui-widget ui-widget-header ui-corner-top" style="padding-left: 5px;"> 
			<fmt:message key="bsEmpresaodepto.formulario.etiqueta.filtrar.titulo"/> 
		</div>
		<div style="display: inline;" id="formInput">
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['cliente']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsEmpresaodepto.filtro.etiqueta.Cliente"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="2" alt="integerMin" type="text" id="clienteMin" name="clienteMin" value="${bsEmpresaodeptoFiltro['clienteMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="2" alt="integerMax" type="text" id="clienteMax" name="clienteMax" value="${bsEmpresaodeptoFiltro['clienteMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['cliente'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['nombre']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsEmpresaodepto.filtro.etiqueta.Nombre"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="3" type="text" id="nombre" name="nombre" value="${bsEmpresaodeptoFiltro['nombre']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['nombre'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['direccion']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsEmpresaodepto.filtro.etiqueta.Direccion"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="4" type="text" id="direccion" name="direccion" value="${bsEmpresaodeptoFiltro['direccion']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['direccion'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['telefono']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsEmpresaodepto.filtro.etiqueta.Telefono"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="5" type="text" id="telefono" name="telefono" value="${bsEmpresaodeptoFiltro['telefono']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['telefono'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['fax']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsEmpresaodepto.filtro.etiqueta.Fax"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="6" type="text" id="fax" name="fax" value="${bsEmpresaodeptoFiltro['fax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['fax'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['email']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsEmpresaodepto.filtro.etiqueta.Email"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="7" type="text" id="email" name="email" value="${bsEmpresaodeptoFiltro['email']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['email'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['nombrecontacto']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsEmpresaodepto.filtro.etiqueta.Nombrecontacto"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="8" type="text" id="nombrecontacto" name="nombrecontacto" value="${bsEmpresaodeptoFiltro['nombrecontacto']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['nombrecontacto'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['apellidocontacto']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsEmpresaodepto.filtro.etiqueta.Apellidocontacto"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="9" type="text" id="apellidocontacto" name="apellidocontacto" value="${bsEmpresaodeptoFiltro['apellidocontacto']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['apellidocontacto'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['proveedor']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsEmpresaodepto.filtro.etiqueta.Proveedor"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="10" alt="integerMin" type="text" id="proveedorMin" name="proveedorMin" value="${bsEmpresaodeptoFiltro['proveedorMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="10" alt="integerMax" type="text" id="proveedorMax" name="proveedorMax" value="${bsEmpresaodeptoFiltro['proveedorMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['proveedor'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['nivelseguridad']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsEmpresaodepto.filtro.etiqueta.Nivelseguridad"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="11" alt="integerMin" type="text" id="nivelseguridadMin" name="nivelseguridadMin" value="${bsEmpresaodeptoFiltro['nivelseguridadMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="11" alt="integerMax" type="text" id="nivelseguridadMax" name="nivelseguridadMax" value="${bsEmpresaodeptoFiltro['nivelseguridadMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['nivelseguridad'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['fechaestatus']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsEmpresaodepto.filtro.etiqueta.Fechaestatus"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="12" type="text" cdate="fechaestatusMax" cattr="minDate" id="fechaestatusMin" name="fechaestatusMin" value="<fmt:formatDate pattern="dd-MM-yyyy" value="${bsEmpresaodeptoFiltro['fechaestatusMin']}"/>"/>
				&nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
				<input tabindex="12" type="text" cdate="fechaestatusMin" cattr="maxDate" id="fechaestatusMax" name="fechaestatusMax" value="<fmt:formatDate pattern="dd-MM-yyyy" value="${bsEmpresaodeptoFiltro['fechaestatusMax']}"/>"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['fechaestatus'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['estatus']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsEmpresaodepto.filtro.etiqueta.Estatus"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="13" alt="integerMin" type="text" id="estatusMin" name="estatusMin" value="${bsEmpresaodeptoFiltro['estatusMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="13" alt="integerMax" type="text" id="estatusMax" name="estatusMax" value="${bsEmpresaodeptoFiltro['estatusMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['estatus'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['origenlotes']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsEmpresaodepto.filtro.etiqueta.Origenlotes"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="14" alt="integerMin" type="text" id="origenlotesMin" name="origenlotesMin" value="${bsEmpresaodeptoFiltro['origenlotesMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="14" alt="integerMax" type="text" id="origenlotesMax" name="origenlotesMax" value="${bsEmpresaodeptoFiltro['origenlotesMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['origenlotes'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['registrofiscal']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsEmpresaodepto.filtro.etiqueta.Registrofiscal"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="15" type="text" id="registrofiscal" name="registrofiscal" value="${bsEmpresaodeptoFiltro['registrofiscal']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['registrofiscal'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['numpatronal']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsEmpresaodepto.filtro.etiqueta.Numpatronal"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="16" alt="numericMin" type="text" id="numpatronalMin" name="numpatronalMin" value="${bsEmpresaodeptoFiltro['numpatronalMin']}"/>
				&nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
				<input tabindex="16" alt="numericMax" type="text" id="numpatronalMax" name="numpatronalMax" value="${bsEmpresaodeptoFiltro['numpatronalMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['numpatronal'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['requierecompro']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsEmpresaodepto.filtro.etiqueta.Requierecompro"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="17" alt="integerMin" type="text" id="requierecomproMin" name="requierecomproMin" value="${bsEmpresaodeptoFiltro['requierecomproMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="17" alt="integerMax" type="text" id="requierecomproMax" name="requierecomproMax" value="${bsEmpresaodeptoFiltro['requierecomproMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['requierecompro'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['esconsumidorfinal']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsEmpresaodepto.filtro.etiqueta.Esconsumidorfinal"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="18" alt="integerMin" type="text" id="esconsumidorfinalMin" name="esconsumidorfinalMin" value="${bsEmpresaodeptoFiltro['esconsumidorfinalMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="18" alt="integerMax" type="text" id="esconsumidorfinalMax" name="esconsumidorfinalMax" value="${bsEmpresaodeptoFiltro['esconsumidorfinalMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['esconsumidorfinal'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['giroempresa']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsEmpresaodepto.filtro.etiqueta.Giroempresa"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="19" type="text" id="giroempresa" name="giroempresa" value="${bsEmpresaodeptoFiltro['giroempresa']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['giroempresa'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['ciudad']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsEmpresaodepto.filtro.etiqueta.Ciudad"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="20" type="text" id="ciudad" name="ciudad" value="${bsEmpresaodeptoFiltro['ciudad']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['ciudad'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['cargo']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsEmpresaodepto.filtro.etiqueta.Cargo"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="21" type="text" id="cargo" name="cargo" value="${bsEmpresaodeptoFiltro['cargo']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['cargo'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['limitexarchivo']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsEmpresaodepto.filtro.etiqueta.Limitexarchivo"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="22" alt="numericMin" type="text" id="limitexarchivoMin" name="limitexarchivoMin" value="${bsEmpresaodeptoFiltro['limitexarchivoMin']}"/>
				&nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
				<input tabindex="22" alt="numericMax" type="text" id="limitexarchivoMax" name="limitexarchivoMax" value="${bsEmpresaodeptoFiltro['limitexarchivoMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['limitexarchivo'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['limitexlote']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsEmpresaodepto.filtro.etiqueta.Limitexlote"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="23" alt="numericMin" type="text" id="limitexloteMin" name="limitexloteMin" value="${bsEmpresaodeptoFiltro['limitexloteMin']}"/>
				&nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
				<input tabindex="23" alt="numericMax" type="text" id="limitexloteMax" name="limitexloteMax" value="${bsEmpresaodeptoFiltro['limitexloteMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['limitexlote'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['limitextransaccion']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsEmpresaodepto.filtro.etiqueta.Limitextransaccion"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="24" alt="numericMin" type="text" id="limitextransaccionMin" name="limitextransaccionMin" value="${bsEmpresaodeptoFiltro['limitextransaccionMin']}"/>
				&nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
				<input tabindex="24" alt="numericMax" type="text" id="limitextransaccionMax" name="limitextransaccionMax" value="${bsEmpresaodeptoFiltro['limitextransaccionMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['limitextransaccion'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
</div>	


</div>
</div>
</form>

<%@ include file="/WEB-INF/global/jsp/jq-footer.jsp" %>
</fmt:bundle>
</body>