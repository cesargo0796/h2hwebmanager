<%@ taglib prefix="c"       uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt"     uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:if test="${ not empty locale}">
	<fmt:setLocale value="${ locale }" />
</c:if>
<fmt:bundle basename="com.ofbox.davivienda.h2h.resourceBundle.MessagesResources">
<input type="hidden" id="nombreDetalle" name="nombreDetalle" value=""/>
<!-- valores de llaves del padre -->
  <input type="hidden" id="empresaodepto" name="empresaodepto" value="${bsEmpresaodeptoDtoCabeza.empresaodepto}"/>

<c:set var="ubicacion" value="cabecera"/>
<c:if test="${bsEmpresaodeptoEnlaceDetalle == 'bsConvenioempresa'}">
	<c:set var="nombre" value="bsConvenioempresa"/>
</c:if>

<script>
$(document).ready(function(){
	$('#iHeader').accordion({collapsible: true, active: 0});
	$('#backLink').button().removeClass('ui-button-text-only').css({'padding':'0.2em 0.5em', 'margin-right':'20px'}).bind("click", function(e) {e.stopPropagation();});;
	$('#backLinkMod').button().removeClass('ui-button-text-only').css({'padding':'0.2em 0.5em', 'margin-right':'20px'}).bind("click", function(e) {e.stopPropagation();});
});
</script>  

<!-- datos de formulario -->
<div id="iHeader">
	<h3><a id="headerInfo" href="#">
			<span id="backLink" onclick="doSubmit('bsEmpresaodepto/cancelarDetalle');"><fmt:message key="globales.cabecera.boton.regresar"/></span>
			<span id="backLinkMod" onclick="doSubmit('bsEmpresaodepto/actualizar');"><fmt:message key="globales.cabecera.boton.actualizar"/></span>	
			 <fmt:message key="bsEmpresaodepto.encabezado.etiqueta.titulo"/> ${bsEmpresaodeptoDtoCabeza.empresaodepto} &nbsp;|			<span style="font-weight: normal;"><fmt:message key="bsEmpresaodepto.encabezado.etiqueta.Cliente"/>:</span>
		    ${bsEmpresaodeptoDtoCabeza.cliente} 
 &nbsp;| 			<span style="font-weight: normal;"><fmt:message key="bsEmpresaodepto.encabezado.etiqueta.Nombre"/>:</span>
		    ${bsEmpresaodeptoDtoCabeza.nombre} 
 &nbsp;|                      	</a></h3>
	<div>
	    <div>
		<strong><fmt:message key="bsEmpresaodepto.encabezado.etiqueta.Cliente"/></strong>&nbsp;
		${bsEmpresaodeptoDtoCabeza.cliente}
		</div>
		
	    <div>
		<strong><fmt:message key="bsEmpresaodepto.encabezado.etiqueta.Nombre"/></strong>&nbsp;
		${bsEmpresaodeptoDtoCabeza.nombre}
		</div>
		
	</div>
</div>

<div id="iTab" class="ui-tabs ui-widget ui-widget-content ui-corner-top" >
 	<ul id="detailTabs" class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
	<c:if test="${bsEmpresaodeptoEnlaceDetalle == 'bsConvenioempresa'}">
		<li class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active">
			<a onClick="$('#nombreDetalle').val('bsConvenioempresa');doSubmit('bsEmpresaodepto/detalle')" class="ui-selected"><fmt:message key="bsEmpresaodepto.encabezado.etiqueta.bsConvenioempresaDrillDown"/></a>
		</li>
	</c:if> 			
	<c:if test="${bsEmpresaodeptoEnlaceDetalle != 'bsConvenioempresa'}">
		<li>
			<a onClick="$('#nombreDetalle').val('bsConvenioempresa');doSubmit('bsEmpresaodepto/detalle')"><fmt:message key="bsEmpresaodepto.encabezado.etiqueta.bsConvenioempresaDrillDown"/></a>
		</li>
	</c:if>		
	</ul>
</div>
<div id="iTabline"></div>
</fmt:bundle>