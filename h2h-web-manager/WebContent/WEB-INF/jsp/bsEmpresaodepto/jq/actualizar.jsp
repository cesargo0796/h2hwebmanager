<%@ taglib prefix="c"       uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt"     uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:if test="${ not empty locale}">
	<fmt:setLocale value="${ locale }" />
</c:if>
<fmt:bundle basename="com.ofbox.davivienda.h2h.resourceBundle.MessagesResources">
	
<c:if test="${ not empty bsEmpresaodeptoEstado.enlaceDetalle}">
	<c:set var="detalle" value="${bsEmpresaodeptoEstado.enlaceDetalle}"/>	
	<c:set var="detalleEnllavador" value="${detalle.enllavador}"/>
</c:if>		

<c:set var="nombre" value="bsEmpresaodepto"/>
<c:set var="ubicacion" value="actualizar"/>	
<c:set var="primerCampo" value="cliente"/>
<%@ include file="/WEB-INF/global/jsp/jq-header.jsp" %>
	
<form  name="mainForm" id="mainForm" action="${NewtPageRenderData.actionUrl}" method="post">
<c:if test="${ not empty detalle.cabecera}">
    <jsp:include page="/WEB-INF/jsp/${detalle.cabecera.locacion}/jq/cabecera.jsp" />
</c:if>
<div id="mainContent">

	<div class="breadCrumbHolder module" style="margin-bottom: 3px;">
<div id="breadcrumb" class="breadCrumb module ui-widget ui-widget-content" style="width: 99%;">
<ul>
	<li><a href="${ctx}/inicio/blank/ria"> HOME </a> </li>
<c:if test="${ not empty ubicacionAplicacion.ruta }">
	<input type="hidden" name="idUbicacionAplicacionActual" id="idUbicacionAplicacionActual" value="${ ubicacionAplicacion.idUbicacionAplicacionActual }"/>
	<c:forEach items="${ ubicacionAplicacion.ruta }" var="ub">
		<li><a href="javascript: doSubmitBreadcrumb('<c:out value="${ ub.submit }"/>', '<c:out value="${ ub.id }"/>')"><c:out value="${ ub.label }" escapeXml="false" /></a>  </li> 
	</c:forEach>			
</c:if>
	<li><fmt:message key="bsEmpresaodepto.bc.etiqueta.actualizar" /></li>
</ul>
</div>
	<input type="hidden" name="id_ubicacion_retorno" id="id_ubicacion_retorno" value=""/>
</div>	
	
<div id="iProcessing" style="visibility: hidden" align="center">
	<img src="${ctx}/include/images/design/processing_request.gif"></img>
</div>

<%--<div id="iTitle" class="ui-state-default ui-widget-content ui-corner-all">
	<fmt:message key="bsEmpresaodepto.formulario.etiqueta.actualizar.titulo"/>
</div> --%>
	<input type="hidden" id="pageLoc" name="pageLoc" value="actualizar"/>
	<input type="hidden" id="primerCampo" name="primerCampo" value="cliente"/>
    <input type="hidden" id="eag" name="eag" value="${eag}"/>
    <input type="hidden" id="movimientoPagina" name="movimientoPagina"/>
    <input type="hidden" id="columna" name="columna"/>
    <input type="hidden" id="fenixf31seguridad" name="fenixf31seguridad" value="<c:out value='${fenixf31seguridad}' />" />
	<c:if test="${not empty scrollTo }">
    <input type="hidden" id="scrollSmooth" value="<c:out value='${scrollTo}' />" />
    </c:if>
	<!-- valores de llaves -->
	<input type="hidden" id="empresaodepto" name="empresaodepto" value="${bsEmpresaodepto.empresaodepto}"/>	
	


<!-- mensajes de error-->
<c:if test="${mensajesUsuario.erroresGlobales}">
<div class="ui-state-error ui-corner-all" style="padding: 0 .7em; font-size: 0.8em;"> 
	<span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span> 
	<strong><fmt:message key="globales.formulario.etiqueta.error"/></strong> <br /> 
	<ul>
		<c:forEach items="${mensajesUsuario.mensajesDeError}" var="mensaje">
        	<li><c:out value="${mensaje}" escapeXml="false"/></li>
     	</c:forEach>
	</ul>
</div>
</c:if>

<div id="action-buttons">
<div id="action-bar">
	<button tabindex="24" id="btnGuardar" type="submit" name="actualizar" onClick="doSubmit<c:if test="${ not empty detalle.cabecera}">Async</c:if>('bsEmpresaodepto/actualizarEjecutar');"><fmt:message key="globales.formulario.boton.actualizar.guardar"/></button>
	<button tabindex="25" id="btnCancelar" type="button" name="cancel" onClick="doSubmit<c:if test="${ not empty detalle.cabecera}">Async</c:if>('bsEmpresaodepto/cancelarActualizar');"><fmt:message key="globales.formulario.boton.actualizar.cancelar"/></button>
</div>
</div>
<br />

<!-- datos de formulario -->
<div><span style="font-size: small;">-Los campos marcados con * son obligatorios</span></div>
<div class="ui-widget ui-widget-content ui-corner-all" style="width: 99%">
		<div id="formInputHeader" class="ui-widget ui-widget-header ui-corner-top" style="padding-left: 5px;"> 
			<fmt:message key="bsEmpresaodepto.formulario.etiqueta.actualizar.titulo"/> 
		</div>
		<div style="display: inline;" id="formInput">
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['cliente']}">	
		<div class="row">
			<span class="label" style="color: #AA0707;"> 
				<fmt:message key="bsEmpresaodepto.formulario.etiqueta.Cliente"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="1" alt="integer" type="text" id="cliente" name="cliente"  value="${bsEmpresaodepto.cliente}" size="10" maxlength="10"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsEmpresaodepto/ayudaPropiedadJson','cliente')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['cliente'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['nombre']}">	
		<div class="row">
			<span class="label" style="color: #AA0707;"> 
				<fmt:message key="bsEmpresaodepto.formulario.etiqueta.Nombre"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="2" type="text" id="nombre" name="nombre"  value="${bsEmpresaodepto.nombre}" size="50"  maxlength="50"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsEmpresaodepto/ayudaPropiedadJson','nombre')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['nombre'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['direccion']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsEmpresaodepto.formulario.etiqueta.Direccion"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="3" type="text" id="direccion" name="direccion"  value="${bsEmpresaodepto.direccion}" size="50"  maxlength="50"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsEmpresaodepto/ayudaPropiedadJson','direccion')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['direccion'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['telefono']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsEmpresaodepto.formulario.etiqueta.Telefono"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="4" type="text" id="telefono" name="telefono"  value="${bsEmpresaodepto.telefono}" size="50"  maxlength="40"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsEmpresaodepto/ayudaPropiedadJson','telefono')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['telefono'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['fax']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsEmpresaodepto.formulario.etiqueta.Fax"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="5" type="text" id="fax" name="fax"  value="${bsEmpresaodepto.fax}" size="50"  maxlength="30"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsEmpresaodepto/ayudaPropiedadJson','fax')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['fax'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['email']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsEmpresaodepto.formulario.etiqueta.Email"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="6" type="text" id="email" name="email"  value="${bsEmpresaodepto.email}" size="50"  maxlength="75"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsEmpresaodepto/ayudaPropiedadJson','email')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['email'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['nombrecontacto']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsEmpresaodepto.formulario.etiqueta.Nombrecontacto"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="7" type="text" id="nombrecontacto" name="nombrecontacto"  value="${bsEmpresaodepto.nombrecontacto}" size="50"  maxlength="30"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsEmpresaodepto/ayudaPropiedadJson','nombrecontacto')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['nombrecontacto'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['apellidocontacto']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsEmpresaodepto.formulario.etiqueta.Apellidocontacto"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="8" type="text" id="apellidocontacto" name="apellidocontacto"  value="${bsEmpresaodepto.apellidocontacto}" size="50"  maxlength="30"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsEmpresaodepto/ayudaPropiedadJson','apellidocontacto')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['apellidocontacto'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['proveedor']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsEmpresaodepto.formulario.etiqueta.Proveedor"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="9" alt="integer" type="text" id="proveedor" name="proveedor"  value="${bsEmpresaodepto.proveedor}" size="10" maxlength="10"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsEmpresaodepto/ayudaPropiedadJson','proveedor')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['proveedor'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['nivelseguridad']}">	
		<div class="row">
			<span class="label" style="color: #AA0707;"> 
				<fmt:message key="bsEmpresaodepto.formulario.etiqueta.Nivelseguridad"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="10" alt="integer" type="text" id="nivelseguridad" name="nivelseguridad"  value="${bsEmpresaodepto.nivelseguridad}" size="10" maxlength="3"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsEmpresaodepto/ayudaPropiedadJson','nivelseguridad')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['nivelseguridad'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['fechaestatus']}">	
		<div class="row">
			<span class="label" style="color: #AA0707;"> 
				<fmt:message key="bsEmpresaodepto.formulario.etiqueta.Fechaestatus"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="11" type="text" cdate="fechaestatus" id="fechaestatus" name="fechaestatus"  value="<fmt:formatDate pattern="dd-MM-yyyy" value="${bsEmpresaodepto.fechaestatus}"/>" size="10" />
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsEmpresaodepto/ayudaPropiedadJson','fechaestatus')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['fechaestatus'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['estatus']}">	
		<div class="row">
			<span class="label" style="color: #AA0707;"> 
				<fmt:message key="bsEmpresaodepto.formulario.etiqueta.Estatus"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="12" alt="integer" type="text" id="estatus" name="estatus"  value="${bsEmpresaodepto.estatus}" size="10" maxlength="3"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsEmpresaodepto/ayudaPropiedadJson','estatus')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['estatus'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['origenlotes']}">	
		<div class="row">
			<span class="label" style="color: #AA0707;"> 
				<fmt:message key="bsEmpresaodepto.formulario.etiqueta.Origenlotes"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="13" alt="integer" type="text" id="origenlotes" name="origenlotes"  value="${bsEmpresaodepto.origenlotes}" size="10" maxlength="3"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsEmpresaodepto/ayudaPropiedadJson','origenlotes')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['origenlotes'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['registrofiscal']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsEmpresaodepto.formulario.etiqueta.Registrofiscal"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="14" type="text" id="registrofiscal" name="registrofiscal"  value="${bsEmpresaodepto.registrofiscal}" size="50"  maxlength="15"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsEmpresaodepto/ayudaPropiedadJson','registrofiscal')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['registrofiscal'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['numpatronal']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="bsEmpresaodepto.formulario.etiqueta.Numpatronal"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="15" alt="numeric" type="text" id="numpatronal" name="numpatronal"  value="${bsEmpresaodepto.numpatronal}" size="10" maxlength="9"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsEmpresaodepto/ayudaPropiedadJson','numpatronal')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['numpatronal'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['requierecompro']}">	
		<div class="row">
			<span class="label" style="color: #AA0707;"> 
				<fmt:message key="bsEmpresaodepto.formulario.etiqueta.Requierecompro"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="16" alt="integer" type="text" id="requierecompro" name="requierecompro"  value="${bsEmpresaodepto.requierecompro}" size="10" maxlength="3"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsEmpresaodepto/ayudaPropiedadJson','requierecompro')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['requierecompro'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['esconsumidorfinal']}">	
		<div class="row">
			<span class="label" style="color: #AA0707;"> 
				<fmt:message key="bsEmpresaodepto.formulario.etiqueta.Esconsumidorfinal"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="17" alt="integer" type="text" id="esconsumidorfinal" name="esconsumidorfinal"  value="${bsEmpresaodepto.esconsumidorfinal}" size="10" maxlength="3"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsEmpresaodepto/ayudaPropiedadJson','esconsumidorfinal')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['esconsumidorfinal'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['giroempresa']}">	
		<div class="row">
			<span class="label" style="color: #AA0707;"> 
				<fmt:message key="bsEmpresaodepto.formulario.etiqueta.Giroempresa"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="18" type="text" id="giroempresa" name="giroempresa"  value="${bsEmpresaodepto.giroempresa}" size="50"  maxlength="50"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsEmpresaodepto/ayudaPropiedadJson','giroempresa')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['giroempresa'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['ciudad']}">	
		<div class="row">
			<span class="label" style="color: #AA0707;"> 
				<fmt:message key="bsEmpresaodepto.formulario.etiqueta.Ciudad"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="19" type="text" id="ciudad" name="ciudad"  value="${bsEmpresaodepto.ciudad}" size="50"  maxlength="50"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsEmpresaodepto/ayudaPropiedadJson','ciudad')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['ciudad'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['cargo']}">	
		<div class="row">
			<span class="label" style="color: #AA0707;"> 
				<fmt:message key="bsEmpresaodepto.formulario.etiqueta.Cargo"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="20" type="text" id="cargo" name="cargo"  value="${bsEmpresaodepto.cargo}" size="50"  maxlength="100"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsEmpresaodepto/ayudaPropiedadJson','cargo')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['cargo'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['limitexarchivo']}">	
		<div class="row">
			<span class="label" style="color: #AA0707;"> 
				<fmt:message key="bsEmpresaodepto.formulario.etiqueta.Limitexarchivo"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="21" alt="numeric" type="text" id="limitexarchivo" name="limitexarchivo"  value="${bsEmpresaodepto.limitexarchivo}" size="10" maxlength="19"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsEmpresaodepto/ayudaPropiedadJson','limitexarchivo')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['limitexarchivo'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['limitexlote']}">	
		<div class="row">
			<span class="label" style="color: #AA0707;"> 
				<fmt:message key="bsEmpresaodepto.formulario.etiqueta.Limitexlote"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="22" alt="numeric" type="text" id="limitexlote" name="limitexlote"  value="${bsEmpresaodepto.limitexlote}" size="10" maxlength="19"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsEmpresaodepto/ayudaPropiedadJson','limitexlote')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['limitexlote'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['limitextransaccion']}">	
		<div class="row">
			<span class="label" style="color: #AA0707;"> 
				<fmt:message key="bsEmpresaodepto.formulario.etiqueta.Limitextransaccion"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="23" alt="numeric" type="text" id="limitextransaccion" name="limitextransaccion"  value="${bsEmpresaodepto.limitextransaccion}" size="10" maxlength="19"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('bsEmpresaodepto/ayudaPropiedadJson','limitextransaccion')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['limitextransaccion'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
		</div>
</div>		
</div>
</form>

<%@ include file="/WEB-INF/global/jsp/jq-footer.jsp" %>
</fmt:bundle>
